#include <windows.h>
#include <dshow.h>
#include <cassert>

#pragma comment(lib, "strmiids")

HRESULT FindUnconnectedPin(IBaseFilter *pFilter, PIN_DIRECTION PinDir, IPin **ppPin);
HRESULT MatchPin(IPin *pPin, PIN_DIRECTION direction, BOOL bShouldBeConnected, BOOL *pResult);
HRESULT IsPinConnected(IPin *pPin, BOOL *pResult);
HRESULT IsPinDirection(IPin *pPin, PIN_DIRECTION dir, BOOL *pResult);
void SafeRelease_IBaseFilter(IBaseFilter **ppF);
void SafeRelease_IEnumPins(IEnumPins **EnumPins);
void SafeRelease_IPin(IPin **pin);

IMoniker *pMoniker = NULL;
HRESULT hr;
IBaseFilter *pSrc = NULL;
IPin *pSrc1 = NULL;
IBaseFilter *pWaveDest = NULL;

IBaseFilter *pWriter = NULL;
IFileSinkFilter *pSink = NULL;
IGraphBuilder *pGraph;
IEnumMoniker *pEnum;

static const GUID CLSID_WavDest =
{ 0x3c78b8e2, 0x6c4d, 0x11d1,{ 0xad, 0xe2, 0x0, 0x0, 0xf8, 0x75, 0x4b, 0x99 } };
//hr = CoCreateInstance(CLSID_WavDest, NULL,CLSCTX_INPROC, IID_IBaseFilter, (void **)&pWavDest);

HRESULT EnumerateDevices(REFGUID category, IEnumMoniker **ppEnum)
{
	// Create the System Device Enumerator.
	ICreateDevEnum *pDevEnum;

	HRESULT hr = CoCreateInstance(CLSID_SystemDeviceEnum, NULL,
		CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&pDevEnum));

	if (SUCCEEDED(hr))
	{
		// Create an enumerator for the category.
		hr = pDevEnum->CreateClassEnumerator(category, ppEnum, 0);
		if (hr == S_FALSE)
		{
			hr = VFW_E_NOT_FOUND;  // The category is empty. Treat as an error.
		}
		pDevEnum->Release();
	}
	return hr;
}

void DisplayDeviceInformation(IEnumMoniker *pEnum)
{
	IMoniker *pMoniker = NULL;
	IBaseFilter *pCap = NULL;
	

	while (pEnum->Next(1, &pMoniker, NULL) == S_OK)
	{
		IPropertyBag *pPropBag;
		HRESULT hr = pMoniker->BindToStorage(0, 0, IID_PPV_ARGS(&pPropBag));
		if (FAILED(hr))
		{
			pMoniker->Release();
			continue;
		}

		VARIANT var;
		VariantInit(&var);

		// Get description or friendly name.
		hr = pPropBag->Read(L"Description", &var, 0);
		if (FAILED(hr))
		{
			hr = pPropBag->Read(L"FriendlyName", &var, 0);
		}
		if (SUCCEEDED(hr))
		{
			printf("%S\n", var.bstrVal);
			VariantClear(&var);
		}

		hr = pPropBag->Write(L"FriendlyName", &var);

		// WaveInID applies only to audio capture devices.
		hr = pPropBag->Read(L"WaveInID", &var, 0);
		if (SUCCEEDED(hr))
		{
			printf("WaveIn ID: %d\n", var.lVal);
			VariantClear(&var);
		}

		hr = pPropBag->Read(L"DevicePath", &var, 0);
		if (SUCCEEDED(hr))
		{
			// The device path is not intended for display.
			printf("Device path: %S\n", var.bstrVal);
			VariantClear(&var);
		}

		pPropBag->Release();
		pMoniker->Release();
	}
}

// Create a filter by CLSID and add it to the graph.
HRESULT AddFilterByCLSID(
	IGraphBuilder *pGraph,      // Pointer to the Filter Graph Manager.
	REFGUID clsid,              // CLSID of the filter to create.
	IBaseFilter **ppF,          // Receives a pointer to the filter.
	LPCWSTR wszName             // A name for the filter (can be NULL).
)
{
	void SafeRelease_IBaseFilter(IBaseFilter **ppF);

	*ppF = 0;

	IBaseFilter *pFilter = NULL;

	HRESULT hr = CoCreateInstance(clsid, NULL, CLSCTX_INPROC_SERVER,
		IID_PPV_ARGS(&pFilter));
	if (FAILED(hr))
	{
		goto done;
	}

	hr = pGraph->AddFilter(pFilter, wszName);
	if (FAILED(hr))
	{
		goto done;
	}

	*ppF = pFilter;
	(*ppF)->AddRef();

done:
	SafeRelease_IBaseFilter(&pFilter);
	
	return hr;
}

void SafeRelease_IBaseFilter(IBaseFilter **ppF)
{
	if (*ppF!=NULL)
	{
		(*ppF)->Release();
	}
}

// Connect output pin to filter.
HRESULT ConnectFilters(
	IGraphBuilder *pGraph, // Filter Graph Manager.
	IPin *pOut,            // Output pin on the upstream filter.
	IBaseFilter *pDest)    // Downstream filter.
{
	IPin *pIn = NULL;

	// Find an input pin on the downstream filter.
	HRESULT hr = FindUnconnectedPin(pDest, PINDIR_INPUT, &pIn);
	if (SUCCEEDED(hr))
	{
		// Try to connect them.
		hr = pGraph->Connect(pOut, pIn);
		pIn->Release();
	}
	return hr;
}

// Return the first unconnected input pin or output pin.
HRESULT FindUnconnectedPin(IBaseFilter *pFilter, PIN_DIRECTION PinDir, IPin **ppPin)
{
	IEnumPins *pEnum = NULL;
	IPin *pPin = NULL;
	BOOL bFound = FALSE;

	HRESULT hr = pFilter->EnumPins(&pEnum);
	if (FAILED(hr))
	{
		goto done;
	}

	while (S_OK == pEnum->Next(1, &pPin, NULL))
	{
		hr = MatchPin(pPin, PinDir, FALSE, &bFound);
		if (FAILED(hr))
		{
			goto done;
		}
		if (bFound)
		{
			*ppPin = pPin;
			(*ppPin)->AddRef();
			break;
		}
		SafeRelease_IPin(&pPin);
	}

	if (!bFound)
	{
		hr = VFW_E_NOT_FOUND;
	}

done:
	SafeRelease_IPin(&pPin);
	SafeRelease_IEnumPins(&pEnum);
	return hr;
}

void SafeRelease_IEnumPins(IEnumPins **EnumPins)
{
	if (EnumPins != NULL)
	{
		(*EnumPins)->Release();
		EnumPins = NULL;
	}
}

void SafeRelease_IPin(IPin **pin)
{
	if (pin!=NULL)
	{
		(*pin)->Release();
		pin = NULL;
	}
}

// Match a pin by pin direction and connection state.
HRESULT MatchPin(IPin *pPin, PIN_DIRECTION direction, BOOL bShouldBeConnected, BOOL *pResult)
{
	assert(pResult != NULL);

	BOOL bMatch = FALSE;
	BOOL bIsConnected = FALSE;

	HRESULT hr = IsPinConnected(pPin, &bIsConnected);
	if (SUCCEEDED(hr))
	{
		if (bIsConnected == bShouldBeConnected)
		{
			hr = IsPinDirection(pPin, direction, &bMatch);
		}
	}

	if (SUCCEEDED(hr))
	{
		*pResult = bMatch;
	}
	return hr;
}

// Query whether a pin is connected to another pin.
//
// Note: This function does not return a pointer to the connected pin.

HRESULT IsPinConnected(IPin *pPin, BOOL *pResult)
{
	IPin *pTmp = NULL;
	HRESULT hr = pPin->ConnectedTo(&pTmp);
	if (SUCCEEDED(hr))
	{
		*pResult = TRUE;
	}
	else if (hr == VFW_E_NOT_CONNECTED)
	{
		// The pin is not connected. This is not an error for our purposes.
		*pResult = FALSE;
		hr = S_OK;
	}

	SafeRelease_IPin(&pTmp);
	return hr;
}

// Query whether a pin has a specified direction (input / output)
HRESULT IsPinDirection(IPin *pPin, PIN_DIRECTION dir, BOOL *pResult)
{
	PIN_DIRECTION pinDir;
	HRESULT hr = pPin->QueryDirection(&pinDir);
	if (SUCCEEDED(hr))
	{
		*pResult = (pinDir == dir);
	}
	return hr;
}

int EnumerateDevice()
{
	hr = EnumerateDevices(CLSID_VideoInputDeviceCategory, &pEnum);
	if (SUCCEEDED(hr))
	{
		printf("Displaying available video endpoints.\n");
		DisplayDeviceInformation(pEnum);
		pEnum->Release();
	}
	else
	{
		printf("Video Device details not got\n");
	}

	printf("--------------------------------------------------\n\n");

	hr = EnumerateDevices(CLSID_AudioInputDeviceCategory, &pEnum);
	if (SUCCEEDED(hr))
	{
		printf("Displaying available audio endpoints.\n");
		DisplayDeviceInformation(pEnum);
		pEnum->Release();
	}
	else
	{
		printf("Audio Device details not got\n");
	}
	return 0;
}

void main()
{
	HRESULT AddFilterByCLSID(
		IGraphBuilder *pGraph,      // Pointer to the Filter Graph Manager.
		REFGUID clsid,              // CLSID of the filter to create.
		IBaseFilter **ppF,          // Receives a pointer to the filter.
		LPCWSTR wszName             // A name for the filter (can be NULL).
	);
	int EnumerateDevice();

	hr = CoInitializeEx(NULL, COINIT_MULTITHREADED);
	if (SUCCEEDED(hr))
	{
		printf("CoInitializeEx() done\n");
	}
	else
	{
		printf("CoInitializeEx() not done\n");
		exit(-1);
	}

	EnumerateDevice();
	
	
	printf("--------------------------------------------------\n\n");
	printf("Creating the Filter Graph Manager.\n");

	hr = CoCreateInstance(CLSID_FilterGraph, NULL, CLSCTX_INPROC_SERVER,
	IID_IGraphBuilder, (void**)&pGraph);
	if (SUCCEEDED(hr))
	{
	printf("Created the Filter Graph Manager.\n");
	}

	//-----------------------------------------------------

	hr = EnumerateDevices(CLSID_AudioInputDeviceCategory, &pEnum);
	if (SUCCEEDED(hr))
	{
	pEnum->Next(1, &pMoniker, NULL);

	printf("Creating a DirectShow capture filter for the device\n");
	hr = pMoniker->BindToObject(0, 0, IID_IBaseFilter, (void**)&pSrc);
	if (SUCCEEDED(hr))
	{
	hr = pGraph->AddFilter(pSrc, L"Capture Filter");
	if (SUCCEEDED(hr))
	{
	printf("Created a DirectShow capture filter for the device\n");
	}
	else
	{
	pMoniker->Release();
	pMoniker = NULL;
	}

	hr = CoCreateInstance(CLSID_WavDest, NULL,CLSCTX_INPROC, IID_IBaseFilter, (void **)&pWaveDest);
	if (SUCCEEDED(hr))
	{
	printf("Got WavDest BaseFilter interface\n");
	}
	else
	{
	printf("Didnt get WavDest BaseFilter interface\n");
	}


	printf("Add the WavDest and the File Writer.\n");
	hr = AddFilterByCLSID(pGraph, CLSID_WavDest, &pWaveDest, L"WavDest");
	if (SUCCEEDED(hr))
	{
	printf("Added WavDest filter\n");
	}
	else
	{
	printf("Could not add WavDest Filter\n");
	}

	hr = AddFilterByCLSID(pGraph, CLSID_FileWriter, &pWriter, L"File Writer");

	// Set the file name.
	hr = pWriter->QueryInterface(IID_IFileSinkFilter, (void**)&pSink);
	hr = pSink->SetFileName(L"./MyWavFile.wav", NULL);

	//hr = ConnectFilters(pGraph, pSrc1, pWaveDest);
	//hr = ConnectFilters(pGraph, pWaveDest, pWriter);
	}
	pEnum->Release();
	}
	else
	{
	printf("Audio Device details not got\n");
	}

	CoUninitialize();
}
