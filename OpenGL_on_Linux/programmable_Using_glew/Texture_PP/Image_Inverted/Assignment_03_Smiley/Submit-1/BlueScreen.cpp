#include <iostream>
#include <stdio.h> 
#include <stdlib.h> 
#include <memory.h> 
#include <X11/Xlib.h> 
#include <X11/Xutil.h> 
#include <X11/XKBlib.h> 
#include <X11/keysym.h>
#include <GL/glew.h> //Change No 1
#include <GL/gl.h>
#include <GL/glx.h> 

#include <SOIL/SOIL.h>

#include "vmath.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;
using namespace std;

enum
{
	AC_ATTRIBUTE_VERTEX = 0,
	AC_ATTRIBUTE_COLOR,
	AC_ATTRIBUTE_NORMAL,
	AC_ATTRIBUTE_TEXTURE0,
};

FILE *gpFile = NULL;
FILE *gpExtensions = NULL;

Display *gpDisplay=NULL;

XVisualInfo *gpXVisualInfo=NULL;

Colormap gColormap;

Window gWindow;

typedef GLXContext (*glXCreateContextAttribsARBProc)(Display*, GLXFBConfig, GLXContext, Bool, const int*);

glXCreateContextAttribsARBProc glXCreateContextAttribsARB=NULL;

GLXFBConfig gGLXFBConfig;

GLXContext gGLXContext;

bool gbFullscreen = false;

GLuint gVertexShaderObject = 0;
GLuint gFragmentShaderObject = 0;
GLuint gShaderProgramObject = 0;

GLuint gVao_Quad1;
GLuint gVbo_Quad_Position;
GLuint gVbo_Quad_Texture1;

GLuint gMVPUniform;
GLuint gTexture_sampler_uniform;
GLuint gTexture_Smiley;

char ascii[32];
int keypressed=0;

const GLfloat SmileyTexCoords[] =
{
		1.0f, 1.0f,
		0.0f, 1.0f,
		0.0f, 0.0f,
		1.0f, 0.0f
};

const GLfloat TexCoords_Case1[] =
{
	0.5f, 0.5f,
	0.0f, 0.5f,
	0.0f, 0.0f,
	0.5f, 0.0f
};

const GLfloat TexCoords_Case2[] =
{
	1.0f, 1.0f,
	0.0f, 1.0f,
	0.0f, 0.0f,
	1.0f, 0.0f
};

const GLfloat TexCoords_Case3[] =
{
	2.0f, 2.0f,
	0.0f, 2.0f,
	0.0f, 0.0f,
	2.0f, 0.0f
};

const GLfloat TexCoords_Case4[] =
{
	0.5f, 0.5f,
	0.5f, 0.5f,
	0.5f, 0.5f,
	0.5f, 0.5f
};

mat4 gPerspectiveProjectionMatrix; //Change 1

void CreateWindow(void);
void ToggleFullscreen(void);
void initialize(void);
void resize(int,int);
void display(void);
void update(void);
void uninitialize(void);
void LoadGLTexture(GLuint *texture,const char *path);
	
int main(int argc, char *argv[])
{
	gpFile=fopen("Log.txt", "w");
	if (gpFile==NULL)
	{
		printf("Log File Can Not Be Created.\n");
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Is Successfully Opened.\n");
	}
	
	CreateWindow();
	
	initialize();

	XEvent event;
	KeySym keySym;
	int winWidth;
	int winHeight;
	bool bDone=false;
	
	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,&event); 
			switch(event.type) 
			{
				case MapNotify:

					break;

				case KeyPress: 
					keySym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
					switch(keySym)
					{

						case XK_Escape:
							bDone=true;
							break;

						case XK_F:
						case XK_f:
							if(gbFullscreen==false)
							{
								ToggleFullscreen();
								gbFullscreen=true;
							}
							else
							{
								ToggleFullscreen();
								gbFullscreen=false;
							}
							break;
	
							case XK_0:
								keypressed=0;
								glDeleteTextures(1, &gTexture_Smiley);
							break;
					
							case XK_1:
								keypressed=1;
								LoadGLTexture(&gTexture_Smiley,"smiley_512x512.png");
							break;
	
							case XK_2:
								keypressed=2;
								LoadGLTexture(&gTexture_Smiley,"smiley_512x512.png");
							break;
		
							case XK_3:
								keypressed=3;
								LoadGLTexture(&gTexture_Smiley,"smiley_512x512.png");
							break;
		
							case XK_4:
								keypressed=4;
								LoadGLTexture(&gTexture_Smiley,"smiley_512x512.png");
							break;

							default:
								keypressed=5;
								break;
					}
					break;

				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1: //Left MouseButton
							break;
						case 2: //Middle Mouse Button
							break;
						case 3: //Right Mouse Button
							break;
						default: 
							break;
					}
					break;

				case MotionNotify: 

					break;


				case ConfigureNotify:
					winWidth=event.xconfigure.width;
					winHeight=event.xconfigure.height;
					resize(winWidth,winHeight);
					break;

				case Expose: 

					break;

				case DestroyNotify:

					break;

				case 33: 
					bDone=true;
					break;

				default:
					break;
			}
		}
		
		display();
		update();
	}
	
	uninitialize();
	return(0);
}

void LoadGLTexture(GLuint *texture,const char *path)
{	
	int width;
	int height;
	unsigned char *imageData=NULL;

	imageData=SOIL_load_image(path,&width,&height,0,SOIL_LOAD_RGBA);
	
	glPixelStorei(GL_UNPACK_ALIGNMENT,4);
	glBindTexture(GL_TEXTURE_2D,*texture);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);
	//gluBuild2DMipmaps(GL_TEXTURE_2D,3,width,height,GL_RGBA,GL_UNSIGNED_BYTE,imageData);
	glTexImage2D(GL_TEXTURE_2D,
			0,
			GL_RGB,
			width, //bmp.bmWidth,
			height, //bmp.bmHeight,
			0,
			GL_RGBA,
			GL_UNSIGNED_BYTE,
			imageData //bmp.bmBits
			);

		glGenerateMipmap(GL_TEXTURE_2D);

	SOIL_free_image_data(imageData);
}


void CreateWindow(void)
{
	XSetWindowAttributes winAttribs;
	GLXFBConfig *pGLXFBConfigs=NULL;
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempXVisualInfo=NULL;
	int iNumFBConfigs=0;
	int styleMask;
	int i;

	int sampleBuffer,samples;
	
	static int frameBufferAttributes[]=
		{
		GLX_X_RENDERABLE,True,

		GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,

		GLX_RENDER_TYPE,GLX_RGBA_BIT,

		GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,

		GLX_RED_SIZE,8,
		GLX_GREEN_SIZE,8,
		GLX_BLUE_SIZE,8,
		GLX_ALPHA_SIZE,8,
		GLX_DEPTH_SIZE,24,
		GLX_STENCIL_SIZE,8,

		GLX_DOUBLEBUFFER,True,

		None};
	
	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("XOpenDisplay() failed\n");
		uninitialize();
		exit(1);
	}
	
	pGLXFBConfigs=glXChooseFBConfig(gpDisplay,DefaultScreen(gpDisplay),frameBufferAttributes,&iNumFBConfigs);
	if(pGLXFBConfigs==NULL)
	{
		printf( "Failed to get valid framebuffer config.\n");
		uninitialize();
		exit(1);
	}
	printf("%d Matching FB Configs Found.\n",iNumFBConfigs);
	
	int bestFramebufferConfig=-1;
	int worstFramebufferConfig=-1;
	int bestNumberOfSamples=-1;
	int worstNumberOfSamples=999;

	for(i=0;i<iNumFBConfigs;i++)
	{
		pTempXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,pGLXFBConfigs[i]);
		if(pTempXVisualInfo)
		{
			glXGetFBConfigAttrib(gpDisplay,pGLXFBConfigs[i],GLX_SAMPLE_BUFFERS,&sampleBuffer);
			glXGetFBConfigAttrib(gpDisplay,pGLXFBConfigs[i],GLX_SAMPLES,&samples);
printf("Matching Framebuffer Config=%d : Visual ID=0x%lu : SAMPLE_BUFFERS=%d : SAMPLES=%d\n",i,pTempXVisualInfo->visualid,sampleBuffer,samples);
			
			if(bestFramebufferConfig < 0  ||  sampleBuffer && samples > bestNumberOfSamples)
			{
				bestFramebufferConfig=i;
				bestNumberOfSamples=samples;
			}


			if( worstFramebufferConfig < 0  ||  !sampleBuffer ||  samples < worstNumberOfSamples)
			{
				worstFramebufferConfig=i;
			    	worstNumberOfSamples=samples;
			}
		}
		XFree(pTempXVisualInfo);
	}

	bestGLXFBConfig = pGLXFBConfigs[bestFramebufferConfig]; 

	gGLXFBConfig=bestGLXFBConfig; 
	
	XFree(pGLXFBConfigs);
	
	gpXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,bestGLXFBConfig);
	printf("Chosen Visual ID=0x%lu\n",gpXVisualInfo->visualid );
	
	winAttribs.border_pixel=0;
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(
					gpDisplay,
					RootWindow(gpDisplay,gpXVisualInfo->screen), 
					gpXVisualInfo->visual,
					AllocNone); 
										
	winAttribs.event_mask=StructureNotifyMask | KeyPressMask | ButtonPressMask |PointerMotionMask|ExposureMask | VisibilityChangeMask;
	
	styleMask=CWBorderPixel | CWEventMask | CWColormap;
	gColormap=winAttribs.colormap;										           
	
	gWindow=XCreateWindow(gpDisplay,
			      RootWindow(gpDisplay,gpXVisualInfo->screen),
			      0,
			      0,
			      WIN_WIDTH,
			      WIN_HEIGHT,
			      0,
			      gpXVisualInfo->depth,         
		              InputOutput, 
			      gpXVisualInfo->visual,
			      styleMask,
			      &winAttribs);

	if(!gWindow)
	{
		printf("XCreateWindow() failed\n");
		uninitialize();
		exit(1);
	}
	
	XStoreName(gpDisplay,gWindow,"First OpenGL BlueScreen Window");
	
	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_WINDOW_DELETE",True);

	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);
	
	XMapWindow(gpDisplay,gWindow);
}

void ToggleFullscreen(void)
{
	Atom wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);
	
	XEvent event;
	memset(&event,0,sizeof(XEvent));
	
	event.type=ClientMessage;

	event.xclient.window=gWindow;

	event.xclient.message_type=wm_state;

	event.xclient.format=32;

	event.xclient.data.l[0]=gbFullscreen ? 0 : 1;

	Atom fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);	

	event.xclient.data.l[1]=fullscreen;
	
	XSendEvent(gpDisplay,
		RootWindow(gpDisplay,gpXVisualInfo->screen),
		False, 
		StructureNotifyMask, 
		&event);	
}

void initialize(void)
{
	void LoadGLTexture(GLuint *texture,const char *path);

	GLint num;
	
	glXCreateContextAttribsARB = (glXCreateContextAttribsARBProc) glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");
	
	GLint attribs[] = 
		{
		GLX_CONTEXT_MAJOR_VERSION_ARB,3,
		GLX_CONTEXT_MINOR_VERSION_ARB,3,
		GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
		0 };
		
	gGLXContext = glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,attribs);

	if(!gGLXContext) 
	{
		GLint attribs[] = 
			{
			GLX_CONTEXT_MAJOR_VERSION_ARB,1,
			GLX_CONTEXT_MINOR_VERSION_ARB,0,
			0 };

		printf("Failed To Create GLX 3.3 context and hence using 1.0 Context\n");
		
		gGLXContext = glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,attribs);
	}
	else 
	{
		printf("OpenGL Context 3.3 Is Created.\n");
	}
	
	if(!glXIsDirect(gpDisplay,gGLXContext))
	{
		printf("Indirect GLX Rendering context obtained\n");
	}
	else
	{
		printf("Direct GLX Rendering context obtained\n" );
	}
	
	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);
	
	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		printf("Failure To Initialize GLEW. Exitting Now ...\n");
		uninitialize();
		exit(1);
	}

	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	const GLchar *vertexShaderSourceCode =
		"#version 330 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec2 vTexture0Coord;" \
		"out vec2 outTexture0Coord;" \
		"uniform mat4 u_mvp_matrix;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvp_matrix * vPosition;" \
		"outTexture0Coord=vTexture0Coord;" \
		"}";

	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

	glCompileShader(gVertexShaderObject);

	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength>0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}
	else
	{
		fprintf(gpFile, "Vertex Shader Compilation Log : No Errors\n");
	}

	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar *fragmentShaderSourceCode =
		"#version 330 core" \
		"\n" \
		"in vec2 outTexture0Coord;" \
		"out vec4 FragColor;" \
		"uniform sampler2D u_texture0_sampler;" \
		"void main(void)" \
		"{" \
		"FragColor = texture(u_texture0_sampler, outTexture0Coord);" \
		"}";

	glShaderSource(gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

	glCompileShader(gFragmentShaderObject);

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength>0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				szInfoLog = NULL;
				uninitialize();
				exit(0);
			}
		}
	}
	else
	{
		fprintf(gpFile, "Fragment Shader Compilation Log : No Errors\n");
	}

	gShaderProgramObject = glCreateProgram();
	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	glBindAttribLocation(gShaderProgramObject, AC_ATTRIBUTE_VERTEX, "vPosition");
	glBindAttribLocation(gShaderProgramObject, AC_ATTRIBUTE_TEXTURE0, "vTexture0Coord");

	glLinkProgram(gShaderProgramObject);

	GLint iShaderLinkProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderLinkProgramLinkStatus);
	if (iShaderLinkProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength>0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader program Link Log : %s\n", szInfoLog);
				free(szInfoLog);
				szInfoLog = NULL;
				uninitialize();
				exit(0);
			}
		}
	}
	else
	{
		fprintf(gpFile, "Shader program Link Log : No Errors\n");
	}

	gMVPUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");
	gTexture_sampler_uniform = glGetUniformLocation(gShaderProgramObject, "u_texture0_sampler");

	const GLfloat QuadVertices[] =
	{
		1.0f, 1.0f, 0.0f,
		-1.0f, 1.0f, 0.0f,
		-1.0f, -1.0f,0.0f,
		1.0f, -1.0f, 0.0f
	};
	
	//-------------
	glGenVertexArrays(1, &gVao_Quad1);
	glBindVertexArray(gVao_Quad1);

	//Vertices for Quad
	glGenBuffers(1, &gVbo_Quad_Position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Quad_Position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(QuadVertices), QuadVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AC_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AC_ATTRIBUTE_VERTEX);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Texture for Quad
	glGenBuffers(1, &gVbo_Quad_Texture1);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Quad_Texture1);
	glBufferData(GL_ARRAY_BUFFER, 8*sizeof(GLfloat), NULL, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AC_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AC_ATTRIBUTE_TEXTURE0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
	//-------------

	
	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_CULL_FACE);

	/*
	LoadGLTexture(&gTexture_Smiley, MAKEINTRESOURCE(IDBITMAP_SMILEY));
	glEnable(GL_TEXTURE_2D);
	*/

	glClearColor(0.0f,0.0f,0.0f,0.0f);

	gPerspectiveProjectionMatrix = mat4::identity(); //Change 2

	resize(WIN_WIDTH,WIN_HEIGHT);

}

void resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}
	
	glViewport(0,0,(GLsizei)width,(GLsizei)height);
	gPerspectiveProjectionMatrix=perspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);
}

void update(void)
{
	

}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	glUseProgram(gShaderProgramObject);

	mat4 modelViewMatrix = mat4::identity();
	mat4 modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	modelViewProjectionMatrix = gPerspectiveProjectionMatrix*modelViewMatrix;
	glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glBindVertexArray(gVao_Quad1);

	if (keypressed == 1)
	{
		glBindBuffer(GL_ARRAY_BUFFER, gVbo_Quad_Texture1);
		glBufferData(GL_ARRAY_BUFFER, sizeof(TexCoords_Case1), TexCoords_Case1, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	else if (keypressed == 2)
	{
		glBindBuffer(GL_ARRAY_BUFFER, gVbo_Quad_Texture1);
		glBufferData(GL_ARRAY_BUFFER, sizeof(TexCoords_Case2), TexCoords_Case2, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	else if (keypressed == 3)
	{
		glBindBuffer(GL_ARRAY_BUFFER, gVbo_Quad_Texture1);
		glBufferData(GL_ARRAY_BUFFER, sizeof(TexCoords_Case3), TexCoords_Case3, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	else if (keypressed == 4)
	{
		glBindBuffer(GL_ARRAY_BUFFER, gVbo_Quad_Texture1);
		glBufferData(GL_ARRAY_BUFFER, sizeof(TexCoords_Case4), TexCoords_Case4, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	else
	{

	}

	glActiveTexture(GL_TEXTURE0);
	LoadGLTexture(&gTexture_Smiley,"smiley_512x512.png");
	glBindTexture(GL_TEXTURE_2D, gTexture_Smiley);
	glUniform1i(gTexture_sampler_uniform, 0);

	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);
	//----------------

	glUseProgram(0);

	glXSwapBuffers(gpDisplay,gWindow);
}

void uninitialize(void)
{	
	if (gVao_Quad1)
	{
		glDeleteVertexArrays(1, &gVao_Quad1);
		gVao_Quad1 = 0;
	}

	if (gVbo_Quad_Position)
	{
		glDeleteBuffers(1, &gVbo_Quad_Position);
		gVbo_Quad_Position = 0;
	}

	if (gVbo_Quad_Texture1)
	{
		glDeleteBuffers(1, &gVbo_Quad_Texture1);
		gVbo_Quad_Texture1 = 0;
	}

	if (gTexture_Smiley)
	{
		glDeleteBuffers(1, &gTexture_Smiley);
		gTexture_Smiley = 0;
	}

	glDetachShader(gShaderProgramObject,gVertexShaderObject);
	glDetachShader(gShaderProgramObject,gFragmentShaderObject);

	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;

	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	glUseProgram(0);

	GLXContext currContext=glXGetCurrentContext();


	if(currContext!=NULL && currContext==gGLXContext)
	{
		glXMakeCurrent(gpDisplay,0,0);
	}
	
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
	}
	
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	
	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}
	
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}
	
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}

	if (gpFile)
	{
		fprintf(gpFile, "Log File Is Successfully Closed.\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}
