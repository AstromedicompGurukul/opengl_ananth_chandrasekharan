#include <iostream>
#include <stdio.h> 
#include <stdlib.h> 
#include <memory.h> 
#include <X11/Xlib.h> 
#include <X11/Xutil.h> 
#include <X11/XKBlib.h> 
#include <X11/keysym.h>
#include <GL/glew.h> //Change No 1
#include <GL/gl.h>
#include <GL/glx.h> 

#include "vmath.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

enum
{
	AC_ATTRIBUTE_VERTEX = 0,
	AC_ATTRIBUTE_COLOR,
	AC_ATTRIBUTE_NORMAL,
	AC_ATTRIBUTE_TEXTURE0,
};


GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

FILE *gpFile = NULL;
FILE *gpExtensions = NULL;

Display *gpDisplay=NULL;

XVisualInfo *gpXVisualInfo=NULL;

Colormap gColormap;

Window gWindow;

typedef GLXContext (*glXCreateContextAttribsARBProc)(Display*, GLXFBConfig, GLXContext, Bool, const int*);

glXCreateContextAttribsARBProc glXCreateContextAttribsARB=NULL;

GLXFBConfig gGLXFBConfig;

GLXContext gGLXContext;

bool gbFullscreen = false;

GLuint gVao;

//Change 1
GLuint gVbo_Position;

//Change 2
GLuint gVbo_Color;

GLuint gMVPUniform;

mat4 gPerspectiveProjectionMatrix; //Change 1

void CreateWindow(void);
void ToggleFullscreen(void);
void initialize(void);
void resize(int,int);
void display(void);
void uninitialize(void);
	
int main(int argc, char *argv[])
{
	gpFile=fopen("Log.txt", "w");
	if (gpFile==NULL)
	{
		printf("Log File Can Not Be Created.\n");
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Is Successfully Opened.\n");
	}
	
	CreateWindow();
	
	initialize();

	XEvent event;
	KeySym keySym;
	int winWidth;
	int winHeight;
	bool bDone=false;
	
	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,&event); 
			switch(event.type) 
			{
				case MapNotify:

					break;

				case KeyPress: 
					keySym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
					switch(keySym)
					{

						case XK_Escape:
							bDone=true;
							break;

						case XK_F:
						case XK_f:
							if(gbFullscreen==false)
							{
								ToggleFullscreen();
								gbFullscreen=true;
							}
							else
							{
								ToggleFullscreen();
								gbFullscreen=false;
							}
							break;

						default:
							break;
					}
					break;

				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1: //Left MouseButton
							break;
						case 2: //Middle Mouse Button
							break;
						case 3: //Right Mouse Button
							break;
						default: 
							break;
					}
					break;

				case MotionNotify: 

					break;


				case ConfigureNotify:
					winWidth=event.xconfigure.width;
					winHeight=event.xconfigure.height;
					resize(winWidth,winHeight);
					break;

				case Expose: 

					break;

				case DestroyNotify:

					break;

				case 33: 
					bDone=true;
					break;

				default:
					break;
			}
		}
		
		display();
	}
	
	uninitialize();
	return(0);
}

void CreateWindow(void)
{
	XSetWindowAttributes winAttribs;
	GLXFBConfig *pGLXFBConfigs=NULL;
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempXVisualInfo=NULL;
	int iNumFBConfigs=0;
	int styleMask;
	int i;

	int sampleBuffer,samples;
	
	static int frameBufferAttributes[]=
		{
		GLX_X_RENDERABLE,True,

		GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,

		GLX_RENDER_TYPE,GLX_RGBA_BIT,

		GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,

		GLX_RED_SIZE,8,
		GLX_GREEN_SIZE,8,
		GLX_BLUE_SIZE,8,
		GLX_ALPHA_SIZE,8,
		GLX_DEPTH_SIZE,24,
		GLX_STENCIL_SIZE,8,

		GLX_DOUBLEBUFFER,True,

		None};
	
	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("XOpenDisplay() failed\n");
		uninitialize();
		exit(1);
	}
	
	pGLXFBConfigs=glXChooseFBConfig(gpDisplay,DefaultScreen(gpDisplay),frameBufferAttributes,&iNumFBConfigs);
	if(pGLXFBConfigs==NULL)
	{
		printf( "Failed to get valid framebuffer config.\n");
		uninitialize();
		exit(1);
	}
	printf("%d Matching FB Configs Found.\n",iNumFBConfigs);
	
	int bestFramebufferConfig=-1;
	int worstFramebufferConfig=-1;
	int bestNumberOfSamples=-1;
	int worstNumberOfSamples=999;

	for(i=0;i<iNumFBConfigs;i++)
	{
		pTempXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,pGLXFBConfigs[i]);
		if(pTempXVisualInfo)
		{
			glXGetFBConfigAttrib(gpDisplay,pGLXFBConfigs[i],GLX_SAMPLE_BUFFERS,&sampleBuffer);
			glXGetFBConfigAttrib(gpDisplay,pGLXFBConfigs[i],GLX_SAMPLES,&samples);
printf("Matching Framebuffer Config=%d : Visual ID=0x%lu : SAMPLE_BUFFERS=%d : SAMPLES=%d\n",i,pTempXVisualInfo->visualid,sampleBuffer,samples);
			
			if(bestFramebufferConfig < 0  ||  sampleBuffer && samples > bestNumberOfSamples)
			{
				bestFramebufferConfig=i;
				bestNumberOfSamples=samples;
			}


			if( worstFramebufferConfig < 0  ||  !sampleBuffer ||  samples < worstNumberOfSamples)
			{
				worstFramebufferConfig=i;
			    	worstNumberOfSamples=samples;
			}
		}
		XFree(pTempXVisualInfo);
	}

	bestGLXFBConfig = pGLXFBConfigs[bestFramebufferConfig]; 

	gGLXFBConfig=bestGLXFBConfig; 
	
	XFree(pGLXFBConfigs);
	
	gpXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,bestGLXFBConfig);
	printf("Chosen Visual ID=0x%lu\n",gpXVisualInfo->visualid );
	
	winAttribs.border_pixel=0;
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(
					gpDisplay,
					RootWindow(gpDisplay,gpXVisualInfo->screen), 
					gpXVisualInfo->visual,
					AllocNone); 
										
	winAttribs.event_mask=StructureNotifyMask | KeyPressMask | ButtonPressMask |PointerMotionMask|ExposureMask | VisibilityChangeMask;
	
	styleMask=CWBorderPixel | CWEventMask | CWColormap;
	gColormap=winAttribs.colormap;										           
	
	gWindow=XCreateWindow(gpDisplay,
			      RootWindow(gpDisplay,gpXVisualInfo->screen),
			      0,
			      0,
			      WIN_WIDTH,
			      WIN_HEIGHT,
			      0,
			      gpXVisualInfo->depth,         
		              InputOutput, 
			      gpXVisualInfo->visual,
			      styleMask,
			      &winAttribs);

	if(!gWindow)
	{
		printf("XCreateWindow() failed\n");
		uninitialize();
		exit(1);
	}
	
	XStoreName(gpDisplay,gWindow,"First OpenGL BlueScreen Window");
	
	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_WINDOW_DELETE",True);

	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);
	
	XMapWindow(gpDisplay,gWindow);
}

void ToggleFullscreen(void)
{
	Atom wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);
	
	XEvent event;
	memset(&event,0,sizeof(XEvent));
	
	event.type=ClientMessage;

	event.xclient.window=gWindow;

	event.xclient.message_type=wm_state;

	event.xclient.format=32;

	event.xclient.data.l[0]=gbFullscreen ? 0 : 1;

	Atom fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);	

	event.xclient.data.l[1]=fullscreen;
	
	XSendEvent(gpDisplay,
		RootWindow(gpDisplay,gpXVisualInfo->screen),
		False, 
		StructureNotifyMask, 
		&event);	
}

void initialize(void)
{
	GLint num;
	
	glXCreateContextAttribsARB = (glXCreateContextAttribsARBProc) glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");
	
	GLint attribs[] = 
		{
		GLX_CONTEXT_MAJOR_VERSION_ARB,3,
		GLX_CONTEXT_MINOR_VERSION_ARB,3,
		GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
		0 };
		
	gGLXContext = glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,attribs);

	if(!gGLXContext) 
	{
		GLint attribs[] = 
			{
			GLX_CONTEXT_MAJOR_VERSION_ARB,1,
			GLX_CONTEXT_MINOR_VERSION_ARB,0,
			0 };

		printf("Failed To Create GLX 3.3 context and hence using 1.0 Context\n");
		
		gGLXContext = glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,attribs);
	}
	else 
	{
		printf("OpenGL Context 3.3 Is Created.\n");
	}
	
	if(!glXIsDirect(gpDisplay,gGLXContext))
	{
		printf("Indirect GLX Rendering context obtained\n");
	}
	else
	{
		printf("Direct GLX Rendering context obtained\n" );
	}
	
	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);
	
	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		printf("Failure To Initialize GLEW. Exitting Now ...\n");
		uninitialize();
		exit(1);
	}
	
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	/* Change 3
	In shader declared a vColor and out_color variable. We will be passing our color array of multiple colors to vColor in vertex shader.
	So vColor has "in". Further in vertex shader we will pass vColor to out_color. So it is "out".
	*/
	const GLchar *vertexShaderSourceCode =
	"#version 330 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;"\
		"out vec4 out_color;" \
		"uniform mat4 u_mvp_matrix;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvp_matrix * vPosition;" \
		"out_color = vColor;" \
		"}";

	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

	glCompileShader(gVertexShaderObject);

	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength>0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}
	else
	{
		fprintf(gpFile, "Vertex Shader Compilation Log : No Errors\n");
	}

	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	/* Change 4
	In vertex shader we did
	In shader declared a vColor and out_color variable. We will be passing our color array of multiple colors to vColor in vertex shader.
	So vColor has "in". Further in vertex shader we will pass vColor to out_color. So it is "out".

	In geometry shader
	we pass out_color to FragColor
	*/
	const GLchar *fragmentShaderSourceCode =
	"#version 330 core" \
		"\n" \
		"in vec4 out_color;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor = out_color;" \
		"}";

	glShaderSource(gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

	glCompileShader(gFragmentShaderObject);

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength>0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				szInfoLog = NULL;
				uninitialize();
				exit(0);
			}
		}
	}
	else
	{
		fprintf(gpFile, "Fragment Shader Compilation Log : No Errors\n");
	}

	gShaderProgramObject = glCreateProgram();
	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	glBindAttribLocation(gShaderProgramObject, AC_ATTRIBUTE_VERTEX, "vPosition");
	glBindAttribLocation(gShaderProgramObject, AC_ATTRIBUTE_COLOR, "vColor");  //Change 5

	glLinkProgram(gShaderProgramObject);

	GLint iShaderLinkProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderLinkProgramLinkStatus);
	if (iShaderLinkProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength>0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader program Link Log : %s\n", szInfoLog);
				free(szInfoLog);
				szInfoLog = NULL;
				uninitialize();
				exit(0);
			}
		}
	}
	else
	{
		fprintf(gpFile, "Shader program Link Log : No Errors\n");
	}

	gMVPUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");

	const GLfloat triangleVertices[] = { 
			0.0f,1.0f,0.0f,
			-1.0f,-1.0f,0.0f,
			1.0f,-1.0f,0.0f };

	//Change 6
	const GLfloat triangleColors[] = {
		1.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 1.0f
	};

	glGenVertexArrays(1, &gVao);
	glBindVertexArray(gVao);

	//Change 7
	glGenBuffers(1, &gVbo_Position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(triangleVertices), triangleVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AC_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AC_ATTRIBUTE_VERTEX);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Change 8
	glGenBuffers(1, &gVbo_Color);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(triangleColors), triangleColors, GL_STATIC_DRAW);
	glVertexAttribPointer(AC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_CULL_FACE);

	gpExtensions=fopen("Extensions.txt", "w");
	if (gpExtensions==NULL)
	{
		printf("Extensions.txt file cannot be created\n");
		exit(0);
	}
	else
	{
		fprintf(gpExtensions, "Extensions.txt File Is Successfully Opened\n");
	}

	fprintf(gpExtensions, "This is Assignment to find extension supported by my graphic card\n");

	const GLubyte *Ganesha_Vendor=(const GLubyte*)malloc(sizeof(1000*sizeof(GLubyte)));
	Ganesha_Vendor=glGetString(GL_VENDOR);
		
	const GLubyte *Ganesha_Renderer=(const GLubyte*)malloc(sizeof(1000*sizeof(GLubyte)));
	Ganesha_Renderer=glGetString(GL_RENDERER);

	const GLubyte *Ganesha_Version=(const GLubyte*)malloc(sizeof(1000*sizeof(GLubyte)));
	Ganesha_Version=glGetString(GL_VERSION);

	const GLubyte *Ganesha_ShadingLanguageVersion=(const GLubyte*)malloc(sizeof(1000*sizeof(GLubyte)));
	Ganesha_ShadingLanguageVersion=glGetString(GL_SHADING_LANGUAGE_VERSION);

	fprintf(gpExtensions, "Vendor : %s\n",Ganesha_Vendor);
	fprintf(gpExtensions, "Renderer : %s\n",Ganesha_Renderer);
	fprintf(gpExtensions, "OpenGL Version : %s\n",Ganesha_Version);
	fprintf(gpExtensions, "OpenGL Shading Language Version : %s\n",Ganesha_ShadingLanguageVersion);

	glGetIntegerv(GL_NUM_EXTENSIONS, &num);

	for (int i = 0; i<num; i++)
	{
		fprintf(gpExtensions,"%d : %s\n",i+1,(char*)glGetStringi(GL_EXTENSIONS, i));
	}

	fprintf(gpExtensions, "Extension.txt File Is Successfully Closed\n");
	fclose(gpExtensions);
	gpExtensions = NULL;

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	gPerspectiveProjectionMatrix = mat4::identity(); 

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}
		
	glViewport(0,0,(GLsizei)width,(GLsizei)height);
	gPerspectiveProjectionMatrix=perspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);

	glUseProgram(gShaderProgramObject);

	mat4 modelViewMatrix = mat4::identity();
	mat4 modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	modelViewProjectionMatrix = gPerspectiveProjectionMatrix*modelViewMatrix;

	glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glBindVertexArray(gVao);

	glDrawArrays(GL_TRIANGLES, 0, 3);

	glBindVertexArray(0);

	glUseProgram(0);

	glXSwapBuffers(gpDisplay,gWindow);
}

void uninitialize(void)
{	
	if (gVao)
	{
		glDeleteVertexArrays(1, &gVao);
		gVao = 0;
	}

	//Change 9
	if (gVbo_Position)
	{
		glDeleteBuffers(1, &gVbo_Position);
		gVbo_Position = 0;
	}

	//Change 10
	if (gVbo_Color)
	{
		glDeleteBuffers(1, &gVbo_Color);
		gVbo_Color = 0;
	}

	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;

	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	glUseProgram(0);


	GLXContext currContext=glXGetCurrentContext();

	if(currContext!=NULL && currContext==gGLXContext)
	{
		glXMakeCurrent(gpDisplay,0,0);
	}
	
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
	}
	
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	
	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}
	
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}
	
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}

	if (gpFile)
	{
		fprintf(gpFile, "Log File Is Successfully Closed.\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}
