#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/gl.h>
#include<GL/glu.h>
#include<GL/glx.h>

#include "LightArrayHeader.h"

using namespace std;

bool bFullScreen=false;
Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo=NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth=800;
int giWindowHeight=600;

GLfloat angleSpin = 0.0f;

GLXContext gGLXContext;

char ascii[32];

FILE *gpFile=NULL;

void doFileIO(const char *msg)
{
	char *temp=NULL;
	temp=(char*)malloc(1000*sizeof(char));

	strcpy(temp,"AC :");
	strcat(temp," ");
	strcat(temp,msg);

	gpFile=fopen("./Log.txt","a");
	if(gpFile==NULL)
	{
		printf("Could not open File\n");
		exit(1);
	}

	fprintf(gpFile,"%s",temp);

	fclose(gpFile);
	gpFile=NULL;

	free(temp);
	temp=NULL;
}

int main(void)
{
	void CreateWindow(void);
	void initialize(void);
	void ToggleFullScreen(void);
	void resize(int width,int height);
	void display(void);
	void update(void);

	int winWidth=giWindowWidth;
	int winHeight=giWindowHeight;

	bool bDone=false;

	if(remove("./Log.txt"))
	{

	}
	else
	{

	}

	doFileIO("Inside main()\n");

	CreateWindow();

	initialize();

	XEvent event;
	KeySym keysym;

	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,&event);
			switch(event.type)
			{
				case MapNotify:
						break;
				case KeyPress:

						keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);

						switch(keysym)
						{

							case XK_Escape:
									bDone=true;
									break;
		
							case XK_F:
							case XK_f:
								if(bFullScreen==false)
								{
									ToggleFullScreen();
									bFullScreen=true;
								}
								else
								{
									ToggleFullScreen();
									bFullScreen=false;
								}
							break;

							default:
								break;
						}	

						XLookupString(&event.xkey,ascii,sizeof(ascii),NULL,NULL);
						switch(ascii[0])
						{	
							case 'l':
								switch (lightkey)
								{
									case true:
										lightkey = false;
										glEnable(GL_LIGHT0);
										glDisable(GL_LIGHTING);
										break;

									case false:
										lightkey = true;
										glEnable(GL_LIGHT0);
										glEnable(GL_LIGHTING);
										break;
								}
							break;	

							case 'L':
								switch (lightkey)
								{
									case true:
										lightkey = false;
										glEnable(GL_LIGHT0);
										glDisable(GL_LIGHTING);
										break;

									case false:
										lightkey = true;
										glEnable(GL_LIGHT0);
										glEnable(GL_LIGHTING);
										break;
								}
							break;

							case 'w':
							xkey = false;
							ykey = false;
							zkey = false;
							light_position[0] = 0.0f;
							light_position[1] = 0.0f;
							light_position[2] = 0.0f;

								break;	
							

							case 'x':
							xkey = true;
							ykey = false;
							zkey = false;
							angleSpin = 0.0f;
							alldat->angleSpin=angleSpin;
								break;	
							
							case 'y':
							xkey = false;
							ykey = true;
							zkey = false;
							angleSpin = 0.0f;
							alldat->angleSpin=angleSpin;
								break;
					
							case 'z':
							xkey = false;
							ykey = false;
							zkey = true;
							angleSpin = 0.0f;
							alldat->angleSpin=angleSpin;	
								break;

							default:
								break;
						}					
						break;

				case ButtonPress:
						switch(event.xbutton.button)
						{
							case 1:
								break;

							case 2:
								break;

							case 3:
								break;

							default:
								break;
						}
						break;

				case MotionNotify:
						break;

				case ConfigureNotify:
						winWidth=event.xconfigure.width;
						winHeight=event.xconfigure.height;
						resize(winWidth,winHeight);
						break;
				case Expose:
						break;
				case DestroyNotify:
						break;
				case 33:
					bDone=true;
					break;

				default:
					break;
			}
		}
		display();
		update();
	}
	doFileIO("Leaving main()\n");
	return 0;
}

void update(void)
{
	angleSpin = angleSpin + 1.0f;
	if (angleSpin >= 360.0f)
	{
		angleSpin = 0.0f;
	}
}


void CreateWindow(void)
{
	void uninitialize(void);

	doFileIO("Inside CreateWindow()\n");

	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;
	
	static int frameBufferAttributes[]=
	{
		GLX_RGBA,
		GLX_RED_SIZE,8,
		GLX_GREEN_SIZE,8,
		GLX_BLUE_SIZE,8,
		GLX_ALPHA_SIZE,8,
		GLX_DOUBLEBUFFER,True,
		GLX_DEPTH_SIZE,24,
		None
	};

	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("Unable to open X Display\n");
		exit(1);
	}	

	defaultScreen=XDefaultScreen(gpDisplay);

	gpXVisualInfo=glXChooseVisual(gpDisplay,defaultScreen,frameBufferAttributes);

	winAttribs.border_pixel=0;
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),gpXVisualInfo->visual,AllocNone);
	gColormap=winAttribs.colormap;
	winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen);
	winAttribs.event_mask=ExposureMask|VisibilityChangeMask|ButtonPressMask|KeyPressMask|PointerMotionMask|StructureNotifyMask;

	styleMask=CWBorderPixel|CWBackPixel|CWEventMask|CWColormap;

	gWindow=XCreateWindow(
	gpDisplay,
	RootWindow(gpDisplay,gpXVisualInfo->screen),
	0,
	0,
	giWindowWidth,
	giWindowHeight,
	0,
	gpXVisualInfo->depth,
	InputOutput,
	gpXVisualInfo->visual,
	styleMask,
	&winAttribs
	);

	if(!gWindow)
	{
		printf("Failed to create Main Window\n");
		uninitialize();
		exit(1);
	}

	XStoreName(gpDisplay,gWindow,"First OpenGL Window");

	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);	
	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

	XMapWindow(gpDisplay,gWindow);

	doFileIO("Leaving Createwindow()\n");
}

void uninitialize(void)
{
	GLXContext currentGLXContext;

	doFileIO("Inside uninitialize()\n");

	if (alldat)
	{
		free(alldat);
		alldat = NULL;
	}

	if (quadric)
	{
		gluDeleteQuadric(quadric);
		quadric = NULL;
	}

	if (quadric1)
	{
		gluDeleteQuadric(quadric1);
		quadric1 = NULL;
	}

	if (quadric2)
	{
		gluDeleteQuadric(quadric2);
		quadric2 = NULL;
	}

	if (quadric3)
	{
		gluDeleteQuadric(quadric3);
		quadric3 = NULL;
	}

	if (quadric4)
	{
		gluDeleteQuadric(quadric4);
		quadric4 = NULL;
	}

	if (quadric5)
	{
		gluDeleteQuadric(quadric5);
		quadric5 = NULL;
	}

	if (quadric6)
	{
		gluDeleteQuadric(quadric6);
		quadric6 = NULL;
	}

	if (quadric7)
	{
		gluDeleteQuadric(quadric7);
		quadric7 = NULL;
	}

	if (quadric8)
	{
		gluDeleteQuadric(quadric8);
		quadric8 = NULL;
	}

	if (quadric9)
	{
		gluDeleteQuadric(quadric9);
		quadric9 = NULL;
	}

	if (quadric10)
	{
		gluDeleteQuadric(quadric10);
		quadric10 = NULL;
	}

	if (quadric11)
	{
		gluDeleteQuadric(quadric11);
		quadric11 = NULL;
	}

	if (quadric12)
	{
		gluDeleteQuadric(quadric12);
		quadric12 = NULL;
	}

	if (quadric13)
	{
		gluDeleteQuadric(quadric13);
		quadric13 = NULL;
	}

	if (quadric14)
	{
		gluDeleteQuadric(quadric14);
		quadric14 = NULL;
	}

	if (quadric15)
	{
		gluDeleteQuadric(quadric15);
		quadric15 = NULL;
	}

	if (quadric16)
	{
		gluDeleteQuadric(quadric16);
		quadric16 = NULL;
	}

	if (quadric17)
	{
		gluDeleteQuadric(quadric17);
		quadric17 = NULL;
	}

	if (quadric18)
	{
		gluDeleteQuadric(quadric18);
		quadric18 = NULL;
	}

	if (quadric19)
	{
		gluDeleteQuadric(quadric19);
		quadric19 = NULL;
	}

	if (quadric20)
	{
		gluDeleteQuadric(quadric20);
		quadric20 = NULL;
	}

	if (quadric21)
	{
		gluDeleteQuadric(quadric21);
		quadric21 = NULL;
	}

	if (quadric22)
	{
		gluDeleteQuadric(quadric22);
		quadric22 = NULL;
	}

	if (quadric23)
	{
		gluDeleteQuadric(quadric23);
		quadric23 = NULL;
	}

	currentGLXContext=glXGetCurrentContext();

	if(currentGLXContext!=NULL && currentGLXContext==gGLXContext)
	{
		glXMakeCurrent(gpDisplay,0,0);
	}

	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
	}

	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo==NULL;
	}

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}

	if(gpFile)
	{
		fclose(gpFile);
		gpFile=NULL;
	}	

	doFileIO("Leaving uninitialize()\n");
}

void initialize(void)
{
	void resize(int,int);
	
	doFileIO("Inside initialize()\n");

	gGLXContext=glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);
	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);
	glClearColor(0.0f,0.0f,0.0f,0.0f);

	//Defined for GL_LIGHT0
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffused);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);

	//Defined Background color
	glClearColor(0.25f, 0.25f, 0.25f, 0.0f);

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glEnable(GL_LIGHT0);
	glEnable(GL_AUTO_NORMAL);
	glEnable(GL_NORMALIZE);
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT,light_model_ambient);
	glLightModelf(GL_LIGHT_MODEL_LOCAL_VIEWER,light_model_local_viewer);

	resize(giWindowWidth,giWindowHeight);

	doFileIO("Leaving uninitialize()\n");
}

void resize(int width,int height)
{	
	doFileIO("Inside resize()\n");

	if(height==0)
	{
		height=1;
	}
	glViewport(0,0,(GLsizei)width,(GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

	doFileIO("Leaving resize()\n");
}

void ToggleFullScreen(void)
{
	doFileIO("Inside ToggleFullScreen()\n");

	Atom wm_state;
	Atom fullscreen;

	XEvent xev={0};

	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);
	memset(&xev,0,sizeof(xev));

	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=bFullScreen?0:1;

	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
	xev.xclient.data.l[1]=fullscreen;

	XSendEvent(
	gpDisplay,
	RootWindow(gpDisplay,gpXVisualInfo->screen),
	False,
	StructureNotifyMask,
	&xev
	);

	doFileIO("Outside ToggleFullScreen()\n");
	
}

void display(void)
{	
	void DrawSphere(AllData *alldat);
	void FillOneArraytoAnother(GLfloat *dest, GLfloat *source, int count);
	void FillValueInArr(GLfloat *dest, GLfloat x, GLfloat y, GLfloat z);

	doFileIO("Inside Display()\n");
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//----------------------------------------

	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient10, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused11, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular12, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness13, 1);
	alldat->quadric = quadric;
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.8f, 0.9f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);
		
	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient20, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused21, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular22, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness23, 1);
	alldat->quadric = quadric1;
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.2f, 0.9f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient30, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused31, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular32, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness33, 1);
	alldat->quadric = quadric2;
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 0.4f, 0.9f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient40, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused41, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular42, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness43, 1);
	alldat->quadric = quadric3;
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 1.0f, 0.9f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	//----------------------------------------

	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient50, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused51, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular52, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness53, 1);
	alldat->quadric = quadric4;
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.8f, 0.575f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);


	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient60, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused61, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular62, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness63, 1);
	alldat->quadric = quadric5;
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.2f, 0.575f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient70, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused71, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular72, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness73, 1);
	alldat->quadric = quadric6;
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 0.4f, 0.575f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	//----------------------------------------		
	
	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient80, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused81, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular82, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness83, 1);
	alldat->quadric = quadric7;
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 1.0f, 0.575f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient90, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused91, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular92, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness93, 1);
	alldat->quadric = quadric8;
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.8f, 0.25f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f); 
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient100, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused101, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular102, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness103, 1);
	alldat->quadric = quadric9;
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.2f, 0.25f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f); 
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient200, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused201, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular202, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness203, 1);
	alldat->quadric = quadric10;
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 0.4f, 0.25f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f); 
	DrawSphere(alldat);
	
	//----------------------------------------------

	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient300, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused301, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular302, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness303, 1);
	alldat->quadric = quadric11;
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 1.0f, 0.25f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f); 
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient400, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused401, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular402, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness403, 1);
	alldat->quadric = quadric12;
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.8f, -0.075f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f); 
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient500, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused501, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular502, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness503, 1);
	alldat->quadric = quadric13;
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.2f, -0.075f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient600, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused601, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular602, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness603, 1);
	alldat->quadric = quadric14;
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 0.4f, -0.075f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f); 
	DrawSphere(alldat);

	//----------------------------------------------

	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient700, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused701, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular702, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness703, 1);
	alldat->quadric = quadric15;
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 1.0f, -0.075f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f); 
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient800, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused801, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular802, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness803, 1);
	alldat->quadric = quadric16;
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.8f, -0.4f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f); 
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient900, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused901, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular902, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness903, 1);
	alldat->quadric = quadric17;
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.2f, -0.4f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient1000, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused1001, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular1002, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness1003, 1);
	alldat->quadric = quadric18;
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 0.4f, -0.4f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f); 
	DrawSphere(alldat);

	//----------------------------------------------
	
	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient2000, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused2001, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular2002, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness2003, 1);
	alldat->quadric = quadric19;
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 1.0f, -0.4f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient3000, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused3001, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular3002, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness3003, 1);
	alldat->quadric = quadric20;
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.8f, -0.725f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f); 
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient4000, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused4001, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular4002, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness4003, 1);
	alldat->quadric = quadric21;
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.2f, -0.725f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient5000, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused5001, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular5002, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness5003, 1);
	alldat->quadric = quadric22;
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 0.4f, -0.725f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f); 
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient6000, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused6001, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular6002, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness6003, 1);
	alldat->quadric = quadric23;
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 1.0f, -0.725f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	glXSwapBuffers(gpDisplay,gWindow);

	doFileIO("Outside Display()\n");
}

void DrawSphere(AllData *alldat)
{
	glMaterialfv(GL_FRONT, GL_AMBIENT, alldat->AmbientMatArr);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, alldat->DiffusedMatArr);
	glMaterialfv(GL_FRONT, GL_SPECULAR,alldat->SpecularMatArr);
	glMaterialf(GL_FRONT, GL_SHININESS, alldat->ShinynessMat);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(alldat->translateArr[0], alldat->translateArr[1], alldat->translateArr[2]);
	glScalef(alldat->ScalingArr[0], alldat->ScalingArr[1], alldat->ScalingArr[2]);

	if (xkey == true && ykey == false && zkey == false)
	{
		glRotatef(alldat->angleSpin, 1.0f, 0.0f, 0.0f);
		light_position[0] = 0.0f;
		light_position[1] = alldat->angleSpin;
		light_position[2] = 0.0f;
		glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	}
	else if (ykey == true && xkey == false && zkey == false)
	{
		glRotatef(alldat->angleSpin, 0.0f, 1.0f, 0.0f);
		light_position[0] = alldat->angleSpin;
		light_position[1] = 0.0f;
		light_position[2] = 0.0f;
		glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	}
	else if (zkey == true && xkey == false && ykey == false)
	{
		glRotatef(alldat->angleSpin, 0.0f, 0.0f, 1.0f);
		light_position[0] = alldat->angleSpin;
		light_position[1] = 0.0f;
		light_position[2] = 0.0f;
		glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	}
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	alldat->quadric = gluNewQuadric();
	gluSphere(alldat->quadric, 0.75f, 30, 30);
}

void FillOneArraytoAnother(GLfloat *dest,GLfloat *source,int count)
{
	if (count==4)
	{
		for (int i = 0; i <= 3; i++)
		{
			*(dest + i) = *(source + i);
		}
	}
	else if(count==3)
	{
		for (int i = 0; i <= 2; i++)
		{
			*(dest + i) = *(source + i);
		}
	}
	else if (count == 1)
	{
		*dest = *source;
	}
}

void FillValueInArr(GLfloat *dest,GLfloat x,GLfloat y,GLfloat z)
{
	dest[0] = x;
	dest[1] = y;
	dest[2] = z;
}



