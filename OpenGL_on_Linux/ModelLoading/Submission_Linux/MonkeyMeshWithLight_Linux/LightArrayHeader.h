#pragma once
GLfloat light_ambient[] = { 1.0f,0.0f,0.0f,0.0f };
GLfloat light_diffused[] = { 1.0f,0.0f,0.0f,0.0f };
GLfloat light_specular[] = { 1.0f,1.0f,1.0f,0.0f };
GLfloat light_position[] = { 1.0f,1.0f,2.0f,0.0f };
GLfloat material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_shinyness = 50.0f;
