#pragma once
#include "LightArrayHeader.h"

#define BUFFER_SIZE 256
#define S_EQUAL 0

#define NR_POINT_COORDS 3
#define NR_TEXTURE_COORDS 2
#define NR_NORMAL_COORDS 3
#define NR_FACE_TOKENS 3

#define MONKEY_HEAD_X_TRANSLATE 0.0f
#define MONKEY_HEAD_Y_TRANSLATE 0.0f
#define MONKEY_HEAD_Z_TRANSLATE -5.0f

#define MONKEY_HEAD_X_SCALEFACTOR 1.5f
#define MONKEY_HEAD_Y_SCLAEFACTOR 1.5f
#define MONKEY_HEAD_Z_SCALEFACTOR 1.5f

#define START_ANGLE_POS 0.0f
#define END_ANGLE_POS 360.0f
#define MONKEY_HEAD_ANGLE_INCREMENT  1.0f

std::vector<std::vector<float>> g_vertices;
std::vector<std::vector<float>> g_texture;
std::vector<std::vector<float>> g_normals;
std::vector<std::vector<int>> g_face_tri;
std::vector<std::vector<int>> g_face_texture;
std::vector<std::vector<int>> g_face_normals;

GLfloat g_rotate;

FILE *g_fp_meshfile = NULL;

FILE *g_fp_logfile = NULL;

char line[BUFFER_SIZE];
