#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/gl.h>
#include<GL/glu.h>
#include<GL/glx.h>

#include<vector>
#include "CodeVariables.h"

using namespace std;

bool bFullScreen=false;
Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo=NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth=800;
int giWindowHeight=600;

bool lightkey=false;

GLXContext gGLXContext;

char ascii[32];

FILE *gpFile=NULL;

void doFileIO(const char *msg)
{
	char *temp=NULL;
	temp=(char*)malloc(1000*sizeof(char));

	strcpy(temp,"AC :");
	strcat(temp," ");
	strcat(temp,msg);

	gpFile=fopen("./Log.txt","a");
	if(gpFile==NULL)
	{
		printf("Could not open File\n");
		exit(1);
	}

	fprintf(gpFile,"%s",temp);

	fclose(gpFile);
	gpFile=NULL;

	free(temp);
	temp=NULL;
}

int main(void)
{
	void CreateWindow(void);
	void initialize(void);
	void ToggleFullScreen(void);
	void resize(int width,int height);
	void display(void);
	void update(void);

	int winWidth=giWindowWidth;
	int winHeight=giWindowHeight;

	bool bDone=false;

	if(remove("./Log.txt"))
	{

	}
	else
	{

	}

	doFileIO("Inside main()\n");

	CreateWindow();

	initialize();

	XEvent event;
	KeySym keysym;

	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,&event);
			switch(event.type)
			{
				case MapNotify:
						break;
				case KeyPress:

						keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);

						switch(keysym)
						{

							case XK_Escape:
									bDone=true;
									break;
		
							case XK_F:
							case XK_f:
								if(bFullScreen==false)
								{
									ToggleFullScreen();
									bFullScreen=true;
								}
								else
								{
									ToggleFullScreen();
									bFullScreen=false;
								}
							break;

							default:
								break;
						}	

						XLookupString(&event.xkey,ascii,sizeof(ascii),NULL,NULL);
						switch(ascii[0])
						{	
							case 'l':
							case 'L':
								switch (lightkey)
								{
									case true:
										lightkey = false;
										glEnable(GL_LIGHT0);
										glDisable(GL_LIGHTING);
										break;

									case false:
										lightkey = true;
										glEnable(GL_LIGHT0);
										glEnable(GL_LIGHTING);
										break;
								}
								break;	
						}					
						break;

				case ButtonPress:
						switch(event.xbutton.button)
						{
							case 1:
								break;

							case 2:
								break;

							case 3:
								break;

							default:
								break;
						}
						break;

				case MotionNotify:
						break;

				case ConfigureNotify:
						winWidth=event.xconfigure.width;
						winHeight=event.xconfigure.height;
						resize(winWidth,winHeight);
						break;
				case Expose:
						break;
				case DestroyNotify:
						break;
				case 33:
					bDone=true;
					break;

				default:
					break;
			}
		}
		display();
		update();
	}
	doFileIO("Leaving main()\n");
	return 0;
}

void LoadMeshData(void)
{
	void uninitialize(void);

	char sep_space[]=" ";
	char sep_fslash[]="/";

	char *first_token = NULL;

	char *token = NULL;

	char *face_tokens[NR_FACE_TOKENS];

	int nr_tokens;

	char *token_vertex_index = NULL;

	char *token_texture_index = NULL;

	char *token_normal_index = NULL;

	g_fp_meshfile=fopen("MonkeyHead.OBJ","r");
	if (g_fp_meshfile == NULL)
	{
		uninitialize();
		printf("MonkeyHead.OBJ file failed to open\n");
		exit(0);
	}
	else
	{
		printf("MonkeyHead.OBJ file opened\n");
	}
	
	while (fgets(line,BUFFER_SIZE,g_fp_meshfile))
	{
		first_token=strtok(line,sep_space);
		
		if (strcmp(first_token,"v")==S_EQUAL)
		{
			printf("v\n");
			std::vector<float> vec_point_coord(NR_POINT_COORDS);

			for (int i=0;i!=NR_POINT_COORDS;i++)
			{
				vec_point_coord[i] = atof(strtok(NULL,sep_space));
			}
			g_vertices.push_back(vec_point_coord);
		}
		else if (strcmp(first_token, "vt") == S_EQUAL)
		{
			printf("vt\n");
			std::vector<float> vec_texture_coord(NR_TEXTURE_COORDS);

			for (int i = 0; i != NR_TEXTURE_COORDS; i++)
			{
				vec_texture_coord[i] = atof(strtok(NULL, sep_space));
			}
			g_texture.push_back(vec_texture_coord);
		}
		else if (strcmp(first_token, "vn") == S_EQUAL)
		{
			printf("vn\n");
			std::vector<float> vec_normal_coord(NR_NORMAL_COORDS);

			for (int i = 0; i != NR_NORMAL_COORDS; i++)
			{
				vec_normal_coord[i] = atof(strtok(NULL, sep_space));
			}
			g_normals.push_back(vec_normal_coord);
		}
		else if (strcmp(first_token,"f")==S_EQUAL)
		{
			printf("f\n");
			std::vector<int> triangle_vertex_indices(3);
			std::vector<int> texture_vertex_indices(3);
			std::vector<int> normal_vertex_indices(3);

			memset((void*)face_tokens,0,NR_FACE_TOKENS);

			nr_tokens = 0;
			while (token=strtok(NULL,sep_space))
			{
				if (strlen(token)<3)
				{
					break;
				}
				face_tokens[nr_tokens] = token;
				nr_tokens++;
			}

			for (int i=0;i!=NR_FACE_TOKENS;++i)
			{
				token_vertex_index = strtok(face_tokens[i],sep_fslash);
				token_texture_index = strtok(NULL, sep_fslash);
				token_normal_index = strtok(NULL, sep_fslash);
				triangle_vertex_indices[i] = atoi(token_vertex_index);
				texture_vertex_indices[i] = atoi(token_texture_index);
				normal_vertex_indices[i] = atoi(token_normal_index);
			}

			g_face_tri.push_back(triangle_vertex_indices);
			g_face_texture.push_back(texture_vertex_indices);
			g_face_normals.push_back(normal_vertex_indices);
		}
		memset((void*)line,(int)'\0',BUFFER_SIZE);
	}
	
	
	fclose(g_fp_meshfile);
	g_fp_meshfile=NULL;

	fprintf(g_fp_logfile,"g_vertices:%lu g_texture:%lu g_normals:%lu g_face_tri:%lu\n",g_vertices.size(),g_texture.size(),g_normals.size(),g_face_tri.size());
}

void update(void)
{	
	doFileIO("Inside update()\n");

	
	g_rotate = g_rotate + MONKEY_HEAD_ANGLE_INCREMENT;

	if (g_rotate>=END_ANGLE_POS)
	{
		g_rotate = START_ANGLE_POS;
	}
	

	doFileIO("Leaving update()\n");
}


void CreateWindow(void)
{
	void uninitialize(void);

	doFileIO("Inside CreateWindow()\n");

	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;

	/*
	static int frameBufferAttributes[]=
	{
		GLX_RGBA,
		GLX_RED_SIZE,1,
		GLX_GREEN_SIZE,1,
		GLX_BLUE_SIZE,1,
		GLX_ALPHA_SIZE,1,
		None
	};
	*/
	
	static int frameBufferAttributes[]=
	{
		GLX_RGBA,
		GLX_RED_SIZE,8,
		GLX_GREEN_SIZE,8,
		GLX_BLUE_SIZE,8,
		GLX_ALPHA_SIZE,8,
		GLX_DOUBLEBUFFER,True,
		GLX_DEPTH_SIZE,24,
		None
	};

	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("Unable to open X Display\n");
		exit(1);
	}	

	defaultScreen=XDefaultScreen(gpDisplay);

	gpXVisualInfo=glXChooseVisual(gpDisplay,defaultScreen,frameBufferAttributes);

	winAttribs.border_pixel=0;
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),gpXVisualInfo->visual,AllocNone);
	gColormap=winAttribs.colormap;
	winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen);
	winAttribs.event_mask=ExposureMask|VisibilityChangeMask|ButtonPressMask|KeyPressMask|PointerMotionMask|StructureNotifyMask;

	styleMask=CWBorderPixel|CWBackPixel|CWEventMask|CWColormap;

	gWindow=XCreateWindow(
	gpDisplay,
	RootWindow(gpDisplay,gpXVisualInfo->screen),
	0,
	0,
	giWindowWidth,
	giWindowHeight,
	0,
	gpXVisualInfo->depth,
	InputOutput,
	gpXVisualInfo->visual,
	styleMask,
	&winAttribs
	);

	if(!gWindow)
	{
		printf("Failed to create Main Window\n");
		uninitialize();
		exit(1);
	}

	XStoreName(gpDisplay,gWindow,"First OpenGL Window");

	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);	
	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

	XMapWindow(gpDisplay,gWindow);

	doFileIO("Leaving Createwindow()\n");
}

void uninitialize(void)
{
	GLXContext currentGLXContext;

	doFileIO("Inside uninitialize()\n");

	currentGLXContext=glXGetCurrentContext();

	if(currentGLXContext!=NULL && currentGLXContext==gGLXContext)
	{
		glXMakeCurrent(gpDisplay,0,0);
	}

	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
	}

	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo==NULL;
	}

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}

	if(gpFile)
	{
		fclose(gpFile);
		gpFile=NULL;
	}	

	if(g_fp_logfile)
	{
		fclose(g_fp_logfile);
		g_fp_logfile=NULL;
	}

	doFileIO("Leaving uninitialize()\n");
}

void initialize(void)
{
	void resize(int,int);
	void LoadMeshData(void);
	
	doFileIO("Inside initialize()\n");

	g_fp_logfile=fopen("MonkeyHeader.log","w");
	if (g_fp_logfile==NULL)
	{
		uninitialize();
		exit(0);
	}
	else
	{
		printf("MonkeyHeader.log file opened\n");
	}

	printf("Step 1\n");
	gGLXContext=glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);
	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);

	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffused);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness);
	
	glClearColor(0.0f,0.0f,0.0f,0.0f);

	printf("Step 2\n");
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glClearDepth(1.0f);
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);
	glEnable(GL_LIGHT0);

	printf("Step 3\n");
	LoadMeshData();
	
	resize(giWindowWidth,giWindowHeight);

	doFileIO("Leaving uninitialize()\n");
}

void resize(int width,int height)
{	
	doFileIO("Inside resize()\n");

	if(height==0)
	{
		height=1;
	}
	glViewport(0,0,(GLsizei)width,(GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 200.0f);

	doFileIO("Leaving resize()\n");
}

void ToggleFullScreen(void)
{
	doFileIO("Inside ToggleFullScreen()\n");

	Atom wm_state;
	Atom fullscreen;

	XEvent xev={0};

	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);
	memset(&xev,0,sizeof(xev));

	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=bFullScreen?0:1;

	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
	xev.xclient.data.l[1]=fullscreen;

	XSendEvent(
	gpDisplay,
	RootWindow(gpDisplay,gpXVisualInfo->screen),
	False,
	StructureNotifyMask,
	&xev
	);

	doFileIO("Outside ToggleFullScreen()\n");
	
}

void display(void)
{	
	doFileIO("Inside Display()\n");
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	
	glTranslatef(MONKEY_HEAD_X_TRANSLATE,MONKEY_HEAD_Y_TRANSLATE,MONKEY_HEAD_Z_TRANSLATE);
	glScalef(MONKEY_HEAD_X_SCALEFACTOR,MONKEY_HEAD_Y_SCLAEFACTOR,MONKEY_HEAD_Z_SCALEFACTOR);
	glRotatef(g_rotate, 0.0f, 1.0f, 0.0f);

	glFrontFace(GL_CCW);

	glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);

	for (int i=0;i!=g_face_tri.size();++i)
	{
		glBegin(GL_TRIANGLES);
		for (int j=0;j!=g_face_tri[i].size();j++)
		{
			int vi = g_face_tri[i][j] - 1;
			glVertex3f(g_vertices[vi][0],g_vertices[vi][1],g_vertices[vi][2]);
		}
		glEnd();
	}
		
	glXSwapBuffers(gpDisplay,gWindow);

	doFileIO("Outside Display()\n");
}
