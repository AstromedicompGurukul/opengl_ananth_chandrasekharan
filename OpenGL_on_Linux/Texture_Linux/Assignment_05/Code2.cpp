#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/gl.h>
#include<GL/glu.h>
#include<GL/glx.h>

#include<SOIL/SOIL.h>

#define CHECK_IMAGE_WIDTH 64
#define CHECK_IMAGE_HEIGHT 64

using namespace std;

bool bFullScreen=false;
Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo=NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth=800;
int giWindowHeight=600;

GLXContext gGLXContext;

GLubyte checkImage[CHECK_IMAGE_WIDTH][CHECK_IMAGE_HEIGHT][4];
GLuint texname;

int sx, sy;

FILE *gpFile=NULL;
char ascii[32];

void doFileIO(const char *msg)
{
	char *temp=NULL;
	temp=(char*)malloc(1000*sizeof(char));

	strcpy(temp,"AC :");
	strcat(temp," ");
	strcat(temp,msg);

	gpFile=fopen("./Log.txt","a");
	if(gpFile==NULL)
	{
		printf("Could not open File\n");
		exit(1);
	}

	fprintf(gpFile,"%s",temp);

	fclose(gpFile);
	gpFile=NULL;

	free(temp);
	temp=NULL;
}

int main(void)
{
	void CreateWindow(void);
	void initialize(void);
	void ToggleFullScreen(void);
	void resize(int width,int height);
	void display(void);

	int winWidth=giWindowWidth;
	int winHeight=giWindowHeight;

	bool bDone=false;

	doFileIO("Inside main()\n");

	CreateWindow();

	initialize();

	XEvent event;
	KeySym keysym;

	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,&event);
			switch(event.type)
			{
				case MapNotify:
						break;
				case KeyPress:

						keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);

						switch(keysym)
						{

							case XK_Escape:
									bDone=true;
									break;
		
							case XK_F:
							case XK_f:
								if(bFullScreen==false)
								{
									ToggleFullScreen();
									bFullScreen=true;
								}
								else
								{
									ToggleFullScreen();
									bFullScreen=false;
								}
							break;


							default:
								break;
						}	

						XLookupString(&event.xkey,ascii,sizeof(ascii),NULL,NULL);

						switch(ascii[0])
						{							
							//case 0x31:
							//case VK_NUMPAD1:
							case '1':
								glViewport(0, (GLsizei)sy / 2, (GLsizei)sx / 2, (GLsizei)sy / 2);
							break;
	
							//case 0x32:
							//case VK_NUMPAD2:
							case '2':
								glViewport((GLsizei)sx / 2,(GLsizei)sy/2, (GLsizei)sx / 2,(GLsizei)sy / 2);
							break;
		
							//case 0x33:
							//case VK_NUMPAD3:
							case '3':
								glViewport(0,0, (GLsizei)sx/2, (GLsizei)sy/2 );
							break;
		
							//case 0x34:
							//case VK_NUMPAD4:
							case '4':
								glViewport((GLsizei)sx / 2, 0, (GLsizei)sx / 2, (GLsizei)sy / 2);
							break;
		
							//case 0x35:
							//case VK_NUMPAD5:
							case '5':
								glViewport(0, 0, (GLsizei)sx / 2, (GLsizei)sy);
							break;
		
							//case 0x36:
							//case VK_NUMPAD6:
							case '6':
								glViewport((GLsizei)sx/2, 0 , (GLsizei)sx/2, (GLsizei)sy);
							break;

							//case 0x37:
							//case VK_NUMPAD7:
							case '7':
								glViewport(0, (GLsizei)sy / 2, (GLsizei)sx, (GLsizei)sy / 2);
							break;

							//case 0x38:
							//case VK_NUMPAD8:
							case '8':
								glViewport(0, 0, (GLsizei)sx, (GLsizei)sy / 2);
							break;
		
							//case 0x39:
							//case VK_NUMPAD9:
							case '9':
								glViewport((GLsizei)sx/3, (GLsizei)sy / 3, (GLsizei)sx / 3, (GLsizei)sy / 3);
							break;
						}
						break;

				case ButtonPress:
						switch(event.xbutton.button)
						{
							case 1:
								break;

							case 2:
								break;

							case 3:
								break;

							default:
								break;
						}
						break;

				case MotionNotify:
						break;

				case ConfigureNotify:
						//winWidth=event.xconfigure.width;
						//winHeight=event.xconfigure.height;
						//resize(winWidth,winHeight);

						sx = DisplayWidth(gpDisplay,0);
						sy = DisplayHeight(gpDisplay,0);
						resize(sx,sy);
						break;
				case Expose:
						break;
				case DestroyNotify:
						break;
				case 33:
					bDone=true;
					break;

				default:
					break;
			}
		}
		display();
	}
	doFileIO("Outside main()\n");
	return 0;
}

void LoadGLTexture(void)
{
	void MakeCheckImage(void);

	MakeCheckImage();

	glPixelStorei(GL_UNPACK_ALIGNMENT,1);

	glGenTextures(1,&texname);

	glBindTexture(GL_TEXTURE_2D,texname);

	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);

	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);

	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);

	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);

	glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,CHECK_IMAGE_WIDTH,CHECK_IMAGE_HEIGHT,0,GL_RGBA,GL_UNSIGNED_BYTE,checkImage);

	glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_REPLACE);
}

void MakeCheckImage(void)
{
	int i, j;
	int c;

	for (i=0;i<CHECK_IMAGE_HEIGHT;i++)
	{
		for (j=0;j<CHECK_IMAGE_WIDTH;j++)
		{
			c = ( ((i&0x8)==0) ^ ((j&0x8)==0))*255;

			checkImage[i][j][0] = (GLubyte)c;
			checkImage[i][j][1] = (GLubyte)c;
			checkImage[i][j][2] = (GLubyte)c;
			checkImage[i][j][3] = (GLubyte)255;
		}
	}
}

void CreateWindow(void)
{
	void uninitialize(void);

	doFileIO("Inside CreateWindow()\n");

	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;

	static int frameBufferAttributes[]=
	{
		GLX_RGBA,
		GLX_RED_SIZE,8,
		GLX_GREEN_SIZE,8,
		GLX_BLUE_SIZE,8,
		GLX_ALPHA_SIZE,8,
		GLX_DOUBLEBUFFER,True,
		GLX_DEPTH_SIZE,24,
		None
	};

	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("Unable to open X Display\n");
		exit(1);
	}	

	defaultScreen=XDefaultScreen(gpDisplay);

	gpXVisualInfo=glXChooseVisual(gpDisplay,defaultScreen,frameBufferAttributes);

	winAttribs.border_pixel=0;
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),gpXVisualInfo->visual,AllocNone);
	gColormap=winAttribs.colormap;
	winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen);
	winAttribs.event_mask=ExposureMask|VisibilityChangeMask|ButtonPressMask|KeyPressMask|PointerMotionMask|StructureNotifyMask;

	styleMask=CWBorderPixel|CWBackPixel|CWEventMask|CWColormap;

	gWindow=XCreateWindow(
	gpDisplay,
	RootWindow(gpDisplay,gpXVisualInfo->screen),
	0,
	0,
	giWindowWidth,
	giWindowHeight,
	0,
	gpXVisualInfo->depth,
	InputOutput,
	gpXVisualInfo->visual,
	styleMask,
	&winAttribs
	);

	if(!gWindow)
	{
		printf("Failed to create Main Window\n");
		uninitialize();
		exit(1);
	}

	XStoreName(gpDisplay,gWindow,"First OpenGL Window");

	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);	
	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

	XMapWindow(gpDisplay,gWindow);

	doFileIO("Outside CreateWindow()\n");
}

void uninitialize(void)
{
	GLXContext currentGLXContext;

	doFileIO("Inside uninitialize()\n");

	currentGLXContext=glXGetCurrentContext();

	if(currentGLXContext!=NULL && currentGLXContext==gGLXContext)
	{
		glXMakeCurrent(gpDisplay,0,0);
	}

	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
	}

	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo==NULL;
	}

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}

	if(gpFile)
	{
		fclose(gpFile);
		gpFile=NULL;
	}

	doFileIO("Outside uninitialize()\n");
}


void initialize(void)
{
	void LoadGLTexture(void);
	void resize(int,int);

	char path[100];

	doFileIO("Inside initialize()\n");

	gGLXContext=glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);
	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);
	glClearColor(0.0f,0.0f,0.0f,0.0f);

	//Added for Double buffer
	glShadeModel(GL_FLAT);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LINE_SMOOTH);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);

	glEnable(GL_TEXTURE_2D);
	LoadGLTexture();
	resize(giWindowWidth,giWindowHeight);
	doFileIO("Outside initialize()\n");
}

void resize(int width,int height)
{
	doFileIO("Inside resize()\n");	

	if(height==0)
	{
		height=1;
	}
	glViewport(0,0,(GLsizei)width,(GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(40.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

	doFileIO("Outside resize()\n");
}

void ToggleFullScreen(void)
{
	Atom wm_state;
	Atom fullscreen;

	XEvent xev={0};

	doFileIO("Inside ToggleFullScreen()\n");

	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);
	memset(&xev,0,sizeof(xev));

	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=bFullScreen?0:1;

	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
	xev.xclient.data.l[1]=fullscreen;

	XSendEvent(
	gpDisplay,
	RootWindow(gpDisplay,gpXVisualInfo->screen),
	False,
	StructureNotifyMask,
	&xev
	);
	
	doFileIO("Outside ToggleFullScreen()\n");
}

void display(void)
{

	doFileIO("Inside display()\n");


	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f,0.0f,-3.6f);

	glBegin(GL_QUADS);
	
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-2.0f,-1.0f,0.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-2.0f, 1.0f, 0.0f);
	
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.0f, -1.0f, 0.0f);
	
	//-------------------------------

	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, 0.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, 0.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(2.41421f, 1.0f, -1.41421f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(2.41421f, -1.0f, -1.41421f);

	glEnd();

	glXSwapBuffers(gpDisplay,gWindow);

	doFileIO("Outside display()\n");
}

