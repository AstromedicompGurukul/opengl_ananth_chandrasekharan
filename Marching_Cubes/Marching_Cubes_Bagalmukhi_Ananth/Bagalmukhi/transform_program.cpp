#include "transform_program.hpp"

#include <assert.h>
#include <vector>

#include <glm/glm.hpp>

#include "GlErrorCheck.hpp"

using namespace glm;
using namespace std;

TransformProgram::TransformProgram(const GLchar** varyings, int count)
: varyings(varyings), count(count)
{
       //_CRT_SECURE_NO_WARNINGS("TransformProgram::TransformProgram");
    printf("In TransformProgram::TransformProgram()\n");
     printf("Out TransformProgram::TransformProgram()\n");
}

void TransformProgram::link()
{
      //_CRT_SECURE_NO_WARNINGS("TransformProgram::link");
     printf("In TransformProgram::link()\n");
    attachShaders();

    glTransformFeedbackVaryings(programObject, count, varyings, GL_INTERLEAVED_ATTRIBS);

    glLinkProgram(programObject);
    checkLinkStatus();

    CHECK_GL_ERRORS;
     printf("Out TransformProgram::link()\n");
}
