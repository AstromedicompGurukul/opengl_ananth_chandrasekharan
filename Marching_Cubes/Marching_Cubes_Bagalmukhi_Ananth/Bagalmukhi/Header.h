#pragma once
#include "CommonHeader.h"
#include "block_manager.hpp"
#include "density_slicer.hpp"
#include "lod_visualizer.hpp"
#include "DeltaTimer.h"

DensitySlicer density_slicer;
BlockManager block_manager;
LodVisualizer lod;
Swarm swarm;

// Matrices controlling the camera and projection.
glm::vec3 eye_position;
glm::vec3 eye_direction;
glm::vec3 eye_up;
glm::mat4 W;
glm::mat4 proj;
glm::mat4 view;

// First person stuff
std::unordered_set<int> pressed_keys;

// Display options
bool wireframe;
bool first_person_mode;
bool show_lod;
bool show_slicer;
bool show_terrain;
bool generate_blocks;

float rotation;
float rotation_vertical;
float distance_factor;
float camera_speed;
float far_plane;

// If the main mouse is pressed.
bool mouse_down;
bool mouse_down_with_control;
double previous_mouse_x;
double previous_mouse_y;

// Misc
int selected_texture_top;
int selected_texture_front;
int selected_texture_side;

///Tejaswini
int m_windowWidth;
int m_windowHeight;
int m_framebufferWidth;
int m_framebufferHeight;
bool m_paused;
bool m_fullScreen;
///Tejaswini

GLuint resize_window_width = 800;
GLuint resize_window_height = 600;

Bagalmukhi_Timer bgt;