#pragma once

#include "CommonHeader.h"
#include "indexed_geometry.hpp"
#include "ShaderProgram.hpp"
#include "cm/trace.hpp"

class Cube : public IndexedGeometry
{
public:
    Cube(float size);

    // Initialize a cube with corners (0, 0, 0) and (size, size, size)
    void init(ShaderProgram& shaderProgram);

private:
    float size;
};
