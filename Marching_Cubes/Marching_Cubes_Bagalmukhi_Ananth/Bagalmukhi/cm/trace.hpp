#ifndef CM_TRACE_20060209_HPP
#define CM_TRACE_20060209_HPP
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdarg.h>

#define CM_TRACE_FILE(trace_file)	cm::Trace::LogToFile(trace_file)

#define CM_TRACE_FUNC(func_name)    cm::Trace __CM_TRACE__(func_name, "()")
#define CM_TRACE_FUNC_ARG1(func_name, argfmt, arg)   \
            cm::Trace __CM_TRACE__(func_name, argfmt, arg)
#define CM_TRACE_FUNC_ARG2(func_name, argfmt, arg1, arg2)   \
            cm::Trace __CM_TRACE__(func_name, argfmt, arg1, arg2)
#define CM_TRACE_FUNC_ARG3(func_name, argfmt, arg1, arg2, arg3)   \
            cm::Trace __CM_TRACE__(func_name, argfmt, arg1, arg2, arg3)
#define CM_TRACE_FUNC_ARG4(func_name, argfmt, arg1, arg2, arg4)   \
            cm::Trace __CM_TRACE__(func_name, argfmt, arg1, arg2, arg3, arg4)
#define CM_TRACE_FUNC_ARG5(func_name, argfmt, arg1, arg2, arg4, arg5)   \
            cm::Trace __CM_TRACE__(func_name, argfmt, arg1, arg2, arg3, arg4, arg5)
#define CM_TRACE_FUNC_ARG6(func_name, argfmt, arg1, arg2, arg4, arg5, arg6)   \
            cm::Trace __CM_TRACE__(func_name, argfmt, arg1, arg2, arg3, arg4, arg5, arg6)
#define CM_TRACE_FUNC_ARG7(func_name, argfmt, arg1, arg2, arg4, arg5, arg6, arg7)   \
            cm::Trace __CM_TRACE__(func_name, argfmt, arg1, arg2, arg3, arg4, arg5, arg6, arg7)
#define CM_TRACE_FUNC_ARG8(func_name, argfmt, arg1, arg2, arg4, arg5, arg6, arg7, arg8)   \
            cm::Trace __CM_TRACE__(func_name, argfmt, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8)
#define CM_TRACE_FUNC_ARG9(func_name, argfmt, arg1, arg2, arg4, arg5, arg6, arg7, arg8, arg9)   \
            cm::Trace __CM_TRACE__(func_name, argfmt, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9)
#define CM_TRACE_FUNC_ARG10(func_name, argfmt, arg1, arg2, arg4, arg5, arg6, arg7, arg8, arg9, arg10)   \
            cm::Trace __CM_TRACE__(func_name, argfmt, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10)


namespace	cm
{
    class	Trace
    {
    public:
    	explicit Trace(char *func_name, const char* argsfmt, ...)
    	{
            char fmt[256] ={0};
            sprintf(fmt, "%s%s", func_name, argsfmt);
    	    va_list arglist;
    	    va_start(arglist, argsfmt);
    		LogMsg(depth_, depth_ * 2, fmt,  arglist);
    		va_end(arglist);
    		++depth_;
    	}

    	~Trace()
    	{
    		--depth_;
    	}

    	/// special the global log file.
    	void static LogToFile(const char *trace_file)
    	{
    		trace_file_ = trace_file;
    	}

    private:
    	void LogMsg(int depth, int align, const char *fmt, va_list args)
    	{
    		FILE	*fp = fopen(trace_file_.c_str(), "a+");
    		if (fp == NULL)
    		{
    			return;
    		}


    		time_t		curTime;
    		time(&curTime);

    		char	timeStamp[32] = { 0 };
    		strftime(timeStamp, sizeof(timeStamp), "%Y%m%d.%H%M%S", localtime(&curTime));

    		// only log the timestamp when the time changes
    		unsigned int len = fprintf( fp, "%s %*.*s> (%d)",
    				(last_invoke_time_ != curTime) ? timeStamp : "               ",
    				2 * depth,
    				2 * depth,
    				nest_,
    				depth);
    		last_invoke_time_ = curTime;
    		len += vfprintf(fp, fmt, args);
    		len += fwrite("\n", 1, 1, fp);
    		fflush(fp);
    		fclose(fp);
    	}

    private:
    	// the debug trace filename
    	static std::string	trace_file_;

    	// function call stack depth
    	static int			depth_;
    	static const char*  nest_;
    	static time_t       last_invoke_time_;
    };
}	// end namespace cm
#endif // CM_TRACE_20060209_HPP
