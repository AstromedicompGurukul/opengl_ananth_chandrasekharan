#include "indexed_block.hpp"

#include "GlErrorCheck.hpp"

#include "timer.hpp"

using namespace glm;
using namespace std;

IndexedBlock::IndexedBlock(ivec3 index, int size, bool alpha_blend)
: Block(index, size, alpha_blend)
{
    printf("In IndexedBlock()\n");

    // TODO: reevaluate amount of space needed, maybe dynamically
    vertex_unit_size = sizeof(vec3) * 2 + sizeof(float);
    vertex_data_size = BLOCK_SIZE * BLOCK_SIZE *
                       BLOCK_SIZE * vertex_unit_size * 3;

    printf("Out IndexedBlock()\n");

}

IndexedBlock::~IndexedBlock()
{
    // TODO
     printf("In ~IndexedBlock()\n");

      printf("In ~IndexedBlock()\n");
}

void IndexedBlock::init(GLint pos_attrib, GLint normal_attrib, GLint ambient_occlusion_attrib)
{
     printf("In IndexedBlock::init()\n");

    Block::init(pos_attrib, normal_attrib, ambient_occlusion_attrib);

    // TODO: reevaluate amount of space needed, maybe dynamically
    size_t index_unit_size = sizeof(ivec3);
    size_t index_data_size = BLOCK_SIZE * BLOCK_SIZE *
                             BLOCK_SIZE * index_unit_size * 15;

    glGenBuffers(1, &index_buffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer);
    {
        // TODO: Investigate performance of different usage flags.
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, index_data_size, nullptr, GL_STATIC_COPY);
    }
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenTransformFeedbacks(1, &index_feedback);
    {
        glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, index_feedback);

        glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, index_buffer);

        glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, 0);
    }

    CHECK_GL_ERRORS;
     printf("Out IndexedBlock::init()\n");
}

void IndexedBlock::draw()
{
     printf("In IndexedBlock::draw()\n");

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer);

    // Should double check the sign/unsigned thing...
    glDrawElements(GL_TRIANGLES, index_count, GL_UNSIGNED_INT, 0);

      printf("Out IndexedBlock::draw()\n");
}
