#pragma once

#include <glm/glm.hpp>

#include "CommonHeader.h"
#include "ShaderProgram.hpp"

#include "geometry.hpp"
#include "cm/trace.hpp"



class Plane : public Geometry
{
public:
    Plane(float dim);

    // Initialize a grid of size dim x dim on the x-z plane.
    void init(ShaderProgram& shaderProgram, glm::mat4 transform = glm::mat4(1.0f));

private:
    float size;
};
