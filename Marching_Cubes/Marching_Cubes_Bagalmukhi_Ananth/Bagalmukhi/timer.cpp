#include "timer.hpp"

 void Timer::start()
 {	
	 start_time = time(NULL);
 }
 
 
 void Timer::stop()
 {
	 end_time = time(NULL);
 }
 
 double Timer::elapsedSeconds()
 {
	return (double) (end_time - start_time);
 }


