#pragma once

#include <glm/glm.hpp>


#include "CommonHeader.h"
#include "ShaderProgram.hpp"
#include "cm/trace.hpp"

class Geometry
{
public:
    GLuint getVertices() { return geometry_vao; }

protected:
    // Fields related to grid geometry.
    GLuint geometry_vao; // Vertex Array Object
    GLuint geometry_vbo; // Vertex Buffer Object

    void initFromVertices(ShaderProgram& shader_program, float* vertices, int vertex_count);
};
