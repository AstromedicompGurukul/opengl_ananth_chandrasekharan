#pragma once

/*
This code is implemented in linux. Need to port in Windows.
*/
#include "cm/trace.hpp"

#include <time.h>

class Timer {
public:
	void start();
	void stop();
	double elapsedSeconds();

private:
	time_t start_time;
	time_t end_time;
};

