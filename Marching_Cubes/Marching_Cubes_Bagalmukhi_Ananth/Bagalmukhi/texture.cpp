#include "texture.hpp"

#include <iostream>
#include <fstream>
#include <sstream>

#include <glm/glm.hpp>
#include "soil/soil.h"
#include "GlErrorCheck.hpp"

using namespace glm;
using namespace std;

Texture::Texture(string path, GLenum binding)
: path(path)
, width(0)
, height(0)
, texture(-1)
, binding(binding)
{
    //_CRT_SECURE_NO_WARNINGS("Texture::Texture");
    printf("In Texture()\n");

    ifstream file(path);
    if (!file) {
        stringstream errorMessage;
        errorMessage << "Texture file not found: " << path;
        throw errorMessage.str();
    }

    printf("Out Texture()\n");

}

void Texture::init()
{
     //_CRT_SECURE_NO_WARNINGS("Texture::init");
     printf("In Texture::init()\n");

    glGenTextures(1, &texture);
    glActiveTexture(binding);
    glBindTexture(GL_TEXTURE_2D, texture);
    {
        // Note: first pixel is loaded as top-left corner whereas OpenGL expects lower-left
        // corner. This is not handled here so the shader code will need to be aware.
        unsigned char* image = SOIL_load_image(path.c_str(), &width, &height, 0, SOIL_LOAD_RGB);
        assert(width > 0 && height > 0);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
        SOIL_free_image_data(image);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        // TODO: Compare GL_NEAREST_MIPMAP_LINEAR
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glGenerateMipmap(GL_TEXTURE_2D);
    }
    glBindTexture(GL_TEXTURE_2D, 0);

    CHECK_GL_ERRORS;

      printf("Out Texture::init()\n");
}

void Texture::reload(string new_path)
{
      //_CRT_SECURE_NO_WARNINGS("Texture::reload");
     printf("In Texture::reload()\n");
    path = new_path;

    init();
    ifstream file(path);
    if (!file) {
        stringstream errorMessage;
        errorMessage << "Texture file not found: " << path;
        throw errorMessage.str();
    }

     printf("Out Texture::reload()\n");
}

void Texture::rebind()
{
     printf("In Texture::rebind()\n");

    assert(width > 0 && height > 0);
    glActiveTexture(binding);
    glBindTexture(GL_TEXTURE_2D, texture);

     printf("Out Texture::rebind()\n");
}
