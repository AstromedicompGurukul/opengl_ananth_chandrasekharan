#include "CommonHeader.h"
#include "Header.h"
#include "cm/trace.hpp"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/rotate_vector.hpp>

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

HDC ghdc = NULL;
HWND ghwnd = NULL;
HGLRC ghrc = NULL;
DWORD dwStyle;

WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbFullscreen = false;
bool gbEscapeKeyIsPressed = false;
bool gbActiveWindow = false;

FILE* gpFile = NULL;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint gVao;
GLuint gVbo;
GLuint gMVPUniform;

glm::mat4 gPerspectiveProjectionMatrix;

GLint elapsedTime;

GLfloat water_height_parameter;
bool use_water_parameter;
bool use_stencil_parameter;

GLint raster_width;
GLint raster_height;

float factor = 0.02f;

/*
const char* items[] = {
		  "bagalmukhi_Snow",
		  "Ancient Flooring",
		  "Boards",
		  "CherryBark",
		  "Chimeny",
		  "CliffRock",
		  "CliffRock2",
		  "Dirt",
		  "Grass",
		  "Grass2",
		  "GrassDry",
		  "GrassPurpleFlowers",
		  "GrassSparse",
		  "Gravel",
		  "GroundCover",
		  "Hay",
		  "LeafyGround",
		  "Mud",
		  "OakBark",
		  "PackedDirt",
		  "PineBarkYoung",
		  "PineNeedles",
		  "Roof1",
		  "Siding1",
		  "Siding2",
		  "Stone1",
};
*/

const char* items[] = {
	   "GrassPurpleFlowers",
		 "CliffRock",
		  "Boards",
		  "CherryBark",
		  "Chimeny",
		  "CliffRock",
		  "CliffRock2",
		  "Dirt",
		  "Grass",
		  "Grass2",
		  "GrassDry",
		  "GrassPurpleFlowers",
		  "GrassSparse",
		  "Gravel",
		  "GroundCover",
		  "Hay",
		  "LeafyGround",
		  "Mud",
		  "OakBark",
		  "PackedDirt",
		  "PineBarkYoung",
		  "PineNeedles",
		  "Roof1",
		  "Siding1",
		  "Siding2",
		  "Stone1",
};


//--------------------------------------

class R_Window
{

private:
	static R_Window* instance;


public:
	HWND r_hwnd;
	WNDCLASSEX wndclass;
	FILE* fp;
	DWORD dwStyle;
	WINDOWPLACEMENT wpPrev;

	virtual ~R_Window()
	{
		fprintf(gpFile, "R_Window Destructor called\n");
	}

	R_Window()
	{
		this->fp = NULL;
		this->wpPrev = { sizeof(WINDOWPLACEMENT) };
	}

	static R_Window* GetInstance()
	{
		if (!instance)
		{
			instance = new R_Window;
		}
		return instance;
	}

	void set_pipeline()
	{	
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
		glFrontFace(GL_CCW);

		// Setup depth testing
		glEnable(GL_DEPTH_TEST);
		glDepthMask(GL_TRUE);
		glDepthFunc(GL_LEQUAL);
		glDepthRange(0.0f, 1.0f);
		glEnable(GL_DEPTH_CLAMP);

		glClearDepth(1.0f);
		glClearColor(0.3, 0.5, 0.7, 1.0);
	}

	HWND getHWND()
	{
		return r_hwnd;
	}

	void setHWND(HWND hwnd)
	{
		this->r_hwnd = hwnd;
		ghwnd = hwnd;
	}

	virtual void resize(int width, int height)
	{

	}

	virtual void display()
	{

	}

	virtual void update()
	{

	}

	virtual void makeView()
	{

	}

	virtual void resetView()
	{

	}

	virtual void Raster_CreateWindow(HINSTANCE hInstance, TCHAR* szAppName, int nCmdShow)
	{

	}

	virtual void uninitialize(void)
	{

	}

	virtual bool mouseMoveEvent(double xPos, double yPos)
	{
		return false;
	}

	void glew_initialize(void)
	{
		void resize(int, int);
		void uninitialize(void);

		PIXELFORMATDESCRIPTOR pfd;
		int iPixelFormatIndex;

		ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

		pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
		pfd.nVersion = 1;
		pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
		pfd.iPixelType = PFD_TYPE_RGBA;
		pfd.cColorBits = 32;
		pfd.cRedBits = 8;
		pfd.cGreenBits = 8;
		pfd.cBlueBits = 8;
		pfd.cAlphaBits = 8;
		pfd.cDepthBits = 32;

		ghdc = GetDC(ghwnd);

		iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
		if (iPixelFormatIndex == 0)
		{
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}

		if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
		{
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}

		ghrc = wglCreateContext(ghdc);
		if (ghrc == NULL)
		{
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}

		if (wglMakeCurrent(ghdc, ghrc) == FALSE)
		{
			wglDeleteContext(ghrc);
			ghrc = NULL;
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}

		GLenum glew_error = glewInit();
		if (glew_error != GLEW_OK)
		{
			wglDeleteContext(ghrc);
			ghrc = NULL;
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}

	}

	void ToggleFullscreen(void)
	{
		MONITORINFO mi;
		if (gbFullscreen == false)
		{
			dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
			if (dwStyle & WS_OVERLAPPEDWINDOW)
			{
				mi = { sizeof(MONITORINFO) };
				if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
				{
					SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
					SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
				}
			}
			ShowCursor(FALSE);
		}
		else
		{
			SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
			SetWindowPlacement(ghwnd, &wpPrev);
			SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
			ShowCursor(TRUE);
		}
	}
};

class Navigator: public R_Window
{
private:


public:
	Navigator()
	{
		rotation = 0.0f;
		rotation_vertical = 0.0f;
		distance_factor = 1.0f;
		camera_speed = 5.0f;
		far_plane = 20.0f;
		mouse_down = false;
		mouse_down_with_control = false;

		wireframe = false; 
		first_person_mode = false;
		show_lod = false;
		show_slicer = false;
		show_terrain = true;
		generate_blocks = false;

		selected_texture_top = -1;
		selected_texture_front = -1;
		selected_texture_side = -1;
	}
	
	void MainLogic(void)
	{
		glClearColor(0.5, 0.5, 0.5, 1.0);

		GLint result;
		glGetIntegerv(GL_MAX_3D_TEXTURE_SIZE, &result);
		printf("Max 3D texture size: %d\n", result);

		// Build the shaders
		Timer shader_init_timer;
		shader_init_timer.start();
		{
			string dir = "Assets//";
			block_manager.init(dir);
			density_slicer.init(dir);
			lod.init(dir);

			swarm.init(dir);
			swarm.initializeAttributes(*block_manager.terrain_generator);
		}
		shader_init_timer.stop();
		printf("Compiling shaders took %.2f seconds\n", shader_init_timer.elapsedSeconds());

		//
		//resetView(); //Ananth : Was initially uncommented

		// Set up initial view and projection matrices (need to do this here,
		// since it depends on the GLFW window being set up correctly).
		makeView(WIN_WIDTH,WIN_HEIGHT);
		
		resize(WIN_WIDTH,WIN_HEIGHT);
	}

	void initialize()
	{
		glew_initialize();

		MainLogic();
		
		set_pipeline();
		

		//gPerspectiveProjectionMatrix = glm::mat4(1.0f);
		proj = glm::mat4(1.0f);
		resize(WIN_WIDTH, WIN_HEIGHT);
	}

	void resize(int width, int height)
	{
		if (height == 0)
			height = 1;
		glViewport(0, 0, (GLsizei)width, (GLsizei)height);
		
		resetView();
		// Set up initial view and projection matrices (need to do this here,
		// since it depends on the GLFW window being set up correctly).
		makeView(width,height);
		raster_width = width;
		raster_height = height;
	}

	virtual ~Navigator()
	{
		fprintf(gpFile, "MainLogicClass Destructor called\n");
	}

	void  resetView()
	{
		float distance = 5.0 * M_SQRT1_2 * distance_factor;
		vec3 x_axis(1.0f, 0.0f, 0.0f);
		vec3 y_axis(0.0f, 1.0f, 0.0f);
		eye_position = rotate(rotate(vec3(0.0f, distance, distance), rotation_vertical, x_axis), rotation, y_axis);
		eye_direction = -normalize(eye_position);

		eye_up = vec3(0.0f, 1.0f, 0.0f);
		vec3 eye_right = cross(eye_direction, eye_up);
		eye_up = cross(eye_right, eye_direction);
	}

	void makeView(int width,int height)
	{
		proj = glm::perspective(glm::radians(45.0f),float(width) / float(height),0.01f, far_plane);
		view = lookAt(eye_position, eye_position + eye_direction, eye_up);
	}

	void display(void)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		bgt.start();

		if (!m_paused)
		{
			if (wireframe) {
				glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			}

			glEnable(GL_DEPTH_TEST);

			if (show_slicer) {
				density_slicer.draw(proj, view, W, 2.0f,
					block_manager.terrain_generator->period,
					block_manager.terrain_generator->octaves,
					block_manager.terrain_generator->octaves_decay,
					block_manager.terrain_generator->warp_frequency,
					block_manager.terrain_generator->warp_strength);
			}

			if (show_terrain) {
				block_manager.renderBlocks(proj, view, W, eye_position);
			}

			if (show_lod) {
				lod.draw(proj, view, W, eye_position);
			}

			// swarm.draw(proj, view, W, eye_position, *block_manager.terrain_generator);

			// Restore defaults
			glBindVertexArray(0);
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

			CHECK_GL_ERRORS;

			glUseProgram(0);

			bgt.stop();

			elapsedTime = bgt.getElapsedTimeInSec();

			SwapBuffers(ghdc);
		}
	}

	void uninitialize(void)
	{
		if (gbFullscreen == true)
		{
			dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
			SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
			SetWindowPlacement(ghwnd, &wpPrev);
			SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

			ShowCursor(TRUE);

		}



		if (gVao)
		{
			glDeleteVertexArrays(1, &gVao);
			gVao = 0;
		}

		if (gVbo)
		{
			glDeleteBuffers(1, &gVbo);
			gVbo = 0;
		}

		glDetachShader(gShaderProgramObject, gVertexShaderObject);

		glDetachShader(gShaderProgramObject, gFragmentShaderObject);

		glDeleteShader(gVertexShaderObject);
		gVertexShaderObject = 0;

		glDeleteShader(gFragmentShaderObject);
		gFragmentShaderObject = 0;

		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;

		glUseProgram(0);

		wglMakeCurrent(NULL, NULL);

		wglDeleteContext(ghrc);
		ghrc = NULL;

		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;

		if (gpFile)
		{
			fprintf(gpFile, "\nLog file is Successfully closed");
			fclose(gpFile);
			gpFile = NULL;
		}

		DestroyWindow(ghwnd);
		ghwnd = NULL;
	}

	void update(void)
	{

	}

	bool mouseMoveEvent(double xPos, double yPos)
	{
		bool eventHandled = false;
		if (mouse_down) {
			float dy = yPos - previous_mouse_y;
			float dx = xPos - previous_mouse_x;

			//TO DO : first preson value update

			if (first_person_mode) {
				vec3 eye_right = cross(eye_direction, eye_up);
				eye_direction = rotate(eye_direction, dy / 500.0f, eye_right);
				eye_up = rotate(eye_up, dy / 500.0f, eye_right);

				eye_direction = rotate(eye_direction, dx / 500.0f, vec3(0.0, 1.0, 0.0));
				eye_up = rotate(eye_up, dx / 500.0f, vec3(0.0, 1.0, 0.0));

				eye_direction = normalize(eye_direction);
				eye_up = normalize(eye_up);

				// TODO: Might want to prevent looking too far up or down.
				// http://gamedev.stackexchange.com/questions/19507/how-should-i-implement-a-first-person-camera

				makeView(raster_width,raster_height);
			}
			else {
				if (mouse_down_with_control) {
					rotation_vertical += -dy / 500.0f;
					rotation_vertical = std::max(std::min(rotation_vertical, PI / 4.0f), -PI / 4.0f);
					resetView();
					makeView(raster_width, raster_height);
				}
				else {
					rotation += -dx / 500.0f;
					resetView();
					makeView(raster_width, raster_height);
				}
			}
		}

		previous_mouse_x = xPos;
		previous_mouse_y = yPos;

		printf("Out Navigator::mouseMoveEvent()\n");

		return eventHandled;
	}


	void Raster_CreateWindow(HINSTANCE hInstance, TCHAR* szAppName, int nCmdShow)
	{
		void initialize();

		if (fopen_s(&this->fp, "Log.txt", "w") != 0)
		{
			MessageBox(NULL, TEXT("Log File Cannot be created"), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);

			exit(0);
		}
		else
		{
			gpFile = this->fp;
			fprintf(gpFile, "Log File Successfully Opened!\n");
		}

		this->wndclass.cbClsExtra = 0;
		this->wndclass.cbWndExtra = 0;
		this->wndclass.hbrBackground = CreateSolidBrush(RGB(0, 255, 0));
		this->wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
		this->wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
		this->wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
		this->wndclass.hInstance = hInstance;
		this->wndclass.lpfnWndProc = WndProc;
		this->wndclass.lpszClassName = szAppName;
		this->wndclass.lpszMenuName = NULL;
		this->wndclass.cbSize = sizeof(WNDCLASSEX);
		this->wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

		RegisterClassEx(&this->wndclass);
		this->r_hwnd = CreateWindowEx(WS_EX_APPWINDOW,
			szAppName,
			TEXT("Shree Ganesha"),
			WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
			CW_USEDEFAULT,
			CW_USEDEFAULT,
			WIN_WIDTH,
			WIN_HEIGHT,
			NULL,
			NULL,
			hInstance,
			NULL);
		ghwnd = this->r_hwnd;

		this->initialize();
		ShowWindow(ghwnd, nCmdShow);
		SetForegroundWindow(ghwnd);
		SetFocus(ghwnd);
	}

protected:


};

R_Window *R_Window::instance = NULL;
R_Window* window1 = NULL;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nCmdShow)
{
	MSG msg;
	TCHAR szAppName[] = TEXT("Bagalmukhi");
	bool bDone = false;

	window1 = new Navigator;
	window1->Raster_CreateWindow(hInstance, szAppName, nCmdShow);

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				window1->update();
				window1->display();
				if (gbEscapeKeyIsPressed == true)
				{
					bDone = true;
				}
			}
		}
	}
	window1->uninitialize();
	return((int)msg.wParam);
}


LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullscreen(void);
	void resize(int, int);
	void uninitialize(void);
	vec3 eye_right;
	float offset = -0.5f;
	
	float arr[64] = { 
		1.0f, 1.0f,1.0f,
		0.0f, 0.0f, 0.0f, 
		0.0f,  0.0f,0.0f, 
		0.0f,  0.0f, 0.0f,

		0.0f, 0.0f, 0.0f,
		1.0f, 1.0f,1.0f, 
		0.0f, 0.0f, 0.0f, 
		0.0f, 0.0f, 0.0f, 

		0.0f, 0.0f,0.0f,
		0.0f, 0.0f, 0.0f, 
		1.0f, 1.0f, 1.0f,
		0.0f, 0.0f,0.0f,

		-0.5f,-0.5f,-0.5f,
		-0.5f,-0.5f,-0.5f,
		-0.5f,-0.5f,-0.5f,
		 1.0f,1.0f,1.0f
	};

	if (first_person_mode)
	{
		eye_right = cross(eye_direction, eye_up);
		factor = 0.02f * camera_speed;
	}

	switch (iMsg)
	{
	case WM_CREATE:
		break;

	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case 0x1b:
			DestroyWindow(ghwnd);
			break;

		case 0x46: //F
			if (gbFullscreen == false)
			{
				window1->ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				window1->ToggleFullscreen();
				gbFullscreen = false;
			}
			break;

		case 0x41: //A
			if (!m_paused)
			{
				if (first_person_mode)
				{
					eye_position -= eye_right * factor; //LeftA
					window1->makeView();
				}
				// Create a global transformation for the model.
				W = glm::translate(mat4(1.0f), vec3(offset, offset, offset));
				block_manager.update(elapsedTime, proj, view, W, eye_position, generate_blocks);
			}
			break;

		case 0x44: //D
			if (!m_paused)
			{
				if (first_person_mode)
				{
					eye_position += eye_right * factor; //Right
					window1->makeView();
				}
				// Create a global transformation for the model.
				W = glm::translate(mat4(1.0f), vec3(offset, offset, offset));
				block_manager.update(elapsedTime, proj, view, W, eye_position, generate_blocks);
			}
			break;

		case 0x53: //S
			if (!m_paused)
			{
				if (first_person_mode)
				{
					eye_position -= eye_direction * factor; //Back
					window1->makeView();
				}
				// Create a global transformation for the model.
				W = glm::translate(mat4(1.0f), vec3(offset, offset, offset));
				block_manager.update(elapsedTime, proj, view, W, eye_position, generate_blocks);
			}
			break;

		case 0x57: //W
			if (!m_paused)
			{
				if (first_person_mode)
				{
					eye_position += eye_direction * factor; //Forward
					window1->makeView();
				}
				// Create a global transformation for the model.
				W = glm::translate(mat4(1.0f), vec3(offset, offset, offset));
				block_manager.update(elapsedTime, proj, view, W, eye_position, generate_blocks);
			}
			break;

		case 0x51: //Q
		if (!m_paused)
		{
			if (first_person_mode)
			{
				eye_position += eye_up * factor; //Up
				window1->makeView();
			}
			// Create a global transformation for the model.
			W = glm::translate(mat4(1.0f), vec3(offset, offset, offset));
			block_manager.update(elapsedTime, proj, view, W, eye_position, generate_blocks);
		}
			break;

		case 0x45: //E
			if (!m_paused)
			{
				if (first_person_mode)
				{
					eye_position -= eye_up * factor; //Down
					window1->makeView();
				}
				// Create a global transformation for the model.
				W = glm::translate(mat4(1.0f), vec3(offset, offset, offset));
				block_manager.update(elapsedTime, proj, view, W, eye_position, generate_blocks);
			}
			break;

		case 0x54: //T : Use top texture
			block_manager.terrain_renderer.changeTopTexture(items[0]); //Currently keeping index as 0
			break;

		case 0x58: //X : Use side texture
			block_manager.terrain_renderer.changeSideTexture(items[0]); //Currently keeping index as 0
			break;

		case 0x59: //Y : Use front texture
			block_manager.terrain_renderer.changeFrontTexture(items[0]); //Currently keeping index as 0
			break;

		case 0x30: //0 : Use One block
			block_manager.block_display_type = OneBlock;
			block_manager.regenerateAllBlocks();
			break;

		case 0x31: //1 : Use One block
			block_manager.block_display_type = EightBlocks;
			block_manager.regenerateAllBlocks();
			window1->resetView();
			window1->makeView();
			first_person_mode = false;
			break;

		case 0x32: //2 : Use All block
			block_manager.block_display_type = All;
			block_manager.regenerateAllBlocks();
			window1->resetView();
			window1->makeView();
			first_person_mode = false;
			break;

		case VK_NUMPAD0: //Enable or Disable First Person Mode
			if(first_person_mode == true)
			{
				first_person_mode = false;
			}
			else
			{
				first_person_mode = true;
				window1->resetView();
				window1->makeView();
			}
			break;

		case VK_NUMPAD1: //Increase Camera Speed
			if (camera_speed <= 0.5f && camera_speed >=20.0f)
			{
				camera_speed = camera_speed + 0.01f;
			}
			else
			{
				camera_speed = 20.0f;
			}
			break;

		case VK_NUMPAD2: //Decrease Camera Speed
			if (camera_speed <= 0.5f && camera_speed >= 20.0f)
			{
				camera_speed = camera_speed - 0.01f;
			}
			else
			{
				camera_speed = 0.5f;
			}
			break;

		case VK_NUMPAD3: //Increase far plane
			if (far_plane <= 0.1f && far_plane <= 100.0f)
			{
				far_plane = far_plane + 0.01f;
			}
			else
			{
				far_plane = 100.0f;
			}
			window1->makeView();
			break;

		case VK_NUMPAD4: //Decrase far plane
			if (far_plane <= 0.1f && far_plane <= 100.0f)
			{
				far_plane = far_plane - 0.01f;
			}
			else
			{
				far_plane = 0.1f;
			}
			window1->makeView();
			break;

		case VK_NUMPAD5: //Enable or disable Ambient Occlusion
			if (block_manager.use_ambient == false)
			{
				block_manager.use_ambient = true;
			}
			else
			{
				block_manager.use_ambient = false;
			}

			break;

		case VK_NUMPAD6: //Enable or Disable Normal map
			if (block_manager.use_normal_map == true)
			{
				block_manager.use_normal_map = false;
			}
			else
			{
				block_manager.use_normal_map = true;
			}
			break;

		case VK_NUMPAD7: //Increment Light X
			if (block_manager.light_x >= 0.0f && block_manager.light_x<= 70.0f)
			{
				block_manager.light_x = block_manager.light_x + 0.01f;
			}
			else
			{
				block_manager.light_x = 70.0f;
			}
			break;

		case VK_NUMPAD8: //Decement Light X
			if (block_manager.light_x >= 0.0f && block_manager.light_x <= 70.0f)
			{
				block_manager.light_x = block_manager.light_x - 0.01f;
			}
			else
			{
				block_manager.light_x = 0.0f;
				block_manager.regenerateAllBlocks(false);
			}
			break;

		case VK_NUMPAD9: //Steps on shaort range ambient occlusion
			if (block_manager.terrain_generator->use_short_range_ambient_occlusion == true)
			{
				block_manager.terrain_generator->use_short_range_ambient_occlusion = false;
			}
			else
			{
				block_manager.terrain_generator->use_short_range_ambient_occlusion = true;
			}
			break;

		case VK_F1: //Steps for Long Range Ambient Occlusion
			if (block_manager.terrain_generator->use_long_range_ambient_occlusion == true)
			{
				block_manager.terrain_generator->use_long_range_ambient_occlusion = false;
			}
			else
			{
				block_manager.terrain_generator->use_long_range_ambient_occlusion = true;
				block_manager.regenerateAllBlocks(false);
			}
			break;

		case VK_F2: //Increase Ambient Occlusion Param 1
			if (block_manager.terrain_generator->ambient_occlusion_param.x<=0.0f && block_manager.terrain_generator->ambient_occlusion_param.x>=1.0f)
			{
				block_manager.terrain_generator->ambient_occlusion_param.x += 0.01f;
				
			}
			else
			{
				block_manager.terrain_generator->ambient_occlusion_param.x = 1.0f;
			}
			block_manager.regenerateAllBlocks(false);
			break;

		case VK_F3: //Decrease Ambient Occlusion Param 2
			if (block_manager.terrain_generator->ambient_occlusion_param.y <= 0.0f && block_manager.terrain_generator->ambient_occlusion_param.y >= 15.0f)
			{
				block_manager.terrain_generator->ambient_occlusion_param.y += 0.01f;

			}
			else
			{
				block_manager.terrain_generator->ambient_occlusion_param.y = 1.0f;
			}
			block_manager.regenerateAllBlocks(false);
			break;

		case VK_F4: //Decrease Ambient Occlusion Param 3
			if (block_manager.terrain_generator->ambient_occlusion_param.z <= 0.0f && block_manager.terrain_generator->ambient_occlusion_param.z >= 1.0f)
			{
				block_manager.terrain_generator->ambient_occlusion_param.z += 0.01f;

			}
			else
			{
				block_manager.terrain_generator->ambient_occlusion_param.z = 1.0f;
			}
			block_manager.regenerateAllBlocks(false);
			break; 

		default:
			break;
		}
		break;

	case WM_SIZE:
		window1->resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_CLOSE :
		PostQuitMessage(0);

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	case WM_MOUSEMOVE:
		switch (wParam)
		{
		case MK_CONTROL:
			mouse_down_with_control = true;

		case MK_LBUTTON:
		case MK_MBUTTON:
		case MK_RBUTTON:
			mouse_down = true;
			break;

		default:
			break;
		}
		window1->mouseMoveEvent(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_MBUTTONUP:
	case WM_LBUTTONUP:
	case WM_RBUTTONUP:
		mouse_down = false;
		break;
	case WM_KEYUP:
		switch (wParam)
		{
		case VK_LCONTROL:
		case VK_RCONTROL:
		case VK_LSHIFT:
		case VK_RSHIFT:
			mouse_down_with_control = false;
			break;
		default:
			break;
		}

		break;


	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}
