#include "plane.hpp"

#include <algorithm>

#include "GlErrorCheck.hpp"

using namespace glm;

Plane::Plane(float size)
: size(size)
{
    //_CRT_SECURE_NO_WARNINGS("Plane::Plane");
    printf("In Plane::Plane()\n");
     printf("Out Plane::Plane()\n");
}

void Plane::init(ShaderProgram& shaderProgram, mat4 transform)
{
     //_CRT_SECURE_NO_WARNINGS("Plane::init");
     printf("In Plane::init()\n");
    float half_size = size / 2;

    vec3 plane_vertices[] = {
        vec3(transform * vec4(half_size, 0, half_size, 1)),
        vec3(transform * vec4(half_size, 0, -half_size, 1)),
        vec3(transform * vec4(-half_size, 0, -half_size, 1)),

        vec3(transform * vec4(-half_size, 0, -half_size, 1)),
        vec3(transform * vec4(-half_size, 0, half_size, 1)),
        vec3(transform * vec4(half_size, 0, half_size, 1))
    };

    initFromVertices(shaderProgram, (float*)&plane_vertices, 3 * 3 * 2);
     printf("Out Plane::init()\n");
}
