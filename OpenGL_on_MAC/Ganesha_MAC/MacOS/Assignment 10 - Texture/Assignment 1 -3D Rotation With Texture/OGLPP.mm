#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "vmath.h"


enum {
	VDG_ATTRIBUTE_POSITION = 0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0
};


CVReturn MyDisplayLinkCallback(CVDisplayLinkRef ,const CVTimeStamp *,const CVTimeStamp *,CVOptionFlags,CVOptionFlags *,void *);


FILE *gpFile = NULL;


@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

int main(int argc, char const *argv[])
{
	NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];

	NSApp = [NSApplication sharedApplication];

	[NSApp setDelegate:[[AppDelegate alloc]init]];

	[NSApp run];

	[pPool release];

	return 0;
}

@implementation AppDelegate
{
@private 
	NSWindow *window;
	GLView *glView;
}

- (void)applicationDidFinishLaunching: (NSNotification *) aNotification
{
	NSBundle *mainBundle = [NSBundle mainBundle];
	NSString *appDirName = [mainBundle bundlePath];
	NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];
	NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
	const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
	gpFile = fopen(pszLogFileNameWithPath,"w");
	if(gpFile == NULL)
	{
		printf("Can not crate Log file\n Exiting...");
		[self release];
		[NSApp terminate:self];
	} 
	fprintf(gpFile, "Program is started successfully.\n");

	NSRect win_rect;
	win_rect = NSMakeRect(0.0,0.0,800.0,600.0);

	window = [[NSWindow alloc] initWithContentRect:win_rect styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable backing:NSBackingStoreBuffered defer:NO];

	[window setTitle:@"macOS 3D Rotation With Texture"];
	[window center];

	glView = [[GLView alloc]initWithFrame:win_rect];

	[window setContentView : glView];

	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];
}

- (void)applicationWillTerminate:(NSNotification *)notification
{
	fprintf(gpFile, "Program is terminated successfully\n");
	if(gpFile)
	{
		fclose(gpFile);
		gpFile = NULL;
	}
}

- (void)windowWillClose:(NSNotification *)notification
{
	[NSApp terminate:self];
}

- (void)dealloc
{
	[glView release];

	[window release];

	[super dealloc];
}
@end


@implementation GLView
{
	CVDisplayLinkRef displayLink;

	GLuint gVertexShaderObject;
	GLuint gFragmentShaderObject;
	GLuint gShaderProgramObject;

	
    GLuint gVao_pyramid;
    GLuint gVao_cube;

    GLuint gVbo_pyramid_position;
    GLuint gVbo_pyramid_texture;
    GLuint gVbo_cube_position;
    GLuint gVbo_cube_texture;

	GLuint gMVPUniform;
    GLuint gTexture_smapler_uniform;

    GLuint pyramid_texture;
    GLuint cube_texture;

	vmath::mat4 gPerspectiveProjectionMatrix;

    float angle_pyramid;
    float angle_cube;

}

- (id)initWithFrame: (NSRect)frame;
{
	self = [super initWithFrame:frame];

	if(self)
	{
		[[self window]setContentView:self];

		NSOpenGLPixelFormatAttribute attr[]=
		{
			NSOpenGLPFAOpenGLProfile,
			NSOpenGLProfileVersion4_1Core,
			NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
			NSOpenGLPFANoRecovery,
			NSOpenGLPFAAccelerated,
			NSOpenGLPFAColorSize,24,
			NSOpenGLPFADepthSize,24,
			NSOpenGLPFAAlphaSize,8,
			NSOpenGLPFADoubleBuffer,
			0
		};

		NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attr]autorelease];

		if(pixelFormat == nil)
		{
			fprintf(gpFile, "No valid opengl pixel format is available\nExitting...");
			[self release];
			[NSApp terminate:self];
		}

		NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
		[self setPixelFormat:pixelFormat];
		[self setOpenGLContext:glContext];
	}

	return(self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
	NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];

	[self drawView];
	[pPool release];
	return (kCVReturnSuccess);
}

-(void)prepareOpenGL
{
	fprintf(gpFile, "OpenGL Version :%s\n",glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL Version :%s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt=1;
	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

	 //vertex shader
    gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

    const GLchar *vertexShaderSourceCode =
    "#version 410 core" \
    "\n" \
    "in vec2 vTexture0_Coord;" \
    "in vec4 vPosition;" \
    "out vec2 out_texture0_coord;" \
    "uniform mat4 u_mvp_matrix;" \
    "void main(void)" \
    "{" \
    "gl_Position = u_mvp_matrix * vPosition;" \
    "out_texture0_coord = vTexture0_Coord;"\
    "}";

    glShaderSource(gVertexShaderObject,1,(const GLchar **)&vertexShaderSourceCode,NULL);
    glCompileShader(gVertexShaderObject);
    
    GLint iInfoLogLength = 0;
    GLint iShaderCompiledStatus = 0;
    char * szInfoLog = NULL;
    glGetShaderiv(gVertexShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);
    if(iShaderCompiledStatus ==GL_FALSE)
    {
        glGetShaderiv(gVertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
        if(iInfoLogLength > 0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gVertexShaderObject,iInfoLogLength,&written,szInfoLog);
                fprintf(gpFile, "Vertex Shader Compilation Log: %s\n",szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }    

    //fragment shader
    gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    const GLchar * gFragmentShaderSourceCode = 
    "#version 410 core"\
    "\n"\
    "in vec2 out_texture0_coord;"
    "out vec4 FragColor;"\
    "uniform sampler2D u_texture0_sampler;" \
    "void main(void)"\
    "{"\
    "FragColor = texture(u_texture0_sampler,out_texture0_coord);"\
    "}";

    glShaderSource(gFragmentShaderObject,1,(const GLchar **)&gFragmentShaderSourceCode,NULL);
    glCompileShader(gFragmentShaderObject);

    glGetShaderiv(gFragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);
    if(iShaderCompiledStatus ==GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
        if(iInfoLogLength > 0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gFragmentShaderObject,iInfoLogLength,&written,szInfoLog);
                fprintf(gpFile, "Fragment Shader Compilation Log: %s\n",szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }

    //shader program

    gShaderProgramObject = glCreateProgram();
    glAttachShader(gShaderProgramObject,gVertexShaderObject);
    glAttachShader(gShaderProgramObject,gFragmentShaderObject);

    //bind in variables before linking
    glBindAttribLocation(gShaderProgramObject,VDG_ATTRIBUTE_POSITION,"vPosition");
    glBindAttribLocation(gShaderProgramObject,VDG_ATTRIBUTE_TEXTURE0,"vTexture0_Coord");


    glLinkProgram(gShaderProgramObject);
    GLint iShaderProgramLinkStatus = 0;
    glGetProgramiv(gShaderProgramObject,GL_LINK_STATUS,&iShaderProgramLinkStatus);
    if(iShaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
        if(iInfoLogLength > 0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(gShaderProgramObject,iInfoLogLength,&written,szInfoLog);
                fprintf(gpFile, "Shader Program  Link Log: %s\n",szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }

    //get uniform location
    gMVPUniform = glGetUniformLocation(gShaderProgramObject,"u_mvp_matrix");
    gTexture_smapler_uniform = glGetUniformLocation(gShaderProgramObject,"u_texture0_sampler");

    //pyramid
    const GLfloat pyramidVertices[] = 
    {
        0.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, 1.0f,
        0.0f, 1.0f, 0.0f,
        1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, -1.0f,
        0.0f, 1.0f, 0.0f,
        1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
        0.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, 1.0f
    };

    glGenVertexArrays(1, &gVao_pyramid);
    glBindVertexArray(gVao_pyramid);
    //for vertices
    glGenBuffers(1, &gVbo_pyramid_position);
    glBindBuffer(GL_ARRAY_BUFFER, gVbo_pyramid_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);

    glVertexAttribPointer(VDG_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    glEnableVertexAttribArray(VDG_ATTRIBUTE_POSITION);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    //for texture coordinates
    const GLfloat pyramidTexCoords[] =
    {
        0.5f,1.0f,
        0.0f,0.0f,
        1.0f,0.0f,
        0.5f,1.0f,
        1.0f,0.0f,
        0.0f,0.0f, 
        0.5f,1.0f,
        1.0f,0.0f,
        0.0f,0.0f,
        0.5f,1.0f,
        0.0f,0.0f,
        1.0f,0.0f
    };

    glGenBuffers(1,&gVbo_pyramid_texture);
    glBindBuffer(GL_ARRAY_BUFFER,gVbo_pyramid_texture);
    glBufferData(GL_ARRAY_BUFFER,sizeof(pyramidTexCoords),pyramidTexCoords,GL_STATIC_DRAW);

    glVertexAttribPointer(VDG_ATTRIBUTE_TEXTURE0,2,GL_FLOAT,GL_FALSE,0,NULL);

    glEnableVertexAttribArray(VDG_ATTRIBUTE_TEXTURE0);

    glBindBuffer(GL_ARRAY_BUFFER,0);
    glBindVertexArray(0);

    //cube
    const GLfloat cubeVertices[] = 
    {
        1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f, 1.0f,
        -1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, 1.0f,
        1.0f, 1.0f, -1.0f,
        1.0f, 1.0f, 1.0f,
        1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, -1.0f,
        -1.0f, 1.0f, -1.0f,
        1.0f, 1.0f, -1.0f,
        1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
        -1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, 1.0f,
        -1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, -1.0f,
        -1.0f, 1.0f, -1.0f,
        1.0f, -1.0f, 1.0f,
        -1.0f, -1.0f, 1.0f,
        -1.0f, -1.0f, -1.0f,
        1.0f, -1.0f, -1.0f  
    };

    glGenVertexArrays(1, &gVao_cube);
    glBindVertexArray(gVao_cube);

    glGenBuffers(1, &gVbo_cube_position);
    glBindBuffer(GL_ARRAY_BUFFER, gVbo_cube_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);

    glVertexAttribPointer(VDG_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    glEnableVertexAttribArray(VDG_ATTRIBUTE_POSITION);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    const GLfloat cubeTexCoords[] = 
    
    //cube tex coords
    {
        0.0f,0.0f,
        1.0f,0.0f,
        1.0f,1.0f,
        0.0f,1.0f,
        1.0f,0.0f,
        1.0f,1.0f,
        0.0f,1.0f,
        0.0f,0.0f,
        1.0f,0.0f,
        1.0f,1.0f,
        0.0f,1.0f,
        0.0f,0.0f,
        0.0f,0.0f,
        1.0f,0.0f,
        1.0f,1.0f,
        0.0f,1.0f,
        0.0f,1.0f,
        0.0f,0.0f,
        1.0f,0.0f,
        1.0f,1.0f,
        1.0f,1.0f,
        0.0f,1.0f,
        0.0f,0.0f,
        1.0f,0.0f    
    };
    glGenBuffers(1, &gVbo_cube_texture);
    glBindBuffer(GL_ARRAY_BUFFER, gVbo_cube_texture);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cubeTexCoords), cubeTexCoords, GL_STATIC_DRAW);

    glVertexAttribPointer(VDG_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);

    glEnableVertexAttribArray(VDG_ATTRIBUTE_TEXTURE0);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);


    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glEnable(GL_CULL_FACE);

    pyramid_texture = [self loadTextureFromBMPFile:"stone.bmp"];
    cube_texture = [self loadTextureFromBMPFile:"kundali.bmp"];


	glClearColor(0.0f,0.0f,0.0f,0.0f); 

	gPerspectiveProjectionMatrix = vmath::mat4::identity();

    angle_cube = 0.0f;
    angle_pyramid = 0.0f;

	CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
	CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);
	CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
	CGLPixelFormatObj cglPixelFormat= (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);

	CVDisplayLinkStart(displayLink);
}

-(GLuint)loadTextureFromBMPFile:(const char *)texFileName
{
    NSBundle *mainBundle=[NSBundle mainBundle];
    NSString *appDirName =[mainBundle bundlePath];
    NSString *parentDirPath =[appDirName stringByDeletingLastPathComponent];
    NSString *texFileNameWithPath = [NSString stringWithFormat:@"%@/%s",parentDirPath,texFileName];

    NSImage *bmpFile = [[NSImage alloc]initWithContentsOfFile:texFileNameWithPath];
    if(!bmpFile)
    {
        NSLog(@"Can't find %@", texFileNameWithPath);
        return(0);
    }

    CGImageRef cgImage = [bmpFile CGImageForProposedRect:nil context:nil hints:nil];

    int w = (int)CGImageGetWidth(cgImage);
    int h = (int)CGImageGetHeight(cgImage);

    CFDataRef imageData =CGDataProviderCopyData(CGImageGetDataProvider(cgImage));
    void* 
    pixels = (void *)CFDataGetBytePtr(imageData);

    GLuint bmpTexture;
    glGenTextures(1,&bmpTexture);
    glPixelStorei(GL_UNPACK_ALIGNMENT,1);
    glBindTexture(GL_TEXTURE_2D,bmpTexture);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);
    glTexImage2D(GL_TEXTURE_2D,0,GL_RGB,w,h,0,GL_RGBA,GL_UNSIGNED_BYTE,pixels);
    glGenerateMipmap(GL_TEXTURE_2D);

    CFRelease(imageData);
    return(bmpTexture);
}


-(void)reshape
{
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
	NSRect rect = [self bounds];

	GLfloat width = rect.size.width;
	GLfloat height = rect.size.height;

	glViewport(0,0,(GLsizei)width,(GLsizei)height);

	gPerspectiveProjectionMatrix =vmath::perspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);

	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

- (void)drawRect: (NSRect)dirtyRect
{	
	[self drawView];
}

- (void)drawView
{
	[[self openGLContext]makeCurrentContext];
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);


	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    glUseProgram(gShaderProgramObject);

    vmath::mat4 modelViewMatrix = vmath::mat4::identity();
    vmath::mat4 modelViewProjectionMatrix = vmath::mat4::identity();
    vmath::mat4 rotationMatrix = vmath::mat4::identity();

    modelViewMatrix = vmath::translate(-1.5f,0.0f,-6.0f);
    rotationMatrix = vmath::rotate(angle_pyramid,0.0f,1.0f,0.0f);
    modelViewMatrix = modelViewMatrix * rotationMatrix;
    modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;

    glUniformMatrix4fv(gMVPUniform,1,GL_FALSE,modelViewProjectionMatrix);
    
    glBindTexture(GL_TEXTURE_2D,pyramid_texture);

    glBindVertexArray(gVao_pyramid);

    glDrawArrays(GL_TRIANGLES,0,12);

    glBindVertexArray(0);


    //cube block
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    vmath::mat4 scaleMatrix = vmath::mat4::identity();
    
    scaleMatrix = vmath::scale(0.75f, 0.75f, 0.75f);
    modelViewMatrix = vmath::translate(1.5f,0.0f,-6.0f) * scaleMatrix;
    rotationMatrix = vmath::rotate(angle_cube,angle_cube,angle_cube);
    modelViewMatrix = modelViewMatrix * rotationMatrix;
    modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;

    glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    
    glBindTexture(GL_TEXTURE_2D,cube_texture);

    glBindVertexArray(gVao_cube);

    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 20, 4);

    glBindVertexArray(0);

    glUseProgram(0);    

    [self spin];
	CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(void)spin
{
    angle_pyramid = angle_pyramid + 1.0f;
    if(angle_pyramid >=360.0f)
        angle_pyramid -= 360.0f;

    angle_cube = angle_cube + 1.0f;
    if(angle_cube >=360.0f)
        angle_cube -= 360.0f;
}

- (BOOL)acceptsFirstResponder
{
	[[self window]makeFirstResponder:self];
	return(YES);
}

- (void)keyDown: (NSEvent *)theEvent
{
	int key = (int) [[theEvent characters]characterAtIndex:0];
	switch(key)
	{
		case 27:
			[ self release];
			[NSApp terminate:self];
			break;
		case 'F':
		case 'f':
			[[self window]toggleFullScreen:self];
			break;
		default:
			break;
	}
}

-(void)mouseDown: (NSEvent *)theEvent
{
	[self setNeedsDisplay:YES];
}


-(void)mouseDragged: (NSEvent *)theEvent
{

}

-(void)rightMouseDown: (NSEvent *)theEvent
{
} 

-(void)dealloc
{
	 if(gVao_pyramid)
    {
        glDeleteVertexArrays(1,&gVao_pyramid);
        gVao_pyramid = 0;
    }

    if(gVao_cube)
    {
        glDeleteVertexArrays(1,&gVao_cube);
        gVao_cube = 0;
    }

    if(gVbo_pyramid_position)
    {
        glDeleteBuffers(1,&gVbo_pyramid_position);
        gVbo_pyramid_position = 0;
    }

    if(gVbo_pyramid_texture)
    {
        glDeleteBuffers(1,&gVbo_pyramid_texture);
        gVbo_pyramid_texture = 0;
    }

    if(gVbo_cube_position)
    {
        glDeleteBuffers(1,&gVbo_cube_position);
        gVbo_cube_position = 0;
    }

    if (gVbo_cube_texture)
    {
        glDeleteBuffers(1, &gVbo_cube_texture);
        gVbo_cube_texture = 0;
    }

    if(pyramid_texture)
    {
        glDeleteTextures(1,&pyramid_texture);
        pyramid_texture=0;
    }

    if(cube_texture)
    {
        glDeleteTextures(1,&cube_texture);
      
    }   
    glDetachShader(gShaderProgramObject,gVertexShaderObject);
    glDetachShader(gShaderProgramObject,gFragmentShaderObject);

    glDeleteShader(gVertexShaderObject);
    gVertexShaderObject = 0;

    glDeleteShader(gFragmentShaderObject);
    gFragmentShaderObject = 0;

    glDeleteProgram(gShaderProgramObject);
    gShaderProgramObject = 0;
    
    glUseProgram(0);

	CVDisplayLinkStop(displayLink);
	CVDisplayLinkRelease(displayLink);
	[super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink,const CVTimeStamp *pNow,const CVTimeStamp *pOutputTime,CVOptionFlags flagsIn,CVOptionFlags *fOagsOut,void *pDisplayLinkContext)
{
	CVReturn result = [(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
	return (result);
}