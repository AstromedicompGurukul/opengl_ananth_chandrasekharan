#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "vmath.h"


enum {
	VDG_ATTRIBUTE_POSITION = 0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0
};


CVReturn MyDisplayLinkCallback(CVDisplayLinkRef ,const CVTimeStamp *,const CVTimeStamp *,CVOptionFlags,CVOptionFlags *,void *);


FILE *gpFile = NULL;


@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

int main(int argc, char const *argv[])
{
	NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];

	NSApp = [NSApplication sharedApplication];

	[NSApp setDelegate:[[AppDelegate alloc]init]];

	[NSApp run];

	[pPool release];

	return 0;
}

@implementation AppDelegate
{
@private 
	NSWindow *window;
	GLView *glView;
}

- (void)applicationDidFinishLaunching: (NSNotification *) aNotification
{
	NSBundle *mainBundle = [NSBundle mainBundle];
	NSString *appDirName = [mainBundle bundlePath];
	NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];
	NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
	const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
	gpFile = fopen(pszLogFileNameWithPath,"w");
	if(gpFile == NULL)
	{
		printf("Can not crate Log file\n Exiting...");
		[self release];
		[NSApp terminate:self];
	} 
	fprintf(gpFile, "Program is started successfully.\n");

	NSRect win_rect;
	win_rect = NSMakeRect(0.0,0.0,800.0,600.0);

	window = [[NSWindow alloc] initWithContentRect:win_rect styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable backing:NSBackingStoreBuffered defer:NO];

	[window setTitle:@"macOS Sphere Light On Cube"];
	[window center];

	glView = [[GLView alloc]initWithFrame:win_rect];

	[window setContentView : glView];

	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];
}

- (void)applicationWillTerminate:(NSNotification *)notification
{
	fprintf(gpFile, "Program is terminated successfully\n");
	if(gpFile)
	{
		fclose(gpFile);
		gpFile = NULL;
	}
}

- (void)windowWillClose:(NSNotification *)notification
{
	[NSApp terminate:self];
}

- (void)dealloc
{
	[glView release];

	[window release];

	[super dealloc];
}
@end


@implementation GLView
{
	CVDisplayLinkRef displayLink;

	GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;

    vmath::mat4 gPerspectiveProjectionMatrix;

    GLuint gVao_cube;

    GLuint gVbo_cube_position;
    GLuint gVbo_cube_normal;

    GLuint gModelViewMatrixUniform,gProjectionMatrixUniform;
    GLuint gLdUniform,gKduniform,gLightPositionUniform;
    GLuint gLKeyPressedUniform;


    BOOL gbAnimate;
    BOOL gbLight;
    BOOL bIsLkeyPressed;

    float angle_cube;

}

- (id)initWithFrame: (NSRect)frame;
{
	self = [super initWithFrame:frame];

	if(self)
	{
		[[self window]setContentView:self];

		NSOpenGLPixelFormatAttribute attr[]=
		{
			NSOpenGLPFAOpenGLProfile,
			NSOpenGLProfileVersion4_1Core,
			NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
			NSOpenGLPFANoRecovery,
			NSOpenGLPFAAccelerated,
			NSOpenGLPFAColorSize,24,
			NSOpenGLPFADepthSize,24,
			NSOpenGLPFAAlphaSize,8,
			NSOpenGLPFADoubleBuffer,
			0
		};

		NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attr]autorelease];

		if(pixelFormat == nil)
		{
			fprintf(gpFile, "No valid opengl pixel format is available\nExitting...");
			[self release];
			[NSApp terminate:self];
		}

		NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
		[self setPixelFormat:pixelFormat];
		[self setOpenGLContext:glContext];
	}

	return(self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
	NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];

	[self drawView];
	[pPool release];
	return (kCVReturnSuccess);
}

-(void)prepareOpenGL
{
	fprintf(gpFile, "OpenGL Version :%s\n",glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL Version :%s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt=1;
	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

	 //vertex shader
    gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

    const GLchar *vertexShaderSourceCode =
        "#version 330 core" \
        "\n" \
        "in vec4 vPosition;" \
        "in vec3 vNormal;" \
        "uniform mat4 u_model_view_matrix;" \
        "uniform mat4 u_projection_matrix;" \
        "uniform int u_LKeyPressed;" \
        "uniform vec3 u_Ld;" \
        "uniform vec3 u_Kd;" \
        "uniform vec4 u_light_position;" \
        "out vec3 diffuse_light;" \
        "void main (void)" \
        "{" \
        "if(u_LKeyPressed == 1)" \
        "{" \
        "vec4 eyeCoordinated = u_model_view_matrix * vPosition;" \
        "vec3 tnorm = normalize(mat3(u_model_view_matrix)*vNormal);" \
        "vec3 s = normalize(vec3(u_light_position - eyeCoordinated));" \
        "diffuse_light = u_Ld * u_Kd * max(dot(s,tnorm),0.0);" \
        "}" \
        "gl_Position = u_projection_matrix * u_model_view_matrix *vPosition;" \
        "}";

    glShaderSource(gVertexShaderObject,1,(const GLchar **)&vertexShaderSourceCode,NULL);
    glCompileShader(gVertexShaderObject);
    
    GLint iInfoLogLength = 0;
    GLint iShaderCompiledStatus = 0;
    char * szInfoLog = NULL;
    glGetShaderiv(gVertexShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);
    if(iShaderCompiledStatus ==GL_FALSE)
    {
        glGetShaderiv(gVertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
        if(iInfoLogLength > 0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gVertexShaderObject,iInfoLogLength,&written,szInfoLog);
                fprintf(gpFile, "Vertex Shader Compilation Log: %s\n",szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }    

    //fragment shader
    gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    
    const GLchar * gFragmentShaderSourceCode =
        "#version 330 core" \
        "\n" \
        "in vec3 diffuse_light;"
        "out vec4 FragColor;" \
        "uniform int u_LKeyPressed;"\
        "void main (void)" \
        "{" \
        "vec4 color;" \
        "if(u_LKeyPressed == 1)" \
        "{" \
        "color = vec4(diffuse_light,1.0);" \
        "}" \
        "else" \
        "{" \
        "color = vec4(1.0,1.0,1.0,1.0);" \
        "}" \
        "FragColor = color;" \
        "}";

    glShaderSource(gFragmentShaderObject,1,(const GLchar **)&gFragmentShaderSourceCode,NULL);
    glCompileShader(gFragmentShaderObject);

    glGetShaderiv(gFragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);
    if(iShaderCompiledStatus ==GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
        if(iInfoLogLength > 0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gFragmentShaderObject,iInfoLogLength,&written,szInfoLog);
                fprintf(gpFile, "Fragment Shader Compilation Log: %s\n",szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }

    //shader program

    gShaderProgramObject = glCreateProgram();
    glAttachShader(gShaderProgramObject,gVertexShaderObject);
    glAttachShader(gShaderProgramObject,gFragmentShaderObject);

    //bind in variables before linking
    glBindAttribLocation(gShaderProgramObject,VDG_ATTRIBUTE_POSITION,"vPosition");
    glBindAttribLocation(gShaderProgramObject,VDG_ATTRIBUTE_NORMAL,"vNormal");


    glLinkProgram(gShaderProgramObject);
    GLint iShaderProgramLinkStatus = 0;
    glGetProgramiv(gShaderProgramObject,GL_LINK_STATUS,&iShaderProgramLinkStatus);
    if(iShaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
        if(iInfoLogLength > 0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(gShaderProgramObject,iInfoLogLength,&written,szInfoLog);
                fprintf(gpFile, "Shader Program  Link Log: %s\n",szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }

    //get uniform location
    gProjectionMatrixUniform = glGetUniformLocation(gShaderProgramObject,"u_projection_matrix");
    gModelViewMatrixUniform = glGetUniformLocation(gShaderProgramObject,"u_model_view_matrix");
    gLightPositionUniform = glGetUniformLocation(gShaderProgramObject,"u_light_position");
    gKduniform = glGetUniformLocation(gShaderProgramObject,"u_Kd");
    gLdUniform = glGetUniformLocation(gShaderProgramObject,"u_Ld");
    gLKeyPressedUniform = glGetUniformLocation(gShaderProgramObject,"u_LKeyPressed");
    

    //cube
    const GLfloat cubeVertices[] = 
    {
        1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f, 1.0f,
        -1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, 1.0f,
        1.0f, 1.0f, -1.0f,
        1.0f, 1.0f, 1.0f,
        1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, -1.0f,
        -1.0f, 1.0f, -1.0f,
        1.0f, 1.0f, -1.0f,
        1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
        -1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, 1.0f,
        -1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, -1.0f,
        -1.0f, 1.0f, -1.0f,
        1.0f, -1.0f, 1.0f,
        -1.0f, -1.0f, 1.0f,
        -1.0f, -1.0f, -1.0f,
        1.0f, -1.0f, -1.0f  
    };

    glGenVertexArrays(1, &gVao_cube);
    glBindVertexArray(gVao_cube);

    glGenBuffers(1, &gVbo_cube_position);
    glBindBuffer(GL_ARRAY_BUFFER, gVbo_cube_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);

    glVertexAttribPointer(VDG_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    glEnableVertexAttribArray(VDG_ATTRIBUTE_POSITION);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    const GLfloat cubeNormals[] = 
    
    //cube normals
    {
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        1.0f,0.0f,0.0f,
        1.0f,0.0f,0.0f,
        1.0f,0.0f,0.0f,
        1.0f,0.0f,0.0f,
        0.0f,0.0f,-1.0f,
        0.0f,0.0f,-1.0f,
        0.0f,0.0f,-1.0f,
        0.0f,0.0f,-1.0f,
        -1.0f,0.0f,0.0f,
        -1.0f,0.0f,0.0f,
        -1.0f,0.0f,0.0f,
        -1.0f,0.0f,0.0f,
        0.0f,1.0f,0.0f,
        0.0f,1.0f,0.0f,
        0.0f,1.0f,0.0f,
        0.0f,1.0f,0.0f,
        0.0f,-1.0f,0.0f,
        0.0f,-1.0f,0.0f,
        0.0f,-1.0f,0.0f,
        0.0f,-1.0f,0.0f    
    };
    glGenBuffers(1, &gVbo_cube_normal);
    glBindBuffer(GL_ARRAY_BUFFER, gVbo_cube_normal);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cubeNormals), cubeNormals, GL_STATIC_DRAW);

    glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);


    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);


	glClearColor(0.0f,0.0f,0.0f,0.0f); 

	gPerspectiveProjectionMatrix = vmath::mat4::identity();

    angle_cube = 0.0f;

    bIsLkeyPressed = NO;

	CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
	CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);
	CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
	CGLPixelFormatObj cglPixelFormat= (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);

	CVDisplayLinkStart(displayLink);
}

-(void)reshape
{
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
	NSRect rect = [self bounds];

	GLfloat width = rect.size.width;
	GLfloat height = rect.size.height;

	glViewport(0,0,(GLsizei)width,(GLsizei)height);

	gPerspectiveProjectionMatrix =vmath::perspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);

	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

- (void)drawRect: (NSRect)dirtyRect
{	
	[self drawView];
}

- (void)drawView
{
	[[self openGLContext]makeCurrentContext];
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);


	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glUseProgram(gShaderProgramObject);

    if(gbLight == YES)
    {
       glUniform1i(gLKeyPressedUniform,1);

        glUniform3f(gLdUniform,1.0f,1.0f,1.0f);
        glUniform3f(gKduniform ,0.5f,0.5f,0.5f);

        float lightPosition[] = {0.0f,0.0f,2.0f,1.0f};
        glUniform4fv(gLightPositionUniform,1,(GLfloat *)lightPosition);


    }
    else
    {
        glUniform1i(gLKeyPressedUniform,0);
    }

    vmath::mat4 modelMatrix = vmath::mat4::identity();
    vmath::mat4 modelViewMatrix = vmath::mat4::identity();
    vmath::mat4 rotationMatrix = vmath::mat4::identity();
    
    modelMatrix = vmath::translate(0.0f,0.0f,-5.0f);
    rotationMatrix = vmath::rotate(angle_cube,angle_cube,angle_cube);
    modelViewMatrix = modelMatrix * rotationMatrix;

    
    glUniformMatrix4fv(gModelViewMatrixUniform, 1, GL_FALSE, modelViewMatrix);
    glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

    glBindVertexArray(gVao_cube);

    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 20, 4);

    glBindVertexArray(0);

    glUseProgram(0);    

    [self spin];
	CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(void)spin
{
    angle_cube = angle_cube + 1.0f;
    if(angle_cube >=360.0f)
        angle_cube -= 360.0f;
}

- (BOOL)acceptsFirstResponder
{
	[[self window]makeFirstResponder:self];
	return(YES);
}

- (void)keyDown: (NSEvent *)theEvent
{
	int key = (int) [[theEvent characters]characterAtIndex:0];
	switch(key)
	{
		case 27:
			[ self release];
			[NSApp terminate:self];
			break;
		case 'F':
		case 'f':
			[[self window]toggleFullScreen:self];
			break;
        case 'l':
        case 'L':
             if(bIsLkeyPressed == NO)
            {
                gbLight = YES;
                bIsLkeyPressed = YES;
            }
            else
            {
                gbLight = NO;
                bIsLkeyPressed = NO;
            }
            break;
		default:
			break;
	}
}

-(void)mouseDown: (NSEvent *)theEvent
{
	[self setNeedsDisplay:YES];
}


-(void)mouseDragged: (NSEvent *)theEvent
{

}

-(void)rightMouseDown: (NSEvent *)theEvent
{
} 

-(void)dealloc
{
	
    if(gVao_cube)
    {
        glDeleteVertexArrays(1,&gVao_cube);
        gVao_cube = 0;
    }

    if(gVbo_cube_position)
    {
        glDeleteBuffers(1,&gVbo_cube_position);
        gVbo_cube_position = 0;
    }

    if (gVbo_cube_normal)
    {
        glDeleteBuffers(1, &gVbo_cube_normal);
        gVbo_cube_normal = 0;
    }

    glDetachShader(gShaderProgramObject,gVertexShaderObject);
    glDetachShader(gShaderProgramObject,gFragmentShaderObject);

    glDeleteShader(gVertexShaderObject);
    gVertexShaderObject = 0;

    glDeleteShader(gFragmentShaderObject);
    gFragmentShaderObject = 0;

    glDeleteProgram(gShaderProgramObject);
    gShaderProgramObject = 0;
    
    glUseProgram(0);
    

	CVDisplayLinkStop(displayLink);
	CVDisplayLinkRelease(displayLink);
	[super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink,const CVTimeStamp *pNow,const CVTimeStamp *pOutputTime,CVOptionFlags flagsIn,CVOptionFlags *fOagsOut,void *pDisplayLinkContext)
{
	CVReturn result = [(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
	return (result);
}
