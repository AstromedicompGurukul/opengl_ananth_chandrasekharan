#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "vmath.h"


enum {
	VDG_ATTRIBUTE_POSITION = 0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0
};


CVReturn MyDisplayLinkCallback(CVDisplayLinkRef ,const CVTimeStamp *,const CVTimeStamp *,CVOptionFlags,CVOptionFlags *,void *);


FILE *gpFile = NULL;


@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

int main(int argc, char const *argv[])
{
	NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];

	NSApp = [NSApplication sharedApplication];

	[NSApp setDelegate:[[AppDelegate alloc]init]];

	[NSApp run];

	[pPool release];

	return 0;
}

@implementation AppDelegate
{
@private 
	NSWindow *window;
	GLView *glView;
}

- (void)applicationDidFinishLaunching: (NSNotification *) aNotification
{
	NSBundle *mainBundle = [NSBundle mainBundle];
	NSString *appDirName = [mainBundle bundlePath];
	NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];
	NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
	const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
	gpFile = fopen(pszLogFileNameWithPath,"w");
	if(gpFile == NULL)
	{
		printf("Can not crate Log file\n Exiting...");
		[self release];
		[NSApp terminate:self];
	} 
	fprintf(gpFile, "Program is started successfully.\n");

	NSRect win_rect;
	win_rect = NSMakeRect(0.0,0.0,800.0,600.0);

	window = [[NSWindow alloc] initWithContentRect:win_rect styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable backing:NSBackingStoreBuffered defer:NO];

	[window setTitle:@"macOS Sphere Two Lights"];
	[window center];

	glView = [[GLView alloc]initWithFrame:win_rect];

	[window setContentView : glView];

	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];
}

- (void)applicationWillTerminate:(NSNotification *)notification
{
	fprintf(gpFile, "Program is terminated successfully\n");
	if(gpFile)
	{
		fclose(gpFile);
		gpFile = NULL;
	}
}

- (void)windowWillClose:(NSNotification *)notification
{
	[NSApp terminate:self];
}

- (void)dealloc
{
	[glView release];

	[window release];

	[super dealloc];
}
@end


@implementation GLView
{
	CVDisplayLinkRef displayLink;

	GLuint gVertexShaderObject;
	GLuint gFragmentShaderObject;
	GLuint gShaderProgramObject;

    GLuint gModelMatrixUniform,gViewMatrixUniform,gProjectionMatrixUniform;
    GLuint gLightPositionUniform;
    GLuint gLKeyPressedUniform;
    GLuint Toggle_uniform;

    GLuint La_Uniform;
    GLuint Ld_Uniform;
    GLuint Ls_Uniform;

    GLuint gLightPositionUniform1;
    GLuint La1_uniform;
    GLuint Ld1_Uniform;
    GLuint Ls1_Uniform;

    GLuint Ka_Uniform;
    GLuint Kd_Uniform;
    GLuint Ks_Uniform;
    GLuint Material_shininess_uniform;

    GLuint gVao_pyramid;

    GLuint gVbo_pyramid_position;
    GLuint gVbo_pyramid_normal;

    GLfloat lightAmbient1[4];
    GLfloat lightDiffuse1[4];
    GLfloat lightSpecular1[4];
    GLfloat lightPosition1[4];

    GLfloat lightAmbient2[4];
    GLfloat lightDiffuse2[4];
    GLfloat lightSpecular2[4];
    GLfloat lightPosition2[4];

    GLfloat material_Ambient[4];
    GLfloat material_Diffuse[4];
    GLfloat material_Specular[4];
    GLfloat material_shininess;

	vmath::mat4 gPerspectiveProjectionMatrix;

    BOOL gbAnimate;
    BOOL gbLight;
    BOOL bIsLkeyPressed;

    float anglePyramid;
}

- (id)initWithFrame: (NSRect)frame;
{
	self = [super initWithFrame:frame];

	if(self)
	{
		[[self window]setContentView:self];

		NSOpenGLPixelFormatAttribute attr[]=
		{
			NSOpenGLPFAOpenGLProfile,
			NSOpenGLProfileVersion4_1Core,
			NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
			NSOpenGLPFANoRecovery,
			NSOpenGLPFAAccelerated,
			NSOpenGLPFAColorSize,24,
			NSOpenGLPFADepthSize,24,
			NSOpenGLPFAAlphaSize,8,
			NSOpenGLPFADoubleBuffer,
			0
		};

		NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attr]autorelease];

		if(pixelFormat == nil)
		{
			fprintf(gpFile, "No valid opengl pixel format is available\nExitting...");
			[self release];
			[NSApp terminate:self];
		}

		NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
		[self setPixelFormat:pixelFormat];
		[self setOpenGLContext:glContext];
	}

	return(self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
	NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];

	[self drawView];
	[pPool release];
	return (kCVReturnSuccess);
}

-(void)prepareOpenGL
{
	fprintf(gpFile, "OpenGL Version :%s\n",glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL Version :%s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt=1;
	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

	 //vertex shader
    gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

    const GLchar *vertexShaderSourceCode =
        "#version 330 core" \
        "\n" \
        "in vec4 vPosition;" \
        "in vec3 vNormal;" \
        "uniform mat4 u_model_matrix;" \
        "uniform mat4 u_view_matrix;" \
        "uniform mat4 u_projection_matrix;" \
        "uniform int u_LKeyPressed;" \
        "uniform vec3 u_La;" \
        "uniform vec3 u_Ld;" \
        "uniform vec3 u_Ls;" \
        "uniform vec3 u_Ka;" \
        "uniform vec3 u_Kd;" \
        "uniform vec3 u_Ks;" \
        "uniform vec4 u_light_position;" \
        "uniform vec3 u_La1;" \
        "uniform vec3 u_Ld1;" \
        "uniform vec3 u_Ls1;" \
        "uniform vec4 u_light_position1;" \
        "uniform float u_material_shininess;" \
        "out vec3 phong_ads_color;" \
        "void main (void)" \
        "{" \
        "if(u_LKeyPressed == 1)" \
        "{" \
        "vec4 eyeCoordinates =u_view_matrix * u_model_matrix * vPosition;" \
        "vec3 transformed_normals = normalize(mat3(u_view_matrix * u_model_matrix)*vNormal);" \
        
        "vec3 light_direction1 = normalize(vec3(u_light_position) - eyeCoordinates.xyz);" \
        "float tn_dot_ld1 = max(dot(transformed_normals,light_direction1),0.0);" \
        "vec3 ambient1 = u_La * u_Ka;" \
        "vec3 diffuse1 = u_Ld * u_Kd * tn_dot_ld1;" \
        "vec3 reflection_vector1 = reflect(-light_direction1 , transformed_normals);" \
        "vec3 viewer_vector = normalize(-eyeCoordinates.xyz);" \
        "vec3 specular1 = u_Ls * u_Ks * pow(max(dot(reflection_vector1,viewer_vector),0.0),u_material_shininess);" \
        
        "vec3 light_direction2 = normalize(vec3(u_light_position1) - eyeCoordinates.xyz);" \
        "float tn_dot_ld2 = max(dot(transformed_normals,light_direction2),0.0);" \
        "vec3 ambient2 = u_La1 * u_Ka;" \
        "vec3 diffuse2 = u_Ld1 * u_Kd * tn_dot_ld2;" \
        "vec3 reflection_vector2 = reflect(-light_direction2 , transformed_normals);" \
        "vec3 specular2 = u_Ls1 * u_Ks * pow(max(dot(reflection_vector2,viewer_vector),0.0),u_material_shininess);" \
        "vec3 ambient = ambient1 + ambient2;" \
        "vec3 diffuse = diffuse1 + diffuse2;" \
        "vec3 specular = specular1 + specular2;" \
        "phong_ads_color = ambient + diffuse + specular; "\
        "}" \
        "else" \
        "{" \
        " phong_ads_color = vec3(1.0,1.0,1.0);"
        "}" \
        "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix *vPosition;" \
        "}";

    glShaderSource(gVertexShaderObject,1,(const GLchar **)&vertexShaderSourceCode,NULL);
    glCompileShader(gVertexShaderObject);
    
    GLint iInfoLogLength = 0;
    GLint iShaderCompiledStatus = 0;
    char * szInfoLog = NULL;
    glGetShaderiv(gVertexShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);
    if(iShaderCompiledStatus ==GL_FALSE)
    {
        glGetShaderiv(gVertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
        if(iInfoLogLength > 0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gVertexShaderObject,iInfoLogLength,&written,szInfoLog);
                fprintf(gpFile, "Vertex Shader Compilation Log: %s\n",szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }    

    //fragment shader
    gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    
    const GLchar * gFragmentShaderSourceCode =
        "#version 330 core" \
        "\n" \
        "in vec3 phong_ads_color;"
        "out vec4 FragColor;" \
        "void main (void)" \
        "{" \
        "FragColor = vec4(phong_ads_color,1.0);" \
        "}";

    glShaderSource(gFragmentShaderObject,1,(const GLchar **)&gFragmentShaderSourceCode,NULL);
    glCompileShader(gFragmentShaderObject);

    glGetShaderiv(gFragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);
    if(iShaderCompiledStatus ==GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
        if(iInfoLogLength > 0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gFragmentShaderObject,iInfoLogLength,&written,szInfoLog);
                fprintf(gpFile, "Fragment Shader Compilation Log: %s\n",szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }

    //shader program

    gShaderProgramObject = glCreateProgram();
    glAttachShader(gShaderProgramObject,gVertexShaderObject);
    glAttachShader(gShaderProgramObject,gFragmentShaderObject);

    //bind in variables before linking
    glBindAttribLocation(gShaderProgramObject,VDG_ATTRIBUTE_POSITION,"vPosition");
    glBindAttribLocation(gShaderProgramObject,VDG_ATTRIBUTE_NORMAL,"vNormal");


    glLinkProgram(gShaderProgramObject);
    GLint iShaderProgramLinkStatus = 0;
    glGetProgramiv(gShaderProgramObject,GL_LINK_STATUS,&iShaderProgramLinkStatus);
    if(iShaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
        if(iInfoLogLength > 0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(gShaderProgramObject,iInfoLogLength,&written,szInfoLog);
                fprintf(gpFile, "Shader Program  Link Log: %s\n",szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }

    //get uniform location
    gModelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
    gViewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
    gProjectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");
    
    Ka_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");
    Kd_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
    Ks_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");
    Material_shininess_uniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");

    gLightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");
    Ld_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld");
    La_Uniform = glGetUniformLocation(gShaderProgramObject, "u_La");
    Ls_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls");
    gLKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_LKeyPressed");
    
    gLightPositionUniform1 = glGetUniformLocation(gShaderProgramObject, "u_light_position1");
    Ld1_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld1");
    La1_uniform = glGetUniformLocation(gShaderProgramObject, "u_La1");
    Ls1_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls1");
    

  //pyramid
    const GLfloat pyramidVertices[] = 
    {
        0.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, 1.0f,
        0.0f, 1.0f, 0.0f,
        1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, -1.0f,
        0.0f, 1.0f, 0.0f,
        1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
        0.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, 1.0f
    };

    glGenVertexArrays(1, &gVao_pyramid);
    glBindVertexArray(gVao_pyramid);
    //for vertices
    glGenBuffers(1, &gVbo_pyramid_position);
    glBindBuffer(GL_ARRAY_BUFFER, gVbo_pyramid_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);

    glVertexAttribPointer(VDG_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    glEnableVertexAttribArray(VDG_ATTRIBUTE_POSITION);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    //for colors
    const GLfloat pyramidNormals[] =
    {
        0.0f,0.447214f,0.894427f,
        0.0f,0.447214f,0.894427f,
        0.0f,0.447214f,0.894427f,
        0.894427f,0.447214f,0.0f,
        0.894427f,0.447214f,0.0f,
        0.894427f,0.447214f,0.0f,
        0.0f,0.447214f,-0.894427f,
        0.0f,0.447214f,-0.894427f,
        0.0f,0.447214f,-0.894427f,
        -0.894427f,0.447214f,0.0f,
        -0.894427f,0.447214f,0.0f,
        -0.894427f,0.447214f,0.0f
    };

    glGenBuffers(1,&gVbo_pyramid_normal);
    glBindBuffer(GL_ARRAY_BUFFER,gVbo_pyramid_normal);
    glBufferData(GL_ARRAY_BUFFER,sizeof(pyramidNormals),pyramidNormals,GL_STATIC_DRAW);

    glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL,3,GL_FLOAT,GL_FALSE,0,NULL);

    glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);

    glBindBuffer(GL_ARRAY_BUFFER,0);
    glBindVertexArray(0);



    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);


	glClearColor(0.0f,0.0f,0.0f,0.0f); 

	gPerspectiveProjectionMatrix = vmath::mat4::identity();

    lightAmbient1[0] = 0.0f;
    lightAmbient1[1] = 0.0f;
    lightAmbient1[2] = 0.0f;
    lightAmbient1[3] = 1.0f;

    lightDiffuse1[0]= 1.0f;
    lightDiffuse1[1]= 0.0f;
    lightDiffuse1[2]= 0.0f;
    lightDiffuse1[3]= 0.0f;

    lightSpecular1[0] = 1.0f;
    lightSpecular1[1] = 0.0f;
    lightSpecular1[2] = 0.0f;
    lightSpecular1[3] = 0.0f;

    lightPosition1[0] = 200.0f;
    lightPosition1[1] = 100.0f;
    lightPosition1[2] = 100.0f;
    lightPosition1[3] = 1.0f;


    lightAmbient2[0] = 0.0f;
    lightAmbient2[1] = 0.0f;
    lightAmbient2[2] = 0.0f;
    lightAmbient2[3] = 1.0f;

    lightDiffuse2[0]= 0.0f;
    lightDiffuse2[1]= 0.0f;
    lightDiffuse2[2]= 1.0f;
    lightDiffuse2[3]= 0.0f;

    lightSpecular2[0] = 0.0f;
    lightSpecular2[1] = 0.0f;
    lightSpecular2[2] = 1.0f;
    lightSpecular2[3] = 0.0f;

    lightPosition2[0] = -200.0f;
    lightPosition2[1] = 100.0f;
    lightPosition2[2] = 100.0f;
    lightPosition2[3] = 1.0f;


    material_Ambient[0] = 0.0f;
    material_Ambient[1] = 0.0f;
    material_Ambient[2] = 0.0f;
    material_Ambient[3] = 1.0f;

    material_Diffuse[0]= 1.0f;
    material_Diffuse[1]= 1.0f;
    material_Diffuse[2]= 1.0f;
    material_Diffuse[3]= 1.0f;

    material_Specular[0] = 1.0f;
    material_Specular[1] = 1.0f;
    material_Specular[2] = 1.0f;
    material_Specular[3] = 1.0f;

    material_shininess = 50.0f;
    anglePyramid = 0.0f;

    bIsLkeyPressed = NO;

	CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
	CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);
	CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
	CGLPixelFormatObj cglPixelFormat= (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);

	CVDisplayLinkStart(displayLink);
}

-(void)reshape
{
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
	NSRect rect = [self bounds];

	GLfloat width = rect.size.width;
	GLfloat height = rect.size.height;

	glViewport(0,0,(GLsizei)width,(GLsizei)height);

	gPerspectiveProjectionMatrix =vmath::perspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);

	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

- (void)drawRect: (NSRect)dirtyRect
{	
	[self drawView];
}

- (void)drawView
{
	[[self openGLContext]makeCurrentContext];
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);


	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glUseProgram(gShaderProgramObject);

    if(gbLight == YES)
    {
        glUniform1i(gLKeyPressedUniform,1);
       
        //light properties
        glUniform3fv(La_Uniform,1,lightAmbient1);
        glUniform3fv(Ld_Uniform,1,lightDiffuse1);
        glUniform3fv(Ls_Uniform,1,lightSpecular1);
        glUniform4fv(gLightPositionUniform,1,lightPosition1);

        glUniform3fv(La1_uniform,1,lightAmbient2);
        glUniform3fv(Ld1_Uniform,1,lightDiffuse2);
        glUniform3fv(Ls1_Uniform,1,lightSpecular2);
        glUniform4fv(gLightPositionUniform1,1,lightPosition2);

        glUniform3fv(Ka_Uniform,1,material_Ambient);
        glUniform3fv(Kd_Uniform,1,material_Diffuse);
        glUniform3fv(Ks_Uniform,1,material_Specular);
        glUniform1f(Material_shininess_uniform,material_shininess);

    }
    else
    {
        glUniform1i(gLKeyPressedUniform,0);
    }

    vmath::mat4 modelMatrix = vmath::mat4::identity();
    vmath::mat4 viewMatrix = vmath::mat4::identity();

    vmath::mat4 rotationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::rotate(anglePyramid,0.0f,1.0f,0.0f);
    modelMatrix = vmath::translate(0.0f,0.0f,-5.0f);
    modelMatrix = modelMatrix * rotationMatrix;
    glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(gViewMatrixUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);


    glBindVertexArray(gVao_pyramid);

    glDrawArrays(GL_TRIANGLES,0,12);

    glBindVertexArray(0);

    glUseProgram(0);

    [self spin];
	CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(void)spin
{
   anglePyramid = anglePyramid + 0.9f;
    if (anglePyramid >= 360.0f)
        anglePyramid = 0.0f;
}

- (BOOL)acceptsFirstResponder
{
	[[self window]makeFirstResponder:self];
	return(YES);
}

- (void)keyDown: (NSEvent *)theEvent
{
	int key = (int) [[theEvent characters]characterAtIndex:0];
	switch(key)
	{
		case 27:
			[ self release];
			[NSApp terminate:self];
			break;
		case 'F':
		case 'f':
			[[self window]toggleFullScreen:self];
			break;
        case 'l':
        case 'L':
             if(bIsLkeyPressed == NO)
            {
                gbLight = YES;
                bIsLkeyPressed = YES;
            }
            else
            {
                gbLight = NO;
                bIsLkeyPressed = NO;
            }
            break;        
		default:
			break;
	}
}

-(void)mouseDown: (NSEvent *)theEvent
{
	[self setNeedsDisplay:YES];
}


-(void)mouseDragged: (NSEvent *)theEvent
{

}

-(void)rightMouseDown: (NSEvent *)theEvent
{
} 

-(void)dealloc
{
	if (gVao_pyramid)
    {
        glDeleteVertexArrays(1, &gVao_pyramid);
        gVao_pyramid = 0;
    }

    if (gVbo_pyramid_position)
    {
        glDeleteBuffers(1, &gVbo_pyramid_position);
        gVbo_pyramid_position = 0;
    }


    if (gVbo_pyramid_normal)
    {
        glDeleteBuffers(1, &gVbo_pyramid_normal);
        gVbo_pyramid_normal = 0;
    }


    glDetachShader(gShaderProgramObject,gVertexShaderObject);
    glDetachShader(gShaderProgramObject,gFragmentShaderObject);

    glDeleteShader(gVertexShaderObject);
    gVertexShaderObject = 0;

    glDeleteShader(gFragmentShaderObject);
    gFragmentShaderObject = 0;

    glDeleteProgram(gShaderProgramObject);
    gShaderProgramObject = 0;
    
    glUseProgram(0);

	CVDisplayLinkStop(displayLink);
	CVDisplayLinkRelease(displayLink);
	[super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink,const CVTimeStamp *pNow,const CVTimeStamp *pOutputTime,CVOptionFlags flagsIn,CVOptionFlags *fOagsOut,void *pDisplayLinkContext)
{
	CVReturn result = [(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
	return (result);
}