#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import <math.h>

#import "vmath.h"
#import "sphere.h"

enum {
	VDG_ATTRIBUTE_POSITION = 0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0
};


CVReturn MyDisplayLinkCallback(CVDisplayLinkRef ,const CVTimeStamp *,const CVTimeStamp *,CVOptionFlags,CVOptionFlags *,void *);


FILE *gpFile = NULL;


@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

int main(int argc, char const *argv[])
{
	NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];

	NSApp = [NSApplication sharedApplication];

	[NSApp setDelegate:[[AppDelegate alloc]init]];

	[NSApp run];

	[pPool release];

	return 0;
}

@implementation AppDelegate
{
@private 
	NSWindow *window;
	GLView *glView;
}

- (void)applicationDidFinishLaunching: (NSNotification *) aNotification
{
	NSBundle *mainBundle = [NSBundle mainBundle];
	NSString *appDirName = [mainBundle bundlePath];
	NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];
	NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
	const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
	gpFile = fopen(pszLogFileNameWithPath,"w");
	if(gpFile == NULL)
	{
		printf("Can not crate Log file\n Exiting...");
		[self release];
		[NSApp terminate:self];
	} 
	fprintf(gpFile, "Program is started successfully.\n");

	NSRect win_rect;
	win_rect = NSMakeRect(0.0,0.0,800.0,600.0);

	window = [[NSWindow alloc] initWithContentRect:win_rect styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable backing:NSBackingStoreBuffered defer:NO];

	[window setTitle:@"macOS Sphere Material"];
	[window center];

	glView = [[GLView alloc]initWithFrame:win_rect];

	[window setContentView : glView];

	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];
}

- (void)applicationWillTerminate:(NSNotification *)notification
{
	fprintf(gpFile, "Program is terminated successfully\n");
	if(gpFile)
	{
		fclose(gpFile);
		gpFile = NULL;
	}
}

- (void)windowWillClose:(NSNotification *)notification
{
	[NSApp terminate:self];
}

- (void)dealloc
{
	[glView release];

	[window release];

	[super dealloc];
}
@end


@implementation GLView
{
	CVDisplayLinkRef displayLink;

	GLuint gVertexShaderObject;
	GLuint gFragmentShaderObject;
	GLuint gShaderProgramObject;

    GLuint gModelMatrixUniform,gViewMatrixUniform,gProjectionMatrixUniform;
    GLuint gLightPositionUniform;
    GLuint gLKeyPressedUniform;

    GLuint La_uniform;
    GLuint Ld_Uniform;
    GLuint Ls_Uniform;

    GLuint Ka_Uniform;
    GLuint Kd_Uniform;
    GLuint Ks_Uniform;
    GLuint Material_shininess_uniform;

    float sphere_vertices[1146];
    float shpere_normals[1146];
    float sphere_textures[764];
    unsigned short shpere_elements[2280];

    int gNumVertices;
    int gNumElements;
    GLuint gVao_sphere;
    GLuint gVbo_sphere_position;
    GLuint gVbo_sphere_normal;
    GLuint gVbo_sphere_elements;

    GLfloat lightAmbient[4];
    GLfloat lightDiffuse[4];
    GLfloat lightSpecular[4];
    GLfloat lightPosition[4];

    GLfloat material_Ambient[24][4];
    GLfloat material_Diffuse[24][4];
    GLfloat material_Specular[24][4];
    GLfloat material_shininess[24];

	vmath::mat4 gPerspectiveProjectionMatrix;

    BOOL gbAnimate;
    BOOL gbLight;
    BOOL bIsLkeyPressed;

    int gWidth,gHeight;
    
    BOOL xKeyIsPressed;
    BOOL yKeyIsPressed;
    BOOL zKeyIsPressed;

    float angle_sphere;


}

- (id)initWithFrame: (NSRect)frame;
{
	self = [super initWithFrame:frame];

	if(self)
	{
		[[self window]setContentView:self];

		NSOpenGLPixelFormatAttribute attr[]=
		{
			NSOpenGLPFAOpenGLProfile,
			NSOpenGLProfileVersion4_1Core,
			NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
			NSOpenGLPFANoRecovery,
			NSOpenGLPFAAccelerated,
			NSOpenGLPFAColorSize,24,
			NSOpenGLPFADepthSize,24,
			NSOpenGLPFAAlphaSize,8,
			NSOpenGLPFADoubleBuffer,
			0
		};

		NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attr]autorelease];

		if(pixelFormat == nil)
		{
			fprintf(gpFile, "No valid opengl pixel format is available\nExitting...");
			[self release];
			[NSApp terminate:self];
		}

		NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
		[self setPixelFormat:pixelFormat];
		[self setOpenGLContext:glContext];
	}

	return(self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
	NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];

	[self drawView];
	[pPool release];
	return (kCVReturnSuccess);
}

-(void)prepareOpenGL
{
	fprintf(gpFile, "OpenGL Version :%s\n",glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL Version :%s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt=1;
	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

	 //vertex shader
    gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

    const GLchar *vertexShaderSourceCode =
        "#version 410 core" \
        "\n" \
        "in vec4 vPosition;" \
        "in vec3 vNormal;" \
        "uniform mat4 u_model_matrix;" \
        "uniform mat4 u_view_matrix;" \
        "uniform mat4 u_projection_matrix;" \
        "uniform int u_LKeyPressed;" \
        "uniform vec4 u_light_position;" \
        "out vec3 transformed_normals;" \
        "out vec3 light_direction;"\
        "out vec3 viewer_vector;"\
        "void main (void)" \
        "{" \
        "if(u_LKeyPressed == 1)" \
        "{" \
        "vec4 eyeCoordinates =u_view_matrix * u_model_matrix * vPosition;" \
        "transformed_normals = mat3(u_view_matrix * u_model_matrix)*vNormal;" \
        "light_direction = vec3(u_light_position) - eyeCoordinates.xyz;" \
        "viewer_vector = -eyeCoordinates.xyz;" \
        "}" \
        "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix *vPosition;" \
        "}";

    glShaderSource(gVertexShaderObject,1,(const GLchar **)&vertexShaderSourceCode,NULL);
    glCompileShader(gVertexShaderObject);
    
    GLint iInfoLogLength = 0;
    GLint iShaderCompiledStatus = 0;
    char * szInfoLog = NULL;
    glGetShaderiv(gVertexShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);
    if(iShaderCompiledStatus ==GL_FALSE)
    {
        glGetShaderiv(gVertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
        if(iInfoLogLength > 0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gVertexShaderObject,iInfoLogLength,&written,szInfoLog);
                fprintf(gpFile, "Vertex Shader Compilation Log: %s\n",szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }    

    //fragment shader
    gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    
    const GLchar * gFragmentShaderSourceCode =
        "#version 410 core" \
        "\n" \
        "in vec3 transformed_normals;"\
        "in vec3 light_direction;"\
        "in vec3 viewer_vector;"\
        "uniform vec3 u_La;" \
        "uniform vec3 u_Ld;" \
        "uniform vec3 u_Ls;" \
        "uniform vec3 u_Ka;" \
        "uniform vec3 u_Kd;" \
        "uniform vec3 u_Ks;" \
        "uniform float u_material_shininess;" \
        "uniform int u_LKeyPressed;"
        "out vec4 FragColor;" \
        "void main (void)" \
        "{" \
        "vec3 phong_ads_color;" \
        "if(u_LKeyPressed == 1)"\
        "{" \
        "vec3 normalized_transformed_normals = normalize(transformed_normals);"\
        "vec3 normalized_light_direction = normalize(light_direction);"\
        "vec3 normalized_viewer_vector = normalize(viewer_vector);"\
        "vec3 ambient = u_La * u_Ka;"\
        "float tn_dot_ld = max(dot(normalized_transformed_normals , normalized_light_direction),0.0);"\
        "vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;"\
        "vec3 reflection_vector = reflect(-normalized_light_direction,normalized_transformed_normals);"\
        "vec3 specular = u_Ls * u_Ks *pow(max(dot(reflection_vector,normalized_viewer_vector),0.0),u_material_shininess);"\
        "phong_ads_color = ambient + diffuse + specular;"\
        "}"\
        "else"\
        "{"\
        "phong_ads_color = vec3(1.0,1.0,1.0);"\
        "}"\
        "FragColor = vec4(phong_ads_color,1.0);" \
        "}";

    glShaderSource(gFragmentShaderObject,1,(const GLchar **)&gFragmentShaderSourceCode,NULL);
    glCompileShader(gFragmentShaderObject);

    glGetShaderiv(gFragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);
    if(iShaderCompiledStatus ==GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
        if(iInfoLogLength > 0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gFragmentShaderObject,iInfoLogLength,&written,szInfoLog);
                fprintf(gpFile, "Fragment Shader Compilation Log: %s\n",szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }

    //shader program

    gShaderProgramObject = glCreateProgram();
    glAttachShader(gShaderProgramObject,gVertexShaderObject);
    glAttachShader(gShaderProgramObject,gFragmentShaderObject);

    //bind in variables before linking
    glBindAttribLocation(gShaderProgramObject,VDG_ATTRIBUTE_POSITION,"vPosition");
    glBindAttribLocation(gShaderProgramObject,VDG_ATTRIBUTE_NORMAL,"vNormal");


    glLinkProgram(gShaderProgramObject);
    GLint iShaderProgramLinkStatus = 0;
    glGetProgramiv(gShaderProgramObject,GL_LINK_STATUS,&iShaderProgramLinkStatus);
    if(iShaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
        if(iInfoLogLength > 0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(gShaderProgramObject,iInfoLogLength,&written,szInfoLog);
                fprintf(gpFile, "Shader Program  Link Log: %s\n",szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }

    //get uniform location
    gModelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
    gViewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
    gProjectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");
    
    Ka_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");
    Kd_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
    Ks_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");
    Material_shininess_uniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");

    gLightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");
    Ld_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld");
    La_uniform = glGetUniformLocation(gShaderProgramObject, "u_La");
    Ls_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls");
    gLKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_LKeyPressed");
    
    getSphereVertexData(sphere_vertices,shpere_normals,sphere_textures,shpere_elements);
    gNumVertices = getNumberOfSphereVertices();
    gNumElements = getNumberOfSphereElements();

  glGenVertexArrays(1, &gVao_sphere);
    glBindVertexArray(gVao_sphere);
    //vertices
    glGenBuffers(1, &gVbo_sphere_position);
    glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);

    glVertexAttribPointer(VDG_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    glEnableVertexAttribArray(VDG_ATTRIBUTE_POSITION);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    //normals
    glGenBuffers(1, &gVbo_sphere_normal);
    glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
    glBufferData(GL_ARRAY_BUFFER, sizeof(shpere_normals), shpere_normals, GL_STATIC_DRAW);

    glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    //elements
    glGenBuffers(1, &gVbo_sphere_elements);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_elements);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(shpere_elements), shpere_elements, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);


    glBindVertexArray(0);


    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);


	glClearColor(0.2f,0.2f,0.2f,0.0f); 

	gPerspectiveProjectionMatrix = vmath::mat4::identity();

    lightAmbient[0] = 0.0f;
    lightAmbient[1] = 0.0f;
    lightAmbient[2] = 0.0f;
    lightAmbient[3] = 1.0f;

    lightDiffuse[0]= 1.0f;
    lightDiffuse[1]= 1.0f;
    lightDiffuse[2]= 1.0f;
    lightDiffuse[3]= 1.0f;

    lightSpecular[0] = 1.0f;
    lightSpecular[1] = 1.0f;
    lightSpecular[2] = 1.0f;
    lightSpecular[3] = 1.0f;

    lightPosition[0] = 50.0f;
    lightPosition[1] = 50.0f;
    lightPosition[2] = 100.0f;
    lightPosition[3] = 1.0f;


    GLfloat material_Ambient1[][4] = {{0.0215f,0.1745f,0.0215f,1.0f},
                             {0.135f,0.2225f,0.1575f,1.0f},
                             {0.05375f,0.05f,0.06625f,1.0f},
                             {0.25f,0.20725f,0.20725f,1.0f},
                             {0.1745f,0.01175f,0.01175f,1.0f},
                             {0.1f,0.18725f,0.1745f,1.0f},
                             {0.329412f,0.223529f,0.027451f,1.0f},
                             {0.2125f,0.1275f,0.054f,1.0f},
                             {0.25f,0.25f,0.25f,1.0f},
                             {0.19125f,0.0735f,0.0225f,1.0f},
                             {0.24725f,0.1995f,0.0745f,1.0f},
                             {0.19225f,0.19225f,0.19225f,1.0f},
                             {0.0f,0.0f,0.0f,1.0f},
                             {0.0f,0.1f,0.06f,1.0f},
                             {0.0f,0.0f,0.0f,1.0f},
                             {0.0f,0.0f,0.0f,1.0f},
                             {0.0f,0.0f,0.0f,1.0f},
                             {0.0f,0.0f,0.0f,1.0f},
                             {0.02f,0.02f,0.02f,1.0f},
                             {0.0f,0.05f,0.05f,1.0f},
                             {0.0f,0.05f,0.0f,1.0f},
                             {0.05f,0.0f,0.0f,1.0f},
                             {0.05f,0.05f,0.05f,1.0f},
                             {0.05f,0.05f,0.0f,1.0f}};
    GLfloat material_Diffuse1[][4]= {{0.07568f,0.61424f,0.07568f,1.0f},
                            {0.54f,0.89f,0.63f,1.0f},
                            {0.18275f,0.17f,0.22525f,1.0f},
                            {1.0f,0.829f,0.829f,1.0f},
                            {0.61424f,0.04136f,0.04136f,1.0f},
                            {0.396f,0.74151f,0.69102f,1.0f},
                            {0.780392f,0.568627f,0.113725f,1.0f},
                            {0.714f,0.4284f,0.18144f,1.0f},
                            {0.4f,0.4f,0.4f,1.0f},
                            {0.7038f,0.27048f,0.0828f,1.0f},
                            {0.75164f,0.60648f,0.22648f,1.0f},
                            {0.50754f,0.50754f,0.50754f,1.0f},
                            {0.01f,0.01f,0.01f,1.0f},
                            {0.0f,0.50980392f,0.50980392f,1.0f},
                            {0.0f,0.35f,0.1f,1.0f},
                            {0.5f,0.0f,0.0f,1.0f},
                            {0.55f,0.55f,0.55f,1.0f},
                            {0.5f,0.5f,0.0f,1.0f},
                            {0.01f,0.01f,0.01f,1.0f},
                            {0.4f,0.5f,0.5f,1.0f},
                            {0.4f,0.5f,0.4f,1.0f},
                            {0.5f,0.4f,0.4f,1.0f},
                            {0.5f,0.5f,0.5f,1.0f},
                            {0.5f,0.5f,0.4f,1.0f}};
    GLfloat material_Specular1[][4] = {{0.633f,0.7278f,0.633f,1.0f},
                                {0.316228f,0.316228f,0.316228f,1.0f},
                                {0.332741f,0.328634f,0.346435f,1.0f},
                                {0.296648f,0.296648f,0.296648f,1.0f},
                                {0.727811f,0.626959f,0.626959f,1.0f},
                                {0.297254f,0.30829f,0.306678f,1.0f},
                                {0.992157f,0.941176f,0.807843f,1.0f},
                                {0.393548f,0.271906f,0.166721f,1.0f},
                                {0.774597f,0.774597f,0.774597f,1.0f},
                                {0.256777f,0.137622f,0.086014f,1.0f},
                                {0.628281f,0.555802f,0.366065f,1.0f},
                                {0.508273f,0.508273f,0.508273f,1.0f},
                                {0.50f,0.50f,0.50f,1.0f},
                                {0.50196078f,0.50196078f,0.50196078f,1.0f},
                                {0.45f,0.55f,0.45f,1.0f},
                                {0.7f,0.6f,0.6f,1.0f},
                                {0.70f,0.70f,0.70f,1.0f},
                                {0.60f,0.60f,0.50f,1.0f},
                                {0.4f,0.4f,0.4f,1.0f},
                                {0.04f,0.7f,0.7f,1.0f},
                                {0.04f,0.7f,0.04f,1.0f},
                                {0.7f,0.04f,0.04f,1.0f},
                                {0.7f,0.7f,0.7f,1.0f},
                                {0.7f,0.7f,0.04f,1.0f}};
    GLfloat material_shininess1[] = {0.6 * 128,
                                0.1 * 128,
                                0.3 * 128,
                                0.088 * 128,
                                0.6 * 128,
                                0.1 * 128,
                                0.21794872 * 128,
                                0.2 * 128,
                                0.6 * 128,
                                0.1 * 128,
                                0.4 * 128,
                                0.4 * 128,
                                0.25 * 128,
                                0.25 * 128,
                                0.25 * 128,
                                0.25 * 128,
                                0.25 * 128,
                                0.25 * 128,
                                0.078125 * 128,
                                0.078125 * 128,
                                0.078125 * 128,
                                0.078125 * 128,
                                0.078125 * 128,
                                0.078125 * 128
                            };

    for (int i = 0; i < 24; ++i)
    {
        for (int j = 0; j < 4; ++j)
        {
            material_Ambient[i][j] = material_Ambient1[i][j];
            material_Diffuse[i][j] = material_Diffuse1[i][j];
            material_Specular[i][j] = material_Specular1[i][j];
        }
        material_shininess[i] = material_shininess1[i];
    }                        

    bIsLkeyPressed = NO;
    xKeyIsPressed = YES;
    yKeyIsPressed = NO;
    zKeyIsPressed = NO;
    angle_sphere = 0.0f;
	CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
	CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);
	CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
	CGLPixelFormatObj cglPixelFormat= (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);

	CVDisplayLinkStart(displayLink);
}

-(void)reshape
{
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
	NSRect rect = [self bounds];

	GLfloat width = rect.size.width;
	GLfloat height = rect.size.height;

	//glViewport(0,0,(GLsizei)width,(GLsizei)height);

	//gPerspectiveProjectionMatrix =vmath::perspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);
    gWidth = width;
    gHeight = height;
	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

- (void)drawRect: (NSRect)dirtyRect
{	
	[self drawView];
}

- (void)drawView
{
	[[self openGLContext]makeCurrentContext];
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);


    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    int count = 0;
    for (int i = 0; i < 4; i++)
    {
        for (int j = 5; j >= 0; j--)
        {
            glViewport(0 +(i * gWidth/4) , 0 + (j* gHeight/6),(GLsizei) gWidth/4  , (GLsizei) gHeight/6 );
            gPerspectiveProjectionMatrix = vmath::perspective(45.0f,((GLfloat) gWidth/4) / ((GLfloat) gHeight/6) ,0.1f,100.0f);


            glUseProgram(gShaderProgramObject);

            if(gbLight == true)
            {
                glUniform1i(gLKeyPressedUniform,1);

                //light properties
                if(xKeyIsPressed){
                //x-axis
                    lightPosition[0] = 0.0f; 
                    lightPosition[1] = 100.0f * cos(angle_sphere); 
                    lightPosition[2] = 100.0f * sin(angle_sphere); 
                }else if(yKeyIsPressed){
                //y-axis
                    lightPosition[0] = 100.0f * cos(angle_sphere); 
                    lightPosition[1] = 0.0f;
                    lightPosition[2] = 100.0f * sin(angle_sphere); 
                }else if(zKeyIsPressed)
                {
                //z-axis
                    lightPosition[0] = 100.0f * cos(angle_sphere); 
                    lightPosition[1] = 100.0f * sin(angle_sphere);
                    lightPosition[2] = 0.0f; 
                }


                glUniform3fv(La_uniform,1,lightAmbient);
                glUniform3fv(Ld_Uniform,1,lightDiffuse);
                glUniform3fv(Ls_Uniform,1,lightSpecular);
                glUniform4fv(gLightPositionUniform,1,lightPosition);

                glUniform3fv(Ka_Uniform,1,material_Ambient[count]);
                glUniform3fv(Kd_Uniform,1,material_Diffuse[count]);
                glUniform3fv(Ks_Uniform,1,material_Specular[count]);
                glUniform1f(Material_shininess_uniform,material_shininess[count]);
            }
            else
            {
                glUniform1i(gLKeyPressedUniform,0);
            }

            vmath::mat4 modelMatrix = vmath::mat4::identity();
            vmath::mat4 viewMatrix = vmath::mat4::identity();
            
            modelMatrix = vmath::translate(0.0f,0.0f,-2.0f);

            
            glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
            glUniformMatrix4fv(gViewMatrixUniform, 1, GL_FALSE, viewMatrix);
            glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);


            glBindVertexArray(gVao_sphere);

            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,gVbo_sphere_elements);
            glDrawElements(GL_TRIANGLES, gNumElements,GL_UNSIGNED_SHORT, 0);
            

            glBindVertexArray(0);


            glUseProgram(0);
            count++;
        }
            
    }

    [self spin];
	CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(void)spin
{
   angle_sphere = angle_sphere + 0.009f;
    if(angle_sphere > 360.0f)
        angle_sphere = angle_sphere-360.0f;
}

- (BOOL)acceptsFirstResponder
{
	[[self window]makeFirstResponder:self];
	return(YES);
}

- (void)keyDown: (NSEvent *)theEvent
{
	int key = (int) [[theEvent characters]characterAtIndex:0];
	switch(key)
	{
		case 27:
			[ self release];
			[NSApp terminate:self];
			break;
		case 'F':
		case 'f':
			[[self window]toggleFullScreen:self];
			break;
        case 'l':
        case 'L':
             if(bIsLkeyPressed == NO)
            {
                gbLight = YES;
                bIsLkeyPressed = YES;
            }
            else
            {
                gbLight = NO;
                bIsLkeyPressed = NO;
            }
            break;
        case 'x':
        case 'X'://x key
                xKeyIsPressed = YES;
                yKeyIsPressed = NO;
                zKeyIsPressed = NO;
        break;
        case 'y':
        case 'Y'://y key
                xKeyIsPressed = NO;
                yKeyIsPressed = YES;
                zKeyIsPressed = NO;
        break;
        case 'z':
        case 'Z'://z key
                xKeyIsPressed = NO;
                yKeyIsPressed = NO;
                zKeyIsPressed = YES;
        break;
		default:
			break;
	}
}

-(void)mouseDown: (NSEvent *)theEvent
{
	[self setNeedsDisplay:YES];
}


-(void)mouseDragged: (NSEvent *)theEvent
{

}

-(void)rightMouseDown: (NSEvent *)theEvent
{
} 

-(void)dealloc
{
	if (gVao_sphere)
    {
        glDeleteVertexArrays(1, &gVao_sphere);
        gVao_sphere = 0;
    }

    if (gVbo_sphere_position)
    {
        glDeleteBuffers(1, &gVbo_sphere_position);
        gVbo_sphere_position = 0;
    }

    if (gVbo_sphere_elements)
    {
        glDeleteBuffers(1, &gVbo_sphere_elements);
        gVbo_sphere_elements = 0;
    }

    if (gVbo_sphere_normal)
    {
        glDeleteBuffers(1, &gVbo_sphere_normal);
        gVbo_sphere_normal = 0;
    }


    glDetachShader(gShaderProgramObject,gVertexShaderObject);
    glDetachShader(gShaderProgramObject,gFragmentShaderObject);

    glDeleteShader(gVertexShaderObject);
    gVertexShaderObject = 0;

    glDeleteShader(gFragmentShaderObject);
    gFragmentShaderObject = 0;

    glDeleteProgram(gShaderProgramObject);
    gShaderProgramObject = 0;
    
    glUseProgram(0);

	CVDisplayLinkStop(displayLink);
	CVDisplayLinkRelease(displayLink);
	[super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink,const CVTimeStamp *pNow,const CVTimeStamp *pOutputTime,CVOptionFlags flagsIn,CVOptionFlags *fOagsOut,void *pDisplayLinkContext)
{
	CVReturn result = [(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
	return (result);
}