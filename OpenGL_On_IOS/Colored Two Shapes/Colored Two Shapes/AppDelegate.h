//
//  AppDelegate.h
//  Colored Two Shapes
//
//  Created by Harsh Patel on 24/06/18.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

