//
//  GLESView.h
//  Colored Two Shapes
//
//  Created by Harsh Patel on 24/06/18.
//

#import <UIKit/UIKit.h>

@interface GLESView : UIView <UIGestureRecognizerDelegate>
-(void)stopAnimation;
-(void)startAnimation;
@end
