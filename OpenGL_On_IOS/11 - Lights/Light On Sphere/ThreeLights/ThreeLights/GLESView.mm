#import <OpenGLES/ES3/gl.h>

#import <OpenGLES/ES3/glext.h>

#import "GLESView.h"
#import "vmath.h"
#import "sphere.h"

enum {
    VDG_ATTRIBUTE_POSITION = 0,
    VDG_ATTRIBUTE_COLOR,
    VDG_ATTRIBUTE_NORMAL,
    VDG_ATTRIBUTE_TEXTURE0
};

@implementation GLESView
{
	EAGLContext *eaglContext;

	GLuint defaultFrameBuffer;
	GLuint colorRenderBuffer;
	GLuint depthRenderBuffer;

    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;

    GLuint gVertexShaderObject_perFragment;
    GLuint gFragmentShaderObject_perFragment;
    GLuint gShaderProgramObject_perFragment;

    GLuint gModelMatrixUniform,gViewMatrixUniform,gProjectionMatrixUniform;
    GLuint gLightPositionUniform;
    GLuint gDoubleTapUniform;
    GLuint Toggle_uniform;

    GLuint La_uniform;
    GLuint Ld_Uniform;
    GLuint Ls_Uniform;

    GLuint gLightPositionUniform1;
    GLuint La1_uniform;
    GLuint Ld1_Uniform;
    GLuint Ls1_Uniform;

    GLuint gLightPositionUniform2;
    GLuint La2_uniform;
    GLuint Ld2_Uniform;
    GLuint Ls2_Uniform;

    GLuint Ka_Uniform;
    GLuint Kd_Uniform;
    GLuint Ks_Uniform;
    GLuint Material_shininess_uniform;

    GLuint gModelMatrixUniform_perFragment,gViewMatrixUniform_perFragment,gProjectionMatrixUniform_perFragment;
    GLuint gDoubleTapUniform_perFragment;
    GLuint gLightPositionUniform_perFragment;

    GLuint La_uniform_perFragment;
    GLuint Ld_Uniform_perFragment;
    GLuint Ls_Uniform_perFragment;


    GLuint gLightPositionUniform1_perFragment;
    GLuint La1_uniform_perFragment;
    GLuint Ld1_Uniform_perFragment;
    GLuint Ls1_Uniform_perFragment;


    GLuint gLightPositionUniform2_perFragment;
    GLuint La2_uniform_perFragment;
    GLuint Ld2_Uniform_perFragment;
    GLuint Ls2_Uniform_perFragment;

    GLuint Ka_Uniform_perFragment;
    GLuint Kd_Uniform_perFragment;
    GLuint Ks_Uniform_perFragment;
    GLuint Material_shininess_uniform_perFragment;

    float sphere_vertices[1146];
    float shpere_normals[1146];
    float sphere_textures[764];
    unsigned short shpere_elements[2280];

    int gNumVertices;
    int gNumElements;
    GLuint gVao_sphere;
    GLuint gVbo_sphere_position;
    GLuint gVbo_sphere_normal;
    GLuint gVbo_sphere_elements;

    GLfloat lightAmbient1[4];
    GLfloat lightDiffuse1[4];
    GLfloat lightSpecular1[4];
    GLfloat lightPosition1[4];

    GLfloat lightAmbient2[4];
    GLfloat lightDiffuse2[4];
    GLfloat lightSpecular2[4];
    GLfloat lightPosition2[4];

    GLfloat lightAmbient3[4];
    GLfloat lightDiffuse3[4];
    GLfloat lightSpecular3[4];
    GLfloat lightPosition3[4];

    GLfloat material_Ambient[4];
    GLfloat material_Diffuse[4];
    GLfloat material_Specular[4];
    GLfloat material_shininess;

    BOOL isAnimating;
    BOOL gbLight;
    BOOL bIsDoubleTap;

    int gToggle;
    float rotateAngle;

    vmath::mat4 gPerspectiveProjectionMatrix;

	id displayLink;
	NSInteger animationFrameInterval;

}

-(id) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self )
    {
        CAEAGLLayer *eaglLayer = (CAEAGLLayer *)super.layer;
        eaglLayer.opaque=YES;
        eaglLayer.drawableProperties=[ NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat ,nil ];
        
        eaglContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if (eaglContext==nil)
        {
            [self release];
            return nil;
        }
        [EAGLContext setCurrentContext:eaglContext];
     
        glGenFramebuffers(1,&defaultFrameBuffer);
        glGenRenderbuffers(1,&colorRenderBuffer);
        glBindFramebuffer(GL_FRAMEBUFFER,defaultFrameBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER,colorRenderBuffer);

        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderBuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
        
        if (glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Failed to Create Complete FrameBuffer Object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1, &defaultFrameBuffer);
            glDeleteRenderbuffers(1, &colorRenderBuffer);
            glDeleteRenderbuffers(1, &depthRenderBuffer);
            //[self release];
            return nil;
        }
        
        printf("Renderer : %s |\n GL Version : %s |\n GLSL Version : %s \n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        //vertex shader
        gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

        const GLchar *vertexShaderSourceCode =
            "#version 300 es" \
            "\n" \
            "in vec4 vPosition;" \
            "in vec3 vNormal;" \
            "uniform mat4 u_model_matrix;" \
            "uniform mat4 u_view_matrix;" \
            "uniform mat4 u_projection_matrix;" \
            "uniform mediump int u_double_tap;" \
            "uniform vec3 u_La;" \
            "uniform vec3 u_Ld;" \
            "uniform vec3 u_Ls;" \
            "uniform vec3 u_Ka;" \
            "uniform vec3 u_Kd;" \
            "uniform vec3 u_Ks;" \
            "uniform vec4 u_light_position;" \
            "uniform vec3 u_La1;" \
            "uniform vec3 u_Ld1;" \
            "uniform vec3 u_Ls1;" \
            "uniform vec4 u_light_position1;" \
            "uniform vec3 u_La2;" \
            "uniform vec3 u_Ld2;" \
            "uniform vec3 u_Ls2;" \
            "uniform vec4 u_light_position2;" \
            "uniform float u_material_shininess;" \
            "out vec3 phong_ads_color;" \
            "void main (void)" \
            "{" \
            "if(u_double_tap == 1)" \
            "{" \
            "vec4 eyeCoordinates =u_view_matrix * u_model_matrix * vPosition;" \
            "vec3 transformed_normals = normalize(mat3(u_view_matrix * u_model_matrix)*vNormal);" \
            
            "vec3 light_direction1 = normalize(vec3(u_light_position) - eyeCoordinates.xyz);" \
            "float tn_dot_ld1 = max(dot(transformed_normals,light_direction1),0.0);" \
            "vec3 ambient1 = u_La * u_Ka;" \
            "vec3 diffuse1 = u_Ld * u_Kd * tn_dot_ld1;" \
            "vec3 reflection_vector1 = reflect(-light_direction1 , transformed_normals);" \
            "vec3 viewer_vector = normalize(-eyeCoordinates.xyz);" \
            "vec3 specular1 = u_Ls * u_Ks * pow(max(dot(reflection_vector1,viewer_vector),0.0),u_material_shininess);" \
            
            "vec3 light_direction2 = normalize(vec3(u_light_position1) - eyeCoordinates.xyz);" \
            "float tn_dot_ld2 = max(dot(transformed_normals,light_direction2),0.0);" \
            "vec3 ambient2 = u_La1 * u_Ka;" \
            "vec3 diffuse2 = u_Ld1 * u_Kd * tn_dot_ld2;" \
            "vec3 reflection_vector2 = reflect(-light_direction2 , transformed_normals);" \
            "vec3 specular2 = u_Ls1 * u_Ks * pow(max(dot(reflection_vector2,viewer_vector),0.0),u_material_shininess);" \
            
            "vec3 light_direction3 = normalize(vec3(u_light_position2) - eyeCoordinates.xyz);" \
            "float tn_dot_ld3 = max(dot(transformed_normals,light_direction3),0.0);" \
            "vec3 ambient3 = u_La2 * u_Ka;" \
            "vec3 diffuse3 = u_Ld2 * u_Kd * tn_dot_ld3;" \
            "vec3 reflection_vector3 = reflect(-light_direction3 , transformed_normals);" \
            "vec3 specular3 = u_Ls2 * u_Ks * pow(max(dot(reflection_vector3,viewer_vector),0.0),u_material_shininess);" \
            
            "vec3 ambient = ambient1 + ambient2 + ambient3;" \
            "vec3 diffuse = diffuse1 + diffuse2 + diffuse3;" \
            "vec3 specular = specular1 + specular2 + specular3;" \
            "phong_ads_color = ambient + diffuse + specular; "\
            "}" \
            "else" \
            "{" \
            " phong_ads_color = vec3(1.0,1.0,1.0);"
            "}" \
            "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix *vPosition;" \
            "}";

        glShaderSource(gVertexShaderObject,1,(const GLchar **)&vertexShaderSourceCode,NULL);
        glCompileShader(gVertexShaderObject);
        
        GLint iInfoLogLength = 0;
        GLint iShaderCompiledStatus = 0;
        char * szInfoLog = NULL;
        glGetShaderiv(gVertexShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);
        if(iShaderCompiledStatus ==GL_FALSE)
        {
            glGetShaderiv(gVertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
            if(iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if(szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(gVertexShaderObject,iInfoLogLength,&written,szInfoLog);
                    printf("Vertex Shader Compilation Log: %s\n",szInfoLog);
                    free(szInfoLog);
                    [self release];
                    exit(0);
                }
            }
        }    

        //fragment shader
        gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

        
        const GLchar * gFragmentShaderSourceCode =
            "#version 300 es" \
            "\n" \
            "precision highp float;"\
            "in vec3 phong_ads_color;"
            "out vec4 FragColor;" \
            "void main (void)" \
            "{" \
            "FragColor = vec4(phong_ads_color,1.0);" \
            "}";

        glShaderSource(gFragmentShaderObject,1,(const GLchar **)&gFragmentShaderSourceCode,NULL);
        glCompileShader(gFragmentShaderObject);

        glGetShaderiv(gFragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);
        if(iShaderCompiledStatus ==GL_FALSE)
        {
            glGetShaderiv(gFragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
            if(iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if(szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(gFragmentShaderObject,iInfoLogLength,&written,szInfoLog);
                    printf("Fragment Shader Compilation Log: %s\n",szInfoLog);
                    free(szInfoLog);
                    [self release];
                    exit(0);
                }
            }
        }

        //shader program

        gShaderProgramObject = glCreateProgram();
        glAttachShader(gShaderProgramObject,gVertexShaderObject);
        glAttachShader(gShaderProgramObject,gFragmentShaderObject);

        //bind in variables before linking
        glBindAttribLocation(gShaderProgramObject,VDG_ATTRIBUTE_POSITION,"vPosition");
        glBindAttribLocation(gShaderProgramObject,VDG_ATTRIBUTE_NORMAL,"vNormal");


        glLinkProgram(gShaderProgramObject);
        GLint iShaderProgramLinkStatus = 0;
        glGetProgramiv(gShaderProgramObject,GL_LINK_STATUS,&iShaderProgramLinkStatus);
        if(iShaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(gShaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
            if(iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if(szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(gShaderProgramObject,iInfoLogLength,&written,szInfoLog);
                    printf("Shader Program  Link Log: %s\n",szInfoLog);
                    free(szInfoLog);
                    [self release];
                    exit(0);
                }
            }
        }

        //get uniform location
        gModelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
        gViewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
        gProjectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");
        
        Ka_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");
        Kd_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
        Ks_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");
        Material_shininess_uniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");

        gLightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");
        Ld_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld");
        La_uniform = glGetUniformLocation(gShaderProgramObject, "u_La");
        Ls_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls");
        gDoubleTapUniform = glGetUniformLocation(gShaderProgramObject, "u_double_tap");
        
        gLightPositionUniform1 = glGetUniformLocation(gShaderProgramObject, "u_light_position1");
        Ld1_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld1");
        La1_uniform = glGetUniformLocation(gShaderProgramObject, "u_La1");
        Ls1_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls1");
        
        gLightPositionUniform2 = glGetUniformLocation(gShaderProgramObject, "u_light_position2");
        Ld2_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld2");
        La2_uniform = glGetUniformLocation(gShaderProgramObject, "u_La2");
        Ls2_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls2");


        //######################################
        //          Per Fragment
        //######################################
        //vertex shader
        gVertexShaderObject_perFragment  = glCreateShader(GL_VERTEX_SHADER);

        const GLchar *vertexShaderSourceCode_perFragment =
            "#version 300 es" \
            "\n" \
            "in vec4 vPosition;" \
            "in vec3 vNormal;" \
            "uniform mat4 u_model_matrix;" \
            "uniform mat4 u_view_matrix;" \
            "uniform mat4 u_projection_matrix;" \
            "uniform mediump int u_double_tap;" \
            "uniform vec4 u_light_position;" \
            "uniform vec4 u_light_position1;" \
            "uniform vec4 u_light_position2;" \
            "out vec3 transformed_normals;" \
            "out vec3 light_direction1;"\
            "out vec3 light_direction2;"\
            "out vec3 light_direction3;"\
            "out vec3 viewer_vector;"\
            "void main (void)" \
            "{" \
            "if(u_double_tap == 1)" \
            "{" \
            "vec4 eyeCoordinates =u_view_matrix * u_model_matrix * vPosition;" \
            "transformed_normals = mat3(u_view_matrix * u_model_matrix)*vNormal;" \
            "light_direction1 = vec3(u_light_position) - eyeCoordinates.xyz;" \
            "light_direction2 = vec3(u_light_position1) - eyeCoordinates.xyz;" \
            "light_direction3 = vec3(u_light_position2) - eyeCoordinates.xyz;" \
            "viewer_vector = -eyeCoordinates.xyz;" \
            "}" \
            "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix *vPosition;" \
            "}";

        glShaderSource(gVertexShaderObject_perFragment , 1, (const GLchar **)&vertexShaderSourceCode_perFragment, NULL);
        glCompileShader(gVertexShaderObject_perFragment );

        iInfoLogLength = 0;
        iShaderCompiledStatus = 0;
        szInfoLog = NULL;
        glGetShaderiv(gVertexShaderObject_perFragment , GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(gVertexShaderObject_perFragment , GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(gVertexShaderObject_perFragment , iInfoLogLength, &written, szInfoLog);
                    printf("Vertex Shader Info Log: %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                    exit(0);
                }
            }
        }
        //fragment shader
        gFragmentShaderObject_perFragment  = glCreateShader(GL_FRAGMENT_SHADER);

        const GLchar * fragmentShaderSourceCode_perFragment =
            "#version 300 es" \
            "\n" \
            "precision highp float;"\
            "in vec3 transformed_normals;"\
            "in vec3 light_direction1;"\
            "in vec3 light_direction2;"\
            "in vec3 light_direction3;"\
            "in vec3 viewer_vector;"\
            "uniform vec3 u_La;" \
            "uniform vec3 u_Ld;" \
            "uniform vec3 u_Ls;" \
            "uniform vec3 u_La1;" \
            "uniform vec3 u_Ld1;" \
            "uniform vec3 u_Ls1;" \
            "uniform vec3 u_La2;" \
            "uniform vec3 u_Ld2;" \
            "uniform vec3 u_Ls2;" \
            "uniform vec3 u_Ka;" \
            "uniform vec3 u_Kd;" \
            "uniform vec3 u_Ks;" \
            "uniform float u_material_shininess;" \
            "uniform int u_double_tap;"
            "out vec4 FragColor;" \
            "void main (void)" \
            "{" \
            "vec3 phong_ads_color;" \
            "if(u_double_tap == 1)"\
            "{" \
            "vec3 normalized_transformed_normals = normalize(transformed_normals);"\
            "vec3 normalized_light_direction1 = normalize(light_direction1);"\
            "vec3 normalized_light_direction2 = normalize(light_direction2);"\
            "vec3 normalized_light_direction3 = normalize(light_direction3);"\
            "vec3 normalized_viewer_vector = normalize(viewer_vector);"\
            
            "vec3 ambient1 = u_La * u_Ka;"\
            "float tn_dot_ld1 = max(dot(normalized_transformed_normals , normalized_light_direction1),0.0);"\
            "vec3 diffuse1 = u_Ld * u_Kd * tn_dot_ld1;"\
            "vec3 reflection_vector1 = reflect(-normalized_light_direction1,normalized_transformed_normals);"\
            "vec3 specular1 = u_Ls * u_Ks *pow(max(dot(reflection_vector1,normalized_viewer_vector),0.0),u_material_shininess);"\
            
            "vec3 ambient2 = u_La1 * u_Ka;"\
            "float tn_dot_ld2 = max(dot(normalized_transformed_normals , normalized_light_direction2),0.0);"\
            "vec3 diffuse2 = u_Ld1 * u_Kd * tn_dot_ld2;"\
            "vec3 reflection_vector2 = reflect(-normalized_light_direction2,normalized_transformed_normals);"\
            "vec3 specular2 = u_Ls1 * u_Ks *pow(max(dot(reflection_vector2,normalized_viewer_vector),0.0),u_material_shininess);"\
            
            "vec3 ambient3 = u_La2 * u_Ka;"\
            "float tn_dot_ld3 = max(dot(normalized_transformed_normals , normalized_light_direction3),0.0);"\
            "vec3 diffuse3 = u_Ld2 * u_Kd * tn_dot_ld3;"\
            "vec3 reflection_vector3 = reflect(-normalized_light_direction3,normalized_transformed_normals);"\
            "vec3 specular3 = u_Ls2 * u_Ks *pow(max(dot(reflection_vector3,normalized_viewer_vector),0.0),u_material_shininess);"\
            
            "vec3 ambient = ambient1 + ambient2 + ambient3;" \
            "vec3 diffuse = diffuse1 + diffuse2 + diffuse3;" \
            "vec3 specular = specular1 + specular2 + specular3;" \

            "phong_ads_color = ambient + diffuse + specular;"\
            "}"\
            "else"\
            "{"\
            "phong_ads_color = vec3(1.0,1.0,1.0);"\
            "}"\
            "FragColor = vec4(phong_ads_color,1.0);" \
            "}";
        glShaderSource(gFragmentShaderObject_perFragment , 1, (const GLchar **)&fragmentShaderSourceCode_perFragment, NULL);
        glCompileShader(gFragmentShaderObject_perFragment );
        glGetShaderiv(gFragmentShaderObject_perFragment , GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(gFragmentShaderObject_perFragment , GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(gFragmentShaderObject_perFragment , iInfoLogLength, &written, szInfoLog);
                    printf("Fragment Shader Info Log: %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                    exit(0);
                }
            }
        }

        //shader program

        gShaderProgramObject_perFragment = glCreateProgram();
        glAttachShader(gShaderProgramObject_perFragment, gVertexShaderObject_perFragment );
        glAttachShader(gShaderProgramObject_perFragment, gFragmentShaderObject_perFragment );
        //bind in variables before linking
        glBindAttribLocation(gShaderProgramObject_perFragment, VDG_ATTRIBUTE_POSITION, "vPosition");
        glBindAttribLocation(gShaderProgramObject_perFragment, VDG_ATTRIBUTE_NORMAL, "vNormal");

        glLinkProgram(gShaderProgramObject_perFragment);

        iShaderProgramLinkStatus = 0;
        glGetProgramiv(gShaderProgramObject_perFragment, GL_LINK_STATUS, &iShaderProgramLinkStatus);
        if (iShaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(gShaderProgramObject_perFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(gShaderProgramObject_perFragment, iInfoLogLength, &written, szInfoLog);
                    printf("Program Link Log: %s \n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                    exit(0);
                }
            }
        }


        //get uniform's address
        gModelMatrixUniform_perFragment= glGetUniformLocation(gShaderProgramObject_perFragment, "u_model_matrix");
        gViewMatrixUniform_perFragment= glGetUniformLocation(gShaderProgramObject_perFragment, "u_view_matrix");
        gProjectionMatrixUniform_perFragment= glGetUniformLocation(gShaderProgramObject_perFragment, "u_projection_matrix");
        
        Ka_Uniform_perFragment= glGetUniformLocation(gShaderProgramObject_perFragment, "u_Ka");
        Kd_Uniform_perFragment= glGetUniformLocation(gShaderProgramObject_perFragment, "u_Kd");
        Ks_Uniform_perFragment= glGetUniformLocation(gShaderProgramObject_perFragment, "u_Ks");
        Material_shininess_uniform_perFragment= glGetUniformLocation(gShaderProgramObject_perFragment, "u_material_shininess");

        gLightPositionUniform_perFragment= glGetUniformLocation(gShaderProgramObject_perFragment, "u_light_position");
        Ld_Uniform_perFragment= glGetUniformLocation(gShaderProgramObject_perFragment, "u_Ld");
        La_uniform_perFragment= glGetUniformLocation(gShaderProgramObject_perFragment, "u_La");
        Ls_Uniform_perFragment= glGetUniformLocation(gShaderProgramObject_perFragment, "u_Ls");
        
        gLightPositionUniform1_perFragment = glGetUniformLocation(gShaderProgramObject_perFragment, "u_light_position1");
        Ld1_Uniform_perFragment= glGetUniformLocation(gShaderProgramObject_perFragment, "u_Ld1");
        La1_uniform_perFragment= glGetUniformLocation(gShaderProgramObject_perFragment, "u_La1");
        Ls1_Uniform_perFragment= glGetUniformLocation(gShaderProgramObject_perFragment, "u_Ls1");
        
        gLightPositionUniform2_perFragment = glGetUniformLocation(gShaderProgramObject_perFragment, "u_light_position2");
        Ld2_Uniform_perFragment= glGetUniformLocation(gShaderProgramObject_perFragment, "u_Ld2");
        La2_uniform_perFragment= glGetUniformLocation(gShaderProgramObject_perFragment, "u_La2");
        Ls2_Uniform_perFragment= glGetUniformLocation(gShaderProgramObject_perFragment, "u_Ls2");

        gDoubleTapUniform_perFragment = glGetUniformLocation(gShaderProgramObject_perFragment, "u_double_tap");
        
        getSphereVertexData(sphere_vertices,shpere_normals,sphere_textures,shpere_elements);
        gNumVertices = getNumberOfSphereVertices();
        gNumElements = getNumberOfSphereElements();

        glGenVertexArrays(1, &gVao_sphere);
        glBindVertexArray(gVao_sphere);
        //vertices
        glGenBuffers(1, &gVbo_sphere_position);
        glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
        glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);

        glVertexAttribPointer(VDG_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

        glEnableVertexAttribArray(VDG_ATTRIBUTE_POSITION);
        
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        //normals
        glGenBuffers(1, &gVbo_sphere_normal);
        glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
        glBufferData(GL_ARRAY_BUFFER, sizeof(shpere_normals), shpere_normals, GL_STATIC_DRAW);

        glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

        glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);
        
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        //elements
        glGenBuffers(1, &gVbo_sphere_elements);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_elements);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(shpere_elements), shpere_elements, GL_STATIC_DRAW);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);


        glBindVertexArray(0);


        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);


        isAnimating=NO;
        animationFrameInterval=60;
        
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f); //blue
        
        gPerspectiveProjectionMatrix = vmath::mat4::identity();

        lightAmbient1[0] = 0.0f;
        lightAmbient1[1] = 0.0f;
        lightAmbient1[2] = 0.0f;
        lightAmbient1[3] = 1.0f;

        lightDiffuse1[0]= 1.0f;
        lightDiffuse1[1]= 0.0f;
        lightDiffuse1[2]= 0.0f;
        lightDiffuse1[3]= 0.0f;

        lightSpecular1[0] = 1.0f;
        lightSpecular1[1] = 0.0f;
        lightSpecular1[2] = 0.0f;
        lightSpecular1[3] = 0.0f;

        lightPosition1[0] = 0.0f;
        lightPosition1[1] = 1000.0f;
        lightPosition1[2] = 1000.0f;
        lightPosition1[3] = 1.0f;


        lightAmbient2[0] = 0.0f;
        lightAmbient2[1] = 0.0f;
        lightAmbient2[2] = 0.0f;
        lightAmbient2[3] = 1.0f;

        lightDiffuse2[0]= 0.0f;
        lightDiffuse2[1]= 1.0f;
        lightDiffuse2[2]= 0.0f;
        lightDiffuse2[3]= 0.0f;

        lightSpecular2[0] = 0.0f;
        lightSpecular2[1] = 1.0f;
        lightSpecular2[2] = 0.0f;
        lightSpecular2[3] = 0.0f;

        lightPosition2[0] = 0.0f;
        lightPosition2[1] = 1000.0f;
        lightPosition2[2] = 1000.0f;
        lightPosition2[3] = 1.0f;

        lightAmbient3[0] = 0.0f;
        lightAmbient3[1] = 0.0f;
        lightAmbient3[2] = 0.0f;
        lightAmbient3[3] = 1.0f;

        lightDiffuse3[0]= 0.0f;
        lightDiffuse3[1]= 0.0f;
        lightDiffuse3[2]= 1.0f;
        lightDiffuse3[3]= 0.0f;

        lightSpecular3[0] = 0.0f;
        lightSpecular3[1] = 0.0f;
        lightSpecular3[2] = 1.0f;
        lightSpecular3[3] = 0.0f;

        lightPosition3[0] = 0.0f;
        lightPosition3[1] = 1000.0f;
        lightPosition3[2] = 1000.0f;
        lightPosition3[3] = 1.0f;

        material_Ambient[0] = 0.0f;
        material_Ambient[1] = 0.0f;
        material_Ambient[2] = 0.0f;
        material_Ambient[3] = 1.0f;

        material_Diffuse[0]= 1.0f;
        material_Diffuse[1]= 1.0f;
        material_Diffuse[2]= 1.0f;
        material_Diffuse[3]= 1.0f;

        material_Specular[0] = 1.0f;
        material_Specular[1] = 1.0f;
        material_Specular[2] = 1.0f;
        material_Specular[3] = 1.0f;

        material_shininess = 50.0f;
        gToggle = 1;
        bIsDoubleTap = NO;
        rotateAngle = 0.0f;
      
        UITapGestureRecognizer *singleTapGestureRecognizer = [ [ UITapGestureRecognizer alloc ] initWithTarget:self action:@selector(onSingleTap:)] ;
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1]; 
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        UISwipeGestureRecognizer *swipGestureRecognizer = [ [UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:) ] ;
        [self addGestureRecognizer:swipGestureRecognizer];
        
        UILongPressGestureRecognizer *longPressGestureRecognizer = [ [UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:) ];
        
        [self addGestureRecognizer:longPressGestureRecognizer];
        
        
    }
    
    return (self);
    
}

+(Class)layerClass
{
    return ([CAEAGLLayer class]);
}

-(void)drawView:(id)sender
{
    [EAGLContext setCurrentContext:eaglContext];
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
    
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);
    
    glUseProgram(gShaderProgramObject);


    vmath::mat4 modelMatrix = vmath::mat4::identity();
    vmath::mat4 viewMatrix = vmath::mat4::identity();
    
    modelMatrix = vmath::translate(0.0f,0.0f,-2.0f);

    if(gToggle ==1)
    {
        glUseProgram(gShaderProgramObject);

        if(gbLight == YES)
        {
            glUniform1i(gDoubleTapUniform,1);

            //light properties
            //red x axis rotation
            lightPosition1[1] = 1000.0f * cos(rotateAngle);
            lightPosition1[2] = 1000.0f * sin(rotateAngle);
            glUniform3fv(La_uniform,1,lightAmbient1);
            glUniform3fv(Ld_Uniform,1,lightDiffuse1);
            glUniform3fv(Ls_Uniform,1,lightSpecular1);
            glUniform4fv(gLightPositionUniform,1,lightPosition1);
            
            //blue z -axis rotation
            lightPosition2[0] = 1000.0f * cos(rotateAngle);
            lightPosition2[1] = 1000.0f * sin(rotateAngle);
            glUniform3fv(La1_uniform,1,lightAmbient2);
            glUniform3fv(Ld1_Uniform,1,lightDiffuse2);
            glUniform3fv(Ls1_Uniform,1,lightSpecular2);
            glUniform4fv(gLightPositionUniform1,1,lightPosition2);

            //grenn y-axis rotation
            lightPosition3[0] = 1000.0f * cos(rotateAngle);
            lightPosition3[2] = 1000.0f * sin(rotateAngle);
            glUniform3fv(La2_uniform,1,lightAmbient3);
            glUniform3fv(Ld2_Uniform,1,lightDiffuse3);
            glUniform3fv(Ls2_Uniform,1,lightSpecular3);
            glUniform4fv(gLightPositionUniform2,1,lightPosition3);

            glUniform3fv(Ka_Uniform,1,material_Ambient);
            glUniform3fv(Kd_Uniform,1,material_Diffuse);
            glUniform3fv(Ks_Uniform,1,material_Specular);
            glUniform1f(Material_shininess_uniform,material_shininess);

        }
        else
        {
            glUniform1i(gDoubleTapUniform,0);

        }
        glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
        glUniformMatrix4fv(gViewMatrixUniform, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

    }
    else
    {
        glUseProgram(gShaderProgramObject_perFragment);

        if(gbLight == YES)
        {
            glUniform1i(gDoubleTapUniform_perFragment,1); 
            //light properties
            //red x axis rotation
            lightPosition1[1] = 1000.0f * cos(rotateAngle);
            lightPosition1[2] = 1000.0f * sin(rotateAngle);
            glUniform3fv(La_uniform_perFragment,1,lightAmbient1);
            glUniform3fv(Ld_Uniform_perFragment,1,lightDiffuse1);
            glUniform3fv(Ls_Uniform_perFragment,1,lightSpecular1);
            glUniform4fv(gLightPositionUniform_perFragment,1,lightPosition1);
            
            //blue z -axis rotation
            lightPosition2[0] = 1000.0f * cos(rotateAngle);
            lightPosition2[1] = 1000.0f * sin(rotateAngle);
            glUniform3fv(La1_uniform_perFragment,1,lightAmbient2);
            glUniform3fv(Ld1_Uniform_perFragment,1,lightDiffuse2);
            glUniform3fv(Ls1_Uniform_perFragment,1,lightSpecular2);
            glUniform4fv(gLightPositionUniform1_perFragment,1,lightPosition2);

            //green y-axis rotation
            lightPosition3[0] = 1000.0f * cos(rotateAngle);
            lightPosition3[2] = 1000.0f * sin(rotateAngle);
            glUniform3fv(La2_uniform_perFragment,1,lightAmbient3);
            glUniform3fv(Ld2_Uniform_perFragment,1,lightDiffuse3);
            glUniform3fv(Ls2_Uniform_perFragment,1,lightSpecular3);
            glUniform4fv(gLightPositionUniform2_perFragment,1,lightPosition3);

            glUniform3fv(Ka_Uniform_perFragment,1,material_Ambient);
            glUniform3fv(Kd_Uniform_perFragment,1,material_Diffuse);
            glUniform3fv(Ks_Uniform_perFragment,1,material_Specular);
            glUniform1f(Material_shininess_uniform_perFragment,material_shininess);
        }
        else
        {
            glUniform1i(gDoubleTapUniform_perFragment,0);
        }
        glUniformMatrix4fv(gModelMatrixUniform_perFragment, 1, GL_FALSE, modelMatrix);
        glUniformMatrix4fv(gViewMatrixUniform_perFragment, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(gProjectionMatrixUniform_perFragment, 1, GL_FALSE, gPerspectiveProjectionMatrix);
    }
    


    
    glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(gViewMatrixUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);


    glBindVertexArray(gVao_sphere);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,gVbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, gNumElements,GL_UNSIGNED_SHORT, 0);
    

    glBindVertexArray(0);


    glUseProgram(0);


    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];

    [self spin];  
    
}

-(void)spin
{
    rotateAngle = rotateAngle + 0.009f;
    if (rotateAngle >= 360.0f)
        rotateAngle = 0.0f;   
}

-(void)layoutSubviews
{
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)self.layer];
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
    
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
    
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Failed to Create Complete FrameBuffer Object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }

    glViewport(0,0,(GLsizei)width,(GLsizei)height);

    gPerspectiveProjectionMatrix =vmath::perspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);
    

    [self drawView:nil];
}

-(void)startAnimation
{
    if (!isAnimating)
    {
        displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimating=YES;
    }
}

-(void)stopAnimation
{
    if (isAnimating)
    {
        [displayLink invalidate];
        displayLink=nil;
        
        isAnimating=NO;
    }
}

-(BOOL) acceptsFirstResponder
{
    return YES;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
       
}


-(void) onLongPress: (UIGestureRecognizer *)gr
{

}

-(void) onSwipe: (UIGestureRecognizer *)gr
{
    [self release];
    exit(0);
}

-(void) onDoubleTap: (UITapGestureRecognizer *)gr
{
    if(bIsDoubleTap == NO)
    {
        gbLight = YES;
        bIsDoubleTap = YES;
    }
    else
    {
        gbLight = NO;
        bIsDoubleTap = NO;
    }
   
}

-(void) onSingleTap: (UITapGestureRecognizer *)gr
{
    if(gToggle ==1)
        gToggle =2;
    else
        gToggle=1;
   
}

-(void)dealloc
{
    if (gVao_sphere)
    {
        glDeleteVertexArrays(1, &gVao_sphere);
        gVao_sphere = 0;
    }

    if (gVbo_sphere_position)
    {
        glDeleteBuffers(1, &gVbo_sphere_position);
        gVbo_sphere_position = 0;
    }

    if (gVbo_sphere_elements)
    {
        glDeleteBuffers(1, &gVbo_sphere_elements);
        gVbo_sphere_elements = 0;
    }

    if (gVbo_sphere_normal)
    {
        glDeleteBuffers(1, &gVbo_sphere_normal);
        gVbo_sphere_normal = 0;
    }

    glDetachShader(gShaderProgramObject,gVertexShaderObject);
    glDetachShader(gShaderProgramObject,gFragmentShaderObject);

    glDeleteShader(gVertexShaderObject);
    gVertexShaderObject = 0;

    glDeleteShader(gFragmentShaderObject);
    gFragmentShaderObject = 0;

    glDeleteProgram(gShaderProgramObject);
    gShaderProgramObject = 0;

    glDetachShader(gShaderProgramObject_perFragment,gVertexShaderObject_perFragment);
    glDetachShader(gShaderProgramObject_perFragment,gFragmentShaderObject_perFragment);

    glDeleteShader(gVertexShaderObject_perFragment);
    gVertexShaderObject_perFragment = 0;

    glDeleteShader(gFragmentShaderObject_perFragment);
    gFragmentShaderObject_perFragment = 0;

    glDeleteProgram(gShaderProgramObject_perFragment);
    gShaderProgramObject_perFragment = 0;
    
    glUseProgram(0);

    if (depthRenderBuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderBuffer);
        depthRenderBuffer=0;
    }
    
    if (colorRenderBuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderBuffer);
        colorRenderBuffer=0;
    }
    
    if (defaultFrameBuffer)
    {
        glDeleteRenderbuffers(1, &defaultFrameBuffer);
        defaultFrameBuffer=0;
    }
    
    if ([EAGLContext currentContext]==eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];

    eaglContext=nil;
    
    [super dealloc];
}

@end
