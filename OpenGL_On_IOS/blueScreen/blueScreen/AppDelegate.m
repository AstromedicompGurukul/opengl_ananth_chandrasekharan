#import "AppDelegate.h"

#import "ViewController.h"

#import "GLESView.h"

@implementation AppDelegate
{
@private
    UIWindow *mainWindow;
    ViewController *mainViewController;
    GLESView *myView;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    CGRect screenBounds =[[UIScreen mainScreen]bounds];
    
    mainWindow = [[UIWindow alloc]initWithFrame:screenBounds];
    
    mainViewController = [[ViewController alloc]init];
    
    [mainWindow setRootViewController:mainViewController];
    
    myView=[[GLESView alloc]initWithFrame:screenBounds];
    
    [mainViewController setView:myView];
    
    [myView release];
    
    [mainWindow addSubview:[mainViewController view]];
    
    [mainWindow makeKeyAndVisible];
    
    [myView startAnimation];
    
    return (YES);
}


- (void)applicationWillResignActive:(UIApplication *)application {
     [myView stopAnimation];
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    [myView startAnimation];
}


- (void)applicationWillTerminate:(UIApplication *)application {
    [myView stopAnimation];
    
}

-(void)dealloc{
    
    [myView release];
    
    [mainViewController release];
    
    [mainWindow release];
    
    [super dealloc];
}
@end
