package  com.astromedicomp.Ganesha_PerFragmentPhong;

public class GLESMacros
{
    // attribute index
    public static final int ARD_ATTRIBUTE_VERTEX=0;
    public static final int ARD_ATTRIBUTE_COLOR=1;
    public static final int ARD_ATTRIBUTE_NORMAL=2;
    public static final int ARD_ATTRIBUTE_TEXTURE0=3;
}
