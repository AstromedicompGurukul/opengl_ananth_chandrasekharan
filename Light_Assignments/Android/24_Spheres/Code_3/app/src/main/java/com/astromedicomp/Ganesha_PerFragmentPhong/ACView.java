package com.astromedicomp.Ganesha_PerFragmentPhong;

import android.content.Context;
import android.opengl.GLSurfaceView;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;
import android.opengl.GLES32;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.lang.Math;
import android.opengl.Matrix;

public class ACView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	private final Context context;

	private GestureDetector gestureDetector;

	private int gVertexShaderObject;
	private int gFragmentShaderObject;
	private int gShaderProgramObject;

	private int numVertices;
	private int numElements;

	private float angleX_axis = 0.0f;
	private float angleY_axis = 0.0f;
	private float angleZ_axis = 0.0f;

	private int[] vao_sphere = new int[1];
    private int[] vbo_sphere_position = new int[1];
    private int[] vbo_sphere_normal = new int[1];
    private int[] vbo_sphere_element = new int[1];

    private float light_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f};
    private float light_diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f};
    private float light_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f};
    private float[] lightPosition = new float[4];

private float material1_ambient[] = { 0.0215f, 0.1745f, 0.0215f, 1.0f };
private float material1_diffuse[] = { 0.07568f, 0.061424f, 0.07568f, 1.0f };
private float material1_specular[] = { 0.633f, 0.727811f, 0.633f, 1.0f };
private float material1_shininess = 76.8f;

//Jade
private float material2_ambient[] = { 0.135f, 0.2225f ,0.1575f, 1.0f };
private float material2_diffuse[] = { 0.54f, 0.89f, 0.63f, 1.0f };
private float material2_specular[] = { 0.316288f, 0.316288f, 0.316288f, 1.0f };
private float material2_shininess = 12.8f;

//Obsidian
private float material3_ambient[] = { 0.05375f, 0.05f, 0.06625f, 1.0f };
private float material3_diffuse[] = { 0.18275f, 0.17f, 0.22525f, 1.0f };
private float material3_specular[] = { 0.332741f, 0.328634f, 0.346435f, 1.0f };
private float material3_shininess =  38.4f;

//Pearl
private float material4_ambient[] = { 0.25f, 0.20725f, 0.20725f, 1.0f };
private float material4_diffuse[] = { 1.0f, 0.829f, 0.829f, 1.0f };
private float material4_specular[] = { 0.296648f, 0.296648f, 0.296648f, 1.0f };
private float material4_shininess = 11.264f;

//Ruby
private float material5_ambient[] = { 0.1745f, 0.01175f, 0.01175f, 1.0f };
private float material5_diffuse[] = { 0.61424f, 0.04136f, 0.04136f, 1.0f };
private float material5_specular[] = { 0.727811f, 0.626959f, 0.626959f, 1.0f };
private float material5_shininess =  76.8f;

//Turquoise
private float material6_ambient[] = { 0.1f, 0.18725f, 0.1745f, 1.0f };
private float material6_diffuse[] = { 0.396f, 0.74151f, 0.69102f, 1.0f };
private float material6_specular[] = { 0.297254f, 0.30829f, 0.306678f, 1.0f };
private float material6_shininess =  12.8f;

//Brass
private float material7_ambient[] = { 0.329412f, 0.223529f, 0.027451f, 1.0f };
private float material7_diffuse[] = { 0.780392f, 0.568627f, 0.113725f, 1.0f };
private float material7_specular[] = { 0.992157f, 0.941176f, 0.807843f, 1.0f };
private float material7_shininess =  27.897443616f;

//Bronze
private float material8_ambient[] = { 0.2125f, 0.1275f, 0.054f, 1.0f };
private float material8_diffuse[] = { 0.714f, 0.4284f, 0.18144f, 1.0f };
private float material8_specular[] = { 0.393548f, 0.271906f, 0.166721f, 1.0f };
private float material8_shininess =  25.6f;

//Chrome
private float material9_ambient[] = { 0.25f, 0.25f, 0.25f, 1.0f };
private float material9_diffuse[] = { 0.4f, 0.4f, 0.4f, 1.0f };
private float material9_specular[] = { 0.774597f, 0.774597f, 0.774597f, 1.0f };
private float material9_shininess = 76.8f;

//Copper
private float material10_ambient[] = { 0.19125f, 0.0735f, 0.0225f, 1.0f };
private float material10_diffuse[] = { 0.7038f, 0.27048f, 0.0828f, 1.0f };
private float material10_specular[] = { 0.256777f, 0.137622f, 0.086014f, 1.0f };
private float material10_shininess = 12.8f;

//Gold
private float material11_ambient[] = { 0.24725f, 0.1995f, 0.0745f, 1.0f };
private float material11_diffuse[] = { 0.75164f, 0.60648f, 0.22648f, 1.0f };
private float material11_specular[] = { 0.628281f, 0.555802f, 0.366065f, 1.0f };
private float material11_shininess = 51.2f;

//Silver
private float material12_ambient[] = { 0.19225f, 0.19225f, 0.19225f, 1.0f };
private float material12_diffuse[] = { 0.50754f, 0.50754f, 0.50754f, 1.0f };
private float material12_specular[] = { 0.508273f, 0.508273f, 0.508273f, 1.0f };
private float material12_shininess = 51.2f;

//Black
private float material13_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
private float material13_diffuse[] = { 0.01f, 0.01f, 0.01f, 1.0f };
private float material13_specular[] = { 0.50f, 0.50f, 0.50f, 1.0f };
private float material13_shininess = 32.0f;

//Cyan
private float material14_ambient[] = { 0.0f, 0.1f, 0.06f, 1.0f };
private float material14_diffuse[] = { 0.0f, 0.50980392f, 0.50980392f, 1.0f };
private float material14_specular[] = { 0.50196078f, 0.50196078f, 0.50196078f, 1.0f };
private float material14_shininess =  32.0f;

//Green
private float material15_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
private float material15_diffuse[] = { 0.1f, 0.35f, 0.1f, 1.0f };
private float material15_specular[] = { 0.45f, 0.55f, 0.45f, 1.0f };
private float material15_shininess = 32.0f;

//Red
private float material16_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
private float material16_diffuse[] = { 0.5f, 0.0f, 0.0f, 1.0f };
private float material16_specular[] = { 0.7f, 0.6f, 0.6f, 1.0f };
private float material16_shininess = 32.0f;

//White
private float material17_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
private float material17_diffuse[] = { 0.55f, 0.55f, 0.55f, 1.0f };
private float material17_specular[] = { 0.70f, 0.70f, 0.70f, 1.0f };
private float material17_shininess = 32.0f;

//Yellow Plastic
private float material18_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
private float material18_diffuse[] = { 0.5f, 0.5f, 0.0f, 1.0f };
private float material18_specular[] = { 0.60f, 0.60f, 0.50f, 1.0f };
private float material18_shininess = 32.0f;

//Black
private float material19_ambient[] = { 0.02f, 0.02f, 0.02f, 1.0f };
private float material19_diffuse[] = { 0.01f, 0.01f, 0.01f, 1.0f };
private float material19_specular[] = { 0.4f, 0.4f, 0.4f, 1.0f };
private float material19_shininess = 10.0f;

//Cyan
private float material20_ambient[] = { 0.0f, 0.05f, 0.05f, 1.0f };
private float material20_diffuse[] = { 0.4f, 0.5f, 0.5f, 1.0f };
private float material20_specular[] = { 0.04f, 0.7f, 0.7f, 1.0f };
private float material20_shininess = 10.0f;

//Green
private float material21_ambient[] = { 0.0f, 0.05f, 0.0f, 1.0f };
private float material21_diffuse[] = { 0.4f, 0.5f, 0.4f, 1.0f };
private float material21_specular[] = { 0.04f, 0.7f, 0.04f, 1.0f };
private float material21_shininess = 10.0f;

//Red
private float material22_ambient[] = { 0.05f, 0.0f, 0.0f, 1.0f };
private float material22_diffuse[] = { 0.5f, 0.4f, 0.4f, 1.0f };
private float material22_specular[] = { 0.7f, 0.04f, 0.04f, 1.0f };
private float material22_shininess = 10.0f;

//White
private float material23_ambient[] = { 0.05f, 0.05f, 0.05f, 1.0f };
private float material23_diffuse[] = { 0.5f, 0.5f, 0.5f, 1.0f };
private float material23_specular[] = { 0.7f, 0.7f, 0.7f, 1.0f };
private float material23_shininess = 10.0f;

//Yellow Rubber
private float material24_ambient[] = { 0.05f, 0.05f, 0.0f, 1.0f };
private float material24_diffuse[] = { 0.5f, 0.5f, 0.4f, 1.0f };
private float material24_specular[] = { 0.7f, 0.7f, 0.04f, 1.0f };
private float material24_shininess = 10.0f;


private int gModelMatrixUniform, gViewMatrixUniform, gProjectionMatrixUniform;

private int gLdUniform;
private int gLaUniform;
private int gLsUniform;

private int gKa1Uniform;
private int gKd1Uniform;
private int gKs1Uniform;
private int gmaterial1_shininess_uniform;

private int glight_position_uniform;
private int gSphereNo;
	
	private int doubleTapUniform;

	private float perspectiveProjectionMatrix[] = new float[16];

	private int doubleTap;
	private int singleTap = 1;

	public ACView(Context drawingContext)
	{
		super(drawingContext);
		context = drawingContext;
		setEGLContextClientVersion(3);
		setRenderer(this);
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		gestureDetector = new GestureDetector(context, this, null, false);
		gestureDetector.setOnDoubleTapListener(this);
	}


@Override
public void onSurfaceCreated(GL10 gl, EGLConfig config)
{
	String glesVersion = gl.glGetString(GL10.GL_VERSION);
	System.out.println("OGLES: OpenGL-ES Version = "+glesVersion);
	String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
	System.out.println("OGLES: GLSL Version = "+glslVersion);
	System.out.println("OGLES: Before initialize call");
	initialize(gl);
}

@Override
public void onSurfaceChanged(GL10 unused, int width, int height)
{
	resize(width, height);
}

@Override
public void onDrawFrame(GL10 unused)
{
	display();
}

@Override
public boolean onTouchEvent(MotionEvent e)
{
	int eventaction = e.getAction();
	if(!gestureDetector.onTouchEvent(e))
		super.onTouchEvent(e);
	return(true);
}

@Override
public boolean onDoubleTap(MotionEvent e)
{
	doubleTap++;
	if(doubleTap > 1)
		doubleTap = 0;
	return(true);
}

@Override
public boolean onDoubleTapEvent(MotionEvent e)
{
	return(true);
}

@Override
public boolean onSingleTapConfirmed(MotionEvent e)
{
	singleTap++;
	if(singleTap > 3)
		singleTap = 1;
	return(true);
}

@Override
public boolean onDown(MotionEvent e)
{
	return(true);
}

@Override 
public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
{
	return(true);
}

@Override
public void onLongPress(MotionEvent e)
{

}

@Override
public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
{
	uninitialize();
	System.exit(0);
	return(true);
}

@Override 
public void onShowPress(MotionEvent e)
{

}

@Override
public boolean onSingleTapUp(MotionEvent e)
{
	return(true);
}

private void initialize(GL10 gl)
{
	System.out.println("OGLES: In Initialize : Begining");
	System.out.println("OGLES: In Initialize : Before Vertex Shader Compilation");

	gVertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
	final String vertexShaderSourceCode = String.format
	(
		"#version 310 es"+
		"\n"+
		"in vec4 vPosition;"+
		"in vec3 vNormal;"+
		"uniform mat4 u_model_matrix;"+
		"uniform mat4 u_view_matrix;"+
		"uniform mat4 u_projection_matrix;"+
		"uniform mediump int u_double_tap;"+
		"uniform vec4 u_light_position;"+
		"out vec3 transformed_normals;"+
		"out vec3 light_direction;"+
		"out vec3 viewer_vector;"+
		"void main(void)"+
		"{"+
		"if(u_double_tap == 1)"+
		"{"+
		"vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition;"+
		"transformed_normals = (mat3(u_view_matrix * u_model_matrix) * vNormal);"+
		"light_direction = (vec3(u_light_position) - eyeCoordinates.xyz);"+
		"viewer_vector = -eyeCoordinates.xyz;"+
		"}"+
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"+
		"}"
	);

	GLES32.glShaderSource(gVertexShaderObject, vertexShaderSourceCode);
	GLES32.glCompileShader(gVertexShaderObject);
	int[] iShaderCompiledStatus = new int[1];
	int[] iInfoLogLength = new int[1];
	String szInfoLog = null;
	GLES32.glGetShaderiv(gVertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
	if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
	{
		GLES32.glGetShaderiv(gVertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);

		if(iInfoLogLength[0] > 0)
		{
			szInfoLog = GLES32.glGetShaderInfoLog(gVertexShaderObject);
			System.out.println("OGLES: Vertex Shader Compilation Log = "+szInfoLog);
			uninitialize();
			System.exit(0);
		}
	}	

	System.out.println("OGLES: In Initialize : After Vertex Shader Compilation");
	System.out.println("OGLES: In Initialize : Before Fragment Shader Compilation");

	gFragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
	final String fragmentShaderSourceCode = String.format
	(
		"#version 310 es"+
		"\n"+
		"precision highp float;"+
		"in vec3 transformed_normals;"+
		"in vec3 light_direction;"+
		"in vec3 viewer_vector;"+
		"out vec4 FragColor;"+
		"uniform vec3 u_La;" +
		"uniform vec3 u_Ka1;" +
		"uniform vec3 u_Ld;" +
		"uniform vec3 u_Kd1;" +
		"uniform vec3 u_Ls;" +
		"uniform vec3 u_Ks1;" +
		"uniform float u_material1_shininess;" +
		"uniform int sphere;"+
		"uniform int u_double_tap;"+
		"void main(void)"+
		"{"+
		"vec3 phong_ads_color;"+
		"if(u_double_tap == 1)"+
		"{"+
		"vec3 normalized_transformed_normals = normalize(transformed_normals);" +
		"vec3 normalized_light_direction = normalize(light_direction);" +
		"vec3 normalized_viewer_vector = normalize(viewer_vector);" +
		"vec3 ambient = u_La * u_Ka1;" +
		"float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction), 0.0);" +
		"vec3 diffuse = u_Ld * u_Kd1 * tn_dot_ld;" +
		"vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);" +
		"vec3 specular = u_Ls * u_Ks1 * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material1_shininess);" +
		"phong_ads_color = ambient + diffuse + specular;" +
		"}"+
		"else"+
		"{"+
		"phong_ads_color = vec3(1.0 ,1.0, 1.0);"+
		"}"+
		"FragColor = vec4(phong_ads_color, 1.0);"+
		"}"
	);

	GLES32.glShaderSource(gFragmentShaderObject,fragmentShaderSourceCode);
	GLES32.glCompileShader(gFragmentShaderObject);
	iShaderCompiledStatus[0] = 0;
	iInfoLogLength[0] = 0;
	szInfoLog = null;
	GLES32.glGetShaderiv(gFragmentShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus, 0);
	if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
	{
		GLES32.glGetShaderiv(gFragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
		if(iInfoLogLength[0] > 0)
		{
			szInfoLog = GLES32.glGetShaderInfoLog(gFragmentShaderObject);
			System.out.println("OGLES: Fragment Shader Compilation Log = "+szInfoLog);
			uninitialize();
			System.exit(0);
		}
	}
	System.out.println("OGLES: In Initialize : After Fragment Shader Compilation");

	gShaderProgramObject = GLES32.glCreateProgram();
	GLES32.glAttachShader(gShaderProgramObject,gVertexShaderObject);
	GLES32.glAttachShader(gShaderProgramObject,gFragmentShaderObject);
	GLES32.glBindAttribLocation(gShaderProgramObject,GLESMacros.ARD_ATTRIBUTE_VERTEX,"vPosition");
	GLES32.glBindAttribLocation(gShaderProgramObject,GLESMacros.ARD_ATTRIBUTE_NORMAL, "vNormal");
	GLES32.glLinkProgram(gShaderProgramObject);
	int[] iShaderProgramLinkStatus = new int[1];
	iInfoLogLength[0] = 0;
	szInfoLog = null;
	GLES32.glGetProgramiv(gShaderProgramObject, GLES32.GL_LINK_STATUS, iShaderProgramLinkStatus, 0);
	if(iShaderProgramLinkStatus[0] == GLES32.GL_FALSE)
	{
		GLES32.glGetProgramiv(gShaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
		if(iInfoLogLength[0] > 0)
		{
			szInfoLog = GLES32.glGetProgramInfoLog(gShaderProgramObject);
			System.out.println("OGLES: Shader Program Link Log = "+szInfoLog);
			uninitialize();
			System.exit(0);
		}
	}

	System.out.println("OGLES: In Initialize : Before glGetUniformLocation");
	gModelMatrixUniform = GLES32.glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	gViewMatrixUniform = GLES32.glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	gProjectionMatrixUniform = GLES32.glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");

	//gLKeyPressedUniform = GLES32.glGetUniformLocation(gShaderProgramObject, "u_lighting_enabled");
	
	doubleTapUniform = GLES32.glGetUniformLocation(gShaderProgramObject, "u_double_tap");
	gLaUniform = GLES32.glGetUniformLocation(gShaderProgramObject, "u_La");
	gKa1Uniform = GLES32.glGetUniformLocation(gShaderProgramObject, "u_Ka1");
	
	gLdUniform = GLES32.glGetUniformLocation(gShaderProgramObject, "u_Ld");
	gKd1Uniform = GLES32.glGetUniformLocation(gShaderProgramObject, "u_Kd1");
	
	gLsUniform = GLES32.glGetUniformLocation(gShaderProgramObject, "u_Ls");
	gKs1Uniform = GLES32.glGetUniformLocation(gShaderProgramObject, "u_Ks1");
	
	glight_position_uniform = GLES32.glGetUniformLocation(gShaderProgramObject, "u_light_position");

	gmaterial1_shininess_uniform = GLES32.glGetUniformLocation(gShaderProgramObject, "u_material1_shininess");
	
	System.out.println("In Initialize : After glGetUniformLocation");

	Sphere sphere=new Sphere();
    float sphere_vertices[]=new float[1146];
    float sphere_normals[]=new float[1146];
    float sphere_textures[]=new float[764];
    short sphere_elements[]=new short[2280];

    sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
    numVertices = sphere.getNumberOfSphereVertices();
    numElements = sphere.getNumberOfSphereElements();
	
	 // vao
        GLES32.glGenVertexArrays(1,vao_sphere,0);
        GLES32.glBindVertexArray(vao_sphere[0]);
        
        // position vbo
        GLES32.glGenBuffers(1,vbo_sphere_position,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_sphere_position[0]);
        
        ByteBuffer byteBuffer=ByteBuffer.allocateDirect(sphere_vertices.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_vertices);
        verticesBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            sphere_vertices.length * 4,
                            verticesBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glVertexAttribPointer(GLESMacros.ARD_ATTRIBUTE_VERTEX,
                                     3,
                                     GLES32.GL_FLOAT,
                                     false,0,0);
        
        GLES32.glEnableVertexAttribArray(GLESMacros.ARD_ATTRIBUTE_VERTEX);
        
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
        
        // normal vbo
        GLES32.glGenBuffers(1,vbo_sphere_normal,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_sphere_normal[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(sphere_normals.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_normals);
        verticesBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            sphere_normals.length * 4,
                            verticesBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glVertexAttribPointer(GLESMacros.ARD_ATTRIBUTE_NORMAL,
                                     3,
                                     GLES32.GL_FLOAT,
                                     false,0,0);
        
        GLES32.glEnableVertexAttribArray(GLESMacros.ARD_ATTRIBUTE_NORMAL);
        
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
        
        // element vbo
        GLES32.glGenBuffers(1,vbo_sphere_element,0);
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(sphere_elements.length * 2);
        byteBuffer.order(ByteOrder.nativeOrder());
        ShortBuffer elementsBuffer=byteBuffer.asShortBuffer();
        elementsBuffer.put(sphere_elements);
        elementsBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER,
                            sphere_elements.length * 2,
                            elementsBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,0);

        GLES32.glBindVertexArray(0);


	GLES32.glEnable(GLES32.GL_DEPTH_TEST);
	GLES32.glDepthFunc(GLES32.GL_LEQUAL);
	//GLES32.glEnable(GLES32.GL_CULL_FACE);
	GLES32.glClearColor(0.5f, 0.5f, 0.5f, 0.0f);

	doubleTap = 0;

	Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
	System.out.println("OGLES: In Initialize : End");
}	

private void resize(int width, int height)
{
	if(height == 0)
		height = 1;
	GLES32.glViewport(0, 0, width, height);
	Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width / (float)height, 0.1f, 100.0f);
}

public void display()
{
	GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
	GLES32.glUseProgram(gShaderProgramObject);

	if(doubleTap == 1)
	{
		GLES32.glUniform1i(doubleTapUniform, 1);
		GLES32.glUniform3fv(gLaUniform, 1, light_ambient, 0);
		GLES32.glUniform3fv(gLdUniform, 1, light_diffuse, 0);
		GLES32.glUniform3fv(gLsUniform, 1, light_specular, 0);
	
		

		if (singleTap == 1)
		{
			lightPosition[0] = 0.0f;
			lightPosition[1] = 100 * (float)(Math.sin(angleX_axis));
			lightPosition[2] = 100 * (float)(Math.cos(angleX_axis));
			lightPosition[3] = 1.0f;
		}

		if (singleTap == 2)
		{
			lightPosition[0] = 100 * (float)(Math.cos(angleY_axis));
			lightPosition[1] = 0.0f;
			lightPosition[2] = 100 * (float)(Math.sin(angleY_axis));
			lightPosition[3] = 1.0f;
		}

		if (singleTap == 3)
		{
			lightPosition[0] = 100 * (float)(Math.cos(angleZ_axis));
			lightPosition[1] = 100 * (float)(Math.sin(angleZ_axis));
			lightPosition[2] = 0.0f;
			lightPosition[3] = 1.0f;
		}

		GLES32.glUniform4fv(glight_position_uniform, 1, lightPosition, 0);
	}

	else
	{
		GLES32.glUniform1i(doubleTapUniform, 0);
	}

	float modelMatrix[] = new float[16];
	float viewMatrix[] = new float[16];
	//float[] scaleMatrix = new float[] { 0.50f, 0.50f, 0.50f };
/////////////////////////////////////////////////////1st sphere/////////////////////////////////////////
	Matrix.setIdentityM(modelMatrix, 0);
	Matrix.setIdentityM(viewMatrix, 0);
	
	Matrix.translateM(modelMatrix, 0, -3.0f, 2.0f, -6.0f);
	Matrix.scaleM(modelMatrix, 0, 0.75f, 0.75f, 0.75f);
	//Matrix.rotateM(rotationMatrix, 0, angle_cube, 1.0f, 1.0f, 1.0f);
	//Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);
	GLES32.glUniformMatrix4fv(gModelMatrixUniform, 1, false, modelMatrix, 0);
	GLES32.glUniformMatrix4fv(gViewMatrixUniform, 1, false, viewMatrix, 0);
	
	GLES32.glUniform3fv(gKa1Uniform, 1, material1_ambient, 0);
	GLES32.glUniform3fv(gKd1Uniform, 1, material1_diffuse, 0);
	GLES32.glUniform3fv(gKs1Uniform, 1, material1_specular, 0);
	GLES32.glUniform1f(gmaterial1_shininess_uniform, material1_shininess);
	GLES32.glUniformMatrix4fv(gProjectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
	GLES32.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
    GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
    GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
    GLES32.glBindVertexArray(0);

  ////////////////////////////2nd Sphere////////////////////////////////////////////

    Matrix.setIdentityM(modelMatrix, 0);
	Matrix.setIdentityM(viewMatrix, 0);
	
	Matrix.translateM(modelMatrix, 0, -1.0f, 2.0f, -6.0f);
	Matrix.scaleM(modelMatrix, 0, 0.75f, 0.75f, 0.75f);
	//Matrix.rotateM(rotationMatrix, 0, angle_cube, 1.0f, 1.0f, 1.0f);
	//Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);
	GLES32.glUniformMatrix4fv(gModelMatrixUniform, 1, false, modelMatrix, 0);
	GLES32.glUniformMatrix4fv(gViewMatrixUniform, 1, false, viewMatrix, 0);

	GLES32.glUniform3fv(gKa1Uniform, 1, material2_ambient, 0);
	GLES32.glUniform3fv(gKd1Uniform, 1, material2_diffuse, 0);
	GLES32.glUniform3fv(gKs1Uniform, 1, material2_specular, 0);
	GLES32.glUniform1f(gmaterial1_shininess_uniform, material2_shininess);
	GLES32.glUniformMatrix4fv(gProjectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
	GLES32.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
    GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
    GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
    GLES32.glBindVertexArray(0);




////////////////////////////3nd Sphere////////////////////////////////////////////

    Matrix.setIdentityM(modelMatrix, 0);
	Matrix.setIdentityM(viewMatrix, 0);
	
	Matrix.translateM(modelMatrix, 0, 1.0f, 2.0f, -6.0f);
	Matrix.scaleM(modelMatrix, 0, 0.75f, 0.75f, 0.75f);
	//Matrix.rotateM(rotationMatrix, 0, angle_cube, 1.0f, 1.0f, 1.0f);
	//Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);
	GLES32.glUniformMatrix4fv(gModelMatrixUniform, 1, false, modelMatrix, 0);
	GLES32.glUniformMatrix4fv(gViewMatrixUniform, 1, false, viewMatrix, 0);
	//GLES32.glUniform1i(gSphereNo, 3);
	GLES32.glUniform3fv(gKa1Uniform, 1, material3_ambient, 0);
	GLES32.glUniform3fv(gKd1Uniform, 1, material3_diffuse, 0);
	GLES32.glUniform3fv(gKs1Uniform, 1, material3_specular, 0);
	GLES32.glUniform1f(gmaterial1_shininess_uniform, material3_shininess);
	GLES32.glUniformMatrix4fv(gProjectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
	GLES32.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
    GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
    GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
    GLES32.glBindVertexArray(0);



////////////////////////////4th Sphere////////////////////////////////////////////

    Matrix.setIdentityM(modelMatrix, 0);
	Matrix.setIdentityM(viewMatrix, 0);
	
	Matrix.translateM(modelMatrix, 0, 3.0f, 2.0f, -6.0f);
	Matrix.scaleM(modelMatrix, 0, 0.75f, 0.75f, 0.75f);
	//Matrix.rotateM(rotationMatrix, 0, angle_cube, 1.0f, 1.0f, 1.0f);
	//Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);
	GLES32.glUniformMatrix4fv(gModelMatrixUniform, 1, false, modelMatrix, 0);
	GLES32.glUniformMatrix4fv(gViewMatrixUniform, 1, false, viewMatrix, 0);
	//GLES32.glUniform1i(gSphereNo, 4);
	GLES32.glUniform3fv(gKa1Uniform, 1, material4_ambient, 0);
	GLES32.glUniform3fv(gKd1Uniform, 1, material4_diffuse, 0);
	GLES32.glUniform3fv(gKs1Uniform, 1, material4_specular, 0);
	GLES32.glUniform1f(gmaterial1_shininess_uniform, material4_shininess);
	GLES32.glUniformMatrix4fv(gProjectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
	GLES32.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
    GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
    GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
    GLES32.glBindVertexArray(0);

   ////////////////////////////5th Sphere////////////////////////////////////////////

    Matrix.setIdentityM(modelMatrix, 0);
	Matrix.setIdentityM(viewMatrix, 0);
	
	Matrix.translateM(modelMatrix, 0, -3.0f, 1.2f, -6.0f);
	Matrix.scaleM(modelMatrix, 0, 0.75f, 0.75f, 0.75f);
	//Matrix.rotateM(rotationMatrix, 0, angle_cube, 1.0f, 1.0f, 1.0f);
	//Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);
	GLES32.glUniformMatrix4fv(gModelMatrixUniform, 1, false, modelMatrix, 0);
	GLES32.glUniformMatrix4fv(gViewMatrixUniform, 1, false, viewMatrix, 0);
	//GLES32.glUniform1i(gSphereNo, 5);
	GLES32.glUniform3fv(gKa1Uniform, 1, material5_ambient, 0);
	GLES32.glUniform3fv(gKd1Uniform, 1, material5_diffuse, 0);
	GLES32.glUniform3fv(gKs1Uniform, 1, material5_specular, 0);
	GLES32.glUniform1f(gmaterial1_shininess_uniform, material5_shininess);
	GLES32.glUniformMatrix4fv(gProjectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
	GLES32.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
    GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
    GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
    GLES32.glBindVertexArray(0);

////////////////////////////6th Sphere////////////////////////////////////////////

    Matrix.setIdentityM(modelMatrix, 0);
	Matrix.setIdentityM(viewMatrix, 0);
	
	Matrix.translateM(modelMatrix, 0, -1.0f, 1.2f, -6.0f);
	Matrix.scaleM(modelMatrix, 0, 0.75f, 0.75f, 0.75f);
	//Matrix.rotateM(rotationMatrix, 0, angle_cube, 1.0f, 1.0f, 1.0f);
	//Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);
	GLES32.glUniformMatrix4fv(gModelMatrixUniform, 1, false, modelMatrix, 0);
	GLES32.glUniformMatrix4fv(gViewMatrixUniform, 1, false, viewMatrix, 0);
	//GLES32.glUniform1i(gSphereNo, 6);
	GLES32.glUniform3fv(gKa1Uniform, 1, material6_ambient, 0);
	GLES32.glUniform3fv(gKd1Uniform, 1, material6_diffuse, 0);
	GLES32.glUniform3fv(gKs1Uniform, 1, material6_specular, 0);
	GLES32.glUniform1f(gmaterial1_shininess_uniform, material6_shininess);
	GLES32.glUniformMatrix4fv(gProjectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
	GLES32.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
    GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
    GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
    GLES32.glBindVertexArray(0);

////////////////////////////7th Sphere////////////////////////////////////////////

    Matrix.setIdentityM(modelMatrix, 0);
	Matrix.setIdentityM(viewMatrix, 0);
	
	Matrix.translateM(modelMatrix, 0, 1.0f, 1.2f, -6.0f);
	Matrix.scaleM(modelMatrix, 0, 0.75f, 0.75f, 0.75f);
	//Matrix.rotateM(rotationMatrix, 0, angle_cube, 1.0f, 1.0f, 1.0f);
	//Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);
	GLES32.glUniformMatrix4fv(gModelMatrixUniform, 1, false, modelMatrix, 0);
	GLES32.glUniformMatrix4fv(gViewMatrixUniform, 1, false, viewMatrix, 0);
	//GLES32.glUniform1i(gSphereNo, 7);
	GLES32.glUniform3fv(gKa1Uniform, 1, material7_ambient, 0);
	GLES32.glUniform3fv(gKd1Uniform, 1, material7_diffuse, 0);
	GLES32.glUniform3fv(gKs1Uniform, 1, material7_specular, 0);
	GLES32.glUniform1f(gmaterial1_shininess_uniform, material7_shininess);
	GLES32.glUniformMatrix4fv(gProjectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
	GLES32.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
    GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
    GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
    GLES32.glBindVertexArray(0);

////////////////////////////8th Sphere////////////////////////////////////////////

    Matrix.setIdentityM(modelMatrix, 0);
	Matrix.setIdentityM(viewMatrix, 0);
	
	Matrix.translateM(modelMatrix, 0, 3.0f, 1.2f, -6.0f);
	Matrix.scaleM(modelMatrix, 0, 0.75f, 0.75f, 0.75f);
	//Matrix.rotateM(rotationMatrix, 0, angle_cube, 1.0f, 1.0f, 1.0f);
	//Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);
	GLES32.glUniformMatrix4fv(gModelMatrixUniform, 1, false, modelMatrix, 0);
	GLES32.glUniformMatrix4fv(gViewMatrixUniform, 1, false, viewMatrix, 0);
	//GLES32.glUniform1i(gSphereNo, 8);
	GLES32.glUniform3fv(gKa1Uniform, 1, material8_ambient, 0);
	GLES32.glUniform3fv(gKd1Uniform, 1, material8_diffuse, 0);
	GLES32.glUniform3fv(gKs1Uniform, 1, material8_specular, 0);
	GLES32.glUniform1f(gmaterial1_shininess_uniform, material8_shininess);
	GLES32.glUniformMatrix4fv(gProjectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
	GLES32.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
    GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
    GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
    GLES32.glBindVertexArray(0);





////////////////////////////9th Sphere////////////////////////////////////////////

    Matrix.setIdentityM(modelMatrix, 0);
	Matrix.setIdentityM(viewMatrix, 0);
	
	Matrix.translateM(modelMatrix, 0, -3.0f, 0.4f, -6.0f);
	Matrix.scaleM(modelMatrix, 0, 0.75f, 0.75f, 0.75f);
	//Matrix.rotateM(rotationMatrix, 0, angle_cube, 1.0f, 1.0f, 1.0f);
	//Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);
	GLES32.glUniformMatrix4fv(gModelMatrixUniform, 1, false, modelMatrix, 0);
	GLES32.glUniformMatrix4fv(gViewMatrixUniform, 1, false, viewMatrix, 0);
	//GLES32.glUniform1i(gSphereNo, 9);
	GLES32.glUniform3fv(gKa1Uniform, 1, material9_ambient, 0);
	GLES32.glUniform3fv(gKd1Uniform, 1, material9_diffuse, 0);
	GLES32.glUniform3fv(gKs1Uniform, 1, material9_specular, 0);
	GLES32.glUniform1f(gmaterial1_shininess_uniform, material9_shininess);
	GLES32.glUniformMatrix4fv(gProjectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
	GLES32.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
    GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
    GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
    GLES32.glBindVertexArray(0);


////////////////////////////10th Sphere////////////////////////////////////////////

    Matrix.setIdentityM(modelMatrix, 0);
	Matrix.setIdentityM(viewMatrix, 0);
	
	Matrix.translateM(modelMatrix, 0, -1.0f, 0.4f, -6.0f);
	Matrix.scaleM(modelMatrix, 0, 0.75f, 0.75f, 0.75f);
	//Matrix.rotateM(rotationMatrix, 0, angle_cube, 1.0f, 1.0f, 1.0f);
	//Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);
	GLES32.glUniformMatrix4fv(gModelMatrixUniform, 1, false, modelMatrix, 0);
	GLES32.glUniformMatrix4fv(gViewMatrixUniform, 1, false, viewMatrix, 0);
	//GLES32.glUniform1i(gSphereNo, 10);
	GLES32.glUniform3fv(gKa1Uniform, 1, material10_ambient, 0);
	GLES32.glUniform3fv(gKd1Uniform, 1, material10_diffuse, 0);
	GLES32.glUniform3fv(gKs1Uniform, 1, material10_specular, 0);
	GLES32.glUniform1f(gmaterial1_shininess_uniform, material10_shininess);
	GLES32.glUniformMatrix4fv(gProjectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
	GLES32.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
    GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
    GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
    GLES32.glBindVertexArray(0);

////////////////////////////11th Sphere////////////////////////////////////////////

    Matrix.setIdentityM(modelMatrix, 0);
	Matrix.setIdentityM(viewMatrix, 0);
	
	Matrix.translateM(modelMatrix, 0, 1.0f, 0.4f, -6.0f);
	Matrix.scaleM(modelMatrix, 0, 0.75f, 0.75f, 0.75f);
	//Matrix.rotateM(rotationMatrix, 0, angle_cube, 1.0f, 1.0f, 1.0f);
	//Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);
	GLES32.glUniformMatrix4fv(gModelMatrixUniform, 1, false, modelMatrix, 0);
	GLES32.glUniformMatrix4fv(gViewMatrixUniform, 1, false, viewMatrix, 0);
	//GLES32.glUniform1i(gSphereNo, 11);
	GLES32.glUniform3fv(gKa1Uniform, 1, material11_ambient, 0);
	GLES32.glUniform3fv(gKd1Uniform, 1, material11_diffuse, 0);
	GLES32.glUniform3fv(gKs1Uniform, 1, material11_specular, 0);
	GLES32.glUniform1f(gmaterial1_shininess_uniform, material11_shininess);
	GLES32.glUniformMatrix4fv(gProjectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
	GLES32.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
    GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
    GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
    GLES32.glBindVertexArray(0);


////////////////////////////12th Sphere////////////////////////////////////////////

    Matrix.setIdentityM(modelMatrix, 0);
	Matrix.setIdentityM(viewMatrix, 0);
	
	Matrix.translateM(modelMatrix, 0, 3.0f, 0.4f, -6.0f);
	Matrix.scaleM(modelMatrix, 0, 0.75f, 0.75f, 0.75f);
	//Matrix.rotateM(rotationMatrix, 0, angle_cube, 1.0f, 1.0f, 1.0f);
	//Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);
	GLES32.glUniformMatrix4fv(gModelMatrixUniform, 1, false, modelMatrix, 0);
	GLES32.glUniformMatrix4fv(gViewMatrixUniform, 1, false, viewMatrix, 0);
	//GLES32.glUniform1i(gSphereNo, 12);
	GLES32.glUniform3fv(gKa1Uniform, 1, material12_ambient, 0);
	GLES32.glUniform3fv(gKd1Uniform, 1, material12_diffuse, 0);
	GLES32.glUniform3fv(gKs1Uniform, 1, material12_specular, 0);
	GLES32.glUniform1f(gmaterial1_shininess_uniform, material12_shininess);
	GLES32.glUniformMatrix4fv(gProjectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
	GLES32.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
    GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
    GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
    GLES32.glBindVertexArray(0);


 ////////////////////////////13th Sphere////////////////////////////////////////////

    Matrix.setIdentityM(modelMatrix, 0);
	Matrix.setIdentityM(viewMatrix, 0);
	
	Matrix.translateM(modelMatrix, 0, -3.0f, -0.4f, -6.0f);
	Matrix.scaleM(modelMatrix, 0, 0.75f, 0.75f, 0.75f);
	//Matrix.rotateM(rotationMatrix, 0, angle_cube, 1.0f, 1.0f, 1.0f);
	//Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);
	GLES32.glUniformMatrix4fv(gModelMatrixUniform, 1, false, modelMatrix, 0);
	GLES32.glUniformMatrix4fv(gViewMatrixUniform, 1, false, viewMatrix, 0);
	//GLES32.glUniform1i(gSphereNo, 13);
	GLES32.glUniform3fv(gKa1Uniform, 1, material13_ambient, 0);
	GLES32.glUniform3fv(gKd1Uniform, 1, material13_diffuse, 0);
	GLES32.glUniform3fv(gKs1Uniform, 1, material13_specular, 0);
	GLES32.glUniform1f(gmaterial1_shininess_uniform, material13_shininess);
	GLES32.glUniformMatrix4fv(gProjectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
	GLES32.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
    GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
    GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
    GLES32.glBindVertexArray(0);


////////////////////////////14th Sphere////////////////////////////////////////////

    Matrix.setIdentityM(modelMatrix, 0);
	Matrix.setIdentityM(viewMatrix, 0);
	
	Matrix.translateM(modelMatrix, 0, -1.0f, -0.4f, -6.0f);
	Matrix.scaleM(modelMatrix, 0, 0.75f, 0.75f, 0.75f);
	//Matrix.rotateM(rotationMatrix, 0, angle_cube, 1.0f, 1.0f, 1.0f);
	//Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);
	GLES32.glUniformMatrix4fv(gModelMatrixUniform, 1, false, modelMatrix, 0);
	GLES32.glUniformMatrix4fv(gViewMatrixUniform, 1, false, viewMatrix, 0);
	//GLES32.glUniform1i(gSphereNo, 14);
	GLES32.glUniform3fv(gKa1Uniform, 1, material14_ambient, 0);
	GLES32.glUniform3fv(gKd1Uniform, 1, material14_diffuse, 0);
	GLES32.glUniform3fv(gKs1Uniform, 1, material14_specular, 0);
	GLES32.glUniform1f(gmaterial1_shininess_uniform, material14_shininess);
	GLES32.glUniformMatrix4fv(gProjectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
	GLES32.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
    GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
    GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
    GLES32.glBindVertexArray(0);



 ////////////////////////////15th Sphere////////////////////////////////////////////

    Matrix.setIdentityM(modelMatrix, 0);
	Matrix.setIdentityM(viewMatrix, 0);
	
	Matrix.translateM(modelMatrix, 0, 1.0f, -0.4f, -6.0f);
	Matrix.scaleM(modelMatrix, 0, 0.75f, 0.75f, 0.75f);
	//Matrix.rotateM(rotationMatrix, 0, angle_cube, 1.0f, 1.0f, 1.0f);
	//Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);
	GLES32.glUniformMatrix4fv(gModelMatrixUniform, 1, false, modelMatrix, 0);
	GLES32.glUniformMatrix4fv(gViewMatrixUniform, 1, false, viewMatrix, 0);
	//GLES32.glUniform1i(gSphereNo, 15);
	GLES32.glUniform3fv(gKa1Uniform, 1, material15_ambient, 0);
	GLES32.glUniform3fv(gKd1Uniform, 1, material15_diffuse, 0);
	GLES32.glUniform3fv(gKs1Uniform, 1, material15_specular, 0);
	GLES32.glUniform1f(gmaterial1_shininess_uniform, material15_shininess);
	GLES32.glUniformMatrix4fv(gProjectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
	GLES32.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
    GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
    GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
    GLES32.glBindVertexArray(0);



  ////////////////////////////16th Sphere////////////////////////////////////////////

    Matrix.setIdentityM(modelMatrix, 0);
	Matrix.setIdentityM(viewMatrix, 0);
	
	Matrix.translateM(modelMatrix, 0, 3.0f, -0.4f, -6.0f);
	Matrix.scaleM(modelMatrix, 0, 0.75f, 0.75f, 0.75f);
	//Matrix.rotateM(rotationMatrix, 0, angle_cube, 1.0f, 1.0f, 1.0f);
	//Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);
	GLES32.glUniformMatrix4fv(gModelMatrixUniform, 1, false, modelMatrix, 0);
	GLES32.glUniformMatrix4fv(gViewMatrixUniform, 1, false, viewMatrix, 0);
	//GLES32.glUniform1i(gSphereNo, 16);
	GLES32.glUniform3fv(gKa1Uniform, 1, material16_ambient, 0);
	GLES32.glUniform3fv(gKd1Uniform, 1, material16_diffuse, 0);
	GLES32.glUniform3fv(gKs1Uniform, 1, material16_specular, 0);
	GLES32.glUniform1f(gmaterial1_shininess_uniform, material16_shininess);
	GLES32.glUniformMatrix4fv(gProjectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
	GLES32.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
    GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
    GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
    GLES32.glBindVertexArray(0);




////////////////////////////17th Sphere////////////////////////////////////////////

    Matrix.setIdentityM(modelMatrix, 0);
	Matrix.setIdentityM(viewMatrix, 0);
	
	Matrix.translateM(modelMatrix, 0, -3.0f, -1.2f, -6.0f);
	Matrix.scaleM(modelMatrix, 0, 0.75f, 0.75f, 0.75f);
	//Matrix.rotateM(rotationMatrix, 0, angle_cube, 1.0f, 1.0f, 1.0f);
	//Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);
	GLES32.glUniformMatrix4fv(gModelMatrixUniform, 1, false, modelMatrix, 0);
	GLES32.glUniformMatrix4fv(gViewMatrixUniform, 1, false, viewMatrix, 0);
	//GLES32.glUniform1i(gSphereNo, 17);
	GLES32.glUniform3fv(gKa1Uniform, 1, material17_ambient, 0);
	GLES32.glUniform3fv(gKd1Uniform, 1, material17_diffuse, 0);
	GLES32.glUniform3fv(gKs1Uniform, 1, material17_specular, 0);
	GLES32.glUniform1f(gmaterial1_shininess_uniform, material17_shininess);
	GLES32.glUniformMatrix4fv(gProjectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
	GLES32.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
    GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
    GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
    GLES32.glBindVertexArray(0);

////////////////////////////18th Sphere////////////////////////////////////////////

    Matrix.setIdentityM(modelMatrix, 0);
	Matrix.setIdentityM(viewMatrix, 0);
	
	Matrix.translateM(modelMatrix, 0, -1.0f, -1.2f, -6.0f);
	Matrix.scaleM(modelMatrix, 0, 0.75f, 0.75f, 0.75f);
	//Matrix.rotateM(rotationMatrix, 0, angle_cube, 1.0f, 1.0f, 1.0f);
	//Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);
	GLES32.glUniformMatrix4fv(gModelMatrixUniform, 1, false, modelMatrix, 0);
	GLES32.glUniformMatrix4fv(gViewMatrixUniform, 1, false, viewMatrix, 0);
	//GLES32.glUniform1i(gSphereNo, 18);
	GLES32.glUniform3fv(gKa1Uniform, 1, material18_ambient, 0);
	GLES32.glUniform3fv(gKd1Uniform, 1, material18_diffuse, 0);
	GLES32.glUniform3fv(gKs1Uniform, 1, material18_specular, 0);
	GLES32.glUniform1f(gmaterial1_shininess_uniform, material18_shininess);
	GLES32.glUniformMatrix4fv(gProjectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
	GLES32.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
    GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
    GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
    GLES32.glBindVertexArray(0);


 ////////////////////////////19th Sphere////////////////////////////////////////////

    Matrix.setIdentityM(modelMatrix, 0);
	Matrix.setIdentityM(viewMatrix, 0);
	
	Matrix.translateM(modelMatrix, 0, 1.0f, -1.2f, -6.0f);
	Matrix.scaleM(modelMatrix, 0, 0.75f, 0.75f, 0.75f);
	//Matrix.rotateM(rotationMatrix, 0, angle_cube, 1.0f, 1.0f, 1.0f);
	//Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);
	GLES32.glUniformMatrix4fv(gModelMatrixUniform, 1, false, modelMatrix, 0);
	GLES32.glUniformMatrix4fv(gViewMatrixUniform, 1, false, viewMatrix, 0);
	//GLES32.glUniform1i(gSphereNo, 19);
	GLES32.glUniform3fv(gKa1Uniform, 1, material19_ambient, 0);
	GLES32.glUniform3fv(gKd1Uniform, 1, material19_diffuse, 0);
	GLES32.glUniform3fv(gKs1Uniform, 1, material19_specular, 0);
	GLES32.glUniform1f(gmaterial1_shininess_uniform, material19_shininess);
	GLES32.glUniformMatrix4fv(gProjectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
	GLES32.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
    GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
    GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
    GLES32.glBindVertexArray(0);


////////////////////////////20th Sphere////////////////////////////////////////////

    Matrix.setIdentityM(modelMatrix, 0);
	Matrix.setIdentityM(viewMatrix, 0);
	
	Matrix.translateM(modelMatrix, 0, 3.0f, -1.2f, -6.0f);
	Matrix.scaleM(modelMatrix, 0, 0.75f, 0.75f, 0.75f);
	//Matrix.rotateM(rotationMatrix, 0, angle_cube, 1.0f, 1.0f, 1.0f);
	//Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);
	GLES32.glUniformMatrix4fv(gModelMatrixUniform, 1, false, modelMatrix, 0);
	GLES32.glUniformMatrix4fv(gViewMatrixUniform, 1, false, viewMatrix, 0);
//	GLES32.glUniform1i(gSphereNo, 20);
	GLES32.glUniform3fv(gKa1Uniform, 1, material20_ambient, 0);
	GLES32.glUniform3fv(gKd1Uniform, 1, material20_diffuse, 0);
	GLES32.glUniform3fv(gKs1Uniform, 1, material20_specular, 0);
	GLES32.glUniform1f(gmaterial1_shininess_uniform, material20_shininess);
	GLES32.glUniformMatrix4fv(gProjectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
	GLES32.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
    GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
    GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
    GLES32.glBindVertexArray(0);

////////////////////////////21st Sphere////////////////////////////////////////////

    Matrix.setIdentityM(modelMatrix, 0);
	Matrix.setIdentityM(viewMatrix, 0);
	
	Matrix.translateM(modelMatrix, 0, -3.0f, -2.0f, -6.0f);
	Matrix.scaleM(modelMatrix, 0, 0.75f, 0.75f, 0.75f);
	//Matrix.rotateM(rotationMatrix, 0, angle_cube, 1.0f, 1.0f, 1.0f);
	//Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);
	GLES32.glUniformMatrix4fv(gModelMatrixUniform, 1, false, modelMatrix, 0);
	GLES32.glUniformMatrix4fv(gViewMatrixUniform, 1, false, viewMatrix, 0);
	//GLES32.glUniform1i(gSphereNo, 21);
	GLES32.glUniform3fv(gKa1Uniform, 1, material21_ambient, 0);
	GLES32.glUniform3fv(gKd1Uniform, 1, material21_diffuse, 0);
	GLES32.glUniform3fv(gKs1Uniform, 1, material21_specular, 0);
	GLES32.glUniform1f(gmaterial1_shininess_uniform, material21_shininess);
	GLES32.glUniformMatrix4fv(gProjectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
	GLES32.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
    GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
    GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
    GLES32.glBindVertexArray(0);


 ////////////////////////////22nd Sphere////////////////////////////////////////////

    Matrix.setIdentityM(modelMatrix, 0);
	Matrix.setIdentityM(viewMatrix, 0);
	
	Matrix.translateM(modelMatrix, 0, -1.0f, -2.0f, -6.0f);
	Matrix.scaleM(modelMatrix, 0, 0.75f, 0.75f, 0.75f);
	//Matrix.rotateM(rotationMatrix, 0, angle_cube, 1.0f, 1.0f, 1.0f);
	//Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);
	GLES32.glUniformMatrix4fv(gModelMatrixUniform, 1, false, modelMatrix, 0);
	GLES32.glUniformMatrix4fv(gViewMatrixUniform, 1, false, viewMatrix, 0);
	//GLES32.glUniform1i(gSphereNo, 22);
	GLES32.glUniform3fv(gKa1Uniform, 1, material22_ambient, 0);
	GLES32.glUniform3fv(gKd1Uniform, 1, material22_diffuse, 0);
	GLES32.glUniform3fv(gKs1Uniform, 1, material22_specular, 0);
	GLES32.glUniform1f(gmaterial1_shininess_uniform, material22_shininess);
	GLES32.glUniformMatrix4fv(gProjectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
	GLES32.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
    GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
    GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
    GLES32.glBindVertexArray(0);

////////////////////////////23rd Sphere////////////////////////////////////////////

    Matrix.setIdentityM(modelMatrix, 0);
	Matrix.setIdentityM(viewMatrix, 0);
	
	Matrix.translateM(modelMatrix, 0, 1.0f, -2.0f, -6.0f);
	Matrix.scaleM(modelMatrix, 0, 0.75f, 0.75f, 0.75f);
	//Matrix.rotateM(rotationMatrix, 0, angle_cube, 1.0f, 1.0f, 1.0f);
	//Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);
	GLES32.glUniformMatrix4fv(gModelMatrixUniform, 1, false, modelMatrix, 0);
	GLES32.glUniformMatrix4fv(gViewMatrixUniform, 1, false, viewMatrix, 0);
	//GLES32.glUniform1i(gSphereNo, 23);
	GLES32.glUniform3fv(gKa1Uniform, 1, material23_ambient, 0);
	GLES32.glUniform3fv(gKd1Uniform, 1, material23_diffuse, 0);
	GLES32.glUniform3fv(gKs1Uniform, 1, material23_specular, 0);
	GLES32.glUniform1f(gmaterial1_shininess_uniform, material23_shininess);
	GLES32.glUniformMatrix4fv(gProjectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
	GLES32.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
    GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
    GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
    GLES32.glBindVertexArray(0);

////////////////////////////24th Sphere////////////////////////////////////////////

    Matrix.setIdentityM(modelMatrix, 0);
	Matrix.setIdentityM(viewMatrix, 0);
	
	Matrix.translateM(modelMatrix, 0, 3.0f, -2.0f, -6.0f);
	Matrix.scaleM(modelMatrix, 0, 0.75f, 0.75f, 0.75f);
	//Matrix.rotateM(rotationMatrix, 0, angle_cube, 1.0f, 1.0f, 1.0f);
	//Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);
	GLES32.glUniformMatrix4fv(gModelMatrixUniform, 1, false, modelMatrix, 0);
	GLES32.glUniformMatrix4fv(gViewMatrixUniform, 1, false, viewMatrix, 0);
	//GLES32.glUniform1i(gSphereNo, 24);
	GLES32.glUniform3fv(gKa1Uniform, 1, material24_ambient, 0);
	GLES32.glUniform3fv(gKd1Uniform, 1, material24_diffuse, 0);
	GLES32.glUniform3fv(gKs1Uniform, 1, material24_specular, 0);
	GLES32.glUniform1f(gmaterial1_shininess_uniform, material24_shininess);
	GLES32.glUniformMatrix4fv(gProjectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
	GLES32.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
    GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
    GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
    GLES32.glBindVertexArray(0);



	GLES32.glUseProgram(0);
	requestRender();
	spin();
}

void uninitialize()
{
	

	if(vao_sphere[0] != 0)
	{
		GLES32.glDeleteVertexArrays(1, vao_sphere, 0);
		vao_sphere[0] = 0;
	}

	if(vbo_sphere_position[0] != 0)
	{
		GLES32.glDeleteVertexArrays(1, vbo_sphere_position, 0);
		vbo_sphere_position[0] = 0;
	}

	if(vbo_sphere_normal[0] != 0)
	{
		GLES32.glDeleteVertexArrays(1, vbo_sphere_normal, 0);
		vbo_sphere_normal[0] = 0;
	}

	if(vbo_sphere_element[0] != 0)
	{
		GLES32.glDeleteVertexArrays(1, vbo_sphere_element, 0);
		vbo_sphere_element[0] = 0;
	}


	if(gShaderProgramObject != 0)
	{
		if(gVertexShaderObject != 0)
		{
			GLES32.glDetachShader(gShaderProgramObject, gVertexShaderObject);
			GLES32.glDeleteShader(gVertexShaderObject);
			gVertexShaderObject = 0;
		}

		if(gFragmentShaderObject != 0)
		{
			GLES32.glDetachShader(gShaderProgramObject, gFragmentShaderObject);
			GLES32.glDeleteShader(gFragmentShaderObject);
			gFragmentShaderObject = 0;
		}
	}

	if(gShaderProgramObject != 0)
	{
		GLES32.glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;
	}

}

void spin()
{

		angleX_axis = angleX_axis + 0.05f;

		if (angleX_axis >= 360.0f)
		{
			angleX_axis = angleX_axis - 360.0f;
		}
	

		angleY_axis = angleY_axis + 0.05f;

		if (angleY_axis >= 360.0f)
		{
			angleY_axis = angleY_axis - 360.0f;
		}
	
		angleZ_axis = angleZ_axis + 0.05f;

		if (angleZ_axis >= 360.0f)
		{
			angleZ_axis = angleZ_axis - 360.0f;
		}
	
}

}
