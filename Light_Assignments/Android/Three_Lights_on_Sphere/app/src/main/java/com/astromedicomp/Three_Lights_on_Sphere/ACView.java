						package com.astromedicomp.Three_Lights_on_Sphere;

						import android.content.Context; // for drawing context related
						import android.opengl.GLSurfaceView; // for OpenGL Surface View and all related
						import javax.microedition.khronos.opengles.GL10; // for OpenGLES 1.0 needed as param type GL10
						import javax.microedition.khronos.egl.EGLConfig; // for EGLConfig needed as param type EGLConfig
						import android.opengl.GLES32; // for OpenGLES 3.2
						import android.opengl.GLES10; // for OpenGLES 3.2
						import android.view.MotionEvent; // for "MotionEvent"
						import android.view.GestureDetector; // for GestureDetector
						import android.view.GestureDetector.OnGestureListener; // OnGestureListener
						import android.view.GestureDetector.OnDoubleTapListener; // for OnDoubleTapListener

						import java.nio.ByteBuffer;
						import java.nio.ByteOrder;
						import java.nio.FloatBuffer;
						import java.nio.ShortBuffer;
						import java.lang.Math;
						import android.opengl.Matrix;	//for Matrix math

						//for vbo
						public class ACView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
						{
							private final Context context;

							private GestureDetector gestureDetector;

							private int vertexShaderObject;
							private int fragmentShaderObject;
							private int shaderProgramObject;

							private int numElements;
							private int numVertices;
	
							private int[]  vao_sphere = new int[1];
							private int[] vbo_sphere_position = new int[1];
							private int[] vbo_sphere_normal = new int[1];
							private int[] vbo_sphere_element = new int[1];
							private float gAngleBlue=0.0f;
							private float gAngleRed=0.0f;
							private float gAngleGreen=0.0f;

							private float gAngleRadianceBlue = 0.0f;
							private float gAngleRadianceRed = 0.0f;
							private float gangleRadianceGreen = 0.0f;
							private float M_PI =(float) 3.1415926535;
							private float light_ambient[] = {0.0f,0.0f,0.0f,1.0f};
							private float light_diffuse[] = {0.0f,0.0f,1.0f,1.0f};
							private float light_specular[] = {0.0f,0.0f,1.0f,1.0f};
							private float light_position[] = { 0.0f,0.0f,100.0f,1.0f };
    
							private float light_ambientRed[] = {0.0f,0.0f,0.0f,1.0f};
							private float light_diffuseRed[] = {1.0f,0.0f,0.0f,1.0f};
							private float light_specularRed[] = {1.0f,0.0f,0.0f,1.0f};
							private float light_positionRed[] = { 100.0f,0.0f,0.0f,1.0f };
    
							private float light_ambientGreen[] = {0.0f,0.0f,0.0f,1.0f};
							private float light_diffuseGreen[] = {0.0f,1.0f,0.0f,1.0f};
							private float light_specularGreen[] = {0.0f,1.0f,0.0f,1.0f};
							private float light_positionGreen[] = { 0.0f,100.0f,0.0f,1.0f };


							private float material_ambient[] = {0.0f,0.0f,0.0f,1.0f};
							private float material_diffuse[] = {1.0f,1.0f,1.0f,1.0f};
							private float material_specular[] = {1.0f,1.0f,1.0f,1.0f};
							private float material_shininess = 50.0f;

							private int modelMatrixUniform, viewMatrixUniform, projectionMatrixUniform;
							private int laUniform, ldUniform, lsUniform, lightPositionUniform;
							private int kaUniform, kdUniform, ksUniform, materialShininessUniform;

							private int laUniformRed, ldUniformRed, lsUniformRed, lightPositionUniformRed;
							private int kaUniformRed, kdUniformRed, ksUniformRed, materialShininessUniformRed;

							private int laUniformGreen, ldUniformGreen, lsUniformGreen, lightPositionUniformGreen;
							private int kaUniformGreen, kdUniformGreen, ksUniformGreen;

							private int singleTapUniform;
							private int doubleTapUniform;

							private int doubleTap;
							private int singleTap;
		
							private float myArrRed[] = new float[4];
							private float myArrGreen[] = new float[4];
							private float myArrBlue[] = new float[4];

							private float perspectiveProjectionMatrix[]=new float[16];
	
							public ACView(Context drawingContext)	
							{
								super(drawingContext);

								context = drawingContext;

								//accordingly set EGLContext to current supported version of OpenGL-ES
								setEGLContextClientVersion(3);

								//set Renderer for drawing on GLSurfaceView
								setRenderer(this);

								//Render the view only when there is a change in the drawing data
								setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);		
		
								gestureDetector = new GestureDetector(context,this,null,false);
								gestureDetector.setOnDoubleTapListener(this); 
							}

							//Overriden method of GLSurfaceView.Renderer (Init code)
							@Override
							public void onSurfaceCreated(GL10 gl,EGLConfig config)
							{
								//get OpenGL - ES Version
								  String glesVersion = gl.glGetString(GL10.GL_VERSION);
								System.out.println("VDG: OpenGL-ES Version = "+glesVersion);   
								//get GLSL Version
								String	glslVersion=gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
								System.out.println("VDG: GLSL Version = "+glslVersion);
	
								initialize(gl);
							}

							//Overriden method for GLSurfaceView.Renderer(Change size code)
							@Override
							public void onSurfaceChanged(GL10 unused, int width,int height)
							{
								resize(width, height);
							}

							//Overriden method for GLSurfaceView.Renderer (Rendering code)
							@Override
							public void onDrawFrame(GL10 unused)
							{
								display();
							}

							//Handling 'onTouchEvent' Is The Most IMPORTANT
							//Because It Triggers All Gesture and Tap Events
							@Override
							public boolean onTouchEvent(MotionEvent e)
							{
								//code
								int eventaction = e.getAction();
								if(!gestureDetector.onTouchEvent(e))
									super.onTouchEvent(e);
								return(true);
							}

	
							//abstract method from OnDoubleTapListener so must be implemented
							@Override
							public boolean onDoubleTap(MotionEvent e)
							{
								return(true);
							}

							//abstract method from OnDoubleTapListener so must be implemented
							@Override
							public boolean onDoubleTapEvent(MotionEvent e)
							{
								doubleTap++;
								if(doubleTap > 1)
									doubleTap = 0;
								return(true);
							}

							//abstract method from OnDoubleTapListener so must be implemented
							 @Override
							public boolean onSingleTapConfirmed(MotionEvent e)
							{      
							 singleTap++;
							 if(singleTap > 2)
								singleTap = 0;
							 return(true);
							}

							//abstract method from OnGestureListener so must be implemented
							 @Override
 
							   public boolean onDown(MotionEvent e)
							{
							return(true);
							}

							//abstract method from OnGestureListener so must be implemented
							@Override
							public boolean onFling(MotionEvent e1,MotionEvent e2, float velocityX, float velocityY)
							{
								return(true);
							}

							//abstract method from OnGestureListener so must be implemented
							@Override
							public void onLongPress(MotionEvent e)
							{
							}

							//abstract method from OnGestureListener so must be implemented
							@Override
							public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
							{
								uninitialize();
								System.exit(0);
								return(true);
							}
	
							//abstract method from OnGestureListener so must be implemented
							@Override
							public void onShowPress(MotionEvent e)
							{
							}

							//abstract method from OnGestureListener so must be implemented
							@Override
							public boolean onSingleTapUp(MotionEvent e)
							{
								return(true);
							}

							private void initialize(GL10 gl)
							{
								//******VERTEX SHADER
								//create shader
								System.out.println("VDG: Inside Initialize");
								vertexShaderObject=GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

								//vertex shader source code
								final String vertexShaderSourceCode = String.format
								(
								"#version 320 es"+
								 "\n"+
								 "precision highp float;"+
								 "in vec4 vPosition;"+
								 "in vec3 vNormal;"+
								 "uniform mat4 u_model_matrix;"+
								 "uniform mat4 u_view_matrix;"+
								 "uniform mat4 u_projection_matrix;"+

								 "uniform vec3 u_La;" +
								 "uniform vec3 u_Ld;" +
								 "uniform vec3 u_Ls;" +
								 "uniform vec4 u_light_position;" +
								 "uniform vec3 u_Ka;" +
								 "uniform vec3 u_Kd;" +
								 "uniform vec3 u_Ks;" +

								  "uniform vec3 u_LaRed;" +
								 "uniform vec3 u_LdRed;" +
								 "uniform vec3 u_LsRed;" +
								 "uniform vec4 u_light_positionRed;" +
								 "uniform vec3 u_KaRed;" +
								 "uniform vec3 u_KdRed;" +
								 "uniform vec3 u_KsRed;" +

								 "uniform vec3 u_LaGreen;" +
								 "uniform vec3 u_LdGreen;" +
								 "uniform vec3 u_LsGreen;" +
								 "uniform vec4 u_light_positionGreen;" +
								 "uniform vec3 u_KaGreen;" +
								 "uniform vec3 u_KdGreen;" +
								 "uniform vec3 u_KsGreen;" +

								 "uniform float u_material_shininess;" +
								 "uniform int u_single_tap;" +
								 "uniform int u_double_tap;" +
								 "out vec3 viewer_vector;" +
								 "out vec3 transformed_normals;" +
								 "out vec3 light_direction;" +
 								 "out vec3 light_directionRed;" +
  								 "out vec3 light_directionGreen;" +

								 "out vec3 phong_ads_colorVer;" +
 								 "out vec3 phong_ads_colorRedVer;" +
								 "out vec3 phong_ads_colorGreenVer;" +

								 "void main(void)"+
								 "{"+
								 "if (u_double_tap == 1 )"+
								 "{"+
								 "vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;"+
								 "vec3 transformed_normals=normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);"+

								 "vec3 light_direction = normalize(vec3(u_light_position) - eye_coordinates.xyz);"+
								 "float tn_dot_ld = max(dot(transformed_normals, light_direction),0.0);"+
								 "vec3 ambient = u_La * u_Ka;"+
								 "vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;"+
								 "vec3 reflection_vector = reflect(-light_direction, transformed_normals);"+
								 "vec3 viewer_vector = normalize(-eye_coordinates.xyz);"+
								 "vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, viewer_vector), 0.0), u_material_shininess);"+
								 "phong_ads_colorVer=ambient + diffuse + specular;"+

								"vec3 light_directionRed = normalize(vec3(u_light_positionRed) - eye_coordinates.xyz);"+
								 "float tn_dot_ldRed = max(dot(transformed_normals, light_directionRed),0.0);"+
								 "vec3 ambientRed = u_LaRed * u_KaRed;"+
								 "vec3 diffuseRed = u_LdRed * u_KdRed * tn_dot_ldRed;"+
								 "vec3 reflection_vectorRed = reflect(-light_directionRed, transformed_normals);"+
								 "vec3 specularRed = u_LsRed * u_KsRed * pow(max(dot(reflection_vectorRed, viewer_vector), 0.0), u_material_shininess);"+
								 "phong_ads_colorRedVer=ambientRed + diffuseRed + specularRed;"+

								 "vec3 light_directionGreen = normalize(vec3(u_light_positionGreen) - eye_coordinates.xyz);"+
								 "float tn_dot_ldGreen = max(dot(transformed_normals, light_directionGreen),0.0);"+
								 "vec3 ambientGreen = u_LaGreen * u_KaGreen;"+
								 "vec3 diffuseGreen = u_LdGreen * u_KdGreen * tn_dot_ldGreen;"+
								 "vec3 reflection_vectorGreen = reflect(-light_directionGreen, transformed_normals);"+
								 "vec3 specularGreen = u_LsGreen * u_KsGreen * pow(max(dot(reflection_vectorGreen, viewer_vector), 0.0), u_material_shininess);"+
								 "phong_ads_colorGreenVer=ambientGreen + diffuseGreen + specularGreen;"+

								 "}"+
								 "else if(u_single_tap == 1)" +
								 "{" +
								 "vec4 eye_coordinates = u_view_matrix * u_model_matrix *vPosition;" +
							 	 "transformed_normals = mat3(u_view_matrix * u_model_matrix) * vNormal;" +
								 "light_direction = vec3(u_light_position) - eye_coordinates.xyz;" +
 								 "light_directionRed = vec3(u_light_positionRed) - eye_coordinates.xyz;" +
  								 "light_directionGreen = vec3(u_light_positionGreen) - eye_coordinates.xyz;" +

								 "viewer_vector = -eye_coordinates.xyz;" +
								 "}" +
								 "gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"+
								 "}"
								);


							//provide source code to shader
							GLES32.glShaderSource(vertexShaderObject,vertexShaderSourceCode);

							//compile shader and check for errors
							GLES32.glCompileShader(vertexShaderObject);
							int[] iShaderCompiledStatus = new int[1];
							int[] iInfoLength = new int[1];
							String szInfoLength=null;
							GLES32.glGetShaderiv(vertexShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);
							if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
							{
								GLES32.glGetShaderiv(vertexShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLength,0);
								if(iInfoLength[0] > 0)
								{
									szInfoLength = GLES32.glGetShaderInfoLog(vertexShaderObject);
									System.out.println("VDG: Vertex Shader Compilation Log = "+szInfoLength);
									uninitialize();
									System.exit(0);
								}
							}
			
							System.out.println("VDG: Before Fragment Shader");

							//********FRAGMENT SHADER
							fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

							final String fragmentShaderSourceCode = String.format
							(  "#version 320 es"+
								 "\n"+
								"precision highp float;"+
								"in vec3 phong_ads_colorVer;"+
								"in vec3 phong_ads_colorRedVer;"+
								"in vec3 phong_ads_colorGreenVer;"+

								"in vec3 transformed_normals;" +
								"in vec3 light_direction;" +
								"in vec3 light_directionRed;" +
								"in vec3 light_directionGreen;" +
								"in vec3 viewer_vector;" +							

								"uniform vec3 u_La;" +
								"uniform vec3 u_Ld;" +
								"uniform vec3 u_Ls;" +
								"uniform vec3 u_Ka;" +
								"uniform vec3 u_Kd;" +
								"uniform vec3 u_Ks;" +

								"uniform vec3 u_LaRed;" +
								"uniform vec3 u_LdRed;" +
								"uniform vec3 u_LsRed;" +
								"uniform vec3 u_KaRed;" +
								"uniform vec3 u_KdRed;" +
								"uniform vec3 u_KsRed;" +

								
								"uniform vec3 u_LaGreen;" +
								"uniform vec3 u_LdGreen;" +
								"uniform vec3 u_LsGreen;" +
								"uniform vec3 u_KaGreen;" +
								"uniform vec3 u_KdGreen;" +
								"uniform vec3 u_KsGreen;" +

								"uniform float u_material_shininess;" +
								"uniform highp int u_single_tap;" +
								"uniform highp int u_double_tap;" +	
								"out vec4 FragColor;" +
								"void main(void)" +
								"{" +
								"vec3 phong_ads_color2;" +
								"vec3 phong_ads_colorRed;" +
								"vec3 phong_ads_colorGreen;" +
								"if(u_double_tap == 1)" +
								"{" +
								"FragColor = vec4(phong_ads_colorVer,1.0) + vec4(phong_ads_colorRedVer,1.0) + vec4(phong_ads_colorGreenVer,1.0);" +
								"}"+
								"else if(u_single_tap == 1)" +
								"{" +
								"vec3 normalized_transformed_normals=normalize(transformed_normals);" +
								"vec3 normalized_viewer_vector=normalize(viewer_vector);" +

								"vec3 normalized_light_direction=normalize(light_direction);" +
								"vec3 ambient = u_La * u_Ka;" +
								"float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction),0.0);" +
								"vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;" +
								"vec3 reflection_vector = reflect(-normalized_light_direction,normalized_transformed_normals);" +
								"vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector,normalized_viewer_vector),0.0),u_material_shininess);" +
								"phong_ads_color2 = ambient + diffuse + specular;" +

								"vec3 normalized_light_directionRed=normalize(light_directionRed);" +
								"vec3 ambientRed = u_LaRed * u_KaRed;" +
								"float tn_dot_ldRed = max(dot(normalized_transformed_normals, normalized_light_directionRed),0.0);" +
								"vec3 diffuseRed = u_LdRed * u_KdRed * tn_dot_ldRed;" +
								"vec3 reflection_vectorRed = reflect(-normalized_light_directionRed,normalized_transformed_normals);" +
								"vec3 specularRed = u_LsRed * u_KsRed * pow(max(dot(reflection_vectorRed,normalized_viewer_vector),0.0),u_material_shininess);" +
								"phong_ads_colorRed = ambientRed + diffuseRed + specularRed;" +

								"vec3 normalized_light_directionGreen=normalize(light_directionGreen);" +
								"vec3 ambientGreen = u_LaGreen * u_KaGreen;" +
								"float tn_dot_ldGreen = max(dot(normalized_transformed_normals, normalized_light_directionGreen),0.0);" +
								"vec3 diffuseGreen = u_LdGreen * u_KdGreen * tn_dot_ldGreen;" +
								"vec3 reflection_vectorGreen = reflect(-normalized_light_directionGreen,normalized_transformed_normals);" +
								"vec3 specularGreen = u_LsGreen * u_KsGreen * pow(max(dot(reflection_vectorGreen,normalized_viewer_vector),0.0),u_material_shininess);" +
								"phong_ads_colorGreen = ambientGreen + diffuseGreen + specularGreen;" +

								"FragColor = vec4(phong_ads_color2,1.0) + vec4(phong_ads_colorRed,1.0) +  vec4(phong_ads_colorGreen,1.0);" +						
								"}" +
								"else" +
								"{" +
								"FragColor= vec4(1.0,1.0,1.0,1.0);" +
								 "}"+
								 "}"
							);

								GLES32.glShaderSource(fragmentShaderObject,fragmentShaderSourceCode);
								GLES32.glCompileShader(fragmentShaderObject);
								iShaderCompiledStatus[0] = 0;
								 iInfoLength[0] = 0;
								szInfoLength=null;
								GLES32.glGetShaderiv(fragmentShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);
								if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
								{
								GLES32.glGetShaderiv(fragmentShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLength,0);
									if(iInfoLength[0] > 0)
									{
									szInfoLength = GLES32.glGetShaderInfoLog(fragmentShaderObject);
									System.out.println("VDG: Fragment Shader Compilation Log = "+szInfoLength);
									uninitialize();
									System.exit(0);
									}
								}


							//create shader object
							shaderProgramObject=GLES32.glCreateProgram();

							//attach vertex shader  to shader program
							GLES32.glAttachShader(shaderProgramObject,vertexShaderObject);

							//attach fragment to shader program
							GLES32.glAttachShader(shaderProgramObject,fragmentShaderObject);

							//pre-link binding of shader program 
							GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");

							GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.VDG_ATTRIBUTE_NORMAL,"vNormal");

							//link the two shaders together to shader program object
							GLES32.glLinkProgram(shaderProgramObject);
							int[] iShaderProgramLinkStatus = new int[1];
							iInfoLength[0]  = 0;
							String szInfoLog = null;
							GLES32.glGetProgramiv(shaderProgramObject,GLES32.GL_LINK_STATUS,iShaderProgramLinkStatus,0);
							if(iShaderProgramLinkStatus[0] == GLES32.GL_FALSE)
							{
								GLES32.glGetProgramiv(shaderProgramObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLength,0);
								if(iInfoLength[0] > 0)
								{
									szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
									System.out.println("VDG: Shader Program Link Log = "+szInfoLog);
									uninitialize();
									System.exit(0);
								}
							}

							//get uniform locations
								 modelMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_model_matrix");
								viewMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_view_matrix");
								projectionMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_projection_matrix");
        
								singleTapUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_single_tap");
								doubleTapUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_double_tap");
        
								laUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_La");
								ldUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ld");
								lsUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ls");
								lightPositionUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_light_position");;

								kaUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ka");
								kdUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Kd");
								ksUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ks");

								
								laUniformRed = GLES32.glGetUniformLocation(shaderProgramObject, "u_LaRed");
								ldUniformRed = GLES32.glGetUniformLocation(shaderProgramObject, "u_LdRed");
								lsUniformRed = GLES32.glGetUniformLocation(shaderProgramObject, "u_LsRed");
								lightPositionUniformRed = GLES32.glGetUniformLocation(shaderProgramObject, "u_light_positionRed");;

								kaUniformRed = GLES32.glGetUniformLocation(shaderProgramObject, "u_KaRed");
								kdUniformRed = GLES32.glGetUniformLocation(shaderProgramObject, "u_KdRed");
								ksUniformRed = GLES32.glGetUniformLocation(shaderProgramObject, "u_KsRed");

										
								laUniformGreen = GLES32.glGetUniformLocation(shaderProgramObject, "u_LaGreen");
								ldUniformGreen = GLES32.glGetUniformLocation(shaderProgramObject, "u_LdGreen");
								lsUniformGreen = GLES32.glGetUniformLocation(shaderProgramObject, "u_LsGreen");
								lightPositionUniformGreen = GLES32.glGetUniformLocation(shaderProgramObject, "u_light_positionGreen");;

								kaUniformGreen = GLES32.glGetUniformLocation(shaderProgramObject, "u_KaGreen");
								kdUniformGreen = GLES32.glGetUniformLocation(shaderProgramObject, "u_KdGreen");
								ksUniformGreen = GLES32.glGetUniformLocation(shaderProgramObject, "u_KsGreen");


								materialShininessUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_material_shininess");;
		
							//** vertices , colors, shader attribs, vbo, vao initializations
							Sphere sphere = new Sphere();
							float sphere_vertices[] = new float[1146];
							float sphere_normals[] = new float[1146];
							float sphere_textures[] = new float[764];
							short sphere_elements[] = new short[2280];	
							sphere.getSphereVertexData(sphere_vertices,sphere_normals,sphere_textures,sphere_elements);
							numVertices = sphere.getNumberOfSphereVertices();
							numElements = sphere.getNumberOfSphereElements();

							//vao
							GLES32.glGenVertexArrays(1,vao_sphere,0);
							GLES32.glBindVertexArray(vao_sphere[0]);

							//position vbo
						   ////////////////////////////////////////////////////////////////////////////
   							GLES32.glGenBuffers(1,vbo_sphere_position,0);
							GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_sphere_position[0]);
							ByteBuffer byteBuffer =ByteBuffer.allocateDirect(sphere_vertices.length * 4);
							byteBuffer.order(ByteOrder.nativeOrder());
							FloatBuffer verticesBuffer=byteBuffer.asFloatBuffer();												
							verticesBuffer.put(sphere_vertices);
							verticesBuffer.position(0);	

							GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,sphere_vertices.length*4,verticesBuffer,GLES32.GL_STATIC_DRAW);
						
							   GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_VERTEX,
														3,
														GLES32.GL_FLOAT,
														false,0,0);

						   GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_VERTEX);
						   GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

   							System.out.println("VDG: Before VBO Normal");

							//NORMAL VBO
							GLES32.glGenBuffers(1,vbo_sphere_normal,0);
							GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_sphere_normal[0]);
							byteBuffer=ByteBuffer.allocateDirect(sphere_normals.length*4);
							byteBuffer.order(ByteOrder.nativeOrder());
							verticesBuffer=byteBuffer.asFloatBuffer();
							verticesBuffer.put(sphere_normals);
							verticesBuffer.position(0);
	
							GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
												sphere_normals.length *4,
												verticesBuffer,
												GLES32.GL_STATIC_DRAW);
						
							GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_NORMAL,
															3,
															GLES32.GL_FLOAT,
															false,0,0);		
	
							GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_NORMAL);

							GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
	
							//ELEMENT VBO

								GLES32.glGenBuffers(1,vbo_sphere_element,0);
								GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
        
								byteBuffer=ByteBuffer.allocateDirect(sphere_elements.length * 2);
								byteBuffer.order(ByteOrder.nativeOrder());
								ShortBuffer elementsBuffer=byteBuffer.asShortBuffer();
								elementsBuffer.put(sphere_elements);
								elementsBuffer.position(0);
        
								GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER,
													sphere_elements.length * 2,
													elementsBuffer,
													GLES32.GL_STATIC_DRAW);
        
								GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,0);


							GLES32.glBindVertexArray(0);

							GLES32.glEnable(GLES32.GL_DEPTH_TEST);

							GLES32.glDepthFunc(GLES32.GL_LEQUAL);

							GLES32.glEnable(GLES32.GL_CULL_FACE);

							GLES32.glClearColor(0.0f,0.0f,0.0f,1.0f);
								doubleTap=0;
								singleTap = 0;
							Matrix.setIdentityM(perspectiveProjectionMatrix,0);
	
						}

						private void resize(int width, int height)
						{
							GLES32.glViewport(0,0,width,height);

							if(height == 0)
								 height = 1;

							Matrix.perspectiveM(perspectiveProjectionMatrix,0,45.0f,(float)width / (float)height,0.1f,100.0f);
						}


						public void display()
						{
							GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

							GLES32.glUseProgram(shaderProgramObject);

							if(doubleTap==1)
							{
	
								 GLES32.glUniform1i(doubleTapUniform, 1);
            
									// setting light's properties
									GLES32.glUniform3fv(laUniform, 1, light_ambient, 0);
									GLES32.glUniform3fv(ldUniform, 1, light_diffuse, 0);
									GLES32.glUniform3fv(lsUniform, 1, light_specular, 0);
									GLES32.glUniform4fv(lightPositionUniform, 1, myArrBlue, 0);
            
									// setting material's properties
									GLES32.glUniform3fv(kaUniform, 1, material_ambient, 0);
									GLES32.glUniform3fv(kdUniform, 1, material_diffuse, 0);
									GLES32.glUniform3fv(ksUniform, 1, material_specular, 0);
									GLES32.glUniform1f(materialShininessUniform, material_shininess);

									GLES32.glUniform3fv(laUniformRed, 1, light_ambientRed, 0);
									GLES32.glUniform3fv(ldUniformRed, 1, light_diffuseRed, 0);
									GLES32.glUniform3fv(lsUniformRed, 1, light_specularRed, 0);
									GLES32.glUniform4fv(lightPositionUniformRed, 1, myArrRed, 0);
            
									// setting material's properties
									GLES32.glUniform3fv(kaUniformRed, 1, material_ambient, 0);
									GLES32.glUniform3fv(kdUniformRed, 1, material_diffuse, 0);
									GLES32.glUniform3fv(ksUniformRed, 1, material_specular, 0);
									GLES32.glUniform1f(materialShininessUniform, material_shininess);

									GLES32.glUniform3fv(laUniformGreen, 1, light_ambientGreen, 0);
									GLES32.glUniform3fv(ldUniformGreen, 1, light_diffuseGreen, 0);
									GLES32.glUniform3fv(lsUniformGreen, 1, light_specularGreen, 0);
									GLES32.glUniform4fv(lightPositionUniformGreen, 1, myArrGreen, 0);
            
									// setting material's properties
									GLES32.glUniform3fv(kaUniformGreen, 1, material_ambient, 0);
									GLES32.glUniform3fv(kdUniformGreen, 1, material_diffuse, 0);
									GLES32.glUniform3fv(ksUniformGreen, 1, material_specular, 0);
									GLES32.glUniform1f(materialShininessUniform, material_shininess);

							}
						
							else if(singleTap==1)
							{
									GLES32.glUniform1i(singleTapUniform, 1);
            
									// setting light's properties
									GLES32.glUniform3fv(laUniform, 1, light_ambient, 0);
									GLES32.glUniform3fv(ldUniform, 1, light_diffuse, 0);
									GLES32.glUniform3fv(lsUniform, 1, light_specular, 0);
									GLES32.glUniform4fv(lightPositionUniform, 1, myArrBlue, 0);
            
									// setting material's properties
									GLES32.glUniform3fv(kaUniform, 1, material_ambient, 0);
									GLES32.glUniform3fv(kdUniform, 1, material_diffuse, 0);
									GLES32.glUniform3fv(ksUniform, 1, material_specular, 0);
									GLES32.glUniform1f(materialShininessUniform, material_shininess);

											GLES32.glUniform3fv(laUniformRed, 1, light_ambientRed, 0);
									GLES32.glUniform3fv(ldUniformRed, 1, light_diffuseRed, 0);
									GLES32.glUniform3fv(lsUniformRed, 1, light_specularRed, 0);
									GLES32.glUniform4fv(lightPositionUniformRed, 1, myArrRed, 0);
            
									// setting material's properties
									GLES32.glUniform3fv(kaUniformRed, 1, material_ambient, 0);
									GLES32.glUniform3fv(kdUniformRed, 1, material_diffuse, 0);
									GLES32.glUniform3fv(ksUniformRed, 1, material_specular, 0);
									GLES32.glUniform1f(materialShininessUniform, material_shininess);

									GLES32.glUniform3fv(laUniformGreen, 1, light_ambientGreen, 0);
									GLES32.glUniform3fv(ldUniformGreen, 1, light_diffuseGreen, 0);
									GLES32.glUniform3fv(lsUniformGreen, 1, light_specularGreen, 0);
									GLES32.glUniform4fv(lightPositionUniformGreen, 1, myArrGreen, 0);
            
									// setting material's properties
									GLES32.glUniform3fv(kaUniformGreen, 1, material_ambient, 0);
									GLES32.glUniform3fv(kdUniformGreen, 1, material_diffuse, 0);
									GLES32.glUniform3fv(ksUniformGreen, 1, material_specular, 0);
									GLES32.glUniform1f(materialShininessUniform, material_shininess);


							}
							else if(singleTap==2)
							{
									GLES32.glUniform1i(singleTapUniform, 1);
            
									// setting light's properties
									GLES32.glUniform3fv(laUniform, 1, light_ambient, 0);
									GLES32.glUniform3fv(ldUniform, 1, light_diffuse, 0);
									GLES32.glUniform3fv(lsUniform, 1, light_specular, 0);
									GLES32.glUniform4fv(lightPositionUniform, 1, myArrBlue, 0);
            
									// setting material's properties
									GLES32.glUniform3fv(kaUniform, 1, material_ambient, 0);
									GLES32.glUniform3fv(kdUniform, 1, material_diffuse, 0);
									GLES32.glUniform3fv(ksUniform, 1, material_specular, 0);
									GLES32.glUniform1f(materialShininessUniform, material_shininess);

									GLES32.glUniform3fv(laUniformRed, 1, light_ambientRed, 0);
									GLES32.glUniform3fv(ldUniformRed, 1, light_diffuseRed, 0);
									GLES32.glUniform3fv(lsUniformRed, 1, light_specularRed, 0);
									GLES32.glUniform4fv(lightPositionUniformRed, 1, myArrRed, 0);
            
									// setting material's properties
									GLES32.glUniform3fv(kaUniformRed, 1, material_ambient, 0);
									GLES32.glUniform3fv(kdUniformRed, 1, material_diffuse, 0);
									GLES32.glUniform3fv(ksUniformRed, 1, material_specular, 0);
									GLES32.glUniform1f(materialShininessUniform, material_shininess);
								
									GLES32.glUniform3fv(laUniformGreen, 1, light_ambientGreen, 0);
									GLES32.glUniform3fv(ldUniformGreen, 1, light_diffuseGreen, 0);
									GLES32.glUniform3fv(lsUniformGreen, 1, light_specularGreen, 0);
									GLES32.glUniform4fv(lightPositionUniformGreen, 1, myArrGreen, 0);
            
									// setting material's properties
									GLES32.glUniform3fv(kaUniformGreen, 1, material_ambient, 0);
									GLES32.glUniform3fv(kdUniformGreen, 1, material_diffuse, 0);
									GLES32.glUniform3fv(ksUniformGreen, 1, material_specular, 0);
									GLES32.glUniform1f(materialShininessUniform, material_shininess);
							}
						   else {
								 GLES32.glUniform1i(doubleTapUniform, 0);
						   }

							float viewMatrix[]=new float[16];
							float modelMatrix[]=new float[16];
						
							//set mode matrix and view matrix to identity matrix
							Matrix.setIdentityM(viewMatrix,0);
						
							Matrix.setIdentityM(modelMatrix,0);

							Matrix.translateM(modelMatrix,0,0.0f,0.0f,-1.5f);
							
							GLES32.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);
							GLES32.glUniformMatrix4fv(viewMatrixUniform,1,false,viewMatrix,0);
							GLES32.glUniformMatrix4fv(projectionMatrixUniform,1,false,perspectiveProjectionMatrix,0);

					
							myArrRed[1] = light_positionRed[0] * (float)Math.sin(gAngleRadianceRed);
							myArrRed[2] = light_positionRed[0] * (float)Math.cos(gAngleRadianceRed);

							myArrGreen[0] = light_positionGreen[1] * (float)Math.sin(gangleRadianceGreen);
							myArrGreen[2] = light_positionGreen[1] * (float)Math.cos(gangleRadianceGreen);

							myArrBlue[0] = light_position[2] * (float)Math.sin(gAngleRadianceBlue);
							myArrBlue[1] = light_position[2] * (float)Math.cos(gAngleRadianceBlue);


							//bind vao
							GLES32.glBindVertexArray(vao_sphere[0]);	
	
							GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);		
							GLES32.glDrawElements(GLES32.GL_TRIANGLES,numElements,GLES32.GL_UNSIGNED_SHORT,0);

							GLES32.glBindVertexArray(0);	
		
							GLES32.glUseProgram(0);
							update();
							requestRender();
						}
						
						public void update()
						{
						  gAngleBlue = gAngleBlue + 4.0f;
						  if(gAngleBlue >= 360.0f)
								gAngleBlue = 0.0f;

							gAngleRadianceBlue  = gAngleBlue*(M_PI/ 180.0f);

						 gAngleRed = gAngleRed + 4.0f;
						  if(gAngleRed >= 360.0f)
								gAngleRed = 0.0f;
								
								gAngleRadianceRed = gAngleRed*(M_PI/180.0f);

						 gAngleGreen = gAngleGreen + 4.0f;
						  if(gAngleGreen >= 360.0f)
								gAngleGreen = 0.0f;
								gangleRadianceGreen = gAngleGreen*(M_PI/180.0f);
						}
						void uninitialize()
						{
							if(vao_sphere[0] !=0 )
							{
								GLES32.glDeleteVertexArrays(1,vao_sphere,0);
								vao_sphere[0] = 0;
							}
	
							if(vbo_sphere_position [0] !=0)
							{
								GLES32.glDeleteBuffers(1,vbo_sphere_position ,0);
								vbo_sphere_normal [0] = 0;
							}
							if(vbo_sphere_normal [0] !=0)
							{
								GLES32.glDeleteBuffers(1,vbo_sphere_normal ,0);
								vbo_sphere_normal [0] = 0;
							}
							if(vbo_sphere_element [0] !=0)
							{
								GLES32.glDeleteBuffers(1,vbo_sphere_element ,0);
								vbo_sphere_element [0] = 0;
							}
	
							if(shaderProgramObject !=0 )
							{
								if(vertexShaderObject != 0)
								{
									//detach vertex shader
									GLES32.glDetachShader(shaderProgramObject,vertexShaderObject);
									//delete vertex shader object
									GLES32.glDeleteShader(vertexShaderObject);
									vertexShaderObject =0;
								}

								if(fragmentShaderObject != 0)
								{
									//detach fragment shader object
									GLES32.glDetachShader(shaderProgramObject,fragmentShaderObject);
									//delete fragment shader
									GLES32.glDeleteShader(fragmentShaderObject);
									fragmentShaderObject = 0;	
								}
							}

							//delete shader program object
							if(shaderProgramObject!=0)
							{
								GLES32.glDeleteProgram(shaderProgramObject);
								shaderProgramObject = 0;
	
							}
						  }
						}

