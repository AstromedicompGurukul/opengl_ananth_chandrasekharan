/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.astromedicomp.Three_Lights_on_Sphere;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.astromedicomp.Three_Lights_on_Sphere";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = -1;
  public static final String VERSION_NAME = "";
}
