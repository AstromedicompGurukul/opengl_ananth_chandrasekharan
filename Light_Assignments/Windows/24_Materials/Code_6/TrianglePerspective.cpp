#include<windows.h>
#include<stdio.h>
#include<gl/glew.h>
#include<gl/GL.h>

#include"vmath.h"
#include"Sphere.h"
#include "ArrayHeader.h"

using namespace vmath;

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")

#pragma comment(lib,"Sphere.lib")


//Prototype of WndProc() declared gloabally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable declarations
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullScreen = false;

enum
{
	VDG_ATTRIBUTE_VERTEX = 0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0,
};

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint gVertexShaderObject1;
GLuint gFragmentShaderObject1;
GLuint gShaderProgramObject1;


GLuint gNumElements;
GLuint gNumVertices;

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

GLuint gVao_Sphere;
GLuint gVbo_Sphere_positions;
GLuint gVbo_Sphere_normal;
GLuint gVbo_Sphere_element;

GLuint model_matrix_uniform, view_matrix_uniform, projection_matrix_uniform;

GLuint L_KeyPressed_uniform;

GLuint La_uniform;
GLuint Ld_uniform;
GLuint Ls_uniform;
GLuint light_position_uniform;
GLfloat angleBlue = 0.0f; //x axis
GLfloat angleRed = 0.0f; //y axis
GLfloat angleGreen = 0.0f; //z axis

GLuint Ka_uniform;
GLuint Kd_uniform;
GLuint Ks_uniform;
GLuint material_shininess_uniform;

mat4 gPerspectiveProjectionMatrix;

GLfloat myrotationArrBlue[4];
GLfloat myrotationArrRed[4];
GLfloat myrotationArrGreen[4];

bool gbLight;
bool x_direction = false;
bool y_direction = false;
bool z_direction = false;

FILE *gpFile = NULL;

//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nCmdShow)
{
	void display();
	void initialize();
	void uninitialize();
	void update();

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("RTROGL");
	bool bDone = false;

	fopen_s(&gpFile, "LogFile.txt", "w");
	if (gpFile == NULL)
	{
		printf("Error opening file\n");
		exit(0);
	}

	//initializing members of wndclass
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;


	//Registering class
	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("OpenGL Programmable PipeLine Native Windowing"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Game Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			update();

			display();
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
			}
		}
	}

	uninitialize();
	fprintf(gpFile, "Exitting Main..\n");
	return((int)msg.wParam);
}


LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void display();
	void resize(int, int);
	void ToggleFullScreen(void);
	void uninitialize(void);

	static bool gbIsLKeyPressed = false;
	static bool gbIsMKeyPressed = false;
	static bool gbIsAKeyPressed = false;
	//	static bool 

	fprintf(gpFile, "Entering WndProc..\n");

	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			if (gbEscapeKeyIsPressed == false)
			{
				gbEscapeKeyIsPressed = true;
			}
			break;

		case 0x46:
			if (gbFullScreen == false)
			{
				ToggleFullScreen();
				gbFullScreen = true;
			}
			else
			{
				ToggleFullScreen();
				gbFullScreen = false;
			}
			break;

		case 0x4c:
			if (gbLight == false)
			{
				
				gbLight = true;
			}
			else
			{
				gbLight = false;
			}
			break;

		case 0x4d:
			if (x_direction == false)
			{
				x_direction = true;
				y_direction = false;
				z_direction = false;
			}
			else if (x_direction == true)
			{
				x_direction = false;
				y_direction = false;
				z_direction = false;
			}
			break;

		case 0x4e:
			if (y_direction == false)
			{
				y_direction = true;
				x_direction = false;
				z_direction = false;
			}
			else if (y_direction == true)
			{
				y_direction = false;
				x_direction = false;
				z_direction = false;
			}
			break;

		case 0x4f:
			if (z_direction == false)
			{
				z_direction = true;
				y_direction = false;
				x_direction = false;
			}
			else if (z_direction == true)
			{
				z_direction = false;
				y_direction = false;
				x_direction = false;
			}
			break;
		}
		break;

	case WM_LBUTTONDOWN:
		break;

	case WM_CLOSE:
		uninitialize();
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
	fprintf(gpFile, "Exitting WndProc..\n");

}

void ToggleFullScreen(void)
{
	MONITORINFO mi;

	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	void resize(int, int);
	void uninitialize();
	fprintf(gpFile, "Entering Initialize..\n");

	int LoadGLTextures(GLuint *, TCHAR[]);

	//Variable 
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;
	//GLint num;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//initialize code
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 8;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == NULL)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	//*****VERTEX SHADER*******
	//create shader 
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//provide source code to shader
	const GLchar * vertexShaderShaderCode =
		"#version 440 core"	\
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform int u_lighting_enabled;" \
		"uniform vec4 u_light_position;" \
		"uniform float u_material_shininess;" \
		"out vec3 transformed_normals;" \
		"out vec3 light_direction;" \
		"out vec3 viewer_vector;" \
		"void main(void)" \
		"{" \
		"if(u_lighting_enabled == 1)" \
		"{" \
			"vec4 eye_coordinates= u_view_matrix * u_model_matrix *vPosition;" \
			"transformed_normals = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" \
			"viewer_vector = normalize(-eye_coordinates.xyz);" \
			"light_direction = normalize(vec3(u_light_position) - eye_coordinates.xyz);"\
		"}" \
		"gl_Position =u_projection_matrix *  u_view_matrix * u_model_matrix * vPosition;" \
		"}";

	glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertexShaderShaderCode, NULL);

	//compile shader
	glCompileShader(gVertexShaderObject);

	GLint iInfoLength = 0;
	GLint iShaderCompiledStatus = 0;
	char * szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Compilation Log:%s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}
	else
	{
		fprintf(gpFile, "Vertex Shader Compilation Log: No Errors\n");
	}

	//*****FRAGMENT SHADER****
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar *fragmentShaderSourceCode =
		"#version 440 core" \
		"\n" \
		"in vec3 transformed_normals;" \
		"in vec3 light_direction;" \
		"in vec3 light_directionRed;" \
		"in vec3 light_directionGreen;" \
		"in vec3 viewer_vector;" \
		"out vec4 FragColor;" \
		"uniform vec3 u_La;" \
		"uniform vec3 u_Ld;" \
		"uniform vec3 u_Ls;" \
		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Kd;" \
		"uniform vec3 u_Ks;" \
		"uniform float u_material_shininess;" \
		"uniform int u_lighting_enabled;" \
		"void main(void)" \
		"{" \
		"vec3 phong_ads_color;" \
		"vec3 phong_ads_colorRed;" \
		"vec3 phong_ads_colorGreen;" \
		"if(u_lighting_enabled==1)" \
		"{" \
		"vec3 normalized_transformed_normals=normalize(transformed_normals);" \
		"vec3 normalized_light_direction=normalize(light_direction);" \
		"vec3 normalized_viewer_vector=normalize(viewer_vector);" \
		"vec3 ambient = u_La * u_Ka;" \
		"float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction),0.0);" \
		"vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;" \
		"vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);" \
		"vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);" \
		"phong_ads_color=ambient + diffuse + specular;" \
		"}" \
		"else" \
		"{" \
		"phong_ads_color = vec3(1.0, 1.0, 1.0);" \
		"}" \
		"FragColor = vec4(phong_ads_color, 1.0)+vec4(phong_ads_colorRed, 1.0)+vec4(phong_ads_colorGreen, 1.0);" \
		"}";

	glShaderSource(gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

	//compile shader
	glCompileShader(gFragmentShaderObject);

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iShaderCompiledStatus);
		if (iInfoLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment shader compilation Log %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}
	else
	{
		fprintf(gpFile, "Fragment shader compilation Log : No Errors\n");
	}

	//*****SHADER PROGRAM******
	//create shader program
	gShaderProgramObject = glCreateProgram();

	//attach vertex shader to shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	//attach fragment shader to shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");

	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");
	//link shader
	glLinkProgram(gShaderProgramObject);

	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Status %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}
	else
	{
		fprintf(gpFile, "Shader Program Link Status : No Errors\n");
	}

	// get uniform locations

	model_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	view_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	projection_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");

	//L/I Key is pressed or not
	L_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObject, "u_lighting_enabled");

	//ambient color intensity of LIGHT
	La_uniform = glGetUniformLocation(gShaderProgramObject, "u_La");
	//diffuse color intensity of LIGHT
	Ld_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld");
	//specular color intensity of LIGHT
	Ls_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls");
	//position of light
	light_position_uniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");

	//ambient reflective color intensity of MATERIAL
	Ka_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");
	//diffuse reflective color intensity of MATERIAL
	Kd_uniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");

	//specular reflective color intensity of MATERIAL
	Ks_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");

	//shininess of material (value is conventionally between 1 to 200)
	material_shininess_uniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");
	
	//vertices colors shader attribs 
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);

	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	//vao
	glGenVertexArrays(1, &gVao_Sphere);
	glBindVertexArray(gVao_Sphere);

	//position vbo
	glGenBuffers(1, &gVbo_Sphere_positions);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Sphere_positions);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//normal vbo
	glGenBuffers(1, &gVbo_Sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//element vbo
	glGenBuffers(1, &gVbo_Sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_Sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	fprintf(gpFile, "After all vertices and normals..\n");

	glShadeModel(GL_SMOOTH);
	//DEPTH
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_CULL_FACE);
	glClearColor(0.80f, 0.80f, 0.80f, 0.80f);

	gPerspectiveProjectionMatrix = mat4::identity();
	gbLight = false;
	resize(WIN_WIDTH, WIN_HEIGHT);
	fprintf(gpFile, "Exitting Initialize..\n");
}

void display_corner()
{
	if (gbLight == true)
	{
		glUniform1i(L_KeyPressed_uniform, 1);

		//setting lights properties
		glUniform3fv(La_uniform, 1, light_ambient);
		glUniform3fv(Ld_uniform, 1, light_diffused);
		glUniform3fv(Ls_uniform, 1, light_specular);

		if (x_direction == true && y_direction == false && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrBlue);
		}
		else if (x_direction == false && y_direction == true && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrRed);
		}
		else if (x_direction == false && y_direction == false && z_direction == true)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrGreen);
		}
		//setting material's properties
		glUniform3fv(Ka_uniform, 1, material_ambient10);
		glUniform3fv(Kd_uniform, 1, material_diffused11);
		glUniform3fv(Ks_uniform, 1, material_specular12);
		glUniform1f(material_shininess_uniform, material_shinyness13);
	}
	else
	{
		//set 'u_lighting_enabled' uniform
		glUniform1i(L_KeyPressed_uniform, 0);
	}

	//OpenGL Drawing
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 rotationMatrixBlue = mat4::identity(); //For x axis
	mat4 rotationMatrixRed = mat4::identity();
	mat4 rotationMatrixGreen = mat4::identity();

	modelMatrix = translate(-2.0f, 0.9f, -3.0f);
	modelMatrix = modelMatrix*scale(0.3f, 0.3f, 0.3f);

	if (x_direction == true)
	{
		myrotationArrBlue[1] = (GLfloat)(light_position[0] * sin(angleBlue));
		myrotationArrBlue[2] = (GLfloat)(light_position[0] * cos(angleBlue));
		y_direction = false;
		z_direction = false;
	}
	else if (y_direction == true)
	{
		myrotationArrRed[0] = (GLfloat)(light_position[1] * sin(angleRed));
		myrotationArrRed[2] = (GLfloat)(light_position[1] * cos(angleRed));
		x_direction = false;
		z_direction = false;
	}
	else if (z_direction == true)
	{
		myrotationArrGreen[0] = (GLfloat)(light_position[2] * sin(angleGreen));
		myrotationArrGreen[1] = (GLfloat)(light_position[2] * cos(angleGreen));
		x_direction = false;
		y_direction = false;
	}

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_Sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_Sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
}

void display11()
{
	if (gbLight == true)
	{
		glUniform1i(L_KeyPressed_uniform, 1);

		//setting lights properties
		glUniform3fv(La_uniform, 1, light_ambient);
		glUniform3fv(Ld_uniform, 1, light_diffused);
		glUniform3fv(Ls_uniform, 1, light_specular);

		if (x_direction == true && y_direction==false && z_direction==false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrBlue);
		}
		else if (x_direction == false && y_direction == true && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrRed);
		}
		else if (x_direction == false && y_direction == false && z_direction == true)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrGreen);
		}

		//setting material's properties
		glUniform3fv(Ka_uniform, 1, material_ambient10);
		glUniform3fv(Kd_uniform, 1, material_diffused11);
		glUniform3fv(Ks_uniform, 1, material_specular12);
		glUniform1f(material_shininess_uniform, material_shinyness13);
	}
	else
	{
		//set 'u_lighting_enabled' uniform
		glUniform1i(L_KeyPressed_uniform, 0);
	}

	//OpenGL Drawing
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 rotationMatrixBlue = mat4::identity(); //For x axis
	mat4 rotationMatrixRed = mat4::identity();
	mat4 rotationMatrixGreen = mat4::identity();

	modelMatrix = translate(-0.8f, 0.9f, -3.0f);
	modelMatrix = modelMatrix*scale(0.3f, 0.3f, 0.3f);

	if (x_direction == true)
	{
		myrotationArrBlue[1] = (GLfloat)(light_position[0] * sin(angleBlue));
		myrotationArrBlue[2] = (GLfloat)(light_position[0] * cos(angleBlue));
		y_direction = false;
		z_direction = false;
	}
	else if (y_direction == true)
	{
		myrotationArrRed[0] = (GLfloat)(light_position[1] * sin(angleRed));
		myrotationArrRed[2] = (GLfloat)(light_position[1] * cos(angleRed));
		x_direction = false;
		z_direction = false;
	}
	else if (z_direction == true)
	{
		myrotationArrGreen[0] = (GLfloat)(light_position[2] * sin(angleGreen));
		myrotationArrGreen[1] = (GLfloat)(light_position[2] * cos(angleGreen));
		x_direction = false;
		y_direction = false;
	}

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_Sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_Sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
}


void display12()
{
	if (gbLight == true)
	{
		glUniform1i(L_KeyPressed_uniform, 1);

		//setting lights properties
		glUniform3fv(La_uniform, 1, light_ambient);
		glUniform3fv(Ld_uniform, 1, light_diffused);
		glUniform3fv(Ls_uniform, 1, light_specular);

		if (x_direction == true && y_direction == false && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrBlue);
		}
		else if (x_direction == false && y_direction == true && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrRed);
		}
		else if (x_direction == false && y_direction == false && z_direction == true)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrGreen);
		}

		//setting material's properties
		glUniform3fv(Ka_uniform, 1, material_ambient20);
		glUniform3fv(Kd_uniform, 1, material_diffused21);
		glUniform3fv(Ks_uniform, 1, material_specular22);
		glUniform1f(material_shininess_uniform, material_shinyness23);
	}
	else
	{
		//set 'u_lighting_enabled' uniform
		glUniform1i(L_KeyPressed_uniform, 0);
	}

	//OpenGL Drawing
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 rotationMatrixBlue = mat4::identity(); //For x axis
	mat4 rotationMatrixRed = mat4::identity();
	mat4 rotationMatrixGreen = mat4::identity();

	modelMatrix = translate(-0.2f, 0.9f, -3.0f);
	modelMatrix = modelMatrix*scale(0.3f, 0.3f, 0.3f);

	if (x_direction == true)
	{
		myrotationArrBlue[1] = (GLfloat)(light_position[0] * sin(angleBlue));
		myrotationArrBlue[2] = (GLfloat)(light_position[0] * cos(angleBlue));
		y_direction = false;
		z_direction = false;
	}
	else if (y_direction == true)
	{
		myrotationArrRed[0] = (GLfloat)(light_position[1] * sin(angleRed));
		myrotationArrRed[2] = (GLfloat)(light_position[1] * cos(angleRed));
		x_direction = false;
		z_direction = false;
	}
	else if (z_direction == true)
	{
		myrotationArrGreen[0] = (GLfloat)(light_position[2] * sin(angleGreen));
		myrotationArrGreen[1] = (GLfloat)(light_position[2] * cos(angleGreen));
		x_direction = false;
		y_direction = false;
	}

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_Sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_Sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
}

void display13()
{
	if (gbLight == true)
	{
		glUniform1i(L_KeyPressed_uniform, 1);

		//setting lights properties
		glUniform3fv(La_uniform, 1, light_ambient);
		glUniform3fv(Ld_uniform, 1, light_diffused);
		glUniform3fv(Ls_uniform, 1, light_specular);

		if (x_direction == true && y_direction == false && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrBlue);
		}
		else if (x_direction == false && y_direction == true && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrRed);
		}
		else if (x_direction == false && y_direction == false && z_direction == true)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrGreen);
		}

		//setting material's properties
		glUniform3fv(Ka_uniform, 1, material_ambient30);
		glUniform3fv(Kd_uniform, 1, material_diffused31);
		glUniform3fv(Ks_uniform, 1, material_specular32);
		glUniform1f(material_shininess_uniform, material_shinyness33);
	}
	else
	{
		//set 'u_lighting_enabled' uniform
		glUniform1i(L_KeyPressed_uniform, 0);
	}

	//OpenGL Drawing
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 rotationMatrixBlue = mat4::identity(); //For x axis
	mat4 rotationMatrixRed = mat4::identity();
	mat4 rotationMatrixGreen = mat4::identity();

	modelMatrix = translate(0.4f, 0.9f, -3.0f);
	modelMatrix = modelMatrix*scale(0.3f, 0.3f, 0.3f);

	if (x_direction == true)
	{
		myrotationArrBlue[1] = (GLfloat)(light_position[0] * sin(angleBlue));
		myrotationArrBlue[2] = (GLfloat)(light_position[0] * cos(angleBlue));
		y_direction = false;
		z_direction = false;
	}
	else if (y_direction == true)
	{
		myrotationArrRed[0] = (GLfloat)(light_position[1] * sin(angleRed));
		myrotationArrRed[2] = (GLfloat)(light_position[1] * cos(angleRed));
		x_direction = false;
		z_direction = false;
	}
	else if (z_direction == true)
	{
		myrotationArrGreen[0] = (GLfloat)(light_position[2] * sin(angleGreen));
		myrotationArrGreen[1] = (GLfloat)(light_position[2] * cos(angleGreen));
		x_direction = false;
		y_direction = false;
	}

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_Sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_Sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
}

void display14()
{
	if (gbLight == true)
	{
		glUniform1i(L_KeyPressed_uniform, 1);

		//setting lights properties
		glUniform3fv(La_uniform, 1, light_ambient);
		glUniform3fv(Ld_uniform, 1, light_diffused);
		glUniform3fv(Ls_uniform, 1, light_specular);

		if (x_direction == true && y_direction == false && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrBlue);
		}
		else if (x_direction == false && y_direction == true && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrRed);
		}
		else if (x_direction == false && y_direction == false && z_direction == true)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrGreen);
		}


		//setting material's properties
		glUniform3fv(Ka_uniform, 1, material_ambient40);
		glUniform3fv(Kd_uniform, 1, material_diffused41);
		glUniform3fv(Ks_uniform, 1, material_specular42);
		glUniform1f(material_shininess_uniform, material_shinyness43);
	}
	else
	{
		//set 'u_lighting_enabled' uniform
		glUniform1i(L_KeyPressed_uniform, 0);
	}

	//OpenGL Drawing
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 rotationMatrixBlue = mat4::identity(); //For x axis
	mat4 rotationMatrixRed = mat4::identity();
	mat4 rotationMatrixGreen = mat4::identity();

	modelMatrix = translate(1.0f, 0.9f, -3.0f);
	modelMatrix = modelMatrix*scale(0.3f, 0.3f, 0.3f);

	if (x_direction == true)
	{
		myrotationArrBlue[1] = (GLfloat)(light_position[0] * sin(angleBlue));
		myrotationArrBlue[2] = (GLfloat)(light_position[0] * cos(angleBlue));
		y_direction = false;
		z_direction = false;
	}
	else if (y_direction == true)
	{
		myrotationArrRed[0] = (GLfloat)(light_position[1] * sin(angleRed));
		myrotationArrRed[2] = (GLfloat)(light_position[1] * cos(angleRed));
		x_direction = false;
		z_direction = false;
	}
	else if (z_direction == true)
	{
		myrotationArrGreen[0] = (GLfloat)(light_position[2] * sin(angleGreen));
		myrotationArrGreen[1] = (GLfloat)(light_position[2] * cos(angleGreen));
		x_direction = false;
		y_direction = false;
	}

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_Sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_Sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
}

void display20()
{
	if (gbLight == true)
	{
		glUniform1i(L_KeyPressed_uniform, 1);

		//setting lights properties
		glUniform3fv(La_uniform, 1, light_ambient);
		glUniform3fv(Ld_uniform, 1, light_diffused);
		glUniform3fv(Ls_uniform, 1, light_specular);

		if (x_direction == true && y_direction == false && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrBlue);
		}
		else if (x_direction == false && y_direction == true && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrRed);
		}
		else if (x_direction == false && y_direction == false && z_direction == true)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrGreen);
		}

		//setting material's properties
		glUniform3fv(Ka_uniform, 1, material_ambient50);
		glUniform3fv(Kd_uniform, 1, material_diffused51);
		glUniform3fv(Ks_uniform, 1, material_specular52);
		glUniform1f(material_shininess_uniform, material_shinyness53);
	}
	else
	{
		//set 'u_lighting_enabled' uniform
		glUniform1i(L_KeyPressed_uniform, 0);
	}

	//OpenGL Drawing
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 rotationMatrixBlue = mat4::identity(); //For x axis
	mat4 rotationMatrixRed = mat4::identity();
	mat4 rotationMatrixGreen = mat4::identity();

	modelMatrix = translate(-0.8f, 0.575f, -3.0f);
	modelMatrix = modelMatrix*scale(0.3f, 0.3f, 0.3f);

	if (x_direction == true)
	{
		myrotationArrBlue[1] = (GLfloat)(light_position[0] * sin(angleBlue));
		myrotationArrBlue[2] = (GLfloat)(light_position[0] * cos(angleBlue));
		y_direction = false;
		z_direction = false;
	}
	else if (y_direction == true)
	{
		myrotationArrRed[0] = (GLfloat)(light_position[1] * sin(angleRed));
		myrotationArrRed[2] = (GLfloat)(light_position[1] * cos(angleRed));
		x_direction = false;
		z_direction = false;
	}
	else if (z_direction == true)
	{
		myrotationArrGreen[0] = (GLfloat)(light_position[2] * sin(angleGreen));
		myrotationArrGreen[1] = (GLfloat)(light_position[2] * cos(angleGreen));
		x_direction = false;
		y_direction = false;
	}

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_Sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_Sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
}

void display21()
{
	if (gbLight == true)
	{
		glUniform1i(L_KeyPressed_uniform, 1);

		//setting lights properties
		glUniform3fv(La_uniform, 1, light_ambient);
		glUniform3fv(Ld_uniform, 1, light_diffused);
		glUniform3fv(Ls_uniform, 1, light_specular);

		if (x_direction == true && y_direction == false && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrBlue);
		}
		else if (x_direction == false && y_direction == true && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrRed);
		}
		else if (x_direction == false && y_direction == false && z_direction == true)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrGreen);
		}

		//setting material's properties
		glUniform3fv(Ka_uniform, 1, material_ambient60);
		glUniform3fv(Kd_uniform, 1, material_diffused61);
		glUniform3fv(Ks_uniform, 1, material_specular62);
		glUniform1f(material_shininess_uniform, material_shinyness63);
	}
	else
	{
		//set 'u_lighting_enabled' uniform
		glUniform1i(L_KeyPressed_uniform, 0);
	}

	//OpenGL Drawing
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 rotationMatrixBlue = mat4::identity(); //For x axis
	mat4 rotationMatrixRed = mat4::identity();
	mat4 rotationMatrixGreen = mat4::identity();

	modelMatrix = translate(-0.2f, 0.575f, -3.0f);
	modelMatrix = modelMatrix*scale(0.3f, 0.3f, 0.3f);

	if (x_direction == true)
	{
		myrotationArrBlue[1] = (GLfloat)(light_position[0] * sin(angleBlue));
		myrotationArrBlue[2] = (GLfloat)(light_position[0] * cos(angleBlue));
		y_direction = false;
		z_direction = false;
	}
	else if (y_direction == true)
	{
		myrotationArrRed[0] = (GLfloat)(light_position[1] * sin(angleRed));
		myrotationArrRed[2] = (GLfloat)(light_position[1] * cos(angleRed));
		x_direction = false;
		z_direction = false;
	}
	else if (z_direction == true)
	{
		myrotationArrGreen[0] = (GLfloat)(light_position[2] * sin(angleGreen));
		myrotationArrGreen[1] = (GLfloat)(light_position[2] * cos(angleGreen));
		x_direction = false;
		y_direction = false;
	}

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_Sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_Sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
}

void display22()
{
	if (gbLight == true)
	{
		glUniform1i(L_KeyPressed_uniform, 1);

		//setting lights properties
		glUniform3fv(La_uniform, 1, light_ambient);
		glUniform3fv(Ld_uniform, 1, light_diffused);
		glUniform3fv(Ls_uniform, 1, light_specular);

		if (x_direction == true && y_direction == false && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrBlue);
		}
		else if (x_direction == false && y_direction == true && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrRed);
		}
		else if (x_direction == false && y_direction == false && z_direction == true)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrGreen);
		}

		//setting material's properties
		glUniform3fv(Ka_uniform, 1, material_ambient70);
		glUniform3fv(Kd_uniform, 1, material_diffused71);
		glUniform3fv(Ks_uniform, 1, material_specular72);
		glUniform1f(material_shininess_uniform, material_shinyness73);
	}
	else
	{
		//set 'u_lighting_enabled' uniform
		glUniform1i(L_KeyPressed_uniform, 0);
	}

	//OpenGL Drawing
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 rotationMatrixBlue = mat4::identity(); //For x axis
	mat4 rotationMatrixRed = mat4::identity();
	mat4 rotationMatrixGreen = mat4::identity();

	modelMatrix = translate(0.4f, 0.575f, -3.0f);
	modelMatrix = modelMatrix*scale(0.3f, 0.3f, 0.3f);

	if (x_direction == true)
	{
		myrotationArrBlue[1] = (GLfloat)(light_position[0] * sin(angleBlue));
		myrotationArrBlue[2] = (GLfloat)(light_position[0] * cos(angleBlue));
		y_direction = false;
		z_direction = false;
	}
	else if (y_direction == true)
	{
		myrotationArrRed[0] = (GLfloat)(light_position[1] * sin(angleRed));
		myrotationArrRed[2] = (GLfloat)(light_position[1] * cos(angleRed));
		x_direction = false;
		z_direction = false;
	}
	else if (z_direction == true)
	{
		myrotationArrGreen[0] = (GLfloat)(light_position[2] * sin(angleGreen));
		myrotationArrGreen[1] = (GLfloat)(light_position[2] * cos(angleGreen));
		x_direction = false;
		y_direction = false;
	}

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_Sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_Sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
}

void display23()
{
	if (gbLight == true)
	{
		glUniform1i(L_KeyPressed_uniform, 1);

		//setting lights properties
		glUniform3fv(La_uniform, 1, light_ambient);
		glUniform3fv(Ld_uniform, 1, light_diffused);
		glUniform3fv(Ls_uniform, 1, light_specular);

		if (x_direction == true && y_direction == false && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrBlue);
		}
		else if (x_direction == false && y_direction == true && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrRed);
		}
		else if (x_direction == false && y_direction == false && z_direction == true)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrGreen);
		}


		//setting material's properties
		glUniform3fv(Ka_uniform, 1, material_ambient80);
		glUniform3fv(Kd_uniform, 1, material_diffused81);
		glUniform3fv(Ks_uniform, 1, material_specular82);
		glUniform1f(material_shininess_uniform, material_shinyness83);
	}
	else
	{
		//set 'u_lighting_enabled' uniform
		glUniform1i(L_KeyPressed_uniform, 0);
	}

	//OpenGL Drawing
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 rotationMatrixBlue = mat4::identity(); //For x axis
	mat4 rotationMatrixRed = mat4::identity();
	mat4 rotationMatrixGreen = mat4::identity();

	modelMatrix = translate(1.0f, 0.575f, -3.0f);
	modelMatrix = modelMatrix*scale(0.3f, 0.3f, 0.3f);

	if (x_direction == true)
	{
		myrotationArrBlue[1] = (GLfloat)(light_position[0] * sin(angleBlue));
		myrotationArrBlue[2] = (GLfloat)(light_position[0] * cos(angleBlue));
		y_direction = false;
		z_direction = false;
	}
	else if (y_direction == true)
	{
		myrotationArrRed[0] = (GLfloat)(light_position[1] * sin(angleRed));
		myrotationArrRed[2] = (GLfloat)(light_position[1] * cos(angleRed));
		x_direction = false;
		z_direction = false;
	}
	else if (z_direction == true)
	{
		myrotationArrGreen[0] = (GLfloat)(light_position[2] * sin(angleGreen));
		myrotationArrGreen[1] = (GLfloat)(light_position[2] * cos(angleGreen));
		x_direction = false;
		y_direction = false;
	}

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_Sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_Sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
}

void display30()
{
	if (gbLight == true)
	{
		glUniform1i(L_KeyPressed_uniform, 1);

		//setting lights properties
		glUniform3fv(La_uniform, 1, light_ambient);
		glUniform3fv(Ld_uniform, 1, light_diffused);
		glUniform3fv(Ls_uniform, 1, light_specular);

		if (x_direction == true && y_direction == false && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrBlue);
		}
		else if (x_direction == false && y_direction == true && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrRed);
		}
		else if (x_direction == false && y_direction == false && z_direction == true)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrGreen);
		}


		//setting material's properties
		glUniform3fv(Ka_uniform, 1, material_ambient90);
		glUniform3fv(Kd_uniform, 1, material_diffused91);
		glUniform3fv(Ks_uniform, 1, material_specular92);
		glUniform1f(material_shininess_uniform, material_shinyness93);
	}
	else
	{
		//set 'u_lighting_enabled' uniform
		glUniform1i(L_KeyPressed_uniform, 0);
	}

	//OpenGL Drawing
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 rotationMatrixBlue = mat4::identity(); //For x axis
	mat4 rotationMatrixRed = mat4::identity();
	mat4 rotationMatrixGreen = mat4::identity();

	modelMatrix = translate(-0.8f, 0.25f, -3.0f);
	modelMatrix = modelMatrix*scale(0.3f, 0.3f, 0.3f);

	if (x_direction == true)
	{
		myrotationArrBlue[1] = (GLfloat)(light_position[0] * sin(angleBlue));
		myrotationArrBlue[2] = (GLfloat)(light_position[0] * cos(angleBlue));
		y_direction = false;
		z_direction = false;
	}
	else if (y_direction == true)
	{
		myrotationArrRed[0] = (GLfloat)(light_position[1] * sin(angleRed));
		myrotationArrRed[2] = (GLfloat)(light_position[1] * cos(angleRed));
		x_direction = false;
		z_direction = false;
	}
	else if (z_direction == true)
	{
		myrotationArrGreen[0] = (GLfloat)(light_position[2] * sin(angleGreen));
		myrotationArrGreen[1] = (GLfloat)(light_position[2] * cos(angleGreen));
		x_direction = false;
		y_direction = false;
	}

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_Sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_Sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
}

void display31()
{
	if (gbLight == true)
	{
		glUniform1i(L_KeyPressed_uniform, 1);

		//setting lights properties
		glUniform3fv(La_uniform, 1, light_ambient);
		glUniform3fv(Ld_uniform, 1, light_diffused);
		glUniform3fv(Ls_uniform, 1, light_specular);

		if (x_direction == true && y_direction == false && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrBlue);
		}
		else if (x_direction == false && y_direction == true && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrRed);
		}
		else if (x_direction == false && y_direction == false && z_direction == true)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrGreen);
		}


		//setting material's properties
		glUniform3fv(Ka_uniform, 1, material_ambient100);
		glUniform3fv(Kd_uniform, 1, material_diffused101);
		glUniform3fv(Ks_uniform, 1, material_specular102);
		glUniform1f(material_shininess_uniform, material_shinyness103);
	}
	else
	{
		//set 'u_lighting_enabled' uniform
		glUniform1i(L_KeyPressed_uniform, 0);
	}

	//OpenGL Drawing
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 rotationMatrixBlue = mat4::identity(); //For x axis
	mat4 rotationMatrixRed = mat4::identity();
	mat4 rotationMatrixGreen = mat4::identity();

	modelMatrix = translate(-0.2f, 0.25f, -3.0f);
	modelMatrix = modelMatrix*scale(0.3f, 0.3f, 0.3f);

	if (x_direction == true)
	{
		myrotationArrBlue[1] = (GLfloat)(light_position[0] * sin(angleBlue));
		myrotationArrBlue[2] = (GLfloat)(light_position[0] * cos(angleBlue));
		y_direction = false;
		z_direction = false;
	}
	else if (y_direction == true)
	{
		myrotationArrRed[0] = (GLfloat)(light_position[1] * sin(angleRed));
		myrotationArrRed[2] = (GLfloat)(light_position[1] * cos(angleRed));
		x_direction = false;
		z_direction = false;
	}
	else if (z_direction == true)
	{
		myrotationArrGreen[0] = (GLfloat)(light_position[2] * sin(angleGreen));
		myrotationArrGreen[1] = (GLfloat)(light_position[2] * cos(angleGreen));
		x_direction = false;
		y_direction = false;
	}

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_Sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_Sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
}

void display32()
{
	if (gbLight == true)
	{
		glUniform1i(L_KeyPressed_uniform, 1);

		//setting lights properties
		glUniform3fv(La_uniform, 1, light_ambient);
		glUniform3fv(Ld_uniform, 1, light_diffused);
		glUniform3fv(Ls_uniform, 1, light_specular);

		if (x_direction == true && y_direction == false && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrBlue);
		}
		else if (x_direction == false && y_direction == true && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrRed);
		}
		else if (x_direction == false && y_direction == false && z_direction == true)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrGreen);
		}


		//setting material's properties
		glUniform3fv(Ka_uniform, 1, material_ambient200);
		glUniform3fv(Kd_uniform, 1, material_diffused201);
		glUniform3fv(Ks_uniform, 1, material_specular202);
		glUniform1f(material_shininess_uniform, material_shinyness203);
	}
	else
	{
		//set 'u_lighting_enabled' uniform
		glUniform1i(L_KeyPressed_uniform, 0);
	}

	//OpenGL Drawing
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 rotationMatrixBlue = mat4::identity(); //For x axis
	mat4 rotationMatrixRed = mat4::identity();
	mat4 rotationMatrixGreen = mat4::identity();

	modelMatrix = translate(0.4f, 0.25f, -3.0f);
	modelMatrix = modelMatrix*scale(0.3f, 0.3f, 0.3f);

	if (x_direction == true)
	{
		myrotationArrBlue[1] = (GLfloat)(light_position[0] * sin(angleBlue));
		myrotationArrBlue[2] = (GLfloat)(light_position[0] * cos(angleBlue));
		y_direction = false;
		z_direction = false;
	}
	else if (y_direction == true)
	{
		myrotationArrRed[0] = (GLfloat)(light_position[1] * sin(angleRed));
		myrotationArrRed[2] = (GLfloat)(light_position[1] * cos(angleRed));
		x_direction = false;
		z_direction = false;
	}
	else if (z_direction == true)
	{
		myrotationArrGreen[0] = (GLfloat)(light_position[2] * sin(angleGreen));
		myrotationArrGreen[1] = (GLfloat)(light_position[2] * cos(angleGreen));
		x_direction = false;
		y_direction = false;
	}

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_Sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_Sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
}

void display33()
{
	if (gbLight == true)
	{
		glUniform1i(L_KeyPressed_uniform, 1);

		//setting lights properties
		glUniform3fv(La_uniform, 1, light_ambient);
		glUniform3fv(Ld_uniform, 1, light_diffused);
		glUniform3fv(Ls_uniform, 1, light_specular);

		if (x_direction == true && y_direction == false && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrBlue);
		}
		else if (x_direction == false && y_direction == true && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrRed);
		}
		else if (x_direction == false && y_direction == false && z_direction == true)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrGreen);
		}


		//setting material's properties
		glUniform3fv(Ka_uniform, 1, material_ambient300);
		glUniform3fv(Kd_uniform, 1, material_diffused301);
		glUniform3fv(Ks_uniform, 1, material_specular302);
		glUniform1f(material_shininess_uniform, material_shinyness303);
	}
	else
	{
		//set 'u_lighting_enabled' uniform
		glUniform1i(L_KeyPressed_uniform, 0);
	}

	//OpenGL Drawing
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 rotationMatrixBlue = mat4::identity(); //For x axis
	mat4 rotationMatrixRed = mat4::identity();
	mat4 rotationMatrixGreen = mat4::identity();

	modelMatrix = translate(1.0f, 0.25f, -3.0f);
	modelMatrix = modelMatrix*scale(0.3f, 0.3f, 0.3f);

	if (x_direction == true)
	{
		myrotationArrBlue[1] = (GLfloat)(light_position[0] * sin(angleBlue));
		myrotationArrBlue[2] = (GLfloat)(light_position[0] * cos(angleBlue));
		y_direction = false;
		z_direction = false;
	}
	else if (y_direction == true)
	{
		myrotationArrRed[0] = (GLfloat)(light_position[1] * sin(angleRed));
		myrotationArrRed[2] = (GLfloat)(light_position[1] * cos(angleRed));
		x_direction = false;
		z_direction = false;
	}
	else if (z_direction == true)
	{
		myrotationArrGreen[0] = (GLfloat)(light_position[2] * sin(angleGreen));
		myrotationArrGreen[1] = (GLfloat)(light_position[2] * cos(angleGreen));
		x_direction = false;
		y_direction = false;
	}

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_Sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_Sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
}

void display40()
{
	if (gbLight == true)
	{
		glUniform1i(L_KeyPressed_uniform, 1);

		//setting lights properties
		glUniform3fv(La_uniform, 1, light_ambient);
		glUniform3fv(Ld_uniform, 1, light_diffused);
		glUniform3fv(Ls_uniform, 1, light_specular);

		if (x_direction == true && y_direction == false && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrBlue);
		}
		else if (x_direction == false && y_direction == true && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrRed);
		}
		else if (x_direction == false && y_direction == false && z_direction == true)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrGreen);
		}

		//setting material's properties
		glUniform3fv(Ka_uniform, 1, material_ambient400);
		glUniform3fv(Kd_uniform, 1, material_diffused401);
		glUniform3fv(Ks_uniform, 1, material_specular402);
		glUniform1f(material_shininess_uniform, material_shinyness403);
	}
	else
	{
		//set 'u_lighting_enabled' uniform
		glUniform1i(L_KeyPressed_uniform, 0);
	}

	//OpenGL Drawing
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 rotationMatrixBlue = mat4::identity(); //For x axis
	mat4 rotationMatrixRed = mat4::identity();
	mat4 rotationMatrixGreen = mat4::identity();

	modelMatrix = translate(-0.8f, -0.075f, -3.0f);
	modelMatrix = modelMatrix*scale(0.3f, 0.3f, 0.3f);

	if (x_direction == true)
	{
		myrotationArrBlue[1] = (GLfloat)(light_position[0] * sin(angleBlue));
		myrotationArrBlue[2] = (GLfloat)(light_position[0] * cos(angleBlue));
		y_direction = false;
		z_direction = false;
	}
	else if (y_direction == true)
	{
		myrotationArrRed[0] = (GLfloat)(light_position[1] * sin(angleRed));
		myrotationArrRed[2] = (GLfloat)(light_position[1] * cos(angleRed));
		x_direction = false;
		z_direction = false;
	}
	else if (z_direction == true)
	{
		myrotationArrGreen[0] = (GLfloat)(light_position[2] * sin(angleGreen));
		myrotationArrGreen[1] = (GLfloat)(light_position[2] * cos(angleGreen));
		x_direction = false;
		y_direction = false;
	}

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_Sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_Sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
}

void display41()
{
	if (gbLight == true)
	{
		glUniform1i(L_KeyPressed_uniform, 1);

		//setting lights properties
		glUniform3fv(La_uniform, 1, light_ambient);
		glUniform3fv(Ld_uniform, 1, light_diffused);
		glUniform3fv(Ls_uniform, 1, light_specular);

		if (x_direction == true && y_direction == false && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrBlue);
		}
		else if (x_direction == false && y_direction == true && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrRed);
		}
		else if (x_direction == false && y_direction == false && z_direction == true)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrGreen);
		}


		//setting material's properties
		glUniform3fv(Ka_uniform, 1, material_ambient500);
		glUniform3fv(Kd_uniform, 1, material_diffused501);
		glUniform3fv(Ks_uniform, 1, material_specular502);
		glUniform1f(material_shininess_uniform, material_shinyness503);
	}
	else
	{
		//set 'u_lighting_enabled' uniform
		glUniform1i(L_KeyPressed_uniform, 0);
	}

	//OpenGL Drawing
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 rotationMatrixBlue = mat4::identity(); //For x axis
	mat4 rotationMatrixRed = mat4::identity();
	mat4 rotationMatrixGreen = mat4::identity();

	modelMatrix = translate(-0.2f, -0.075f, -3.0f);
	modelMatrix = modelMatrix*scale(0.3f, 0.3f, 0.3f);

	if (x_direction == true)
	{
		myrotationArrBlue[1] = (GLfloat)(light_position[0] * sin(angleBlue));
		myrotationArrBlue[2] = (GLfloat)(light_position[0] * cos(angleBlue));
		y_direction = false;
		z_direction = false;
	}
	else if (y_direction == true)
	{
		myrotationArrRed[0] = (GLfloat)(light_position[1] * sin(angleRed));
		myrotationArrRed[2] = (GLfloat)(light_position[1] * cos(angleRed));
		x_direction = false;
		z_direction = false;
	}
	else if (z_direction == true)
	{
		myrotationArrGreen[0] = (GLfloat)(light_position[2] * sin(angleGreen));
		myrotationArrGreen[1] = (GLfloat)(light_position[2] * cos(angleGreen));
		x_direction = false;
		y_direction = false;
	}

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_Sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_Sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
}

void display42()
{
	if (gbLight == true)
	{
		glUniform1i(L_KeyPressed_uniform, 1);

		//setting lights properties
		glUniform3fv(La_uniform, 1, light_ambient);
		glUniform3fv(Ld_uniform, 1, light_diffused);
		glUniform3fv(Ls_uniform, 1, light_specular);

		if (x_direction == true && y_direction == false && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrBlue);
		}
		else if (x_direction == false && y_direction == true && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrRed);
		}
		else if (x_direction == false && y_direction == false && z_direction == true)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrGreen);
		}

		//setting material's properties
		glUniform3fv(Ka_uniform, 1, material_ambient600);
		glUniform3fv(Kd_uniform, 1, material_diffused601);
		glUniform3fv(Ks_uniform, 1, material_specular602);
		glUniform1f(material_shininess_uniform, material_shinyness603);
	}
	else
	{
		//set 'u_lighting_enabled' uniform
		glUniform1i(L_KeyPressed_uniform, 0);
	}

	//OpenGL Drawing
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 rotationMatrixBlue = mat4::identity(); //For x axis
	mat4 rotationMatrixRed = mat4::identity();
	mat4 rotationMatrixGreen = mat4::identity();

	modelMatrix = translate(0.4f, -0.075f, -3.0f);
	modelMatrix = modelMatrix*scale(0.3f, 0.3f, 0.3f);

	if (x_direction == true)
	{
		myrotationArrBlue[1] = (GLfloat)(light_position[0] * sin(angleBlue));
		myrotationArrBlue[2] = (GLfloat)(light_position[0] * cos(angleBlue));
		y_direction = false;
		z_direction = false;
	}
	else if (y_direction == true)
	{
		myrotationArrRed[0] = (GLfloat)(light_position[1] * sin(angleRed));
		myrotationArrRed[2] = (GLfloat)(light_position[1] * cos(angleRed));
		x_direction = false;
		z_direction = false;
	}
	else if (z_direction == true)
	{
		myrotationArrGreen[0] = (GLfloat)(light_position[2] * sin(angleGreen));
		myrotationArrGreen[1] = (GLfloat)(light_position[2] * cos(angleGreen));
		x_direction = false;
		y_direction = false;
	}

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_Sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_Sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
}

void display43()
{
	if (gbLight == true)
	{
		glUniform1i(L_KeyPressed_uniform, 1);

		//setting lights properties
		glUniform3fv(La_uniform, 1, light_ambient);
		glUniform3fv(Ld_uniform, 1, light_diffused);
		glUniform3fv(Ls_uniform, 1, light_specular);

		if (x_direction == true && y_direction == false && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrBlue);
		}
		else if (x_direction == false && y_direction == true && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrRed);
		}
		else if (x_direction == false && y_direction == false && z_direction == true)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrGreen);
		}


		//setting material's properties
		glUniform3fv(Ka_uniform, 1, material_ambient700);
		glUniform3fv(Kd_uniform, 1, material_diffused701);
		glUniform3fv(Ks_uniform, 1, material_specular702);
		glUniform1f(material_shininess_uniform, material_shinyness703);
	}
	else
	{
		//set 'u_lighting_enabled' uniform
		glUniform1i(L_KeyPressed_uniform, 0);
	}

	//OpenGL Drawing
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 rotationMatrixBlue = mat4::identity(); //For x axis
	mat4 rotationMatrixRed = mat4::identity();
	mat4 rotationMatrixGreen = mat4::identity();

	modelMatrix = translate(1.0f, -0.075f, -3.0f);
	modelMatrix = modelMatrix*scale(0.3f, 0.3f, 0.3f);

	if (x_direction == true)
	{
		myrotationArrBlue[1] = (GLfloat)(light_position[0] * sin(angleBlue));
		myrotationArrBlue[2] = (GLfloat)(light_position[0] * cos(angleBlue));
		y_direction = false;
		z_direction = false;
	}
	else if (y_direction == true)
	{
		myrotationArrRed[0] = (GLfloat)(light_position[1] * sin(angleRed));
		myrotationArrRed[2] = (GLfloat)(light_position[1] * cos(angleRed));
		x_direction = false;
		z_direction = false;
	}
	else if (z_direction == true)
	{
		myrotationArrGreen[0] = (GLfloat)(light_position[2] * sin(angleGreen));
		myrotationArrGreen[1] = (GLfloat)(light_position[2] * cos(angleGreen));
		x_direction = false;
		y_direction = false;
	}

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_Sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_Sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
}

void display50()
{
	if (gbLight == true)
	{
		glUniform1i(L_KeyPressed_uniform, 1);

		//setting lights properties
		glUniform3fv(La_uniform, 1, light_ambient);
		glUniform3fv(Ld_uniform, 1, light_diffused);
		glUniform3fv(Ls_uniform, 1, light_specular);

		if (x_direction == true && y_direction == false && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrBlue);
		}
		else if (x_direction == false && y_direction == true && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrRed);
		}
		else if (x_direction == false && y_direction == false && z_direction == true)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrGreen);
		}


		//setting material's properties
		glUniform3fv(Ka_uniform, 1, material_ambient800);
		glUniform3fv(Kd_uniform, 1, material_diffused801);
		glUniform3fv(Ks_uniform, 1, material_specular802);
		glUniform1f(material_shininess_uniform, material_shinyness803);
	}
	else
	{
		//set 'u_lighting_enabled' uniform
		glUniform1i(L_KeyPressed_uniform, 0);
	}

	//OpenGL Drawing
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 rotationMatrixBlue = mat4::identity(); //For x axis
	mat4 rotationMatrixRed = mat4::identity();
	mat4 rotationMatrixGreen = mat4::identity();

	modelMatrix = translate(-0.8f, -0.4f, -3.0f);
	modelMatrix = modelMatrix*scale(0.3f, 0.3f, 0.3f);

	if (x_direction == true)
	{
		myrotationArrBlue[1] = (GLfloat)(light_position[0] * sin(angleBlue));
		myrotationArrBlue[2] = (GLfloat)(light_position[0] * cos(angleBlue));
		y_direction = false;
		z_direction = false;
	}
	else if (y_direction == true)
	{
		myrotationArrRed[0] = (GLfloat)(light_position[1] * sin(angleRed));
		myrotationArrRed[2] = (GLfloat)(light_position[1] * cos(angleRed));
		x_direction = false;
		z_direction = false;
	}
	else if (z_direction == true)
	{
		myrotationArrGreen[0] = (GLfloat)(light_position[2] * sin(angleGreen));
		myrotationArrGreen[1] = (GLfloat)(light_position[2] * cos(angleGreen));
		x_direction = false;
		y_direction = false;
	}

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_Sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_Sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
}

void display51()
{
	if (gbLight == true)
	{
		glUniform1i(L_KeyPressed_uniform, 1);

		//setting lights properties
		glUniform3fv(La_uniform, 1, light_ambient);
		glUniform3fv(Ld_uniform, 1, light_diffused);
		glUniform3fv(Ls_uniform, 1, light_specular);

		if (x_direction == true && y_direction == false && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrBlue);
		}
		else if (x_direction == false && y_direction == true && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrRed);
		}
		else if (x_direction == false && y_direction == false && z_direction == true)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrGreen);
		}


		//setting material's properties
		glUniform3fv(Ka_uniform, 1, material_ambient900);
		glUniform3fv(Kd_uniform, 1, material_diffused901);
		glUniform3fv(Ks_uniform, 1, material_specular902);
		glUniform1f(material_shininess_uniform, material_shinyness903);
	}
	else
	{
		//set 'u_lighting_enabled' uniform
		glUniform1i(L_KeyPressed_uniform, 0);
	}

	//OpenGL Drawing
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 rotationMatrixBlue = mat4::identity(); //For x axis
	mat4 rotationMatrixRed = mat4::identity();
	mat4 rotationMatrixGreen = mat4::identity();

	modelMatrix = translate(-0.2f, -0.4f, -3.0f);
	modelMatrix = modelMatrix*scale(0.3f, 0.3f, 0.3f);

	if (x_direction == true)
	{
		myrotationArrBlue[1] = (GLfloat)(light_position[0] * sin(angleBlue));
		myrotationArrBlue[2] = (GLfloat)(light_position[0] * cos(angleBlue));
		y_direction = false;
		z_direction = false;
	}
	else if (y_direction == true)
	{
		myrotationArrRed[0] = (GLfloat)(light_position[1] * sin(angleRed));
		myrotationArrRed[2] = (GLfloat)(light_position[1] * cos(angleRed));
		x_direction = false;
		z_direction = false;
	}
	else if (z_direction == true)
	{
		myrotationArrGreen[0] = (GLfloat)(light_position[2] * sin(angleGreen));
		myrotationArrGreen[1] = (GLfloat)(light_position[2] * cos(angleGreen));
		x_direction = false;
		y_direction = false;
	}

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_Sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_Sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
}

void display52()
{
	if (gbLight == true)
	{
		glUniform1i(L_KeyPressed_uniform, 1);

		//setting lights properties
		glUniform3fv(La_uniform, 1, light_ambient);
		glUniform3fv(Ld_uniform, 1, light_diffused);
		glUniform3fv(Ls_uniform, 1, light_specular);

		if (x_direction == true && y_direction == false && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrBlue);
		}
		else if (x_direction == false && y_direction == true && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrRed);
		}
		else if (x_direction == false && y_direction == false && z_direction == true)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrGreen);
		}


		//setting material's properties
		glUniform3fv(Ka_uniform, 1, material_ambient1000);
		glUniform3fv(Kd_uniform, 1, material_diffused1001);
		glUniform3fv(Ks_uniform, 1, material_specular1002);
		glUniform1f(material_shininess_uniform, material_shinyness1003);
	}
	else
	{
		//set 'u_lighting_enabled' uniform
		glUniform1i(L_KeyPressed_uniform, 0);
	}

	//OpenGL Drawing
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 rotationMatrixBlue = mat4::identity(); //For x axis
	mat4 rotationMatrixRed = mat4::identity();
	mat4 rotationMatrixGreen = mat4::identity();

	modelMatrix = translate(0.4f, -0.4f, -3.0f);
	modelMatrix = modelMatrix*scale(0.3f, 0.3f, 0.3f);

	if (x_direction == true)
	{
		myrotationArrBlue[1] = (GLfloat)(light_position[0] * sin(angleBlue));
		myrotationArrBlue[2] = (GLfloat)(light_position[0] * cos(angleBlue));
		y_direction = false;
		z_direction = false;
	}
	else if (y_direction == true)
	{
		myrotationArrRed[0] = (GLfloat)(light_position[1] * sin(angleRed));
		myrotationArrRed[2] = (GLfloat)(light_position[1] * cos(angleRed));
		x_direction = false;
		z_direction = false;
	}
	else if (z_direction == true)
	{
		myrotationArrGreen[0] = (GLfloat)(light_position[2] * sin(angleGreen));
		myrotationArrGreen[1] = (GLfloat)(light_position[2] * cos(angleGreen));
		x_direction = false;
		y_direction = false;
	}

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_Sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_Sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
}

void display53()
{
	if (gbLight == true)
	{
		glUniform1i(L_KeyPressed_uniform, 1);

		//setting lights properties
		glUniform3fv(La_uniform, 1, light_ambient);
		glUniform3fv(Ld_uniform, 1, light_diffused);
		glUniform3fv(Ls_uniform, 1, light_specular);

		if (x_direction == true && y_direction == false && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrBlue);
		}
		else if (x_direction == false && y_direction == true && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrRed);
		}
		else if (x_direction == false && y_direction == false && z_direction == true)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrGreen);
		}


		//setting material's properties
		glUniform3fv(Ka_uniform, 1, material_ambient2000);
		glUniform3fv(Kd_uniform, 1, material_diffused2001);
		glUniform3fv(Ks_uniform, 1, material_specular2002);
		glUniform1f(material_shininess_uniform, material_shinyness2003);
	}
	else
	{
		//set 'u_lighting_enabled' uniform
		glUniform1i(L_KeyPressed_uniform, 0);
	}

	//OpenGL Drawing
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 rotationMatrixBlue = mat4::identity(); //For x axis
	mat4 rotationMatrixRed = mat4::identity();
	mat4 rotationMatrixGreen = mat4::identity();

	modelMatrix = translate(1.0f, -0.4f, -3.0f);
	modelMatrix = modelMatrix*scale(0.3f, 0.3f, 0.3f);

	if (x_direction == true)
	{
		myrotationArrBlue[1] = (GLfloat)(light_position[0] * sin(angleBlue));
		myrotationArrBlue[2] = (GLfloat)(light_position[0] * cos(angleBlue));
		y_direction = false;
		z_direction = false;
	}
	else if (y_direction == true)
	{
		myrotationArrRed[0] = (GLfloat)(light_position[1] * sin(angleRed));
		myrotationArrRed[2] = (GLfloat)(light_position[1] * cos(angleRed));
		x_direction = false;
		z_direction = false;
	}
	else if (z_direction == true)
	{
		myrotationArrGreen[0] = (GLfloat)(light_position[2] * sin(angleGreen));
		myrotationArrGreen[1] = (GLfloat)(light_position[2] * cos(angleGreen));
		x_direction = false;
		y_direction = false;
	}

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_Sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_Sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
}

void display60()
{
	if (gbLight == true)
	{
		glUniform1i(L_KeyPressed_uniform, 1);

		//setting lights properties
		glUniform3fv(La_uniform, 1, light_ambient);
		glUniform3fv(Ld_uniform, 1, light_diffused);
		glUniform3fv(Ls_uniform, 1, light_specular);

		if (x_direction == true && y_direction == false && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrBlue);
		}
		else if (x_direction == false && y_direction == true && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrRed);
		}
		else if (x_direction == false && y_direction == false && z_direction == true)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrGreen);
		}


		//setting material's properties
		glUniform3fv(Ka_uniform, 1, material_ambient3000);
		glUniform3fv(Kd_uniform, 1, material_diffused3001);
		glUniform3fv(Ks_uniform, 1, material_specular3002);
		glUniform1f(material_shininess_uniform, material_shinyness3003);
	}
	else
	{
		//set 'u_lighting_enabled' uniform
		glUniform1i(L_KeyPressed_uniform, 0);
	}

	//OpenGL Drawing
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 rotationMatrixBlue = mat4::identity(); //For x axis
	mat4 rotationMatrixRed = mat4::identity();
	mat4 rotationMatrixGreen = mat4::identity();

	modelMatrix = translate(-0.8f, -0.725f, -3.0f);
	modelMatrix = modelMatrix*scale(0.3f, 0.3f, 0.3f);

	if (x_direction == true)
	{
		myrotationArrBlue[1] = (GLfloat)(light_position[0] * sin(angleBlue));
		myrotationArrBlue[2] = (GLfloat)(light_position[0] * cos(angleBlue));
		y_direction = false;
		z_direction = false;
	}
	else if (y_direction == true)
	{
		myrotationArrRed[0] = (GLfloat)(light_position[1] * sin(angleRed));
		myrotationArrRed[2] = (GLfloat)(light_position[1] * cos(angleRed));
		x_direction = false;
		z_direction = false;
	}
	else if (z_direction == true)
	{
		myrotationArrGreen[0] = (GLfloat)(light_position[2] * sin(angleGreen));
		myrotationArrGreen[1] = (GLfloat)(light_position[2] * cos(angleGreen));
		x_direction = false;
		y_direction = false;
	}

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_Sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_Sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
}

void display61()
{
	if (gbLight == true)
	{
		glUniform1i(L_KeyPressed_uniform, 1);

		//setting lights properties
		glUniform3fv(La_uniform, 1, light_ambient);
		glUniform3fv(Ld_uniform, 1, light_diffused);
		glUniform3fv(Ls_uniform, 1, light_specular);

		if (x_direction == true && y_direction == false && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrBlue);
		}
		else if (x_direction == false && y_direction == true && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrRed);
		}
		else if (x_direction == false && y_direction == false && z_direction == true)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrGreen);
		}


		//setting material's properties
		glUniform3fv(Ka_uniform, 1, material_ambient4000);
		glUniform3fv(Kd_uniform, 1, material_diffused4001);
		glUniform3fv(Ks_uniform, 1, material_specular4002);
		glUniform1f(material_shininess_uniform, material_shinyness4003);
	}
	else
	{
		//set 'u_lighting_enabled' uniform
		glUniform1i(L_KeyPressed_uniform, 0);
	}

	//OpenGL Drawing
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 rotationMatrixBlue = mat4::identity(); //For x axis
	mat4 rotationMatrixRed = mat4::identity();
	mat4 rotationMatrixGreen = mat4::identity();

	modelMatrix = translate(-0.2f, -0.725f, -3.0f);
	modelMatrix = modelMatrix*scale(0.3f, 0.3f, 0.3f);

	if (x_direction == true)
	{
		myrotationArrBlue[1] = (GLfloat)(light_position[0] * sin(angleBlue));
		myrotationArrBlue[2] = (GLfloat)(light_position[0] * cos(angleBlue));
		y_direction = false;
		z_direction = false;
	}
	else if (y_direction == true)
	{
		myrotationArrRed[0] = (GLfloat)(light_position[1] * sin(angleRed));
		myrotationArrRed[2] = (GLfloat)(light_position[1] * cos(angleRed));
		x_direction = false;
		z_direction = false;
	}
	else if (z_direction == true)
	{
		myrotationArrGreen[0] = (GLfloat)(light_position[2] * sin(angleGreen));
		myrotationArrGreen[1] = (GLfloat)(light_position[2] * cos(angleGreen));
		x_direction = false;
		y_direction = false;
	}

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_Sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_Sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
}

void display62()
{
	if (gbLight == true)
	{
		glUniform1i(L_KeyPressed_uniform, 1);

		//setting lights properties
		glUniform3fv(La_uniform, 1, light_ambient);
		glUniform3fv(Ld_uniform, 1, light_diffused);
		glUniform3fv(Ls_uniform, 1, light_specular);

		if (x_direction == true && y_direction == false && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrBlue);
		}
		else if (x_direction == false && y_direction == true && z_direction == false)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrRed);
		}
		else if (x_direction == false && y_direction == false && z_direction == true)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrGreen);
		}

		//setting material's properties
		glUniform3fv(Ka_uniform, 1, material_ambient5000);
		glUniform3fv(Kd_uniform, 1, material_diffused5001);
		glUniform3fv(Ks_uniform, 1, material_specular5002);
		glUniform1f(material_shininess_uniform, material_shinyness5003);
	}
	else
	{
		//set 'u_lighting_enabled' uniform
		glUniform1i(L_KeyPressed_uniform, 0);
	}

	//OpenGL Drawing
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 rotationMatrixBlue = mat4::identity(); //For x axis
	mat4 rotationMatrixRed = mat4::identity();
	mat4 rotationMatrixGreen = mat4::identity();

	modelMatrix = translate(0.4f, -0.725f, -3.0f);
	modelMatrix = modelMatrix*scale(0.3f, 0.3f, 0.3f);

	if (x_direction == true)
	{
		myrotationArrBlue[1] = (GLfloat)(light_position[0] * sin(angleBlue));
		myrotationArrBlue[2] = (GLfloat)(light_position[0] * cos(angleBlue));
		y_direction = false;
		z_direction = false;
	}
	else if (y_direction == true)
	{
		myrotationArrRed[0] = (GLfloat)(light_position[1] * sin(angleRed));
		myrotationArrRed[2] = (GLfloat)(light_position[1] * cos(angleRed));
		x_direction = false;
		z_direction = false;
	}
	else if (z_direction == true)
	{
		myrotationArrGreen[0] = (GLfloat)(light_position[2] * sin(angleGreen));
		myrotationArrGreen[1] = (GLfloat)(light_position[2] * cos(angleGreen));
		x_direction = false;
		y_direction = false;
	}

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_Sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_Sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
}

void display63()
{
	if (gbLight == true)
	{
		glUniform1i(L_KeyPressed_uniform, 1);

		//setting lights properties
		glUniform3fv(La_uniform, 1, light_ambient);
		glUniform3fv(Ld_uniform, 1, light_diffused);
		glUniform3fv(Ls_uniform, 1, light_specular);

		if (x_direction == true)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrBlue);
		}
		else if (y_direction == true)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrRed);
		}
		else if (z_direction == true)
		{
			glUniform4fv(light_position_uniform, 1, myrotationArrGreen);
		}

		//setting material's properties
		glUniform3fv(Ka_uniform, 1, material_ambient6000);
		glUniform3fv(Kd_uniform, 1, material_diffused6001);
		glUniform3fv(Ks_uniform, 1, material_specular6002);
		glUniform1f(material_shininess_uniform, material_shinyness6003);
	}
	else
	{
		//set 'u_lighting_enabled' uniform
		glUniform1i(L_KeyPressed_uniform, 0);
	}

	//OpenGL Drawing
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 rotationMatrixBlue = mat4::identity(); //For x axis
	mat4 rotationMatrixRed = mat4::identity();
	mat4 rotationMatrixGreen = mat4::identity();

	modelMatrix = translate(1.0f, -0.725f, -3.0f);
	modelMatrix = modelMatrix*scale(0.3f, 0.3f, 0.3f);

	if (x_direction == true)
	{
		myrotationArrBlue[1] = (GLfloat)(light_position[0] * sin(angleBlue));
		myrotationArrBlue[2] = (GLfloat)(light_position[0] * cos(angleBlue));
		y_direction = false;
		z_direction = false;
	}
	else if (y_direction == true)
	{
		myrotationArrRed[0] = (GLfloat)(light_position[1] * sin(angleRed));
		myrotationArrRed[2] = (GLfloat)(light_position[1] * cos(angleRed));
		x_direction = false;
		z_direction = false;
	}
	else if (z_direction == true)
	{
		myrotationArrGreen[0] = (GLfloat)(light_position[2] * sin(angleGreen));
		myrotationArrGreen[1] = (GLfloat)(light_position[2] * cos(angleGreen));
		x_direction = false;
		y_direction = false;
	}

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_Sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_Sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//start using OpenGL program object
	glUseProgram(gShaderProgramObject);

	display_corner();
	
	display11();
	display12();
	display13();
	display14();

	display20();
	display21();
	display22();
	display23();

	display30();
	display31();
	display32();
	display33();

	display40();
	display41();
	display42();
	display43();

	display50();
	display51();
	display52();
	display53();

	display60();
	display61();
	display62();
	display63();

	glBindVertexArray(0);
	glUseProgram(0);

	SwapBuffers(ghdc);
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	gPerspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void uninitialize(void)
{

	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
	if (gVao_Sphere)
	{
		glDeleteVertexArrays(1, &gVao_Sphere);
		gVao_Sphere = 0;
	}

	if (gVbo_Sphere_positions)
	{
		glDeleteBuffers(1, &gVbo_Sphere_positions);
		gVbo_Sphere_positions = 0;
	}

	if (gVbo_Sphere_normal)
	{
		glDeleteBuffers(1, &gVbo_Sphere_normal);
		gVbo_Sphere_normal = 0;
	}

	if (gVbo_Sphere_element)
	{
		glDeleteBuffers(1, &gVbo_Sphere_element);
		gVbo_Sphere_element = 0;
	}
	//detach vertex shader from shader program object
	glDetachShader(gShaderProgramObject, gVertexShaderObject);

	//detach fragment shader from shader program object
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	//delete vertex shader object
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;

	//delete fragment shader object
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	//delete shader program object
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	//unlink shader program
	glUseProgram(0);


	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;
}

void update()
{
	if (x_direction==true && y_direction==false && z_direction==false)
	{
		angleBlue = angleBlue + 0.2f;
		if (angleBlue > 360.0f)
		{
			angleBlue = 0.0f;
		}
	}

	if (x_direction == false && y_direction == true && z_direction == false)
	{
		angleRed = angleRed + 0.2f;
		if (angleRed > 360.0f)
		{
			angleRed = 0.0f;
		}
	}

	if (x_direction ==  false && y_direction == false && z_direction == true)
	{
		angleGreen = angleGreen + 0.2f;
		if (angleGreen > 360.0f)
		{
			angleGreen = 0.0f;
		}
	}
}






























