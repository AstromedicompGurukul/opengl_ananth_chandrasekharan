#pragma once
#include<Windows.h>
#include <gl/GL.h>

GLuint L_KeyPressed_uniform;

GLuint La_uniform;
GLuint Ld_uniform;
GLuint Ls_uniform;
GLuint light_position_uniform;

GLuint Ld_uniform1;
GLuint Ls_uniform1;
GLuint light_position_uniform1;


GLuint Ka_uniform;
GLuint Kd_uniform;
GLuint Ks_uniform;
GLuint material_shininess_uniform;

GLfloat lightAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat lightDiffuse[] = { 1.0f,0.0f,0.0f,0.0f };
GLfloat lightSpecular[] = { 1.0f,0.0f,0.0f,0.0f };
GLfloat lightPosition[] = { 1.0f,0.0f,0.0f,0.0f };

GLfloat lightDiffuse1[] = { 0.0f,0.0f,1.0f,0.0f };
GLfloat lightSpecular1[] = { 0.0f,0.0f,1.0f,0.0f };
GLfloat lightPosition1[] = { -1.0f,0.0f,0.0f,0.0f };

GLfloat material_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_shininess = 50.0f;

GLfloat angleSphere = 0.0f;

