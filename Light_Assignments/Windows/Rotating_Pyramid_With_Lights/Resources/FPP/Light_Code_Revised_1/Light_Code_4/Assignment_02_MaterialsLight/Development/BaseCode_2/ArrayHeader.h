#pragma once
#include<Windows.h>
#include<gl/GL.h>
#include <gl/GLU.h>

BOOL lightkey = FALSE;

//Defined quadric
GLUquadric *quadric = NULL;
GLUquadric *quadric1 = NULL;
GLUquadric *quadric2 = NULL;
GLUquadric *quadric3 = NULL;
GLUquadric *quadric4 = NULL;
GLUquadric *quadric5 = NULL;
GLUquadric *quadric6 = NULL;
GLUquadric *quadric7 = NULL;
GLUquadric *quadric8 = NULL;
GLUquadric *quadric9 = NULL;
GLUquadric *quadric10 = NULL;
GLUquadric *quadric11 = NULL;
GLUquadric *quadric12 = NULL;
GLUquadric *quadric13 = NULL;
GLUquadric *quadric14 = NULL;
GLUquadric *quadric15 = NULL;
GLUquadric *quadric16 = NULL;
GLUquadric *quadric17 = NULL;
GLUquadric *quadric18 = NULL;
GLUquadric *quadric19 = NULL;
GLUquadric *quadric20 = NULL;
GLUquadric *quadric21 = NULL;
GLUquadric *quadric22 = NULL;
GLUquadric *quadric23 = NULL;

//For Light 0 
GLfloat light_ambient[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat light_diffused[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat light_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat light_position[] = { 0.0f,0.0f,3.0f,1.0f };

GLfloat light_model_ambient[] = {0.2f,0.2f ,0.2f ,0.0f};
GLfloat light_model_local_viewer = 0.0f;


//1st sphere on 1st column emerald
GLfloat material_ambient10[] = { 0.0215f,0.1745f,0.0215f,1.0f };
GLfloat material_diffused11[] = { 0.07568f,0.61424f,0.07568f,1.0f };
GLfloat material_specular12[] = { 0.633f,0.0727811f,0.633f,1.0f };
GLfloat material_shinyness13 = (0.6f * 128);

//2nd sphere on 1st column jade
GLfloat material_ambient20[] = { 0.135f,0.2225f,0.1575f,1.0f };
GLfloat material_diffused21[] = { 0.54f,0.89f,0.63f,1.0f};
GLfloat material_specular22[] = {0.316228f,0.316228f,0.316228f,1.0f };
GLfloat material_shinyness23 = (0.1f * 128);

//3rd sphere on 1st column obsidium
GLfloat material_ambient30[] = { 0.05375f,0.05f,0.06625f,1.0f };
GLfloat material_diffused31[] = { 0.18275f,0.17f,0.22525f,1.0f };
GLfloat material_specular32[] = { 0.332741f,0.328634f,0.346435f,1.0f };
GLfloat material_shinyness33 = (0.3f * 128);

//4th sphere on 1st column pear1
GLfloat material_ambient40[] = { 0.25f,0.20725f,0.20725f,1.0f };
GLfloat material_diffused41[] = { 1.0f,0.829f,0.829f,1.0f };
GLfloat material_specular42[] = { 0.296648f,0.296648f,0.296648f,1.0f };
GLfloat material_shinyness43 = (0.088f * 128);

//----------------------------------------------------------------

//1st sphere on 2nd column ruby
GLfloat material_ambient50[] = { 0.1745f,0.01175f,0.01175f,1.0f };
GLfloat material_diffused51[] = { 0.61424f,0.04136f,0.04136f,1.0f };
GLfloat material_specular51[] = { 0.727811f,0.626959f,0.626959f,1.0f };
GLfloat material_shinyness53 = (0.6f * 128);

//2nd sphere on 2nd column turquoise
GLfloat material_ambient60[] = { 0.1f,0.18725f,0.1745f,1.0f };
GLfloat material_diffused61[] = { 0.396f,0.74151f,0.69102f,1.0f};
GLfloat material_specular62[] = { 0.297254f,0.30829f,0.306678f,1.0f };
GLfloat material_shinyness63 = (0.1f * 128);

//3rd sphere on 2nd column brass
GLfloat material_ambient70[] = { 0.329412f,0.223529f,0.027451f,1.0f };
GLfloat material_diffused71[] = { 0.780392f,0.568627f,0.113725f,1.0f };
GLfloat material_specular72[] = { 0.992157f,0.941176f,0.807843f,1.0f };
GLfloat material_shinyness73 = (0.21794872f * 128);

//4th sphere on 2nd column bronze
GLfloat material_ambient80[] = { 0.2125f,0.1275f,0.054f,1.0f };
GLfloat material_diffused81[] = {0.714f,0.4284f,0.18144f,1.0f };
GLfloat material_specular82[] = { 0.393548f,0.271906f,0.166721f,1.0f };
GLfloat material_shinyness83 = (0.2f * 128);

//----------------------------------------------------------------

//1st sphere on 3nd column chrome
GLfloat material_ambient90[] = {0.25f,0.25f,0.25f,1.0f };
GLfloat material_diffused91[] = { 0.4f,0.4f,0.4f,1.0f};
GLfloat material_specular92[] = { 0.774597f,0.774597f,0.774597f,1.0f };
GLfloat material_shinyness93 = (0.6f * 128);

//2nd sphere on 3nd column copper
GLfloat material_ambient100[] = { 0.19125f,0.0735f,0.0225f,1.0f };
GLfloat material_diffused101[] = { 0.7038f,0.27048f,0.0828f,1.0f};
GLfloat material_specular102[] = { 0.256777f,0.137622f,0.086014f,1.0f };
GLfloat material_shinyness103 = (0.1f * 128);

//3rd sphere on 3nd column silver
GLfloat material_ambient200[] = { 0.19225f,0.19225f,0.19225f,1.0f };
GLfloat material_diffused201[] = { 0.50754f,0.50754f,0.50754f,1.0f };
GLfloat material_specular202[] = { 0.508273f,0.508273f,0.508273f,1.0f };
GLfloat material_shinyness203 = (0.4f * 128);

//4th sphere on 3nd column black
GLfloat material_ambient300[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_diffused301[] = { 0.01f,0.01f,0.01f,1.0f };
GLfloat material_specular302[] = { 0.50f,0.50f,0.50f,1.0f };
GLfloat material_shinyness303 = (0.25f * 128);

//----------------------------------------------------------------

//1st sphere on 4th column cyan
GLfloat material_ambient400[] = { 0.0f,0.1f,0.06f,1.0f };
GLfloat material_diffused401[] = { 0.0f,0.50980392f,0.50980392f,1.0f };
GLfloat material_specular402[] = { 0.50196078f,0.50196078f,0.50196078f,1.0f };
GLfloat material_shinyness403 = (0.25f * 128);

//2nd sphere on 4th column green
GLfloat material_ambient500[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_diffused501[] = { 0.1f,0.35f,0.1f,1.0f };
GLfloat material_specular502[] = { 0.45f,0.55f,0.45f,1.0f };
GLfloat material_shinyness503 = (0.25f * 128);

//3rd sphere on 4th column red
GLfloat material_ambient600[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_diffused601[] = { 0.5f,0.0f,0.0f,1.0f };
GLfloat material_specular602[] = { 0.7f,0.6f,0.6f,1.0f };
GLfloat material_shinyness603 = (0.25f * 128);

//4rd sphere on 4th column white
GLfloat material_ambient700[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_diffused701[] = { 0.55f,0.55f,0.55f,1.0f };
GLfloat material_specular702[] = { 0.70f,0.70f,0.70f,1.0f };
GLfloat material_shinyness703 = (0.25f * 128);

//----------------------------------------------------------------

//1st sphere on 5th column yellow plastic
GLfloat material_ambient800[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_diffused801[] = { 0.5f,0.5f,0.0f,1.0f };
GLfloat material_specular802[] = { 0.60f,0.60f,0.50f,1.0f };
GLfloat material_shinyness803 = (0.25f * 128);

//2nd sphere on 5th column black
GLfloat material_ambient900[] = { 0.02f,0.02f,0.02f,1.0f };
GLfloat material_diffused901[] = { 0.01f,0.01f,0.01f,1.0f };
GLfloat material_specular902[] = { 0.40f,0.40f,0.40f,1.0f };
GLfloat material_shinyness903 = (0.078125f * 128);

//3rd sphere on 5th column cyan
GLfloat material_ambient1000[] = { 0.0f,0.05f,0.05f,1.0f };
GLfloat material_diffused1001[] = { 0.4f,0.5f,0.5f,1.0f };
GLfloat material_specular1002[] = { 0.04f,0.7f,0.7f,1.0f };
GLfloat material_shinyness1003 = (0.078125f * 128);

//4th sphere on 5th column red
GLfloat material_ambient2000[] = { 0.0f,0.05f,0.05f,1.0f };
GLfloat material_diffused2001[] = { 0.4f,0.5f,0.5f,1.0f };
GLfloat material_specular2002[] = { 0.04f,0.7f,0.7f,1.0f };
GLfloat material_shinyness2003 = (0.078125f * 128);

//----------------------------------------------------------------

//1st sphere on 6th column green
GLfloat material_ambient3000[] = { 0.0f,0.05f,0.0f,1.0f };
GLfloat material_diffused3001[] = { 0.4f,0.5f,0.4f,1.0f };
GLfloat material_specular3002[] = { 0.04f,0.7f,0.04f,1.0f };
GLfloat material_shinyness3003 = (0.078125f * 128);

//2nd sphere on 6th column red
GLfloat material_ambient4000[] = { 0.05f,0.0f,0.0f,1.0f };
GLfloat material_diffused4001[] = { 0.5f,0.4f,0.4f,1.0f };
GLfloat material_specular4002[] = { 0.7f,0.04f,0.04f,1.0f };
GLfloat material_shinyness4003 = (0.078125f * 128);

//3rd sphere on 6th column white
GLfloat material_ambient5000[] = { 0.05f,0.05f,0.05f,1.0f };
GLfloat material_diffused5001[] = { 0.5f,0.5f,0.5f,1.0f };
GLfloat material_specular5002[] = { 0.7f,0.7f,0.7f,1.0f };
GLfloat material_shinyness5003 = (0.078125f * 128);

//4th sphere on 6th column yellow rubber
GLfloat material_ambient6000[] = { 0.05f,0.05f,0.0f,1.0f };
GLfloat material_diffused6001[] = { 0.5f,0.5f,0.4f,1.0f };
GLfloat material_specular6002[] = { 0.7f,0.7f,0.04f,1.0f };
GLfloat material_shinyness6003 = (0.078125f * 128);


//----------------------------------------------------------------
