#include<Windows.h>
#include<gl/GL.h>
#include<gl/GLU.h>

#include "ArrayHeader.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib,"opengl32.lib")
#pragma comment (lib,"glu32.lib")

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullScreen = false;

GLfloat angleSpin = 0.0f;

LRESULT CALLBACK AcCallBack(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam);

void update(void)
{
	angleSpin = angleSpin + 1.0f;
	if (angleSpin >= 360.0f)
	{
		angleSpin = 0.0f;
	}
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);
	void resize(int, int);

	WNDCLASSEX ac;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("Rotating Pyramid");
	bool bDone = false;

	ac.cbSize = sizeof(WNDCLASSEX);
	ac.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	ac.cbClsExtra = 0;
	ac.cbWndExtra = 0;
	ac.hInstance = hInstance;
	ac.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	ac.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	ac.hCursor = LoadCursor(NULL, IDC_ARROW);
	ac.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	ac.lpfnWndProc = AcCallBack;
	ac.lpszClassName = szClassName;
	ac.lpszMenuName = NULL;

	RegisterClassEx(&ac);

	hwnd = CreateWindowEx(
		WS_EX_APPWINDOW,
		szClassName,
		TEXT("Shree Ganesha"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);

	ghwnd = hwnd;

	initialize();

	ShowWindow(hwnd, SW_SHOWNORMAL);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				display();
				update();
				
				if (gbEscapeKeyIsPressed == true)
				{
					bDone = true;
				}
			}
		}
	}
	uninitialize();
	return ((int)msg.wParam);
}

void initialize(void)
{
	void resize(GLint width, GLint height);

	PIXELFORMATDESCRIPTOR pfd;
	GLint iPixelFormatIndex;

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cBlueBits = 8;
	pfd.cGreenBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	//Defined for GL_LIGHT0
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffused);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);

	//Defined Background color
	glClearColor(0.25f, 0.25f, 0.25f, 0.0f);

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glEnable(GL_LIGHT0);
	glEnable(GL_AUTO_NORMAL);
	glEnable(GL_NORMALIZE);
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT,light_model_ambient);
	glLightModelf(GL_LIGHT_MODEL_LOCAL_VIEWER,light_model_local_viewer);

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void uninitialize(void)
{
	if (gbFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	if (quadric)
	{
		gluDeleteQuadric(quadric);
		quadric = NULL;
	}

	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(ghrc);
	ghrc = NULL;
	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;
	DestroyWindow(ghwnd);
	ghwnd = NULL;

}

void resize(GLint width, GLint height)
{
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void ToggleFullScreen(void)
{
	MONITORINFO mi;

	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle&WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle&~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//----------------------------------------
		glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient10);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffused11);
		glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular12);
		glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness13);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-0.8f, 0.9f, -3.0f);
		glScalef(0.2f, 0.2f, 0.2f);
		if (xkey == TRUE && ykey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 1.0f, 0.0f, 0.0f);
			light_position[0] = 0.0f;
			light_position[1] = angleSpin;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (ykey == TRUE && xkey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 1.0f, 0.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (zkey == TRUE && xkey == FALSE && ykey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 0.0f, 1.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		quadric = gluNewQuadric();
		gluSphere(quadric, 0.75f, 30, 30);

		glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient20);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffused21);
		glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular22);
		glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness23);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-0.2f, 0.9f, -3.0f);
		glScalef(0.2f, 0.2f, 0.2f);
		if (xkey == TRUE && ykey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 1.0f, 0.0f, 0.0f);
			light_position[0] = 0.0f;
			light_position[1] = angleSpin;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (ykey == TRUE && xkey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 1.0f, 0.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (zkey == TRUE && xkey == FALSE && ykey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 0.0f, 1.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		quadric1 = gluNewQuadric();
		gluSphere(quadric1, 0.75f, 30, 30);

		glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient30);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffused31);
		glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular32);
		glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness33);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(0.4f, 0.9f, -3.0f);
		glScalef(0.2f, 0.2f, 0.2f);
		if (xkey == TRUE && ykey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 1.0f, 0.0f, 0.0f);
			light_position[0] = 0.0f;
			light_position[1] = angleSpin;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (ykey == TRUE && xkey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 1.0f, 0.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (zkey == TRUE && xkey == FALSE && ykey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 0.0f, 1.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		quadric2 = gluNewQuadric();
		gluSphere(quadric2, 0.75f, 30, 30);

		glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient40);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffused41);
		glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular42);
		glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness43);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(1.0f, 0.9f, -3.0f);
		glScalef(0.2f, 0.2f, 0.2f);
		if (xkey == TRUE && ykey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 1.0f, 0.0f, 0.0f);
			light_position[0] = 0.0f;
			light_position[1] = angleSpin;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (ykey == TRUE && xkey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 1.0f, 0.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (zkey == TRUE && xkey == FALSE && ykey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 0.0f, 1.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}

		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		quadric3 = gluNewQuadric();
		gluSphere(quadric3, 0.75f, 30, 30);

		//----------------------------------------
		glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient50);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffused51);
		glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular52);
		glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness53);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-0.8f, 0.575f, -3.0f);
		glScalef(0.2f, 0.2f, 0.2f);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		quadric4 = gluNewQuadric();
		gluSphere(quadric4, 0.75f, 30, 30);

		glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient60);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffused61);
		glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular62);
		glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness63);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-0.2f, 0.575f, -3.0f);
		glScalef(0.2f, 0.2f, 0.2f);
		if (xkey == TRUE && ykey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 1.0f, 0.0f, 0.0f);
			light_position[0] = 0.0f;
			light_position[1] = angleSpin;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (ykey == TRUE && xkey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 1.0f, 0.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (zkey == TRUE && xkey == FALSE && ykey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 0.0f, 1.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}

		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		quadric5 = gluNewQuadric();
		gluSphere(quadric5, 0.75f, 30, 30);

		glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient70);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffused71);
		glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular72);
		glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness73);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(0.4f, 0.575f, -3.0f);
		glScalef(0.2f, 0.2f, 0.2f);
		if (xkey == TRUE && ykey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 1.0f, 0.0f, 0.0f);
			light_position[0] = 0.0f;
			light_position[1] = angleSpin;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (ykey == TRUE && xkey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 1.0f, 0.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (zkey == TRUE && xkey == FALSE && ykey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 0.0f, 1.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		quadric6 = gluNewQuadric();
		gluSphere(quadric6, 0.75f, 30, 30);

		glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient80);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffused81);
		glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular82);
		glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness83);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(1.0f, 0.575f, -3.0f);
		glScalef(0.2f, 0.2f, 0.2f);
		if (xkey == TRUE && ykey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 1.0f, 0.0f, 0.0f);
			light_position[0] = 0.0f;
			light_position[1] = angleSpin;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (ykey == TRUE && xkey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 1.0f, 0.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (zkey == TRUE && xkey == FALSE && ykey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 0.0f, 1.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		quadric7 = gluNewQuadric();
		gluSphere(quadric7, 0.75f, 30, 30);

		//----------------------------------------

		glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient90);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffused91);
		glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular92);
		glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness93);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-0.8f, 0.25f, -3.0f);
		glScalef(0.2f, 0.2f, 0.2f);
		if (xkey == TRUE && ykey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 1.0f, 0.0f, 0.0f);
			light_position[0] = 0.0f;
			light_position[1] = angleSpin;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (ykey == TRUE && xkey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 1.0f, 0.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (zkey == TRUE && xkey == FALSE && ykey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 0.0f, 1.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		quadric8 = gluNewQuadric();
		gluSphere(quadric8, 0.75f, 30, 30);

		glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient100);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffused101);
		glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular102);
		glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness103);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-0.2f, 0.25f, -3.0f);
		glScalef(0.2f, 0.2f, 0.2f);
		if (xkey == TRUE && ykey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 1.0f, 0.0f, 0.0f);
			light_position[0] = 0.0f;
			light_position[1] = angleSpin;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (ykey == TRUE && xkey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 1.0f, 0.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (zkey == TRUE && xkey == FALSE && ykey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 0.0f, 1.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		quadric9 = gluNewQuadric();
		gluSphere(quadric9, 0.75f, 30, 30);

		glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient200);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffused201);
		glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular202);
		glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness203);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(0.4f, 0.25f, -3.0f);
		glScalef(0.2f, 0.2f, 0.2f);
		if (xkey == TRUE && ykey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 1.0f, 0.0f, 0.0f);
			light_position[0] = 0.0f;
			light_position[1] = angleSpin;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (ykey == TRUE && xkey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 1.0f, 0.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (zkey == TRUE && xkey == FALSE && ykey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 0.0f, 1.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		quadric10 = gluNewQuadric();
		gluSphere(quadric10, 0.75f, 30, 30);

		glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient300);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffused301);
		glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular302);
		glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness303);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(1.0f, 0.25f, -3.0f);
		glScalef(0.2f, 0.2f, 0.2f);
		if (xkey == TRUE && ykey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 1.0f, 0.0f, 0.0f);
			light_position[0] = 0.0f;
			light_position[1] = angleSpin;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (ykey == TRUE && xkey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 1.0f, 0.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (zkey == TRUE && xkey == FALSE && ykey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 0.0f, 1.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		quadric11 = gluNewQuadric();
		gluSphere(quadric11, 0.75f, 30, 30);
		//----------------------------------------
		
		glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient400);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffused401);
		glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular402);
		glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness403);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-0.8f, -0.075f, -3.0f);
		glScalef(0.2f, 0.2f, 0.2f);
		if (xkey == TRUE && ykey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 1.0f, 0.0f, 0.0f);
			light_position[0] = 0.0f;
			light_position[1] = angleSpin;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (ykey == TRUE && xkey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 1.0f, 0.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (zkey == TRUE && xkey == FALSE && ykey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 0.0f, 1.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		quadric12 = gluNewQuadric();
		gluSphere(quadric12, 0.75f, 30, 30);

		glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient500);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffused501);
		glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular502);
		glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness503);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-0.2f, -0.075f, -3.0f);
		glScalef(0.2f, 0.2f, 0.2f);
		if (xkey == TRUE && ykey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 1.0f, 0.0f, 0.0f);
			light_position[0] = 0.0f;
			light_position[1] = angleSpin;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (ykey == TRUE && xkey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 1.0f, 0.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (zkey == TRUE && xkey == FALSE && ykey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 0.0f, 1.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		quadric13 = gluNewQuadric();
		gluSphere(quadric13, 0.75f, 30, 30);

		glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient600);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffused601);
		glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular602);
		glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness603);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(0.4f, -0.075f, -3.0f);
		glScalef(0.2f, 0.2f, 0.2f);
		if (xkey == TRUE && ykey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 1.0f, 0.0f, 0.0f);
			light_position[0] = 0.0f;
			light_position[1] = angleSpin;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (ykey == TRUE && xkey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 1.0f, 0.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (zkey == TRUE && xkey == FALSE && ykey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 0.0f, 1.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		quadric14 = gluNewQuadric();
		gluSphere(quadric14, 0.75f, 30, 30);

		glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient700);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffused701);
		glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular702);
		glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness703);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(1.0f, -0.075f, -3.0f);
		glScalef(0.2f, 0.2f, 0.2f);
		if (xkey == TRUE && ykey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 1.0f, 0.0f, 0.0f);
			light_position[0] = 0.0f;
			light_position[1] = angleSpin;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (ykey == TRUE && xkey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 1.0f, 0.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (zkey == TRUE && xkey == FALSE && ykey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 0.0f, 1.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		quadric15 = gluNewQuadric();
		gluSphere(quadric15, 0.75f, 30, 30);

		//----------------------------------------------
		glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient800);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffused801);
		glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular802);
		glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness803);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-0.8f, -0.4f, -3.0f);
		glScalef(0.2f, 0.2f, 0.2f);
		if (xkey == TRUE && ykey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 1.0f, 0.0f, 0.0f);
			light_position[0] = 0.0f;
			light_position[1] = angleSpin;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (ykey == TRUE && xkey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 1.0f, 0.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (zkey == TRUE && xkey == FALSE && ykey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 0.0f, 1.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		quadric16 = gluNewQuadric();
		gluSphere(quadric16, 0.75f, 30, 30);

		glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient900);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffused901);
		glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular902);
		glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness903);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-0.2f, -0.4f, -3.0f);
		glScalef(0.2f, 0.2f, 0.2f);
		if (xkey == TRUE && ykey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 1.0f, 0.0f, 0.0f);
			light_position[0] = 0.0f;
			light_position[1] = angleSpin;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (ykey == TRUE && xkey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 1.0f, 0.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (zkey == TRUE && xkey == FALSE && ykey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 0.0f, 1.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		quadric17 = gluNewQuadric();
		gluSphere(quadric17, 0.75f, 30, 30);

		glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient1000);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffused1001);
		glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular1002);
		glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness1003);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(0.4f, -0.4f, -3.0f);
		glScalef(0.2f, 0.2f, 0.2f);
		if (xkey == TRUE && ykey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 1.0f, 0.0f, 0.0f);
			light_position[0] = 0.0f;
			light_position[1] = angleSpin;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (ykey == TRUE && xkey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 1.0f, 0.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (zkey == TRUE && xkey == FALSE && ykey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 0.0f, 1.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		quadric18 = gluNewQuadric();
		gluSphere(quadric18, 0.75f, 30, 30);

		glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient2000);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffused2001);
		glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular2002);
		glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness2003);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(1.0f, -0.4f, -3.0f);
		glScalef(0.2f, 0.2f, 0.2f);
		if (xkey == TRUE && ykey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 1.0f, 0.0f, 0.0f);
			light_position[0] = 0.0f;
			light_position[1] = angleSpin;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (ykey == TRUE && xkey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 1.0f, 0.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (zkey == TRUE && xkey == FALSE && ykey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 0.0f, 1.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		quadric19 = gluNewQuadric();
		gluSphere(quadric19, 0.75f, 30, 30);

		//----------------------------------------------

		glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient3000);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffused3001);
		glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular3002);
		glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness3003);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-0.8f, -0.725f, -3.0f);
		glScalef(0.2f, 0.2f, 0.2f);
		if (xkey == TRUE && ykey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 1.0f, 0.0f, 0.0f);
			light_position[0] = 0.0f;
			light_position[1] = angleSpin;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (ykey == TRUE && xkey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 1.0f, 0.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (zkey == TRUE && xkey == FALSE && ykey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 0.0f, 1.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		quadric20 = gluNewQuadric();
		gluSphere(quadric20, 0.75f, 30, 30);

		glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient4000);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffused4001);
		glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular4002);
		glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness4003);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-0.2f, -0.725f, -3.0f);
		glScalef(0.2f, 0.2f, 0.2f);
		if (xkey == TRUE && ykey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 1.0f, 0.0f, 0.0f);
			light_position[0] = 0.0f;
			light_position[1] = angleSpin;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (ykey == TRUE && xkey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 1.0f, 0.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (zkey == TRUE && xkey == FALSE && ykey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 0.0f, 1.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		quadric21 = gluNewQuadric();
		gluSphere(quadric21, 0.75f, 30, 30);

		glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient5000);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffused5001);
		glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular5002);
		glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness5003);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(0.4f, -0.725f, -3.0f);
		glScalef(0.2f, 0.2f, 0.2f);
		if (xkey == TRUE && ykey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 1.0f, 0.0f, 0.0f);
			light_position[0] = 0.0f;
			light_position[1] = angleSpin;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (ykey == TRUE && xkey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 1.0f, 0.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (zkey == TRUE && xkey == FALSE && ykey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 0.0f, 1.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		quadric22 = gluNewQuadric();
		gluSphere(quadric22, 0.75f, 30, 30);

		glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient6000);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffused6001);
		glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular6002);
		glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness6003);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(1.0f, -0.725f, -3.0f);
		glScalef(0.2f, 0.2f, 0.2f);
		if (xkey == TRUE && ykey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 1.0f, 0.0f, 0.0f);
			light_position[0] = 0.0f;
			light_position[1] = angleSpin;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (ykey == TRUE && xkey == FALSE && zkey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 1.0f, 0.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}
		else if (zkey == TRUE && xkey == FALSE && ykey == FALSE)
		{
			glRotatef(angleSpin, 0.0f, 0.0f, 1.0f);
			light_position[0] = angleSpin;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		}

		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		quadric23 = gluNewQuadric();
		gluSphere(quadric23, 0.75f, 30, 30);

		//----------------------------------------------
	SwapBuffers(ghdc);
}

/*
void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//----------------------------------------
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient10);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffused11);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular12);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness13);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -3.0f);
	if (xkey == TRUE && ykey==FALSE && zkey==FALSE)
	{
		glRotatef(angleSpin, 1.0f, 0.0f, 0.0f);
		light_position[0] = 0.0f;
		light_position[1] = angleSpin;
		light_position[2] = 0.0f;
		glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	}
	else if (ykey == TRUE && xkey==FALSE && zkey==FALSE)
	{
		glRotatef(angleSpin, 0.0f, 1.0f, 0.0f);
		light_position[0] = angleSpin;
		light_position[1] = 0.0f;
		light_position[2] = 0.0f;
		glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	}
	else if (zkey == TRUE && xkey==FALSE && ykey==FALSE)
	{
		glRotatef(angleSpin, 0.0f, 0.0f, 1.0f);
		light_position[0] = angleSpin;
		light_position[1] = 0.0f;
		light_position[2] = 0.0f;
		glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	}
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.75f, 30, 30);
	//----------------------------------------------
	SwapBuffers(ghdc);
}
*/

LRESULT CALLBACK AcCallBack(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void uninitialize(void);
	void resize(GLint width, GLint height);
	void ToggleFullScreen(void);

	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
		{
			gbActiveWindow = true;
		}
		else
		{
			gbActiveWindow = false;
		}
		break;

	case WM_ERASEBKGND:
		return  0;

	case WM_LBUTTONDOWN:
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_CLOSE:
		uninitialize();
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			if (gbEscapeKeyIsPressed == false)
			{
				gbEscapeKeyIsPressed = true;
			}
			break;

		case 0x46:
			if (gbFullScreen == false)
			{
				ToggleFullScreen();
				gbFullScreen = true;
			}
			else
			{
				ToggleFullScreen();
				gbFullScreen = false;
			}
			break;

		case 0x4C:
			//L key
			switch (lightkey)
			{
			case TRUE:
				lightkey = FALSE;
				glDisable(GL_LIGHTING);
				break;

			case FALSE:
				lightkey = TRUE;
				glEnable(GL_LIGHTING);
				break;
			}
			break;

		case 0x57:  //w button  (Stop at position and Resume at original position)
			xkey = FALSE;
			ykey = FALSE;
			zkey = FALSE;
			light_position[0] = 0.0f;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			break;

		case 0x58: //x button
			xkey = TRUE;
			ykey = FALSE;
			zkey = FALSE;
			angleSpin = 0.0f;
			break;

		case 0x59: //y button
			xkey = FALSE;
			ykey = TRUE;
			zkey = FALSE;
			angleSpin = 0.0f;
			break;	

		case 0x5A: //z button
			xkey = FALSE;
			ykey = FALSE;
			zkey = TRUE;
			angleSpin = 0.0f;
			break;

		default:
			break;
		}
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}
	return DefWindowProc(hwnd, iMsg, wParam, lParam);
}