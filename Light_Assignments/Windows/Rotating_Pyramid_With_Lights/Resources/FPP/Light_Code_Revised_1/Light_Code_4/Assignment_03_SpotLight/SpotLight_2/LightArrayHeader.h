#pragma once
#include<Windows.h>
#include<gl/GL.h>
#include <gl/GLU.h>

/*
GLfloat light_ambient[] = {1.0f,0.0f,0.0f,1.0f};
GLfloat light_diffused[] = { 1.0f,0.0f,0.0f,1.0f };
GLfloat light_specular[] = {1.0f,1.0f,1.0f,1.0f };
GLfloat light_position[] = { 0.6f,1.0f,1.0f,0.0f };
GLfloat material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_shinyness = 50.0f;
*/


//Normal Light
GLfloat light0_ambient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat light0_diffuse[] = { 1.0f,0.0f,0.0f,0.0f };
GLfloat light0_position[] = { -1.0f,0.0f,0.0f,0.0f };

//Spot Light
GLfloat light1_ambient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat light1_diffuse[] = { 0.0f,0.0f,1.0f,0.0f };
GLfloat light1_position[] = { -1.0f,0.0f,-3.0f,1.0f }; //mistake here
GLfloat light1_direction[] = {1.0f,0.0f,0.0f,0.0f }; //mistake here


GLfloat material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_shinyness[] = { 50.0f };

BOOL light0_flag = false;
