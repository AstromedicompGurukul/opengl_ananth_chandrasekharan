#include<Windows.h>
#include<gl/GL.h>
#include<gl/GLU.h>

#include "LightArrayHeader.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib,"opengl32.lib")
#pragma comment (lib,"glu32.lib")

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullScreen = false;

GLfloat anglePyramid = 0.0f;
GLfloat angleCube = 0.0f;
GLfloat angleSphere = 0.0f;


//Defined quadric
GLUquadric *quadric = NULL; 

#define NUMBER_OF_ROWS 4
#define NUMBER_OF_COLOUMNS 4

LRESULT CALLBACK AcCallBack(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam);

void update(void)
{
	anglePyramid = anglePyramid + 0.05f;

	if (anglePyramid >= 360.0f)
	{
		anglePyramid = 0.0f;
	}

	angleCube = angleCube + 0.05f;
	if (angleCube>=360.0f)
	{
		angleCube = 0.0f;
	}

	angleSphere = angleSphere + 0.05f;
	if (angleSphere >= 360.0f)
	{
		angleSphere = 0.0f;
	}
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);
	void resize(int, int);

	WNDCLASSEX ac;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("Rotating Pyramid");
	bool bDone = false;

	ac.cbSize = sizeof(WNDCLASSEX);
	ac.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	ac.cbClsExtra = 0;
	ac.cbWndExtra = 0;
	ac.hInstance = hInstance;
	ac.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	ac.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	ac.hCursor = LoadCursor(NULL, IDC_ARROW);
	ac.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	ac.lpfnWndProc = AcCallBack;
	ac.lpszClassName = szClassName;
	ac.lpszMenuName = NULL;

	RegisterClassEx(&ac);

	hwnd = CreateWindowEx(
		WS_EX_APPWINDOW,
		szClassName,
		TEXT("Shree Ganesha"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);

	ghwnd = hwnd;

	initialize();

	ShowWindow(hwnd, SW_SHOWNORMAL);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				update();
				display();
				if (gbEscapeKeyIsPressed == true)
				{
					bDone = true;
				}
			}
		}
	}
	uninitialize();
	return ((int)msg.wParam);
}

void initialize(void)
{
	void resize(GLint width, GLint height);

	PIXELFORMATDESCRIPTOR pfd;
	GLint iPixelFormatIndex;

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cBlueBits = 8;
	pfd.cGreenBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	//Defined for GL_LIGHT0
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient0);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffused0);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular0);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position0);

	//Defined for GL_LIGHT1
	glLightfv(GL_LIGHT1, GL_AMBIENT, light_ambient1);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, light_diffused1);
	glLightfv(GL_LIGHT1, GL_SPECULAR, light_specular1);
	glLightfv(GL_LIGHT1, GL_POSITION, light_position1);

	//Defined for Material
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffused);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void uninitialize(void)
{
	if (gbFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	if (quadric)
	{
		gluDeleteQuadric(quadric);
		quadric = NULL;
	}

	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(ghrc);
	ghrc = NULL;
	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;
	DestroyWindow(ghwnd);
	ghwnd = NULL;

}

void resize(GLint width, GLint height)
{
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void ToggleFullScreen(void)
{
	MONITORINFO mi;

	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle&WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle&~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	if (bCube==GL_TRUE)
	{
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(0.0f, 0.0f, -6.0f);
		glScalef(0.75f, 0.75f, 0.75f);
		glRotatef(angleCube, 0.0f, 1.0f, 0.0f);

		glBegin(GL_QUADS);

		//****TOP FACE****
		glNormal3f(0.0f, 1.0f, 0.0f); //normal for top face of cube
		glVertex3f(1.0f, 1.0f, -1.0f); //right-top of top face
		glVertex3f(-1.0f, 1.0f, -1.0f); //left-top of top face
		glVertex3f(-1.0f, 1.0f, 1.0f); //left-bottom of top face
		glVertex3f(1.0f, 1.0f, 1.0f); //right-bottom of top face

		//****BOTTOM FACE****
		glNormal3f(0.0f, -1.0f, 0.0f); //normal for bottom face of cube
		glVertex3f(1.0f, -1.0f, 1.0f); //right-top of bottom face
		glVertex3f(-1.0f, -1.0f, 1.0f); //left-top of bottom face 
		glVertex3f(-1.0f, -1.0f, -1.0f); //left-bottom of bottom face
		glVertex3f(1.0f, -1.0f, -1.0f); //right-bottm of bottom face

		//****FRONT FACE****
		glNormal3f(0.0f, 0.0f, 1.0f); //normal for front face of cube
		glVertex3f(1.0f, 1.0f, 1.0f); //right-top of front face
		glVertex3f(-1.0f, 1.0f, 1.0f); //left-top of front face
		glVertex3f(-1.0f, -1.0f, 1.0f); //left-bottom of front face
		glVertex3f(1.0f, -1.0f, 1.0f); //right-bottom of front face

		//****BACK FACE****
		glNormal3f(0.0f, 0.0f, -1.0f); //normal for back face of cube
		glVertex3f(1.0f, -1.0f, -1.0f); //right-top of back face
		glVertex3f(-1.0f, -1.0f, -1.0f); //left-top of back face
		glVertex3f(-1.0f, 1.0f, -1.0f); //left-bottom of back face
		glVertex3f(1.0f, 1.0f, -1.0f); //right-bottom of back face

		//****LEFT FACE****
		glNormal3f(-1.0f, 0.0f, 0.0f); //normal for left face of cube
		glVertex3f(-1.0f, 1.0f, 1.0f); //right-top of left face
		glVertex3f(-1.0f, 1.0f, -1.0f); //left-top of left face
		glVertex3f(-1.0f, -1.0f, -1.0f); //left-bottom of left face
		glVertex3f(-1.0f, -1.0f, 1.0f); //right-bottom of left face

		//****RIGHT FACE****
		glNormal3f(1.0f, 0.0f, 0.0f); //normal for right face of cube
		glVertex3f(1.0f, 1.0f, -1.0f); //right-top of right face
		glVertex3f(1.0f, 1.0f, 1.0f); //left-top of right face
		glVertex3f(1.0f, -1.0f, 1.0f); //left-bottom of right face
		glVertex3f(1.0f, -1.0f, -1.0f); //right-bottom of right face

		glEnd();

	}
	else if (bPyramid==GL_TRUE)
	{
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		glTranslatef(0.0f, 0.0f, -6.0f);
		
		glRotatef(anglePyramid, 0.0f, 1.0f, 0.0f);
		
		glBegin(GL_TRIANGLES);

		//****FRONT FACE****
		glNormal3f(0.0f, 0.447214f, 0.894427f); //normal for front face of pyramid
		glVertex3f(0.0f, 1.0f, 0.0f); //apex of triangle
		glVertex3f(-1.0f, -1.0f, 1.0f); //left-bottom tip of triangle
		glVertex3f(1.0f, -1.0f, 1.0f); //right-bottom tip of triangle

		//****BACK FACE****
		glNormal3f(0.0f, 0.447214f, -0.894427f); //normal for back face of pyramid
		glVertex3f(0.0f, 1.0f, 0.0f); //apex of triangle
		glVertex3f(1.0f, -1.0f, -1.0f); //left-bottom tip of triangle
		glVertex3f(-1.0f, -1.0f, -1.0f); //right-bottom tip of triangle

		//****RIGHT FACE****
		glNormal3f(0.894427f, 0.447214f, 0.0f); //normal for right face of pyramid
		glVertex3f(0.0f, 1.0f, 0.0f); //apex of triangle
		glVertex3f(1.0f, -1.0f, 1.0f); //left-bottom tip of triangle
		glVertex3f(1.0f, -1.0f, -1.0f); //right-bottom tip of triangle

		//****LEFT FACE****
		glNormal3f(-0.894427f, 0.447214f, 0.0f); //normal for left face of pyramid
		glVertex3f(0.0f, 1.0f, 0.0f); //apex of triangle
		glVertex3f(-1.0f, -1.0f, -1.0f); //left-bottom tip of triangle
		glVertex3f(-1.0f, -1.0f, 1.0f); //right-bottom tip of triangle
		
		glEnd();

	}
	else if (bSphere==GL_TRUE)
	{
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(0.0f, 0.0f, -3.0f);
		glRotatef(angleSphere,0.0f,1.0f,0.0f);

		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		quadric = gluNewQuadric();
		//glColor3f(1.0f, 1.0f, 1.0f);
		gluSphere(quadric, 0.75f, 30, 30);
	}
	else
	{

	}
	SwapBuffers(ghdc);
}

LRESULT CALLBACK AcCallBack(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void uninitialize(void);
	void resize(GLint width, GLint height);
	void ToggleFullScreen(void);

	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
		{
			gbActiveWindow = true;
		}
		else
		{
			gbActiveWindow = false;
		}
		break;

	case WM_ERASEBKGND:
		return  0;

	case WM_LBUTTONDOWN:
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_CLOSE:
		uninitialize();
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			if (gbEscapeKeyIsPressed == false)
			{
				gbEscapeKeyIsPressed = true;
			}
			break;

		case 0x46:
			if (gbFullScreen == false)
			{
				ToggleFullScreen();
				gbFullScreen = true;
			}
			else
			{
				ToggleFullScreen();
				gbFullScreen = false;
			}
			break;

		case 0x4C:
			//L key
			switch (lightkey)
			{
			case TRUE:
				lightkey = FALSE;
				//glEnable(GL_LIGHT0);
				//glEnable(GL_LIGHT1);
				glDisable(GL_LIGHTING);
				break;

			case FALSE:
				lightkey = TRUE;
				//glEnable(GL_LIGHT0);
				//glEnable(GL_LIGHT1);
				glEnable(GL_LIGHTING);
				break;
			}
			break;

		case 0x43:
			//C key
			if (bCube==GL_FALSE)
			{
				bCube = GL_TRUE;
				bSphere = GL_FALSE;
				bPyramid = GL_FALSE;
			}
			else
			{
				bCube = GL_FALSE;
				bSphere = GL_FALSE;
				bPyramid = GL_FALSE;
			}
			break;

		case 0x50:
			//P key
			if (bPyramid == GL_FALSE)
			{
				bCube = GL_FALSE;
				bSphere = GL_FALSE;
				bPyramid = GL_TRUE;
			}
			else
			{
				bCube = GL_FALSE;
				bSphere = GL_FALSE;
				bPyramid = GL_FALSE;
			}
			break;

		case 0x53:
			//S key
			if (bSphere == GL_FALSE)
			{
				bCube = GL_FALSE;
				bSphere = GL_TRUE;
				bPyramid = GL_FALSE;
			}
			else
			{
				bCube = GL_FALSE;
				bSphere = GL_FALSE;
				bPyramid = GL_FALSE;
			}
			break;

		default:
			break;
		}
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}
	return DefWindowProc(hwnd, iMsg, wParam, lParam);
}