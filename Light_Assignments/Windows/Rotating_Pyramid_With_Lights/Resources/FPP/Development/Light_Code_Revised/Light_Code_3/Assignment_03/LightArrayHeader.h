#pragma once
#include<Windows.h>
#include<gl/GL.h>
#include <gl/GLU.h>

BOOL lightkey = FALSE; //Lighting Key
BOOL Pkey = FALSE;  //Pyramid key
BOOL Ckey = FALSE; //Cube key
BOOL Skey = FALSE; //Sphere key

GLboolean bPyramid = GL_FALSE;
GLboolean bCube = GL_FALSE;
GLboolean bSphere = GL_FALSE;

//For Light 0 (Red Light)
GLfloat light_ambient0[] = { 0.0f,0.0f,0.0f,0.0f }; //Has to be zero 
GLfloat light_diffused0[] = { 1.0f,0.0f,0.0f,0.0f }; //decides color of light (Red)
GLfloat light_specular0[] = { 1.0f,0.0f,0.0f,0.0f }; //decides color of light (Red)
GLfloat light_position0[] = { 2.0f,1.0f,1.0f,0.0f };


//For Light 1 (Blue Light)
GLfloat light_ambient1[] = { 0.0f,0.0f,0.0f,0.0f }; //Has to be zero
GLfloat light_diffused1[] = { 0.0f,0.0f,1.0f,0.0f }; //decides color of light (Blue)
GLfloat light_specular1[] = { 0.0f,0.0f,1.0f,0.0f }; //decides color of light (Blue)
GLfloat light_position1[] = { -2.0f,1.0f,1.0f,0.0f };

//For material
GLfloat material_ambient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat material_diffused[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_shinyness = 50.0f;

