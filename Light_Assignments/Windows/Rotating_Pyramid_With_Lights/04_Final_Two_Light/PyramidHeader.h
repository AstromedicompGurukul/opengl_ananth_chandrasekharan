#pragma once
#include<Windows.h>
#include <gl/GL.h>

GLuint gVao_Pyramid;
GLuint gVbo_Pyramid_Position;
GLuint gVbo_Pyramid_Normals;
GLfloat gAnglePyramid = 0.0f;

const GLfloat PyramidVertices[] =
{
	0, 1, 0,
	-1, -1, 1,
	1, -1, 1,

	0, 1, 0,
	1, -1, 1,
	1, -1, -1,

	0, 1, 0,
	1, -1, -1,
	-1, -1, -1,

	0, 1, 0,
	-1, -1, -1,
	-1, -1, 1
};

/*
const GLfloat PyramidNormals[]=
{
	0.0f, 0.0f, 1.0f,
	0.0f, 0.0f, -1.0f,
	1.0f, 0.0f, 0.0f,
	-1.0f, 0.0f, 0.0f,

	0.0f, 0.0f, 1.0f,
	0.0f, 0.0f, -1.0f,
	1.0f, 0.0f, 0.0f,
	-1.0f, 0.0f, 0.0f,

	0.0f, 0.0f, 1.0f,
	0.0f, 0.0f, -1.0f,
	1.0f, 0.0f, 0.0f,
	-1.0f, 0.0f, 0.0f,

	0.0f, 0.0f, 1.0f,
	0.0f, 0.0f, -1.0f,
	1.0f, 0.0f, 0.0f,
	-1.0f, 0.0f, 0.0f
};
*/

const GLfloat PyramidNormals[] =
{
	0.0f, 0.447214f, 0.894427f,
	0.0f, 0.447214f, -0.894427f,
	0.0f, 0.447214f, -0.894427f,
	- 0.894427f, 0.447214f, 0.0f,

	0.0f, 0.447214f, 0.894427f,
	0.0f, 0.447214f, -0.894427f,
	0.0f, 0.447214f, -0.894427f,
	-0.894427f, 0.447214f, 0.0f,

	0.0f, 0.447214f, 0.894427f,
	0.0f, 0.447214f, -0.894427f,
	0.0f, 0.447214f, -0.894427f,
	-0.894427f, 0.447214f, 0.0f,
};