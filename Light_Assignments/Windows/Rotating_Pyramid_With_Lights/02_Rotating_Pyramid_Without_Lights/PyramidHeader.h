#pragma once
#include<Windows.h>
#include <gl/GL.h>

GLuint gVao_Pyramid;
GLuint gVbo_Pyramid_Position;
GLfloat gAnglePyramid = 0.0f;

const GLfloat PyramidVertices[] =
{
	0, 1, 0,
	-1, -1, 1,
	1, -1, 1,

	0, 1, 0,
	1, -1, 1,
	1, -1, -1,

	0, 1, 0,
	1, -1, -1,
	-1, -1, -1,

	0, 1, 0,
	-1, -1, -1,
	-1, -1, 1
};