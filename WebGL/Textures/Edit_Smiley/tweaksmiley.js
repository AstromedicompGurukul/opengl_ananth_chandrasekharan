//global variables
var canvas = null;
var gl = null;
var bFullscreen = false;
var digitKeyIsPressed = 0;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros = 
{
	ARD_ATTRIBUTE_VERTEX:0,
	ARD_ATTRIBUTE_COLOR:1,
	ARD_ATTRIBUTE_NORMAL:2,
	ARD_ATTRIBUTE_TEXTURE0:3,
};

var checkImageWidth = 64;
var checkImageHeight = 64;
var checkImage = new Uint8Array(16384);
var texName;

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vaoSmiley;

var vboSmileyPosition;
var vboSmileyTexture;

var smiley_Texture;
var uniform_texture0_sampler;

var mvpUniform;

var perspectiveProjectionMatrix;

//To start animation : To have requestAnimation to be 
//called "cross-browser" compatible

var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

var cancelAnimationFrame = 
window.cancelAnimationFrame ||
window.webkitCancelRequestAnimationFrame ||
window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame ||
window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame ||
window.oCancelAnimationFrame ||
window.msCanceclRequestAnimationFrame ||
window.msCancelAnimationFrame;

function main()
{
	canvas = document.getElementById("OGL");
	if(!canvas)
		console.log("Obtaining Canvas Failed\n");
	else
		console.log("Obtaining Canvas Succeeded\n");
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;

	//register keyboard's keydown event handler
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	//initialize WebGL
	init();

	//start drawing here as warming-up
	resize();
	draw();
}

function toggleFullscreen()
{
	var fullscreen_element = 
	document.fullscreenElement ||
	document.webkitFullscreenElement ||
	document.mozFullScreenElement ||
	document.msFullscreenElement ||
	null;

	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullscreen = true;
	}

	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullscreen = false;
	}
}

function init()
{
	gl = canvas.getContext("webgl2");
	if(gl == null)
	{
		console.log("Failed to get the rendering context for WebGL2");
		return;
	}

	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;

	//vertex shader
	var vertexShaderSourceCode = 
	"#version 300 es"+
	"\n"+
	"in vec4 vPosition;"+
	"in vec2 vTexture0_Coord;"+
	"out vec2 out_texture0_coord;"+
	"uniform mat4 u_mvp_matrix;"+
	"out vec4 outColor;"+
	"void main(void)"+
	"{"+
	"gl_Position = u_mvp_matrix * vPosition;"+
	"out_texture0_coord = vTexture0_Coord;"+
	"}";

	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}	

	//fragment Shader
	var fragmentShaderSourceCode = 
	"#version 300 es"+
	"\n"+
	"precision highp float;"+
	"in vec2 out_texture0_coord;"+
	"out vec4 FragColor;"+
	"uniform sampler2D u_texture0_sampler;"+
	"void main(void)"+
	"{"+
	"FragColor = texture(u_texture0_sampler, out_texture0_coord);"+
	"}"

	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject, fragmentShaderSourceCode);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//shader Program
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);

	//pre-link binding of shader program object with vertex shader attributes
	
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.ARD_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.ARD_ATTRIBUTE_TEXTURE0, "vTexture0_Coord");
	gl.linkProgram(shaderProgramObject);
	if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//Load Smiley Texture
	smiley_Texture = gl.createTexture();
	smiley_Texture.image = new Image();
	smiley_Texture.image.src = "smiley_512x512.png";
	smiley_Texture.image.onload = function()
	{
		gl.bindTexture(gl.TEXTURE_2D, smiley_Texture);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, smiley_Texture.image);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.bindTexture(gl.TEXTURE_2D, null);
	}

	loadGLTexturesWhiteSquare();

	//get MVP Uniform Location

	mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");
	uniform_texture0_sampler = gl.getUniformLocation(shaderProgramObject, "u_texture0_sampler");
	//vertices, colors, shader attribs, vao, vbo initialization
	var smileyVertices = new Float32Array([1.0,1.0,0.0,
		-1.0,1.0,0.0,
		-1.0,-1.0,0.0,
		1.0,-1.0,0.0 
	]);

	
	vaoSmiley = gl.createVertexArray();
	gl.bindVertexArray(vaoSmiley);

	vboSmileyPosition = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboSmileyPosition);
	gl.bufferData(gl.ARRAY_BUFFER, smileyVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.ARD_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.ARD_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	vboSmileyTexture = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboSmileyTexture);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(), gl.DYNAMIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.ARD_ATTRIBUTE_TEXTURE0, 2, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.ARD_ATTRIBUTE_TEXTURE0);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindVertexArray(null);

	//gl.shadeModel(gl.SMOOTH);
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);

	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	if(bFullscreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}

	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	//set viewport 
	gl.viewport(0, 0, canvas.width, canvas.height);

	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
}

function draw()
{
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	gl.useProgram(shaderProgramObject);

	var modelViewMatrix = mat4.create();
	var modelViewProjectionMatrix = mat4.create();
	//var rotationMatrix = mat4.create();
	//var translationMatrix = mat4.create();
	//mat4.translate(translationMatrix, translationMatrix, [0.0, 0.0, -3.0]);
	//mat4.multiply(modelViewMatrix, modelViewMatrix, translationMatrix);

	mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -4.0]);
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	gl.bindVertexArray(vaoSmiley);
	var smileyTexcoords = new Float32Array(8);

	var smileyTexcoordsSquare = new Float32Array([1.0, 1.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0]);

	switch(digitKeyIsPressed)
	{
		case 1:
			smileyTexcoords[0] = 0.5;
			smileyTexcoords[1] = 0.5;
			smileyTexcoords[2] = 0.0;
			smileyTexcoords[3] = 0.5;

			smileyTexcoords[4] = 0.0;
			smileyTexcoords[5] = 0.0;

			smileyTexcoords[6] = 0.5;
			smileyTexcoords[7] = 0.0;

			gl.bindBuffer(gl.ARRAY_BUFFER, vboSmileyTexture);
			gl.bufferData(gl.ARRAY_BUFFER, smileyTexcoords, gl.DYNAMIC_DRAW);
			gl.bindBuffer(gl.ARRAY_BUFFER, null);

			gl.bindTexture(gl.TEXTURE_2D, smiley_Texture);
			gl.uniform1i(uniform_texture0_sampler, 0);
			gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);

		break;

		case 2:
			smileyTexcoords[0] = 1.0;
			smileyTexcoords[1] = 1.0;
			smileyTexcoords[2] = 0.0;
			smileyTexcoords[3] = 1.0;

			smileyTexcoords[4] = 0.0;
			smileyTexcoords[5] = 0.0;

			smileyTexcoords[6] = 1.0;
			smileyTexcoords[7] = 0.0;

			gl.bindBuffer(gl.ARRAY_BUFFER, vboSmileyTexture);
			gl.bufferData(gl.ARRAY_BUFFER, smileyTexcoords, gl.DYNAMIC_DRAW);
			gl.bindBuffer(gl.ARRAY_BUFFER, null);

			gl.bindTexture(gl.TEXTURE_2D, smiley_Texture);
			gl.uniform1i(uniform_texture0_sampler, 0);
			gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);

		break;


		case 3:
			smileyTexcoords[0] = 2.0;
			smileyTexcoords[1] = 2.0;
			smileyTexcoords[2] = 0.0;
			smileyTexcoords[3] = 2.0;

			smileyTexcoords[4] = 0.0;
			smileyTexcoords[5] = 0.0;

			smileyTexcoords[6] = 2.0;
			smileyTexcoords[7] = 0.0;

			gl.bindBuffer(gl.ARRAY_BUFFER, vboSmileyTexture);
			gl.bufferData(gl.ARRAY_BUFFER, smileyTexcoords, gl.DYNAMIC_DRAW);
			gl.bindBuffer(gl.ARRAY_BUFFER, null);

			gl.bindTexture(gl.TEXTURE_2D, smiley_Texture);
			gl.uniform1i(uniform_texture0_sampler, 0);
			gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);

		break;


		case 4:
			smileyTexcoords[0] = 0.5;
			smileyTexcoords[1] = 0.5;
			smileyTexcoords[2] = 0.5;
			smileyTexcoords[3] = 0.5;

			smileyTexcoords[4] = 0.5;
			smileyTexcoords[5] = 0.5;

			smileyTexcoords[6] = 0.5;
			smileyTexcoords[7] = 0.5;

			gl.bindBuffer(gl.ARRAY_BUFFER, vboSmileyTexture);
			gl.bufferData(gl.ARRAY_BUFFER, smileyTexcoords, gl.DYNAMIC_DRAW);
			gl.bindBuffer(gl.ARRAY_BUFFER, null);

			gl.bindTexture(gl.TEXTURE_2D, smiley_Texture);
			gl.uniform1i(uniform_texture0_sampler, 0);
			gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);

		break;

		case 0:
		
			gl.bindBuffer(gl.ARRAY_BUFFER, vboSmileyTexture);
			gl.bufferData(gl.ARRAY_BUFFER, smileyTexcoordsSquare, gl.DYNAMIC_DRAW);
			gl.bindBuffer(gl.ARRAY_BUFFER, null);

			gl.bindTexture(gl.TEXTURE_2D, texName);
			gl.uniform1i(uniform_texture0_sampler, 0);
			gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);

		break;
	}

	gl.bindVertexArray(null);

	gl.useProgram(null);
	requestAnimationFrame(draw, canvas);
}

function uninitialize()
{
	if(vaoSmiley)
	{
		gl.deleteVertexArray(vaoSmiley);
		vaoSmiley = null;
	}

	if(smiley_Texture)
	{
		gl.deleteTexture(smiley_Texture);
		smiley_Texture = null;
	}

	if(vboSmileyPosition)
	{
		gl.deleteBuffer(vboSmileyPosition);
		vboSmileyPosition = null;
	}

	if(vboSmileyTexture)
	{
		gl.deleteBuffer(vboSmileyTexture);
		vboSmileyTexture = null;
	}

	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}

		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}

function keyDown(event)
{
	switch(event.key)
	{
	
		case 'F':
		case 'f':
		toggleFullscreen();
		break;

		case 'A':
		case 'a':
			digitKeyIsPressed = 1;
		break;

		case 'B':
		case 'b':
			digitKeyIsPressed = 2;
		break;

		case 'C':
		case 'c':
			digitKeyIsPressed = 3;
		break;

		case 'D':
		case 'd':
			digitKeyIsPressed = 4;
		break;

		default:
			digitKeyIsPressed = 0;
		break;
	}

	switch(event.keyCode)
	{
		case 27:
		uninitialize();
		window.close();
	}
}

function mouseDown()
{

}

function loadGLTexturesWhiteSquare()
{
	makeCheckImage();

	gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);

	texName = gl.createTexture();

	gl.bindTexture(gl.TEXTURE_2D, texName);

	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);

	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, checkImageWidth, checkImageHeight, 0, gl.RGBA, gl.UNSIGNED_BYTE, checkImage);
}

function makeCheckImage()
{
	var i, j;
	var n = 0;

	for( i = 0; i < 64; i++)
	{
		for( j = 0; j < 64; j++)
		{
			c = (((i & 0x8) == 0) ^ ((j & 0X8) == 0)) * 255;
			checkImage[n++] = 255;
			checkImage[n++] = 255;
			checkImage[n++] = 255;
			checkImage[n++] = 255;
	
		}
	}
}