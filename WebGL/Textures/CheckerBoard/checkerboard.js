//global variables
var canvas = null;
var gl = null;
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;
var array_buffer = new Float32Array();
const WebGLMacros = 
{
	ARD_ATTRIBUTE_VERTEX:0,
	ARD_ATTRIBUTE_COLOR:1,
	ARD_ATTRIBUTE_NORMAL:2,
	ARD_ATTRIBUTE_TEXTURE0:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var checkImageWidth = 64;
var checkImageHeight = 64;
var checkImage = new Uint8Array(16384);

//var checkerBoardVertices;

//var checkImage[]

var vaoCheckerBoard;

var vboCheckerPosition;
var vboCheckerTexture;

var texName;
var uniform_texture0_sampler;

var mvpUniform;

var perspectiveProjectionMatrix;

//To start animation : To have requestAnimation to be 
//called "cross-browser" compatible

var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

var cancelAnimationFrame = 
window.cancelAnimationFrame ||
window.webkitCancelRequestAnimationFrame ||
window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame ||
window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame ||
window.oCancelAnimationFrame ||
window.msCanceclRequestAnimationFrame ||
window.msCancelAnimationFrame;

function main()
{
	canvas = document.getElementById("OGL");
	if(!canvas)
		console.log("Obtaining Canvas Failed\n");
	else
		console.log("Obtaining Canvas Succeeded\n");
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;

	//register keyboard's keydown event handler
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	//initialize WebGL
	init();

	//start drawing here as warming-up
	resize();
	draw();
}

function toggleFullscreen()
{
	var fullscreen_element = 
	document.fullscreenElement ||
	document.webkitFullscreenElement ||
	document.mozFullScreenElement ||
	document.msFullscreenElement ||
	null;

	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullscreen = true;
	}

	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullscreen = false;
	}
}

function init()
{
	gl = canvas.getContext("webgl2");
	if(gl == null)
	{
		console.log("Failed to get the rendering context for WebGL2");
		return;
	}

	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;

	//vertex shader
	var vertexShaderSourceCode = 
	"#version 300 es"+
	"\n"+
	"in vec4 vPosition;"+
	"in vec2 vTexture0_Coord;"+
	"out vec2 out_texture0_coord;"+
	"uniform mat4 u_mvp_matrix;"+
	"out vec4 outColor;"+
	"void main(void)"+
	"{"+
	"gl_Position = u_mvp_matrix * vPosition;"+
	"out_texture0_coord = vTexture0_Coord;"+
	"}";

	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}	

	//fragment Shader
	var fragmentShaderSourceCode = 
	"#version 300 es"+
	"\n"+
	"precision highp float;"+
	"in vec2 out_texture0_coord;"+
	"out vec4 FragColor;"+
	"uniform sampler2D u_texture0_sampler;"+
	"void main(void)"+
	"{"+
	"FragColor = texture(u_texture0_sampler, out_texture0_coord);"+
	"}"

	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject, fragmentShaderSourceCode);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//shader Program
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);

	//pre-link binding of shader program object with vertex shader attributes
	
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.ARD_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.ARD_ATTRIBUTE_TEXTURE0, "vTexture0_Coord");
	gl.linkProgram(shaderProgramObject);
	if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//Load Smiley Texture
	//texName = gl.createTexture();
	//texName.image = new Image();
	//smiley_Texture.image.src = "Smiley.png";
/*
	texName.image.onload = function()
	{
		makeCheckImage();
		arrayGen();
		gl.bindTexture(gl.TEXTURE_2D, texName);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texName.array_buffer);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.bindTexture(gl.TEXTURE_2D, null);
	}*/

	//get MVP Uniform Location

	mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");
	uniform_texture0_sampler = gl.getUniformLocation(shaderProgramObject, "u_texture0_sampler");
	//vertices, colors, shader attribs, vao, vbo initialization
	
	var checkerBoardTexture = new Float32Array([
		1.0,1.0,
		0.0,1.0,
		0.0,0.0,
		1.0,0.0
	]);
	//checkerBoardVertices = [ 0.0, 1.0, 0.0, -2.0, 1.0, 0.0, -2.0, -1.0, 0.0, 0.0, -1.0, 0.0];
	
	//checkerBoardVertices = new Float32Array([0.0, 1.0, 0.0, -2.0, 1.0, 0.0, -2.0, -1.0, 0.0, 0.0, -1.0, 0.0]);

	vaoCheckerBoard = gl.createVertexArray();
	gl.bindVertexArray(vaoCheckerBoard);

	vboCheckerPosition = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboCheckerPosition);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(), gl.DYNAMIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.ARD_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.ARD_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	vboCheckerTexture = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboCheckerTexture);
	gl.bufferData(gl.ARRAY_BUFFER, checkerBoardTexture, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.ARD_ATTRIBUTE_TEXTURE0, 2, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.ARD_ATTRIBUTE_TEXTURE0);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindVertexArray(null);

	//gl.shadeModel(gl.SMOOTH);
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);

	loadGLTextures();

	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	if(bFullscreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}

	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	//set viewport 
	gl.viewport(0, 0, canvas.width, canvas.height);

	// PMG Correction
	// Give FOV, NEAR and FAR as specified by sir
	mat4.perspective(perspectiveProjectionMatrix, 60.0 * (Math.PI / 180), parseFloat(canvas.width)/parseFloat(canvas.height), 1.0, 30.0);
}

function draw()
{
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	gl.useProgram(shaderProgramObject);

	var modelViewMatrix = mat4.create();
	var modelViewProjectionMatrix = mat4.create();

	mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -3.6]);

	//mat4.scale(modelViewMatrix, modelViewMatrix, [3.5, 3.5, 3.5]);
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);

	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	gl.bindVertexArray(vaoCheckerBoard);

	var checkerBoardVertices = new Float32Array(12);
	
	//checkerBoardVertices = [ 0.0, 1.0, 0.0, -2.0, 1.0, 0.0, -2.0, -1.0, 0.0, 0.0, -1.0, 0.0];
	checkerBoardVertices[0] = 0.0;
	checkerBoardVertices[1] = 1.0;
	checkerBoardVertices[2] = 0.0;
	checkerBoardVertices[3] = -2.0;
	checkerBoardVertices[4] = 1.0;
	checkerBoardVertices[5] = 0.0;
	checkerBoardVertices[6] = -2.0;
	checkerBoardVertices[7] = -1.0;
	checkerBoardVertices[8] = 0.0;
	checkerBoardVertices[9] = 0.0;
	checkerBoardVertices[10] = -1.0;
	checkerBoardVertices[11] = 0.0;


	gl.bindBuffer(gl.ARRAY_BUFFER, vboCheckerPosition);
	gl.bufferData(gl.ARRAY_BUFFER, checkerBoardVertices, gl.DYNAMIC_DRAW);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
		
//	gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, texName);
	gl.uniform1i(uniform_texture0_sampler, 0);
	gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);

	
	//gl.drawArrays(gl.TRIANGLES, 0, 12);
	/*gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);*/

	//checkerBoardVertices = [ 2.41421, 1.0, -1.41421, 1.0, 1.0, 0.0, 1.0, -1.0, 0.0, 2.41421, -1.0, -1.41421];
	checkerBoardVertices[0] = 2.41421;
	checkerBoardVertices[1] = 1.0;
	checkerBoardVertices[2] = -1.41421;
	checkerBoardVertices[3] = 1.0;
	checkerBoardVertices[4] = 1.0;
	checkerBoardVertices[5] = 0.0;
	checkerBoardVertices[6] = 1.0;
	checkerBoardVertices[7] = -1.0;
	checkerBoardVertices[8] = 0.0;
	checkerBoardVertices[9] = 2.41421;
	checkerBoardVertices[10] = -1.0;
	checkerBoardVertices[11] = -1.41421;

	gl.bindBuffer(gl.ARRAY_BUFFER, vboCheckerPosition);
	gl.bufferData(gl.ARRAY_BUFFER, checkerBoardVertices, gl.DYNAMIC_DRAW);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindTexture(gl.TEXTURE_2D, texName);
	gl.uniform1i(uniform_texture0_sampler, 0);
	gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
/*	gl.drawArrays(gl.TRIANGLE_FAN, 4, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);*/

	gl.bindVertexArray(null);

	gl.useProgram(null);
	requestAnimationFrame(draw, canvas);
}

function uninitialize()
{
	if(vaoCheckerBoard)
	{
		gl.deleteVertexArray(vaoCheckerBoard);
		vaoCheckerBoard = null;
	}

	if(texName)
	{
		gl.deleteTexture(texName);
		texName = null;
	}

	if(vboCheckerPosition)
	{
		gl.deleteBuffer(vboCheckerPosition);
		vboCheckerPosition = null;
	}

	if(vboCheckerTexture)
	{
		gl.deleteBuffer(vboCheckerTexture);
		vboCheckerTexture = null;
	}

	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}

		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}

function keyDown(event)
{
	switch(event.key)
	{
	
		case 'F':
		case 'f':
		toggleFullscreen();
		break;
	}

	switch(event.keyCode)
	{
		case 27:
		uninitialize();
		window.close();
	}
}

function mouseDown()
{

}

function loadGLTextures()
{
	makeCheckImage();
	//arrayGen();

	gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);

	texName	= gl.createTexture();
	gl.bindTexture(gl.TEXTURE_2D, texName);
	//gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
	//gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA,checkImageWidth, checkImageHeight, 0, gl.RGBA, gl.UNSIGNED_BYTE, checkImage);
	//gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, array_buffer);
}

function makeCheckImage()
{
	var i, j;
	var  n = 0;
	for(i = 0; i < checkImageHeight; i++)
	{
		for(j = 0; j < checkImageWidth; j++)
		{			
				c = (((i & 0x8) == 0) ^ ((j & 0x8) == 0)) * 255;	
				checkImage[n++] = c;
				checkImage[n++] = c;
				checkImage[n++] = c;
				checkImage[n++] = 255;
			
		}
	}
}

/*function arrayGen()
{
	var i,j,k;

	for(i = 0; i < 64; i++)
	{
		for(j = 0; j < 64; j++)
		{
			for(k = 0; k< 4; k++)
			{
				array_buffer[n] = checkImage[i][j][k];
			}
			n = n + 1;
		}
	}	
}

function makeCheckImage()
{

	var i;
	var n = 0;

	for(i = 0; i < 64; i++)	
	{
		c = (((i & 0x8) == 0) ^ ((j & 0x8) == 0)) * 255;
		
				checkImage[n++] = c;
				checkImage[n++] = c;
				checkImage[n++] = c;
				checkImage[n++] = 255;
	}

}*/