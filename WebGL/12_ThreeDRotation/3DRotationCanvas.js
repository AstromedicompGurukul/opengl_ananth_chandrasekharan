//global variables
var canvas = null;
var gl = null;
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros = 
{
	ARD_ATTRIBUTE_VERTEX:0,
	ARD_ATTRIBUTE_COLOR:1,
	ARD_ATTRIBUTE_NORMAL:2,
	ARD_ATTRIBUTE_TEXTURE0:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vaoCube;
var vaoPyramid;

var vboPosition;
var vboColor;
var mvpUniform;

var anglePyramid = 0.0;
var angleCube = 0.0;

var perspectiveProjectionMatrix;

//To start animation : To have requestAnimation to be 
//called "cross-browser" compatible

var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

var cancelAnimationFrame = 
window.cancelAnimationFrame ||
window.webkitCancelRequestAnimationFrame ||
window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame ||
window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame ||
window.oCancelAnimationFrame ||
window.msCanceclRequestAnimationFrame ||
window.msCancelAnimationFrame;

function main()
{
	canvas = document.getElementById("OGL");
	if(!canvas)
		console.log("Obtaining Canvas Failed\n");
	else
		console.log("Obtaining Canvas Succeeded\n");
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;

	//register keyboard's keydown event handler
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	//initialize WebGL
	init();

	//start drawing here as warming-up
	resize();
	draw();
}

function toggleFullscreen()
{
	var fullscreen_element = 
	document.fullscreenElement ||
	document.webkitFullscreenElement ||
	document.mozFullScreenElement ||
	document.msFullscreenElement ||
	null;

	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullscreen = true;
	}

	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullscreen = false;
	}
}

function init()
{
	gl = canvas.getContext("webgl2");
	if(gl == null)
	{
		console.log("Failed to get the rendering context for WebGL2");
		return;
	}

	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;

	//vertex shader
	var vertexShaderSourceCode = 
	"#version 300 es"+
	"\n"+
	"in vec4 vPosition;"+
	"in vec4 vColor;"+
	"uniform mat4 u_mvp_matrix;"+
	"out vec4 outColor;"+
	"void main(void)"+
	"{"+
	"gl_Position = u_mvp_matrix * vPosition;"+
	"outColor = vColor;"+
	"}";

	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}	

	//fragment Shader
	var fragmentShaderSourceCode = 
	"#version 300 es"+
	"\n"+
	"precision highp float;"+
	"in vec4 outColor;"+
	"out vec4 FragColor;"+
	"void main(void)"+
	"{"+
	"FragColor = outColor;"+
	"}"

	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject, fragmentShaderSourceCode);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//shader Program
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);

	//pre-link binding of shader program object with vertex shader attributes
	
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.ARD_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.ARD_ATTRIBUTE_COLOR, "vColor");
	gl.linkProgram(shaderProgramObject);
	if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//get MVP Uniform Location

	mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");

	//vertices, colors, shader attribs, vao, vbo initialization
	var pyramidVertices = new Float32Array([0.0, 1.0, 0.0,//apex
	-1.0, -1.0, 1.0,//left corner 
	1.0, -1.0, 1.0,//right corne

								  //RIGHT FACE
	0.0, 1.0, 0.0,//apex
	1.0, -1.0, 1.0,//left corner of right face
	1.0, -1.0, -1.0,//right corner of right face

								   //BACK FACE
	0.0, 1.0, 0.0,//apex
	1.0, -1.0, -1.0,//left corner of back face
	-1.0, -1.0, -1.0,//right coner of back face

									//LEFT FACE
	0.0, 1.0, 0.0,//apex
	-1.0, -1.0, -1.0,//left corner of left face 
	-1.0, -1.0, 1.0]);


	var pyramidColor = new Float32Array([1.0, 0.0, 0.0, //red
	0.0, 1.0, 0.0,//green
	0.0, 0.0, 1.0,//blue
								  
	1.0, 0.0, 0.0, //red
	0.0, 0.0, 1.0,//blue
	0.0, 1.0, 0.0,//green

	1.0, 0.0, 0.0,//red
	0.0, 1.0, 0.0,//green
	0.0, 0.0, 1.0,//blue

	1.0, 0.0, 0.0,//red
	0.0, 0.0, 1.0,//blue
	0.0, 1.0, 0.0,//green
	]);
	
	vaoTriangle = gl.createVertexArray();
	gl.bindVertexArray(vaoPyramid);

	vboPosition = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboPosition);
	gl.bufferData(gl.ARRAY_BUFFER, pyramidVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.ARD_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.ARD_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	vboColor = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboColor);
	gl.bufferData(gl.ARRAY_BUFFER, pyramidColor, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.ARD_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.ARD_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindVertexArray(null);

	var cubeVertices = new Float32Array([1.0, 1.0, -1.0,
	-1.0, 1.0, -1.0,
	-1.0, 1.0, 1.0,
	1.0, 1.0, 1.0,

	//BOTTOM FACE
	1.0, -1.0, -1.0,//right top of bottom face
	-1.0, -1.0, -1.0,
	-1.0, -1.0, 1.0,
	1.0, -1.0, 1.0,

	//FRONT FACE
	1.0, 1.0, 1.0,//right top
	-1.0, 1.0, 1.0,//left top
	-1.0, -1.0, 1.0,//left bottom
	1.0, -1.0, 1.0,

	//BACK FACE
	 1.0, 1.0, -1.0,//right top of back face
	-1.0, 1.0, -1.0,//left top of back face
	-1.0, -1.0, -1.0,//left bottom of back face
	1.0, -1.0, -1.0,//right bottom of back face

	 //RIGHT FACE
	1.0,1.0, -1.0,//right top of right face
	1.0, 1.0, 1.0,//left top of right face
	1.0, -1.0, 1.0,//left bottom of right face
	1.0, -1.0, -1.0,//right bottom of right facce


	 //LEFT FACE
	-1.0, 1.0, 1.0,//right top of left face
	-1.0, 1.0, -1.0,//left top of left face
	-1.0, -1.0, -1.0,//left bottom of left face
	-1.0, -1.0, 1.0//right bottom of left face
]);

	var cubeColor = new Float32Array([
			0.0, 1.0, 0.0,//green
	0.0, 1.0, 0.0,//green
	0.0, 1.0, 0.0,//green
	0.0, 1.0, 0.0,//green

	0.0, 1.0, 1.0,//cyan
	0.0, 1.0, 1.0,//cyan
	0.0, 1.0, 1.0,//cyan
	0.0, 1.0, 1.0,//cyan

	1.0, 0.0, 0.0, //red
	1.0, 0.0, 0.0, //red
	1.0, 0.0, 0.0, //red
	1.0, 0.0, 0.0, //red

	1.0, 1.0, 1.0,//yellow
	1.0, 1.0, 1.0,//yellow
	1.0, 1.0, 1.0,//yellow
	1.0, 1.0, 1.0,//yellow

	0.0,0.0,1.0,//blue
	0.0,0.0,1.0,//blue
	0.0,0.0,1.0,//blue
	0.0,0.0,1.0,//blue

	1.0,0.0,1.0,//magenta
	1.0,0.0,1.0,//magenta
	1.0,0.0,1.0,//magenta
	1.0,0.0,1.0]);

	vaoCube = gl.createVertexArray();
	gl.bindVertexArray(vaoCube);

	vboPosition = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboPosition);
	gl.bufferData(gl.ARRAY_BUFFER, cubeVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.ARD_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.ARD_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	vboColor = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboColor);
	gl.bufferData(gl.ARRAY_BUFFER, cubeColor, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.ARD_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.ARD_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);	

	gl.bindVertexArray(null);

	//gl.shadeModel(gl.SMOOTH);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);

	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	if(bFullscreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}

	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	//set viewport 
	gl.viewport(0, 0, canvas.width, canvas.height);

	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
}

function draw()
{
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	gl.useProgram(shaderProgramObject);

	var modelViewMatrix = mat4.create();
	var modelViewProjectionMatrix = mat4.create();
	//var rotationMatrix = mat4.create();
	//var translationMatrix = mat4.create();
	//mat4.translate(translationMatrix, translationMatrix, [0.0, 0.0, -3.0]);
	//mat4.multiply(modelViewMatrix, modelViewMatrix, translationMatrix);

	mat4.translate(modelViewMatrix, modelViewMatrix, [-1.5, 0.0, -6.0]);
	mat4.rotateY(modelViewMatrix, modelViewMatrix, degToRad(anglePyramid));
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	gl.bindVertexArray(vaoPyramid);
	gl.drawArrays(gl.TRIANGLES, 0, 12);
	gl.bindVertexArray(null);

	//SQUARE

	mat4.identity(modelViewMatrix);
	//mat4.identity(rotationMatrix);
	mat4.identity(modelViewProjectionMatrix);
	mat4.translate(modelViewMatrix, modelViewMatrix, [1.5, 0.0, -6.0]);
	mat4.rotateX(modelViewMatrix, modelViewMatrix, degToRad(angleCube));
	mat4.rotateY(modelViewMatrix, modelViewMatrix, degToRad(angleCube));
	mat4.rotateZ(modelViewMatrix, modelViewMatrix, degToRad(angleCube));
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	gl.bindVertexArray(vaoCube);
	gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 4, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);
	gl.bindVertexArray(null);


	gl.useProgram(null);
	update();
	requestAnimationFrame(draw, canvas);
}

function uninitialize()
{
	if(vaoCube)
	{
		gl.deleteVertexArray(vaoCube);
		vaoCube = null;
	}

	if(vaoPyramid)
	{
		gl.deleteVertexArray(vaoPyramid);
		vaoPyramid = null;
	}

	if(vboPosition)
	{
		gl.deleteBuffer(vboPosition);
		vboPosition = null;
	}

	if(vboColor)
	{
		gl.deleteBuffer(vboColor);
		vboColor = null;
	}

	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}

		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}

function keyDown(event)
{
	switch(event.key)
	{
	
		case 'F':
		case 'f':
		toggleFullscreen();
		break;
	}

	switch(event.keyCode)
	{
		case 27:
		uninitialize();
		window.close();
	}
}

function mouseDown()
{

}

function degToRad(degrees)
{
	return(degrees * Math.PI/180.0);
}

function update()
{
	anglePyramid = anglePyramid + 0.5;
	if(anglePyramid >= 360.0)
	{
		anglePyramid = 0.0;
	}

	angleCube = angleCube + 0.5;
	if(angleCube >= 360.0)
	{
		angleCube = 0.0;
	}
}