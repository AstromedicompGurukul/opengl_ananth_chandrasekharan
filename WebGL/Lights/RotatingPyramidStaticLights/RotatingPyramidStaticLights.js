var canvas = null;
var gl = null;
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros = 
{
	ARD_ATTRIBUTE_VERTEX:0,
	ARD_ATTRIBUTE_COLOR:1,
	ARD_ATTRIBUTE_NORMAL:2,
	ARD_ATTRIBUTE_TEXTURE0:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vaoPyramid;
var vboPosition;
var vboNormal;

var anglePyramid = 0.0;

//First Light
var light1_ambient = [0.0, 0.0, 0.0];
var light1_diffuse = [1.0, 0.0, 0.0];
var light1_specular = [1.0, 0.0, 0.0];
var light1_position = [100.0, 100.0, 100.0, 1.0];

//Second Light
var light2_ambient = [0.0, 0.0, 0.0];
var light2_diffuse = [0.0, 0.0, 1.0];
var light2_specular = [0.0, 0.0, 1.0];
var light2_position = [-100.0, 100.0, 100.0, 1.0];

var material_ambient = [0.0, 0.0, 0.0];
var material_diffuse = [1.0, 1.0, 1.0];
var material_specular = [1.0, 1.0, 1.0];
var material_shininess = 50.0;

var perspectiveProjectionMatrix;

var modelMatrixUniform,viewMatrixUniform, projectionMatrixUniform;
var la1Uniform, ld1Uniform, ls1Uniform;
var ka1Uniform, kd1Uniform, ks1Uniform, material1ShininessUniform, light1PositionUniform;

var la2Uniform, ld2Uniform, ls2Uniform;
var ka2Uniform, kd2Uniform, ks2Uniform, material2ShininessUniform, light2PositionUniform;


var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

var cancelAnimationFrame =
window.cancelAnimationFrame ||
window.webkitCancelRequestAnimationFrame || 
window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame || 
window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || 
window.oCancelAnimationFrame ||
window.msCancelRequestAnimationFrame || 
window.msCancelAnimationFrame;

function main()
{  
    canvas = document.getElementById("AMC");
    if(!canvas)
        console.log("Obtaining Canvas Failed\n");
    else
        console.log("Obtaining Canvas Succeeded\n");
    canvas_original_width=canvas.width;
    canvas_original_height=canvas.height;
   
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);

    // initialize WebGL
    init();
    
    // start drawing here as warming-up
    resize();
    draw();
}

function toggleFullScreen()
{
    var fullscreen_element =
    document.fullscreenElement ||
    document.webkitFullscreenElement ||
    document.mozFullScreenElement ||
    document.msFullscreenElement ||
    null;

    if(fullscreen_element==null)
    {
        if(canvas.requestFullscreen)
            canvas.requestFullscreen();
        else if(canvas.mozRequestFullScreen)
            canvas.mozRequestFullScreen();
        else if(canvas.webkitRequestFullscreen)
            canvas.webkitRequestFullscreen();
        else if(canvas.msRequestFullscreen)
            canvas.msRequestFullscreen();
        bFullscreen=true;
    }
    else 
    {
        if(document.exitFullscreen)
            document.exitFullscreen();
        else if(document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if(document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if(document.msExitFullscreen)
            document.msExitFullscreen();
        bFullscreen=false;
    }
}

function init()
{
	gl = canvas.getContext("webgl2");
	if(gl == null)
	{
		console.log("Failed to get the rendering context for WebGL2");
		return;
	}

	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;

	 var vertexShaderSourceCode=
    "#version 300 es"+
    "\n"+
    "in vec4 vPosition;"+
    "in vec3 vNormal;"+
    "uniform mat4 u_model_matrix;"+
    "uniform mat4 u_view_matrix;"+
    "uniform mat4 u_projection_matrix;"+
    "uniform vec3 u_La1;"+
    "uniform vec3 u_Ld1;"+
    "uniform vec3 u_Ls1;"+
    "uniform vec4 u_light1_position;"+
    "uniform vec3 u_Ka1;"+
    "uniform vec3 u_Kd1;"+
    "uniform vec3 u_Ks1;"+
    "uniform float u_material1_shininess;"+
    "out vec3 phong_ads_color1;"+
    "uniform vec3 u_La2;"+
    "uniform vec3 u_Ld2;"+
    "uniform vec3 u_Ls2;"+
    "uniform vec4 u_light2_position;"+
    "uniform vec3 u_Ka2;"+
    "uniform vec3 u_Kd2;"+
    "uniform vec3 u_Ks2;"+
    "uniform float u_material2_shininess;"+
    "out vec3 phong_ads_color2;"+
    "void main(void)"+
    "{"+
    "vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;"+
    "vec3 transformed_normals=normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);"+

    "vec3 light1_direction = normalize(vec3(u_light1_position) - eye_coordinates.xyz);"+
    "vec3 light2_direction = normalize(vec3(u_light2_position) - eye_coordinates.xyz);"+

    "float tn_dot_ld1 = max(dot(transformed_normals, light1_direction),0.0);"+
    "float tn_dot_ld2 = max(dot(transformed_normals, light2_direction),0.0);"+
   
    "vec3 ambient1 = u_La1 * u_Ka1;"+
    "vec3 diffuse1 = u_Ld1 * u_Kd1 * tn_dot_ld1;"+
    "vec3 reflection_vector1 = reflect(-light1_direction, transformed_normals);"+

    "vec3 ambient2 = u_La2 * u_Ka2;"+
    "vec3 diffuse2 = u_Ld2 * u_Kd2 * tn_dot_ld2;"+
    "vec3 reflection_vector2 = reflect(-light2_direction, transformed_normals);"+

    "vec3 viewer_vector = normalize(-eye_coordinates.xyz);"+
    
    "vec3 specular1 = u_Ls1 * u_Ks1 * pow(max(dot(reflection_vector1, viewer_vector), 0.0), u_material1_shininess);"+
    "phong_ads_color1 = ambient1 + diffuse1 + specular1;"+

    "vec3 specular2 = u_Ls2 * u_Ks2 * pow(max(dot(reflection_vector2, viewer_vector), 0.0), u_material2_shininess);"+
    "phong_ads_color2 = ambient2 + diffuse2 + specular2;"+
    
    "gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"+
    "}"

	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}	

	var fragmentShaderSourceCode = 
	"#version 300 es"+
	"\n"+
	"precision highp float;"+
	"in vec3 phong_ads_color1;"+
	"in vec3 phong_ads_color2;"+
	"out vec4 FragColor;"+
	"void main(void)"+
	"{"+
	"FragColor = vec4(phong_ads_color1, 1.0) + vec4(phong_ads_color2, 1.0);"+
	"}"

	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//shader Program
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);

	//pre-link binding of shader program object with vertex shader attributes
	
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.ARD_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.ARD_ATTRIBUTE_NORMAL, "vNormal");
	gl.linkProgram(shaderProgramObject);
	if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	modelMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_model_matrix");
	viewMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_view_matrix");
	projectionMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_projection_matrix");


	la1Uniform = gl.getUniformLocation(shaderProgramObject, "u_La1");
	ld1Uniform = gl.getUniformLocation(shaderProgramObject, "u_Ld1");
	ls1Uniform = gl.getUniformLocation(shaderProgramObject, "u_Ls1");
	light1PositionUniform = gl.getUniformLocation(shaderProgramObject, "u_light1_position");
	ka1Uniform = gl.getUniformLocation(shaderProgramObject, "u_Ka1");
	kd1Uniform = gl.getUniformLocation(shaderProgramObject, "u_Kd1");
	ks1Uniform = gl.getUniformLocation(shaderProgramObject, "u_Ks1");
	material1ShininessUniform = gl.getUniformLocation(shaderProgramObject, "u_material1_shininess");

	la2Uniform = gl.getUniformLocation(shaderProgramObject, "u_La2");
	ld2Uniform = gl.getUniformLocation(shaderProgramObject, "u_Ld2");
	ls2Uniform = gl.getUniformLocation(shaderProgramObject, "u_Ls2");
	light2PositionUniform = gl.getUniformLocation(shaderProgramObject, "u_light2_position");
	ka2Uniform = gl.getUniformLocation(shaderProgramObject, "u_Ka2");
	kd2Uniform = gl.getUniformLocation(shaderProgramObject, "u_Kd2");
	ks2Uniform = gl.getUniformLocation(shaderProgramObject, "u_Ks2");
	material2ShininessUniform = gl.getUniformLocation(shaderProgramObject, "u_material2_shininess");

	//vertices, colors, shader attribs, vao, vbo initialization
	var pyramidVertices = new Float32Array([0.0, 1.0, 0.0,//apex
	-1.0, -1.0, 1.0,//left corner 
	1.0, -1.0, 1.0,//right corne

								  //RIGHT FACE
	0.0, 1.0, 0.0,//apex
	1.0, -1.0, 1.0,//left corner of right face
	1.0, -1.0, -1.0,//right corner of right face

								   //BACK FACE
	0.0, 1.0, 0.0,//apex
	1.0, -1.0, -1.0,//left corner of back face
	-1.0, -1.0, -1.0,//right coner of back face

									//LEFT FACE
	0.0, 1.0, 0.0,//apex
	-1.0, -1.0, -1.0,//left corner of left face 
	-1.0, -1.0, 1.0]);


	var pyramidNormal = new Float32Array([0.0, 0.447214, 0.894427,
		0.0, 0.447214, 0.894427,
		0.0, 0.447214, 0.894427,

		0.894427, 0.447214, 0.0,
		0.894427, 0.447214, 0.0,
		0.894427, 0.447214, 0.0,

		0.0, 0.447214, -0.894427,
		0.0, 0.447214, -0.894427,
		0.0, 0.447214, -0.894427,

		-0.894427, 0.447214, 0.0,
		-0.894427, 0.447214, 0.0,
		-0.894427, 0.447214, 0.0
	]);
	
	vaoPyramid = gl.createVertexArray();
	gl.bindVertexArray(vaoPyramid);

	vboPosition = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboPosition);
	gl.bufferData(gl.ARRAY_BUFFER, pyramidVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.ARD_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.ARD_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	vboNormal = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboNormal);
	gl.bufferData(gl.ARRAY_BUFFER, pyramidNormal, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.ARD_ATTRIBUTE_NORMAL, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.ARD_ATTRIBUTE_NORMAL);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindVertexArray(null);

	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	gl.enable(gl.CULL_FACE);
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	if(bFullscreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}

	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	//set viewport 
	gl.viewport(0, 0, canvas.width, canvas.height);

	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
}

function draw()
{
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	gl.useProgram(shaderProgramObject);

	var modelMatrix = mat4.create();
	var viewMatrix = mat4.create();
	//var modelViewProjectionMatrix = mat4.create();

	mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, -5.0]);
	mat4.rotateY(modelMatrix, modelMatrix, degToRad(anglePyramid));
	
	gl.uniform3fv(la1Uniform, light1_ambient);
	gl.uniform3fv(ld1Uniform, light1_diffuse);
	gl.uniform3fv(ls1Uniform, light1_specular);
	gl.uniform3fv(ka1Uniform, material_ambient);
	gl.uniform3fv(kd1Uniform, material_diffuse);
	gl.uniform3fv(ks1Uniform, material_specular);
	gl.uniform1f(material1ShininessUniform, material_shininess);
	gl.uniform4fv(light1PositionUniform, light1_position);

	gl.uniform3fv(la2Uniform, light2_ambient);
	gl.uniform3fv(ld2Uniform, light2_diffuse);
	gl.uniform3fv(ls2Uniform, light2_specular);
	gl.uniform3fv(ka2Uniform, material_ambient);
	gl.uniform3fv(kd2Uniform, material_diffuse);
	gl.uniform3fv(ks2Uniform, material_specular);
	gl.uniform1f(material2ShininessUniform, material_shininess);
	gl.uniform4fv(light2PositionUniform, light2_position);
	
	gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);
	
	gl.bindVertexArray(vaoPyramid);
	gl.drawArrays(gl.TRIANGLES, 0, 12);
	gl.bindVertexArray(null);

	gl.useProgram(null);
	update();
	requestAnimationFrame(draw, canvas);
}

function uninitialize()
{
	if(vaoPyramid)
	{
		gl.deleteVertexArray(vaoPyramid);
		vaoPyramid = null;
	}

	if(vboPosition)
	{
		gl.deleteBuffer(vboPosition);
		vboPosition = null;
	}

	if(vboNormal)
	{
		gl.deleteBuffer(vboNormal);
		vboNormal = null;
	}

	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}

		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}

function keyDown(event)
{
	switch(event.key)
	{
		case 'F':
		case 'f':
		toggleFullScreen();
		break;
	}

	switch(event.keyCode)
	{
		case 27:
		uninitialize();
		window.close();
		break;

		case 70:
			toggleFullScreen();
			break;
	}
}

function mouseDown()
{

}

function degToRad(degrees)
{
	return(degrees * Math.PI/180.0);
}

function update()
{
	anglePyramid = anglePyramid + 0.5;
	if(anglePyramid >= 360.0)
	{
		anglePyramid = 0.0;
	}
}