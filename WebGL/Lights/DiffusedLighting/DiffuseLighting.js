var canvas = null;
var gl = null;
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros = 
{
	ARD_ATTRIBUTE_VERTEX:0,
	ARD_ATTRIBUTE_COLOR:1,
	ARD_ATTRIBUTE_NORMAL:2,
	ARD_ATTRIBUTE_TEXTURE0:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vaoCube;
var vboPosition;
var vboNormal;

var perspectiveProjectionMatrix;

var modelViewMatrixUniform, projectionMatrixUniform;
var ldUniform, kdUniform, lightPositionUniform;
var LKeyPressedUniform;

var angleCube = 0.0;

var bLKeyPressed = false;

var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

var cancelAnimationFrame =
window.cancelAnimationFrame ||
window.webkitCancelRequestAnimationFrame || 
window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame || 
window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || 
window.oCancelAnimationFrame ||
window.msCancelRequestAnimationFrame || 
window.msCancelAnimationFrame;

function main()
{  
    canvas = document.getElementById("OGL");
    if(!canvas)
        console.log("Obtaining Canvas Failed\n");
    else
        console.log("Obtaining Canvas Succeeded\n");
    canvas_original_width=canvas.width;
    canvas_original_height=canvas.height;
   
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);

    // initialize WebGL
    init();
    
    // start drawing here as warming-up
    resize();
    draw();
}

function toggleFullScreen()
{
    var fullscreen_element =
    document.fullscreenElement ||
    document.webkitFullscreenElement ||
    document.mozFullScreenElement ||
    document.msFullscreenElement ||
    null;

    if(fullscreen_element==null)
    {
        if(canvas.requestFullscreen)
            canvas.requestFullscreen();
        else if(canvas.mozRequestFullScreen)
            canvas.mozRequestFullScreen();
        else if(canvas.webkitRequestFullscreen)
            canvas.webkitRequestFullscreen();
        else if(canvas.msRequestFullscreen)
            canvas.msRequestFullscreen();
        bFullscreen=true;
    }
    else 
    {
        if(document.exitFullscreen)
            document.exitFullscreen();
        else if(document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if(document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if(document.msExitFullscreen)
            document.msExitFullscreen();
        bFullscreen=false;
    }
}

function init()
{
	gl = canvas.getContext("webgl2");
	if(gl == null)
	{
		console.log("Failed to get the rendering context for WebGL2");
		return;
	}

	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;

	//vertex shader
	var vertexShaderSourceCode = 
	"#version 300 es"+
	"\n"+
	"in vec4 vPosition;"+
	"in vec3 vNormal;"+
	"uniform mat4 u_model_view_matrix;"+
	"uniform mat4 u_projection_matrix;"+
	"uniform mediump int u_LKeyPressed;"+
	"uniform vec3 u_Ld;"+
	"uniform vec3 u_Kd;"+
	"uniform vec4 u_light_poistion;"+
	"out vec3 diffuse_light;"+
	"void main(void)"+
	"{"+
	"if(u_LKeyPressed == 1)"+
	"{"+
	"vec4 eyeCoordinates = u_model_view_matrix * vPosition;"+
	"vec3 tnorm = normalize(mat3(u_model_view_matrix) * vNormal);"+
	"vec3 s = normalize(vec3(u_light_poistion - eyeCoordinates));"+
	"diffuse_light = u_Ld * u_Kd * max(dot(s, tnorm), 0.0);"+
	"}"+
	"gl_Position = u_projection_matrix * u_model_view_matrix * vPosition;"+
	"}";

	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}	

	//fragment Shader
	var fragmentShaderSourceCode = 
	"#version 300 es"+
	"\n"+
	"precision highp float;"+
	"in vec3 diffuse_light;"+
	"out vec4 FragColor;"+
	"uniform int u_LKeyPressed;"+
	"void main(void)"+
	"{"+
	"vec4 color;"+
	"if(u_LKeyPressed == 1)"+
	"{"+
	"color = vec4(diffuse_light, 1.0);"+
	"}"+
	"else"+
	"{"+
	"color = vec4(1.0, 1.0, 1.0, 1.0);"+
	"}"+
	"FragColor = color;"+
	"}";

	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject, fragmentShaderSourceCode);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//shader Program
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);

	//pre-link binding of shader program object with vertex shader attributes
	
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.ARD_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.ARD_ATTRIBUTE_NORMAL, "vNormal");
	gl.linkProgram(shaderProgramObject);
	if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//get MVP Uniform Location

	modelViewMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_model_view_matrix");
	projectionMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_projection_matrix");
	LKeyPressedUniform = gl.getUniformLocation(shaderProgramObject, "u_LKeyPressed");
	
	ldUniform = gl.getUniformLocation(shaderProgramObject, "u_Ld");
	kdUniform = gl.getUniformLocation(shaderProgramObject, "u_Kd");
	lightPositionUniform = gl.getUniformLocation(shaderProgramObject, "u_light_poistion");

	var cubeVertices = new Float32Array([1.0, 1.0, -1.0,
	-1.0, 1.0, -1.0,
	-1.0, 1.0, 1.0,
	1.0, 1.0, 1.0,

	//BOTTOM FACE
	1.0, -1.0, -1.0,//right top of bottom face
	-1.0, -1.0, -1.0,
	-1.0, -1.0, 1.0,
	1.0, -1.0, 1.0,

	//FRONT FACE
	1.0, 1.0, 1.0,//right top
	-1.0, 1.0, 1.0,//left top
	-1.0, -1.0, 1.0,//left bottom
	1.0, -1.0, 1.0,

	//BACK FACE
	 1.0, 1.0, -1.0,//right top of back face
	-1.0, 1.0, -1.0,//left top of back face
	-1.0, -1.0, -1.0,//left bottom of back face
	1.0, -1.0, -1.0,//right bottom of back face

	 //RIGHT FACE
	1.0,1.0, -1.0,//right top of right face
	1.0, 1.0, 1.0,//left top of right face
	1.0, -1.0, 1.0,//left bottom of right face
	1.0, -1.0, -1.0,//right bottom of right facce


	 //LEFT FACE
	-1.0, 1.0, 1.0,//right top of left face
	-1.0, 1.0, -1.0,//left top of left face
	-1.0, -1.0, -1.0,//left bottom of left face
	-1.0, -1.0, 1.0//right bottom of left face
]);

	var cubeNormals = new Float32Array([
			0.0, 1.0, 0.0,
            0.0, 1.0, 0.0,
            0.0, 1.0, 0.0,
            0.0, 1.0, 0.0,
                                      
                                      // bottom
            0.0, -1.0, 0.0,
            0.0, -1.0, 0.0,
            0.0, -1.0, 0.0,
            0.0, -1.0, 0.0,
                                      
                                      // front
            0.0, 0.0, 1.0,
            0.0, 0.0, 1.0,
            0.0, 0.0, 1.0,
            0.0, 0.0, 1.0,
                                      
                                      // back
           0.0, 0.0, -1.0,
           0.0, 0.0, -1.0,
           0.0, 0.0, -1.0,
           0.0, 0.0, -1.0,
                                    
                                      // left
          -1.0, 0.0, 0.0,
          -1.0, 0.0, 0.0,
          -1.0, 0.0, 0.0,
          -1.0, 0.0, 0.0,
                                      
                                      // right
           1.0, 0.0, 0.0,
           1.0, 0.0, 0.0,
           1.0, 0.0, 0.0,
           1.0, 0.0, 0.0]);

	vaoCube= gl.createVertexArray();
	gl.bindVertexArray(vaoCube);

	vboPosition = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboPosition);
	gl.bufferData(gl.ARRAY_BUFFER, cubeVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.ARD_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.ARD_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	vboNormal = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboNormal);
	gl.bufferData(gl.ARRAY_BUFFER, cubeNormals, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.ARD_ATTRIBUTE_NORMAL, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.ARD_ATTRIBUTE_NORMAL);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);	

	gl.bindVertexArray(null);

	//gl.shadeModel(gl.SMOOTH);
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);

	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	if(bFullscreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}

	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	//set viewport 
	gl.viewport(0, 0, canvas.width, canvas.height);

	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
}

function draw()
{
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	gl.useProgram(shaderProgramObject);

	if(bLKeyPressed == true)
	{
		gl.uniform1i(LKeyPressedUniform, 1);
		gl.uniform3f(ldUniform, 1.0, 1.0, 1.0);
		gl.uniform3f(kdUniform, 0.5, 0.5, 0.5);
		var lightPosition = [0.0, 0.0, 2.0, 1.0];
		gl.uniform4fv(lightPositionUniform, lightPosition);
	}
	else
	{
		gl.uniform1i(LKeyPressedUniform, 0);
	}	

	var modelViewMatrix = mat4.create();
	mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -5.0]);
	mat4.rotateX(modelViewMatrix, modelViewMatrix, degToRad(angleCube));
	mat4.rotateY(modelViewMatrix, modelViewMatrix, degToRad(angleCube));
	mat4.rotateZ(modelViewMatrix, modelViewMatrix, degToRad(angleCube));
	gl.uniformMatrix4fv(modelViewMatrixUniform, false, modelViewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);

	gl.bindVertexArray(vaoCube);
	gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 4, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);
	gl.bindVertexArray(null);

	gl.useProgram(null);
	update();
	requestAnimationFrame(draw, canvas);

}

function uninitialize()
{
	if(vaoCube)
	{
		gl.deleteVertexArray(vaoCube);
		vaoCube = null;
	}

	if(vboNormal)
	{
		gl.deleteBuffer(vboNormal);
		vboNormal = null;
	}

	if(vboPosition)
	{
		gl.deleteBuffer(vboPosition);
		vboPosition = null;
	}

	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}

		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}

function keyDown(event)
{
	switch(event.key)
	{
		case 'F':
		case 'f':
		toggleFullScreen();
		break;
	}

	switch(event.keyCode)
	{
		case 27:
		uninitialize();
		window.close();
		break;

		case 76:
			if(bLKeyPressed == false)
				bLKeyPressed = true;
			else 
				bLKeyPressed = false;
			break;

		case 70:
			toggleFullScreen();
			break;
	}
}

function mouseDown()
{

}

function degToRad(degrees)
{
	return(degrees * Math.PI/180.0);
}

function update()
{
	angleCube = angleCube + 0.5;
	if(angleCube >= 360.0)
	{
		angleCube = 0.0;
	}
}
