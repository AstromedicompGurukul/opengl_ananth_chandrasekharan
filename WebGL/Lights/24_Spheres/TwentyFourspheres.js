var canvas = null;
var gl = null;
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros = 
{
	VDG_ATTRIBUTE_VERTEX:0,
	VDG_ATTRIBUTE_COLOR:1,
	VDG_ATTRIBUTE_NORMAL:2,
	VDG_ATTRIBUTE_TEXTURE0:3,
};



var vertexShaderObject;
var fragmentShaderObject;

var shaderProgramObject;

var light_ambient = [0.0, 0.0, 0.0];
var light_diffuse = [1.0, 1.0, 1.0];
var light_specular = [1.0, 1.0, 1.0];
var light_position = [100.0, 100.0, 100.0, 1.0];

//Emrald
var material1_ambient = [ 0.0215, 0.1745, 0.0215];
var material1_diffuse = [0.07568, 0.061424, 0.07568];
var material1_specular = [0.633, 0.727811, 0.633];
var material1_shininess =  76.8;

//Jade
var material2_ambient = [ 0.135, 0.2225, 0.1575];
var material2_diffuse = [ 0.54, 0.89, 0.63];
var material2_specular = [ 0.316288, 0.316288, 0.316288];
var material2_shininess =  12.8;

//Obsidian
var material3_ambient = [ 0.05375, 0.05, 0.06625];
var material3_diffuse = [ 0.18275, 0.17, 0.22525];
var material3_specular = [ 0.332741, 0.328634, 0.346435];
var material3_shininess = 38.4;

//Pearl
var material4_ambient = [ 0.25, 0.20725, 0.20725];
var material4_diffuse = [ 1.0, 0.829, 0.829];
var material4_specular = [ 0.296648, 0.296648, 0.296648];
var material4_shininess = 11.264;

//Ruby
var material5_ambient = [ 0.1745, 0.01175, 0.01175];
var material5_diffuse = [ 0.61424, 0.04136, 0.04136];
var material5_specular = [ 0.727811, 0.626959, 0.626959];
var material5_shininess = 76.8 ;

//Turquoise
var material6_ambient = [ 0.1, 0.18725, 0.1745];
var material6_diffuse = [ 0.396, 0.74151, 0.69102];
var material6_specular = [ 0.297254, 0.30829, 0.306678];
var material6_shininess = 12.8;

//Brass
var material7_ambient = [ 0.329412, 0.223529, 0.027451];
var material7_diffuse = [ 0.780392, 0.568627, 0.113725];
var material7_specular = [ 0.992157, 0.941176, 0.807843];
var material7_shininess = 27.897443616;

//Bronze
var material8_ambient = [ 0.2125, 0.1275, 0.054];
var material8_diffuse = [ 0.714, 0.4284, 0.18144];
var material8_specular = [ 0.393548, 0.271906, 0.166721];
var material8_shininess =  25.6;

//Chrome
var material9_ambient = [ 0.25, 0.25, 0.25];
var material9_diffuse = [ 0.4, 0.4, 0.4];
var material9_specular = [ 0.774597, 0.774597, 0.774597];
var material9_shininess = 76.8;

//Copper
var material10_ambient = [ 0.19125, 0.0735, 0.0225];
var material10_diffuse = [ 0.7038, 0.27048, 0.0828];
var material10_specular = [ 0.256777, 0.137622, 0.086014];
var material10_shininess = 12.8;

//Gold
var material11_ambient = [ 0.24725, 0.1995, 0.0745];
var material11_diffuse = [ 0.75164, 0.60648, 0.22648];
var material11_specular = [ 0.628281, 0.555802, 0.366065];
var material11_shininess = 51.2;

//Silver
var material12_ambient = [ 0.19225, 0.19225, 0.19225];
var material12_diffuse = [ 0.50754, 0.50754, 0.50754];
var material12_specular = [ 0.508273, 0.50827, 0.508273];
var material12_shininess = 51.2;

//Black
var material13_ambient = [ 0.0, 0.0, 0.0];
var material13_diffuse = [ 0.01, 0.01, 0.01];
var material13_specular = [ 0.50, 0.50, 0.50];
var material13_shininess = 32.0;

//Cyan
var material14_ambient = [ 0.0, 0.1, 0.06];
var material14_diffuse = [ 0.0, 0.50980392, 0.50980392];
var material14_specular = [ 0.50196078, 0.50196078, 0.50196078];
var material14_shininess = 32.0;

//Green
var material15_ambient = [ 0.0, 0.0, 0.0];
var material15_diffuse = [ 0.1, 0.35, 0.1];
var material15_specular = [ 0.45, 0.55, 0.45];
var material15_shininess = 32.0;

//Red
var material16_ambient = [ 0.0, 0.0, 0.0];
var material16_diffuse = [ 0.5, 0.0, 0.0];
var material16_specular = [ 0.7, 0.6, 0.6];
var material16_shininess = 32.0;

//White
var material17_ambient = [ 0.0, 0.0, 0.0];
var material17_diffuse = [ 0.55, 0.55, 0.55];
var material17_specular = [ 0.70, 0.70, 0.70];
var material17_shininess = 32.0;

//Yellow Plastic
var material18_ambient = [ 0.0, 0.0, 0.0];
var material18_diffuse = [ 0.5, 0.5, 0.0];
var material18_specular = [ 0.60, 0.60, 0.50];
var material18_shininess = 32.0;

//Black
var material19_ambient = [ 0.02, 0.02, 0.02];
var material19_diffuse = [ 0.01, 0.01, 0.01];
var material19_specular = [ 0.4, 0.4, 0.4];
var material19_shininess = 10.0;

//Cyan
var material20_ambient = [ 0.0, 0.05, 0.05];
var material20_diffuse = [ 0.4, 0.5, 0.5];
var material20_specular = [ 0.04, 0.7, 0.7];
var material20_shininess = 10.0;

//Green
var material21_ambient = [ 0.0, 0.05, 0.0];
var material21_diffuse = [ 0.4, 0.5, 0.4];
var material21_specular = [ 0.04, 0.7, 0.04];
var material21_shininess = 10.0;

//Red
var material22_ambient = [ 0.05, 0.0, 0.0];
var material22_diffuse = [ 0.5, 0.4, 0.4];
var material22_specular = [ 0.7, 0.04, 0.04];
var material22_shininess = 10.0;

//White
var material23_ambient = [ 0.05, 0.05, 0.05];
var material23_diffuse = [ 0.5, 0.5, 0.5];
var material23_specular = [ 0.7, 0.7, 0.7];
var material23_shininess = 10.0;

//Yellow Rubber
var material24_ambient = [ 0.05, 0.05, 0.0];
var material24_diffuse = [ 0.5, 0.5, 0.4];
var material24_specular = [ 0.7, 0.7, 0.04];
var material24_shininess = 10.0;


var sphere = null;

var perspectiveProjectionMatrix;

var modelMatrixUniform,viewMatrixUniform, projectionMatrixUniform;

var laUniform, ldUniform, lsUniform, lightPositionUniform;

var kaUniform, kdUniform, ksUniform, materialShininessUniform;

var light = false;
var bLKeyPressed = false;;

var xAxis;
var yAxis;
var zAxis;

var angle_xAxis = 0.0;
var angle_yAxis = 0.0;
var angle_zAxis = 0.0;


var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

var cancelAnimationFrame =
window.cancelAnimationFrame ||
window.webkitCancelRequestAnimationFrame || 
window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame || 
window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || 
window.oCancelAnimationFrame ||
window.msCancelRequestAnimationFrame || 
window.msCancelAnimationFrame;

function main()
{  
    canvas = document.getElementById("AMC");
    if(!canvas)
        console.log("Obtaining Canvas Failed\n");
    else
        console.log("Obtaining Canvas Succeeded\n");
    canvas_original_width=canvas.width;
    canvas_original_height=canvas.height;
   
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);

    // initialize WebGL
    init();
    
    // start drawing here as warming-up
    resize();
    draw();
}

function toggleFullScreen()
{
    var fullscreen_element =
    document.fullscreenElement ||
    document.webkitFullscreenElement ||
    document.mozFullScreenElement ||
    document.msFullscreenElement ||
    null;

    if(fullscreen_element==null)
    {
        if(canvas.requestFullscreen)
            canvas.requestFullscreen();
        else if(canvas.mozRequestFullScreen)
            canvas.mozRequestFullScreen();
        else if(canvas.webkitRequestFullscreen)
            canvas.webkitRequestFullscreen();
        else if(canvas.msRequestFullscreen)
            canvas.msRequestFullscreen();
        bFullscreen=true;
    }
    else 
    {
        if(document.exitFullscreen)
            document.exitFullscreen();
        else if(document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if(document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if(document.msExitFullscreen)
            document.msExitFullscreen();
        bFullscreen=false;
    }
}

function init()
{
	gl = canvas.getContext("webgl2");
	if(gl == null)
	{
		console.log("Failed to get the rendering context for WebGL2");
		return;
	}

	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;

	 var vertexShaderSourceCode=
    "#version 300 es"+
    "\n"+
    "in vec4 vPosition;"+
    "in vec3 vNormal;"+
    "uniform mat4 u_model_matrix;"+
    "uniform mat4 u_view_matrix;"+
    "uniform mat4 u_projection_matrix;"+
    "uniform mediump int u_LKeyPressed;"+
    "uniform vec4 u_light_position;"+
    "out vec3 transformed_normals;"+
    "out vec3 light_direction;"+
    "out vec3 viewer_vector;"+
    "void main(void)"+
    "{"+
    "if(u_LKeyPressed == 1)"+
    "{"+
    "vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;"+
    "transformed_normals=normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);"+
    "light_direction = normalize(vec3(u_light_position) - eye_coordinates.xyz);"+
    "viewer_vector = -eye_coordinates.xyz;"+
    "}"+
    "gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"+
    "}"

	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}	

	var fragmentShaderSourceCode = 
	"#version 300 es"+
	"\n"+
	"precision highp float;"+
	"in vec3 transformed_normals;"+
	"in vec3 light_direction;"+
	"in vec3 viewer_vector;"+
	"out vec4 FragColor;"+
	"uniform vec3 u_La;"+
    "uniform vec3 u_Ld;"+
    "uniform vec3 u_Ls;"+
    "uniform vec3 u_Ka;"+
    "uniform vec3 u_Kd;"+
    "uniform vec3 u_Ks;"+
    "uniform float u_material_shininess;"+
    "uniform int u_LKeyPressed;"+
	"void main(void)"+
	"{"+
	"vec3 phong_ads_color;"+
	"if(u_LKeyPressed == 1)"+
	"{"+
	"vec3 normalized_transformed_normals = normalize(transformed_normals);"+
	"vec3 normalized_light_direction = normalize(light_direction);"+
	"vec3 normalized_viewer_vector = normalize(viewer_vector);"+
	"vec3 ambient = u_La * u_Ka;"+
	"float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction), 0.0);"+
	"vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;"+
	"vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);"+
	"vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);"+
	"phong_ads_color = ambient + diffuse + specular;"+
	"}"+
	"else"+
	"{"+
	"phong_ads_color = vec3(1.0, 1.0, 1.0);"+
	"}"+
	"FragColor = vec4(phong_ads_color, 1.0);"+
	"}"

	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//shader Program
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);

	//pre-link binding of shader program object with vertex shader attributes
	
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.VDG_ATTRIBUTE_NORMAL, "vNormal");
	gl.linkProgram(shaderProgramObject);
	if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	modelMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_model_matrix");
	viewMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_view_matrix");
	projectionMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_projection_matrix");
	LKeyPressedUniform = gl.getUniformLocation(shaderProgramObject, "u_LKeyPressed");
	laUniform = gl.getUniformLocation(shaderProgramObject, "u_La");
	ldUniform = gl.getUniformLocation(shaderProgramObject, "u_Ld");
	lsUniform = gl.getUniformLocation(shaderProgramObject, "u_Ls");
	lightPositionUniform = gl.getUniformLocation(shaderProgramObject, "u_light_position");
	kaUniform = gl.getUniformLocation(shaderProgramObject, "u_Ka");
	kdUniform = gl.getUniformLocation(shaderProgramObject, "u_Kd");
	ksUniform = gl.getUniformLocation(shaderProgramObject, "u_Ks");
	materialShininessUniform = gl.getUniformLocation(shaderProgramObject, "u_material_shininess");

	sphere = new Mesh();
	makeSphere(sphere, 2.0, 30, 30);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	gl.enable(gl.CULL_FACE);
	gl.clearColor(0.5, 0.5, 0.5, 1.0);
	perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	if(bFullscreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}

	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	//set viewport 
	gl.viewport(0, 0, canvas.width, canvas.height);

	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
}

function draw()
{
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	gl.useProgram(shaderProgramObject);

	if(light == true)
	{
		gl.uniform1i(LKeyPressedUniform, 1);
		gl.uniform3fv(laUniform, light_ambient);
		gl.uniform3fv(ldUniform, light_diffuse);
		gl.uniform3fv(lsUniform, light_specular);
		
		if(xAxis == true)
		{
			light_position[0] = 0.0;
			light_position[1] = 400 * degToRad(Math.sin(angle_xAxis));
			light_position[2] = 400 * degToRad(Math.cos(angle_xAxis));
			light_position[3] = 1.0;
		}

		if(yAxis == true)
		{
			light_position[1] = 0.0;
			light_position[2] = 400 * degToRad(Math.sin(angle_yAxis));
			light_position[0] = 400 * degToRad(Math.cos(angle_yAxis));
			light_position[3] = 1.0;
		}

		if(zAxis == true)
		{
			light_position[2] = 0.0;
			light_position[1] = 400 * degToRad(Math.sin(angle_zAxis));
			light_position[0] = 400 * degToRad(Math.cos(angle_zAxis));
			light_position[3] = 1.0;
		}
		gl.uniform4fv(lightPositionUniform, light_position);
	}

	else
	{
		gl.uniform1i(LKeyPressedUniform, 0);
	}

// 1st sphere
	var modelMatrix = mat4.create();
	var viewMatrix = mat4.create();
	//var scaleMatrix = mat4.create();

	mat4.translate(modelMatrix, modelMatrix, [-3.5, 2.5, -6.0]);
	mat4.scale(modelMatrix, modelMatrix, [0.15, 0.15, 0.15]);
	gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);
	gl.uniform3fv(kaUniform, material1_ambient);
	gl.uniform3fv(kdUniform, material1_diffuse);
	gl.uniform3fv(ksUniform, material1_specular);
	gl.uniform1f(materialShininessUniform, material1_shininess);
	sphere.draw();

// 2nd sphere
	 modelMatrix = mat4.create();
	 viewMatrix = mat4.create();
	
	mat4.translate(modelMatrix, modelMatrix, [-1.5, 2.5, -6.0]);
	mat4.scale(modelMatrix, modelMatrix, [0.15, 0.15, 0.15]);
	gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);
	gl.uniform3fv(kaUniform, material2_ambient);
	gl.uniform3fv(kdUniform, material2_diffuse);
	gl.uniform3fv(ksUniform, material2_specular);
	gl.uniform1f(materialShininessUniform, material2_shininess);
	sphere.draw();

// 3rd sphere
	 modelMatrix = mat4.create();
	 viewMatrix = mat4.create();
	
	mat4.translate(modelMatrix, modelMatrix, [0.5, 2.5, -6.0]);
	mat4.scale(modelMatrix, modelMatrix, [0.15, 0.15, 0.15]);
	gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);
	gl.uniform3fv(kaUniform, material3_ambient);
	gl.uniform3fv(kdUniform, material3_diffuse);
	gl.uniform3fv(ksUniform, material3_specular);
	gl.uniform1f(materialShininessUniform, material3_shininess);
	sphere.draw();

// 4th sphere
	 modelMatrix = mat4.create();
	 viewMatrix = mat4.create();
	
	mat4.translate(modelMatrix, modelMatrix, [2.5, 2.5, -6.0]);
	mat4.scale(modelMatrix, modelMatrix, [0.15, 0.15, 0.15]);
	gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);
	gl.uniform3fv(kaUniform, material4_ambient);
	gl.uniform3fv(kdUniform, material4_diffuse);
	gl.uniform3fv(ksUniform, material4_specular);
	gl.uniform1f(materialShininessUniform, material4_shininess);
	sphere.draw();

// 5th sphere
	 modelMatrix = mat4.create();
	 viewMatrix = mat4.create();
	
	mat4.translate(modelMatrix, modelMatrix, [-3.5, 1.5, -6.0]);
	mat4.scale(modelMatrix, modelMatrix, [0.15, 0.15, 0.15]);
	gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);
	gl.uniform3fv(kaUniform, material5_ambient);
	gl.uniform3fv(kdUniform, material5_diffuse);
	gl.uniform3fv(ksUniform, material5_specular);
	gl.uniform1f(materialShininessUniform, material5_shininess);
	sphere.draw();

// 6th sphere
	 modelMatrix = mat4.create();
	 viewMatrix = mat4.create();
	
	mat4.translate(modelMatrix, modelMatrix, [-1.5, 1.5, -6.0]);
	mat4.scale(modelMatrix, modelMatrix, [0.15, 0.15, 0.15]);
	gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);
	gl.uniform3fv(kaUniform, material6_ambient);
	gl.uniform3fv(kdUniform, material6_diffuse);
	gl.uniform3fv(ksUniform, material6_specular);
	gl.uniform1f(materialShininessUniform, material6_shininess);
	sphere.draw();

// 7th sphere
	 modelMatrix = mat4.create();
	 viewMatrix = mat4.create();
	
	mat4.translate(modelMatrix, modelMatrix, [0.5, 1.5, -6.0]);
	mat4.scale(modelMatrix, modelMatrix, [0.15, 0.15, 0.15]);
	gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);
	gl.uniform3fv(kaUniform, material7_ambient);
	gl.uniform3fv(kdUniform, material7_diffuse);
	gl.uniform3fv(ksUniform, material7_specular);
	gl.uniform1f(materialShininessUniform, material7_shininess);
	sphere.draw();

// 8th sphere
	 modelMatrix = mat4.create();
	 viewMatrix = mat4.create();
	
	mat4.translate(modelMatrix, modelMatrix, [2.5, 1.5, -6.0]);
	mat4.scale(modelMatrix, modelMatrix, [0.15, 0.15, 0.15]);
	gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);
	gl.uniform3fv(kaUniform, material8_ambient);
	gl.uniform3fv(kdUniform, material8_diffuse);
	gl.uniform3fv(ksUniform, material8_specular);
	gl.uniform1f(materialShininessUniform, material8_shininess);
	sphere.draw();


// 9th sphere
	 modelMatrix = mat4.create();
	 viewMatrix = mat4.create();
	
	mat4.translate(modelMatrix, modelMatrix, [-3.5, 0.5, -6.0]);
	mat4.scale(modelMatrix, modelMatrix, [0.15, 0.15, 0.15]);
	gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);
	gl.uniform3fv(kaUniform, material9_ambient);
	gl.uniform3fv(kdUniform, material9_diffuse);
	gl.uniform3fv(ksUniform, material9_specular);
	gl.uniform1f(materialShininessUniform, material9_shininess);
	sphere.draw();

// 10th sphere
	 modelMatrix = mat4.create();
	 viewMatrix = mat4.create();
	
	mat4.translate(modelMatrix, modelMatrix, [-1.5, 0.5, -6.0]);
	mat4.scale(modelMatrix, modelMatrix, [0.15, 0.15, 0.15]);
	gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);
	gl.uniform3fv(kaUniform, material10_ambient);
	gl.uniform3fv(kdUniform, material10_diffuse);
	gl.uniform3fv(ksUniform, material10_specular);
	gl.uniform1f(materialShininessUniform, material10_shininess);
	sphere.draw();

// 11th sphere
	 modelMatrix = mat4.create();
	 viewMatrix = mat4.create();
	
	mat4.translate(modelMatrix, modelMatrix, [0.5, 0.5, -6.0]);
	mat4.scale(modelMatrix, modelMatrix, [0.15, 0.15, 0.15]);
	gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);
	gl.uniform3fv(kaUniform, material11_ambient);
	gl.uniform3fv(kdUniform, material11_diffuse);
	gl.uniform3fv(ksUniform, material11_specular);
	gl.uniform1f(materialShininessUniform, material11_shininess);
	sphere.draw();

// 12th sphere
	 modelMatrix = mat4.create();
	 viewMatrix = mat4.create();
	
	mat4.translate(modelMatrix, modelMatrix, [2.5, 0.5, -6.0]);
	mat4.scale(modelMatrix, modelMatrix, [0.15, 0.15, 0.15]);
	gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);
	gl.uniform3fv(kaUniform, material12_ambient);
	gl.uniform3fv(kdUniform, material12_diffuse);
	gl.uniform3fv(ksUniform, material12_specular);
	gl.uniform1f(materialShininessUniform, material12_shininess);
	sphere.draw();

// 13th sphere
	 modelMatrix = mat4.create();
	 viewMatrix = mat4.create();
	
	mat4.translate(modelMatrix, modelMatrix, [-3.5, -0.5, -6.0]);
	mat4.scale(modelMatrix, modelMatrix, [0.15, 0.15, 0.15]);
	gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);
	gl.uniform3fv(kaUniform, material13_ambient);
	gl.uniform3fv(kdUniform, material13_diffuse);
	gl.uniform3fv(ksUniform, material13_specular);
	gl.uniform1f(materialShininessUniform, material13_shininess);
	sphere.draw();

// 14th sphere
	 modelMatrix = mat4.create();
	 viewMatrix = mat4.create();
	
	mat4.translate(modelMatrix, modelMatrix, [-1.5, -0.5, -6.0]);
	mat4.scale(modelMatrix, modelMatrix, [0.15, 0.15, 0.15]);
	gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);
	gl.uniform3fv(kaUniform, material14_ambient);
	gl.uniform3fv(kdUniform, material14_diffuse);
	gl.uniform3fv(ksUniform, material14_specular);
	gl.uniform1f(materialShininessUniform, material14_shininess);
	sphere.draw();

// 15th sphere
	 modelMatrix = mat4.create();
	 viewMatrix = mat4.create();
	
	mat4.translate(modelMatrix, modelMatrix, [0.5, -0.5, -6.0]);
	mat4.scale(modelMatrix, modelMatrix, [0.15, 0.15, 0.15]);
	gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);
	gl.uniform3fv(kaUniform, material15_ambient);
	gl.uniform3fv(kdUniform, material15_diffuse);
	gl.uniform3fv(ksUniform, material15_specular);
	gl.uniform1f(materialShininessUniform, material15_shininess);
	sphere.draw();

// 16th sphere
	 modelMatrix = mat4.create();
	 viewMatrix = mat4.create();
	
	mat4.translate(modelMatrix, modelMatrix, [2.5, -0.5, -6.0]);
	mat4.scale(modelMatrix, modelMatrix, [0.15, 0.15, 0.15]);
	gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);
	gl.uniform3fv(kaUniform, material16_ambient);
	gl.uniform3fv(kdUniform, material16_diffuse);
	gl.uniform3fv(ksUniform, material16_specular);
	gl.uniform1f(materialShininessUniform, material16_shininess);
	sphere.draw();

// 17th sphere
	 modelMatrix = mat4.create();
	 viewMatrix = mat4.create();
	
	mat4.translate(modelMatrix, modelMatrix, [-3.5, -1.5, -6.0]);
	mat4.scale(modelMatrix, modelMatrix, [0.15, 0.15, 0.15]);
	gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);
	gl.uniform3fv(kaUniform, material17_ambient);
	gl.uniform3fv(kdUniform, material17_diffuse);
	gl.uniform3fv(ksUniform, material17_specular);
	gl.uniform1f(materialShininessUniform, material17_shininess);
	sphere.draw();

// 18th sphere
	 modelMatrix = mat4.create();
	 viewMatrix = mat4.create();
	
	mat4.translate(modelMatrix, modelMatrix, [-1.5, -1.5, -6.0]);
	mat4.scale(modelMatrix, modelMatrix, [0.15, 0.15, 0.15]);
	gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);
	gl.uniform3fv(kaUniform, material18_ambient);
	gl.uniform3fv(kdUniform, material18_diffuse);
	gl.uniform3fv(ksUniform, material18_specular);
	gl.uniform1f(materialShininessUniform, material18_shininess);
	sphere.draw();

// 19th sphere
	 modelMatrix = mat4.create();
	 viewMatrix = mat4.create();
	
	mat4.translate(modelMatrix, modelMatrix, [0.5, -1.5, -6.0]);
	mat4.scale(modelMatrix, modelMatrix, [0.15, 0.15, 0.15]);
	gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);
	gl.uniform3fv(kaUniform, material19_ambient);
	gl.uniform3fv(kdUniform, material19_diffuse);
	gl.uniform3fv(ksUniform, material19_specular);
	gl.uniform1f(materialShininessUniform, material19_shininess);
	sphere.draw();

// 20th sphere
	 modelMatrix = mat4.create();
	 viewMatrix = mat4.create();
	
	mat4.translate(modelMatrix, modelMatrix, [2.5, -1.5, -6.0]);
	mat4.scale(modelMatrix, modelMatrix, [0.15, 0.15, 0.15]);
	gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);
	gl.uniform3fv(kaUniform, material20_ambient);
	gl.uniform3fv(kdUniform, material20_diffuse);
	gl.uniform3fv(ksUniform, material20_specular);
	gl.uniform1f(materialShininessUniform, material20_shininess);
	sphere.draw();


// 21th sphere
	 modelMatrix = mat4.create();
	 viewMatrix = mat4.create();
	
	mat4.translate(modelMatrix, modelMatrix, [-3.5, -2.5, -6.0]);
	mat4.scale(modelMatrix, modelMatrix, [0.15, 0.15, 0.15]);
	gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);
	gl.uniform3fv(kaUniform, material21_ambient);
	gl.uniform3fv(kdUniform, material21_diffuse);
	gl.uniform3fv(ksUniform, material21_specular);
	gl.uniform1f(materialShininessUniform, material21_shininess);
	sphere.draw();

// 22th sphere
	 modelMatrix = mat4.create();
	 viewMatrix = mat4.create();
	
	mat4.translate(modelMatrix, modelMatrix, [-1.5, -2.5, -6.0]);
	mat4.scale(modelMatrix, modelMatrix, [0.15, 0.15, 0.15]);
	gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);
	gl.uniform3fv(kaUniform, material22_ambient);
	gl.uniform3fv(kdUniform, material22_diffuse);
	gl.uniform3fv(ksUniform, material22_specular);
	gl.uniform1f(materialShininessUniform, material22_shininess);
	sphere.draw();

// 23th sphere
	 modelMatrix = mat4.create();
	 viewMatrix = mat4.create();
	
	mat4.translate(modelMatrix, modelMatrix, [0.5, -2.5, -6.0]);
	mat4.scale(modelMatrix, modelMatrix, [0.15, 0.15, 0.15]);
	gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);
	gl.uniform3fv(kaUniform, material23_ambient);
	gl.uniform3fv(kdUniform, material23_diffuse);
	gl.uniform3fv(ksUniform, material23_specular);
	gl.uniform1f(materialShininessUniform, material23_shininess);
	sphere.draw();

// 24th sphere
	 modelMatrix = mat4.create();
	 viewMatrix = mat4.create();
	
	mat4.translate(modelMatrix, modelMatrix, [2.5, -2.5, -6.0]);
	mat4.scale(modelMatrix, modelMatrix, [0.15, 0.15, 0.15]);
	gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);
	gl.uniform3fv(kaUniform, material24_ambient);
	gl.uniform3fv(kdUniform, material24_diffuse);
	gl.uniform3fv(ksUniform, material24_specular);
	gl.uniform1f(materialShininessUniform, material24_shininess);
	sphere.draw();


	gl.useProgram(null);
	update();
	requestAnimationFrame(draw, canvas);
}

function uninitialize()
{
	if(sphere)
	{
		sphere.deallocate();
		sphere = null;
	}

	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}

		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}

function keyDown(event)
{
	switch(event.key)
	{
		case 'F':
		case 'f':
		toggleFullScreen();
		break;

		case 'X':
		case 'x':
			xAxis = true;
			yAxis = false;
			zAxis = false;
			break;

		case 'Y':
		case 'y':
			xAxis = false;
			yAxis = true;
			zAxis = false;
			break;

		case 'Z':
		case 'z':
			xAxis = false;
			yAxis = false;
			zAxis = true;
			break;

	}

	switch(event.keyCode)
	{
		case 27:
		uninitialize();
		window.close();
		break;

		case 76:
			if(bLKeyPressed == false)
			{
				light = true;
				bLKeyPressed = true;
			}
			else
			{ 
				light = false;
				bLKeyPressed = false;
			}
			break;

		case 70:
			toggleFullScreen();
			break;
	}
}

function mouseDown()
{

}

function update()
{
	angle_xAxis = angle_xAxis + 0.05;
	if(angle_xAxis >= 360.0)
	{
		angle_xAxis = angle_xAxis - 360.0;
	}

	angle_yAxis = angle_yAxis + 0.05;
	if(angle_yAxis >= 360.0)
	{
		angle_yAxis = angle_yAxis - 360.0;
	}

	angle_zAxis = angle_zAxis + 0.05;
	if(angle_zAxis >= 360.0)
	{
		angle_zAxis = angle_zAxis - 360.0;
	}
}

function degToRad(degrees)
{
	return(degrees * Math.PI/180.0);
}