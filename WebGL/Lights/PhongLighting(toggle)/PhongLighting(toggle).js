var canvas = null;
var gl = null;
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros = 
{
	VDG_ATTRIBUTE_VERTEX:0,
	VDG_ATTRIBUTE_COLOR:1,
	VDG_ATTRIBUTE_NORMAL:2,
	VDG_ATTRIBUTE_TEXTURE0:3,
};

var vertexShaderObjectPhongPerVertex;
var fragmentShaderObjectPhongPerVertex;
var shaderProgramObjectPhongPerVertex;

var vertexShaderObjectPhongPerFragment;
var fragmentShaderObjectPhongPerFragment;
var shaderProgramObjectPhongPerFragment;

var perVertex = true;
var perFragment = false;

var light_ambient = [0.0, 0.0, 0.0];
var light_diffuse = [1.0, 1.0, 1.0];
var light_specular = [1.0, 1.0, 1.0];
var light_position = [100.0, 100.0, 100.0, 1.0];

var material_ambient = [0.0, 0.0, 0.0];
var material_diffuse = [1.0, 1.0, 1.0];
var material_specular = [1.0, 1.0, 1.0];
var material_shininess = 50.0;

var sphere = null;

var perspectiveProjectionMatrixf;

var modelMatrixUniformf,viewMatrixUniformf, projectionMatrixUniformf;

var laUniformf, ldUniformf, lsUniformf;

var kaUniformf, kdUniformf, ksUniformf, materialShininessUniformf, lightPositionUniformf;
var LKeyPressedUniformf;

var perspectiveProjectionMatrix;

var modelMatrixUniform,viewMatrixUniform, projectionMatrixUniform;

var laUniform, ldUniform, lsUniform;

var kaUniform, kdUniform, ksUniform, materialShininessUniform, lightPositionUniform;
var LKeyPressedUniform;

var bLKeyPressed = false;

var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

var cancelAnimationFrame =
window.cancelAnimationFrame ||
window.webkitCancelRequestAnimationFrame || 
window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame || 
window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || 
window.oCancelAnimationFrame ||
window.msCancelRequestAnimationFrame || 
window.msCancelAnimationFrame;

function main()
{  
    canvas = document.getElementById("AMC");
    if(!canvas)
        console.log("Obtaining Canvas Failed\n");
    else
        console.log("Obtaining Canvas Succeeded\n");
    canvas_original_width=canvas.width;
    canvas_original_height=canvas.height;
   
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);

    // initialize WebGL
    init();
    
    // start drawing here as warming-up
    resize();
    draw();
}

function toggleFullScreen()
{
    var fullscreen_element =
    document.fullscreenElement ||
    document.webkitFullscreenElement ||
    document.mozFullScreenElement ||
    document.msFullscreenElement ||
    null;

    if(fullscreen_element==null)
    {
        if(canvas.requestFullscreen)
            canvas.requestFullscreen();
        else if(canvas.mozRequestFullScreen)
            canvas.mozRequestFullScreen();
        else if(canvas.webkitRequestFullscreen)
            canvas.webkitRequestFullscreen();
        else if(canvas.msRequestFullscreen)
            canvas.msRequestFullscreen();
        bFullscreen=true;
    }
    else 
    {
        if(document.exitFullscreen)
            document.exitFullscreen();
        else if(document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if(document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if(document.msExitFullscreen)
            document.msExitFullscreen();
        bFullscreen=false;
    }
}

function init()
{
	gl = canvas.getContext("webgl2");
	if(gl == null)
	{
		console.log("Failed to get the rendering context for WebGL2");
		return;
	}

	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;

	 var vertexShaderSourceCodePerVertex=
    "#version 300 es"+
    "\n"+
    "in vec4 vPosition;"+
    "in vec3 vNormal;"+
    "uniform mat4 u_model_matrix;"+
    "uniform mat4 u_view_matrix;"+
    "uniform mat4 u_projection_matrix;"+
    "uniform mediump int u_LKeyPressed;"+
    "uniform vec3 u_La;"+
    "uniform vec3 u_Ld;"+
    "uniform vec3 u_Ls;"+
    "uniform vec4 u_light_position;"+
    "uniform vec3 u_Ka;"+
    "uniform vec3 u_Kd;"+
    "uniform vec3 u_Ks;"+
    "uniform float u_material_shininess;"+
    "out vec3 phong_ads_color;"+
    "void main(void)"+
    "{"+
    "if(u_LKeyPressed == 1)"+
    "{"+
    "vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;"+
    "vec3 transformed_normals=normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);"+
    "vec3 light_direction = normalize(vec3(u_light_position) - eye_coordinates.xyz);"+
    "float tn_dot_ld = max(dot(transformed_normals, light_direction),0.0);"+
    "vec3 ambient = u_La * u_Ka;"+
    "vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;"+
    "vec3 reflection_vector = reflect(-light_direction, transformed_normals);"+
    "vec3 viewer_vector = normalize(-eye_coordinates.xyz);"+
    "vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, viewer_vector), 0.0), u_material_shininess);"+
    "phong_ads_color=ambient + diffuse + specular;"+
    "}"+
    "else"+
    "{"+
    "phong_ads_color = vec3(1.0, 1.0, 1.0);"+
    "}"+
    "gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"+
    "}"

	vertexShaderObjectPhongPerVertex = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObjectPhongPerVertex, vertexShaderSourceCodePerVertex);
	gl.compileShader(vertexShaderObjectPhongPerVertex);
	if(gl.getShaderParameter(vertexShaderObjectPhongPerVertex, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObjectPhongPerVertex);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}	

	var fragmentShaderSourceCodePerVertex = 
	"#version 300 es"+
	"\n"+
	"precision highp float;"+
	"in vec3 phong_ads_color;"+
	"out vec4 FragColor;"+
	"void main(void)"+
	"{"+
	"FragColor = vec4(phong_ads_color, 1.0);"+
	"}"

	fragmentShaderObjectPhongPerVertex = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObjectPhongPerVertex, fragmentShaderSourceCodePerVertex);
	gl.compileShader(fragmentShaderObjectPhongPerVertex);
	if(gl.getShaderParameter(fragmentShaderObjectPhongPerVertex, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObjectPhongPerVertex);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//shader Program
	shaderProgramObjectPhongPerVertex = gl.createProgram();
	gl.attachShader(shaderProgramObjectPhongPerVertex, vertexShaderObjectPhongPerVertex);
	gl.attachShader(shaderProgramObjectPhongPerVertex, fragmentShaderObjectPhongPerVertex);

	//pre-link binding of shader program object with vertex shader attributes
	
	gl.bindAttribLocation(shaderProgramObjectPhongPerVertex, WebGLMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObjectPhongPerVertex, WebGLMacros.VDG_ATTRIBUTE_NORMAL, "vNormal");
	gl.linkProgram(shaderProgramObjectPhongPerVertex);
	if(!gl.getProgramParameter(shaderProgramObjectPhongPerVertex, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObjectPhongPerVertex);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	modelMatrixUniform = gl.getUniformLocation(shaderProgramObjectPhongPerVertex, "u_model_matrix");
	viewMatrixUniform = gl.getUniformLocation(shaderProgramObjectPhongPerVertex, "u_view_matrix");
	projectionMatrixUniform = gl.getUniformLocation(shaderProgramObjectPhongPerVertex, "u_projection_matrix");
	LKeyPressedUniform = gl.getUniformLocation(shaderProgramObjectPhongPerVertex, "u_LKeyPressed");
	laUniform = gl.getUniformLocation(shaderProgramObjectPhongPerVertex, "u_La");
	ldUniform = gl.getUniformLocation(shaderProgramObjectPhongPerVertex, "u_Ld");
	lsUniform = gl.getUniformLocation(shaderProgramObjectPhongPerVertex, "u_Ls");
	lightPositionUniform = gl.getUniformLocation(shaderProgramObjectPhongPerVertex, "u_light_position");
	kaUniform = gl.getUniformLocation(shaderProgramObjectPhongPerVertex, "u_Ka");
	kdUniform = gl.getUniformLocation(shaderProgramObjectPhongPerVertex, "u_Kd");
	ksUniform = gl.getUniformLocation(shaderProgramObjectPhongPerVertex, "u_Ks");
	materialShininessUniform = gl.getUniformLocation(shaderProgramObjectPhongPerVertex, "u_material_shininess");


	//Per Fragment

	 var vertexShaderSourceCodePerFragment=
    "#version 300 es"+
    "\n"+
    "in vec4 vPosition;"+
    "in vec3 vNormal;"+
    "uniform mat4 u_model_matrix;"+
    "uniform mat4 u_view_matrix;"+
    "uniform mat4 u_projection_matrix;"+
    "uniform mediump int u_LKeyPressed;"+
    "uniform vec4 u_light_position;"+
    "out vec3 transformed_normals;"+
    "out vec3 light_direction;"+
    "out vec3 viewer_vector;"+
    "void main(void)"+
    "{"+
    "if(u_LKeyPressed == 1)"+
    "{"+
    "vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;"+
    "transformed_normals=normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);"+
    "light_direction = normalize(vec3(u_light_position) - eye_coordinates.xyz);"+
    "viewer_vector = -eye_coordinates.xyz;"+
    "}"+
    "gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"+
    "}"

	vertexShaderObjectPhongPerFragment = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObjectPhongPerFragment, vertexShaderSourceCodePerFragment);
	gl.compileShader(vertexShaderObjectPhongPerFragment);
	if(gl.getShaderParameter(vertexShaderObjectPhongPerFragment, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObjectPhongPerFragment);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}	

	var fragmentShaderSourceCodePerFragment = 
	"#version 300 es"+
	"\n"+
	"precision highp float;"+
	"in vec3 transformed_normals;"+
	"in vec3 light_direction;"+
	"in vec3 viewer_vector;"+
	"out vec4 FragColor;"+
	"uniform vec3 u_La;"+
    "uniform vec3 u_Ld;"+
    "uniform vec3 u_Ls;"+
    "uniform vec3 u_Ka;"+
    "uniform vec3 u_Kd;"+
    "uniform vec3 u_Ks;"+
    "uniform float u_material_shininess;"+
    "uniform int u_LKeyPressed;"+
	"void main(void)"+
	"{"+
	"vec3 phong_ads_color;"+
	"if(u_LKeyPressed == 1)"+
	"{"+
	"vec3 normalized_transformed_normals = normalize(transformed_normals);"+
	"vec3 normalized_light_direction = normalize(light_direction);"+
	"vec3 normalized_viewer_vector = normalize(viewer_vector);"+
	"vec3 ambient = u_La * u_Ka;"+
	"float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction), 0.0);"+
	"vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;"+
	"vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);"+
	"vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);"+
	"phong_ads_color = ambient + diffuse + specular;"+
	"}"+
	"else"+
	"{"+
	"phong_ads_color = vec3(1.0, 1.0, 1.0);"+
	"}"+
	"FragColor = vec4(phong_ads_color, 1.0);"+
	"}"

	fragmentShaderObjectPhongPerFragment = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObjectPhongPerFragment, fragmentShaderSourceCodePerFragment);
	gl.compileShader(fragmentShaderObjectPhongPerFragment);
	if(gl.getShaderParameter(fragmentShaderObjectPhongPerFragment, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObjectPhongPerFragment);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//shader Program
	shaderProgramObjectPhongPerFragment = gl.createProgram();
	gl.attachShader(shaderProgramObjectPhongPerFragment, vertexShaderObjectPhongPerFragment);
	gl.attachShader(shaderProgramObjectPhongPerFragment, fragmentShaderObjectPhongPerFragment);

	//pre-link binding of shader program object with vertex shader attributes
	
	gl.bindAttribLocation(shaderProgramObjectPhongPerFragment, WebGLMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObjectPhongPerFragment, WebGLMacros.VDG_ATTRIBUTE_NORMAL, "vNormal");
	gl.linkProgram(shaderProgramObjectPhongPerFragment);
	if(!gl.getProgramParameter(shaderProgramObjectPhongPerFragment, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObjectPhongPerFragment);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	
	//sphere = new Mesh();
	//makeSphere(sphere, 2.0, 30, 30);

	

	modelMatrixUniformf = gl.getUniformLocation(shaderProgramObjectPhongPerFragment, "u_model_matrix");
	viewMatrixUniformf = gl.getUniformLocation(shaderProgramObjectPhongPerFragment, "u_view_matrix");
	projectionMatrixUniformf = gl.getUniformLocation(shaderProgramObjectPhongPerFragment, "u_projection_matrix");
	LKeyPressedUniformf = gl.getUniformLocation(shaderProgramObjectPhongPerFragment, "u_LKeyPressed");
	laUniformf = gl.getUniformLocation(shaderProgramObjectPhongPerFragment, "u_La");
	ldUniformf = gl.getUniformLocation(shaderProgramObjectPhongPerFragment, "u_Ld");
	lsUniformf = gl.getUniformLocation(shaderProgramObjectPhongPerFragment, "u_Ls");
	lightPositionUniformf = gl.getUniformLocation(shaderProgramObjectPhongPerFragment, "u_light_position");
	kaUniformf = gl.getUniformLocation(shaderProgramObjectPhongPerFragment, "u_Ka");
	kdUniformf = gl.getUniformLocation(shaderProgramObjectPhongPerFragment, "u_Kd");
	ksUniformf = gl.getUniformLocation(shaderProgramObjectPhongPerFragment, "u_Ks");
	materialShininessUniformf = gl.getUniformLocation(shaderProgramObjectPhongPerFragment, "u_material_shininess");

	
	sphere = new Mesh();
	makeSphere(sphere, 2.0, 30, 30);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	gl.enable(gl.CULL_FACE);
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	if(bFullscreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}

	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	//set viewport 
	gl.viewport(0, 0, canvas.width, canvas.height);

	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
}

function draw()
{
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	
	if(perVertex == true)
	{
		gl.useProgram(shaderProgramObjectPhongPerVertex);
		if(bLKeyPressed == true)
	{
		gl.uniform1i(LKeyPressedUniform, 1);
		gl.uniform3fv(laUniform, light_ambient);
		gl.uniform3fv(ldUniform, light_diffuse);
		gl.uniform3fv(lsUniform, light_specular);
		gl.uniform4fv(lightPositionUniform, light_position);
		gl.uniform3fv(kaUniform, material_ambient);
		gl.uniform3fv(kdUniform, material_diffuse);
		gl.uniform3fv(ksUniform, material_specular);
		gl.uniform1f(materialShininessUniform, material_shininess);
	}

	else
	{
		gl.uniform1i(LKeyPressedUniform, 0);
	}

	var modelMatrix = mat4.create();
	var viewMatrix = mat4.create();

	mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, -6.0]);
	gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);
	sphere.draw();
	gl.useProgram(null);
	requestAnimationFrame(draw, canvas);
	}

	else 
	{
		gl.useProgram(shaderProgramObjectPhongPerFragment);
		if(bLKeyPressed == true)
	{
		gl.uniform1i(LKeyPressedUniformf, 1);
		gl.uniform3fv(laUniformf, light_ambient);
		gl.uniform3fv(ldUniformf, light_diffuse);
		gl.uniform3fv(lsUniformf, light_specular);
		gl.uniform4fv(lightPositionUniformf, light_position);
		gl.uniform3fv(kaUniformf, material_ambient);
		gl.uniform3fv(kdUniformf, material_diffuse);
		gl.uniform3fv(ksUniformf, material_specular);
		gl.uniform1f(materialShininessUniformf, material_shininess);
	}

	else
	{
		gl.uniform1i(LKeyPressedUniformf, 0);
	}

	var modelMatrix = mat4.create();
	var viewMatrix = mat4.create();

	mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, -6.0]);
	gl.uniformMatrix4fv(modelMatrixUniformf, false, modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniformf, false, viewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniformf, false, perspectiveProjectionMatrix);
	sphere.draw();
	gl.useProgram(null);
	requestAnimationFrame(draw, canvas);
	}
	
}

function uninitialize()
{
	if(sphere)
	{
		sphere.deallocate();
		sphere = null;
	}

	if(shaderProgramObjectPhongPerFragment)
	{
		if(fragmentShaderObjectPhongPerFragment)
		{
			gl.detachShader(shaderProgramObjectPhongPerFragment, fragmentShaderObjectPhongPerFragment);
			gl.deleteShader(fragmentShaderObjectPhongPerFragment);
			fragmentShaderObjectPhongPerFragment = null;
		}

		if(vertexShaderObjectPhongPerFragment)
		{
			gl.detachShader(shaderProgramObjectPhongPerFragment, vertexShaderObjectPhongPerFragment);
			gl.deleteShader(vertexShaderObjectPhongPerFragment);
			vertexShaderObjectPhongPerFragment = null;
		}
		gl.deleteProgram(shaderProgramObjectPhongPerFragment);
		shaderProgramObjectPhongPerFragment = null;
	}


	if(shaderProgramObjectPhongPerVertex)
	{
		if(fragmentShaderObjectPhongPerVertex)
		{
			gl.detachShader(shaderProgramObjectPhongPerVertex, fragmentShaderObjectPhongPerVertex);
			gl.deleteShader(fragmentShaderObjectPhongPerVertex);
			fragmentShaderObjectPhongPerVertex = null;
		}

		if(vertexShaderObjectPhongPerVertex)
		{
			gl.detachShader(shaderProgramObjectPhongPerVertex, vertexShaderObjectPhongPerVertex);
			gl.deleteShader(vertexShaderObjectPhongPerVertex);
			vertexShaderObjectPhongPerVertex = null;
		}
		gl.deleteProgram(shaderProgramObjectPhongPerVertex);
		shaderProgramObjectPhongPerVertex = null;
	}
}

function keyDown(event)
{
	switch(event.key)
	{
		case 'Q':
		case 'q':
		toggleFullScreen();
		break;
	
		case 'F':
		case 'f':
			perFragment = true;
			perVertex = false;
			break;

		case 'V':
		case 'v':
			perVertex = true;
			perFragment = false;
			break;
	}

	switch(event.keyCode)
	{
		case 27:
		uninitialize();
		window.close();
		break;

		case 76:
			if(bLKeyPressed == false)
				bLKeyPressed = true;
			else 
				bLKeyPressed = false;
			break;	
	}
}

function mouseDown()
{

}
