var canvas = null;
var gl = null;
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros = 
{
	VDG_ATTRIBUTE_VERTEX:0,
	VDG_ATTRIBUTE_COLOR:1,
	VDG_ATTRIBUTE_NORMAL:2,
	VDG_ATTRIBUTE_TEXTURE0:3,
};

var vertexShaderObjectPhongPerVertex;
var fragmentShaderObjectPhongPerVertex;
var shaderProgramObjectPhongPerVertex;

var vertexShaderObjectPhongPerFragment;
var fragmentShaderObjectPhongPerFragment;
var shaderProgramObjectPhongPerFragment;

var perVertex = true;
var perFragment = false;

var light1_ambient = [0.0, 0.0, 0.0];
var light1_diffuse = [1.0, 0.0, 0.0];
var light1_specular = [1.0, 0.0, 0.0];

var light2_ambient = [0.0, 0.0, 0.0];
var light2_diffuse = [0.0, 1.0, 0.0];
var light2_specular = [0.0, 1.0, 0.0];

var light3_ambient = [0.0, 0.0, 0.0];
var light3_diffuse = [0.0, 0.0, 1.0];
var light3_specular = [0.0, 0.0, 1.0];
//var light1_position = [100.0, 100.0, 100.0, 1.0];

var material_ambient = [0.0, 0.0, 0.0];
var material_diffuse = [1.0, 1.0, 1.0];
var material_specular = [1.0, 1.0, 1.0];
var material_shininess = 50.0;

var sphere = null;

var perspectiveProjectionMatrixf;

var modelMatrixUniformf,viewMatrixUniformf, projectionMatrixUniformf;

var la1Uniformf, ld1Uniformf, ls1Uniformf;
var ka1Uniformf, kd1Uniformf, ks1Uniformf, material1ShininessUniformf, light1PositionUniformf;

var la2Uniformf, ld2Uniformf, ls2Uniformf;
var ka2Uniformf, kd2Uniformf, ks2Uniformf, material2ShininessUniformf, light2PositionUniformf;

var la3Uniformf, ld3Uniformf, ls3Uniformf;
var ka3Uniformf, kd3Uniformf, ks3Uniformf, material3ShininessUniformf, light3PositionUniformf;

//var LKeyPressedUniformf;

var perspectiveProjectionMatrix;

var modelMatrixUniform,viewMatrixUniform, projectionMatrixUniform;

var la1Uniform, ld1Uniform, ls1Uniform;
var ka1Uniform, kd1Uniform, ks1Uniform, material1ShininessUniform, light1PositionUniform;

var la2Uniform, ld2Uniform, ls2Uniform;
var ka2Uniform, kd2Uniform, ks2Uniform, material2ShininessUniform, light2PositionUniform;

var la3Uniform, ld3Uniform, ls3Uniform;
var ka3Uniform, kd3Uniform, ks3Uniform, material3ShininessUniform, light3PositionUniform;

var angleRed = 0.0, angleGreen = 0.0, angleBlue = 0.0;

//var LKeyPressedUniform;

//var bLKeyPressed = false;

var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

var cancelAnimationFrame =
window.cancelAnimationFrame ||
window.webkitCancelRequestAnimationFrame || 
window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame || 
window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || 
window.oCancelAnimationFrame ||
window.msCancelRequestAnimationFrame || 
window.msCancelAnimationFrame;

function main()
{  
    canvas = document.getElementById("AMC");
    if(!canvas)
        console.log("Obtaining Canvas Failed\n");
    else
        console.log("Obtaining Canvas Succeeded\n");
    canvas_original_width=canvas.width;
    canvas_original_height=canvas.height;
   
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);

    // initialize WebGL
    init();
    
    // start drawing here as warming-up
    resize();
    draw();
}

function toggleFullScreen()
{
    var fullscreen_element =
    document.fullscreenElement ||
    document.webkitFullscreenElement ||
    document.mozFullScreenElement ||
    document.msFullscreenElement ||
    null;

    if(fullscreen_element==null)
    {
        if(canvas.requestFullscreen)
            canvas.requestFullscreen();
        else if(canvas.mozRequestFullScreen)
            canvas.mozRequestFullScreen();
        else if(canvas.webkitRequestFullscreen)
            canvas.webkitRequestFullscreen();
        else if(canvas.msRequestFullscreen)
            canvas.msRequestFullscreen();
        bFullscreen=true;
    }
    else 
    {
        if(document.exitFullscreen)
            document.exitFullscreen();
        else if(document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if(document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if(document.msExitFullscreen)
            document.msExitFullscreen();
        bFullscreen=false;
    }
}

function init()
{
	gl = canvas.getContext("webgl2");
	if(gl == null)
	{
		console.log("Failed to get the rendering context for WebGL2");
		return;
	}

	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;

	 var vertexShaderSourceCodePerVertex=
    "#version 300 es"+
    "\n"+
    "in vec4 vPosition;"+
    "in vec3 vNormal;"+
    "uniform mat4 u_model_matrix;"+
    "uniform mat4 u_view_matrix;"+
    "uniform mat4 u_projection_matrix;"+
    "uniform vec3 u_La1;"+
    "uniform vec3 u_Ld1;"+
    "uniform vec3 u_Ls1;"+
    "uniform vec4 u_light1_position;"+
    "uniform vec3 u_Ka1;"+
    "uniform vec3 u_Kd1;"+
    "uniform vec3 u_Ks1;"+
    "uniform float u_material1_shininess;"+
    "out vec3 phong_ads_color1;"+
    "uniform vec3 u_La2;"+
    "uniform vec3 u_Ld2;"+
    "uniform vec3 u_Ls2;"+
    "uniform vec4 u_light2_position;"+
    "uniform vec3 u_Ka2;"+
    "uniform vec3 u_Kd2;"+
    "uniform vec3 u_Ks2;"+
    "uniform float u_material2_shininess;"+
    "out vec3 phong_ads_color2;"+
    "uniform vec3 u_La3;"+
    "uniform vec3 u_Ld3;"+
    "uniform vec3 u_Ls3;"+
    "uniform vec4 u_light3_position;"+
    "uniform vec3 u_Ka3;"+
    "uniform vec3 u_Kd3;"+
    "uniform vec3 u_Ks3;"+
    "uniform float u_material3_shininess;"+
    "out vec3 phong_ads_color3;"+
    "void main(void)"+
    "{"+
    "vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;"+
    "vec3 transformed_normals=normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);"+

    "vec3 light_direction1 = normalize(vec3(u_light1_position) - eye_coordinates.xyz);"+
    "vec3 light_direction2 = normalize(vec3(u_light2_position) - eye_coordinates.xyz);"+
    "vec3 light_direction3 = normalize(vec3(u_light3_position) - eye_coordinates.xyz);"+
    
    "float tn_dot_ld1 = max(dot(transformed_normals, light_direction1),0.0);"+
	"float tn_dot_ld2 = max(dot(transformed_normals, light_direction2),0.0);"+
	"float tn_dot_ld3 = max(dot(transformed_normals, light_direction3),0.0);"+
    

    "vec3 ambient1 = u_La1 * u_Ka1;"+
    "vec3 diffuse1 = u_Ld1 * u_Kd1 * tn_dot_ld1;"+

    "vec3 ambient2 = u_La2 * u_Ka2;"+
    "vec3 diffuse2 = u_Ld2 * u_Kd2 * tn_dot_ld2;"+

    "vec3 ambient3 = u_La3 * u_Ka3;"+
    "vec3 diffuse3 = u_Ld3 * u_Kd3 * tn_dot_ld3;"+
    
    "vec3 reflection_vector1 = reflect(-light_direction1, transformed_normals);"+
    "vec3 reflection_vector2 = reflect(-light_direction2, transformed_normals);"+
    "vec3 reflection_vector3 = reflect(-light_direction3, transformed_normals);"+
    
    "vec3 viewer_vector = normalize(-eye_coordinates.xyz);"+
    
    "vec3 specular1 = u_Ls1 * u_Ks1 * pow(max(dot(reflection_vector1, viewer_vector), 0.0), u_material1_shininess);"+
    "vec3 specular2 = u_Ls2 * u_Ks2 * pow(max(dot(reflection_vector2, viewer_vector), 0.0), u_material2_shininess);"+
    "vec3 specular3 = u_Ls3 * u_Ks3 * pow(max(dot(reflection_vector3, viewer_vector), 0.0), u_material3_shininess);"+

    "phong_ads_color1 = ambient1 + diffuse1 + specular1;"+
    "phong_ads_color2 = ambient2 + diffuse2 + specular2;"+
    "phong_ads_color3 = ambient3 + diffuse3 + specular3;"+

    "gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"+
    "}"

	vertexShaderObjectPhongPerVertex = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObjectPhongPerVertex, vertexShaderSourceCodePerVertex);
	gl.compileShader(vertexShaderObjectPhongPerVertex);
	if(gl.getShaderParameter(vertexShaderObjectPhongPerVertex, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObjectPhongPerVertex);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}	

	var fragmentShaderSourceCodePerVertex = 
	"#version 300 es"+
	"\n"+
	"precision highp float;"+
	"in vec3 phong_ads_color1;"+
	"in vec3 phong_ads_color2;"+
	"in vec3 phong_ads_color3;"+
	"out vec4 FragColor;"+
	"void main(void)"+
	"{"+
	"FragColor = vec4(phong_ads_color1, 1.0) + vec4(phong_ads_color2, 1.0) + vec4(phong_ads_color3, 1.0);"+
	"}"

	fragmentShaderObjectPhongPerVertex = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObjectPhongPerVertex, fragmentShaderSourceCodePerVertex);
	gl.compileShader(fragmentShaderObjectPhongPerVertex);
	if(gl.getShaderParameter(fragmentShaderObjectPhongPerVertex, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObjectPhongPerVertex);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//shader Program
	shaderProgramObjectPhongPerVertex = gl.createProgram();
	gl.attachShader(shaderProgramObjectPhongPerVertex, vertexShaderObjectPhongPerVertex);
	gl.attachShader(shaderProgramObjectPhongPerVertex, fragmentShaderObjectPhongPerVertex);

	//pre-link binding of shader program object with vertex shader attributes
	
	gl.bindAttribLocation(shaderProgramObjectPhongPerVertex, WebGLMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObjectPhongPerVertex, WebGLMacros.VDG_ATTRIBUTE_NORMAL, "vNormal");
	gl.linkProgram(shaderProgramObjectPhongPerVertex);
	if(!gl.getProgramParameter(shaderProgramObjectPhongPerVertex, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObjectPhongPerVertex);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	modelMatrixUniform = gl.getUniformLocation(shaderProgramObjectPhongPerVertex, "u_model_matrix");
	viewMatrixUniform = gl.getUniformLocation(shaderProgramObjectPhongPerVertex, "u_view_matrix");
	projectionMatrixUniform = gl.getUniformLocation(shaderProgramObjectPhongPerVertex, "u_projection_matrix");
	//LKeyPressedUniform = gl.getUniformLocation(shaderProgramObjectPhongPerVertex, "u_LKeyPressed");
	la1Uniform = gl.getUniformLocation(shaderProgramObjectPhongPerVertex, "u_La1");
	ld1Uniform = gl.getUniformLocation(shaderProgramObjectPhongPerVertex, "u_Ld1");
	ls1Uniform = gl.getUniformLocation(shaderProgramObjectPhongPerVertex, "u_Ls1");
	light1PositionUniform = gl.getUniformLocation(shaderProgramObjectPhongPerVertex, "u_light1_position");
	ka1Uniform = gl.getUniformLocation(shaderProgramObjectPhongPerVertex, "u_Ka1");
	kd1Uniform = gl.getUniformLocation(shaderProgramObjectPhongPerVertex, "u_Kd1");
	ks1Uniform = gl.getUniformLocation(shaderProgramObjectPhongPerVertex, "u_Ks1");
	material1ShininessUniform = gl.getUniformLocation(shaderProgramObjectPhongPerVertex, "u_material1_shininess");

	la2Uniform = gl.getUniformLocation(shaderProgramObjectPhongPerVertex, "u_La2");
	ld2Uniform = gl.getUniformLocation(shaderProgramObjectPhongPerVertex, "u_Ld2");
	ls2Uniform = gl.getUniformLocation(shaderProgramObjectPhongPerVertex, "u_Ls2");
	light2PositionUniform = gl.getUniformLocation(shaderProgramObjectPhongPerVertex, "u_light2_position");
	ka2Uniform = gl.getUniformLocation(shaderProgramObjectPhongPerVertex, "u_Ka2");
	kd2Uniform = gl.getUniformLocation(shaderProgramObjectPhongPerVertex, "u_Kd2");
	ks2Uniform = gl.getUniformLocation(shaderProgramObjectPhongPerVertex, "u_Ks2");
	material2ShininessUniform = gl.getUniformLocation(shaderProgramObjectPhongPerVertex, "u_material2_shininess");

	la3Uniform = gl.getUniformLocation(shaderProgramObjectPhongPerVertex, "u_La3");
	ld3Uniform = gl.getUniformLocation(shaderProgramObjectPhongPerVertex, "u_Ld3");
	ls3Uniform = gl.getUniformLocation(shaderProgramObjectPhongPerVertex, "u_Ls3");
	light3PositionUniform = gl.getUniformLocation(shaderProgramObjectPhongPerVertex, "u_light3_position");
	ka3Uniform = gl.getUniformLocation(shaderProgramObjectPhongPerVertex, "u_Ka3");
	kd3Uniform = gl.getUniformLocation(shaderProgramObjectPhongPerVertex, "u_Kd3");
	ks3Uniform = gl.getUniformLocation(shaderProgramObjectPhongPerVertex, "u_Ks3");
	material3ShininessUniform = gl.getUniformLocation(shaderProgramObjectPhongPerVertex, "u_material3_shininess");


	//Per Fragment

	 var vertexShaderSourceCodePerFragment=
    "#version 300 es"+
    "\n"+
    "in vec4 vPosition;"+
    "in vec3 vNormal;"+
    "uniform mat4 u_model_matrix;"+
    "uniform mat4 u_view_matrix;"+
    "uniform mat4 u_projection_matrix;"+
    "uniform vec4 u_light1_position;"+
    "uniform vec4 u_light2_position;"+
    "uniform vec4 u_light3_position;"+
    "out vec3 transformed_normals;"+
    "out vec3 light1_direction;"+
    "out vec3 light2_direction;"+
    "out vec3 light3_direction;"+
    "out vec3 viewer_vector;"+
    "void main(void)"+
    "{"+
    "vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;"+
    "transformed_normals=normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);"+
    "light1_direction = normalize(vec3(u_light1_position) - eye_coordinates.xyz);"+
    "light2_direction = normalize(vec3(u_light2_position) - eye_coordinates.xyz);"+
    "light3_direction = normalize(vec3(u_light3_position) - eye_coordinates.xyz);"+
    "viewer_vector = -eye_coordinates.xyz;"+
    "gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"+
    "}"

	vertexShaderObjectPhongPerFragment = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObjectPhongPerFragment, vertexShaderSourceCodePerFragment);
	gl.compileShader(vertexShaderObjectPhongPerFragment);
	if(gl.getShaderParameter(vertexShaderObjectPhongPerFragment, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObjectPhongPerFragment);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}	

	var fragmentShaderSourceCodePerFragment = 
	"#version 300 es"+
	"\n"+
	"precision highp float;"+
	"in vec3 transformed_normals;"+
	"in vec3 light1_direction;"+
	"in vec3 light2_direction;"+
	"in vec3 light3_direction;"+
	"in vec3 viewer_vector;"+
	"out vec4 FragColor;"+
	"uniform vec3 u_La1;"+
    "uniform vec3 u_Ld1;"+
    "uniform vec3 u_Ls1;"+
    "uniform vec3 u_Ka1;"+
    "uniform vec3 u_Kd1;"+
    "uniform vec3 u_Ks1;"+
    "uniform float u_material1_shininess;"+
    "uniform vec3 u_La2;"+
    "uniform vec3 u_Ld2;"+
    "uniform vec3 u_Ls2;"+
    "uniform vec3 u_Ka2;"+
    "uniform vec3 u_Kd2;"+
    "uniform vec3 u_Ks2;"+
    "uniform float u_material2_shininess;"+
    "uniform vec3 u_La3;"+
    "uniform vec3 u_Ld3;"+
    "uniform vec3 u_Ls3;"+
    "uniform vec3 u_Ka3;"+
    "uniform vec3 u_Kd3;"+
    "uniform vec3 u_Ks3;"+
    "uniform float u_material3_shininess;"+
    "uniform vec4 u_light1_position;"+
    "uniform vec4 u_light2_position;"+
    "uniform vec4 u_light3_position;"+
  
	"void main(void)"+
	"{"+
	"vec3 phong_ads_color1;"+
	"vec3 phong_ads_color2;"+
	"vec3 phong_ads_color3;"+
	
	"vec3 normalized_transformed_normals = normalize(transformed_normals);"+
	
	"vec3 normalized_light_direction1 = normalize(light1_direction);"+
	"vec3 normalized_light_direction2 = normalize(light2_direction);"+
	"vec3 normalized_light_direction3 = normalize(light3_direction);"+
	
	"vec3 normalized_viewer_vector = normalize(viewer_vector);"+
	
	"vec3 ambient1 = u_La1 * u_Ka1;"+
	"vec3 ambient2 = u_La2 * u_Ka2;"+
	"vec3 ambient3 = u_La3 * u_Ka3;"+

	"float tn_dot_ld1 = max(dot(normalized_transformed_normals, normalized_light_direction1), 0.0);"+
	"float tn_dot_ld2 = max(dot(normalized_transformed_normals, normalized_light_direction2), 0.0);"+
	"float tn_dot_ld3 = max(dot(normalized_transformed_normals, normalized_light_direction3), 0.0);"+
	
	"vec3 diffuse1 = u_Ld1 * u_Kd1 * tn_dot_ld1;"+
	"vec3 diffuse2 = u_Ld2 * u_Kd2 * tn_dot_ld2;"+
	"vec3 diffuse3 = u_Ld3 * u_Kd3 * tn_dot_ld3;"+
	
	"vec3 reflection_vector1 = reflect(-normalized_light_direction1, normalized_transformed_normals);"+
	"vec3 reflection_vector2 = reflect(-normalized_light_direction2, normalized_transformed_normals);"+
	"vec3 reflection_vector3 = reflect(-normalized_light_direction3, normalized_transformed_normals);"+

	"vec3 specular1 = u_Ls1 * u_Ks1 * pow(max(dot(reflection_vector1, normalized_viewer_vector), 0.0), u_material1_shininess);"+
	"vec3 specular2 = u_Ls2 * u_Ks2 * pow(max(dot(reflection_vector2, normalized_viewer_vector), 0.0), u_material2_shininess);"+
	"vec3 specular3 = u_Ls3 * u_Ks3 * pow(max(dot(reflection_vector3, normalized_viewer_vector), 0.0), u_material3_shininess);"+

	"phong_ads_color1 = ambient1 + diffuse1 + specular1;"+
	"phong_ads_color2 = ambient2 + diffuse2 + specular2;"+
	"phong_ads_color3 = ambient3 + diffuse3 + specular3;"+
	
	"FragColor = vec4(phong_ads_color1, 1.0) + vec4(phong_ads_color2, 1.0) + vec4(phong_ads_color3, 1.0);"+
	"}"

	fragmentShaderObjectPhongPerFragment = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObjectPhongPerFragment, fragmentShaderSourceCodePerFragment);
	gl.compileShader(fragmentShaderObjectPhongPerFragment);
	if(gl.getShaderParameter(fragmentShaderObjectPhongPerFragment, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObjectPhongPerFragment);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//shader Program
	shaderProgramObjectPhongPerFragment = gl.createProgram();
	gl.attachShader(shaderProgramObjectPhongPerFragment, vertexShaderObjectPhongPerFragment);
	gl.attachShader(shaderProgramObjectPhongPerFragment, fragmentShaderObjectPhongPerFragment);

	//pre-link binding of shader program object with vertex shader attributes
	
	gl.bindAttribLocation(shaderProgramObjectPhongPerFragment, WebGLMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObjectPhongPerFragment, WebGLMacros.VDG_ATTRIBUTE_NORMAL, "vNormal");
	gl.linkProgram(shaderProgramObjectPhongPerFragment);
	if(!gl.getProgramParameter(shaderProgramObjectPhongPerFragment, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObjectPhongPerFragment);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	
	//sphere = new Mesh();
	//makeSphere(sphere, 2.0, 30, 30);

	

	modelMatrixUniformf = gl.getUniformLocation(shaderProgramObjectPhongPerFragment, "u_model_matrix");
	viewMatrixUniformf = gl.getUniformLocation(shaderProgramObjectPhongPerFragment, "u_view_matrix");
	projectionMatrixUniformf = gl.getUniformLocation(shaderProgramObjectPhongPerFragment, "u_projection_matrix");
	//LKeyPressedUniformf = gl.getUniformLocation(shaderProgramObjectPhongPerFragment, "u_LKeyPressed");
	la1Uniformf = gl.getUniformLocation(shaderProgramObjectPhongPerFragment, "u_La1");
	ld1Uniformf = gl.getUniformLocation(shaderProgramObjectPhongPerFragment, "u_Ld1");
	ls1Uniformf = gl.getUniformLocation(shaderProgramObjectPhongPerFragment, "u_Ls1");
	light1PositionUniformf = gl.getUniformLocation(shaderProgramObjectPhongPerFragment, "u_light1_position");
	ka1Uniformf = gl.getUniformLocation(shaderProgramObjectPhongPerFragment, "u_Ka1");
	kd1Uniformf = gl.getUniformLocation(shaderProgramObjectPhongPerFragment, "u_Kd1");
	ks1Uniformf = gl.getUniformLocation(shaderProgramObjectPhongPerFragment, "u_Ks1");
	material1ShininessUniformf = gl.getUniformLocation(shaderProgramObjectPhongPerFragment, "u_material1_shininess");

	la2Uniformf = gl.getUniformLocation(shaderProgramObjectPhongPerFragment, "u_La2");
	ld2Uniformf = gl.getUniformLocation(shaderProgramObjectPhongPerFragment, "u_Ld2");
	ls2Uniformf = gl.getUniformLocation(shaderProgramObjectPhongPerFragment, "u_Ls2");
	light2PositionUniformf = gl.getUniformLocation(shaderProgramObjectPhongPerFragment, "u_light2_position");
	ka2Uniformf = gl.getUniformLocation(shaderProgramObjectPhongPerFragment, "u_Ka2");
	kd2Uniformf = gl.getUniformLocation(shaderProgramObjectPhongPerFragment, "u_Kd2");
	ks2Uniformf = gl.getUniformLocation(shaderProgramObjectPhongPerFragment, "u_Ks2");
	material2ShininessUniformf = gl.getUniformLocation(shaderProgramObjectPhongPerFragment, "u_material2_shininess");

	la3Uniformf = gl.getUniformLocation(shaderProgramObjectPhongPerFragment, "u_La3");
	ld3niformf = gl.getUniformLocation(shaderProgramObjectPhongPerFragment, "u_Ld3");
	ls3Uniformf = gl.getUniformLocation(shaderProgramObjectPhongPerFragment, "u_Ls3");
	light3PositionUniformf = gl.getUniformLocation(shaderProgramObjectPhongPerFragment, "u_light3_position");
	ka3Uniformf = gl.getUniformLocation(shaderProgramObjectPhongPerFragment, "u_Ka3");
	kd3Uniformf = gl.getUniformLocation(shaderProgramObjectPhongPerFragment, "u_Kd3");
	ks3Uniformf = gl.getUniformLocation(shaderProgramObjectPhongPerFragment, "u_Ks3");
	material3ShininessUniformf = gl.getUniformLocation(shaderProgramObjectPhongPerFragment, "u_material3_shininess");

	
	sphere = new Mesh();
	makeSphere(sphere, 2.0, 30, 30);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	gl.enable(gl.CULL_FACE);
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	if(bFullscreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}

	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	//set viewport 
	gl.viewport(0, 0, canvas.width, canvas.height);

	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
}

function draw()
{
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	
	var light1Position = [0.0, 400 * degToRad(Math.sin(angleRed)), 400 * degToRad(Math.cos(angleRed)), 1.0];
	var light2Position = [400 * degToRad(Math.cos(angleGreen)), 0.0, 400 * degToRad(Math.sin(angleGreen)), 1.0];
	var light3Position = [400 * degToRad(Math.cos(angleBlue)), 400 * degToRad(Math.sin(angleBlue)), 0.0, 1.0];

	if(perVertex == true)
	{
		gl.useProgram(shaderProgramObjectPhongPerVertex);


		gl.uniform3fv(la1Uniform, light1_ambient);
		gl.uniform3fv(ld1Uniform, light1_diffuse);
		gl.uniform3fv(ls1Uniform, light1_specular);
		gl.uniform4fv(light1PositionUniform, light1Position);
		gl.uniform3fv(ka1Uniform, material_ambient);
		gl.uniform3fv(kd1Uniform, material_diffuse);
		gl.uniform3fv(ks1Uniform, material_specular);
		gl.uniform1f(material1ShininessUniform, material_shininess);

		gl.uniform3fv(la2Uniform, light2_ambient);
		gl.uniform3fv(ld2Uniform, light2_diffuse);
		gl.uniform3fv(ls2Uniform, light2_specular);
		gl.uniform4fv(light2PositionUniform, light2Position);
		gl.uniform3fv(ka2Uniform, material_ambient);
		gl.uniform3fv(kd2Uniform, material_diffuse);
		gl.uniform3fv(ks2Uniform, material_specular);
		gl.uniform1f(material2ShininessUniform, material_shininess);

		gl.uniform3fv(la3Uniform, light3_ambient);
		gl.uniform3fv(ld3Uniform, light3_diffuse);
		gl.uniform3fv(ls3Uniform, light3_specular);
		gl.uniform4fv(light3PositionUniform, light3Position);
		gl.uniform3fv(ka3Uniform, material_ambient);
		gl.uniform3fv(kd3Uniform, material_diffuse);
		gl.uniform3fv(ks3Uniform, material_specular);
		gl.uniform1f(material3ShininessUniform, material_shininess);
	

	

	var modelMatrix = mat4.create();
	var viewMatrix = mat4.create();

	mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, -6.0]);
	gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);
	sphere.draw();
	gl.useProgram(null);
	update();
	requestAnimationFrame(draw, canvas);
	}

	else 
	{
		gl.useProgram(shaderProgramObjectPhongPerFragment);
		
		gl.uniform3fv(la1Uniformf, light1_ambient);
		gl.uniform3fv(ld1Uniformf, light1_diffuse);
		gl.uniform3fv(ls1Uniformf, light1_specular);
		gl.uniform4fv(light1PositionUniformf, light1Position);
		gl.uniform3fv(ka1Uniformf, material_ambient);
		gl.uniform3fv(kd1Uniformf, material_diffuse);
		gl.uniform3fv(ks1Uniformf, material_specular);
		gl.uniform1f(material1ShininessUniformf, material_shininess);

		gl.uniform3fv(la2Uniformf, light2_ambient);
		gl.uniform3fv(ld2Uniformf, light2_diffuse);
		gl.uniform3fv(ls2Uniformf, light2_specular);
		gl.uniform4fv(light2PositionUniformf, light2Position);
		gl.uniform3fv(ka2Uniformf, material_ambient);
		gl.uniform3fv(kd2Uniformf, material_diffuse);
		gl.uniform3fv(ks2Uniformf, material_specular);
		gl.uniform1f(material2ShininessUniformf, material_shininess);

		gl.uniform3fv(la3Uniformf, light3_ambient);
		gl.uniform3fv(ld3Uniformf, light3_diffuse);
		gl.uniform3fv(ls3Uniformf, light3_specular);
		gl.uniform4fv(light3PositionUniformf, light3Position);
		gl.uniform3fv(ka3Uniformf, material_ambient);
		gl.uniform3fv(kd3Uniformf, material_diffuse);
		gl.uniform3fv(ks3Uniformf, material_specular);
		gl.uniform1f(material3ShininessUniformf, material_shininess);
	

	var modelMatrix = mat4.create();
	var viewMatrix = mat4.create();

	mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, -6.0]);
	gl.uniformMatrix4fv(modelMatrixUniformf, false, modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniformf, false, viewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniformf, false, perspectiveProjectionMatrix);
	sphere.draw();
	gl.useProgram(null);
	update();
	requestAnimationFrame(draw, canvas);
	}
	
}

function uninitialize()
{
	if(sphere)
	{
		sphere.deallocate();
		sphere = null;
	}

	if(shaderProgramObjectPhongPerFragment)
	{
		if(fragmentShaderObjectPhongPerFragment)
		{
			gl.detachShader(shaderProgramObjectPhongPerFragment, fragmentShaderObjectPhongPerFragment);
			gl.deleteShader(fragmentShaderObjectPhongPerFragment);
			fragmentShaderObjectPhongPerFragment = null;
		}

		if(vertexShaderObjectPhongPerFragment)
		{
			gl.detachShader(shaderProgramObjectPhongPerFragment, vertexShaderObjectPhongPerFragment);
			gl.deleteShader(vertexShaderObjectPhongPerFragment);
			vertexShaderObjectPhongPerFragment = null;
		}
		gl.deleteProgram(shaderProgramObjectPhongPerFragment);
		shaderProgramObjectPhongPerFragment = null;
	}


	if(shaderProgramObjectPhongPerVertex)
	{
		if(fragmentShaderObjectPhongPerVertex)
		{
			gl.detachShader(shaderProgramObjectPhongPerVertex, fragmentShaderObjectPhongPerVertex);
			gl.deleteShader(fragmentShaderObjectPhongPerVertex);
			fragmentShaderObjectPhongPerVertex = null;
		}

		if(vertexShaderObjectPhongPerVertex)
		{
			gl.detachShader(shaderProgramObjectPhongPerVertex, vertexShaderObjectPhongPerVertex);
			gl.deleteShader(vertexShaderObjectPhongPerVertex);
			vertexShaderObjectPhongPerVertex = null;
		}
		gl.deleteProgram(shaderProgramObjectPhongPerVertex);
		shaderProgramObjectPhongPerVertex = null;
	}
}

function keyDown(event)
{
	switch(event.key)
	{
		case 'Q':
		case 'q':
		toggleFullScreen();
		break;
	
		case 'F':
		case 'f':
			perFragment = true;
			perVertex = false;
			break;

		case 'V':
		case 'v':
			perVertex = true;
			perFragment = false;
			break;
	}

	switch(event.keyCode)
	{
		case 27:
		uninitialize();
		window.close();
		break;

		/*case 76:
			if(bLKeyPressed == false)
				bLKeyPressed = true;
			else 
				bLKeyPressed = false;
			break;	*/
	}
}

function mouseDown()
{

}

function degToRad(degrees)
{
	return(degrees * Math.PI / 180.0);
}

function update()
{
	angleRed = angleRed + 0.05;
	angleGreen = angleGreen + 0.05;
	angleBlue = angleBlue + 0.05;

	if(angleRed >= 360.0)
		angleRed = angleRed - 360.0;

	if(angleGreen >= 360.0)
		angleGreen = angleGreen - 360.0;

	if(angleBlue >= 360.0)
		angleBlue = angleBlue - 360.0;

}