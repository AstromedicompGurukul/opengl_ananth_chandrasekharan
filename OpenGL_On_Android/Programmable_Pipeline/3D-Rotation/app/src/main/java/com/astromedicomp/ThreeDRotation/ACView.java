package com.astromedicomp.ThreeDRotation;

import android.content.Context; // for drawing context related

import android.opengl.GLSurfaceView; // for OpenGL Surface View and all related

import javax.microedition.khronos.opengles.GL10; // for OpenGLES 1.0 needed as param type GL10

import javax.microedition.khronos.egl.EGLConfig; // for EGLConfig needed as param type EGLConfig

import android.opengl.GLES32; // for OpenGLES 3.2

import android.view.MotionEvent; // for "MotionEvent"

import android.view.GestureDetector; // for GestureDetector

import android.view.GestureDetector.OnGestureListener; // OnGestureListener

import android.view.GestureDetector.OnDoubleTapListener; // for OnDoubleTapListener

import java.nio.ByteBuffer;

import java.nio.ByteOrder;

import java.nio.FloatBuffer;

import android.opengl.Matrix;


public class ACView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	private final Context context;
        private GestureDetector gestureDetector;

	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;

	/*
	private int[] vao=new int[1];
	private int[] vbo=new int[1];
	*/
	
	/*
	GLuint gVbo_Position;
	GLuint gVbo_Color;
	*/

	/*
	private int[] vao_Triangle=new int[1];
	private int[] Vbo_Triangle_Position=new int[1];
	private int[] Vbo_Triangle_Color=new int[1];
	
	private int[] vao_Square=new int[1];
	private int[] Vbo_Square_Position=new int[1];
	private int[] Vbo_Square_Color=new int[1];
	*/
	
	private int[] Vao_Pyramid=new int[1];
	private int[] Vbo_Pyramid_Position=new int[1];
	private int[] Vbo_Pyramid_Color=new int[1];
	float AnglePyramid = 0.0f;

	private int[] Vao_Cube=new int[1];
	private int[] Vbo_Cube_Position=new int[1];
	private int[] Vbo_Cube_Color=new int[1];
	float AngleCube = 0.0f;
	
    private int mvpUniform;

	private float perspectiveProjectionMatrix[]=new float[16];

    ACView(Context drawingContext)
    {
       super(drawingContext);
        
        context=drawingContext;
        
        setEGLContextClientVersion(3);
        setRenderer(this);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
        
        gestureDetector = new GestureDetector(context, this, null, false); 
        gestureDetector.setOnDoubleTapListener(this);
    }
	
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        // OpenGL-ES version check
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("AC: OpenGL ES Version = "+version);

	String glslVersion=gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
	System.out.println("AC: GLSL Version ="+version);

        
        initialize(gl);
    }
	
	public void onSurfaceChanged(GL10 unused, int width, int height)
    {
        resize(width, height);
    }
	
	public void onDrawFrame(GL10 unused)
    {
        draw();
    }
    
    // Handling 'onTouchEvent' Is The Most IMPORTANT,
    // Because It Triggers All Gesture And Tap Events
    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        // code
        int eventaction = event.getAction();
        if(!gestureDetector.onTouchEvent(event))
            super.onTouchEvent(event);
        return(true);
    }
    
    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTap(MotionEvent e)
    {
        System.out.println("AC: "+"Double Tap");
        return(true);
    }
    
    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTapEvent(MotionEvent e)
    {
        // Do Not Write Any code Here Because Already Written 'onDoubleTap'
        return(true);
    }
    
    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
        System.out.println("AC: "+"Single Tap");
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onDown(MotionEvent e)
    {
        // Do Not Write Any code Here Because Already Written 'onSingleTapConfirmed'
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onLongPress(MotionEvent e)
    {
        System.out.println("AC: "+"Long Press");
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
    {
        System.out.println("AC: "+"Scroll");
	uninitialize();
        System.exit(0);
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onShowPress(MotionEvent e)
    {
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
        return(true);
    }
	
	
	private void update()
	{
		AnglePyramid = AnglePyramid + 0.2f;
		if (AnglePyramid >= 360.0f)
			AnglePyramid = AnglePyramid - 360.0f;

		AngleCube = AngleCube + 0.2f;
		if (AngleCube >= 360.0f)
			AngleCube = AngleCube - 360.0f;

	}

	
    private void initialize(GL10 gl)
    {
        // ***********************************************
        // Vertex Shader
        // ***********************************************
        // create shader
        vertexShaderObject=GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
        
		
		// vertex shader source code
		/*
        final String vertexShaderSourceCode =String.format
        (
         "#version 320 es"+
         "\n"+
         "in vec4 vPosition;"+
         "uniform mat4 u_mvp_matrix;"+
         "void main(void)"+
         "{"+
         "gl_Position = u_mvp_matrix * vPosition;"+
         "}"
        );
		*/
		
		/*
		"#version 440 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;"\
		"out vec4 out_color;" \
		"uniform mat4 u_mvp_matrix;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvp_matrix * vPosition;" \
		"out_color = vColor;" \
		"}";
		*/
		
		final String vertexShaderSourceCode =String.format
        (
         "#version 320 es"+
         "\n" +
		"in vec4 vPosition;" +
		"in vec4 vColor;"+
		"out vec4 out_color;" +
		"uniform mat4 u_mvp_matrix;" +
		"void main(void)" +
		"{" +
		"gl_Position = u_mvp_matrix * vPosition;" +
		"out_color = vColor;" +
		"}"
        );
        
        // provide source code to shader
        GLES32.glShaderSource(vertexShaderObject,vertexShaderSourceCode);
        
        // compile shader & check for errors
        GLES32.glCompileShader(vertexShaderObject);
        int[] iShaderCompiledStatus = new int[1];
        int[] iInfoLogLength = new int[1];
        String szInfoLog=null;
        GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
        if (iShaderCompiledStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            if (iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
                System.out.println("VDG: Vertex Shader Compilation Log = "+szInfoLog);
                uninitialize();
                System.exit(0);
           }
        }

        // ***********************************************
        // Fragment Shader
        // ***********************************************
        // create shader
        fragmentShaderObject=GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
        
        // fragment shader source code
        
		/*
		final String fragmentShaderSourceCode =String.format
        (
         "#version 320 es"+
         "\n"+
         "precision highp float;"+
         "out vec4 FragColor;"+
         "void main(void)"+
         "{"+
         "FragColor = vec4(1.0, 1.0, 1.0, 1.0);"+
         "}"
        );
		*/
		
		/*
		"#version 440 core" \
		"\n" \
		"in vec4 out_color;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor = out_color;" \
		"}";
		*/
		
		final String fragmentShaderSourceCode =String.format
        (
         "#version 320 es"+
         "\n"+
         "precision highp float;"+
         "in vec4 out_color;" +
		"out vec4 FragColor;"+
		"void main(void)"+
		"{" +
		"FragColor = out_color;" +
		"}"
        );

        
        // provide source code to shader
        GLES32.glShaderSource(fragmentShaderObject,fragmentShaderSourceCode);
        
        // compile shader and check for errors
        GLES32.glCompileShader(fragmentShaderObject);
        iShaderCompiledStatus[0] = 0; // re-initialize
        iInfoLogLength[0] = 0; // re-initialize
        szInfoLog=null; // re-initialize
        GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
        if (iShaderCompiledStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            if (iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
                System.out.println("VDG: Fragment Shader Compilation Log = "+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }
        
        // create shader program
        shaderProgramObject=GLES32.glCreateProgram();
        
        // attach vertex shader to shader program
        GLES32.glAttachShader(shaderProgramObject,vertexShaderObject);
        
        // attach fragment shader to shader program
        GLES32.glAttachShader(shaderProgramObject,fragmentShaderObject);
        
        // pre-link binding of shader program object with vertex shader attributes
        GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.AC_ATTRIBUTE_VERTEX,"vPosition");
        
        // link the two shaders together to shader program object
        GLES32.glLinkProgram(shaderProgramObject);
        int[] iShaderProgramLinkStatus = new int[1];
        iInfoLogLength[0] = 0; // re-initialize
        szInfoLog=null; // re-initialize
        GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iShaderProgramLinkStatus, 0);
        if (iShaderProgramLinkStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            if (iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
                System.out.println("VDG: Shader Program Link Log = "+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        // get MVP uniform location
        mvpUniform=GLES32.glGetUniformLocation(shaderProgramObject,"u_mvp_matrix");
        
        
		/*
		// *** vertices, colors, shader attribs, vbo, vao initializations ***
        final float triangleVertices[]= new float[]
        {
            0.0f, 1.0f,0.0f, // appex
            -1.0f,-1.0f,0.0f, // left-bottom
            1.0f,-1.0f,0.0f // right-bottom
        };
		
		final float triangleColors[] = new float[]
		{
		1.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 1.0f
		};
		
		final float squareVertices[] = {
		1.0f, 1.0f, 0.0f,
		-1.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 0.0f,
		1.0f, -1.0f, 0.0f
	};
	*/
	
	final float PyramidVertices[] =
	{
		0, 1, 0,
		-1, -1, 1,
		1, -1, 1,

		0, 1, 0,
		1, -1, 1,
		1, -1, -1,

		0, 1, 0,
		1, -1, -1,
		-1, -1, -1,

		0, 1, 0,
		-1, -1, -1,
		-1, -1, 1
	};

	final float PyramidColors[] =
	{
		1, 0, 0,
		0, 1, 0,
		0, 0, 1,

		1, 0, 0,
		0, 0, 1,
		0, 1, 0,

		1, 0, 0,
		0, 1, 0,
		0, 0, 1,

		1, 0, 0,
		0, 0, 1,
		0, 1, 0,
	};

	final float CubeVertices[] =
	{
		1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,

		1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,

		1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,

		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, -1.0f,

		-1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f,

		1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,
	};

	final float CubeColors[] =
	{
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,

		1.0f, 0.5f, 0.0f,
		1.0f, 0.5f, 0.0f,
		1.0f, 0.5f, 0.0f,
		1.0f, 0.5f, 0.0f,

		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,

		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,

		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,

		1.0f, 0.0f, 1.0f,
		1.0f, 0.0f, 1.0f,
		1.0f, 0.0f, 1.0f,
		1.0f, 0.0f, 1.0f,
	};

		GLES32.glGenVertexArrays(1,Vao_Pyramid,0);
        GLES32.glBindVertexArray(Vao_Pyramid[0]);
		
		//Vbo for Pyramid vertices
		GLES32.glGenBuffers(1,Vbo_Pyramid_Position,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,Vbo_Pyramid_Position[0]);
		
		ByteBuffer PyramidbyteBuffer=ByteBuffer.allocateDirect(PyramidVertices.length * 4);
        PyramidbyteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer PyramidVerticesBuffer=PyramidbyteBuffer.asFloatBuffer();
        PyramidVerticesBuffer.put(PyramidVertices);
        PyramidVerticesBuffer.position(0);
        
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            PyramidVertices.length * 4,
                            PyramidVerticesBuffer,
                            GLES32.GL_STATIC_DRAW);
        
		GLES32.glVertexAttribPointer(GLESMacros.AC_ATTRIBUTE_VERTEX,
                                     3,
                                     GLES32.GL_FLOAT,
                                     false,0,0);
        
		GLES32.glEnableVertexAttribArray(GLESMacros.AC_ATTRIBUTE_VERTEX);        
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		//Vbo for Pyramid vertices
		
		//Vbo for Pyramid Color
		GLES32.glGenBuffers(1,Vbo_Pyramid_Color,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,Vbo_Pyramid_Color[0]);
        
		ByteBuffer PyramidBufferColor=ByteBuffer.allocateDirect(PyramidColors.length * 4);
        PyramidBufferColor.order(ByteOrder.nativeOrder());
        FloatBuffer ColorBufferOfPyramid=PyramidBufferColor.asFloatBuffer();
        ColorBufferOfPyramid.put(PyramidColors);
        ColorBufferOfPyramid.position(0);
        
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            PyramidColors.length * 4,
                            ColorBufferOfPyramid,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glVertexAttribPointer(GLESMacros.AC_ATTRIBUTE_COLOR,
                                     3,
                                     GLES32.GL_FLOAT,
                                     false,0,0);
        
        GLES32.glEnableVertexAttribArray(GLESMacros.AC_ATTRIBUTE_COLOR);
		
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		//Vbo for Pyramid Color ends
	
		//Vbo for Cube
		GLES32.glGenVertexArrays(1,Vao_Cube,0);
        GLES32.glBindVertexArray(Vao_Cube[0]);

		//vbo for Cube position
        GLES32.glGenBuffers(1,Vbo_Cube_Position,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,Vbo_Cube_Position[0]);
        
        ByteBuffer CubebyteBuffer=ByteBuffer.allocateDirect(CubeVertices.length * 4);
        CubebyteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer CubePositionBuffer=CubebyteBuffer.asFloatBuffer();
        CubePositionBuffer.put(CubeVertices);
        CubePositionBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            CubeVertices.length * 4,
                            CubePositionBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glVertexAttribPointer(GLESMacros.AC_ATTRIBUTE_VERTEX,
                                     3,
                                     GLES32.GL_FLOAT,
                                     false,0,0);
        
        GLES32.glEnableVertexAttribArray(GLESMacros.AC_ATTRIBUTE_VERTEX);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		//end of vbo of position
		
		//Vbo for Cube Color
		GLES32.glGenBuffers(1,Vbo_Cube_Color,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,Vbo_Cube_Color[0]);
        
		ByteBuffer CubeBufferColor=ByteBuffer.allocateDirect(CubeColors.length * 4);
        CubeBufferColor.order(ByteOrder.nativeOrder());
        FloatBuffer CubeFloatBuffer=CubeBufferColor.asFloatBuffer();
        CubeFloatBuffer.put(CubeColors);
        CubeFloatBuffer.position(0);
        
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            CubeColors.length * 4,
                            CubeFloatBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glVertexAttribPointer(GLESMacros.AC_ATTRIBUTE_COLOR,
                                     3,
                                     GLES32.GL_FLOAT,
                                     false,0,0);
        
        GLES32.glEnableVertexAttribArray(GLESMacros.AC_ATTRIBUTE_COLOR);
		
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		//Vbo for Cube Color ends
	

        GLES32.glBindVertexArray(0);
		//Vbo for Cube
		
        // enable depth testing
        GLES32.glEnable(GLES32.GL_DEPTH_TEST);
        // depth test to do
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);
        // We will always cull back faces for better performance
        GLES32.glEnable(GLES32.GL_CULL_FACE);
        
        // Set the background color
        GLES32.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        
        // set projectionMatrix to identity matrix
        Matrix.setIdentityM(perspectiveProjectionMatrix,0);
    }
    
    
    private void resize(int width, int height)
    {
        GLES32.glViewport(0, 0, width, height);
    
		/*
        if (width <= height)
            Matrix.orthoM(orthographicProjectionMatrix,0,-100.0f, 100.0f, (-100.0f * (height / width)), (100.0f * (height / width)), -100.0f, 100.0f);
        else
            Matrix.orthoM(orthographicProjectionMatrix,0,(-100.0f * (width / height)), (100.0f * (width / height)), -100.0f, 100.0f, -100.0f, 100.0f);
		*/
		Matrix.perspectiveM(perspectiveProjectionMatrix,0,45.0f,(float)width/(float)height,0.1f,100.0f);
	
	}
	
	
    public void draw()
    {
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
        
        GLES32.glUseProgram(shaderProgramObject);

        float modelViewMatrix[]=new float[16];
        float modelViewProjectionMatrix[]=new float[16];
        
        
		//For Triangle
		Matrix.setIdentityM(modelViewMatrix,0);
        Matrix.setIdentityM(modelViewProjectionMatrix,0);

        Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		Matrix.translateM(modelViewProjectionMatrix,0,1.5f,0.0f,-6.0f);
		Matrix.rotateM(modelViewProjectionMatrix,0,AnglePyramid,0.0f, 1.0f, 0.0f);
        GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
        
        GLES32.glBindVertexArray(Vao_Pyramid[0]);
        
        GLES32.glDrawArrays(GLES32.GL_TRIANGLES,0,12); 
        
        GLES32.glBindVertexArray(0);
       
		
		//For Square
		Matrix.setIdentityM(modelViewMatrix,0);
        Matrix.setIdentityM(modelViewProjectionMatrix,0);

        Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		Matrix.translateM(modelViewProjectionMatrix,0,-1.5f,0.0f,-6.0f);
		Matrix.scaleM(modelViewProjectionMatrix,0,0.75f,0.75f,0.75f);
        Matrix.rotateM(modelViewProjectionMatrix,0,AnglePyramid,AnglePyramid, AnglePyramid,AnglePyramid);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
        
        GLES32.glBindVertexArray(Vao_Cube[0]);
        
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 4, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 8, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 12, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 16, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 20, 4);
        
        GLES32.glBindVertexArray(0);
       
	   
        GLES32.glUseProgram(0);

		update();
        requestRender(); 
		}
		
	void uninitialize()
    {
        if(Vao_Cube[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, Vao_Cube, 0);
            Vao_Cube[0]=0;
        }
		
		if(Vao_Pyramid[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, Vao_Pyramid, 0);
            Vao_Pyramid[0]=0;
        }
        
        if(Vbo_Pyramid_Position[0] != 0)
        {
            GLES32.glDeleteBuffers(1, Vbo_Pyramid_Position, 0);
            Vbo_Pyramid_Position[0]=0;
        }
		
		if(Vbo_Cube_Position[0] != 0)
        {
            GLES32.glDeleteBuffers(1, Vbo_Cube_Position, 0);
            Vbo_Cube_Position[0]=0;
        }
		
		if(Vbo_Cube_Color[0] != 0)
        {
            GLES32.glDeleteBuffers(1, Vbo_Cube_Color, 0);
            Vbo_Cube_Color[0]=0;
        }

        if(shaderProgramObject != 0)
        {
            if(vertexShaderObject != 0)
            {
                GLES32.glDetachShader(shaderProgramObject, vertexShaderObject);
                GLES32.glDeleteShader(vertexShaderObject);
                vertexShaderObject = 0;
            }
            
            if(fragmentShaderObject != 0)
            {
                GLES32.glDetachShader(shaderProgramObject, fragmentShaderObject);
                GLES32.glDeleteShader(fragmentShaderObject);
                fragmentShaderObject = 0;
            }
        }

        if(shaderProgramObject != 0)
        {
            GLES32.glDeleteProgram(shaderProgramObject);
            shaderProgramObject = 0;
        }
    }
}
