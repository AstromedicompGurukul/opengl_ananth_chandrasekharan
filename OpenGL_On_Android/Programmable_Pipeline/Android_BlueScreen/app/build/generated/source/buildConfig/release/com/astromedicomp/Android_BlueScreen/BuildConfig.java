/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.astromedicomp.Android_BlueScreen;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.astromedicomp.Android_BlueScreen";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = -1;
  public static final String VERSION_NAME = "";
}
