package com.astromedicomp.Android_BlueScreen;

import android.content.Context; // for drawing context related

import android.opengl.GLSurfaceView; // for OpenGL Surface View and all related

import javax.microedition.khronos.opengles.GL10; // for OpenGLES 1.0 needed as param type GL10

import javax.microedition.khronos.egl.EGLConfig; // for EGLConfig needed as param type EGLConfig

import android.opengl.GLES32; // for OpenGLES 3.2

import android.view.MotionEvent; // for "MotionEvent"

import android.view.GestureDetector; // for GestureDetector

import android.view.GestureDetector.OnGestureListener; // OnGestureListener

import android.view.GestureDetector.OnDoubleTapListener; // for OnDoubleTapListener

public class ACView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	private final Context context;
    private GestureDetector gestureDetector;
    
    ACView(Context drawingContext)
    {
       super(drawingContext);
        
        context=drawingContext;
        
        setEGLContextClientVersion(3);
        setRenderer(this);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
        
        gestureDetector = new GestureDetector(context, this, null, false); 
        gestureDetector.setOnDoubleTapListener(this);
    }
	
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        // OpenGL-ES version check
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("AC: "+version);
        
        initialize(gl);
    }
	
	public void onSurfaceChanged(GL10 unused, int width, int height)
    {
        resize(width, height);
    }
	
	public void onDrawFrame(GL10 unused)
    {
        draw();
    }
    
    // Handling 'onTouchEvent' Is The Most IMPORTANT,
    // Because It Triggers All Gesture And Tap Events
    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        // code
        int eventaction = event.getAction();
        if(!gestureDetector.onTouchEvent(event))
            super.onTouchEvent(event);
        return(true);
    }
    
    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTap(MotionEvent e)
    {
        System.out.println("AC: "+"Double Tap");
        return(true);
    }
    
    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTapEvent(MotionEvent e)
    {
        // Do Not Write Any code Here Because Already Written 'onDoubleTap'
        return(true);
    }
    
    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
        System.out.println("AC: "+"Single Tap");
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onDown(MotionEvent e)
    {
        // Do Not Write Any code Here Because Already Written 'onSingleTapConfirmed'
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onLongPress(MotionEvent e)
    {
        System.out.println("AC: "+"Long Press");
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
    {
        System.out.println("AC: "+"Scroll");
        System.exit(0);
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onShowPress(MotionEvent e)
    {
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
        return(true);
    }
	
	private void initialize(GL10 gl)
    {
        // Set the background frame color
        GLES32.glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
    }
    
    private void resize(int width, int height)
    {
        // Adjust the viewport based on geometry changes,
        // such as screen rotation
        GLES32.glViewport(0, 0, width, height);
    }
    public void draw()
    {
        // Draw background color
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

	requestRender();
    }
}
