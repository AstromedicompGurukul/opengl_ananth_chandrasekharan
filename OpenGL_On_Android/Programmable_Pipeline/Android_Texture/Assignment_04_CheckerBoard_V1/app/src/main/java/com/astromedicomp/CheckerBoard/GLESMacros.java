package com.astromedicomp.CheckerBoard;

public class GLESMacros
{
    // attribute index
    public static final int AC_ATTRIBUTE_VERTEX=0;
    public static final int AC_ATTRIBUTE_COLOR=1;
    public static final int AC_ATTRIBUTE_NORMAL=2;
    public static final int AC_ATTRIBUTE_TEXTURE0=3;
}
