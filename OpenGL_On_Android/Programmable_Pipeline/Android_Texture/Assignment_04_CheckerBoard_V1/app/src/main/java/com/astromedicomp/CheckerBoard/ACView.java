package com.astromedicomp.CheckerBoard;

import android.content.Context; 

import android.opengl.GLSurfaceView; 

import javax.microedition.khronos.opengles.GL10; 

import javax.microedition.khronos.egl.EGLConfig; 

import android.opengl.GLES32; 

import android.view.MotionEvent;

import android.view.GestureDetector; 

import android.view.GestureDetector.OnGestureListener; 

import android.view.GestureDetector.OnDoubleTapListener; 

import java.nio.ByteBuffer;

import java.nio.ByteOrder;

import java.nio.FloatBuffer;

import android.opengl.Matrix;

import android.graphics.BitmapFactory; 
import android.graphics.Bitmap; 
import android.opengl.GLUtils; 

public class ACView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	private final Context context;
    private GestureDetector gestureDetector;

	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;
   
	private int[] Vao_Quad=new int[1];
	private int[] Vbo_Quad_Position=new int[1];
	private int[] Vbo_Quad_Position1=new int[1];
	private int[] vbo_Quad_texture = new int[1];
   
	private int mvpUniform;
	private int texture0_sampler_uniform;
	
	private ByteBuffer byteBuffer,byteBuffer1,byteBuffer2;
	private FloatBuffer verticesBuffer,verticesBuffer1,verticesBuffer2;
	
	private int[] texture_smiley = new int[1];
	
	 float Ganesha_quadVertices[]= new float[]
        {
          0.0f, 1.0f, 0.0f,
		-2.0f, 1.0f, 0.0f,
		-2.0f, -1.0f,0.0f,
		0.0f, -1.0f, 0.0f
        };

	
	float Ganesha_quadVertices1[]= new float[]
        {
           2.4142f, 1.0f, -1.41421f,
			1.0f, 1.0f, 0.0f,
			1.0f, -1.0f, 0.0f,
			2.4142f, -1.0f, -1.41421f
        };
     
        
        float Ganesha_Texcoords[]= new float[]
        {
            1.0f, 1.0f,
			0.0f, 1.0f,
			0.0f, 0.0f,
			1.0f, 0.0f
        };
   
	private float perspectiveProjectionMatrix[]=new float[16];

    ACView(Context drawingContext)
    {
       super(drawingContext);
        
        context=drawingContext;
        
        setEGLContextClientVersion(3);
        setRenderer(this);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
        
        gestureDetector = new GestureDetector(context, this, null, false); 
        gestureDetector.setOnDoubleTapListener(this);
    }
	
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        // OpenGL-ES version check
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("AC: OpenGL ES Version = "+version);

	String glslVersion=gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
	System.out.println("AC: GLSL Version ="+version);

        
        initialize(gl);
    }
	
	public void onSurfaceChanged(GL10 unused, int width, int height)
    {
        resize(width, height);
    }
	
	public void onDrawFrame(GL10 unused)
    {
        draw();
    }
    
    // Handling 'onTouchEvent' Is The Most IMPORTANT,
    // Because It Triggers All Gesture And Tap Events
    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        // code
        int eventaction = event.getAction();
        if(!gestureDetector.onTouchEvent(event))
            super.onTouchEvent(event);
        return(true);
    }
    
    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTap(MotionEvent e)
    {
        System.out.println("AC: "+"Double Tap");
        return(true);
    }
    
    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTapEvent(MotionEvent e)
    {
        // Do Not Write Any code Here Because Already Written 'onDoubleTap'
        return(true);
    }
    
    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
        System.out.println("AC: "+"Single Tap");
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onDown(MotionEvent e)
    {
        // Do Not Write Any code Here Because Already Written 'onSingleTapConfirmed'
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onLongPress(MotionEvent e)
    {
        System.out.println("AC: "+"Long Press");
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
    {
        System.out.println("AC: "+"Scroll");
	uninitialize();
        System.exit(0);
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onShowPress(MotionEvent e)
    {
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
        return(true);
    }
	
	
	private void update()
	{
		
	}

	
	private void initialize(GL10 gl)
    {
        // ***********************************************
        // Vertex Shader
        // ***********************************************
        // create shader
        vertexShaderObject=GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
        
        // vertex shader source code
        final String vertexShaderSourceCode =String.format
        (
         "#version 320 es"+
         "\n"+
         "in vec4 vPosition;"+
         "in vec2 vTexture0_Coord;"+
         "out vec2 out_texture0_coord;"+
         "uniform mat4 u_mvp_matrix;"+
         "void main(void)"+
         "{"+
         "gl_Position = u_mvp_matrix * vPosition;"+
         "out_texture0_coord = vTexture0_Coord;"+
         "}"
        );
        
        // provide source code to shader
        GLES32.glShaderSource(vertexShaderObject,vertexShaderSourceCode);
        
        // compile shader & check for errors
        GLES32.glCompileShader(vertexShaderObject);
        int[] iShaderCompiledStatus = new int[1];
        int[] iInfoLogLength = new int[1];
        String szInfoLog=null;
        GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
        if (iShaderCompiledStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            if (iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
                System.out.println("AC: Vertex Shader Compilation Log = "+szInfoLog);
                uninitialize();
                System.exit(0);
           }
        }

        // ***********************************************
        // Fragment Shader
        // ***********************************************
        // create shader
        fragmentShaderObject=GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
        
        // fragment shader source code
        final String fragmentShaderSourceCode =String.format
        (
         "#version 320 es"+
         "\n"+
         "precision highp float;"+
         "in vec2 out_texture0_coord;"+
         "uniform highp sampler2D u_texture0_sampler;"+
         "out vec4 FragColor;"+
         "void main(void)"+
         "{"+
         "FragColor = texture(u_texture0_sampler, out_texture0_coord);"+
         "}"
        );
        
        // provide source code to shader
        GLES32.glShaderSource(fragmentShaderObject,fragmentShaderSourceCode);
        
        // compile shader and check for errors
        GLES32.glCompileShader(fragmentShaderObject);
        iShaderCompiledStatus[0] = 0; // re-initialize
        iInfoLogLength[0] = 0; // re-initialize
        szInfoLog=null; // re-initialize
        GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
        if (iShaderCompiledStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            if (iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
                System.out.println("AC: Fragment Shader Compilation Log = "+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }
        
        // create shader program
        shaderProgramObject=GLES32.glCreateProgram();
        
        // attach vertex shader to shader program
        GLES32.glAttachShader(shaderProgramObject,vertexShaderObject);
        
        // attach fragment shader to shader program
        GLES32.glAttachShader(shaderProgramObject,fragmentShaderObject);
        
        // pre-link binding of shader program object with vertex shader attributes
        GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.AC_ATTRIBUTE_VERTEX,"vPosition");
        GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.AC_ATTRIBUTE_TEXTURE0,"vTexture0_Coord");

        // link the two shaders together to shader program object
        GLES32.glLinkProgram(shaderProgramObject);
        int[] iShaderProgramLinkStatus = new int[1];
        iInfoLogLength[0] = 0; // re-initialize
        szInfoLog=null; // re-initialize
        GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iShaderProgramLinkStatus, 0);
        if (iShaderProgramLinkStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            if (iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
                System.out.println("AC: Shader Program Link Log = "+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        // get MVP uniform location
        mvpUniform=GLES32.glGetUniformLocation(shaderProgramObject,"u_mvp_matrix");
        
        // get texture sampler uniform location
        texture0_sampler_uniform=GLES32.glGetUniformLocation(shaderProgramObject,"u_texture0_sampler");
        		
		texture_smiley[0]=loadGLTexture(R.raw.smiley_512x512);

        // *** vertices, colors, shader attribs, vbo, vao initializations ***
        // pyramid vertices
		
		 // vao
        GLES32.glGenVertexArrays(1,Vao_Quad,0);
        GLES32.glBindVertexArray(Vao_Quad[0]);

		
		// position vbo
        GLES32.glGenBuffers(1,Vbo_Quad_Position,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,Vbo_Quad_Position[0]);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            4*3* 4,
                            null,
                            GLES32.GL_DYNAMIC_DRAW);
		
        GLES32.glVertexAttribPointer(GLESMacros.AC_ATTRIBUTE_VERTEX,
                                     3,
                                     GLES32.GL_FLOAT,
                                     false,0,0);
        
        GLES32.glEnableVertexAttribArray(GLESMacros.AC_ATTRIBUTE_VERTEX);
        
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		/*
		// position vbo
        GLES32.glGenBuffers(1,Vbo_Quad_Position1,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,Vbo_Quad_Position1[0]);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            4*3* 4,
                            null,
                            GLES32.GL_DYNAMIC_DRAW);
		
        GLES32.glVertexAttribPointer(GLESMacros.AC_ATTRIBUTE_VERTEX,
                                     3,
                                     GLES32.GL_FLOAT,
                                     false,0,0);
        
        GLES32.glEnableVertexAttribArray(GLESMacros.AC_ATTRIBUTE_VERTEX);
        
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		*/
		
        // texture vbo
        GLES32.glGenBuffers(1,vbo_Quad_texture,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_Quad_texture[0]);
        
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            4*2* 4,
                            null,
                            GLES32.GL_DYNAMIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AC_ATTRIBUTE_TEXTURE0,
                                     2,
                                     GLES32.GL_FLOAT,
                                     false,0,0);
        
        GLES32.glEnableVertexAttribArray(GLESMacros.AC_ATTRIBUTE_TEXTURE0);
        
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

        GLES32.glBindVertexArray(0);
       
        // enable depth testing
        GLES32.glEnable(GLES32.GL_DEPTH_TEST);
        // depth test to do
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);
        // We will always cull back faces for better performance
        GLES32.glEnable(GLES32.GL_CULL_FACE);
        
        // Set the background color
        GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // black
        
        // set projectionMatrix to identitu matrix
        Matrix.setIdentityM(perspectiveProjectionMatrix,0);
    }
	
	private int loadGLTexture(int imageFileResourceID)
    {
        // code
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;
        
        // read in the resource
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), imageFileResourceID, options);
        
        int[] texture=new int[1];
        
        // create a texture object to apply to model
        GLES32.glGenTextures(1, texture, 0);
        
        // indicate that pixel rows are tightly packed (defaults to stride of 4 which is kind of only good for RGBA or FLOAT data types)
        GLES32.glPixelStorei(GLES32.GL_UNPACK_ALIGNMENT,1);
        
        // bind with the texture
        GLES32.glBindTexture(GLES32.GL_TEXTURE_2D,texture[0]);
        
        // set up filter and wrap modes for this texture object
        GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D,GLES32.GL_TEXTURE_MAG_FILTER,GLES32.GL_LINEAR);
        GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D,GLES32.GL_TEXTURE_MIN_FILTER,GLES32.GL_LINEAR_MIPMAP_LINEAR);
        
        // load the bitmap into the bound texture
        GLUtils.texImage2D(GLES32.GL_TEXTURE_2D, 0, bitmap, 0);
        
        // generate mipmap
        GLES32.glGenerateMipmap(GLES32.GL_TEXTURE_2D);
        
        return(texture[0]);
    }
    
    
    private void resize(int width, int height)
    {
        GLES32.glViewport(0, 0, width, height);
    
		/*
        if (width <= height)
            Matrix.orthoM(orthographicProjectionMatrix,0,-100.0f, 100.0f, (-100.0f * (height / width)), (100.0f * (height / width)), -100.0f, 100.0f);
        else
            Matrix.orthoM(orthographicProjectionMatrix,0,(-100.0f * (width / height)), (100.0f * (width / height)), -100.0f, 100.0f, -100.0f, 100.0f);
		*/
		Matrix.perspectiveM(perspectiveProjectionMatrix,0,60.0f,(float)width/(float)height,1.0f,30.0f);
	
	}
	
	
    public void draw()
    {
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
        
        GLES32.glUseProgram(shaderProgramObject);

        float modelViewMatrix[]=new float[16];
        float modelViewProjectionMatrix[]=new float[16];
        
		//For Square
		Matrix.setIdentityM(modelViewMatrix,0);
        Matrix.setIdentityM(modelViewProjectionMatrix,0);

        Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		Matrix.translateM(modelViewProjectionMatrix,0,0.0f,0.0f,-3.6f);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
        
        GLES32.glBindVertexArray(Vao_Quad[0]);
		
		//---------------
		 GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_Quad_texture[0]); 
	
        byteBuffer1=ByteBuffer.allocateDirect(Ganesha_Texcoords.length * 4);
        byteBuffer1.order(ByteOrder.nativeOrder());
        verticesBuffer1=byteBuffer1.asFloatBuffer();
        verticesBuffer1.put(Ganesha_Texcoords);
        verticesBuffer1.position(0);
       
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            Ganesha_Texcoords.length * 4,
                            verticesBuffer1,
                            GLES32.GL_DYNAMIC_DRAW);
        
		
		GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
        GLES32.glBindTexture(GLES32.GL_TEXTURE_2D,texture_smiley[0]);
        GLES32.glUniform1i(texture0_sampler_uniform, 0);
		
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		//---------------
	
		//---------------
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,Vbo_Quad_Position[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(Ganesha_quadVertices.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(Ganesha_quadVertices);
        verticesBuffer.position(0);
		
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            Ganesha_quadVertices.length * 4,
                            verticesBuffer,
                            GLES32.GL_DYNAMIC_DRAW);
		
		GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
        GLES32.glBindTexture(GLES32.GL_TEXTURE_2D,texture_smiley[0]);
        GLES32.glUniform1i(texture0_sampler_uniform, 0);
		
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		//---------------
		
		
		//------------------------------
		 GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,Vbo_Quad_Position[0]);
		
		byteBuffer=ByteBuffer.allocateDirect(Ganesha_quadVertices1.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
        verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(Ganesha_quadVertices1);
        verticesBuffer.position(0);
		
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            Ganesha_quadVertices1.length * 4,
                            verticesBuffer,
                            GLES32.GL_DYNAMIC_DRAW);
		
        		
		GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
        GLES32.glBindTexture(GLES32.GL_TEXTURE_2D,texture_smiley[0]);
        GLES32.glUniform1i(texture0_sampler_uniform, 0);
		
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		//------------------------------
	   
	   GLES32.glBindVertexArray(0);
		
        GLES32.glUseProgram(0);

		update();
        requestRender(); 
		}
		
	void uninitialize()
    {
        if(Vao_Quad[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, Vao_Quad, 0);
            Vao_Quad[0]=0;
        }
		
        
        if(Vbo_Quad_Position[0] != 0)
        {
            GLES32.glDeleteBuffers(1, Vbo_Quad_Position, 0);
            Vbo_Quad_Position[0]=0;
        }
		
		if(vbo_Quad_texture[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_Quad_texture, 0);
            vbo_Quad_texture[0]=0;
        }
		

        if(shaderProgramObject != 0)
        {
            if(vertexShaderObject != 0)
            {
                GLES32.glDetachShader(shaderProgramObject, vertexShaderObject);
                GLES32.glDeleteShader(vertexShaderObject);
                vertexShaderObject = 0;
            }
            
            if(fragmentShaderObject != 0)
            {
                GLES32.glDetachShader(shaderProgramObject, fragmentShaderObject);
                GLES32.glDeleteShader(fragmentShaderObject);
                fragmentShaderObject = 0;
            }
        }

        if(shaderProgramObject != 0)
        {
            GLES32.glDeleteProgram(shaderProgramObject);
            shaderProgramObject = 0;
        }
    }
}
