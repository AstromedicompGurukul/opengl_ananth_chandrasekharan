package com.astromedicomp.CheckerBoard;

import android.content.Context;

import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

import android.opengl.GLSurfaceView;
import android.opengl.GLES32;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.Matrix;

import java.util.List;
import java.util.ArrayList;

public class ACView extends GLSurfaceView implements GLSurfaceView.Renderer,OnGestureListener,OnDoubleTapListener
{

    private final Context context;
    private final int checkImageWidth=64;
    private final int checkImageHeight=64;

    private int TexName[] = new int[1];

    private GestureDetector gestureDetector;

    private int vertexShaderObject;
    private int fragmentShaderObject;
    private int shaderProgramObject;


    private int[] vao_square = new int[1];

    private int[] vbo_square_position = new int[1];
    private int[] vbo_square_texture = new int[1];
    
    private int mvpUniform;
    private int texture0_sampler_uniform;

    private float perspectiveProjectionMatrix[] = new float[16];
    private byte checkImage[][][] = new byte[checkImageHeight][checkImageWidth][4];
    
    ACView(Context drawingContext) {

        super(drawingContext);
        context = drawingContext;

        setEGLContextClientVersion(3);

        setRenderer(this);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

        gestureDetector = new GestureDetector(context,this,null,false);
        gestureDetector.setOnDoubleTapListener(this);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        int eventaction = event.getAction();
        if(!gestureDetector.onTouchEvent(event))
            super.onTouchEvent(event);
        return (true);
    }

    @Override
    public void onSurfaceCreated(GL10 gl,EGLConfig config)
    {
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("VDG: "+version);

        String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("VDG: GLSL version ="+glslVersion);

        initialize(gl);
    }

    @Override
    public void onSurfaceChanged(GL10 unused,int width,int height)
    {
       resize(width,height);
    }

    @Override
    public void onDrawFrame(GL10 unused)
    {
       display();
    }


    @Override
    public boolean onDoubleTap(MotionEvent e)
    {
        System.out.println("VDG: Double Tap");
        return (true);
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent e)
    {
        return (true);
    }
    
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
        System.out.println("VDG: Single Tap");
        return (true);
    }
    
    @Override
    public boolean onDown(MotionEvent e)
    {
        return (true);
    }

    @Override
    public boolean onFling(MotionEvent e1,MotionEvent e2,float velocityX,float velocityY)
    {
        return (true);
    }

    @Override
    public void onLongPress(MotionEvent e)
    {
        System.out.println("VDG: Long Press");
    }

    @Override
    public boolean onScroll(MotionEvent e1,MotionEvent e2,float distanceX,float distanceY)
    {
        uninitialize();
        System.out.println("VDG: Scroll");
        System.exit(0);
        return (true);
    }

    @Override
    public void onShowPress(MotionEvent e)
    {
    }

    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
        return (true);
    }

    private void initialize(GL10 gl)
    {
        //vertex shader

        vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

        final String vertexShaderSourceCode = String.format(
            "#version 320 es"+
            "\n"+
            "in vec4 vPosition;"+
            "in vec2 vTexture0_Coord;"+
            "uniform mat4 u_mvp_matrix;"+
            "out vec2 out_texture0_coord;"+
            "void main(void)"+
            "{"+
            "gl_Position = u_mvp_matrix * vPosition;"+
            "out_texture0_coord = vTexture0_Coord;"+
            "}"
            );

        GLES32.glShaderSource(vertexShaderObject,vertexShaderSourceCode);
        GLES32.glCompileShader(vertexShaderObject);

        int[] iShaderCompiledStatus = new int[1];
        int[] iInfoLogLength = new int[1];
        String szInfoLog = null;

        GLES32.glGetShaderiv(vertexShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);
        if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(vertexShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
            if(iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
                System.out.println("AC: Vertex Shader Compilation Log"+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        //fragment shader
        fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);


        final String fragmentShaderSourceCode = String.format
        (
            "#version 320 es"+
            "\n"+
            "precision highp float;"+
            "in vec2 out_texture0_coord;"+
            "out vec4 FragColor;"+
            "uniform highp sampler2D u_texture0_sampler;"+
            "void main(void)"+
            "{"+
            "FragColor = texture(u_texture0_sampler,out_texture0_coord);"+
            "}"
        );

        GLES32.glShaderSource(fragmentShaderObject,fragmentShaderSourceCode);
        GLES32.glCompileShader(fragmentShaderObject);
        iShaderCompiledStatus[0]=0;
        iInfoLogLength[0]=0;
        szInfoLog = null;

        GLES32.glGetShaderiv(fragmentShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);
        if(iShaderCompiledStatus[0] == GLES32.GL_FALSE);
        {
            GLES32.glGetShaderiv(fragmentShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
            if(iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
                System.out.println("VDG: Fragment Shader Compilation Log"+szInfoLog);
                uninitialize();
                System.exit(0);   
            }
        }


        //shader program
        shaderProgramObject = GLES32.glCreateProgram();
        GLES32.glAttachShader(shaderProgramObject,vertexShaderObject);
        GLES32.glAttachShader(shaderProgramObject,fragmentShaderObject);

        //prelink binding in variables
        GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.AC_ATTRIBUTE_VERTEX,"vPosition");
        GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.AC_ATTRIBUTE_TEXTURE0,"vTexture0_Coord");

        //link program
        GLES32.glLinkProgram(shaderProgramObject);
        int[] iShaderProgramLinkStatus = new int[1];
        iInfoLogLength[0] = 0;
        szInfoLog = null;

        GLES32.glGetProgramiv(shaderProgramObject,GLES32.GL_LINK_STATUS,iShaderProgramLinkStatus,0);
        if(iShaderProgramLinkStatus[0] == GLES32.GL_FALSE);
        {
            GLES32.glGetProgramiv(shaderProgramObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
            if(iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
                System.out.println("VDG: Shader Program Link Log"+szInfoLog);
                uninitialize();
                System.exit(0);   
            }
        }

        //get uniform variable location 
        mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject,"u_mvp_matrix");
        texture0_sampler_uniform = GLES32.glGetUniformLocation(shaderProgramObject,"u_texture0_sampler");

        LoadGLTextures();

        GLES32.glGenVertexArrays(1,vao_square,0);
        GLES32.glBindVertexArray(vao_square[0]);
        //square vertices
        GLES32.glGenBuffers(1,vbo_square_position,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_square_position[0]);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,8*3 * 4 ,
            null,GLES32.GL_DYNAMIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.AC_ATTRIBUTE_VERTEX,3,GLES32.GL_FLOAT,false,0,0);

        GLES32.glEnableVertexAttribArray(GLESMacros.AC_ATTRIBUTE_VERTEX);

        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
    
        float squareTexCoords[]= new float[]
        {
            1.0f,1.0f,
            0.0f,1.0f,
            0.0f,0.0f,
            1.0f,0.0f,
            1.0f,1.0f,
            0.0f,1.0f,
            0.0f,0.0f,
            1.0f,0.0f
        };
        GLES32.glGenBuffers(1,vbo_square_texture,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_square_texture[0]);

        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(squareTexCoords.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer squareTexCoordsBuffer = byteBuffer.asFloatBuffer();
        squareTexCoordsBuffer.put(squareTexCoords);
        squareTexCoordsBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,squareTexCoords.length *4,
            squareTexCoordsBuffer,GLES32.GL_STATIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.AC_ATTRIBUTE_TEXTURE0,2,GLES32.GL_FLOAT,false,0,0);

        GLES32.glEnableVertexAttribArray(GLESMacros.AC_ATTRIBUTE_TEXTURE0);

        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
        GLES32.glBindVertexArray(0);

        GLES32.glEnable(GLES32.GL_DEPTH_TEST);

        GLES32.glDepthFunc(GLES32.GL_LEQUAL);

        GLES32.glEnable(GLES32.GL_CULL_FACE);

        GLES32.glClearColor(0.0f,0.0f,0.0f,0.0f);  //black color

        Matrix.setIdentityM(perspectiveProjectionMatrix,0);
    }


    private void LoadGLTextures()
    {

        MakeCheckImage();
        List<Byte> al = new ArrayList();

        for(int i=0;i<checkImageHeight;i++)
        {
            for (int j = 0; j < checkImageWidth; j++)
            {
                al.add(checkImage[i][j][0]);
                al.add(checkImage[i][j][0]);
                al.add(checkImage[i][j][0]);
                al.add(checkImage[i][j][0]);       
            }   
        }
        
        byte byteArray[] = new byte[al.size()];
  

        for(int i = 0; i < al.size(); i++) {
            byteArray[i] = al.get(i).byteValue();
        }
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(byteArray.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        byteBuffer.put(byteArray);
        byteBuffer.position(0);
        GLES32.glPixelStorei(GLES32.GL_UNPACK_ALIGNMENT,1);
        GLES32.glGenTextures(1,TexName,0);
        GLES32.glBindTexture(GLES32.GL_TEXTURE_2D,TexName[0]);
        GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D,GLES32.GL_TEXTURE_WRAP_S,GLES32.GL_REPEAT);
        GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D,GLES32.GL_TEXTURE_WRAP_T,GLES32.GL_REPEAT);
        GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D,GLES32.GL_TEXTURE_MAG_FILTER,GLES32.GL_NEAREST);
        GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D,GLES32.GL_TEXTURE_MIN_FILTER,GLES32.GL_NEAREST);
        GLES32.glTexImage2D(GLES32.GL_TEXTURE_2D,0,GLES32.GL_RGBA,checkImageWidth,checkImageHeight,0,GLES32.GL_RGBA,GLES32.GL_UNSIGNED_BYTE,byteBuffer);
    }

    private void MakeCheckImage()
    {
        int i,j,b;
        boolean c;

        for(i=0;i<checkImageHeight;i++)
        {
            for (j = 0; j < checkImageWidth; j++)
            {
                c = (((i & 0x8) == 0) ^ ((j & 0x8)==0)) ;
                if(c == true)
                {
                    b = 255;
                }
                else
                {
                    b = 0;
                }
            
                checkImage[i][j][0] = (byte)b;
                checkImage[i][j][1] = (byte)b;
                checkImage[i][j][2] = (byte)b;
                checkImage[i][j][3] = (byte)255;
            }
        }
    }

    
    private void resize(int width,int height)
    {
        GLES32.glViewport(0,0,width,height); 

        Matrix.perspectiveM(perspectiveProjectionMatrix,0,45.0f,(float)width / (float)height,0.1f,100.0f);
    }

    private void update()
    {
       
    }

    public void display()
    {
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT| GLES32.GL_DEPTH_BUFFER_BIT);

        GLES32.glUseProgram(shaderProgramObject);

        float[] modelViewMatrix= new float[16];
        float[] modelViewProjectionMatrix  = new float[16];
        

        Matrix.setIdentityM(modelViewMatrix,0);
        Matrix.setIdentityM(modelViewProjectionMatrix,0);
       

        //square block
        Matrix.translateM(modelViewMatrix,0,0.0f,0.0f,-6.0f);
        Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
        GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
        
        GLES32.glBindVertexArray(vao_square[0]);

        float squareVertices[]= new float[]
        {
            0.0f, 1.0f, 0.0f,
            -2.0f, 1.0f, 0.0f,
            -2.0f, -1.0f, 0.0f,
            0.0f, -1.0f, 0.0f,
            2.41421f, 1.0f, -1.41421f,
            1.0f, 1.0f, 0.0f,
            1.0f, -1.0f, 0.0f,
            2.41421f, -1.0f, -1.41421f
        };


        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(squareVertices.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer squareverticesBuffer = byteBuffer.asFloatBuffer();
        squareverticesBuffer.put(squareVertices);
        squareverticesBuffer.position(0);

        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_square_position[0]);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,squareVertices.length *4,
            squareverticesBuffer,GLES32.GL_DYNAMIC_DRAW);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
        
        GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
        GLES32.glBindTexture(GLES32.GL_TEXTURE_2D,TexName[0]);
        GLES32.glUniform1i(texture0_sampler_uniform,0);
        
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,0,4);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,4,4);

        GLES32.glBindVertexArray(0);

        GLES32.glUseProgram(0);

        update();
        requestRender();
    }

    void uninitialize()
    {
        
         if(TexName[0] != 0)
        {
            GLES32.glDeleteTextures(1,TexName,0);
            TexName[0] = 0;
        }

        if(vbo_square_position[0]!=0)
        {    
            GLES32.glDeleteBuffers(1,vbo_square_position,0);
            vbo_square_position[0] = 0;
        }

        if(vbo_square_texture[0]!=0)
        {    
            GLES32.glDeleteBuffers(1,vbo_square_texture,0);
            vbo_square_texture[0] = 0;
        }

        if(shaderProgramObject != 0)
        {
            if(vertexShaderObject != 0)
            {
                GLES32.glDetachShader(shaderProgramObject,vertexShaderObject);
                GLES32.glDeleteShader(vertexShaderObject);
                vertexShaderObject = 0;
            }

            if(fragmentShaderObject != 0)
            {
                GLES32.glDetachShader(shaderProgramObject,fragmentShaderObject);
                GLES32.glDeleteShader(fragmentShaderObject);
                fragmentShaderObject = 0;
            }
        }

        if(shaderProgramObject != 0)
        {
            GLES32.glDeleteProgram(shaderProgramObject);
            shaderProgramObject = 0;
        }
    }
}