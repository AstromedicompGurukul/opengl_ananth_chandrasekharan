package com.astromedicomp.MultiColoredTriangle;

// default supplied packages by android SDK
import android.app.Activity;
import android.os.Bundle;

// later added packages
import android.view.Window; // for "Window" class
import android.view.WindowManager; // for "WindowManager" class
import android.content.pm.ActivityInfo; // for "ActivityInfo" class
import android.graphics.Color;
import android.view.Gravity;
import android.widget.TextView;

public class ACActivity extends Activity
{
    private ACView acView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        // this is done to get rid of ActionBar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        // this is done to make Fullscreen
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
		//setContentView(R.layout.activity_main);
		getWindow().getDecorView().setBackgroundColor(Color.rgb(0,0,0));
		
        // force activity window orientation to Landscape
        ACActivity.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        //myView=new MyView(this);
		acView=new ACView(this);
        
        // set view as content view of the activity
        //setContentView(myView);
		setContentView(acView);
    }
    
    @Override
    protected void onPause()
    {
        super.onPause();
    }
    
    @Override
    protected void onResume()
    {
        super.onResume();
    }
}
