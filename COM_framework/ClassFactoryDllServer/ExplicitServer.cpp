//#define UNICODE
#include<windows.h>
#include "ClassFactoryHeader.h"
#include "CommonVariables.h"

ISum *pISum = NULL;
ISub *pISub = NULL;

// DllMain
BOOL WINAPI DllMain(HANDLE hModule,DWORD dwReason,LPVOID lpReserved)
{
	void LoadClassFactoryServer();
	void FreeClassFactoryPointers();

	// code
	switch(dwReason)
	{
	case DLL_PROCESS_ATTACH:
		hr = CoInitialize(NULL);
		if (FAILED(hr))
		{
			MessageBox(NULL,TEXT("COM Engine not initialized"),TEXT("Message"),MB_OK|MB_TOPMOST);
		}
		else
		{
			
		}

		LoadClassFactoryServer();
		break;
	case DLL_PROCESS_DETACH:
		FreeClassFactoryPointers();
		CoUninitialize();
		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	}
	return(TRUE);
}

void RegisterClassFactoryServer()
{
	GetSystemDirectory(szWinSysDir, 255);
	wsprintf(szPath, TEXT("%s"), szWinSysDir);

	wsprintf(szPath, TEXT("%s\\regsvr32.exe /s %s\\%s"), szWinSysDir, PathOfClassFactoryDll, NameofClassFactoryDll);
	if (CreateProcess(NULL, szPath, NULL, NULL, TRUE, NULL, NULL, NULL, &si, &pi) != 0)
	{
		WaitForSingleObject(pi.hProcess, INFINITE);
		CloseHandle(pi.hThread);
		CloseHandle(pi.hProcess);
	}
	else
	{
		MessageBox(NULL, TEXT("CreateProcess() failed"), TEXT("Error"), MB_OK | MB_TOPMOST);
	}
}

void UnRegisterClassFactoryServer()
{
	GetSystemDirectory(szWinSysDir, 255);
	wsprintf(szPath, TEXT("%s"), szWinSysDir);

	wsprintf(szPath, TEXT("%s\\regsvr32.exe -u /s %s\\%s"), szWinSysDir, PathOfClassFactoryDll, NameofClassFactoryDll);
	if (CreateProcess(NULL, szPath, NULL, NULL, TRUE, NULL, NULL, NULL, &si, &pi) != 0)
	{
		WaitForSingleObject(pi.hProcess, INFINITE);
		CloseHandle(pi.hThread);
		CloseHandle(pi.hProcess);
	}
	else
	{
		MessageBox(NULL, TEXT("CreateProcess() failed"), TEXT("Error"), MB_OK | MB_TOPMOST);
	}
}

void LoadClassFactoryServer()
{
	void RegisterClassFactoryServer();

	RegisterClassFactoryServer();

	hr = CoCreateInstance(CLSID_SumSub, NULL, CLSCTX_INPROC_SERVER, IID_ISum, (void **)&pISum);
	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("ClassFactory Component for Sum() can not be created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		
	}

	pISum->QueryInterface(IID_ISub, (void**)&pISub);
	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("ClassFactory Component for Sub() can not be created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		
	}
}

void FreeClassFactoryPointers()
{
	void UnRegisterClassFactoryServer();

	if (pISum != NULL)
	{
		pISum->Release();
		pISum = NULL;
	}

	if (pISub != NULL)
	{
		pISub->Release();
		pISub = NULL;
	}

	UnRegisterClassFactoryServer();
}

void Sum(int n1, int n2, int *res)
{
	pISum->Sum(n1, n2, res);
}

void Sub(int n1, int n2, int *res)
{
	pISub->Sub(n1, n2, res);
}


