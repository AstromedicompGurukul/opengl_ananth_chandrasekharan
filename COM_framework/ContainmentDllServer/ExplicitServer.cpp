//#define UNICODE
#include<windows.h>
#include "ContainmentInner.h"
#include "ContainmentOuter.h"
#include "CommonVariables.h"

ISum_Containment *pISumContainment = NULL;
ISub_Containment *pISubContainment = NULL;
IMul *pIMul = NULL;
IDiv *pIDiv = NULL;

// DllMain
BOOL WINAPI DllMain(HANDLE hModule,DWORD dwReason,LPVOID lpReserved)
{
	void LoadContainmentServer();
	void FreeContainmentPointers();

	// code
	switch(dwReason)
	{
	case DLL_PROCESS_ATTACH:
		hr = CoInitialize(NULL);
		if (FAILED(hr))
		{
			MessageBox(NULL,TEXT("COM Engine not initialized"),TEXT("Message"),MB_OK|MB_TOPMOST);
		}
		else
		{
			
		}

		LoadContainmentServer();
		break;
	case DLL_PROCESS_DETACH:
		FreeContainmentPointers();
		CoUninitialize();
		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	}
	return(TRUE);
}

void LoadContainmentServer()
{
	void RegisterContainmentInnerServer();
	void RegisterContainmentOuterServer();

	RegisterContainmentInnerServer();
	RegisterContainmentOuterServer();

	hr = CoCreateInstance(CLSID_SumSub_Containment, NULL, CLSCTX_INPROC_SERVER, IID_ISum_Containment, (void **)&pISumContainment);
	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("Containment Component for Sum() can not be created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		
	}

	hr=pISumContainment->QueryInterface(IID_ISub_Containment, (void**)&pISubContainment);
	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("Containment Component for Sub() can not be created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		
	}

	hr=pISumContainment->QueryInterface(IID_IMul, (void**)&pIMul);
	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("Containment Component for Mul() can not be created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		
	}

	hr= pISumContainment->QueryInterface(IID_IDiv, (void**)&pIDiv);
	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("Containment Component for Div() can not be created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		
	}
}

void FreeContainmentPointers()
{
	void UnRegisterContainmentInnerServer();
	void UnRegisterContainmentOuterServer();

	if (pISumContainment!=NULL)
	{
		pISumContainment->Release();
		pISumContainment = NULL;
	}

	if (pISubContainment!=NULL)
	{
		pISubContainment->Release();
		pISubContainment = NULL;
	}

	if (pIMul!=NULL)
	{
		pIMul->Release();
		pIMul = NULL;
	}

	if (pIDiv!=NULL)
	{
		pIDiv->Release();
		pIDiv = NULL;
	}

	UnRegisterContainmentInnerServer();
	UnRegisterContainmentOuterServer();
}

void RegisterContainmentInnerServer()
{
	GetSystemDirectory(szWinSysDir, 255);
	wsprintf(szPath, TEXT("%s"), szWinSysDir);

	wsprintf(szPath, TEXT("%s\\regsvr32.exe /s  %s\\%s"), szWinSysDir, PathOfClassFactoryDll, NameofContainmentInnerDll);
	if (CreateProcess(NULL, szPath, NULL, NULL, TRUE, NULL, NULL, NULL, &si, &pi) != 0)
	{
		WaitForSingleObject(pi.hProcess, INFINITE);
		CloseHandle(pi.hThread);
		CloseHandle(pi.hProcess);
	}
	else
	{
		MessageBox(NULL, TEXT("CreateProcess() failed"), TEXT("Error"), MB_OK | MB_TOPMOST);
	}
}

void RegisterContainmentOuterServer()
{
	GetSystemDirectory(szWinSysDir, 255);
	wsprintf(szPath, TEXT("%s"), szWinSysDir);

	wsprintf(szPath, TEXT("%s\\regsvr32.exe /s %s\\%s"), szWinSysDir, PathOfClassFactoryDll, NameofContainmentOuterDll);
	if (CreateProcess(NULL, szPath, NULL, NULL, TRUE, NULL, NULL, NULL, &si, &pi) != 0)
	{
		WaitForSingleObject(pi.hProcess, INFINITE);
		CloseHandle(pi.hThread);
		CloseHandle(pi.hProcess);
	}
	else
	{
		MessageBox(NULL, TEXT("CreateProcess() failed"), TEXT("Error"), MB_OK | MB_TOPMOST);
	}
}

void UnRegisterContainmentInnerServer()
{
	GetSystemDirectory(szWinSysDir, 255);
	wsprintf(szPath, TEXT("%s"), szWinSysDir);

	wsprintf(szPath, TEXT("%s\\regsvr32.exe -u /s %s\\%s"), szWinSysDir, PathOfClassFactoryDll, NameofContainmentInnerDll);
	if (CreateProcess(NULL, szPath, NULL, NULL, TRUE, NULL, NULL, NULL, &si, &pi) != 0)
	{
		WaitForSingleObject(pi.hProcess, INFINITE);
		CloseHandle(pi.hThread);
		CloseHandle(pi.hProcess);
	}
	else
	{
		MessageBox(NULL, TEXT("CreateProcess() failed"), TEXT("Error"), MB_OK | MB_TOPMOST);
	}
}

void UnRegisterContainmentOuterServer()
{
	GetSystemDirectory(szWinSysDir, 255);
	wsprintf(szPath, TEXT("%s"), szWinSysDir);

	wsprintf(szPath, TEXT("%s\\regsvr32.exe -u  /s %s\\%s"), szWinSysDir, PathOfClassFactoryDll, NameofContainmentOuterDll);
	if (CreateProcess(NULL, szPath, NULL, NULL, TRUE, NULL, NULL, NULL, &si, &pi) != 0)
	{
		WaitForSingleObject(pi.hProcess, INFINITE);
		CloseHandle(pi.hThread);
		CloseHandle(pi.hProcess);
	}
	else
	{
		MessageBox(NULL, TEXT("CreateProcess() failed"), TEXT("Error"), MB_OK | MB_TOPMOST);
	}
}



void Sum_Containment(int n1, int n2, int *res)
{
	pISumContainment->Sum(n1,n2,res);
}

void Sub_Containment(int n1, int n2, int *res)
{
	pISubContainment->Sub(n1,n2,res);
}

void Mul(int n1,int n2,int *res)
{
	pIMul->Mul(n1,n2,res);
}

void Div(int n1, int n2, int *res)
{
	pIDiv->Div(n1,n2,res);
}



