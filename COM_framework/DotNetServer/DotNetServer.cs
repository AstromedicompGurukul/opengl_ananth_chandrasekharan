﻿using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace ManagedServerforInterop
{
    [ClassInterface(ClassInterfaceType.AutoDispatch)]
    public class Math
    {
        public int MultiplicationValue
        {
            get;
            set;
        }

        public int DivisionValue
        {
            get;
            set;
        }

        public Math()
        {

        }

        public int MultiplicationOfTwoIntegers(int n1,int n2)
        {
            MultiplicationValue = n1 * n2;
            MessageBox.Show("Mul of two integers is "+ MultiplicationValue);
            return MultiplicationValue;
        }

        public int DivisionOfTwoIntegers(int n1,int n2)
        {
            DivisionValue = n1 / n2;
            MessageBox.Show("Div of two integers is " + DivisionValue);
            return DivisionValue;

        }
    }
}