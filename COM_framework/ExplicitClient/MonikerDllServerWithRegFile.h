#pragma once
#include<Windows.h>

class IOddNumber:public IUnknown
{
public:
	// IOddNumber specific method declarations
	virtual HRESULT __stdcall GetNextOddNumber(int *)=0;// pure virtual
};
class IOddNumberFactory:public IUnknown
{
public:
	// IOddNumberFactory specific method declarations
	virtual HRESULT __stdcall SetFirstOddNumber(int,IOddNumber **)=0;// pure virtual
};

// CLSID of OddNumber Component {0A37F4CE-E65D-41d7-9B6C-68CCAB226C6B}
/*
// {24FE38F2-638F-4210-AC88-0563A5CC29A5}
static const GUID <<name>> =
{ 0x24fe38f2, 0x638f, 0x4210, { 0xac, 0x88, 0x5, 0x63, 0xa5, 0xcc, 0x29, 0xa5 } };
*/
const CLSID CLSID_OddNumber= { 0x24fe38f2, 0x638f, 0x4210, 0xac, 0x88, 0x5, 0x63, 0xa5, 0xcc, 0x29, 0xa5};


// IID of IOddNumber Interface
/*
// {EBFFD4BB-AE52-4561-AE9E-55B901A22FEC}
static const GUID <<name>> =
{ 0xebffd4bb, 0xae52, 0x4561, { 0xae, 0x9e, 0x55, 0xb9, 0x1, 0xa2, 0x2f, 0xec } };
*/
const IID IID_IOddNumber= { 0xebffd4bb, 0xae52, 0x4561, 0xae, 0x9e, 0x55, 0xb9, 0x1, 0xa2, 0x2f, 0xec};

// IID of IOddNumberFactory Interface
/*
// {24FE38F2-638F-4210-AC88-0563A5CC29A5}
static const GUID <<name>> =
{ 0x24fe38f2, 0x638f, 0x4210, { 0xac, 0x88, 0x5, 0x63, 0xa5, 0xcc, 0x29, 0xa5 } };
*/
const IID IID_IOddNumberFactory= { 0x24fe38f2, 0x638f, 0x4210, 0xac, 0x88, 0x5, 0x63, 0xa5, 0xcc, 0x29, 0xa5};
