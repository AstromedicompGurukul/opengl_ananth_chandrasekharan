#include<windows.h>
#include<stdio.h>
#include "ClassFactoryHeader.h"
#include "ContainmentInner.h"
#include "ContainmentOuter.h"
#include "AggregationClient.h"
#include "ProjExeServerWithOneInterface.h"
#include "AutomationServer.h"
#include "MonikerDllServerWithRegFile.h"

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

HRESULT hr;

// WinMain()
int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpszCmdLine,int nCmdShow)
{
	// variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[]=TEXT("MyApp");
	// code
	// initialization of WNDCLASSEX
	wndclass.cbSize=sizeof(WNDCLASSEX);
	wndclass.style=CS_HREDRAW|CS_VREDRAW;
	wndclass.cbClsExtra=0;
	wndclass.cbWndExtra=0;
	wndclass.lpfnWndProc=WndProc;
	wndclass.hInstance=hInstance;
	wndclass.hIcon=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hCursor=LoadCursor(NULL,IDC_ARROW);
	wndclass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName=szAppName;
	wndclass.lpszMenuName=NULL;
	wndclass.hIconSm=LoadIcon(NULL,IDI_APPLICATION);
	// register above class
	RegisterClassEx(&wndclass);
	// create window
	hwnd=CreateWindow(szAppName,
		              TEXT("Client incorporating all factory patterns"),
					  WS_OVERLAPPEDWINDOW,
					  CW_USEDEFAULT,
					  CW_USEDEFAULT,
					  CW_USEDEFAULT,
					  CW_USEDEFAULT,
                      NULL,
					  NULL,
					  hInstance,
					  NULL);
	ShowWindow(hwnd,nCmdShow);
	UpdateWindow(hwnd);
	// message loop
	while(GetMessage(&msg,NULL,0,0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return((int)msg.wParam);
}
LRESULT CALLBACK WndProc(HWND hwnd,UINT iMsg,WPARAM wParam,LPARAM lParam)
{
	// variable declarations
	static HMODULE hLib_cf=NULL;
	static HMODULE hLib_ct = NULL;
	static HMODULE hLib_agg = NULL;
	static HMODULE hLib_aut = NULL;
	static HMODULE hLib_exeserver = NULL;
	static HMODULE hLib_moniker = NULL;

	typedef void(*func)(void);
	func classfactoryserver=NULL;
	func classfactoryserver_free=NULL;
	func containmentserver=NULL;
	func containmentserver_free=NULL;
	func aggregationserver=NULL;
	func aggregationserver_free=NULL;
	func exeserver = NULL;
	func exeserver_free = NULL;
	func automationserver = NULL;
	func automationserver_free = NULL;

	typedef void(*monikerfunc) (int);
	monikerfunc moniker = NULL;

	typedef void (*funptr)(int,int,int*);
	funptr func1 = NULL;
	funptr func2 = NULL;

	funptr func3 = NULL;
	funptr func4 = NULL;
	funptr func5 = NULL;
	funptr func6 = NULL;

	int n1, n2;
	int res;
	wchar_t str[255];

	typedef void(*agg)(int, double*);
	agg func7=NULL;
	typedef void (*aggsquare)(int, int*);
	aggsquare func8 = NULL;

	int n3;
	double aggres;

	typedef void(*exeserverfunc)(double,double*);
	exeserverfunc sinfunc=NULL;
	exeserverfunc cosfunc=NULL;
	exeserverfunc tanfunc=NULL;

	double n9;
	double fres;

	typedef void(*automationserverfunc)(double, double*);
	automationserverfunc cuberootfunc = NULL;
	aggsquare cubefunc = NULL;

	int cube_result;
	double cube_root_input;
	double cube_root_result;

	// code
	switch(iMsg)
	{

	case WM_CREATE:

		hLib_cf=LoadLibrary(TEXT("ClassFacoryDllServer.dll"));
		if(hLib_cf==NULL)
		{
			MessageBox(hwnd,TEXT("Can Not Load Dll for Client Dll."),TEXT("Error"),MB_OK);
			DestroyWindow(hwnd);
			return -1;
		}
		else
		{
			
		}

		func1 = (funptr)GetProcAddress(hLib_cf, "Sum");
		if (func1 == NULL)
		{
			MessageBox(hwnd, TEXT("Sum() not found"), TEXT("Error"), MB_OK);
			DestroyWindow(hwnd);
			return -1;
		}
		else
		{
			
		}

		func2 = (funptr)GetProcAddress(hLib_cf, "Sub");
		if (func2 == NULL)
		{
			MessageBox(hwnd, TEXT("Sub() address loaded"), TEXT("Error"), MB_OK);
			DestroyWindow(hwnd);
			return -1;
		}
		else
		{
			
		}

		

		classfactoryserver=(func)GetProcAddress(hLib_cf, "LoadClassFactoryServer");
		if (classfactoryserver == NULL)
		{
			MessageBox(hwnd, TEXT("LoadClassFactoryServer() address not loaded"), TEXT("Error"), MB_OK);
			DestroyWindow(hwnd);
			return -1;
		}
		else
		{
			
		}

		classfactoryserver_free= (func)GetProcAddress(hLib_cf, "FreeClassFactoryPointers");
		if (classfactoryserver == NULL)
		{
			MessageBox(hwnd, TEXT("FreeClassFactoryPointers() address not loaded"), TEXT("Error"), MB_OK);
			DestroyWindow(hwnd);
			return -1;
		}
		else
		{
			
		}
		
		classfactoryserver();

 		n1 = 5;
		n2 = 7;
		func1(n1,n2,&res);
		wsprintf(str,TEXT("Add of %d and %d is %d"),n1,n2,res);
		MessageBox(NULL,str,TEXT("ClassFactory : Message for Add"),MB_OK|MB_TOPMOST);

		n1 = 5;
		n2 = 7;
		func2(n1, n2, &res);
		wsprintf(str, TEXT("Sub of %d and %d is %d"), n1, n2, res);
		MessageBox(NULL, str, TEXT("ClassFactory : Message for Sub"), MB_OK | MB_TOPMOST);

		classfactoryserver_free();

		FreeLibrary(hLib_cf);

		hLib_ct = LoadLibrary(TEXT("ContainmentDllServer.dll"));
		if (hLib_ct == NULL)
		{
			MessageBox(hwnd, TEXT("Can Not Load Dll for Client Dll."), TEXT("Error"), MB_OK);
			DestroyWindow(hwnd);
			return -1;
		}
		else
		{

		}

		containmentserver=(func)GetProcAddress(hLib_ct, "LoadContainmentServer");
		if (containmentserver == NULL)
		{
			MessageBox(hwnd, TEXT("LoadContainmentServer() address not loaded"), TEXT("Error"), MB_OK | MB_TOPMOST);
			DestroyWindow(hwnd);
			return -1;
		}
		else
		{
			
		}

		containmentserver_free = (func)GetProcAddress(hLib_ct, "FreeContainmentPointers");
		if (containmentserver_free == NULL)
		{
			MessageBox(hwnd, TEXT("FreeContainmentPointers() address not loaded"), TEXT("Error"), MB_OK | MB_TOPMOST);
			DestroyWindow(hwnd);
			return -1;
		}
		else
		{
			
		}

		containmentserver();

		func3 = (funptr)GetProcAddress(hLib_ct, "Sum_Containment");
		if (func3 == NULL)
		{
			MessageBox(hwnd, TEXT("Sum_Containment() not found"), TEXT("Error"), MB_OK | MB_TOPMOST);
			DestroyWindow(hwnd);
			return -1;
		}
		else
		{
			
		}

		func4 = (funptr)GetProcAddress(hLib_ct, "Sub_Containment");
		if (func4 == NULL)
		{
			MessageBox(hwnd, TEXT("Sub_Containment() address not loaded"), TEXT("Error"), MB_OK | MB_TOPMOST);
			DestroyWindow(hwnd);
			return -1;
		}
		else
		{
			
		}

		func5 = (funptr)GetProcAddress(hLib_ct, "Mul");
		if (func5 == NULL)
		{
			MessageBox(hwnd, TEXT("Mul() address not loaded"), TEXT("Error"), MB_OK | MB_TOPMOST);
			DestroyWindow(hwnd);
			return -1;
		}
		else
		{
			
		}

		func6 = (funptr)GetProcAddress(hLib_ct, "Div");
		if (func6== NULL)
		{
			MessageBox(hwnd, TEXT("Div() address not loaded"), TEXT("Error"), MB_OK | MB_TOPMOST);
			DestroyWindow(hwnd);
			return -1;
		}
		else
		{
			
		}

		n1 = 10;
		n2 = 34;
		func3(n1, n2, &res);
		wsprintf(str, TEXT("Add of %d and %d is %d"), n1, n2, res);
		MessageBox(NULL, str, TEXT("Containment : Message for Add"), MB_OK | MB_TOPMOST);

		n1 = 10;
		n2 = 2;
		func4(n1, n2, &res);
		wsprintf(str, TEXT("Sum of %d and %d is %d"), n1, n2, res);
		MessageBox(NULL, str, TEXT("Containment : Message for Sub"), MB_OK | MB_TOPMOST);

		n1 = 10;
		n2 = 2;
		func5(n1, n2, &res);
		wsprintf(str, TEXT("Mul of %d and %d is %d"), n1, n2, res);
		MessageBox(NULL, str, TEXT("Containment : Message for Mul"), MB_OK | MB_TOPMOST);

		n1 = 10;
		n2 = 2;
		func6(n1, n2, &res);
		wsprintf(str, TEXT("Div of %d and %d is %d"), n1, n2, res);
		MessageBox(NULL, str, TEXT("Containment : Message for Div"), MB_OK | MB_TOPMOST);

		containmentserver_free();

		FreeLibrary(hLib_ct);

		hLib_agg = LoadLibrary(TEXT("AggregationDllServer.dll"));
		if (hLib_agg == NULL)
		{
			MessageBox(hwnd, TEXT("Can Not Load Dll for Client Dll."), TEXT("Error"), MB_OK);
			DestroyWindow(hwnd);
			return -1;
		}
		else
		{

		}


	   aggregationserver=(func)GetProcAddress(hLib_agg, "LoadAggregationServer");
	   if (aggregationserver == NULL)
	   {
		   MessageBox(hwnd, TEXT("LoadAggregationServer() address not loaded"), TEXT("Error"), MB_OK | MB_TOPMOST);
		   DestroyWindow(hwnd);
		   return -1;
	   }
	   else
	   {
		  
	   }

	   aggregationserver();

	   func7 = (agg)GetProcAddress(hLib_agg, "SquareRoot");
	   if (func7 == NULL)
	   {
		   MessageBox(hwnd, TEXT("SquareRoot() address not loaded"), TEXT("Error"), MB_OK | MB_TOPMOST);
		   DestroyWindow(hwnd);
		   return -1;
	   }
	   else
	   {
		  
	   }

	   n3 = 25;
	   func7(n3, &aggres);
	   swprintf_s(str,TEXT("SquareRoot of %d is %f"),n3,aggres);
	   MessageBox(NULL,str,TEXT("Aggregation : SquareRoot"),MB_OK);

	   func8 = (aggsquare)GetProcAddress(hLib_agg, "Square");
	   if (func8 == NULL)
	   {
		   MessageBox(hwnd, TEXT("Square() address not loaded"), TEXT("Error"), MB_OK | MB_TOPMOST);
		   DestroyWindow(hwnd);
		   return -1;
	   }
	   else
	   {
		  
	   }

	   n1 = 5;
	   func8(n1,&res);
	   wsprintf(str,TEXT("Square of %d is %d"),n1,res);
	   MessageBox(NULL,str,TEXT("Aggregation : Square"),MB_OK | MB_TOPMOST);

	   aggregationserver_free = (func)GetProcAddress(hLib_agg, "FreeAggregationPointers");
	   if (aggregationserver == NULL)
	   {
		   MessageBox(hwnd, TEXT("FreeAggregationPointers() address not loaded"), TEXT("Error"), MB_OK | MB_TOPMOST);
		   DestroyWindow(hwnd);
		   return -1;
	   }
	   else
	   {
		  
	   }

	   aggregationserver_free();

	   FreeLibrary(hLib_agg);

	   hLib_exeserver = LoadLibrary(TEXT("ExeServerDllServer.dll"));
	   if (hLib_exeserver == NULL)
	   {
		   MessageBox(hwnd, TEXT("Can Not Load Dll for Client Dll."), TEXT("Error"), MB_OK);
		   DestroyWindow(hwnd);
		   return -1;
	   }
	   else
	   {

	   }

		exeserver = (func)GetProcAddress(hLib_exeserver, "LoadExeServer");
		if (exeserver == NULL)
		{
			MessageBox(hwnd, TEXT("LoadExeServer() address not loaded"), TEXT("Error"), MB_OK | MB_TOPMOST);
			DestroyWindow(hwnd);
			return -1;
		}
		else
		{
			
		}

		sinfunc = (exeserverfunc)GetProcAddress(hLib_exeserver, "Sin");
		if (sinfunc == NULL)
		{
			MessageBox(hwnd, TEXT("Sin() address not loaded"), TEXT("Error"), MB_OK | MB_TOPMOST);
			DestroyWindow(hwnd);
			return -1;
		}
		else
		{
			
		}

		cosfunc = (exeserverfunc)GetProcAddress(hLib_exeserver, "Cos");
		if (cosfunc == NULL)
		{
			MessageBox(hwnd, TEXT("Cos() address not loaded"), TEXT("Error"), MB_OK | MB_TOPMOST);
			DestroyWindow(hwnd);
			return -1;
		}
		else
		{
			
		}

		tanfunc = (exeserverfunc)GetProcAddress(hLib_exeserver, "Tan");
		if (tanfunc == NULL)
		{
			MessageBox(hwnd, TEXT("tan() address not loaded"), TEXT("Error"), MB_OK | MB_TOPMOST);
			DestroyWindow(hwnd);
			return -1;
		}
		else
		{
			
		}

		exeserver_free = (func)GetProcAddress(hLib_exeserver, "FreeExeServerPointers");
		if (exeserver_free == NULL)
		{
			MessageBox(hwnd, TEXT("FreeExeServerPointers() address not loaded"), TEXT("Error"), MB_OK | MB_TOPMOST);
			DestroyWindow(hwnd);
			return -1;
		}
		else
		{
			
		}

		exeserver();

		n9 = 90.0;
		sinfunc(n9,&fres);
		swprintf_s(str,TEXT("Sin of %lf is %lf"),n9,fres);
		MessageBox(hwnd, str, TEXT("ExeServer : Message for Sin"), MB_OK | MB_TOPMOST);

		n9 = 0.0;
		cosfunc(n9, &fres);
		swprintf_s(str, TEXT("Cos of %lf is %lf"), n9, fres);
		MessageBox(hwnd, str, TEXT("ExeServer : Message for Cos"), MB_OK | MB_TOPMOST);

		n9 = 30.0;
		tanfunc(n9, &fres);
		swprintf_s(str, TEXT("tan of %lf is %lf"), n9, fres);
		MessageBox(hwnd, str, TEXT("ExeServer : Message for tan"), MB_OK | MB_TOPMOST);

		exeserver_free();

		FreeLibrary(hLib_exeserver);

		hLib_aut = LoadLibrary(TEXT("AutomationDllServer.dll"));
		if (hLib_aut == NULL)
		{
			MessageBox(hwnd, TEXT("Can Not Load Dll for Client Dll."), TEXT("Error"), MB_OK);
			DestroyWindow(hwnd);
			return -1;
		}
		else
		{

		}

		automationserver = (func)GetProcAddress(hLib_aut, "LoadAutomationServer");
		if (automationserver == NULL)
		{
			MessageBox(hwnd, TEXT("LoadAutomationServer() address not loaded"), TEXT("Error"), MB_OK | MB_TOPMOST);
			DestroyWindow(hwnd);
			return -1;
		}
		else
		{
			
		}

		automationserver_free = (func)GetProcAddress(hLib_aut, "FreeAutomationPointers");
		if (automationserver_free == NULL)
		{
			MessageBox(hwnd, TEXT("FreeAutomationPointers() address not loaded"), TEXT("Error"), MB_OK | MB_TOPMOST);
			DestroyWindow(hwnd);
			return -1;
		}
		else
		{
			
		}

		cubefunc = (aggsquare)GetProcAddress(hLib_aut, "Cube");
		if (cubefunc == NULL)
		{
			MessageBox(hwnd, TEXT("Cube() address not loaded"), TEXT("Error"), MB_OK | MB_TOPMOST);
			DestroyWindow(hwnd);
			return -1;
		}
		else
		{
			
		}

		cuberootfunc = (automationserverfunc)GetProcAddress(hLib_aut, "CubeRoot");
		if (cuberootfunc == NULL)
		{
			MessageBox(hwnd, TEXT("CubeRoot() address not loaded"), TEXT("Error"), MB_OK | MB_TOPMOST);
			DestroyWindow(hwnd);
			return -1;
		}
		else
		{
			
		}



		automationserver();
		
		n1 = 5;
		cubefunc(n1,&cube_result);
		wsprintf(str, TEXT("Cube of %d is %d"), n1, cube_result);
		MessageBox(NULL, str, TEXT("Automation : Message for Cube"), MB_OK | MB_TOPMOST);

		cube_root_input = 125.0;
		cuberootfunc(cube_root_input,&cube_root_result);
		swprintf_s(str,TEXT("CubeRoot of %f is %f"),cube_root_input,cube_root_result);
		MessageBox(NULL, str, TEXT("Automation : Message for CubeRoot"), MB_OK | MB_TOPMOST);

		automationserver_free();

		FreeLibrary(hLib_aut);

		hLib_moniker = LoadLibrary(TEXT("MonikerDllServer.dll"));
		if (hLib_moniker == NULL)
		{
			MessageBox(hwnd, TEXT("Can Not Load Dll for Client Dll."), TEXT("Error"), MB_OK);
			DestroyWindow(hwnd);
			return -1;
		}
		else
		{

		}

		moniker = (monikerfunc)GetProcAddress(hLib_moniker, "GetNextOddNumber");
		if (moniker == NULL)
		{
			MessageBox(hwnd, TEXT("GetNextOddNumber() address not loaded"), TEXT("Error"), MB_OK | MB_TOPMOST);
			DestroyWindow(hwnd);
			return -1;
		}
		else
		{

		}

		moniker(5);



		FreeLibrary(hLib_moniker);

		break;

	case WM_DESTROY:
		/*
		if(hLib)
			FreeLibrary(hLib);
		*/
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd,iMsg,wParam,lParam));
}
