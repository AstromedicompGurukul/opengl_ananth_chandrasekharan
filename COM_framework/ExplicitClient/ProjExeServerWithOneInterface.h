#pragma once
#include<Windows.h>

class IMathematics : public IUnknown
{
public:
	//ExeServer specific method
	virtual HRESULT __stdcall SineOfAngleInRad(float, float *) = 0;
	virtual HRESULT __stdcall CosineOfAngleInRad(float, float *) = 0;
	virtual HRESULT __stdcall TangentOfAngleRad(float, float *) = 0;
};

/*
// {3AEBAE98-608E-4972-A98E-EA410554BFD0}
static const GUID <<name>> =
{ 0x3aebae98, 0x608e, 0x4972, { 0xa9, 0x8e, 0xea, 0x41, 0x5, 0x54, 0xbf, 0xd0 } };
*/
const CLSID CLSID_SinCosTan = { 0x3aebae98, 0x608e, 0x4972, 0xa9, 0x8e, 0xea, 0x41, 0x5, 0x54, 0xbf, 0xd0};


/*
// {939B5B27-99D7-4B68-A28F-86BD277471F9}
static const GUID <<name>> =
{ 0x939b5b27, 0x99d7, 0x4b68, { 0xa2, 0x8f, 0x86, 0xbd, 0x27, 0x74, 0x71, 0xf9 } };
*/
const IID IID_IMathematics = { 0x939b5b27, 0x99d7, 0x4b68, 0xa2, 0x8f, 0x86, 0xbd, 0x27, 0x74, 0x71, 0xf9};


