//#define UNICODE
#include<windows.h>
#include "AggregationClient.h"
#include "CommonVariables.h"

ISquare *pISquare = NULL;
ISquareRoot *pISquareRoot = NULL;

// DllMain
BOOL WINAPI DllMain(HANDLE hModule,DWORD dwReason,LPVOID lpReserved)
{
	void LoadAggregationServer();
	void FreeAggregationPointers();

	// code
	switch(dwReason)
	{
	case DLL_PROCESS_ATTACH:
		hr = CoInitialize(NULL);
		if (FAILED(hr))
		{
			MessageBox(NULL,TEXT("COM Engine not initialized"),TEXT("Message"),MB_OK|MB_TOPMOST);
		}
		else
		{
			
		}

		LoadAggregationServer();
		break;
	case DLL_PROCESS_DETACH:
		FreeAggregationPointers();
		CoUninitialize();
		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	}
	return(TRUE);
}

void Square(int num, int *pSqr)
{
	pISquare->SquareOfANumber(num, pSqr);
}

void SquareRoot(int num, double *pSqrt)
{
	pISquareRoot->SquareRootOfANumber(num, pSqrt);
}

void FreeAggregationPointers()
{
	void UnRegisterAggregationInnerServer();
	void UnRegisterAggregationOuterServer();

	if (pISquare!=NULL)
	{
		pISquare->Release();
		pISquare = NULL;
	}

	if (pISquareRoot!=NULL)
	{
		pISquareRoot->Release();
		pISquareRoot = NULL;
	}

	UnRegisterAggregationInnerServer();
	UnRegisterAggregationOuterServer();
}

void LoadAggregationServer()
{
	void RegisterAggregationOuterServer();
	void RegisterAggregationInnerServer();

	RegisterAggregationOuterServer();
	RegisterAggregationInnerServer();

	hr = CoCreateInstance(CLSID_SquareRoot, NULL, CLSCTX_INPROC_SERVER, IID_ISquareRoot, (void **)&pISquareRoot);
	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("Aggregation Component for SquareRoot() can not be created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		
	}

	hr = pISquareRoot->QueryInterface(IID_ISquare, (void**)&pISquare);
	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("Aggregation Component for Square() can not be created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{

	}
}

void RegisterAggregationInnerServer()
{
	GetSystemDirectory(szWinSysDir, 255);
	//wsprintf(szPath, TEXT("%s"), szWinSysDir);

	wsprintf(szPath, TEXT("%s\\regsvr32.exe /s %s\\%s"), szWinSysDir, PathOfClassFactoryDll, NameofAggregationInnerDll);
	if (CreateProcess(NULL, szPath, NULL, NULL, TRUE, NULL, NULL, NULL, &si, &pi) != 0)
	{
		WaitForSingleObject(pi.hProcess, INFINITE);
		CloseHandle(pi.hThread);
		CloseHandle(pi.hProcess);
	}
	else
	{
		MessageBox(NULL, TEXT("CreateProcess() failed"), TEXT("Error"), MB_OK | MB_TOPMOST);
	}
}

void RegisterAggregationOuterServer()
{
	GetSystemDirectory(szWinSysDir, 255);
	//wsprintf(szPath, TEXT("%s"), szWinSysDir);

	wsprintf(szPath, TEXT("%s\\regsvr32.exe /s %s\\%s"), szWinSysDir, PathOfClassFactoryDll, NameofAggregationOuterDll);
	if (CreateProcess(NULL, szPath, NULL, NULL, TRUE, NULL, NULL, NULL, &si, &pi) != 0)
	{
		WaitForSingleObject(pi.hProcess, INFINITE);
		CloseHandle(pi.hThread);
		CloseHandle(pi.hProcess);
	}
	else
	{
		MessageBox(NULL, TEXT("CreateProcess() failed"), TEXT("Error"), MB_OK | MB_TOPMOST);
	}
}

void UnRegisterAggregationInnerServer()
{
	GetSystemDirectory(szWinSysDir, 255);
	//wsprintf(szPath, TEXT("%s"), szWinSysDir);

	wsprintf(szPath, TEXT("%s\\regsvr32.exe -u  /s %s\\%s"), szWinSysDir, PathOfClassFactoryDll, NameofAggregationInnerDll);
	if (CreateProcess(NULL, szPath, NULL, NULL, TRUE, NULL, NULL, NULL, &si, &pi) != 0)
	{
		WaitForSingleObject(pi.hProcess, INFINITE);
		CloseHandle(pi.hThread);
		CloseHandle(pi.hProcess);
	}
	else
	{
		MessageBox(NULL, TEXT("CreateProcess() failed"), TEXT("Error"), MB_OK | MB_TOPMOST);
	}
}

void UnRegisterAggregationOuterServer()
{
	GetSystemDirectory(szWinSysDir, 255);
	//wsprintf(szPath, TEXT("%s"), szWinSysDir);

	wsprintf(szPath, TEXT("%s\\regsvr32.exe -u  /s %s\\%s"), szWinSysDir, PathOfClassFactoryDll, NameofAggregationOuterDll);
	if (CreateProcess(NULL, szPath, NULL, NULL, TRUE, NULL, NULL, NULL, &si, &pi) != 0)
	{
		WaitForSingleObject(pi.hProcess, INFINITE);
		CloseHandle(pi.hThread);
		CloseHandle(pi.hProcess);
	}
	else
	{
		MessageBox(NULL, TEXT("CreateProcess() failed"), TEXT("Error"), MB_OK | MB_TOPMOST);
	}
}


