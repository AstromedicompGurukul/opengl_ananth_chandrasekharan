//#define UNICODE
#include<windows.h>

#include "CommonVariables.h"
#include "MonikerDllServerWithRegFile.h"

IOddNumber *pIOddNumber = NULL;
IOddNumberFactory *pIOddNumberFactory = NULL;

// DllMain
BOOL WINAPI DllMain(HANDLE hModule,DWORD dwReason,LPVOID lpReserved)
{
	// code
	switch(dwReason)
	{
	case DLL_PROCESS_ATTACH:
		hr = CoInitialize(NULL);
		if (FAILED(hr))
		{
			MessageBox(NULL,TEXT("COM Engine not initialized"),TEXT("Message"),MB_OK|MB_TOPMOST);
		}
		else
		{
			
		}

		RegisterMonikerServer();

		break;
	case DLL_PROCESS_DETACH:
		UnRegisterMonikerServer();
		CoUninitialize();
		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	}
	return(TRUE);
}

void RegisterMonikerServer()
{
	GetSystemDirectory(szWinSysDir, 255);

	wsprintf(szPath, TEXT("%s\\regsvr32.exe /s %s\\%s"), szWinSysDir, PathOfClassFactoryDll, NameofMonikerServer);
	if (CreateProcess(NULL, szPath, NULL, NULL, TRUE, NULL, NULL, NULL, &si, &pi) != 0)
	{
		WaitForSingleObject(pi.hProcess, INFINITE);
		CloseHandle(pi.hThread);
		CloseHandle(pi.hProcess);
	}
	else
	{
		MessageBox(NULL, TEXT("CreateProcess() failed"), TEXT("Error"), MB_OK | MB_TOPMOST);
	}
}

void UnRegisterMonikerServer()
{
	GetSystemDirectory(szWinSysDir, 255);

	wsprintf(szPath, TEXT("%s\\regsvr32.exe -u /s  %s\\%s"), szWinSysDir, PathOfClassFactoryDll, NameofMonikerServer);
	if (CreateProcess(NULL, szPath, NULL, NULL, TRUE, NULL, NULL, NULL, &si, &pi) != 0)
	{
		WaitForSingleObject(pi.hProcess, INFINITE);
		CloseHandle(pi.hThread);
		CloseHandle(pi.hProcess);
	}
	else
	{
		MessageBox(NULL, TEXT("CreateProcess() failed"), TEXT("Error"), MB_OK | MB_TOPMOST);
	}
}



void GetNextOddNumber(int num)
{
	IBindCtx *pIBindCtx = NULL;
	IMoniker *pIMoniker = NULL;
	ULONG uEaten;
	HRESULT hr;
	LPOLESTR szCLSID = NULL;
	wchar_t wszCLSID[255], wszTemp[255], *ptr;
	int iFirstOddNumber, iNextOddNumber;
	TCHAR str[255];

	if (hr = CreateBindCtx(0, &pIBindCtx) != S_OK)
	{
		MessageBox(NULL, TEXT("Failed To Get IBindCtx Interface Pointer"), TEXT("Error"), MB_OK);
		exit(0);
	}
	// Get String From Of Binary CLSID
	StringFromCLSID(CLSID_OddNumber, &szCLSID);
	wcscpy(wszTemp, szCLSID);
	ptr = wcschr(wszTemp, '{');
	ptr = ptr + 1;// to remove first opening '{' from CLSID string
	wcscpy(wszTemp, ptr);
	wszTemp[(int)wcslen(wszTemp) - 1] = '\0';// to remove last closing '}' from CLSID string
	wsprintf(wszCLSID, TEXT("clsid:%s"), wszTemp);
	// Get Moniker For This CLSID
	hr = MkParseDisplayName(pIBindCtx, wszCLSID, &uEaten, &pIMoniker);
	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("Failed To Get IMoniker Interface Pointer"), TEXT("Error"), MB_OK);
		pIBindCtx->Release();
		pIBindCtx = NULL;
		exit(0);
	}
	// Bind the moniker to the named object
	hr = pIMoniker->BindToObject(pIBindCtx, NULL, IID_IOddNumberFactory, (void**)&pIOddNumberFactory);
	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("Failed To Get Custom Activation - IOddNumberFactory Interface Pointer"), TEXT("Error"), MB_OK);
		pIMoniker->Release();
		pIMoniker = NULL;
		pIBindCtx->Release();
		pIBindCtx = NULL;
		exit(0);
	}
	// release moniker & Binder
	pIMoniker->Release();
	pIMoniker = NULL;
	pIBindCtx->Release();
	pIBindCtx = NULL;
	// initialize arguments hardcoded
	iFirstOddNumber = num;
	// call SetFirstOddNumber() of IOddNumberFactory to get the first odd number
	hr = pIOddNumberFactory->SetFirstOddNumber(iFirstOddNumber, &pIOddNumber);
	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("Can Not Obtain IOddNumber Interface"), TEXT("Error"), MB_OK);
		exit(0);
	}
	// Release IOddNumberFactory Interface
	pIOddNumberFactory->Release();
	pIOddNumberFactory = NULL;
	// call GetNextOddNumber() Of IOddNumber To Get Next Odd Number Of First Odd Number
	pIOddNumber->GetNextOddNumber(&iNextOddNumber);
	// Release IOddNumber Interface
	pIOddNumber->Release();
	pIOddNumber = NULL;
	// show the results
	wsprintf(str, TEXT("The Next Odd Number From %2d Is %2d"), iFirstOddNumber, iNextOddNumber);
	MessageBox(NULL, str, TEXT("Moniker : Message from Moniker"), MB_OK | MB_TOPMOST);
}





