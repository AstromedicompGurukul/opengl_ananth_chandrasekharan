//#define UNICODE
#include<windows.h>
#include "CommonVariables.h"
#include "AutomationServer.h"

IMyMath *pCube = NULL;

// DllMain
BOOL WINAPI DllMain(HANDLE hModule,DWORD dwReason,LPVOID lpReserved)
{
	void LoadAutomationServer();
	void FreeAutomationPointers();

	// code
	switch(dwReason)
	{
	case DLL_PROCESS_ATTACH:
		hr = CoInitialize(NULL);
		if (FAILED(hr))
		{
			MessageBox(NULL,TEXT("COM Engine not initialized"),TEXT("Message"),MB_OK|MB_TOPMOST);
		}
		else
		{
			
		}

		LoadAutomationServer();
		break;
	case DLL_PROCESS_DETACH:
		CoUninitialize();
		FreeAutomationPointers();
		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	}
	return(TRUE);
}

void LoadAutomationServer()
{
	void RegisterAutomationServer();
	void RegisterAutomationProxyStubDll();
	void RegisterAutomationTlb();

	RegisterAutomationServer();
	RegisterAutomationProxyStubDll();
	RegisterAutomationTlb();

	hr = CoCreateInstance(CLSID_MyMath, NULL, CLSCTX_INPROC_SERVER, IID_IMyMath, (void**)&pCube);
	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("Automation Component for Cube() can not be created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		
	}
}

void RegisterAutomationServer()
{
	GetSystemDirectory(szWinSysDir, 255);

	wsprintf(szPath, TEXT("%s\\regsvr32.exe /s %s\\%s"), szWinSysDir, PathOfClassFactoryDll, NameofAutomationDll);
	if (CreateProcess(NULL, szPath, NULL, NULL, TRUE, NULL, NULL, NULL, &si, &pi) != 0)
	{
		WaitForSingleObject(pi.hProcess, INFINITE);
		CloseHandle(pi.hThread);
		CloseHandle(pi.hProcess);
	}
	else
	{
		MessageBox(NULL, TEXT("CreateProcess() failed"), TEXT("Error"), MB_OK | MB_TOPMOST);
	}
}

void UnRegisterAutomationServer()
{
	GetSystemDirectory(szWinSysDir, 255);

	wsprintf(szPath, TEXT("%s\\regsvr32.exe -u  /s %s\\%s"), szWinSysDir, PathOfClassFactoryDll, NameofAutomationDll);
	if (CreateProcess(NULL, szPath, NULL, NULL, TRUE, NULL, NULL, NULL, &si, &pi) != 0)
	{
		WaitForSingleObject(pi.hProcess, INFINITE);
		CloseHandle(pi.hThread);
		CloseHandle(pi.hProcess);
	}
	else
	{
		MessageBox(NULL, TEXT("CreateProcess() failed"), TEXT("Error"), MB_OK | MB_TOPMOST);
	}
}

void RegisterAutomationProxyStubDll()
{
	GetSystemDirectory(szWinSysDir, 255);

	wsprintf(szPath, TEXT("%s\\regsvr32.exe /s %s\\%s"), szWinSysDir, szWinSysDir, NameofAutomationProxystubDll);
	if (CreateProcess(NULL, szPath, NULL, NULL, TRUE, NULL, NULL, NULL, &si, &pi) != 0)
	{
		WaitForSingleObject(pi.hProcess, INFINITE);
		CloseHandle(pi.hThread);
		CloseHandle(pi.hProcess);
	}
	else
	{
		MessageBox(NULL, TEXT("CreateProcess() failed"), TEXT("Error"), MB_OK | MB_TOPMOST);
	}
}

void UnRegisterAutomationProxyStubDll()
{
	GetSystemDirectory(szWinSysDir, 255);

	wsprintf(szPath, TEXT("%s\\regsvr32.exe -u /s  %s\\%s"), szWinSysDir, szWinSysDir, NameofAutomationProxystubDll);
	if (CreateProcess(NULL, szPath, NULL, NULL, TRUE, NULL, NULL, NULL, &si, &pi) != 0)
	{
		WaitForSingleObject(pi.hProcess, INFINITE);
		CloseHandle(pi.hThread);
		CloseHandle(pi.hProcess);
	}
	else
	{
		MessageBox(NULL, TEXT("CreateProcess() failed"), TEXT("Error"), MB_OK | MB_TOPMOST);
	}
}

void RegisterAutomationTlb()
{
	GetSystemDirectory(szWinSysDir, 255);

	//regasm /tlb path of tlb
	wsprintf(szPath, TEXT("%s %s\\%s"), TEXT("C:\\WINDOWS\\Microsoft.NET\\Framework\\v4.0.30319\\regtlibv12.exe"), szWinSysDir, NameofAutomationTlb);
	if (CreateProcess(NULL, szPath, NULL, NULL, TRUE, NULL, NULL, NULL, &si, &pi) != 0)
	{
		WaitForSingleObject(pi.hProcess, INFINITE);
		CloseHandle(pi.hThread);
		CloseHandle(pi.hProcess);
	}
	else
	{
		MessageBox(NULL, TEXT("CreateProcess() failed"), TEXT("Error"), MB_OK | MB_TOPMOST);
	}
}


void FreeAutomationPointers()
{
	void UnRegisterAutomationServer();
	void UnRegisterAutomationProxyStubDll();

	if (pCube!=NULL)
	{
		pCube->Release();
		pCube = NULL;
	}

	UnRegisterAutomationServer();
	UnRegisterAutomationProxyStubDll();
}

void Cube(int n1,int *res)
{
	pCube->Cube(n1,res);
}

void CubeRoot(double n1,double *res)
{
	pCube->CubeRoot(n1,res);
}


