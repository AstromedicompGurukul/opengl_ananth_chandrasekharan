#pragma once
#include<Windows.h>

TCHAR szPath[255];

STARTUPINFO si;
PROCESS_INFORMATION pi;

TCHAR szWinSysDir[255];

TCHAR NameofExeServer[] = TEXT("ExeServer.exe");
TCHAR NameofExeServerProxy[] = TEXT("ExeServerProxyStub.dll");

TCHAR PathOfClassFactoryDll[] = TEXT("C:\\Users\\anant\\Desktop\\COM_Modified_1\\Dlls");

class IMathematics : public IUnknown
{
public:
	//ExeServer specific method
	virtual HRESULT __stdcall SinOfAngle(double Angle, double *pRes) = 0;
	virtual HRESULT __stdcall CosOfAngle(double Angle, double *pRes) = 0;
	virtual HRESULT __stdcall TanOfAngle(double Angle, double *pRes) = 0;
};

//CLSID of SinCosTan component  {26AAF3B2-26AF-4C5D-AA0C-EB3975186D2C}
const CLSID CLSID_SinCosTan = { 0x26aaf3b2, 0x26af, 0x4c5d,{ 0xaa, 0xc, 0xeb, 0x39, 0x75, 0x18, 0x6d, 0x2c } };

//IID of IMathematics Interface {E3A0542C-C080-4990-BE91-1D2DE8F1ACFD}
const IID IID_IMathematics = { 0xe3a0542c, 0xc080, 0x4990,{ 0xbe, 0x91, 0x1d, 0x2d, 0xe8, 0xf1, 0xac, 0xfd } };


