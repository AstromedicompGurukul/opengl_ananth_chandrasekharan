//#define UNICODE
#include<windows.h>
#include "ExeServer.h"

HRESULT hr;
IMathematics *pISin = NULL;
IMathematics *pICos = NULL;
IMathematics *pITan = NULL;

// DllMain
BOOL WINAPI DllMain(HANDLE hModule,DWORD dwReason,LPVOID lpReserved)
{
	void RegisterExeServer();
	void RegisterExeServerProxyStubDll();
	void UnRegisterExeServerProxyStubDll();

	// code
	switch(dwReason)
	{
	case DLL_PROCESS_ATTACH:
		hr = CoInitialize(NULL);
		if (FAILED(hr))
		{
			MessageBox(NULL,TEXT("COM Engine not initialized"),TEXT("Message"),MB_OK|MB_TOPMOST);
		}
		else
		{
			
		}
		RegisterExeServer();
		RegisterExeServerProxyStubDll();
		break;
	case DLL_PROCESS_DETACH:
		UnRegisterExeServerProxyStubDll();
		CoUninitialize();
		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	}
	return(TRUE);
}

void RegisterExeServer()
{
	GetSystemDirectory(szWinSysDir, 255);

	wsprintf(szPath, TEXT("%s\\%s REGSERVER"), PathOfClassFactoryDll, NameofExeServer);
	if (CreateProcess(NULL, szPath, NULL, NULL, TRUE, NULL, NULL, NULL, &si, &pi) != 0)
	{
		WaitForSingleObject(pi.hProcess, INFINITE);
		CloseHandle(pi.hThread);
		CloseHandle(pi.hProcess);
	}
	else
	{
		MessageBox(NULL, TEXT("CreateProcess() failed"), TEXT("Error"), MB_OK | MB_TOPMOST);
	}
}

void RegisterExeServerProxyStubDll()
{
	GetSystemDirectory(szWinSysDir, 255);

	wsprintf(szPath, TEXT("%s\\regsvr32.exe /s  %s\\%s"), szWinSysDir, szWinSysDir, NameofExeServerProxy);
	if (CreateProcess(NULL, szPath, NULL, NULL, TRUE, NULL, NULL, NULL, &si, &pi) != 0)
	{
		WaitForSingleObject(pi.hProcess, INFINITE);
		CloseHandle(pi.hThread);
		CloseHandle(pi.hProcess);
	}
	else
	{
		MessageBox(NULL, TEXT("CreateProcess() failed"), TEXT("Error"), MB_OK | MB_TOPMOST);
	}
}

void UnRegisterExeServerProxyStubDll()
{
	GetSystemDirectory(szWinSysDir, 255);

	wsprintf(szPath, TEXT("%s\\regsvr32.exe -u /s  %s\\%s"), szWinSysDir, szWinSysDir, NameofExeServerProxy);
	if (CreateProcess(NULL, szPath, NULL, NULL, TRUE, NULL, NULL, NULL, &si, &pi) != 0)
	{
		WaitForSingleObject(pi.hProcess, INFINITE);
		CloseHandle(pi.hThread);
		CloseHandle(pi.hProcess);
	}
	else
	{
		MessageBox(NULL, TEXT("CreateProcess() failed"), TEXT("Error"), MB_OK | MB_TOPMOST);
	}
}


void LoadExeServer()
{
	void RegisterExeServer();
	void RegisterExeServerProxyStubDll();

	hr = CoCreateInstance(CLSID_SinCosTan, NULL, CLSCTX_LOCAL_SERVER, IID_IMathematics, (void**)&pISin);
	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("ExeServer Component for Sin() can not be created"), TEXT("Error"), MB_OK|MB_TOPMOST);
		exit(0);
	}
	else
	{

	}
}

void FreeExeServerPointers()
{
	void UnRegisterExeServerProxyStubDll();

	if (pISin!=NULL)
	{
		pISin->Release();
		pISin = NULL;
	}

	UnRegisterExeServerProxyStubDll();
}

void Sin(double n1,double *res)
{
	pISin->SinOfAngle(n1,res);
}

void Cos(double n1,double *res)
{
	pISin->CosOfAngle(n1,res);
}

void Tan(double n1,double *res)
{
	pISin->TanOfAngle(n1,res);
}

