#include <windows.h>
#include <stdio.h>

#include <d3d11.h>
#include <d3dcompiler.h>

#pragma warning (disable : 4838) // to supress warning of XNAMATH unsigned int to INT conversion
#include "XNAMATH\xnamath.h"
#include "Sphere.h"


#pragma comment(lib,"d3d11.lib")
#pragma comment(lib,"D3dcompiler.lib")
#pragma comment(lib,"Sphere.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

FILE *gpFile = NULL;
char gszLogFileName[]="Log.txt";

HWND ghwnd = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = {sizeof(WINDOWPLACEMENT)};

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;
bool bIsAkeyPressed;
bool bIsLkeyPressed;

float gClearColor[4];
IDXGISwapChain *gpIDXGISwapChain = NULL;
ID3D11Device *gpID3D11Device = NULL;
ID3D11DeviceContext *gpID3D11DeviceContext = NULL;
ID3D11RenderTargetView *gpID3D11RenderTargetView = NULL;

ID3D11VertexShader *gpID3D11VertexShader = NULL;
ID3D11PixelShader *gpID3D11PixelShader = NULL;
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Position_Sphere = NULL; // vertex position sphere(like VBO)
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Normal_Sphere = NULL; // vertex position sphere(like VBO)
ID3D11Buffer *gpID3D11Buffer_ConstantBuffer = NULL;  //for uniforms
ID3D11InputLayout *gpID3D11InputLayout =NULL;

ID3D11RasterizerState *gpID3D11RasterizerState = NULL;
ID3D11DepthStencilView *gpID3D11DepthStencilView = NULL;
ID3D11Buffer *gpID3D11Buffer_IndexBuffer = NULL; // elements buffer

bool gbLight;
bool gbToggle;

struct CBUFFER
{
	XMMATRIX ModelMatrix;
	XMMATRIX ViewMatrix;
	XMMATRIX ProjectionMatrix; 
	XMVECTOR u_La1;
	XMVECTOR u_Ld1;
	XMVECTOR u_Ls1;

	XMVECTOR u_La2;
	XMVECTOR u_Ld2;
	XMVECTOR u_Ls2;
	
	XMVECTOR u_La3;
	XMVECTOR u_Ld3;
	XMVECTOR u_Ls3;
	
	XMVECTOR u_Ka;
	XMVECTOR u_Kd;
	XMVECTOR u_Ks;
	
	float u_Material_shininess;
	
	XMVECTOR u_light_position1;
	XMVECTOR u_light_position2;
	XMVECTOR u_light_position3;
	
	unsigned int u_LKeyPressed;
	unsigned int u_Toggle;
};

XMMATRIX gPerspectiveProjectionMatrix;

int gNumVertices;
int gNumElements;

float sphere_vertices[1146];
float shpere_normals[1146];
float sphere_textures[764];
unsigned short shpere_elements[2280];

float lightAmbient1[] = {0.0f,0.0f,0.0f,0.0f};
float lightDiffuse1[]= {1.0f,0.0f,0.0f,0.0f};
float lightSpecular1[] = {1.0f,0.0f,0.0f,0.0f};
float lightPosition1[] = {0.0f,500.0f,-500.0f,1.0f};

float lightAmbient2[] = {0.0f,0.0f,0.0f,0.0f};
float lightDiffuse2[]= {0.0f,0.0f,1.0f,0.0f};
float lightSpecular2[] = {0.0f,0.0f,1.0f,0.0f};
float lightPosition2[] = {500.0f,500.0f,0.0f,1.0f};

float lightAmbient3[] = {0.0f,0.0f,0.0f,0.0f};
float lightDiffuse3[]= {0.0f,1.0f,0.0f,0.0f};
float lightSpecular3[] = {0.0f,1.0f,0.0f,0.0f};
float lightPosition3[] = {500.0f,0.0f,-500.0f,1.0f};

float material_Ambient[] = {0.0f,0.0f,0.0f,1.0f};
float material_Diffuse[]= {1.0f,1.0f,1.0f,1.0f};
float material_Specular[] = {1.0f,1.0f,1.0f,1.0f};
float material_shininess = 50.0f;

static float rotate_angle = 0.0f;

int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpszCmdLine,int iCmdShow)
{
	HRESULT initialize(void);
	void uninitialize(void);
	void display(void);

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[]= TEXT("Three Lights");
	bool bDone = false;


	if(fopen_s(&gpFile,gszLogFileName,"w") !=0)
	{
		MessageBox(NULL,TEXT("Log File Can Not be Created \n Exitting...."),TEXT("Error"),MB_OK|MB_TOPMOST|MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf_s(gpFile, "Log File Is Successfully Opened.\n");
		fclose(gpFile);
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW |CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL,IDC_ARROW);
	wndclass.lpszMenuName = NULL;
	wndclass.lpszClassName = szClassName;
	wndclass.hIconSm = LoadIcon(NULL,IDI_APPLICATION);

	RegisterClassEx(&wndclass);

	hwnd = CreateWindow(szClassName,
		TEXT("Three Lights"),
		WS_OVERLAPPEDWINDOW,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd,iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	HRESULT hr;
	hr = initialize();
	if(FAILED(hr))
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "initialize() Failed. Exitting Now...\n");
		fclose(gpFile);
		hwnd = NULL;
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "initialize() Succeeded.\n");
		fclose(gpFile);
	}

	while(bDone == false)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			display();

			if(gbActiveWindow == true)
			{
				if(gbEscapeKeyIsPressed ==true)
					bDone = true;
			}
		}
	}

	uninitialize();

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd,UINT iMsg,WPARAM wParam,LPARAM lParam)
{
	HRESULT resize(int,int);
	void ToggleFulscreen(void);
	void uninitialize(void);

	HRESULT hr;

	switch(iMsg)
	{
		case WM_ACTIVATE:
			if(HIWORD(wParam) == 0)
				gbActiveWindow = true;
			else
				gbActiveWindow = false;
			break;
		case WM_ERASEBKGND:
			return(0);
		case WM_SIZE:
			if(gpID3D11DeviceContext)
			{
				hr = resize(LOWORD(lParam),HIWORD(lParam));
				if(FAILED(hr))
				{
					fopen_s(&gpFile,gszLogFileName,"a+");
					fprintf_s(gpFile, "resize() Failed. Exitting Now...\n");
					fclose(gpFile);
					return(hr);
				}
				else
				{
					fopen_s(&gpFile,gszLogFileName,"a+");
					fprintf_s(gpFile, "resize() Succeeded\n");
					fclose(gpFile);
				}
			}
			break;
		case WM_KEYDOWN:
			switch(wParam)
			{
				case 0x51:
					if(gbEscapeKeyIsPressed == false)
						gbEscapeKeyIsPressed = true;
					break;
				case VK_ESCAPE: 
					if(gbFullscreen == false)
					{
						ToggleFulscreen();
						gbFullscreen = true;
					}
					else
					{
						ToggleFulscreen();
						gbFullscreen = false;
					}
					break;
				case 0x46: //f or F
					if (gbToggle == true)
						gbToggle = false;
					break;
				case 0x56: //v or V
					if (gbToggle == false)
						gbToggle = true;
					break;	
				case 0x4C:  //L or l
					if (bIsLkeyPressed == false)
					{
						gbLight = true;
						bIsLkeyPressed = true;
					}
					else
					{
						gbLight = false;
						bIsLkeyPressed = false;
					}
					break;

				default:
					break;
			}
			break;
		case WM_LBUTTONDOWN:
			break;
		case WM_CLOSE:
			uninitialize();
			break;
		case WM_DESTROY:
			PostQuitMessage(0);
		default:
			break;
	}

	return(DefWindowProc(hwnd,iMsg,wParam,lParam));
}


void ToggleFulscreen(void)
{
	MONITORINFO mi;

	if(gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd,GWL_STYLE);
		if(dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = {sizeof(MONITORINFO)};
			if(GetWindowPlacement(ghwnd,&wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY),&mi))
			{
				SetWindowLong(ghwnd,GWL_STYLE,dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(ghwnd,HWND_TOP,mi.rcMonitor.left,mi.rcMonitor.top,mi.rcMonitor.right - mi.rcMonitor.left,mi.rcMonitor.bottom - mi.rcMonitor.top,SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd,&wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_NOMOVE| SWP_NOSIZE| SWP_NOOWNERZORDER|SWP_NOZORDER|SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

HRESULT initialize(void)
{
	void uninitialize(void);
	HRESULT resize(int,int);

	HRESULT hr;
	D3D_DRIVER_TYPE d3dDriverType;
	D3D_DRIVER_TYPE d3dDriverTypes[] = {
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_WARP,
		D3D_DRIVER_TYPE_REFERENCE,
	};

	D3D_FEATURE_LEVEL d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_10_0;

	UINT createDeviceFlags = 0;
	UINT numDriverTypes = 0;
	UINT numFeatureLevels = 1;

	numDriverTypes = sizeof(d3dDriverTypes)/sizeof(d3dDriverTypes[0]);

	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
	ZeroMemory((void *)&dxgiSwapChainDesc,sizeof(DXGI_SWAP_CHAIN_DESC));

	dxgiSwapChainDesc.BufferCount =1;
	dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;
	dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
	dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	dxgiSwapChainDesc.OutputWindow = ghwnd;
	dxgiSwapChainDesc.SampleDesc.Count = 1;
	dxgiSwapChainDesc.SampleDesc.Quality = 0;
	dxgiSwapChainDesc.Windowed = TRUE;

	for(UINT driverTypeIndex = 0;driverTypeIndex < numDriverTypes;driverTypeIndex++)
	{
		d3dDriverType = d3dDriverTypes[driverTypeIndex];
		hr = D3D11CreateDeviceAndSwapChain(
			NULL,
			d3dDriverType,
			NULL,
			createDeviceFlags,
			&d3dFeatureLevel_required,
			numFeatureLevels,
			D3D11_SDK_VERSION,
			&dxgiSwapChainDesc,
			&gpIDXGISwapChain,
			&gpID3D11Device,
			&d3dFeatureLevel_acquired,
			&gpID3D11DeviceContext);

		if(SUCCEEDED(hr))
		{
			break;
		}
	}

	if(FAILED(hr))
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain() Failed. Exitting Now...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain() Succeeded\n");
		fprintf_s(gpFile, "The Chosen Driver is of ");
		if(d3dDriverType == D3D_DRIVER_TYPE_HARDWARE)
		{
			fprintf_s(gpFile, "Hardware Type \n");
		}
		else if(d3dDriverType == D3D_DRIVER_TYPE_WARP)
		{
			fprintf_s(gpFile, "WARP Type \n");
		}
		else if(d3dDriverType == D3D_DRIVER_TYPE_REFERENCE)
		{
			fprintf_s(gpFile, "Reference Type \n");
		}
		else
		{
			fprintf_s(gpFile, "Unknown Type \n");
		}

		fprintf_s(gpFile, "The Supported Highest Feature Level is ");
		if(d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_11_0)
		{
			fprintf_s(gpFile, "11.0 \n");
		}
		else if(d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_0)
		{
			fprintf_s(gpFile, "10.0\n");
		}
		else
		{
			fprintf_s(gpFile, "Unknown.\n");
		}
		fclose(gpFile);
	}


	//initialize shader, input layout and constant buffers

	const char *vertexShaderSourceCode =
		"cbuffer ConstantBuffer"\
		"{"\
		"float4x4 modelMatrix;"\
		"float4x4 viewMatrix;"\
		"float4x4 projectionMatrix;"\
		
		"float4 u_La1;"\
		"float4 u_Ld1;"\
		"float4 u_Ls1;"\

		"float4 u_La2;"\
		"float4 u_Ld2;"\
		"float4 u_Ls2;"\

		"float4 u_La3;"\
		"float4 u_Ld3;"\
		"float4 u_Ls3;"\

		"float4 u_Ka;"\
		"float4 u_Kd;"\
		"float4 u_Ks;"\
		"float u_Material_shininess;"\

		"float4 u_light_position1;"\
		"float4 u_light_position2;"\
		"float4 u_light_position3;"\

		"uint u_LKeyPressed;"\
		"uint u_toggle;"\
		"}"\
		"struct vertex_output"\
		"{"\
		"float4 position:SV_POSITION;"\
		"float3 transformed_normals : NORMAL0;"\
		"float3 light_direction1 : NORMAL1;"\
		"float3 light_direction2 : NORMAL2;"\
		"float3 light_direction3 : NORMAL3;"\
		"float3 viewer_vector : NORMAL4;"\
		"float4 color : COLOR;"\
		"};"\
		"vertex_output main(float4 pos:POSITION,float3 normal:NORMAL)"\
		"{"\
		"vertex_output output;"\
		"if(u_toggle == 1)"\
		"{"\
			"if(u_LKeyPressed == 1)"\
			"{"\
				"float4 eyeCoordinates = mul(modelMatrix,pos);"\
				"eyeCoordinates = mul(viewMatrix,eyeCoordinates);"\
				"float3 transformed_normals = normalize(mul((float3x3)mul(modelMatrix,viewMatrix),(float3)normal));"\

					"float3 light_direction1 = (float3) normalize(u_light_position1 - eyeCoordinates);"\
					"float tn_dot_ld1 = max(dot(transformed_normals,light_direction1),0.0);"\
					"float3 ambient1 = u_La1 * u_Ka;"\
					"float3 diffuse1 = u_Ld1 * u_Kd * tn_dot_ld1;"\
					"float3 reflection_vector1 = reflect(-light_direction1,transformed_normals);"\
					"float3 viewer_vector1 = normalize(-eyeCoordinates.xyz);"\
					"float3 specular1 = u_Ls1 * u_Ks * pow(max(dot(reflection_vector1,viewer_vector1),0.0),u_Material_shininess);"\

					"float3 light_direction2 = (float3) normalize(u_light_position2 - eyeCoordinates);"\
					"float tn_dot_ld2 = max(dot(transformed_normals,light_direction2),0.0);"\
					"float3 ambient2 = u_La2 * u_Ka;"\
					"float3 diffuse2 = u_Ld2 * u_Kd * tn_dot_ld2;"\
					"float3 reflection_vector2 = reflect(-light_direction2,transformed_normals);"\
					"float3 viewer_vector2 = normalize(-eyeCoordinates.xyz);"\
					"float3 specular2 = u_Ls2 * u_Ks * pow(max(dot(reflection_vector2,viewer_vector2),0.0),u_Material_shininess);"\

					"float3 light_direction3 = (float3) normalize(u_light_position3 - eyeCoordinates);"\
					"float tn_dot_ld3 = max(dot(transformed_normals,light_direction3),0.0);"\
					"float3 ambient3 = u_La3 * u_Ka;"\
					"float3 diffuse3 = u_Ld3 * u_Kd * tn_dot_ld3;"\
					"float3 reflection_vector3 = reflect(-light_direction3,transformed_normals);"\
					"float3 viewer_vector3 = normalize(-eyeCoordinates.xyz);"\
					"float3 specular3 = u_Ls3 * u_Ks * pow(max(dot(reflection_vector3,viewer_vector3),0.0),u_Material_shininess);"\

					"float3 ambient = ambient1 + ambient2 + ambient3;"\
					"float3 diffuse = diffuse1 + diffuse2 + diffuse3;"\
					"float3 specular = specular1 + specular2 + specular3;"\

				"output.color = float4(ambient + diffuse + specular,1.0);"\
			"}"\
			"else"\
			"{"\
				"output.color = float4(1.0,1.0,1.0,1.0);"\
			"}"\
		"}"\
		"else"\
		"{"\
			"float4 eyeCoordinates = mul(modelMatrix,pos);"\
			"eyeCoordinates = mul(viewMatrix,eyeCoordinates);"\
			"output.transformed_normals = mul((float3x3)mul(modelMatrix,viewMatrix),(float3)normal);"\
			"output.light_direction1 = (float3)(u_light_position1 - eyeCoordinates);"\
			"output.light_direction2 = (float3)(u_light_position2 - eyeCoordinates);"\
			"output.light_direction3 = (float3)(u_light_position3 - eyeCoordinates);"\
			"output.viewer_vector = -eyeCoordinates.xyz;"\
		"}"\
		"float4 world = mul(modelMatrix , pos);"\
		"float4 worldView = mul(viewMatrix , world);"\
		"output.position = mul(projectionMatrix , worldView);"\
		"return (output);"\
		"}";

	ID3DBlob *pID3DBlob_VertexShaderCode = NULL;
	ID3DBlob *pID3DBlob_Error = NULL;

	hr = D3DCompile(vertexShaderSourceCode,  //shader source code
		lstrlenA(vertexShaderSourceCode)+1,  // length of source code as it does not consider length with null termination add 1 to it.
		"VS",   //shader stage
		NULL,	//This is D3D_SHADER_MACRO used when there is #define is there in shader
		D3D_COMPILE_STANDARD_FILE_INCLUDE, //Import standard files
		"main", //entry point function in  shader
		"vs_5_0", //feature level of shader
		0,	//compiler constants like debug,optimise,validation etc..
		0,	//effects constants like debug,optimise,validation etc..
		&pID3DBlob_VertexShaderCode,   //blob to hold intermediate bytecode
		&pID3DBlob_Error);	//blob to hold error code if any.
	if(FAILED(hr))
	{
		if(pID3DBlob_Error !=NULL)
		{
			fopen_s(&gpFile,gszLogFileName,"a+");
			fprintf_s(gpFile, "D3DCompile() Failed. For Vertex Shader: %s\n",(char *)pID3DBlob_Error->GetBufferPointer());
			fclose(gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return(hr);		
		}
		else
		{
			fopen_s(&gpFile,gszLogFileName,"a+");
			fprintf_s(gpFile, "D3DCompile() Failed. Due to COM Error Exitting Now...\n");
			fclose(gpFile);
			return(hr);
		}
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "D3DCompile() Succeeded For Vertex Shader.\n");
		fclose(gpFile);
	}


	hr = gpID3D11Device->CreateVertexShader(pID3DBlob_VertexShaderCode->GetBufferPointer(),
		pID3DBlob_VertexShaderCode->GetBufferSize(),
		NULL, 		//it is for class linkage for variable used accross shaders
		&gpID3D11VertexShader);

	if(FAILED(hr))
	{
		pID3DBlob_VertexShaderCode->Release();
		pID3DBlob_VertexShaderCode = NULL;
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "CreateVertexShader() Failed For Vertex Shader. Exitting Now...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "CreateVertexShader() Succeeded For Vertex Shader.\n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShader,
		0, 	//Linkage objects array
		0);	//elements in linkage object array


	const char *pixelShaderSourceCode =
		"cbuffer ConstantBuffer"\
		"{"\
		"float4x4 modelMatrix;"\
		"float4x4 viewMatrix;"\
		"float4x4 projectionMatrix;"\
		"float4 u_La1;"\
		"float4 u_Ld1;"\
		"float4 u_Ls1;"\

		"float4 u_La2;"\
		"float4 u_Ld2;"\
		"float4 u_Ls2;"\

		"float4 u_La3;"\
		"float4 u_Ld3;"\
		"float4 u_Ls3;"\

		"float4 u_Ka;"\
		"float4 u_Kd;"\
		"float4 u_Ks;"\
		"float u_Material_shininess;"\

		"float4 u_light_position1;"\
		"float4 u_light_position2;"\
		"float4 u_light_position3;"\

		"uint u_LKeyPressed;"\
		"uint u_toggle;"\
		"}"\
		"struct vertex_output"\
		"{"\
		"float4 position:SV_POSITION;"\
		"float3 transformed_normals : NORMAL0;"\
		"float3 light_direction1 : NORMAL1;"\
		"float3 light_direction2 : NORMAL2;"\
		"float3 light_direction3 : NORMAL3;"\
		"float3 viewer_vector : NORMAL4;"\
		"float4 color : COLOR;"\
		"};"\
		"float4 main(float4 pos:SV_POSITION,vertex_output input):SV_TARGET"\
		"{"\
		"float4 phong_ads_color;"\
		"if(u_toggle == 1)"
		"{"\
			"phong_ads_color = input.color;"\
		"}"\
		"else"\
		"{"\
			"if(u_LKeyPressed == 1)"\
			"{"\
				"float3 normalized_transformed_normals = normalize(input.transformed_normals);"\
				"float3 normalized_viewer_vector = normalize(input.viewer_vector);"\

				"float3 normalized_light_direction1 = normalize(input.light_direction1);"\
				"float3 normalized_light_direction2 = normalize(input.light_direction2);"\
				"float3 normalized_light_direction3 = normalize(input.light_direction3);"\
				
				"float tn_dot_ld1 = max(dot(normalized_transformed_normals,normalized_light_direction1),0.0);"\
				"float3 ambient1 = u_La1 * u_Ka;"\
				"float3 diffuse1 = u_Ld1 * u_Kd * tn_dot_ld1;"\
				"float3 reflection_vector1 = reflect(-normalized_light_direction1,normalized_transformed_normals);"\
				"float3 specular1 = u_Ls1 * u_Ks * pow(max(dot(reflection_vector1,normalized_viewer_vector),0.0),u_Material_shininess);"\

				"float tn_dot_ld2 = max(dot(normalized_transformed_normals,normalized_light_direction2),0.0);"\
				"float3 ambient2 = u_La2 * u_Ka;"\
				"float3 diffuse2 = u_Ld2 * u_Kd * tn_dot_ld2;"\
				"float3 reflection_vector2 = reflect(-normalized_light_direction2,normalized_transformed_normals);"\
				"float3 specular2 = u_Ls2 * u_Ks * pow(max(dot(reflection_vector2,normalized_viewer_vector),0.0),u_Material_shininess);"\

				"float tn_dot_ld3 = max(dot(normalized_transformed_normals,normalized_light_direction3),0.0);"\
				"float3 ambient3 = u_La3 * u_Ka;"\
				"float3 diffuse3 = u_Ld3 * u_Kd * tn_dot_ld3;"\
				"float3 reflection_vector3 = reflect(-normalized_light_direction3,normalized_transformed_normals);"\
				"float3 specular3 = u_Ls3 * u_Ks * pow(max(dot(reflection_vector3,normalized_viewer_vector),0.0),u_Material_shininess);"\

				"float3 ambient = ambient1 + ambient2 + ambient3;"\
				"float3 diffuse = diffuse1 + diffuse2 + diffuse3;"\
				"float3 specular = specular1 + specular2 + specular3;"\

				"phong_ads_color = float4(ambient + diffuse + specular,1.0);"\
			"}"\
			"else"\
			"{"\
				"phong_ads_color = float4(1.0,1.0,1.0,1.0);"\
			"}"\
		"}"\
		"return(phong_ads_color);"\
		"}";

	ID3DBlob *pID3DBlob_PixelShaderCode = NULL;
	pID3DBlob_Error = NULL;

	hr = D3DCompile(pixelShaderSourceCode,  //shader source code
		lstrlenA(pixelShaderSourceCode)+1,  // length of source code as it does not consider length with null termination add 1 to it.
		"PS",   //shader stage
		NULL,	//This is D3D_SHADER_MACRO used when there is #define is there in shader
		D3D_COMPILE_STANDARD_FILE_INCLUDE, //Import standard files
		"main", //entry point function in  shader
		"ps_5_0", //feature level of shader
		0,	//compiler constants like debug,optimise,validation etc..
		0,	//effects constants like debug,optimise,validation etc..
		&pID3DBlob_PixelShaderCode,   //blob to hold intermediate bytecode
		&pID3DBlob_Error);	//blob to hold error code if any.
	
	if(FAILED(hr))
	{
		if(pID3DBlob_Error !=NULL)
		{
			pID3DBlob_VertexShaderCode->Release();
			pID3DBlob_VertexShaderCode = NULL;
			fopen_s(&gpFile,gszLogFileName,"a+");
			fprintf_s(gpFile, "D3DCompile() Failed. For Pixel Shader: %s\n",(char *)pID3DBlob_Error->GetBufferPointer());
			fclose(gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return(hr);		
		}
		else
		{
			pID3DBlob_VertexShaderCode->Release();
			pID3DBlob_VertexShaderCode = NULL;
			fopen_s(&gpFile,gszLogFileName,"a+");
			fprintf_s(gpFile, "D3DCompile() Failed For Pixel Shader. Due to COM Error Exitting Now...\n");
			fclose(gpFile);
			return(hr);
		}
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "D3DCompile() Succeeded For Pixel Shader.\n");
		fclose(gpFile);
	}


	hr = gpID3D11Device->CreatePixelShader(pID3DBlob_PixelShaderCode->GetBufferPointer(),
		pID3DBlob_PixelShaderCode->GetBufferSize(),
		NULL, 		//it is for class linkage for variable used accross shaders
		&gpID3D11PixelShader);

	if(FAILED(hr))
	{
		pID3DBlob_VertexShaderCode->Release();
		pID3DBlob_VertexShaderCode = NULL;
		pID3DBlob_PixelShaderCode->Release();
		pID3DBlob_PixelShaderCode = NULL;
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "CreatePixelShader() Failed For Pixel Shader. Exitting Now...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "CreatePixelShader() Succeeded For Pixel Shader.\n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->PSSetShader(gpID3D11PixelShader,
		0, 	//Linkage objects array
		0);	//elements in linkage object array

	
	//code  for input layout
	D3D11_INPUT_ELEMENT_DESC inputElementDesc[2];
	inputElementDesc[0].SemanticName = "POSITION";
	inputElementDesc[0].SemanticIndex = 0;
	inputElementDesc[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	inputElementDesc[0].InputSlot = 0;
	inputElementDesc[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputElementDesc[0].AlignedByteOffset = 0;
	inputElementDesc[0].InstanceDataStepRate = 0;

	inputElementDesc[1].SemanticName = "NORMAL";
	inputElementDesc[1].SemanticIndex = 0;
	inputElementDesc[1].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	inputElementDesc[1].InputSlot = 1;
	inputElementDesc[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputElementDesc[1].AlignedByteOffset = 0;
	inputElementDesc[1].InstanceDataStepRate = 0;


	hr = gpID3D11Device->CreateInputLayout(inputElementDesc,//array of input layout
		2, //number of elements in an array
		pID3DBlob_VertexShaderCode->GetBufferPointer(),
		pID3DBlob_VertexShaderCode->GetBufferSize(),
		&gpID3D11InputLayout
	);
	
	if(FAILED(hr))
	{
		pID3DBlob_VertexShaderCode->Release();
		pID3DBlob_VertexShaderCode = NULL;
		pID3DBlob_PixelShaderCode->Release();
		pID3DBlob_PixelShaderCode = NULL;
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "ID3D11Device::CreateInputLayout() Failed. Exitting Now...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "ID3D11Device::CreateInputLayout() Succeeded.\n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->IASetInputLayout(gpID3D11InputLayout);

	pID3DBlob_VertexShaderCode->Release();
	pID3DBlob_VertexShaderCode = NULL;
	pID3DBlob_PixelShaderCode->Release();
	pID3DBlob_PixelShaderCode = NULL;


 	D3D11_BUFFER_DESC bufferDesc;
	D3D11_MAPPED_SUBRESOURCE mappedSubResource;	

	getSphereVertexData(sphere_vertices, shpere_normals, sphere_textures, shpere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();


 	ZeroMemory(&bufferDesc,sizeof(D3D11_BUFFER_DESC));
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc.ByteWidth = sizeof(float) * ARRAYSIZE(sphere_vertices);
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	
	hr = gpID3D11Device->CreateBuffer(&bufferDesc,
		nullptr,
		&gpID3D11Buffer_VertexBuffer_Position_Sphere);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Failed For Color Buffer Vertex Shader. Exitting Now...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Succeeded For Color Buffer Vertex Shader.\n");
		fclose(gpFile);
	}

	ZeroMemory(&mappedSubResource, sizeof(D3D11_MAPPED_SUBRESOURCE));
	gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Position_Sphere, 0,
		D3D11_MAP_WRITE_DISCARD, 0, &mappedSubResource);
	memcpy(mappedSubResource.pData, sphere_vertices, sizeof(sphere_vertices));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Position_Sphere, 0);


	
 	ZeroMemory(&bufferDesc,sizeof(D3D11_BUFFER_DESC));
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc.ByteWidth = sizeof(float) * ARRAYSIZE(shpere_normals);
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	
	hr = gpID3D11Device->CreateBuffer(&bufferDesc,
		nullptr,
		&gpID3D11Buffer_VertexBuffer_Normal_Sphere);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Failed For Color Buffer Vertex Shader. Exitting Now...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Succeeded For Color Buffer Vertex Shader.\n");
		fclose(gpFile);
	}

	ZeroMemory(&mappedSubResource, sizeof(D3D11_MAPPED_SUBRESOURCE));
	gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Normal_Sphere, 0,
		D3D11_MAP_WRITE_DISCARD, 0, &mappedSubResource);
	memcpy(mappedSubResource.pData, shpere_normals, sizeof(shpere_normals));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Normal_Sphere, 0);

	//elements

 	ZeroMemory(&bufferDesc,sizeof(D3D11_BUFFER_DESC));
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc.ByteWidth = sizeof(float) * ARRAYSIZE(shpere_elements);
	bufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	
	hr = gpID3D11Device->CreateBuffer(&bufferDesc,
		nullptr,
		&gpID3D11Buffer_IndexBuffer);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Failed For Color Buffer Vertex Shader. Exitting Now...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Succeeded For Color Buffer Vertex Shader.\n");
		fclose(gpFile);
	}

	ZeroMemory(&mappedSubResource, sizeof(D3D11_MAPPED_SUBRESOURCE));
	gpID3D11DeviceContext->Map(gpID3D11Buffer_IndexBuffer, 0,
		D3D11_MAP_WRITE_DISCARD, 0, &mappedSubResource);
	memcpy(mappedSubResource.pData, shpere_elements, sizeof(shpere_elements));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_IndexBuffer, 0);


	//now define and set constant buuffers
	D3D11_BUFFER_DESC bufferDesc_ConstantBuffer;
	ZeroMemory(&bufferDesc_ConstantBuffer,sizeof(D3D11_BUFFER_DESC));
	bufferDesc_ConstantBuffer.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc_ConstantBuffer.ByteWidth = sizeof(CBUFFER);
	bufferDesc_ConstantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

	hr = gpID3D11Device->CreateBuffer(&bufferDesc_ConstantBuffer,
		nullptr,
		&gpID3D11Buffer_ConstantBuffer);

	if(FAILED(hr))
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Failed For Constant Buffer. Exitting Now...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Succeeded For Constant Buffer.\n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->VSSetConstantBuffers(0,	//where i see this buffer in my shader
		1,	//how many buffers you have
		&gpID3D11Buffer_ConstantBuffer);

	gpID3D11DeviceContext->PSSetConstantBuffers(0,	//where i see this buffer in my shader
		1,	//how many buffers you have
		&gpID3D11Buffer_ConstantBuffer);


	gClearColor[0] = 0.0f;
	gClearColor[1] = 0.0f;
	gClearColor[2] = 0.0f;
	gClearColor[3] = 0.0f;

	gPerspectiveProjectionMatrix = XMMatrixIdentity();

	D3D11_RASTERIZER_DESC rasterizerDesc;
	ZeroMemory(&rasterizerDesc, sizeof(D3D11_RASTERIZER_DESC));

	rasterizerDesc.AntialiasedLineEnable = FALSE;
	rasterizerDesc.CullMode = D3D11_CULL_NONE;
	rasterizerDesc.DepthBias = 0;
	rasterizerDesc.MultisampleEnable = FALSE;
	rasterizerDesc.DepthBiasClamp = 0.0f;
	rasterizerDesc.DepthClipEnable = TRUE;
	rasterizerDesc.SlopeScaledDepthBias = 0.0f;
	rasterizerDesc.FillMode = D3D11_FILL_SOLID;
	rasterizerDesc.FrontCounterClockwise = FALSE;
	rasterizerDesc.ScissorEnable = FALSE;

	gpID3D11Device->CreateRasterizerState(&rasterizerDesc, &gpID3D11RasterizerState);

	gpID3D11DeviceContext->RSSetState(gpID3D11RasterizerState);

	hr =resize(WIN_WIDTH, WIN_HEIGHT);
	if(FAILED(hr))
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "resize() Failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "resize() Succeeded.\n");
		fclose(gpFile);
	}
	
	gbLight = false;
	return(S_OK);
}

HRESULT resize(int width,int height)
{
	HRESULT hr = S_OK;

	if (gpID3D11DepthStencilView)
	{
		gpID3D11DepthStencilView->Release();
		gpID3D11DepthStencilView = NULL;
	}

	if(gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	gpIDXGISwapChain->ResizeBuffers(1,width,height,DXGI_FORMAT_R8G8B8A8_UNORM,0);

	ID3D11Texture2D *pID3D11Texture2D_BackBuffer;
	gpIDXGISwapChain->GetBuffer(0,__uuidof(ID3D11Texture2D),(LPVOID *)&pID3D11Texture2D_BackBuffer);

	hr = gpID3D11Device->CreateRenderTargetView(pID3D11Texture2D_BackBuffer,NULL,&gpID3D11RenderTargetView);
	if(FAILED(hr))
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "ID3D11Device::CreateRenderTargetView() Failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "ID3D11Device::CreateRenderTargetView() Succeeded.\n");
		fclose(gpFile);
	}

	pID3D11Texture2D_BackBuffer->Release();
	pID3D11Texture2D_BackBuffer= NULL;

	//create depth buffer and set it to render target view

	//first fill the texture descriptor which is used to create  texture which we will use it as buffer.

	D3D11_TEXTURE2D_DESC textureDesc;
	ZeroMemory(&textureDesc, sizeof(D3D11_TEXTURE2D_DESC));

	textureDesc.ArraySize = 1;
	textureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	textureDesc.CPUAccessFlags = 0;
	textureDesc.Format = DXGI_FORMAT_D32_FLOAT;
	textureDesc.Height = height;
	textureDesc.Width = width;
	textureDesc.MipLevels = 1;
	textureDesc.MiscFlags = 0;
	textureDesc.SampleDesc.Count = 1;
	textureDesc.SampleDesc.Quality = 0;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;

	ID3D11Texture2D *pID3D11Texture2D_DepthBuffer = NULL;

	hr = gpID3D11Device->CreateTexture2D(&textureDesc, NULL, &pID3D11Texture2D_DepthBuffer);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateTexture2D() Failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateTexture2D() Succeeded.\n");
		fclose(gpFile);
	}

	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	ZeroMemory(&depthStencilViewDesc, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));

	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;
	depthStencilViewDesc.Format = DXGI_FORMAT_D32_FLOAT;

	hr = gpID3D11Device->CreateDepthStencilView(pID3D11Texture2D_DepthBuffer, &depthStencilViewDesc, &gpID3D11DepthStencilView);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateDepthStencilView() Failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateDepthStencilView() Succeeded.\n");
		fclose(gpFile);
	}

	pID3D11Texture2D_DepthBuffer->Release();
	pID3D11Texture2D_DepthBuffer = NULL;

	gpID3D11DeviceContext->OMSetRenderTargets(1,&gpID3D11RenderTargetView,gpID3D11DepthStencilView);


	D3D11_VIEWPORT d3dViewPort;
	d3dViewPort.TopLeftX = 0;
	d3dViewPort.TopLeftY = 0;
	d3dViewPort.Width = (float)width;
	d3dViewPort.Height = (float)height;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;

	gpID3D11DeviceContext->RSSetViewports(1,&d3dViewPort);


	if (width >= height)
		gPerspectiveProjectionMatrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0f), (float)width / (float)height, 0.1f, 100.0f);
	else
		gPerspectiveProjectionMatrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0f), (float)height / (float)width, 0.1f, 100.0f);
	return(hr);
}

void update(void)
{
	rotate_angle = rotate_angle + 0.0009f;
	if(rotate_angle >=360.0f)
		rotate_angle -= 360.0f;
}

void display(void)
{
	void update(void);
	gpID3D11DeviceContext->ClearRenderTargetView(gpID3D11RenderTargetView, gClearColor);
	gpID3D11DeviceContext->ClearDepthStencilView(gpID3D11DepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);

	UINT stride = sizeof(float) * 3;
	UINT offset = 0;

	XMMATRIX worldMatrix = XMMatrixIdentity();
	XMMATRIX viewMatrix = XMMatrixIdentity();
	XMMATRIX wvMatrix = XMMatrixIdentity();

	CBUFFER constantBuffer;


	// draw cube
	gpID3D11DeviceContext->IASetVertexBuffers(0,//input slot
		1,  //how many buffers you have
		&gpID3D11Buffer_VertexBuffer_Position_Sphere,
		&stride,
		&offset);


	gpID3D11DeviceContext->IASetVertexBuffers(1,//input slot
		1,  //how many buffers you have
		&gpID3D11Buffer_VertexBuffer_Normal_Sphere,
		&stride,
		&offset);
	
	//index buffer
	gpID3D11DeviceContext->IASetIndexBuffer(gpID3D11Buffer_IndexBuffer, DXGI_FORMAT_R16_UINT, 0);
	
		//select geometry primitive
	gpID3D11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	worldMatrix = XMMatrixIdentity();
	viewMatrix = XMMatrixIdentity();
	
	worldMatrix =  XMMatrixTranslation(0.0f, 0.0f, 2.0f);
	

	ZeroMemory(&constantBuffer, sizeof(CBUFFER));
	constantBuffer.ModelMatrix = worldMatrix;
	constantBuffer.ViewMatrix = viewMatrix;
	constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix;

	constantBuffer.u_La1 = XMVectorSet(lightAmbient1[0], lightAmbient1[1], lightAmbient1[2], lightAmbient1[3]);
	constantBuffer.u_Ld1 = XMVectorSet(lightDiffuse1[0], lightDiffuse1[1], lightDiffuse1[2], lightDiffuse1[3]);
	constantBuffer.u_Ls1 = XMVectorSet(lightSpecular1[0], lightSpecular1[1], lightSpecular1[2], lightSpecular1[3]);

	constantBuffer.u_La2 = XMVectorSet(lightAmbient2[0], lightAmbient2[1], lightAmbient2[2], lightAmbient2[3]);
	constantBuffer.u_Ld2 = XMVectorSet(lightDiffuse2[0], lightDiffuse2[1], lightDiffuse2[2], lightDiffuse2[3]);
	constantBuffer.u_Ls2 = XMVectorSet(lightSpecular2[0], lightSpecular2[1], lightSpecular2[2], lightSpecular2[3]);

	constantBuffer.u_La3 = XMVectorSet(lightAmbient3[0], lightAmbient3[1], lightAmbient3[2], lightAmbient3[3]);
	constantBuffer.u_Ld3 = XMVectorSet(lightDiffuse3[0], lightDiffuse3[1], lightDiffuse3[2], lightDiffuse3[3]);
	constantBuffer.u_Ls3 = XMVectorSet(lightSpecular3[0], lightSpecular3[1], lightSpecular3[2], lightSpecular3[3]);

	constantBuffer.u_Ka = XMVectorSet(material_Ambient[0], material_Ambient[1], material_Ambient[2], material_Ambient[3]);
	constantBuffer.u_Kd = XMVectorSet(material_Diffuse[0], material_Diffuse[1], material_Diffuse[2], material_Diffuse[3]);
	constantBuffer.u_Ks = XMVectorSet(material_Specular[0], material_Specular[1], material_Specular[2], material_Specular[3]);

	constantBuffer.u_Material_shininess = material_shininess;
	
	lightPosition1[1] = 1000.0f * cos(rotate_angle);
	lightPosition1[2] = 1000.0f * sin(rotate_angle);
	constantBuffer.u_light_position1 = XMVectorSet(lightPosition1[0], lightPosition1[1], -lightPosition1[2], lightPosition1[3]);
	
	lightPosition2[0] = 1000.0f * cos(rotate_angle);
	lightPosition2[1] = 1000.0f * sin(rotate_angle);
	constantBuffer.u_light_position2 = XMVectorSet(lightPosition2[0], lightPosition2[1], -lightPosition2[2], lightPosition2[3]);
	

	lightPosition3[0] = 1000.0f * cos(rotate_angle);
	lightPosition3[2] = 1000.0f * sin(rotate_angle);
	constantBuffer.u_light_position3 = XMVectorSet(lightPosition3[0], lightPosition3[1], -lightPosition3[2], lightPosition3[3]);
	
	if (gbLight == true)
	{
		constantBuffer.u_LKeyPressed = 1;
	}
	else
	{
		constantBuffer.u_LKeyPressed = 0;
	}
	if (gbToggle == true)
	{
		constantBuffer.u_Toggle = 1;
	}
	else
	{
		constantBuffer.u_Toggle = 0;
	}
	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer,
		0,	//index in shader
		NULL, //D3D_BOX bounding box compute shader cube of data
		&constantBuffer,
		0, //width of face of bounding box
		0 );	//depth of bounding box	

	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);



	gpIDXGISwapChain->Present(0,0);
	
	update();

}

void uninitialize(void)
{
	if (gpID3D11Buffer_ConstantBuffer)
	{
		gpID3D11Buffer_ConstantBuffer->Release();
		gpID3D11Buffer_ConstantBuffer = NULL;
	}

	if (gpID3D11InputLayout)
	{
		gpID3D11InputLayout->Release();
		gpID3D11InputLayout = NULL;
	}

	if (gpID3D11Buffer_IndexBuffer)
	{
		gpID3D11Buffer_IndexBuffer->Release();
		gpID3D11Buffer_IndexBuffer = NULL;
	}


	if (gpID3D11Buffer_VertexBuffer_Position_Sphere)
	{
		gpID3D11Buffer_VertexBuffer_Position_Sphere->Release();
		gpID3D11Buffer_VertexBuffer_Position_Sphere = NULL;
	}

	if (gpID3D11Buffer_VertexBuffer_Normal_Sphere)
	{
		gpID3D11Buffer_VertexBuffer_Normal_Sphere->Release();
		gpID3D11Buffer_VertexBuffer_Normal_Sphere = NULL;
	}

	if (gpID3D11RasterizerState)
	{
		gpID3D11RasterizerState->Release();
		gpID3D11RasterizerState = NULL;
	}

	if (gpID3D11PixelShader)
	{
		gpID3D11PixelShader->Release();
		gpID3D11PixelShader = NULL;
	}

	if (gpID3D11VertexShader)
	{
		gpID3D11VertexShader->Release();
		gpID3D11VertexShader = NULL;
	}

	if (gpID3D11DepthStencilView)
	{
		gpID3D11DepthStencilView->Release();
		gpID3D11DepthStencilView = NULL;
	}

	
	if(gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;	
	}

	if(gpIDXGISwapChain)
	{
		gpIDXGISwapChain->Release();
		gpIDXGISwapChain = NULL;	
	}

	if(gpID3D11DeviceContext)
	{
		gpID3D11DeviceContext->Release();
		gpID3D11DeviceContext = NULL;	
	}

	if(gpID3D11Device)
	{
		gpID3D11Device->Release();
		gpID3D11Device = NULL;	
	}

	if(gpFile)
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "uninitialize Succeeded\n");
		fprintf_s(gpFile, "Log File Closed Successfully\n");
		fclose(gpFile);	
	}
}