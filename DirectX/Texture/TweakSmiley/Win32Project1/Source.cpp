#include <windows.h>
#include <stdio.h>

#include <d3d11.h>
#include <d3dcompiler.h>

#pragma warning (disable : 4838) 
#include "XNAMATH\xnamath.h"
#include "WICTextureLoader.h"

#pragma comment(lib,"d3d11.lib")
#pragma comment(lib,"D3dcompiler.lib")
#pragma comment(lib,"DirectXTK.lib") 

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#define checkImageWidth 64
#define checkImageHeight 64

byte checkImage[checkImageHeight][checkImageWidth][4];


LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

FILE *gpFile = NULL;
char gszLogFileName[]="Log.txt";

HWND ghwnd = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = {sizeof(WINDOWPLACEMENT)};

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

float gClearColor[4];
IDXGISwapChain *gpIDXGISwapChain = NULL;
ID3D11Device *gpID3D11Device = NULL;
ID3D11DeviceContext *gpID3D11DeviceContext = NULL;
ID3D11RenderTargetView *gpID3D11RenderTargetView = NULL;

ID3D11VertexShader *gpID3D11VertexShader = NULL;
ID3D11PixelShader *gpID3D11PixelShader = NULL;
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Position_Square = NULL; 
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Texture_Square = NULL; 
ID3D11Buffer *gpID3D11Buffer_ConstantBuffer = NULL;  
ID3D11InputLayout *gpID3D11InputLayout =NULL;

ID3D11RasterizerState *gpID3D11RasterizerState = NULL;
ID3D11DepthStencilView *gpID3D11DepthStencilView = NULL;

ID3D11ShaderResourceView *gpID3D11ShaderResourceView_Texture_Square = NULL;
ID3D11ShaderResourceView *gpID3D11ShaderResourceView_White_Texture = NULL;
ID3D11SamplerState *gpID3D11SamplerState_Texture_Square = NULL;

int gPressedKey;

struct CBUFFER
{
	XMMATRIX WorldViewProjectionMatrix; 
};

XMMATRIX gPerspectiveProjectionMatrix;


int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpszCmdLine,int iCmdShow)
{
	HRESULT initialize(void);
	void uninitialize(void);
	void display(void);

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[]= TEXT("Shree Ganesha : TweakSmiley");
	bool bDone = false;


	if(fopen_s(&gpFile,gszLogFileName,"w") !=0)
	{
		MessageBox(NULL,TEXT("Log file not created"),TEXT("Error"),MB_OK|MB_TOPMOST|MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf_s(gpFile, "Log file is opened.\n");
		fclose(gpFile);
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW |CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL,IDC_ARROW);
	wndclass.lpszMenuName = NULL;
	wndclass.lpszClassName = szClassName;
	wndclass.hIconSm = LoadIcon(NULL,IDI_APPLICATION);

	RegisterClassEx(&wndclass);

	hwnd = CreateWindow(szClassName,
		TEXT("Shree Ganesha : Tweaksmiley"),
		WS_OVERLAPPEDWINDOW,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd,iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	HRESULT hr;
	hr = initialize();
	if(FAILED(hr))
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "initialize() failed\n");
		fclose(gpFile);
		hwnd = NULL;
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "initialize() done\n");
		fclose(gpFile);
	}

	while(bDone == false)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			display();

			if(gbActiveWindow == true)
			{
				if(gbEscapeKeyIsPressed ==true)
					bDone = true;
			}
		}
	}

	uninitialize();

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd,UINT iMsg,WPARAM wParam,LPARAM lParam)
{
	HRESULT resize(int,int);
	void ToggleFulscreen(void);
	void uninitialize(void);

	HRESULT hr;

	switch(iMsg)
	{
		case WM_ACTIVATE:
			if(HIWORD(wParam) == 0)
				gbActiveWindow = true;
			else
				gbActiveWindow = false;
			break;
		case WM_ERASEBKGND:
			return(0);
		case WM_SIZE:
			if(gpID3D11DeviceContext)
			{
				hr = resize(LOWORD(lParam),HIWORD(lParam));
				if(FAILED(hr))
				{
					fopen_s(&gpFile,gszLogFileName,"a+");
					fprintf_s(gpFile, "resize() failed.\n");
					fclose(gpFile);
					return(hr);
				}
				else
				{
					fopen_s(&gpFile,gszLogFileName,"a+");
					fprintf_s(gpFile, "resize() done\n");
					fclose(gpFile);
				}
			}
			break;
		case WM_KEYDOWN:
			switch(wParam)
			{
				case VK_ESCAPE:
					if(gbEscapeKeyIsPressed == false)
						gbEscapeKeyIsPressed = true;
					break;
				case 0x46: //F or f
					if(gbFullscreen == false)
					{
						ToggleFulscreen();
						gbFullscreen = true;
					}
					else
					{
						ToggleFulscreen();
						gbFullscreen = false;
					}
					break;
				case 0x30:
					gPressedKey = 0;
				break;
				case 0x31:
					gPressedKey = 1;
				break;
				case 0x32:
					gPressedKey = 2;
				break;
				case 0x33:
					gPressedKey = 3;
				break;
				case 0x34:
					gPressedKey = 4;
				break;
				case 0x35:
					gPressedKey = 5;
				break;
				default:
					break;
			}
			break;
		case WM_LBUTTONDOWN:
			break;
		case WM_CLOSE:
			uninitialize();
			break;
		case WM_DESTROY:
			PostQuitMessage(0);
		default:
			break;
	}

	return(DefWindowProc(hwnd,iMsg,wParam,lParam));
}


void ToggleFulscreen(void)
{
	MONITORINFO mi;

	if(gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd,GWL_STYLE);
		if(dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = {sizeof(MONITORINFO)};
			if(GetWindowPlacement(ghwnd,&wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY),&mi))
			{
				SetWindowLong(ghwnd,GWL_STYLE,dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(ghwnd,HWND_TOP,mi.rcMonitor.left,mi.rcMonitor.top,mi.rcMonitor.right - mi.rcMonitor.left,mi.rcMonitor.bottom - mi.rcMonitor.top,SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd,&wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_NOMOVE| SWP_NOSIZE| SWP_NOOWNERZORDER|SWP_NOZORDER|SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

HRESULT initialize(void)
{
	void uninitialize(void);
	HRESULT resize(int,int);
	HRESULT LoadD3DTexture(const wchar_t *, ID3D11ShaderResourceView **);
	HRESULT LoadTextureFromMemory(ID3D11ShaderResourceView **ppID3D11ShaderResourceView);

	HRESULT hr;
	D3D_DRIVER_TYPE d3dDriverType;
	D3D_DRIVER_TYPE d3dDriverTypes[] = {
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_WARP,
		D3D_DRIVER_TYPE_REFERENCE,
	};

	D3D_FEATURE_LEVEL d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_10_0;

	UINT createDeviceFlags = 0;
	UINT numDriverTypes = 0;
	UINT numFeatureLevels = 1;

	numDriverTypes = sizeof(d3dDriverTypes)/sizeof(d3dDriverTypes[0]);

	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
	ZeroMemory((void *)&dxgiSwapChainDesc,sizeof(DXGI_SWAP_CHAIN_DESC));

	dxgiSwapChainDesc.BufferCount =1;
	dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;
	dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
	dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	dxgiSwapChainDesc.OutputWindow = ghwnd;
	dxgiSwapChainDesc.SampleDesc.Count = 4;
	dxgiSwapChainDesc.SampleDesc.Quality = 0;
	dxgiSwapChainDesc.Windowed = TRUE;

	for(UINT driverTypeIndex = 0;driverTypeIndex < numDriverTypes;driverTypeIndex++)
	{
		d3dDriverType = d3dDriverTypes[driverTypeIndex];
		hr = D3D11CreateDeviceAndSwapChain(
			NULL,
			d3dDriverType,
			NULL,
			createDeviceFlags,
			&d3dFeatureLevel_required,
			numFeatureLevels,
			D3D11_SDK_VERSION,
			&dxgiSwapChainDesc,
			&gpIDXGISwapChain,
			&gpID3D11Device,
			&d3dFeatureLevel_acquired,
			&gpID3D11DeviceContext);

		if(SUCCEEDED(hr))
		{
			break;
		}
	}

	if(FAILED(hr))
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain() failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain() done\n");
		fprintf_s(gpFile, "The Chosen Driver is  ");
		if(d3dDriverType == D3D_DRIVER_TYPE_HARDWARE)
		{
			fprintf_s(gpFile, "Hardware Type \n");
		}
		else if(d3dDriverType == D3D_DRIVER_TYPE_WARP)
		{
			fprintf_s(gpFile, "WARP Type \n");
		}
		else if(d3dDriverType == D3D_DRIVER_TYPE_REFERENCE)
		{
			fprintf_s(gpFile, "Reference Type \n");
		}
		else
		{
			fprintf_s(gpFile, "Unknown Type \n");
		}

		fprintf_s(gpFile, "The Supported Highest Feature Level is ");
		if(d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_11_0)
		{
			fprintf_s(gpFile, "11.0 \n");
		}
		else if(d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_0)
		{
			fprintf_s(gpFile, "10.0\n");
		}
		else
		{
			fprintf_s(gpFile, "Unknown.\n");
		}
		fclose(gpFile);
	}

	const char *vertexShaderSourceCode = 
	"cbuffer ConstantBuffer"\
	"{"\
	"float4x4 worldViewProjectionMatrix;"
	"}"\
	"struct vertex_output"\
	"{"\
	"float4 position:SV_POSITION;"\
	"float2 texcoord : TEXCOORD;"\
	"};"\
	"vertex_output main(float4 pos:POSITION,float2 texcoord:TEXCOORD)"\
	"{"\
	"vertex_output output;"\
	"output.position = mul(worldViewProjectionMatrix , pos);"\
	"output.texcoord = texcoord;"\
	"return (output);"\
	"}";

	ID3DBlob *pID3DBlob_VertexShaderCode = NULL;
	ID3DBlob *pID3DBlob_Error = NULL;

	hr = D3DCompile(vertexShaderSourceCode,  
		lstrlenA(vertexShaderSourceCode)+1,  
		"VS",   
		NULL,	
		D3D_COMPILE_STANDARD_FILE_INCLUDE, 
		"main", 
		"vs_5_0", 
		0,	
		0,	
		&pID3DBlob_VertexShaderCode,   
		&pID3DBlob_Error);	
	if(FAILED(hr))
	{
		if(pID3DBlob_Error !=NULL)
		{
			fopen_s(&gpFile,gszLogFileName,"a+");
			fprintf_s(gpFile, "D3DCompile() Failed. For Vertex Shader: %s\n",(char *)pID3DBlob_Error->GetBufferPointer());
			fclose(gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return(hr);		
		}
		else
		{
			fopen_s(&gpFile,gszLogFileName,"a+");
			fprintf_s(gpFile, "D3DCompile() Failed. Due to COM Error Exitting Now...\n");
			fclose(gpFile);
			return(hr);
		}
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "D3DCompile() Succeeded For Vertex Shader.\n");
		fclose(gpFile);
	}


	hr = gpID3D11Device->CreateVertexShader(pID3DBlob_VertexShaderCode->GetBufferPointer(),
		pID3DBlob_VertexShaderCode->GetBufferSize(),
		NULL, 		//it is for class linkage for variable used accross shaders
		&gpID3D11VertexShader);

	if(FAILED(hr))
	{
		pID3DBlob_VertexShaderCode->Release();
		pID3DBlob_VertexShaderCode = NULL;
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "CreateVertexShader() Failed For Vertex Shader. Exitting Now...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "CreateVertexShader() Succeeded For Vertex Shader.\n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShader,
		0, 	//Linkage objects array
		0);	//elements in linkage object array


	const char *pixelShaderSourceCode =
		"Texture2D myTexture2D;"\
		"SamplerState mySamplerState;"\
		"float4 main(float4 pos:SV_POSITION,float2 texcoord:TEXCOORD):SV_TARGET"\
		"{"\
		"float4 color = myTexture2D.Sample(mySamplerState,texcoord);"\
		"return(color);"\
		"}";

	ID3DBlob *pID3DBlob_PixelShaderCode = NULL;
	pID3DBlob_Error = NULL;

	hr = D3DCompile(pixelShaderSourceCode,  //shader source code
		lstrlenA(pixelShaderSourceCode)+1,  // length of source code as it does not consider length with null termination add 1 to it.
		"PS",   //shader stage
		NULL,	//This is D3D_SHADER_MACRO used when there is #define is there in shader
		D3D_COMPILE_STANDARD_FILE_INCLUDE, //Import standard files
		"main", //entry point function in  shader
		"ps_5_0", //feature level of shader
		0,	//compiler constants like debug,optimise,validation etc..
		0,	//effects constants like debug,optimise,validation etc..
		&pID3DBlob_PixelShaderCode,   //blob to hold intermediate bytecode
		&pID3DBlob_Error);	//blob to hold error code if any.
	
	if(FAILED(hr))
	{
		if(pID3DBlob_Error !=NULL)
		{
			pID3DBlob_VertexShaderCode->Release();
			pID3DBlob_VertexShaderCode = NULL;
			fopen_s(&gpFile,gszLogFileName,"a+");
			fprintf_s(gpFile, "D3DCompile() Failed. For Pixel Shader: %s\n",(char *)pID3DBlob_Error->GetBufferPointer());
			fclose(gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return(hr);		
		}
		else
		{
			pID3DBlob_VertexShaderCode->Release();
			pID3DBlob_VertexShaderCode = NULL;
			fopen_s(&gpFile,gszLogFileName,"a+");
			fprintf_s(gpFile, "D3DCompile() Failed For Pixel Shader. Due to COM Error Exitting Now...\n");
			fclose(gpFile);
			return(hr);
		}
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "D3DCompile() Succeeded For Pixel Shader.\n");
		fclose(gpFile);
	}


	hr = gpID3D11Device->CreatePixelShader(pID3DBlob_PixelShaderCode->GetBufferPointer(),
		pID3DBlob_PixelShaderCode->GetBufferSize(),
		NULL, 		//it is for class linkage for variable used accross shaders
		&gpID3D11PixelShader);

	if(FAILED(hr))
	{
		pID3DBlob_VertexShaderCode->Release();
		pID3DBlob_VertexShaderCode = NULL;
		pID3DBlob_PixelShaderCode->Release();
		pID3DBlob_PixelShaderCode = NULL;
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "CreatePixelShader() Failed For Pixel Shader. Exitting Now...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "CreatePixelShader() Succeeded For Pixel Shader.\n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->PSSetShader(gpID3D11PixelShader,
		0, 	//Linkage objects array
		0);	//elements in linkage object array

	
	//code  for input layout
	D3D11_INPUT_ELEMENT_DESC inputElementDesc[2];
	inputElementDesc[0].SemanticName = "POSITION";
	inputElementDesc[0].SemanticIndex = 0;
	inputElementDesc[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	inputElementDesc[0].InputSlot = 0;
	inputElementDesc[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputElementDesc[0].AlignedByteOffset = 0;
	inputElementDesc[0].InstanceDataStepRate = 0;

	inputElementDesc[1].SemanticName = "TEXCOORD";
	inputElementDesc[1].SemanticIndex = 0;
	inputElementDesc[1].Format = DXGI_FORMAT_R32G32_FLOAT;
	inputElementDesc[1].InputSlot = 1;
	inputElementDesc[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputElementDesc[1].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	inputElementDesc[1].InstanceDataStepRate = 0;


	hr = gpID3D11Device->CreateInputLayout(inputElementDesc,//array of input layout
		2, //number of elements in an array
		pID3DBlob_VertexShaderCode->GetBufferPointer(),
		pID3DBlob_VertexShaderCode->GetBufferSize(),
		&gpID3D11InputLayout
	);
	
	if(FAILED(hr))
	{
		pID3DBlob_VertexShaderCode->Release();
		pID3DBlob_VertexShaderCode = NULL;
		pID3DBlob_PixelShaderCode->Release();
		pID3DBlob_PixelShaderCode = NULL;
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "ID3D11Device::CreateInputLayout() Failed. Exitting Now...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "ID3D11Device::CreateInputLayout() Succeeded.\n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->IASetInputLayout(gpID3D11InputLayout);

	pID3DBlob_VertexShaderCode->Release();
	pID3DBlob_VertexShaderCode = NULL;
	pID3DBlob_PixelShaderCode->Release();
	pID3DBlob_PixelShaderCode = NULL;

	
	//square VBO
	float vertices_square[]
	{
		-1.0f,+1.0f,-1.0f,
		+1.0f,+1.0f,-1.0f,
		-1.0f,-1.0f,-1.0f,

		-1.0f,-1.0f,-1.0f,
		+1.0f,+1.0f,-1.0f,
		+1.0f,-1.0f,-1.0f
 	};
	D3D11_BUFFER_DESC bufferDesc;
 	ZeroMemory(&bufferDesc,sizeof(D3D11_BUFFER_DESC));
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc.ByteWidth = sizeof(float) * ARRAYSIZE(vertices_square);
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	
	hr = gpID3D11Device->CreateBuffer(&bufferDesc,
		nullptr,
		&gpID3D11Buffer_VertexBuffer_Position_Square);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Failed For Color Buffer Vertex Shader. Exitting Now...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Succeeded For Color Buffer Vertex Shader.\n");
		fclose(gpFile);
	}
	D3D11_MAPPED_SUBRESOURCE mappedSubResource;
	ZeroMemory(&mappedSubResource, sizeof(D3D11_MAPPED_SUBRESOURCE));
	gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Position_Square, 0,
		D3D11_MAP_WRITE_DISCARD, 0, &mappedSubResource);
	memcpy(mappedSubResource.pData, vertices_square, sizeof(vertices_square));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Position_Square, 0);

	float texcoords_square[]
	{
		0.0f,0.0f,
		0.0f,1.0f,
		1.0f,0.0f,

		1.0f,0.0f,
		0.0f,1.0f,
		1.0f,1.0f,
 	};

 	ZeroMemory(&bufferDesc,sizeof(D3D11_BUFFER_DESC));
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc.ByteWidth = sizeof(float) * ARRAYSIZE(texcoords_square);
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	
	hr = gpID3D11Device->CreateBuffer(&bufferDesc,
		nullptr,
		&gpID3D11Buffer_VertexBuffer_Texture_Square);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Failed For Color Buffer Vertex Shader. Exitting Now...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Succeeded For Color Buffer Vertex Shader.\n");
		fclose(gpFile);
	}

	ZeroMemory(&mappedSubResource, sizeof(D3D11_MAPPED_SUBRESOURCE));
	gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Texture_Square, 0,
		D3D11_MAP_WRITE_DISCARD, 0, &mappedSubResource);
	memcpy(mappedSubResource.pData, texcoords_square, sizeof(texcoords_square));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Texture_Square, 0);

	D3D11_BUFFER_DESC bufferDesc_ConstantBuffer;
	ZeroMemory(&bufferDesc_ConstantBuffer,sizeof(D3D11_BUFFER_DESC));
	bufferDesc_ConstantBuffer.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc_ConstantBuffer.ByteWidth = sizeof(CBUFFER);
	bufferDesc_ConstantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

	hr = gpID3D11Device->CreateBuffer(&bufferDesc_ConstantBuffer,
		nullptr,
		&gpID3D11Buffer_ConstantBuffer);

	if(FAILED(hr))
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Failed For Constant Buffer. Exitting Now...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Succeeded For Constant Buffer.\n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->VSSetConstantBuffers(0,	
		1,	
		&gpID3D11Buffer_ConstantBuffer);


	gClearColor[0] = 0.0f;
	gClearColor[1] = 0.0f;
	gClearColor[2] = 0.0f;
	gClearColor[3] = 0.0f;

	gPerspectiveProjectionMatrix = XMMatrixIdentity();

	D3D11_RASTERIZER_DESC rasterizerDesc;
	ZeroMemory(&rasterizerDesc, sizeof(D3D11_RASTERIZER_DESC));

	rasterizerDesc.AntialiasedLineEnable = FALSE;
	rasterizerDesc.CullMode = D3D11_CULL_NONE;
	rasterizerDesc.DepthBias = 0;
	rasterizerDesc.MultisampleEnable = FALSE;
	rasterizerDesc.DepthBiasClamp = 0.0f;
	rasterizerDesc.DepthClipEnable = TRUE;
	rasterizerDesc.SlopeScaledDepthBias = 0.0f;
	rasterizerDesc.FillMode = D3D11_FILL_SOLID;
	rasterizerDesc.FrontCounterClockwise = FALSE;
	rasterizerDesc.ScissorEnable = FALSE;

	gpID3D11Device->CreateRasterizerState(&rasterizerDesc, &gpID3D11RasterizerState);

	gpID3D11DeviceContext->RSSetState(gpID3D11RasterizerState);

	hr = LoadD3DTexture(L"Smiley.bmp", &gpID3D11ShaderResourceView_Texture_Square);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "LoadD3DTexture() Failed For quad Texture.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "LoadD3DTexture() Succeeded For quad Texture.\n");
		fclose(gpFile);
	}

	hr = LoadTextureFromMemory(&gpID3D11ShaderResourceView_White_Texture);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "LoadTextureFromMemory() Failed For quad Texture.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "LoadTextureFromMemory() Succeeded For quad Texture.\n");
		fclose(gpFile);
	}



	D3D11_SAMPLER_DESC samplerDesc;
	ZeroMemory(&samplerDesc, sizeof(D3D11_SAMPLER_DESC));
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;

	hr = gpID3D11Device->CreateSamplerState(&samplerDesc, &gpID3D11SamplerState_Texture_Square);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateSamplerState() Failed For Cube Texture.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateSamplerState() Succeeded For Cube Texture.\n");
		fclose(gpFile);
	}


	hr =resize(WIN_WIDTH, WIN_HEIGHT);
	if(FAILED(hr))
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "resize() Failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "resize() Succeeded.\n");
		fclose(gpFile);
	}

	return(S_OK);
}

void MakeCheckImage(void)
{
	int i;

	for (i = 0; i<checkImageHeight; i++)
	{
		for (int j = 0; j < checkImageWidth; j++)
		{
			
			checkImage[i][j][0] = (byte)255;
			checkImage[i][j][1] = (byte)255;
			checkImage[i][j][2] = (byte)255;
			checkImage[i][j][3] = (byte)255;
		}
	}
}

HRESULT LoadD3DTexture(const wchar_t *textureFileName, ID3D11ShaderResourceView **ppID3D11ShaderResourceView)
{
	HRESULT hr;

	hr = DirectX::CreateWICTextureFromFile(gpID3D11Device, gpID3D11DeviceContext, textureFileName, NULL, ppID3D11ShaderResourceView);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "CreateWICTextureFromFile() failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "CreateWICTextureFromFile() done.\n");
		fclose(gpFile);
	}
	return(hr);

}

HRESULT LoadTextureFromMemory(ID3D11ShaderResourceView **ppID3D11ShaderResourceView)
{
	MakeCheckImage();
	UINT imageBufferSize = checkImageHeight * checkImageWidth * 4;

	BITMAPFILEHEADER BitmapFileHeader;
	ZeroMemory(&BitmapFileHeader, sizeof(BITMAPFILEHEADER));
	BitmapFileHeader.bfOffBits = sizeof(BITMAPINFOHEADER) + sizeof(BITMAPFILEHEADER);
	BitmapFileHeader.bfSize = imageBufferSize + sizeof(BitmapFileHeader) + sizeof(BITMAPINFOHEADER);
	BitmapFileHeader.bfType = 0x4d42;     

	BITMAPINFOHEADER BitmapInfoHeader;
	ZeroMemory(&BitmapInfoHeader, sizeof(BITMAPINFOHEADER));
	BitmapInfoHeader.biBitCount = 32;     
	BitmapInfoHeader.biHeight = checkImageHeight;
	BitmapInfoHeader.biPlanes = 1;   
	BitmapInfoHeader.biSize = sizeof(BITMAPINFOHEADER);
	BitmapInfoHeader.biWidth = checkImageWidth;
	
	BITMAPINFO BitmapInfo;			
	ZeroMemory(&BitmapInfo, sizeof(BITMAPINFO));
	BitmapInfo.bmiHeader = BitmapInfoHeader;

	UINT sizeOfBitmap = sizeof(BitmapFileHeader) + sizeof(BitmapInfoHeader) + sizeof(checkImage);

	byte *bitmap = (BYTE*)malloc(sizeOfBitmap);

	memcpy_s(bitmap, sizeOfBitmap, &BitmapFileHeader, sizeof(BitmapFileHeader));
	memcpy_s(bitmap + sizeof(BitmapFileHeader) , sizeOfBitmap, &BitmapInfoHeader, sizeof(BitmapInfoHeader));
	memcpy_s(bitmap + sizeof(BitmapFileHeader) + sizeof(BitmapInfoHeader), sizeOfBitmap, checkImage, sizeof(checkImage));

	HRESULT hr;

	hr = DirectX::CreateWICTextureFromMemory(gpID3D11Device, gpID3D11DeviceContext,(const uint8_t*) bitmap,(size_t) sizeOfBitmap, NULL, ppID3D11ShaderResourceView,0);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "CreateWICTextureFromMemory() failed.\n");
		fclose(gpFile);
		return(hr);
	}

	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "CreateWICTextureFromMemory() done.\n");
		fclose(gpFile);
	}

	return (hr);
}	

HRESULT resize(int width,int height)
{
	HRESULT hr = S_OK;

	if (gpID3D11DepthStencilView)
	{
		gpID3D11DepthStencilView->Release();
		gpID3D11DepthStencilView = NULL;
	}

	if(gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	gpIDXGISwapChain->ResizeBuffers(1,width,height,DXGI_FORMAT_R8G8B8A8_UNORM,0);

	ID3D11Texture2D *pID3D11Texture2D_BackBuffer;
	gpIDXGISwapChain->GetBuffer(0,__uuidof(ID3D11Texture2D),(LPVOID *)&pID3D11Texture2D_BackBuffer);

	hr = gpID3D11Device->CreateRenderTargetView(pID3D11Texture2D_BackBuffer,NULL,&gpID3D11RenderTargetView);
	if(FAILED(hr))
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "ID3D11Device::CreateRenderTargetView() failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "ID3D11Device::CreateRenderTargetView() done.\n");
		fclose(gpFile);
	}

	pID3D11Texture2D_BackBuffer->Release();
	pID3D11Texture2D_BackBuffer= NULL;

	D3D11_TEXTURE2D_DESC textureDesc;
	ZeroMemory(&textureDesc, sizeof(D3D11_TEXTURE2D_DESC));

	textureDesc.ArraySize = 1;
	textureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	textureDesc.CPUAccessFlags = 0;
	textureDesc.Format = DXGI_FORMAT_D32_FLOAT;
	textureDesc.Height = height;
	textureDesc.Width = width;
	textureDesc.MipLevels = 1;
	textureDesc.MiscFlags = 0;
	textureDesc.SampleDesc.Count = 4;
	textureDesc.SampleDesc.Quality = 0;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;

	ID3D11Texture2D *pID3D11Texture2D_DepthBuffer = NULL;

	hr = gpID3D11Device->CreateTexture2D(&textureDesc, NULL, &pID3D11Texture2D_DepthBuffer);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateTexture2D() failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateTexture2D() done.\n");
		fclose(gpFile);
	}

	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	ZeroMemory(&depthStencilViewDesc, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));

	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;
	depthStencilViewDesc.Format = DXGI_FORMAT_D32_FLOAT;

	hr = gpID3D11Device->CreateDepthStencilView(pID3D11Texture2D_DepthBuffer, &depthStencilViewDesc, &gpID3D11DepthStencilView);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateDepthStencilView() failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateDepthStencilView() done.\n");
		fclose(gpFile);
	}

	pID3D11Texture2D_DepthBuffer->Release();
	pID3D11Texture2D_DepthBuffer = NULL;

	gpID3D11DeviceContext->OMSetRenderTargets(1,&gpID3D11RenderTargetView,gpID3D11DepthStencilView);


	D3D11_VIEWPORT d3dViewPort;
	d3dViewPort.TopLeftX = 0;
	d3dViewPort.TopLeftY = 0;
	d3dViewPort.Width = (float)width;
	d3dViewPort.Height = (float)height;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;

	gpID3D11DeviceContext->RSSetViewports(1,&d3dViewPort);


	if (width >= height)
		gPerspectiveProjectionMatrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0f), (float)width / (float)height, 0.1f, 100.0f);
	else
		gPerspectiveProjectionMatrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0f), (float)height / (float)width, 0.1f, 100.0f);
	return(hr);
}

void update(void)
{

}

void display(void)
{
	void update(void);
	gpID3D11DeviceContext->ClearRenderTargetView(gpID3D11RenderTargetView,gClearColor);
	gpID3D11DeviceContext->ClearDepthStencilView(gpID3D11DepthStencilView,D3D11_CLEAR_DEPTH,1.0f,0);


	XMMATRIX worldMatrix = XMMatrixIdentity();
	XMMATRIX viewMatrix = XMMatrixIdentity();
	XMMATRIX wvpMatrix = XMMatrixIdentity();

	//-----
	UINT stride = sizeof(float) * 3;
	UINT offset = 0;

	gpID3D11DeviceContext->IASetVertexBuffers(0,
		1,  
		&gpID3D11Buffer_VertexBuffer_Position_Square,
		&stride,
		&offset);

	stride = sizeof(float) * 2;

	float texcoords_square[12];
		
	if(gPressedKey ==0)
	{
		texcoords_square[0] = 1.0f;
		texcoords_square[1] = 0.0f;
		texcoords_square[2] = 0.0f;
		texcoords_square[3] = 0.0f;
		texcoords_square[4] = 1.0f;
		texcoords_square[5] = 1.0f;

		texcoords_square[6] = 1.0f;
		texcoords_square[7] = 1.0f;
		texcoords_square[8] = 0.0f;
		texcoords_square[9] = 0.0f;
		texcoords_square[10] = 0.0f;
		texcoords_square[11] = 1.0f;
	}
	else if(gPressedKey ==1)
	{
		texcoords_square[0] = 1.0f;
		texcoords_square[1] = 0.5f;
		texcoords_square[2] = 0.5f;
		texcoords_square[3] = 0.5f;
		texcoords_square[4] = 1.0f;
		texcoords_square[5] = 1.0f;

		texcoords_square[6] = 1.0f;
		texcoords_square[7] = 1.0f;
		texcoords_square[8] = 0.5f;
		texcoords_square[9] = 0.5f;
		texcoords_square[10] = 0.5f;
		texcoords_square[11] = 1.0f;
	}
	else if(gPressedKey ==2)
	{
		texcoords_square[0] = 1.0f;
		texcoords_square[1] = 0.0f;
		texcoords_square[2] = 0.0f;
		texcoords_square[3] = 0.0f;
		texcoords_square[4] = 1.0f;
		texcoords_square[5] = 1.0f;

		texcoords_square[6] = 1.0f;
		texcoords_square[7] = 1.0f;
		texcoords_square[8] = 0.0f;
		texcoords_square[9] = 0.0f;
		texcoords_square[10] = 0.0f;
		texcoords_square[11] = 1.0f;
	}
	else if(gPressedKey ==3)
	{
		texcoords_square[0] = 2.0f;
		texcoords_square[1] = 0.0f;
		texcoords_square[2] = 0.0f;
		texcoords_square[3] = 0.0f;
		texcoords_square[4] = 2.0f;
		texcoords_square[5] = 2.0f;

		texcoords_square[6] = 2.0f;
		texcoords_square[7] = 2.0f;
		texcoords_square[8] = 0.0f;
		texcoords_square[9] = 0.0f;
		texcoords_square[10] = 0.0f;
		texcoords_square[11] = 2.0f;
	}
	else if(gPressedKey ==4)
	{
		texcoords_square[0] = 0.5f;
		texcoords_square[1] = 0.5f;
		texcoords_square[2] = 0.5f;
		texcoords_square[3] = 0.5f;
		texcoords_square[4] = 0.5f;
		texcoords_square[5] = 0.5f;

		texcoords_square[6] = 0.5f;
		texcoords_square[7] = 0.5f;
		texcoords_square[8] = 0.5f;
		texcoords_square[9] = 0.5f;
		texcoords_square[10] = 0.5f;
		texcoords_square[11] = 0.5f;
	}
	else if(gPressedKey ==5)
	{
		texcoords_square[0] = 1.0f;
		texcoords_square[1] = 0.0f;
		texcoords_square[2] = 0.0f;
		texcoords_square[3] = 0.0f;
		texcoords_square[4] = 1.0f;
		texcoords_square[5] = 1.0f;

		texcoords_square[6] = 1.0f;
		texcoords_square[7] = 1.0f;
		texcoords_square[8] = 0.0f;
		texcoords_square[9] = 0.0f;
		texcoords_square[10] = 0.0f;
		texcoords_square[11] = 1.0f;
	}



	D3D11_BUFFER_DESC bufferDesc;

 	ZeroMemory(&bufferDesc,sizeof(D3D11_BUFFER_DESC));
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc.ByteWidth = sizeof(float) * ARRAYSIZE(texcoords_square);
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	
	HRESULT hr = gpID3D11Device->CreateBuffer(&bufferDesc,
		nullptr,
		&gpID3D11Buffer_VertexBuffer_Texture_Square);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() failed For Texture Buffer\n");
		fclose(gpFile);
		
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() done For Texture Buffer.\n");
		fclose(gpFile);
	}
	D3D11_MAPPED_SUBRESOURCE mappedSubResource;
	
	ZeroMemory(&mappedSubResource, sizeof(D3D11_MAPPED_SUBRESOURCE));
	gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Texture_Square, 0,
		D3D11_MAP_WRITE_DISCARD, 0, &mappedSubResource);
	memcpy(mappedSubResource.pData, texcoords_square, sizeof(texcoords_square));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Texture_Square, 0);


	gpID3D11DeviceContext->IASetVertexBuffers(1,
		1,  
		&gpID3D11Buffer_VertexBuffer_Texture_Square,
		&stride,
		&offset);

	if(gPressedKey != 5)
	{
		gpID3D11DeviceContext->PSSetShaderResources(0, 1, &gpID3D11ShaderResourceView_Texture_Square);
	}	
	else
	{
		gpID3D11DeviceContext->PSSetShaderResources(0, 1, &gpID3D11ShaderResourceView_White_Texture);
	}
	gpID3D11DeviceContext->PSSetSamplers(0, 1, &gpID3D11SamplerState_Texture_Square);

	//select geometry primitive
	gpID3D11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	
	worldMatrix =  XMMatrixTranslation(0.0f, 0.0f, 4.0f);
	wvpMatrix = worldMatrix * viewMatrix * gPerspectiveProjectionMatrix;
	
	CBUFFER constantBuffer;
	ZeroMemory(&constantBuffer, sizeof(CBUFFER));
	constantBuffer.WorldViewProjectionMatrix = wvpMatrix;
	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer,
		0,	
		NULL,
		&constantBuffer,
		0, 
		0 );	

	gpID3D11DeviceContext->Draw(6,0);

	gpIDXGISwapChain->Present(0,0);
	update();
}

void uninitialize(void)
{

	if (gpID3D11SamplerState_Texture_Square)
	{
		gpID3D11SamplerState_Texture_Square->Release();
		gpID3D11SamplerState_Texture_Square = NULL;
	}

	if (gpID3D11ShaderResourceView_Texture_Square)
	{
		gpID3D11ShaderResourceView_Texture_Square->Release();
		gpID3D11ShaderResourceView_Texture_Square = NULL;
	}

	if (gpID3D11ShaderResourceView_White_Texture)
	{
		gpID3D11ShaderResourceView_White_Texture->Release();
		gpID3D11ShaderResourceView_White_Texture = NULL;
	}

	if (gpID3D11Buffer_ConstantBuffer)
	{
		gpID3D11Buffer_ConstantBuffer->Release();
		gpID3D11Buffer_ConstantBuffer = NULL;
	}

	if (gpID3D11InputLayout)
	{
		gpID3D11InputLayout->Release();
		gpID3D11InputLayout = NULL;
	}


	if (gpID3D11Buffer_VertexBuffer_Position_Square)
	{
		gpID3D11Buffer_VertexBuffer_Position_Square->Release();
		gpID3D11Buffer_VertexBuffer_Position_Square = NULL;
	}

	if (gpID3D11Buffer_VertexBuffer_Texture_Square)
	{
		gpID3D11Buffer_VertexBuffer_Texture_Square->Release();
		gpID3D11Buffer_VertexBuffer_Texture_Square = NULL;
	}

	if (gpID3D11RasterizerState)
	{
		gpID3D11RasterizerState->Release();
		gpID3D11RasterizerState = NULL;
	}

	if (gpID3D11PixelShader)
	{
		gpID3D11PixelShader->Release();
		gpID3D11PixelShader = NULL;
	}

	if (gpID3D11VertexShader)
	{
		gpID3D11VertexShader->Release();
		gpID3D11VertexShader = NULL;
	}

	if (gpID3D11DepthStencilView)
	{
		gpID3D11DepthStencilView->Release();
		gpID3D11DepthStencilView = NULL;
	}

	
	if(gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;	
	}

	if(gpIDXGISwapChain)
	{
		gpIDXGISwapChain->Release();
		gpIDXGISwapChain = NULL;	
	}

	if(gpID3D11DeviceContext)
	{
		gpID3D11DeviceContext->Release();
		gpID3D11DeviceContext = NULL;	
	}

	if(gpID3D11Device)
	{
		gpID3D11Device->Release();
		gpID3D11Device = NULL;	
	}

	if(gpFile)
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "uninitialize done\n");
		fprintf_s(gpFile, "Log file is closed\n");
		fclose(gpFile);	
	}
}