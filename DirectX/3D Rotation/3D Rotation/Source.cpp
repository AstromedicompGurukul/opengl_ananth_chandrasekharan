#include <windows.h>
#include <stdio.h>

#include <d3d11.h>
#include <d3dcompiler.h>

#pragma warning (disable : 4838) // to supress warning of XNAMATH unsigned int to INT conversion
#include "XNAMATH\xnamath.h"

#pragma comment(lib,"d3d11.lib")
#pragma comment(lib,"D3dcompiler.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

FILE *gpFile = NULL;
char gszLogFileName[]="Log.txt";

HWND ghwnd = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = {sizeof(WINDOWPLACEMENT)};

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

float gClearColor[4];
IDXGISwapChain *gpIDXGISwapChain = NULL;
ID3D11Device *gpID3D11Device = NULL;
ID3D11DeviceContext *gpID3D11DeviceContext = NULL;
ID3D11RenderTargetView *gpID3D11RenderTargetView = NULL;

ID3D11VertexShader *gpID3D11VertexShader = NULL;
ID3D11PixelShader *gpID3D11PixelShader = NULL;
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Position_Pyramid = NULL; // vertex positions triangle (like VBO)
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Color_Pyramid = NULL; // vertex positions triangle (like VBO)
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Position_Cube = NULL; // vertex position square(like VBO)
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Color_Cube = NULL; // vertex position square(like VBO)
ID3D11Buffer *gpID3D11Buffer_ConstantBuffer = NULL;  //for uniforms
ID3D11InputLayout *gpID3D11InputLayout =NULL;

ID3D11RasterizerState *gpID3D11RasterizerState = NULL;
ID3D11DepthStencilView *gpID3D11DepthStencilView = NULL;

struct CBUFFER
{
	XMMATRIX WorldViewProjectionMatrix; //XMMATRIX anologous to vmath::mat4
};

XMMATRIX gPerspectiveProjectionMatrix;

float angle_pyramid = 0.0f;
float angle_cube = 0.0f;

int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpszCmdLine,int iCmdShow)
{
	HRESULT initialize(void);
	void uninitialize(void);
	void display(void);

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[]= TEXT("Shree Ganesha : 3D Rotation");
	bool bDone = false;


	if(fopen_s(&gpFile,gszLogFileName,"w") !=0)
	{
		MessageBox(NULL,TEXT("Log File Can Not be Created \n Exitting...."),TEXT("Error"),MB_OK|MB_TOPMOST|MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf_s(gpFile, "Log File Is Successfully Opened.\n");
		fclose(gpFile);
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW |CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL,IDC_ARROW);
	wndclass.lpszMenuName = NULL;
	wndclass.lpszClassName = szClassName;
	wndclass.hIconSm = LoadIcon(NULL,IDI_APPLICATION);

	RegisterClassEx(&wndclass);

	hwnd = CreateWindow(szClassName,
		TEXT("Shree Ganesha : 3D Rotation"),
		WS_OVERLAPPEDWINDOW,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd,iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	HRESULT hr;
	hr = initialize();
	if(FAILED(hr))
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "initialize() Failed. Exitting Now...\n");
		fclose(gpFile);
		hwnd = NULL;
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "initialize() Succeeded.\n");
		fclose(gpFile);
	}

	while(bDone == false)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			display();

			if(gbActiveWindow == true)
			{
				if(gbEscapeKeyIsPressed ==true)
					bDone = true;
			}
		}
	}

	uninitialize();

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd,UINT iMsg,WPARAM wParam,LPARAM lParam)
{
	HRESULT resize(int,int);
	void ToggleFulscreen(void);
	void uninitialize(void);

	HRESULT hr;

	switch(iMsg)
	{
		case WM_ACTIVATE:
			if(HIWORD(wParam) == 0)
				gbActiveWindow = true;
			else
				gbActiveWindow = false;
			break;
		case WM_ERASEBKGND:
			return(0);
		case WM_SIZE:
			if(gpID3D11DeviceContext)
			{
				hr = resize(LOWORD(lParam),HIWORD(lParam));
				if(FAILED(hr))
				{
					fopen_s(&gpFile,gszLogFileName,"a+");
					fprintf_s(gpFile, "resize() Failed. Exitting Now...\n");
					fclose(gpFile);
					return(hr);
				}
				else
				{
					fopen_s(&gpFile,gszLogFileName,"a+");
					fprintf_s(gpFile, "resize() Succeeded\n");
					fclose(gpFile);
				}
			}
			break;
		case WM_KEYDOWN:
			switch(wParam)
			{
				case VK_ESCAPE:
					if(gbEscapeKeyIsPressed == false)
						gbEscapeKeyIsPressed = true;
					break;
				case 0x46: //F or f
					if(gbFullscreen == false)
					{
						ToggleFulscreen();
						gbFullscreen = true;
					}
					else
					{
						ToggleFulscreen();
						gbFullscreen = false;
					}
					break;
				default:
					break;
			}
			break;
		case WM_LBUTTONDOWN:
			break;
		case WM_CLOSE:
			uninitialize();
			break;
		case WM_DESTROY:
			PostQuitMessage(0);
		default:
			break;
	}

	return(DefWindowProc(hwnd,iMsg,wParam,lParam));
}


void ToggleFulscreen(void)
{
	MONITORINFO mi;

	if(gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd,GWL_STYLE);
		if(dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = {sizeof(MONITORINFO)};
			if(GetWindowPlacement(ghwnd,&wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY),&mi))
			{
				SetWindowLong(ghwnd,GWL_STYLE,dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(ghwnd,HWND_TOP,mi.rcMonitor.left,mi.rcMonitor.top,mi.rcMonitor.right - mi.rcMonitor.left,mi.rcMonitor.bottom - mi.rcMonitor.top,SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd,&wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_NOMOVE| SWP_NOSIZE| SWP_NOOWNERZORDER|SWP_NOZORDER|SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

HRESULT initialize(void)
{
	void uninitialize(void);
	HRESULT resize(int,int);

	HRESULT hr;

	D3D_DRIVER_TYPE d3dDriverType;
	
	D3D_DRIVER_TYPE d3dDriverTypes[] = {
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_WARP,
		D3D_DRIVER_TYPE_REFERENCE,
	};

	D3D_FEATURE_LEVEL d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
	
	D3D_FEATURE_LEVEL d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_10_0;

	UINT createDeviceFlags = 0;
	UINT numDriverTypes = 0;
	UINT numFeatureLevels = 1;

	numDriverTypes = sizeof(d3dDriverTypes)/sizeof(d3dDriverTypes[0]);

	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
	ZeroMemory((void *)&dxgiSwapChainDesc,sizeof(DXGI_SWAP_CHAIN_DESC));

	dxgiSwapChainDesc.BufferCount =1;
	dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;
	dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
	dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	dxgiSwapChainDesc.OutputWindow = ghwnd;
	dxgiSwapChainDesc.SampleDesc.Count = 1;
	dxgiSwapChainDesc.SampleDesc.Quality = 0;
	dxgiSwapChainDesc.Windowed = TRUE;

	for(UINT driverTypeIndex = 0;driverTypeIndex < numDriverTypes;driverTypeIndex++)
	{
		d3dDriverType = d3dDriverTypes[driverTypeIndex];
		
		hr = D3D11CreateDeviceAndSwapChain(
			NULL,
			d3dDriverType,
			NULL,
			createDeviceFlags,
			&d3dFeatureLevel_required,
			numFeatureLevels,
			D3D11_SDK_VERSION,
			&dxgiSwapChainDesc,
			&gpIDXGISwapChain,
			&gpID3D11Device,
			&d3dFeatureLevel_acquired,
			&gpID3D11DeviceContext);

		if(SUCCEEDED(hr))
		{
			break;
		}
	}

	if(FAILED(hr))
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain() Failed. Exitting Now...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		
		fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain() Succeeded\n");
		
		fprintf_s(gpFile, "The Chosen Driver is of ");
		
		if(d3dDriverType == D3D_DRIVER_TYPE_HARDWARE)
		{
			fprintf_s(gpFile, "Hardware Type \n");
		}
		else if(d3dDriverType == D3D_DRIVER_TYPE_WARP)
		{
			fprintf_s(gpFile, "WARP Type \n");
		}
		else if(d3dDriverType == D3D_DRIVER_TYPE_REFERENCE)
		{
			fprintf_s(gpFile, "Reference Type \n");
		}
		else
		{
			fprintf_s(gpFile, "Unknown Type \n");
		}

		fprintf_s(gpFile, "The Supported Highest Feature Level is ");
		
		if(d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_11_0)
		{
			fprintf_s(gpFile, "11.0 \n");
		}
		else if(d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_0)
		{
			fprintf_s(gpFile, "10.0\n");
		}
		else
		{
			fprintf_s(gpFile, "Unknown.\n");
		}
		
		fclose(gpFile);
	}

	const char *vertexShaderSourceCode = 
	"cbuffer ConstantBuffer"\
	"{"\
	"float4x4 worldViewProjectionMatrix;"
	"}"\
	"struct vertex_output"\
	"{"\
	"float4 position:SV_POSITION;"\
	"float4 color : COLOR;"\
	"};"\
	"vertex_output main(float4 pos:POSITION,float4 color:COLOR)"\
	"{"\
	"vertex_output output;"\
	"output.position = mul(worldViewProjectionMatrix , pos);"\
	"output.color = color;"\
	"return (output);"\
	"}";

	ID3DBlob *pID3DBlob_VertexShaderCode = NULL;
	ID3DBlob *pID3DBlob_Error = NULL;

	hr = D3DCompile(vertexShaderSourceCode,  //shader source code is here
		lstrlenA(vertexShaderSourceCode)+1,  // length of source code.
		"VS",   //Vertex Shader
		NULL,	
		D3D_COMPILE_STANDARD_FILE_INCLUDE, 
		"main", 
		"vs_5_0", 
		0,	
		0,	
		&pID3DBlob_VertexShaderCode, 
		&pID3DBlob_Error);

	if(FAILED(hr))
	{
		if(pID3DBlob_Error !=NULL)
		{
			fopen_s(&gpFile,gszLogFileName,"a+");
			fprintf_s(gpFile, "D3DCompile() failed for Vertex Shader: %s\n",(char *)pID3DBlob_Error->GetBufferPointer());
			fclose(gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return(hr);		
		}
		else
		{
			fopen_s(&gpFile,gszLogFileName,"a+");
			fprintf_s(gpFile, "D3DCompile() Failed.\n");
			fclose(gpFile);
			return(hr);
		}
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "D3DCompile() done For Vertex Shader.\n");
		fclose(gpFile);
	}


	hr = gpID3D11Device->CreateVertexShader(pID3DBlob_VertexShaderCode->GetBufferPointer(),
		pID3DBlob_VertexShaderCode->GetBufferSize(),
		NULL, 		
		&gpID3D11VertexShader);

	if(FAILED(hr))
	{
		pID3DBlob_VertexShaderCode->Release();
		pID3DBlob_VertexShaderCode = NULL;
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "CreateVertexShader() failed For Vertex Shader.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");

		fprintf_s(gpFile, "CreateVertexShader() done For vertex shader.\n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShader,
		0, 	
		0);	


	const char *pixelShaderSourceCode = 
	"float4 main(float4 pos:SV_POSITION,float4 color:COLOR):SV_TARGET"\
	"{"\
	"return(color);"\
	"}";

	ID3DBlob *pID3DBlob_PixelShaderCode = NULL;
	pID3DBlob_Error = NULL;

	hr = D3DCompile(pixelShaderSourceCode,  
		lstrlenA(pixelShaderSourceCode)+1,  
		"PS",   
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main", 
		"ps_5_0", 
		0,	
		0,	
		&pID3DBlob_PixelShaderCode,   
		&pID3DBlob_Error);	
	
	if(FAILED(hr))
	{
		if(pID3DBlob_Error !=NULL)
		{
			pID3DBlob_VertexShaderCode->Release();
			pID3DBlob_VertexShaderCode = NULL;
			fopen_s(&gpFile,gszLogFileName,"a+");
			fprintf_s(gpFile, "D3DCompile() failed for Pixel Shader: %s\n",(char *)pID3DBlob_Error->GetBufferPointer());
			fclose(gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return(hr);		
		}
		else
		{
			pID3DBlob_VertexShaderCode->Release();

			pID3DBlob_VertexShaderCode = NULL;
			fopen_s(&gpFile,gszLogFileName,"a+");
			fprintf_s(gpFile, "D3DCompile() failed For Pixel Shader.\n");
			fclose(gpFile);
			return(hr);
		}
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "D3DCompile() Succeeded For Pixel Shader.\n");
		fclose(gpFile);
	}


	hr = gpID3D11Device->CreatePixelShader(pID3DBlob_PixelShaderCode->GetBufferPointer(),
		pID3DBlob_PixelShaderCode->GetBufferSize(),
		NULL, 		//it is for class linkage for variable used accross shaders
		&gpID3D11PixelShader);

	if(FAILED(hr))
	{
		pID3DBlob_VertexShaderCode->Release();
		pID3DBlob_VertexShaderCode = NULL;
		pID3DBlob_PixelShaderCode->Release();
		pID3DBlob_PixelShaderCode = NULL;
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "CreatePixelShader() Failed For Pixel Shader. Exitting Now...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "CreatePixelShader() Succeeded For Pixel Shader.\n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->PSSetShader(gpID3D11PixelShader,
		0, 	//Linkage objects array
		0);	//elements in linkage object array

	
	//code  for input layout
	D3D11_INPUT_ELEMENT_DESC inputElementDesc[2];
	inputElementDesc[0].SemanticName = "POSITION";
	inputElementDesc[0].SemanticIndex = 0;
	inputElementDesc[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	inputElementDesc[0].InputSlot = 0;
	inputElementDesc[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputElementDesc[0].AlignedByteOffset = 0;
	inputElementDesc[0].InstanceDataStepRate = 0;

	inputElementDesc[1].SemanticName = "COLOR";
	inputElementDesc[1].SemanticIndex = 0;
	inputElementDesc[1].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	inputElementDesc[1].InputSlot = 1;
	inputElementDesc[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputElementDesc[1].AlignedByteOffset = 0;
	inputElementDesc[1].InstanceDataStepRate = 0;


	hr = gpID3D11Device->CreateInputLayout(inputElementDesc,//array of input layout
		2, //number of elements in an array
		pID3DBlob_VertexShaderCode->GetBufferPointer(),
		pID3DBlob_VertexShaderCode->GetBufferSize(),
		&gpID3D11InputLayout
	);
	
	if(FAILED(hr))
	{
		pID3DBlob_VertexShaderCode->Release();
		pID3DBlob_VertexShaderCode = NULL;
		pID3DBlob_PixelShaderCode->Release();
		pID3DBlob_PixelShaderCode = NULL;
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "ID3D11Device::CreateInputLayout() Failed. Exitting Now...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "ID3D11Device::CreateInputLayout() Succeeded.\n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->IASetInputLayout(gpID3D11InputLayout);

	pID3DBlob_VertexShaderCode->Release();
	pID3DBlob_VertexShaderCode = NULL;
	pID3DBlob_PixelShaderCode->Release();
	pID3DBlob_PixelShaderCode = NULL;

	//create vertices array and vertex buffer for triangle	
	float vertices_pyramid[]
	{
		//front
		0.0f,1.0f,0.0f, //apex
		1.0f,-1.0f,-1.0f, //right
		-1.0f,-1.0f,-1.0f, //left

		//right
		0.0f,1.0f,0.0f, //apex
		1.0f,-1.0f,1.0f, //left
		1.0f,-1.0f,-1.0f, //right

		//back
		0.0f,1.0f,0.0f, //apex
		-1.0f,-1.0f,1.0f, //right
		1.0f,-1.0f,1.0f, //left

		//left
		0.0f,1.0f,0.0f, //apex
		-1.0f,-1.0f,-1.0f, //left
		-1.0f,-1.0f,1.0f, //right

	};

 	D3D11_BUFFER_DESC bufferDesc;
 	ZeroMemory(&bufferDesc,sizeof(D3D11_BUFFER_DESC));
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc.ByteWidth = sizeof(float) * ARRAYSIZE(vertices_pyramid);
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	
	hr = gpID3D11Device->CreateBuffer(&bufferDesc,
		nullptr,
		&gpID3D11Buffer_VertexBuffer_Position_Pyramid);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Failed For Vertex Buffer. Exitting Now...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Succeeded For Vertex Buffer.\n");
		fclose(gpFile);
	}

	D3D11_MAPPED_SUBRESOURCE mappedSubResource;
	ZeroMemory(&mappedSubResource, sizeof(D3D11_MAPPED_SUBRESOURCE));
	gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Position_Pyramid, 0,
		D3D11_MAP_WRITE_DISCARD, 0, &mappedSubResource);
	memcpy(mappedSubResource.pData, vertices_pyramid, sizeof(vertices_pyramid));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Position_Pyramid, 0);

	//color VBO
	float colors[]
	{
		//front
		1.0f,0.0f,0.0f, //RED
		0.0f,0.0f,1.0f, //GREEN
		0.0f,1.0f,0.0f, //BLUE

		//right
		1.0f,0.0f,0.0f, //RED
		0.0f,1.0f,0.0f, //GREEN
		0.0f,0.0f,1.0f,//BLUE

		//back
		1.0f,0.0f,0.0f, //RED
		0.0f,0.0f,1.0f, //BLUE
		0.0f,1.0f,0.0f, //GREEN

		//left
		1.0f,0.0f,0.0f, //RED
		0.0f,1.0f,0.0f, //GREEM
		0.0f,0.0f,1.0f //BLUE
	};
 	ZeroMemory(&bufferDesc,sizeof(D3D11_BUFFER_DESC));
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc.ByteWidth = sizeof(float) * ARRAYSIZE(colors);
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	
	hr = gpID3D11Device->CreateBuffer(&bufferDesc,
		nullptr,
		&gpID3D11Buffer_VertexBuffer_Color_Pyramid);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Failed For Color Buffer Vertex Shader. Exitting Now...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Succeeded For Color Buffer Vertex Shader.\n");
		fclose(gpFile);
	}

	ZeroMemory(&mappedSubResource, sizeof(D3D11_MAPPED_SUBRESOURCE));
	gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Color_Pyramid, 0,
		D3D11_MAP_WRITE_DISCARD, 0, &mappedSubResource);
	memcpy(mappedSubResource.pData, colors, sizeof(colors));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Color_Pyramid, 0);



	//Square VBO
	float vertices_cube[]
	{
		//top
		-1.0f,+1.0f,+1.0f,
		+1.0f,+1.0f,+1.0f,
		-1.0f,+1.0f,-1.0f,

		-1.0f,+1.0f,-1.0f,
		+1.0f,+1.0f,+1.0f,
		+1.0f,+1.0f,-1.0f,

		//Bottom
		+1.0f,-1.0f,-1.0f,
		+1.0f,-1.0f,+1.0f,
		-1.0f,-1.0f,-1.0f,

		-1.0f,-1.0f,-1.0f,
		+1.0f,-1.0f,+1.0f,
		-1.0f,-1.0f,+1.0f,

		//FRONT
		-1.0f,+1.0f,-1.0f,
		+1.0f,+1.0f,-1.0f,
		-1.0f,-1.0f,-1.0f,

		-1.0f,-1.0f,-1.0f,
		+1.0f,+1.0f,-1.0f,
		+1.0f,-1.0f,-1.0f,

		//BACK
		+1.0f,-1.0f,+1.0f,
		+1.0f,+1.0f,+1.0f,
		-1.0f,-1.0f,+1.0f,

		-1.0f,-1.0f,+1.0f,
		+1.0f,+1.0f,+1.0f,
		-1.0f,+1.0f,+1.0f,

		//left
		-1.0f,+1.0f,+1.0f,
		-1.0f,+1.0f,-1.0f,
		-1.0f,-1.0f,+1.0f,

		-1.0f,-1.0f,+1.0f,
		-1.0f,+1.0f,-1.0f,
		-1.0f,-1.0f,-1.0f,

		//right
		+1.0f,-1.0f,-1.0f,
		+1.0f,+1.0f,-1.0f,
		+1.0f,-1.0f,+1.0f,

		+1.0f,-1.0f,+1.0f,
		+1.0f,+1.0f,-1.0f,
		+1.0f,+1.0f,+1.0f,
 	};

 	ZeroMemory(&bufferDesc,sizeof(D3D11_BUFFER_DESC));
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc.ByteWidth = sizeof(float) * ARRAYSIZE(vertices_cube);
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	
	hr = gpID3D11Device->CreateBuffer(&bufferDesc,
		nullptr,
		&gpID3D11Buffer_VertexBuffer_Position_Cube);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Failed For Color Buffer Vertex Shader. Exitting Now...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Succeeded For Color Buffer Vertex Shader.\n");
		fclose(gpFile);
	}

	ZeroMemory(&mappedSubResource, sizeof(D3D11_MAPPED_SUBRESOURCE));
	gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Position_Cube, 0,
		D3D11_MAP_WRITE_DISCARD, 0, &mappedSubResource);
	memcpy(mappedSubResource.pData, vertices_cube, sizeof(vertices_cube));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Position_Cube, 0);


	//color VBO
	float colors_cube[]
	{
		//top red
		1.0f,0.0f,0.0f,
		1.0f,0.0f,0.0f,
		1.0f,0.0f,0.0f,
		
		1.0f,0.0f,0.0f,
		1.0f,0.0f,0.0f,
		1.0f,0.0f,0.0f,

		//Bottom green
		0.0f,1.0f,0.0f,
		0.0f,1.0f,0.0f,
		0.0f,1.0f,0.0f,

		0.0f,1.0f,0.0f,
		0.0f,1.0f,0.0f,
		0.0f,1.0f,0.0f,


		//Front Blue
		0.0f,0.0f,1.0f,
		0.0f,0.0f,1.0f,
		0.0f,0.0f,1.0f,

		0.0f,0.0f,1.0f,
		0.0f,0.0f,1.0f,
		0.0f,0.0f,1.0f,


		//Back Cyan
		0.0f,1.0f,1.0f,
		0.0f,1.0f,1.0f,
		0.0f,1.0f,1.0f,

		0.0f,1.0f,1.0f,
		0.0f,1.0f,1.0f,
		0.0f,1.0f,1.0f,
		

		//left magenta
		1.0f,0.0f,1.0f,
		1.0f,0.0f,1.0f,
		1.0f,0.0f,1.0f,

		1.0f,0.0f,1.0f,
		1.0f,0.0f,1.0f,
		1.0f,0.0f,1.0f,

		//right yellow
		1.0f,1.0f,0.0f,
		1.0f,1.0f,0.0f,
		1.0f,1.0f,0.0f,

		1.0f,1.0f,0.0f,
		1.0f,1.0f,0.0f,
		1.0f,1.0f,0.0f,
 	};

 	ZeroMemory(&bufferDesc,sizeof(D3D11_BUFFER_DESC));
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc.ByteWidth = sizeof(float) * ARRAYSIZE(colors_cube);
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	
	hr = gpID3D11Device->CreateBuffer(&bufferDesc,
		nullptr,
		&gpID3D11Buffer_VertexBuffer_Color_Cube);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Failed For Color Buffer Vertex Shader. Exitting Now...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Succeeded For Color Buffer Vertex Shader.\n");
		fclose(gpFile);
	}

	ZeroMemory(&mappedSubResource, sizeof(D3D11_MAPPED_SUBRESOURCE));
	gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Color_Cube, 0,
		D3D11_MAP_WRITE_DISCARD, 0, &mappedSubResource);
	memcpy(mappedSubResource.pData, colors_cube, sizeof(colors_cube));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Color_Cube, 0);


	//now define and set constant buuffers
	D3D11_BUFFER_DESC bufferDesc_ConstantBuffer;
	ZeroMemory(&bufferDesc_ConstantBuffer,sizeof(D3D11_BUFFER_DESC));
	bufferDesc_ConstantBuffer.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc_ConstantBuffer.ByteWidth = sizeof(CBUFFER);
	bufferDesc_ConstantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

	hr = gpID3D11Device->CreateBuffer(&bufferDesc_ConstantBuffer,
		nullptr,
		&gpID3D11Buffer_ConstantBuffer);

	if(FAILED(hr))
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Failed For Constant Buffer. Exitting Now...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Succeeded For Constant Buffer.\n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->VSSetConstantBuffers(0,	//where i see this buffer in my shader
		1,	//how many buffers you have
		&gpID3D11Buffer_ConstantBuffer);


	gClearColor[0] = 0.0f;
	gClearColor[1] = 0.0f;
	gClearColor[2] = 0.0f;
	gClearColor[3] = 0.0f;

	gPerspectiveProjectionMatrix = XMMatrixIdentity();

	D3D11_RASTERIZER_DESC rasterizerDesc;
	ZeroMemory(&rasterizerDesc, sizeof(D3D11_RASTERIZER_DESC));

	rasterizerDesc.AntialiasedLineEnable = FALSE;
	rasterizerDesc.CullMode = D3D11_CULL_NONE;
	rasterizerDesc.DepthBias = 0;
	rasterizerDesc.MultisampleEnable = FALSE;
	rasterizerDesc.DepthBiasClamp = 0.0f;
	rasterizerDesc.DepthClipEnable = TRUE;
	rasterizerDesc.SlopeScaledDepthBias = 0.0f;
	rasterizerDesc.FillMode = D3D11_FILL_SOLID;
	rasterizerDesc.FrontCounterClockwise = FALSE;
	rasterizerDesc.ScissorEnable = FALSE;

	gpID3D11Device->CreateRasterizerState(&rasterizerDesc, &gpID3D11RasterizerState);

	gpID3D11DeviceContext->RSSetState(gpID3D11RasterizerState);

	hr =resize(WIN_WIDTH, WIN_HEIGHT);
	if(FAILED(hr))
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "resize() Failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "resize() Succeeded.\n");
		fclose(gpFile);
	}

	return(S_OK);
}

HRESULT resize(int width,int height)
{
	HRESULT hr = S_OK;

	if (gpID3D11DepthStencilView)
	{
		gpID3D11DepthStencilView->Release();
		gpID3D11DepthStencilView = NULL;
	}

	if(gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	gpIDXGISwapChain->ResizeBuffers(1,width,height,DXGI_FORMAT_R8G8B8A8_UNORM,0);

	ID3D11Texture2D *pID3D11Texture2D_BackBuffer;
	gpIDXGISwapChain->GetBuffer(0,__uuidof(ID3D11Texture2D),(LPVOID *)&pID3D11Texture2D_BackBuffer);

	hr = gpID3D11Device->CreateRenderTargetView(pID3D11Texture2D_BackBuffer,NULL,&gpID3D11RenderTargetView);
	if(FAILED(hr))
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "ID3D11Device::CreateRenderTargetView() Failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "ID3D11Device::CreateRenderTargetView() Succeeded.\n");
		fclose(gpFile);
	}

	pID3D11Texture2D_BackBuffer->Release();
	pID3D11Texture2D_BackBuffer= NULL;

	//create depth buffer and set it to render target view

	//first fill the texture descriptor which is used to create  texture which we will use it as buffer.

	D3D11_TEXTURE2D_DESC textureDesc;
	ZeroMemory(&textureDesc, sizeof(D3D11_TEXTURE2D_DESC));

	textureDesc.ArraySize = 1;
	textureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	textureDesc.CPUAccessFlags = 0;
	textureDesc.Format = DXGI_FORMAT_D32_FLOAT;
	textureDesc.Height = height;
	textureDesc.Width = width;
	textureDesc.MipLevels = 1;
	textureDesc.MiscFlags = 0;
	textureDesc.SampleDesc.Count = 1;
	textureDesc.SampleDesc.Quality = 0;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;

	ID3D11Texture2D *pID3D11Texture2D_DepthBuffer = NULL;

	hr = gpID3D11Device->CreateTexture2D(&textureDesc, NULL, &pID3D11Texture2D_DepthBuffer);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateTexture2D() Failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateTexture2D() Succeeded.\n");
		fclose(gpFile);
	}

	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	ZeroMemory(&depthStencilViewDesc, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));

	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;
	depthStencilViewDesc.Format = DXGI_FORMAT_D32_FLOAT;

	hr = gpID3D11Device->CreateDepthStencilView(pID3D11Texture2D_DepthBuffer, &depthStencilViewDesc, &gpID3D11DepthStencilView);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateDepthStencilView() Failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateDepthStencilView() Succeeded.\n");
		fclose(gpFile);
	}

	pID3D11Texture2D_DepthBuffer->Release();
	pID3D11Texture2D_DepthBuffer = NULL;

	gpID3D11DeviceContext->OMSetRenderTargets(1,&gpID3D11RenderTargetView,gpID3D11DepthStencilView);


	D3D11_VIEWPORT d3dViewPort;
	d3dViewPort.TopLeftX = 0;
	d3dViewPort.TopLeftY = 0;
	d3dViewPort.Width = (float)width;
	d3dViewPort.Height = (float)height;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;

	gpID3D11DeviceContext->RSSetViewports(1,&d3dViewPort);


	if (width >= height)
		gPerspectiveProjectionMatrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0f), (float)width / (float)height, 0.1f, 100.0f);
	else
		gPerspectiveProjectionMatrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0f), (float)height / (float)width, 0.1f, 100.0f);
	return(hr);
}

void update(void)
{
	angle_pyramid = angle_pyramid -  0.1f;
	if (angle_pyramid <= 0.0f)
		angle_pyramid = 360.0f;

	angle_cube = angle_cube + 0.1f;
	if (angle_cube > 360.0f)
		angle_cube = 0.0f;
}

void display(void)
{
	void update(void);
	gpID3D11DeviceContext->ClearRenderTargetView(gpID3D11RenderTargetView,gClearColor);
	gpID3D11DeviceContext->ClearDepthStencilView(gpID3D11DepthStencilView,D3D11_CLEAR_DEPTH,1.0f,0);

	UINT stride = sizeof(float) * 3;
	UINT offset = 0;
	//DRAW pyramid
	gpID3D11DeviceContext->IASetVertexBuffers(0,//input slot
		1,  //how many buffers you have
		&gpID3D11Buffer_VertexBuffer_Position_Pyramid,
		&stride,
		&offset);

	gpID3D11DeviceContext->IASetVertexBuffers(1,//input slot
		1,  //how many buffers you have
		&gpID3D11Buffer_VertexBuffer_Color_Pyramid,
		&stride,
		&offset);
	

	//select geometry primitive
	gpID3D11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	XMMATRIX worldMatrix = XMMatrixIdentity();
	XMMATRIX viewMatrix = XMMatrixIdentity();
	XMMATRIX rotationMatrix = XMMatrixIdentity();

	rotationMatrix = XMMatrixRotationY(XMConvertToRadians(angle_pyramid));
	worldMatrix = rotationMatrix * XMMatrixTranslation(-1.5f, 0.0f, 6.0f);
	XMMATRIX wvpMatrix = worldMatrix * viewMatrix * gPerspectiveProjectionMatrix;

	CBUFFER constantBuffer;
	constantBuffer.WorldViewProjectionMatrix = wvpMatrix;
	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer,
		0,	//index in shader
		NULL, //D3D_BOX bounding box compute shader cube of data
		&constantBuffer,
		0, //width of face of bounding box
		0 );	//depth of bounding box	

	gpID3D11DeviceContext->Draw(12,//how many vertices
		0);//offset from where you want to draw
	


	// draw cube
	gpID3D11DeviceContext->IASetVertexBuffers(0,//input slot
		1,  //how many buffers you have
		&gpID3D11Buffer_VertexBuffer_Position_Cube,
		&stride,
		&offset);


	gpID3D11DeviceContext->IASetVertexBuffers(1,//input slot
		1,  //how many buffers you have
		&gpID3D11Buffer_VertexBuffer_Color_Cube,
		&stride,
		&offset);
	//select geometry primitive
	gpID3D11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	worldMatrix = XMMatrixIdentity();
	viewMatrix = XMMatrixIdentity();
	wvpMatrix = XMMatrixIdentity();
	XMMATRIX scaleMatrix = XMMatrixIdentity();
	rotationMatrix  = XMMatrixIdentity();
	
	rotationMatrix = XMMatrixRotationX(XMConvertToRadians(angle_cube)) * XMMatrixRotationY(XMConvertToRadians(angle_cube)) * XMMatrixRotationZ(XMConvertToRadians(angle_cube));
	scaleMatrix  = XMMatrixScaling(0.75f,0.75f,0.75f);
	worldMatrix = scaleMatrix * rotationMatrix * XMMatrixTranslation(1.5f, 0.0f, 6.0f);
	wvpMatrix = worldMatrix * viewMatrix * gPerspectiveProjectionMatrix;

	ZeroMemory(&constantBuffer, sizeof(CBUFFER));
	constantBuffer.WorldViewProjectionMatrix = wvpMatrix;
	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer,
		0,	//index in shader
		NULL, //D3D_BOX bounding box compute shader cube of data
		&constantBuffer,
		0, //width of face of bounding box
		0 );	//depth of bounding box	

	gpID3D11DeviceContext->Draw(6,0);
	gpID3D11DeviceContext->Draw(6,6);
	gpID3D11DeviceContext->Draw(6,12);
	gpID3D11DeviceContext->Draw(12,18);
	gpID3D11DeviceContext->Draw(18,24);
	gpID3D11DeviceContext->Draw(24,30);
	


	gpIDXGISwapChain->Present(0,0);
	update();
}

void uninitialize(void)
{
	if (gpID3D11Buffer_ConstantBuffer)
	{
		gpID3D11Buffer_ConstantBuffer->Release();
		gpID3D11Buffer_ConstantBuffer = NULL;
	}

	if (gpID3D11InputLayout)
	{
		gpID3D11InputLayout->Release();
		gpID3D11InputLayout = NULL;
	}

	if (gpID3D11Buffer_VertexBuffer_Position_Pyramid)
	{
		gpID3D11Buffer_VertexBuffer_Position_Pyramid->Release();
		gpID3D11Buffer_VertexBuffer_Position_Pyramid = NULL;
	}

	if (gpID3D11Buffer_VertexBuffer_Color_Pyramid)
	{
		gpID3D11Buffer_VertexBuffer_Color_Pyramid->Release();
		gpID3D11Buffer_VertexBuffer_Color_Pyramid = NULL;
	}

	if (gpID3D11Buffer_VertexBuffer_Position_Cube)
	{
		gpID3D11Buffer_VertexBuffer_Position_Cube->Release();
		gpID3D11Buffer_VertexBuffer_Position_Cube = NULL;
	}

	if (gpID3D11Buffer_VertexBuffer_Color_Cube)
	{
		gpID3D11Buffer_VertexBuffer_Color_Cube->Release();
		gpID3D11Buffer_VertexBuffer_Color_Cube = NULL;
	}

	if (gpID3D11RasterizerState)
	{
		gpID3D11RasterizerState->Release();
		gpID3D11RasterizerState = NULL;
	}

	if (gpID3D11PixelShader)
	{
		gpID3D11PixelShader->Release();
		gpID3D11PixelShader = NULL;
	}

	if (gpID3D11VertexShader)
	{
		gpID3D11VertexShader->Release();
		gpID3D11VertexShader = NULL;
	}

	if (gpID3D11DepthStencilView)
	{
		gpID3D11DepthStencilView->Release();
		gpID3D11DepthStencilView = NULL;
	}

	
	if(gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;	
	}

	if(gpIDXGISwapChain)
	{
		gpIDXGISwapChain->Release();
		gpIDXGISwapChain = NULL;	
	}

	if(gpID3D11DeviceContext)
	{
		gpID3D11DeviceContext->Release();
		gpID3D11DeviceContext = NULL;	
	}

	if(gpID3D11Device)
	{
		gpID3D11Device->Release();
		gpID3D11Device = NULL;	
	}

	if(gpFile)
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "uninitialize Succeeded\n");
		fprintf_s(gpFile, "Log File Closed Successfully\n");
		fclose(gpFile);	
	}
}