#include <windows.h>
#include <stdio.h>

#include <d3d11.h>
#include <d3dcompiler.h>

#pragma warning (disable : 4838) // to supress warning of XNAMATH unsigned int to INT conversion
#include "XNAMATH\xnamath.h"

#pragma comment(lib,"d3d11.lib")
#pragma comment(lib,"D3dcompiler.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

FILE *gpFile = NULL;
char gszLogFileName[]="Log.txt";

HWND ghwnd = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = {sizeof(WINDOWPLACEMENT)};

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

float gClearColor[4];
IDXGISwapChain *gpIDXGISwapChain = NULL;
ID3D11Device *gpID3D11Device = NULL;
ID3D11DeviceContext *gpID3D11DeviceContext = NULL;
ID3D11RenderTargetView *gpID3D11RenderTargetView = NULL;

ID3D11VertexShader *gpID3D11VertexShader = NULL;
ID3D11HullShader *gpID3D11HullShader = NULL;
ID3D11DomainShader *gpID3D11DomainShader = NULL;
ID3D11PixelShader *gpID3D11PixelShader = NULL;
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Position = NULL; 
ID3D11Buffer *gpID3D11Buffer_ConstantBuffer_DomainShader = NULL;  
ID3D11Buffer *gpID3D11Buffer_ConstantBuffer_HullShader = NULL; 
ID3D11Buffer *gpID3D11Buffer_ConstantBuffer_PixelShader = NULL;  
ID3D11InputLayout *gpID3D11InputLayout =NULL;


struct CBUFFER
{
	XMMATRIX WorldViewProjectionMatrix; 
};

XMMATRIX gPerspectiveProjectionMatrix;

struct CBUFFER_HULLSHADER 
{
	XMVECTOR Hull_Constant_Function_Param;  
};

struct CBUFFER_DOMAINSHADER
{
	XMMATRIX WorldViewProjectionMatrix; 
};

struct CBUFFER_PIXELSHADER
{
	XMVECTOR LineColor; 
};

unsigned int gNumberOfSegments;


int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpszCmdLine,int iCmdShow)
{
	HRESULT initialize(void);
	void uninitialize(void);
	void display(void);

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[]= TEXT("Shree Ganesha : Tesscellation Shader");
	bool bDone = false;


	if(fopen_s(&gpFile,gszLogFileName,"w") !=0)
	{
		MessageBox(NULL,TEXT("Log File not created"),TEXT("Error"),MB_OK| MB_TOPMOST|MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf_s(gpFile, "Log File is opened.\n");
		fclose(gpFile);
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW |CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL,IDC_ARROW);
	wndclass.lpszMenuName = NULL;
	wndclass.lpszClassName = szClassName;
	wndclass.hIconSm = LoadIcon(NULL,IDI_APPLICATION);

	RegisterClassEx(&wndclass);

	hwnd = CreateWindow(szClassName,
		TEXT("Tesscellation Shader"),
		WS_OVERLAPPEDWINDOW,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd,iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	HRESULT hr;
	hr = initialize();
	if(FAILED(hr))
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "initialize() failed\n");
		fclose(gpFile);
		hwnd = NULL;
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+")
			;
		fprintf_s(gpFile, "initialize() done\n");
		fclose(gpFile);
	}

	while(bDone == false)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			display();

			if(gbActiveWindow == true)
			{
				if(gbEscapeKeyIsPressed ==true)
					bDone = true;
			}
		}
	}

	uninitialize();

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd,UINT iMsg,WPARAM wParam,LPARAM lParam)
{
	HRESULT resize(int,int);
	void ToggleFulscreen(void);
	void uninitialize(void);

	HRESULT hr;

	switch(iMsg)
	{
		case WM_ACTIVATE:
			if(HIWORD(wParam) == 0)
				gbActiveWindow = true;
			else
				gbActiveWindow = false;
			break;
		case WM_ERASEBKGND:
			return(0);
		case WM_SIZE:
			if(gpID3D11DeviceContext)
			{
				hr = resize(LOWORD(lParam),HIWORD(lParam));
				if(FAILED(hr))
				{
					fopen_s(&gpFile,gszLogFileName,"a+");
					fprintf_s(gpFile, "resize() Failed. Exitting Now...\n");
					fclose(gpFile);
					return(hr);
				}
				else
				{
					fopen_s(&gpFile,gszLogFileName,"a+");
					fprintf_s(gpFile, "resize() Succeeded\n");
					fclose(gpFile);
				}
			}
			break;
		case WM_KEYDOWN:
			switch(wParam)
			{
				case VK_ESCAPE:
					if(gbEscapeKeyIsPressed == false)
						gbEscapeKeyIsPressed = true;
					break;
				case 0x46: //F or f
					if(gbFullscreen == false)
					{
						ToggleFulscreen();
						gbFullscreen = true;
					}
					else
					{
						ToggleFulscreen();
						gbFullscreen = false;
					}
					break;
				case VK_UP:
					gNumberOfSegments++;
					if(gNumberOfSegments > 30)
						gNumberOfSegments = 30;
					break;
				case VK_DOWN:
					gNumberOfSegments--;
					if(gNumberOfSegments <= 0)
						gNumberOfSegments = 1;
					break;
				default:
					break;
			}
			break;
		case WM_LBUTTONDOWN:
			break;
		case WM_CLOSE:
			uninitialize();
			break;
		case WM_DESTROY:
			PostQuitMessage(0);
		default:
			break;
	}

	return(DefWindowProc(hwnd,iMsg,wParam,lParam));
}


void ToggleFulscreen(void)
{
	MONITORINFO mi;

	if(gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd,GWL_STYLE);
		if(dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = {sizeof(MONITORINFO)};
			if(GetWindowPlacement(ghwnd,&wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY),&mi))
			{
				SetWindowLong(ghwnd,GWL_STYLE,dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(ghwnd,HWND_TOP,mi.rcMonitor.left,mi.rcMonitor.top,mi.rcMonitor.right - mi.rcMonitor.left,mi.rcMonitor.bottom - mi.rcMonitor.top,SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd,&wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_NOMOVE| SWP_NOSIZE| SWP_NOOWNERZORDER|SWP_NOZORDER|SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

HRESULT initialize(void)
{
	void uninitialize(void);
	HRESULT resize(int,int);

	HRESULT hr;
	D3D_DRIVER_TYPE d3dDriverType;
	D3D_DRIVER_TYPE d3dDriverTypes[] = {
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_WARP,
		D3D_DRIVER_TYPE_REFERENCE,
	};

	gNumberOfSegments = 1;

	D3D_FEATURE_LEVEL d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_10_0;

	UINT createDeviceFlags = 0;
	UINT numDriverTypes = 0;
	UINT numFeatureLevels = 1;

	numDriverTypes = sizeof(d3dDriverTypes)/sizeof(d3dDriverTypes[0]);

	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
	ZeroMemory((void *)&dxgiSwapChainDesc,sizeof(DXGI_SWAP_CHAIN_DESC));

	dxgiSwapChainDesc.BufferCount =1;
	dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;
	dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
	dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	dxgiSwapChainDesc.OutputWindow = ghwnd;
	dxgiSwapChainDesc.SampleDesc.Count = 1;
	dxgiSwapChainDesc.SampleDesc.Quality = 0;
	dxgiSwapChainDesc.Windowed = TRUE;

	for(UINT driverTypeIndex = 0;driverTypeIndex < numDriverTypes;driverTypeIndex++)
	{
		d3dDriverType = d3dDriverTypes[driverTypeIndex];
		hr = D3D11CreateDeviceAndSwapChain(
			NULL,
			d3dDriverType,
			NULL,
			createDeviceFlags,
			&d3dFeatureLevel_required,
			numFeatureLevels,
			D3D11_SDK_VERSION,
			&dxgiSwapChainDesc,
			&gpIDXGISwapChain,
			&gpID3D11Device,
			&d3dFeatureLevel_acquired,
			&gpID3D11DeviceContext);

		if(SUCCEEDED(hr))
		{
			break;
		}
	}

	if(FAILED(hr))
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain() failed\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain() done\n");
		fprintf_s(gpFile, "The Chosen Driver is");
		if(d3dDriverType == D3D_DRIVER_TYPE_HARDWARE)
		{
			fprintf_s(gpFile, "Hardware Type \n");
		}
		else if(d3dDriverType == D3D_DRIVER_TYPE_WARP)
		{
			fprintf_s(gpFile, "WARP Type \n");
		}
		else if(d3dDriverType == D3D_DRIVER_TYPE_REFERENCE)
		{
			fprintf_s(gpFile, "Reference Type \n");
		}
		else
		{
			fprintf_s(gpFile, "Unknown Type \n");
		}

		fprintf_s(gpFile, "The Supported Highest Feature Level is ");
		if(d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_11_0)
		{
			fprintf_s(gpFile,"11.0\n");
		}
		else if(d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_0)
		{
			fprintf_s(gpFile,"10.0\n");
		}
		else
		{
			fprintf_s(gpFile,"Unknown.\n");
		}
		fclose(gpFile);
	}

	const char *vertexShaderSourceCode = 
	"struct vertex_output"\
	"{"\
	"float4 position:POSITION;"\
	"};"\
	"vertex_output main(float2 pos:POSITION)"\
	"{"\
	"vertex_output output;"\
	"output.position = float4(pos,0.0,1.0);"\
	"return (output);"\
	"}";

	ID3DBlob *pID3DBlob_VertexShaderCode = NULL;
	ID3DBlob *pID3DBlob_Error = NULL;

	hr = D3DCompile(vertexShaderSourceCode,  
		lstrlenA(vertexShaderSourceCode)+1,  
		"VS",   
		NULL,	
		D3D_COMPILE_STANDARD_FILE_INCLUDE, 
		"main", 
		"vs_5_0", 

		0,	
		0,	
		&pID3DBlob_VertexShaderCode,   
		&pID3DBlob_Error);
	if(FAILED(hr))
	{
		if(pID3DBlob_Error !=NULL)
		{
			fopen_s(&gpFile,gszLogFileName,"a+");
			fprintf_s(gpFile, "D3DCompile() failed. For Vertex Shader: %s\n",(char *)pID3DBlob_Error->GetBufferPointer());
			fclose(gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return(hr);		
		}
		else
		{
			fopen_s(&gpFile,gszLogFileName,"a+");
			fprintf_s(gpFile, "D3DCompile() failed. Due to COM Error exiting\n");
			fclose(gpFile);
			return(hr);
		}
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "D3DCompile() done for Vertex Shader.\n");
		fclose(gpFile);
	}


	hr = gpID3D11Device->CreateVertexShader(pID3DBlob_VertexShaderCode->GetBufferPointer(),
		pID3DBlob_VertexShaderCode->GetBufferSize(),
		NULL, 		//it is for class linkage for variable used accross shaders
		&gpID3D11VertexShader);

	if(FAILED(hr))
	{
		pID3DBlob_VertexShaderCode->Release();
		pID3DBlob_VertexShaderCode = NULL;
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "CreateVertexShader() failed For Vertex Shader.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "CreateVertexShader() done For Vertex Shader.\n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShader,
		0, 	
		0);	


	const char *hullShaderSourceCode = 
	"cbuffer ConstantBuffer"\
	"{"\
	"float4 hull_Constant_Function_Param;"
	"}"\
	"struct vertex_output"\
	"{"\
	"float4 position:POSITION;"\
	"};"\
	"struct Hull_constant_output"\
	"{"\
	"float edges[2]:SV_TessFactor;"\
	"};"\
	"Hull_constant_output hull_constant_fucntion(void)"\
	"{"\
		"Hull_constant_output output;"\
		"float noOfStrips = hull_Constant_Function_Param[0];"\
		"float noOfSegments = hull_Constant_Function_Param[1];"\
		"output.edges[0] = noOfStrips;"\
		"output.edges[1] = noOfSegments;"\
		"return(output);"\
	"}"\
	"struct hull_output"\
	"{"\
	"float4 position:POSITION;"\
	"};"\
	"[domain (\"isoline\")]"\
	"[partitioning (\"integer\")]"\
	"[outputtopology (\"line\")]"\
	"[outputcontrolpoints (4)]"\
	"[patchconstantfunc (\"hull_constant_fucntion\")]"\
	"hull_output main(InputPatch<vertex_output,4> input_patch,uint i:SV_OutputControlPointID)"\
	"{"\
	"hull_output output;"\
	"output.position = input_patch[i].position;"\
	"return (output);"\
	"}";

	ID3DBlob *pID3DBlob_HullShaderCode = NULL;
	pID3DBlob_Error = NULL;

	hr = D3DCompile(hullShaderSourceCode,  
		lstrlenA(hullShaderSourceCode)+1,  
		"HS",   
		NULL,	
		D3D_COMPILE_STANDARD_FILE_INCLUDE, 
		"main", 
		"hs_5_0", 
		0,	
		0,	
		&pID3DBlob_HullShaderCode,   
		&pID3DBlob_Error);	
	if(FAILED(hr))
	{
		if(pID3DBlob_Error !=NULL)
		{
			pID3DBlob_VertexShaderCode->Release();
			pID3DBlob_VertexShaderCode = NULL;
			fopen_s(&gpFile,gszLogFileName,"a+");
			fprintf_s(gpFile, "D3DCompile() failed For Hull Shader: %s\n",(char *)pID3DBlob_Error->GetBufferPointer());
			fclose(gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return(hr);		
		}
		else
		{	
			pID3DBlob_VertexShaderCode->Release();
			pID3DBlob_VertexShaderCode = NULL;
			fopen_s(&gpFile,gszLogFileName,"a+");
			fprintf_s(gpFile, "D3DCompile() failed.\n");
			fclose(gpFile);
			return(hr);
		}
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "D3DCompile() done for Hull Shader.\n");
		fclose(gpFile);
	}


	hr = gpID3D11Device->CreateHullShader(pID3DBlob_HullShaderCode->GetBufferPointer(),
		pID3DBlob_HullShaderCode->GetBufferSize(),
		NULL, 		//it is for class linkage for variable used accross shaders
		&gpID3D11HullShader);

	if(FAILED(hr))
	{
		pID3DBlob_VertexShaderCode->Release();
		pID3DBlob_VertexShaderCode = NULL;
		pID3DBlob_HullShaderCode->Release();
		pID3DBlob_HullShaderCode = NULL;
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "CreateHullShader() failed For Hull Shader.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "CreateHullShader() Succeeded For Hull Shader.\n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->HSSetShader(gpID3D11HullShader,
		0, 	
		0);	

	const char *domainShaderSourceCode = 
	"cbuffer ConstantBuffer"\
	"{"\
	"float4x4 worldViewProjectionMatrix;"
	"}"\
	"struct hull_output"\
	"{"\
	"float4 position:POSITION;"\
	"};"\
	"struct domain_output"\
	"{"\
	"float4 position:SV_POSITION;"\
	"};"\
	"struct Hull_constant_output"\
	"{"\
	"float edges[2]:SV_TessFactor;"\
	"};"\
	"[domain (\"isoline\")]"\
	"domain_output main(Hull_constant_output input,OutputPatch<hull_output,4> output_patch,float2 tessCord:SV_DOMAINLOCATION)"\
	"{"\
	"domain_output output;"\
	"float u = tessCord.x;"\
	"float3 p0 = output_patch[0].position.xyz;"\
	"float3 p1 = output_patch[1].position.xyz;"\
	"float3 p2 = output_patch[2].position.xyz;"\
	"float3 p3 = output_patch[3].position.xyz;"\
	"float u1 = (1.0 - u);" \
	"float u2 = (u * u);" \
	"float b3 = (u * u2);" \
	"float b2 = (3.0 * u2 * u1);" \
	"float b1 = (3.0 * u * u1 * u1);" \
	"float b0 = (u1 * u1 * u1);" \
	"float3 p = p0*b0 + p1*b1 + p2*b2 + p3*b3 ;"\
	"output.position = mul(worldViewProjectionMatrix,float4(p,1.0));"\
	"return (output);"\
	"}";

	ID3DBlob *pID3DBlob_DomainShaderCode = NULL;
	pID3DBlob_Error = NULL;

	hr = D3DCompile(domainShaderSourceCode,  
		lstrlenA(domainShaderSourceCode)+1,  
		"DS",   
		NULL,	
		D3D_COMPILE_STANDARD_FILE_INCLUDE, 
		"main", 
		"ds_5_0", 
		0,	
		0,	
		&pID3DBlob_DomainShaderCode,   
		&pID3DBlob_Error);	
	if(FAILED(hr))
	{
		if(pID3DBlob_Error !=NULL)
		{
			pID3DBlob_HullShaderCode->Release();
			pID3DBlob_HullShaderCode = NULL;
			pID3DBlob_VertexShaderCode->Release();
			pID3DBlob_VertexShaderCode = NULL;
			fopen_s(&gpFile,gszLogFileName,"a+");
			fprintf_s(gpFile, "D3DCompile() Failed. For Domain Shader: %s\n",(char *)pID3DBlob_Error->GetBufferPointer());
			fclose(gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return(hr);		
		}
		else
		{	
			pID3DBlob_HullShaderCode->Release();
			pID3DBlob_HullShaderCode = NULL;
			pID3DBlob_VertexShaderCode->Release();
			pID3DBlob_VertexShaderCode = NULL;
			fopen_s(&gpFile,gszLogFileName,"a+");
			fprintf_s(gpFile, "D3DCompile() Failed. Due to COM Error Exitting Now...\n");
			fclose(gpFile);
			return(hr);
		}
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "D3DCompile() Succeeded For Domain Shader.\n");
		fclose(gpFile);
	}


	hr = gpID3D11Device->CreateDomainShader(pID3DBlob_DomainShaderCode->GetBufferPointer(),
		
		pID3DBlob_DomainShaderCode->GetBufferSize(),
		
		NULL, 		

		&gpID3D11DomainShader);


	if(FAILED(hr))
	{
		pID3DBlob_VertexShaderCode->Release();
		pID3DBlob_VertexShaderCode = NULL;
		pID3DBlob_HullShaderCode->Release();
		pID3DBlob_HullShaderCode = NULL;
		pID3DBlob_DomainShaderCode->Release();
		pID3DBlob_DomainShaderCode = NULL;
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "CreateDomainShader() failed for Domain Shader.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "CreateDomainShader() done for Domain Shader.\n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->DSSetShader(gpID3D11DomainShader,
		0, 	
		0);	



	


	const char *pixelShaderSourceCode = 
	"cbuffer ConstantBuffer"\
	"{"\
	"float4 lineColor:COLOR;"
	"}"\
	"float4 main(float4 position:SV_POSITION):SV_TARGET"\
	"{"\
	"float4 color = lineColor;"\
	"return(color);"\
	"}";

	ID3DBlob *pID3DBlob_PixelShaderCode = NULL;
	pID3DBlob_Error = NULL;

	hr = D3DCompile(pixelShaderSourceCode, 
		lstrlenA(pixelShaderSourceCode)+1,  
		"PS",   
		NULL,	
		D3D_COMPILE_STANDARD_FILE_INCLUDE, 
		"main", 
		"ps_5_0", 
		0,	
		0,	
		&pID3DBlob_PixelShaderCode,   
		&pID3DBlob_Error);	
	
	if(FAILED(hr))
	{
		if(pID3DBlob_Error !=NULL)
		{
			pID3DBlob_HullShaderCode->Release();
			pID3DBlob_HullShaderCode = NULL;
			pID3DBlob_DomainShaderCode->Release();
			pID3DBlob_DomainShaderCode = NULL;
			pID3DBlob_VertexShaderCode->Release();
			pID3DBlob_VertexShaderCode = NULL;
			fopen_s(&gpFile,gszLogFileName,"a+");
			fprintf_s(gpFile, "D3DCompile() Failed. For Pixel Shader: %s\n",(char *)pID3DBlob_Error->GetBufferPointer());
			fclose(gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return(hr);		
		}
		else
		{
			pID3DBlob_HullShaderCode->Release();
			pID3DBlob_HullShaderCode = NULL;
			pID3DBlob_DomainShaderCode->Release();
			pID3DBlob_DomainShaderCode = NULL;
			pID3DBlob_VertexShaderCode->Release();
			pID3DBlob_VertexShaderCode = NULL;
			fopen_s(&gpFile,gszLogFileName,"a+");
			fprintf_s(gpFile, "D3DCompile() Failed For Pixel Shader. Due to COM Error Exitting Now...\n");
			fclose(gpFile);
			return(hr);
		}
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "D3DCompile() Succeeded For Pixel Shader.\n");
		fclose(gpFile);
	}


	hr = gpID3D11Device->CreatePixelShader(pID3DBlob_PixelShaderCode->GetBufferPointer(),
		pID3DBlob_PixelShaderCode->GetBufferSize(),
		NULL, 		
		&gpID3D11PixelShader);

	if(FAILED(hr))
	{
		pID3DBlob_HullShaderCode->Release();
		pID3DBlob_HullShaderCode = NULL;
		pID3DBlob_DomainShaderCode->Release();
		pID3DBlob_DomainShaderCode = NULL;
		pID3DBlob_VertexShaderCode->Release();
		pID3DBlob_VertexShaderCode = NULL;
		pID3DBlob_PixelShaderCode->Release();
		pID3DBlob_PixelShaderCode = NULL;
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "CreatePixelShader() Failed For Pixel Shader. Exitting Now...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "CreatePixelShader() Succeeded For Pixel Shader.\n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->PSSetShader(gpID3D11PixelShader,
		0, 	
		0);	

	D3D11_INPUT_ELEMENT_DESC inputElementDesc[1];
	inputElementDesc[0].SemanticName = "POSITION";
	inputElementDesc[0].SemanticIndex = 0;
	inputElementDesc[0].Format = DXGI_FORMAT_R32G32_FLOAT;
	inputElementDesc[0].InputSlot = 0;
	inputElementDesc[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputElementDesc[0].AlignedByteOffset = 0;
	inputElementDesc[0].InstanceDataStepRate = 0;

	hr = gpID3D11Device->CreateInputLayout(inputElementDesc,
		1, 
		pID3DBlob_VertexShaderCode->GetBufferPointer(),
		pID3DBlob_VertexShaderCode->GetBufferSize(),
		&gpID3D11InputLayout
	);
	
	if(FAILED(hr))
	{
		pID3DBlob_HullShaderCode->Release();
		pID3DBlob_HullShaderCode = NULL;
		pID3DBlob_DomainShaderCode->Release();
		pID3DBlob_DomainShaderCode = NULL;
		pID3DBlob_VertexShaderCode->Release();
		pID3DBlob_VertexShaderCode = NULL;
		pID3DBlob_PixelShaderCode->Release();
		pID3DBlob_PixelShaderCode = NULL;
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "ID3D11Device::CreateInputLayout() Failed. Exitting Now...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "ID3D11Device::CreateInputLayout() Succeeded.\n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->IASetInputLayout(gpID3D11InputLayout);

	pID3DBlob_HullShaderCode->Release();
	pID3DBlob_HullShaderCode = NULL;
	pID3DBlob_DomainShaderCode->Release();
	pID3DBlob_DomainShaderCode = NULL;
	pID3DBlob_VertexShaderCode->Release();
	pID3DBlob_VertexShaderCode = NULL;
	pID3DBlob_PixelShaderCode->Release();
	pID3DBlob_PixelShaderCode = NULL;


	float vertices_line[]
	{
		-1.0f,-1.0f,
		-0.5f,1.0f,
		0.5f,-1.0f,
		1.0f,1.0f
 	};

 	D3D11_BUFFER_DESC bufferDesc;
 	ZeroMemory(&bufferDesc,sizeof(D3D11_BUFFER_DESC));
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc.ByteWidth = sizeof(float) * ARRAYSIZE(vertices_line);
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	
	hr = gpID3D11Device->CreateBuffer(&bufferDesc,
		nullptr,
		&gpID3D11Buffer_VertexBuffer_Position);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() failed For Vertex Buffer.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() done For Vertex Buffer.\n");
		fclose(gpFile);
	}

	D3D11_MAPPED_SUBRESOURCE mappedSubResource;
	ZeroMemory(&mappedSubResource, sizeof(D3D11_MAPPED_SUBRESOURCE));
	gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Position, 0,
		D3D11_MAP_WRITE_DISCARD, 0, &mappedSubResource);
	memcpy(mappedSubResource.pData, vertices_line, sizeof(vertices_line));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Position, 0);

	D3D11_BUFFER_DESC bufferDesc_ConstantBuffer_HullShader;
	ZeroMemory(&bufferDesc_ConstantBuffer_HullShader,sizeof(D3D11_BUFFER_DESC));
	bufferDesc_ConstantBuffer_HullShader.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc_ConstantBuffer_HullShader.ByteWidth = sizeof(CBUFFER_HULLSHADER);
	bufferDesc_ConstantBuffer_HullShader.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

	hr = gpID3D11Device->CreateBuffer(&bufferDesc_ConstantBuffer_HullShader,
		nullptr,
		&gpID3D11Buffer_ConstantBuffer_HullShader);

	if(FAILED(hr))
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Failed For Constant Buffer For Hull Shader. Exitting Now...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Succeeded For Constant Buffer For Hull Shader.\n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->HSSetConstantBuffers(0,	
		1,	
		&gpID3D11Buffer_ConstantBuffer_HullShader);


	//Domain SHADER CONSTANT BUFFER
	D3D11_BUFFER_DESC bufferDesc_ConstantBuffer_DomainShader;
	ZeroMemory(&bufferDesc_ConstantBuffer_DomainShader,sizeof(D3D11_BUFFER_DESC));
	bufferDesc_ConstantBuffer_DomainShader.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc_ConstantBuffer_DomainShader.ByteWidth = sizeof(CBUFFER_DOMAINSHADER);
	bufferDesc_ConstantBuffer_DomainShader.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

	hr = gpID3D11Device->CreateBuffer(&bufferDesc_ConstantBuffer_DomainShader,
		nullptr,
		&gpID3D11Buffer_ConstantBuffer_DomainShader);

	if(FAILED(hr))
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Failed For Constant Buffer For Domain Shader. Exitting Now...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() done for Constant Buffer For Domain Shader.\n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->DSSetConstantBuffers(0,
		1,	
		&gpID3D11Buffer_ConstantBuffer_DomainShader);

	D3D11_BUFFER_DESC bufferDesc_ConstantBuffer_PixelShader;
	ZeroMemory(&bufferDesc_ConstantBuffer_PixelShader,sizeof(D3D11_BUFFER_DESC));
	bufferDesc_ConstantBuffer_PixelShader.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc_ConstantBuffer_PixelShader.ByteWidth = sizeof(CBUFFER_PIXELSHADER);
	bufferDesc_ConstantBuffer_PixelShader.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

	hr = gpID3D11Device->CreateBuffer(&bufferDesc_ConstantBuffer_PixelShader,
		nullptr,
		&gpID3D11Buffer_ConstantBuffer_PixelShader);

	if(FAILED(hr))
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() done for Constant Buffer For Pixel Shader. Exitting Now...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() done for Constant Buffer For Pixel Shader.\n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->PSSetConstantBuffers(0,
		1,	
		&gpID3D11Buffer_ConstantBuffer_PixelShader);


	gClearColor[0] = 0.0f;
	gClearColor[1] = 0.0f;
	gClearColor[2] = 0.0f;
	gClearColor[3] = 0.0f;

	gPerspectiveProjectionMatrix = XMMatrixIdentity();

	hr =resize(WIN_WIDTH, WIN_HEIGHT);
	if(FAILED(hr))
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "resize() failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "resize() done.\n");
		fclose(gpFile);
	}

	return(S_OK);
}

HRESULT resize(int width,int height)
{
	HRESULT hr = S_OK;

	if(gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	gpIDXGISwapChain->ResizeBuffers(1,width,height,DXGI_FORMAT_R8G8B8A8_UNORM,0);

	ID3D11Texture2D *pID3D11Texture2D_BackBuffer;
	gpIDXGISwapChain->GetBuffer(0,__uuidof(ID3D11Texture2D),(LPVOID *)&pID3D11Texture2D_BackBuffer);

	hr = gpID3D11Device->CreateRenderTargetView(pID3D11Texture2D_BackBuffer,NULL,&gpID3D11RenderTargetView);
	if(FAILED(hr))
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "ID3D11Device::CreateRenderTargetView() Failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "ID3D11Device::CreateRenderTargetView() Succeeded.\n");
		fclose(gpFile);
	}

	pID3D11Texture2D_BackBuffer->Release();
	pID3D11Texture2D_BackBuffer= NULL;

	gpID3D11DeviceContext->OMSetRenderTargets(1,&gpID3D11RenderTargetView,NULL);


	D3D11_VIEWPORT d3dViewPort;
	d3dViewPort.TopLeftX = 0;
	d3dViewPort.TopLeftY = 0;
	d3dViewPort.Width = (float)width;
	d3dViewPort.Height = (float)height;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;

	gpID3D11DeviceContext->RSSetViewports(1,&d3dViewPort);


	if (width >= height)
		gPerspectiveProjectionMatrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0f), (float)width / (float)height, 0.1f, 100.0f);
	else
		gPerspectiveProjectionMatrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0f), (float)height / (float)width, 0.1f, 100.0f);
	return(hr);
}

void display(void)
{
	gpID3D11DeviceContext->ClearRenderTargetView(gpID3D11RenderTargetView,gClearColor);

	UINT stride = sizeof(float) * 2;
	UINT offset = 0;
	
	gpID3D11DeviceContext->IASetVertexBuffers(0,
		1,  
		&gpID3D11Buffer_VertexBuffer_Position,
		&stride,
		&offset);

	gpID3D11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_4_CONTROL_POINT_PATCHLIST);

	XMMATRIX worldMatrix = XMMatrixIdentity();
	XMMATRIX viewMatrix = XMMatrixIdentity();

	worldMatrix = XMMatrixTranslation(0.0f, 0.0f, 3.0f);
	XMMATRIX wvpMatrix = worldMatrix * viewMatrix * gPerspectiveProjectionMatrix;

	CBUFFER_DOMAINSHADER constantBuffer;
	constantBuffer.WorldViewProjectionMatrix = wvpMatrix;
	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer_DomainShader,
		0,	
		NULL, 
		&constantBuffer,
		0, 
		0 );		

	TCHAR str[255];
	wsprintf(str,TEXT("Direct3D Window: [Segments = %d]"),gNumberOfSegments);
	SetWindowText(ghwnd,str);

	CBUFFER_HULLSHADER constantBuffer_Hull;
	constantBuffer_Hull.Hull_Constant_Function_Param = XMVectorSet(1,gNumberOfSegments, 0.0, 0.0);
	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer_HullShader,
		0,	
		NULL, 
		&constantBuffer_Hull,
		0, 
		0 );	

	CBUFFER_PIXELSHADER constantBuffer_Pixel;
	constantBuffer_Pixel.LineColor = XMVectorSet(0.0,0.0, 1.0, 0.0);
	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer_PixelShader,
		0,	
		NULL, 
		&constantBuffer_Pixel,
		0, 
		0 );	



	gpID3D11DeviceContext->Draw(4,0);
	

	gpIDXGISwapChain->Present(0,0);
}

void uninitialize(void)
{
	if (gpID3D11Buffer_ConstantBuffer_DomainShader)
	{
		gpID3D11Buffer_ConstantBuffer_DomainShader->Release();
		gpID3D11Buffer_ConstantBuffer_DomainShader = NULL;
	}

	if (gpID3D11Buffer_ConstantBuffer_HullShader)
	{
		gpID3D11Buffer_ConstantBuffer_HullShader->Release();
		gpID3D11Buffer_ConstantBuffer_HullShader = NULL;
	}

	if (gpID3D11Buffer_ConstantBuffer_PixelShader)
	{
		gpID3D11Buffer_ConstantBuffer_PixelShader->Release();
		gpID3D11Buffer_ConstantBuffer_PixelShader = NULL;
	}

	if (gpID3D11InputLayout)
	{
		gpID3D11InputLayout->Release();
		gpID3D11InputLayout = NULL;
	}

	if (gpID3D11Buffer_VertexBuffer_Position)
	{
		gpID3D11Buffer_VertexBuffer_Position->Release();
		gpID3D11Buffer_VertexBuffer_Position = NULL;
	}

	if (gpID3D11PixelShader)
	{
		gpID3D11PixelShader->Release();
		gpID3D11PixelShader = NULL;
	}

	if (gpID3D11DomainShader)
	{
		gpID3D11DomainShader->Release();
		gpID3D11DomainShader = NULL;
	}

	if (gpID3D11HullShader)
	{
		gpID3D11HullShader->Release();
		gpID3D11HullShader = NULL;
	}
	
	if (gpID3D11VertexShader)
	{
		gpID3D11VertexShader->Release();
		gpID3D11VertexShader = NULL;
	}
	
	if(gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;	
	}

	if(gpIDXGISwapChain)
	{
		gpIDXGISwapChain->Release();
		gpIDXGISwapChain = NULL;	
	}

	if(gpID3D11DeviceContext)
	{
		gpID3D11DeviceContext->Release();
		gpID3D11DeviceContext = NULL;	
	}

	if(gpID3D11Device)
	{
		gpID3D11Device->Release();
		gpID3D11Device = NULL;	
	}

	if(gpFile)
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "uninitialize done\n");
		fprintf_s(gpFile, "log file closed\n");
		fclose(gpFile);	
	}
}