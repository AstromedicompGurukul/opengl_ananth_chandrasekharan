#include <windows.h>
#include <stdio.h>

#include <d3d11.h>
#include <d3dcompiler.h>

#pragma warning (disable : 4838) 
#include "XNAMATH\xnamath.h"
#include "WICTextureLoader.h"

#pragma comment(lib,"d3d11.lib")
#pragma comment(lib,"D3dcompiler.lib")
#pragma comment(lib,"DirectXTK.lib") 

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

FILE *gpFile = NULL;
char gszLogFileName[]="Log.txt";

HWND ghwnd = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = {sizeof(WINDOWPLACEMENT)};

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

float gClearColor[4];
IDXGISwapChain *gpIDXGISwapChain = NULL;
ID3D11Device *gpID3D11Device = NULL;
ID3D11DeviceContext *gpID3D11DeviceContext = NULL;
ID3D11RenderTargetView *gpID3D11RenderTargetView = NULL;

ID3D11VertexShader *gpID3D11VertexShader = NULL;
ID3D11PixelShader *gpID3D11PixelShader = NULL;
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Cube_VCNT = NULL; 
ID3D11Buffer *gpID3D11Buffer_ConstantBuffer = NULL;  
ID3D11InputLayout *gpID3D11InputLayout =NULL;

ID3D11RasterizerState *gpID3D11RasterizerState = NULL;
ID3D11DepthStencilView *gpID3D11DepthStencilView = NULL;


ID3D11ShaderResourceView *gpID3D11ShaderResourceView_Texture_Cube = NULL;
ID3D11SamplerState *gpID3D11SamplerState_Texture_Cube = NULL;


bool gbLight;
bool bIsLkeyPressed;

struct CBUFFER
{
	XMMATRIX ModelMatrix;
	XMMATRIX ViewMatrix;
	XMMATRIX ProjectionMatrix;
	XMVECTOR u_La;
	XMVECTOR u_Ld;
	XMVECTOR u_Ls;
	XMVECTOR u_Ka;
	XMVECTOR u_Kd;
	XMVECTOR u_Ks;
	float u_Material_shininess;
	XMVECTOR u_light_position;
	unsigned int u_LKeyPressed;
};

XMMATRIX gPerspectiveProjectionMatrix;



float lightAmbient[] = {0.0f,0.0f,0.0f,1.0f};
float lightDiffuse[]= {1.0f,1.0f,1.0f,1.0f};
float lightSpecular[] = {1.0f,1.0f,1.0f,1.0f};
float lightPosition[] = {100.0f,0.0f,-100.0f,1.0f};

float material_Ambient[] = {0.0f,0.0f,0.0f,1.0f};
float material_Diffuse[]= {1.0f,1.0f,1.0f,1.0f};
float material_Specular[] = {1.0f,1.0f,1.0f,1.0f};
float material_shininess = 128.0f;


float angle_cube = 0.0f;

int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpszCmdLine,int iCmdShow)
{
	HRESULT initialize(void);
	void uninitialize(void);
	void display(void);

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[]= TEXT("Shree Ganesha : Interleaved");
	bool bDone = false;
	

	if(fopen_s(&gpFile,gszLogFileName,"w") !=0)
	{
		MessageBox(NULL,TEXT("Log file not created"),TEXT("Error"),MB_OK|MB_TOPMOST|MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf_s(gpFile, "Log file Is opened.\n");
		fclose(gpFile);
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW |CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL,IDC_ARROW);
	wndclass.lpszMenuName = NULL;
	wndclass.lpszClassName = szClassName;
	wndclass.hIconSm = LoadIcon(NULL,IDI_APPLICATION);

	RegisterClassEx(&wndclass);

	hwnd = CreateWindow(szClassName,
		TEXT("Shree Ganesha : Interleaved"),
		WS_OVERLAPPEDWINDOW,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd,iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	HRESULT hr;
	hr = initialize();
	if(FAILED(hr))
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "initialize() failed.\n");
		fclose(gpFile);
		hwnd = NULL;
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "initialize() done.\n");
		fclose(gpFile);
	}

	while(bDone == false)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			display();

			if(gbActiveWindow == true)
			{
				if(gbEscapeKeyIsPressed ==true)
					bDone = true;
			}
		}
	}

	uninitialize();

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd,UINT iMsg,WPARAM wParam,LPARAM lParam)
{
	HRESULT resize(int,int);
	void ToggleFulscreen(void);
	void uninitialize(void);

	HRESULT hr;

	switch(iMsg)
	{
		case WM_ACTIVATE:
			if(HIWORD(wParam) == 0)
				gbActiveWindow = true;
			else
				gbActiveWindow = false;
			break;
		case WM_ERASEBKGND:
			return(0);
		case WM_SIZE:
			if(gpID3D11DeviceContext)
			{
				hr = resize(LOWORD(lParam),HIWORD(lParam));
				if(FAILED(hr))
				{
					fopen_s(&gpFile,gszLogFileName,"a+");
					fprintf_s(gpFile, "resize() failed.\n");
					fclose(gpFile);
					return(hr);
				}
				else
				{
					fopen_s(&gpFile,gszLogFileName,"a+");
					fprintf_s(gpFile, "resize() done\n");
					fclose(gpFile);
				}
			}
			break;
		case WM_KEYDOWN:
			switch(wParam)
			{
				case VK_ESCAPE:
					if(gbEscapeKeyIsPressed == false)
						gbEscapeKeyIsPressed = true;
					break;
				case 0x46: //F or f
					if(gbFullscreen == false)
					{
						ToggleFulscreen();
						gbFullscreen = true;
					}
					else
					{
						ToggleFulscreen();
						gbFullscreen = false;
					}
					break;
				case 0x4C:  //L or l
					if (bIsLkeyPressed == false)
					{
						gbLight = true;
						bIsLkeyPressed = true;
					}
					else
					{
						gbLight = false;
						bIsLkeyPressed = false;
					}
					break;

				default:
					break;
			}
			break;
		case WM_LBUTTONDOWN:
			break;
		case WM_CLOSE:
			uninitialize();
			break;
		case WM_DESTROY:
			PostQuitMessage(0);
		default:
			break;
	}

	return(DefWindowProc(hwnd,iMsg,wParam,lParam));
}


void ToggleFulscreen(void)
{
	MONITORINFO mi;

	if(gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd,GWL_STYLE);
		if(dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = {sizeof(MONITORINFO)};
			if(GetWindowPlacement(ghwnd,&wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY),&mi))
			{
				SetWindowLong(ghwnd,GWL_STYLE,dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(ghwnd,HWND_TOP,mi.rcMonitor.left,mi.rcMonitor.top,mi.rcMonitor.right - mi.rcMonitor.left,mi.rcMonitor.bottom - mi.rcMonitor.top,SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd,&wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_NOMOVE| SWP_NOSIZE| SWP_NOOWNERZORDER|SWP_NOZORDER|SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

HRESULT initialize(void)
{
	void uninitialize(void);
	HRESULT resize(int,int);
	HRESULT LoadD3DTexture(const wchar_t *, ID3D11ShaderResourceView **);

	HRESULT hr;
	D3D_DRIVER_TYPE d3dDriverType;
	D3D_DRIVER_TYPE d3dDriverTypes[] = {
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_WARP,
		D3D_DRIVER_TYPE_REFERENCE,
	};

	D3D_FEATURE_LEVEL d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_10_0;

	UINT createDeviceFlags = 0;
	UINT numDriverTypes = 0;
	UINT numFeatureLevels = 1;

	numDriverTypes = sizeof(d3dDriverTypes)/sizeof(d3dDriverTypes[0]);

	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
	ZeroMemory((void *)&dxgiSwapChainDesc,sizeof(DXGI_SWAP_CHAIN_DESC));

	dxgiSwapChainDesc.BufferCount =1;
	dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;
	dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
	dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	dxgiSwapChainDesc.OutputWindow = ghwnd;
	dxgiSwapChainDesc.SampleDesc.Count = 1;
	dxgiSwapChainDesc.SampleDesc.Quality = 0;
	dxgiSwapChainDesc.Windowed = TRUE;

	for(UINT driverTypeIndex = 0;driverTypeIndex < numDriverTypes;driverTypeIndex++)
	{
		d3dDriverType = d3dDriverTypes[driverTypeIndex];
		hr = D3D11CreateDeviceAndSwapChain(
			NULL,
			d3dDriverType,
			NULL,
			createDeviceFlags,
			&d3dFeatureLevel_required,
			numFeatureLevels,
			D3D11_SDK_VERSION,
			&dxgiSwapChainDesc,
			&gpIDXGISwapChain,
			&gpID3D11Device,
			&d3dFeatureLevel_acquired,
			&gpID3D11DeviceContext);

		if(SUCCEEDED(hr))
		{
			break;
		}
	}

	if(FAILED(hr))
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain() failed\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain() done\n");
		fprintf_s(gpFile, "The Chosen Driver is of ");
		if(d3dDriverType == D3D_DRIVER_TYPE_HARDWARE)
		{
			fprintf_s(gpFile, "Hardware Type \n");
		}
		else if(d3dDriverType == D3D_DRIVER_TYPE_WARP)
		{
			fprintf_s(gpFile, "WARP Type \n");
		}
		else if(d3dDriverType == D3D_DRIVER_TYPE_REFERENCE)
		{
			fprintf_s(gpFile, "Reference Type \n");
		}
		else
		{
			fprintf_s(gpFile, "Unknown Type \n");
		}

		fprintf_s(gpFile, "The Supported highest feature level is");
		if(d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_11_0)
		{
			fprintf_s(gpFile, "11.0 \n");
		}
		else if(d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_0)
		{
			fprintf_s(gpFile, "10.0\n");
		}
		else
		{
			fprintf_s(gpFile, "Unknown.\n");
		}
		fclose(gpFile);
	}


	const char *vertexShaderSourceCode = 
	"cbuffer ConstantBuffer"\
	"{"\
	"float4x4 modelMatrix;"\
	"float4x4 viewMatrix;"\
	"float4x4 projectionMatrix;"\
	"float4 u_La;"\
	"float4 u_Ld;"\
	"float4 u_Ls;"\
	"float4 u_Ka;"\
	"float4 u_Kd;"\
	"float4 u_Ks;"\
	"float u_Material_shininess;"\
	"float4 u_light_position;"\
	"uint u_LKeyPressed;"\
	"}"\
	"struct vertex_output"\
	"{"\
	"float4 position:SV_POSITION;"\
	"float2 texcoord : TEXCOORD;"\
	"float3 color:COLOR;"\
	"float3 transformed_normals : NORMAL0;"\
	"float3 light_direction : NORMAL1;"\
	"float3 viewer_vector : NORMAL2;"\
	"};"\
	"vertex_output main(float4 pos:POSITION,float3 color:COLOR,float3 normal:NORMAL,float2 texcoord:TEXCOORD)"\
	"{"\
	"vertex_output output;"\
	"if(u_LKeyPressed == 1)"
	"{"\
	"float4 eyeCoordinates = mul(modelMatrix,pos);"\
	"eyeCoordinates = mul(viewMatrix,eyeCoordinates);"\
	"output.transformed_normals =mul((float3x3)mul(modelMatrix,viewMatrix),(float3)normal);"\
	"output.light_direction = (float3) (u_light_position - eyeCoordinates);"\
	"output.viewer_vector = -eyeCoordinates.xyz;"\
	"}"\
	"float4 world = mul(modelMatrix , pos);"\
	"float4 worldView = mul(viewMatrix , world);"\
	"output.position = mul(projectionMatrix , worldView);"\
	"output.texcoord = texcoord;"\
	"output.color = color;"\
	"return (output);"\
	"}";

	ID3DBlob *pID3DBlob_VertexShaderCode = NULL;
	ID3DBlob *pID3DBlob_Error = NULL;

	hr = D3DCompile(vertexShaderSourceCode,  //shader source code
		lstrlenA(vertexShaderSourceCode)+1,  // length of source code as it does not consider length with null termination add 1 to it.
		"VS",   //shader stage
		NULL,	//This is D3D_SHADER_MACRO used when there is #define is there in shader
		D3D_COMPILE_STANDARD_FILE_INCLUDE, //Import standard files
		"main", //entry point function in  shader
		"vs_5_0", //feature level of shader
		0,	//compiler constants like debug,optimise,validation etc..
		0,	//effects constants like debug,optimise,validation etc..
		&pID3DBlob_VertexShaderCode,   //blob to hold intermediate bytecode
		&pID3DBlob_Error);	//blob to hold error code if any.
	if(FAILED(hr))
	{
		if(pID3DBlob_Error !=NULL)
		{
			fopen_s(&gpFile,gszLogFileName,"a+");
			fprintf_s(gpFile, "D3DCompile() Failed. For Vertex Shader: %s\n",(char *)pID3DBlob_Error->GetBufferPointer());
			fclose(gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return(hr);		
		}
		else
		{
			fopen_s(&gpFile,gszLogFileName,"a+");
			fprintf_s(gpFile, "D3DCompile() Failed. Due to COM Error Exitting Now...\n");
			fclose(gpFile);
			return(hr);
		}
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "D3DCompile() Succeeded For Vertex Shader.\n");
		fclose(gpFile);
	}


	hr = gpID3D11Device->CreateVertexShader(pID3DBlob_VertexShaderCode->GetBufferPointer(),
		pID3DBlob_VertexShaderCode->GetBufferSize(),
		NULL, 		
		&gpID3D11VertexShader);

	if(FAILED(hr))
	{
		pID3DBlob_VertexShaderCode->Release();
		pID3DBlob_VertexShaderCode = NULL;
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "CreateVertexShader() failed For Vertex Shader.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "CreateVertexShader() done For Vertex Shader.\n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShader,
		0, 	
		0);	


	const char *pixelShaderSourceCode =
		"Texture2D myTexture2D;"\
		"SamplerState mySamplerState;"\
		"cbuffer ConstantBuffer"\
		"{"\
		"float4x4 modelMatrix;"\
		"float4x4 viewMatrix;"\
		"float4x4 projectionMatrix;"\
		"float4 u_La;"\
		"float4 u_Ld;"\
		"float4 u_Ls;"\
		"float4 u_Ka;"\
		"float4 u_Kd;"\
		"float4 u_Ks;"\
		"float u_Material_shininess;"\
		"float4 u_light_position;"\
		"uint u_LKeyPressed;"\
		"}"\
		"struct vertex_output"\
		"{"\
		"float4 position:SV_POSITION;"\
		"float2 texcoord : TEXCOORD;"\
		"float3 color:COLOR;"\
		"float3 transformed_normals : NORMAL0;"\
		"float3 light_direction : NORMAL1;"\
		"float3 viewer_vector : NORMAL2;"\
		"};"\
		"float4 main(float4 pos:SV_POSITION,vertex_output input):SV_TARGET"\
		"{"\
		"float4 phong_ads_color;"\
		"if(u_LKeyPressed == 1)"
		"{"\
		"float3 normalized_transformed_normals = normalize(input.transformed_normals);"\
		"float3 normalized_light_direction = normalize(input.light_direction);"\
		"float3 normalized_viewer_vector = normalize(input.viewer_vector);"\
		"float tn_dot_ld = max(dot(normalized_transformed_normals,normalized_light_direction),0.0);"\
		"float3 ambient = u_La * u_Ka;"\
		"float3 diffuse = u_Ld * u_Kd * tn_dot_ld;"\
		"float3 reflection_vector = reflect(-normalized_light_direction,normalized_transformed_normals);"\
		"float3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector,normalized_viewer_vector),0.0),u_Material_shininess);"\
		"phong_ads_color = float4(ambient + diffuse + specular,1.0) * float4(input.color,1.0) * myTexture2D.Sample(mySamplerState,input.texcoord);"
		"}"\
		"else"\
		"{"\
		"phong_ads_color =  float4(input.color,1.0) * myTexture2D.Sample(mySamplerState,input.texcoord);"\
		"}"\
		"return(phong_ads_color);"\
		"}";

	ID3DBlob *pID3DBlob_PixelShaderCode = NULL;
	pID3DBlob_Error = NULL;

	hr = D3DCompile(pixelShaderSourceCode,  //shader source code
		lstrlenA(pixelShaderSourceCode)+1,  // length of source code as it does not consider length with null termination add 1 to it.
		"PS",   //shader stage
		NULL,	//This is D3D_SHADER_MACRO used when there is #define is there in shader
		D3D_COMPILE_STANDARD_FILE_INCLUDE, //Import standard files
		"main", //entry point function in  shader
		"ps_5_0", //feature level of shader
		0,	//compiler constants like debug,optimise,validation etc..
		0,	//effects constants like debug,optimise,validation etc..
		&pID3DBlob_PixelShaderCode,   //blob to hold intermediate bytecode
		&pID3DBlob_Error);	//blob to hold error code if any.
	
	if(FAILED(hr))
	{
		if(pID3DBlob_Error !=NULL)
		{
			pID3DBlob_VertexShaderCode->Release();
			pID3DBlob_VertexShaderCode = NULL;
			fopen_s(&gpFile,gszLogFileName,"a+");
			fprintf_s(gpFile, "D3DCompile() Failed. for Pixel Shader: %s\n",(char *)pID3DBlob_Error->GetBufferPointer());
			fclose(gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return(hr);		
		}
		else
		{
			pID3DBlob_VertexShaderCode->Release();
			pID3DBlob_VertexShaderCode = NULL;
			fopen_s(&gpFile,gszLogFileName,"a+");
			fprintf_s(gpFile, "D3DCompile() failed For Pixel Shader. Due to COM Error Exitting Now...\n");
			fclose(gpFile);
			return(hr);
		}
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "D3DCompile() done For Pixel Shader.\n");
		fclose(gpFile);
	}


	hr = gpID3D11Device->CreatePixelShader(pID3DBlob_PixelShaderCode->GetBufferPointer(),
		pID3DBlob_PixelShaderCode->GetBufferSize(),
		NULL, 		//it is for class linkage for variable used accross shaders
		&gpID3D11PixelShader);

	if(FAILED(hr))
	{
		pID3DBlob_VertexShaderCode->Release();
		pID3DBlob_VertexShaderCode = NULL;
		pID3DBlob_PixelShaderCode->Release();
		pID3DBlob_PixelShaderCode = NULL;
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "CreatePixelShader() failed For Pixel Shader. Exitting Now...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "CreatePixelShader() done For Pixel Shader.\n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->PSSetShader(gpID3D11PixelShader,
		0, 	
		0);	

	


	D3D11_INPUT_ELEMENT_DESC inputElementDesc[4];

	inputElementDesc[0].SemanticName = "POSITION";
	inputElementDesc[0].SemanticIndex = 0;
	inputElementDesc[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	inputElementDesc[0].InputSlot = 0;
	inputElementDesc[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputElementDesc[0].AlignedByteOffset = 0;
	inputElementDesc[0].InstanceDataStepRate = 0;
	
	inputElementDesc[1].SemanticName = "COLOR";
	inputElementDesc[1].SemanticIndex = 0;
	inputElementDesc[1].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	inputElementDesc[1].InputSlot = 1;
	inputElementDesc[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputElementDesc[1].AlignedByteOffset = 0;
	inputElementDesc[1].InstanceDataStepRate = 0;


	inputElementDesc[2].SemanticName = "NORMAL";
	inputElementDesc[2].SemanticIndex = 0;
	inputElementDesc[2].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	inputElementDesc[2].InputSlot = 2;
	inputElementDesc[2].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputElementDesc[2].AlignedByteOffset = 0;
	inputElementDesc[2].InstanceDataStepRate = 0;


	inputElementDesc[3].SemanticName = "TEXCOORD";
	inputElementDesc[3].SemanticIndex = 0;
	inputElementDesc[3].Format = DXGI_FORMAT_R32G32_FLOAT;
	inputElementDesc[3].InputSlot = 3;
	inputElementDesc[3].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputElementDesc[3].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	inputElementDesc[3].InstanceDataStepRate = 0;

	hr = gpID3D11Device->CreateInputLayout(inputElementDesc,
		4, 
		pID3DBlob_VertexShaderCode->GetBufferPointer(),
		pID3DBlob_VertexShaderCode->GetBufferSize(),
		&gpID3D11InputLayout
	);
	
	if(FAILED(hr))
	{
		pID3DBlob_VertexShaderCode->Release();
		pID3DBlob_VertexShaderCode = NULL;
		pID3DBlob_PixelShaderCode->Release();
		pID3DBlob_PixelShaderCode = NULL;
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "ID3D11Device::CreateInputLayout() failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "ID3D11Device::CreateInputLayout() done.\n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->IASetInputLayout(gpID3D11InputLayout);

	pID3DBlob_VertexShaderCode->Release();
	pID3DBlob_VertexShaderCode = NULL;
	pID3DBlob_PixelShaderCode->Release();
	pID3DBlob_PixelShaderCode = NULL;


	//Cube VBO
	float CUBE_VCNT[]
	{
		-1.0f,+1.0f,+1.0f,1.0f,0.0f,0.0f,0.0f,1.0f,0.0f,+0.0f,+0.0f,
		+1.0f,+1.0f,+1.0f,1.0f,0.0f,0.0f,0.0f,1.0f,0.0f,+0.0f,+1.0f,
		-1.0f,+1.0f,-1.0f,1.0f,0.0f,0.0f,0.0f,1.0f,0.0f,+1.0f,+0.0f,
		-1.0f,+1.0f,-1.0f,1.0f,0.0f,0.0f,0.0f,1.0f,0.0f,+1.0f,+0.0f,
		+1.0f,+1.0f,+1.0f,1.0f,0.0f,0.0f,0.0f,1.0f,0.0f,+0.0f,+1.0f,
		+1.0f,+1.0f,-1.0f,1.0f,0.0f,0.0f,0.0f,1.0f,0.0f,+1.0f,+1.0f,
		+1.0f,-1.0f,-1.0f,0.0f,1.0f,0.0f,0.0f,-1.0f,0.0f,+0.0f,+0.0f,
		+1.0f,-1.0f,+1.0f,0.0f,1.0f,0.0f,0.0f,-1.0f,0.0f,+0.0f,+1.0f,
		-1.0f,-1.0f,-1.0f,0.0f,1.0f,0.0f,0.0f,-1.0f,0.0f,+1.0f,+0.0f,
		-1.0f,-1.0f,-1.0f,0.0f,1.0f,0.0f,0.0f,-1.0f,0.0f,+1.0f,+0.0f,
		+1.0f,-1.0f,+1.0f,0.0f,1.0f,0.0f,0.0f,-1.0f,0.0f,+0.0f,+1.0f,
		-1.0f,-1.0f,+1.0f,0.0f,1.0f,0.0f,0.0f,-1.0f,0.0f,+1.0f,+1.0f,
		-1.0f,+1.0f,-1.0f,0.0f,0.0f,1.0f,0.0f,0.0f,-1.0f,+0.0f,+0.0f,
		+1.0f,+1.0f,-1.0f,0.0f,0.0f,1.0f,0.0f,0.0f,-1.0f,+0.0f,+1.0f,
		-1.0f,-1.0f,-1.0f,0.0f,0.0f,1.0f,0.0f,0.0f,-1.0f,+1.0f,+0.0f,
		-1.0f,-1.0f,-1.0f,0.0f,0.0f,1.0f,0.0f,0.0f,-1.0f,+1.0f,+0.0f,
		+1.0f,+1.0f,-1.0f,0.0f,0.0f,1.0f,0.0f,0.0f,-1.0f,+0.0f,+1.0f,
		+1.0f,-1.0f,-1.0f,0.0f,0.0f,1.0f,0.0f,0.0f,-1.0f,+1.0f,+1.0f,
		+1.0f,-1.0f,+1.0f,0.0f,1.0f,1.0f,0.0f,0.0f,1.0f,+0.0f,+0.0f,
		+1.0f,+1.0f,+1.0f,0.0f,1.0f,1.0f,0.0f,0.0f,1.0f,+0.0f,+1.0f,
		-1.0f,-1.0f,+1.0f,0.0f,1.0f,1.0f,0.0f,0.0f,1.0f,+1.0f,+0.0f,
		-1.0f,-1.0f,+1.0f,0.0f,1.0f,1.0f,0.0f,0.0f,1.0f,+1.0f,+0.0f,
		+1.0f,+1.0f,+1.0f,0.0f,1.0f,1.0f,0.0f,0.0f,1.0f,+0.0f,+1.0f,
		-1.0f,+1.0f,+1.0f,0.0f,1.0f,1.0f,0.0f,0.0f,1.0f,+1.0f,+1.0f,
		-1.0f,+1.0f,+1.0f,1.0f,0.0f,1.0f,-1.0f,0.0f,0.0f,+0.0f,+0.0f,
		-1.0f,+1.0f,-1.0f,1.0f,0.0f,1.0f,-1.0f,0.0f,0.0f,+0.0f,+1.0f,
		-1.0f,-1.0f,+1.0f,1.0f,0.0f,1.0f,-1.0f,0.0f,0.0f,+1.0f,+0.0f,
		-1.0f,-1.0f,+1.0f,1.0f,0.0f,1.0f,-1.0f,0.0f,0.0f,+1.0f,+0.0f,
		-1.0f,+1.0f,-1.0f,1.0f,0.0f,1.0f,-1.0f,0.0f,0.0f,+0.0f,+1.0f,
		-1.0f,-1.0f,-1.0f,1.0f,0.0f,1.0f,-1.0f,0.0f,0.0f,+1.0f,+1.0f,
		+1.0f,-1.0f,-1.0f,1.0f,1.0f,0.0f,1.0f,0.0f,0.0f,+0.0f,+0.0f,
		+1.0f,+1.0f,-1.0f,1.0f,1.0f,0.0f,1.0f,0.0f,0.0f,+0.0f,+1.0f,
		+1.0f,-1.0f,+1.0f,1.0f,1.0f,0.0f,1.0f,0.0f,0.0f,+1.0f,+0.0f,
		+1.0f,-1.0f,+1.0f,1.0f,1.0f,0.0f,1.0f,0.0f,0.0f,+1.0f,+0.0f,
		+1.0f,+1.0f,-1.0f,1.0f,1.0f,0.0f,1.0f,0.0f,0.0f,+0.0f,+1.0f,
		+1.0f,+1.0f,+1.0f,1.0f,1.0f,0.0f,1.0f,0.0f,0.0f,+1.0f,+1.0f,
 	};

 	D3D11_BUFFER_DESC bufferDesc;
 	ZeroMemory(&bufferDesc,sizeof(D3D11_BUFFER_DESC));
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc.ByteWidth = sizeof(float) * ARRAYSIZE(CUBE_VCNT);
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	
	hr = gpID3D11Device->CreateBuffer(&bufferDesc,
		nullptr,
		&gpID3D11Buffer_VertexBuffer_Cube_VCNT);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() failed For Color Buffer Vertex Shader.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() done For Color Buffer Vertex Shader.\n");
		fclose(gpFile);
	}
	D3D11_MAPPED_SUBRESOURCE mappedSubResource;

	ZeroMemory(&mappedSubResource, sizeof(D3D11_MAPPED_SUBRESOURCE));
	gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Cube_VCNT, 0,
		D3D11_MAP_WRITE_DISCARD, 0, &mappedSubResource);
	memcpy(mappedSubResource.pData, CUBE_VCNT, sizeof(CUBE_VCNT));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Cube_VCNT, 0);



	D3D11_BUFFER_DESC bufferDesc_ConstantBuffer;
	ZeroMemory(&bufferDesc_ConstantBuffer,sizeof(D3D11_BUFFER_DESC));
	bufferDesc_ConstantBuffer.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc_ConstantBuffer.ByteWidth = sizeof(CBUFFER);
	bufferDesc_ConstantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

	hr = gpID3D11Device->CreateBuffer(&bufferDesc_ConstantBuffer,
		nullptr,
		&gpID3D11Buffer_ConstantBuffer);

	if(FAILED(hr))
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() failed For Constant Buffer.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Succeeded For Constant Buffer.\n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->VSSetConstantBuffers(0,	
		1,	
		&gpID3D11Buffer_ConstantBuffer);

	gpID3D11DeviceContext->PSSetConstantBuffers(0,	
		1,	
		&gpID3D11Buffer_ConstantBuffer);


	gClearColor[0] = 0.0f;
	gClearColor[1] = 0.0f;
	gClearColor[2] = 0.0f;
	gClearColor[3] = 0.0f;

	gPerspectiveProjectionMatrix = XMMatrixIdentity();

	D3D11_RASTERIZER_DESC rasterizerDesc;
	ZeroMemory(&rasterizerDesc, sizeof(D3D11_RASTERIZER_DESC));

	rasterizerDesc.AntialiasedLineEnable = FALSE;
	rasterizerDesc.CullMode = D3D11_CULL_NONE;
	rasterizerDesc.DepthBias = 0;
	rasterizerDesc.MultisampleEnable = FALSE;
	rasterizerDesc.DepthBiasClamp = 0.0f;
	rasterizerDesc.DepthClipEnable = TRUE;
	rasterizerDesc.SlopeScaledDepthBias = 0.0f;
	rasterizerDesc.FillMode = D3D11_FILL_SOLID;
	rasterizerDesc.FrontCounterClockwise = FALSE;
	rasterizerDesc.ScissorEnable = FALSE;

	gpID3D11Device->CreateRasterizerState(&rasterizerDesc, &gpID3D11RasterizerState);

	gpID3D11DeviceContext->RSSetState(gpID3D11RasterizerState);


	//CUBE TEXTURE

	hr = LoadD3DTexture(L"marble.bmp", &gpID3D11ShaderResourceView_Texture_Cube);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "LoadD3DTexture() Failed For Cube Texture.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "LoadD3DTexture() Succeeded For Cube Texture.\n");
		fclose(gpFile);
	}

	D3D11_SAMPLER_DESC samplerDesc;

	ZeroMemory(&samplerDesc, sizeof(D3D11_SAMPLER_DESC));
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;

	hr = gpID3D11Device->CreateSamplerState(&samplerDesc, &gpID3D11SamplerState_Texture_Cube);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateSamplerState() Failed For Cube Texture.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateSamplerState() Succeeded For Cube Texture.\n");
		fclose(gpFile);
	}


	hr =resize(WIN_WIDTH, WIN_HEIGHT);
	if(FAILED(hr))
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "resize() Failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "resize() Succeeded.\n");
		fclose(gpFile);
	}

	return(S_OK);
}

HRESULT LoadD3DTexture(const wchar_t *textureFileName, ID3D11ShaderResourceView **ppID3D11ShaderResourceView)
{
	HRESULT hr;

	hr = DirectX::CreateWICTextureFromFile(gpID3D11Device, gpID3D11DeviceContext, textureFileName, NULL, ppID3D11ShaderResourceView);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "CreateWICTextureFromFile() Failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "CreateWICTextureFromFile() Succeeded.\n");
		fclose(gpFile);
	}
	return(hr);

}

HRESULT resize(int width,int height)
{
	HRESULT hr = S_OK;

	if (gpID3D11DepthStencilView)
	{
		gpID3D11DepthStencilView->Release();
		gpID3D11DepthStencilView = NULL;
	}

	if(gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	gpIDXGISwapChain->ResizeBuffers(1,width,height,DXGI_FORMAT_R8G8B8A8_UNORM,0);

	ID3D11Texture2D *pID3D11Texture2D_BackBuffer;
	gpIDXGISwapChain->GetBuffer(0,__uuidof(ID3D11Texture2D),(LPVOID *)&pID3D11Texture2D_BackBuffer);

	hr = gpID3D11Device->CreateRenderTargetView(pID3D11Texture2D_BackBuffer,NULL,&gpID3D11RenderTargetView);
	if(FAILED(hr))
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "ID3D11Device::CreateRenderTargetView() Failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "ID3D11Device::CreateRenderTargetView() Succeeded.\n");
		fclose(gpFile);
	}

	pID3D11Texture2D_BackBuffer->Release();
	pID3D11Texture2D_BackBuffer= NULL;

	D3D11_TEXTURE2D_DESC textureDesc;
	ZeroMemory(&textureDesc, sizeof(D3D11_TEXTURE2D_DESC));

	textureDesc.ArraySize = 1;
	textureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	textureDesc.CPUAccessFlags = 0;
	textureDesc.Format = DXGI_FORMAT_D32_FLOAT;
	textureDesc.Height = height;
	textureDesc.Width = width;
	textureDesc.MipLevels = 1;
	textureDesc.MiscFlags = 0;
	textureDesc.SampleDesc.Count = 1;
	textureDesc.SampleDesc.Quality = 0;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;

	ID3D11Texture2D *pID3D11Texture2D_DepthBuffer = NULL;

	hr = gpID3D11Device->CreateTexture2D(&textureDesc, NULL, &pID3D11Texture2D_DepthBuffer);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateTexture2D() Failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateTexture2D() Succeeded.\n");
		fclose(gpFile);
	}

	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	ZeroMemory(&depthStencilViewDesc, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));

	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;
	depthStencilViewDesc.Format = DXGI_FORMAT_D32_FLOAT;

	hr = gpID3D11Device->CreateDepthStencilView(pID3D11Texture2D_DepthBuffer, &depthStencilViewDesc, &gpID3D11DepthStencilView);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateDepthStencilView() Failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateDepthStencilView() Succeeded.\n");
		fclose(gpFile);
	}

	pID3D11Texture2D_DepthBuffer->Release();
	pID3D11Texture2D_DepthBuffer = NULL;

	gpID3D11DeviceContext->OMSetRenderTargets(1,&gpID3D11RenderTargetView,gpID3D11DepthStencilView);


	D3D11_VIEWPORT d3dViewPort;
	d3dViewPort.TopLeftX = 0;
	d3dViewPort.TopLeftY = 0;
	d3dViewPort.Width = (float)width;
	d3dViewPort.Height = (float)height;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;

	gpID3D11DeviceContext->RSSetViewports(1,&d3dViewPort);


	if (width >= height)
		gPerspectiveProjectionMatrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0f), (float)width / (float)height, 0.1f, 100.0f);
	else
		gPerspectiveProjectionMatrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0f), (float)height / (float)width, 0.1f, 100.0f);
	return(hr);
}

void update(void)
{
	angle_cube = angle_cube + 0.1f;
	if (angle_cube > 360.0f)
		angle_cube = 0.0f;
}

void display(void)
{
	void update(void);
	gpID3D11DeviceContext->ClearRenderTargetView(gpID3D11RenderTargetView,gClearColor);
	gpID3D11DeviceContext->ClearDepthStencilView(gpID3D11DepthStencilView,D3D11_CLEAR_DEPTH,1.0f,0);

	UINT stride = sizeof(float) * 11;
	UINT offset = 0;
	// draw cube
	
	gpID3D11DeviceContext->IASetVertexBuffers(0,//input slot
		1,  
		&gpID3D11Buffer_VertexBuffer_Cube_VCNT,
		&stride,
		&offset);

	stride = sizeof(float) * 11;
	offset = sizeof(float)* 3;
	gpID3D11DeviceContext->IASetVertexBuffers(1,//input slot
		1,  //how many buffers you have
		&gpID3D11Buffer_VertexBuffer_Cube_VCNT,
		&stride,
		&offset);
	offset = sizeof(float)* 6;
	stride = sizeof(float) * 11;

	gpID3D11DeviceContext->IASetVertexBuffers(2,
		1, 
		&gpID3D11Buffer_VertexBuffer_Cube_VCNT,
		&stride,
		&offset);
	stride = sizeof(float) * 11;
	offset = sizeof(float) * 9;
	gpID3D11DeviceContext->IASetVertexBuffers(3,
		1, 
		&gpID3D11Buffer_VertexBuffer_Cube_VCNT,
		&stride,
		&offset);

	

	gpID3D11DeviceContext->PSSetShaderResources(0, 1, &gpID3D11ShaderResourceView_Texture_Cube);
	gpID3D11DeviceContext->PSSetSamplers(0, 1, &gpID3D11SamplerState_Texture_Cube);

	//select geometry primitive
	gpID3D11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	XMMATRIX worldMatrix = XMMatrixIdentity();
	XMMATRIX viewMatrix = XMMatrixIdentity();
	XMMATRIX scaleMatrix = XMMatrixIdentity();
	XMMATRIX rotationMatrix  = XMMatrixIdentity();
	
	rotationMatrix = XMMatrixRotationX(XMConvertToRadians(angle_cube)) * XMMatrixRotationY(XMConvertToRadians(angle_cube)) * XMMatrixRotationZ(XMConvertToRadians(angle_cube));
	scaleMatrix  = XMMatrixScaling(0.75f,0.75f,0.75f);
	worldMatrix = scaleMatrix * rotationMatrix * XMMatrixTranslation(0.0f, 0.0f, 4.0f);
	//wvpMatrix = worldMatrix * viewMatrix * gPerspectiveProjectionMatrix;
	CBUFFER constantBuffer;

	ZeroMemory(&constantBuffer, sizeof(CBUFFER));
	constantBuffer.ModelMatrix = worldMatrix;
	constantBuffer.ViewMatrix = viewMatrix;
	constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix;
	constantBuffer.u_La = XMVectorSet(lightAmbient[0], lightAmbient[1], lightAmbient[2], lightAmbient[3]);
	constantBuffer.u_Ld = XMVectorSet(lightDiffuse[0], lightDiffuse[1], lightDiffuse[2], lightDiffuse[3]);
	constantBuffer.u_Ls = XMVectorSet(lightSpecular[0], lightSpecular[1], lightSpecular[2], lightSpecular[3]);

	constantBuffer.u_Ka = XMVectorSet(material_Ambient[0], material_Ambient[1], material_Ambient[2], material_Ambient[3]);
	constantBuffer.u_Kd = XMVectorSet(material_Diffuse[0], material_Diffuse[1], material_Diffuse[2], material_Diffuse[3]);
	constantBuffer.u_Ks = XMVectorSet(material_Specular[0], material_Specular[1], material_Specular[2], material_Specular[3]);

	constantBuffer.u_Material_shininess = material_shininess;

	constantBuffer.u_light_position = XMVectorSet(lightPosition[0], lightPosition[1], lightPosition[2], lightPosition[3]);
	if (gbLight == true)
	{
		constantBuffer.u_LKeyPressed = 1;
	}
	else
	{
		constantBuffer.u_LKeyPressed = 0;
	}

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer,
		0,	//index in shader
		NULL, //D3D_BOX bounding box compute shader cube of data
		&constantBuffer,
		0, //width of face of bounding box
		0 );	//depth of bounding box	

	gpID3D11DeviceContext->Draw(6,0);
	gpID3D11DeviceContext->Draw(6, 6);
	gpID3D11DeviceContext->Draw(6, 12);
	gpID3D11DeviceContext->Draw(12, 18);
	gpID3D11DeviceContext->Draw(18, 24);
	gpID3D11DeviceContext->Draw(24, 30);



	gpIDXGISwapChain->Present(0,0);
	update();
}

void uninitialize(void)
{

		if (gpID3D11SamplerState_Texture_Cube)
	{
		gpID3D11SamplerState_Texture_Cube->Release();
		gpID3D11SamplerState_Texture_Cube = NULL;
	}

	if (gpID3D11ShaderResourceView_Texture_Cube)
	{
		gpID3D11ShaderResourceView_Texture_Cube->Release();
		gpID3D11ShaderResourceView_Texture_Cube = NULL;
	}


	if (gpID3D11Buffer_ConstantBuffer)
	{
		gpID3D11Buffer_ConstantBuffer->Release();
		gpID3D11Buffer_ConstantBuffer = NULL;
	}

	if (gpID3D11InputLayout)
	{
		gpID3D11InputLayout->Release();
		gpID3D11InputLayout = NULL;
	}

	
	if (gpID3D11Buffer_VertexBuffer_Cube_VCNT)
	{
		gpID3D11Buffer_VertexBuffer_Cube_VCNT->Release();
		gpID3D11Buffer_VertexBuffer_Cube_VCNT = NULL;
	}

	if (gpID3D11RasterizerState)
	{
		gpID3D11RasterizerState->Release();
		gpID3D11RasterizerState = NULL;
	}

	if (gpID3D11PixelShader)
	{
		gpID3D11PixelShader->Release();
		gpID3D11PixelShader = NULL;
	}

	if (gpID3D11VertexShader)
	{
		gpID3D11VertexShader->Release();
		gpID3D11VertexShader = NULL;
	}

	if (gpID3D11DepthStencilView)
	{
		gpID3D11DepthStencilView->Release();
		gpID3D11DepthStencilView = NULL;
	}

	
	if(gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;	
	}

	if(gpIDXGISwapChain)
	{
		gpIDXGISwapChain->Release();
		gpIDXGISwapChain = NULL;	
	}

	if(gpID3D11DeviceContext)
	{
		gpID3D11DeviceContext->Release();
		gpID3D11DeviceContext = NULL;	
	}

	if(gpID3D11Device)
	{
		gpID3D11Device->Release();
		gpID3D11Device = NULL;	
	}

	if(gpFile)
	{
		fopen_s(&gpFile,gszLogFileName,"a+");
		fprintf_s(gpFile, "uninitialize Succeeded\n");
		fprintf_s(gpFile, "Log File Closed Successfully\n");
		fclose(gpFile);	
	}
}
