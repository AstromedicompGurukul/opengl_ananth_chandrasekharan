#pragma once
#include<Windows.h>
#include<gl/GL.h>
#include <gl/GLU.h>

GLfloat light_ambient[] = {0.5f,0.5f,0.5f,1.0f};
GLfloat light_diffused[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat light_specular[] = {1.0f,1.0f,1.0f,1.0f };
GLfloat light_position[] = { 0.0f,0.0f,2.0f,1.0f };

BOOL lightkey = FALSE;