#include<Windows.h>
#include<stdlib.h>
#include<gl/GL.h>
#include<gl/GLU.h>

#include "ArrayHeader.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib,"opengl32.lib")
#pragma comment (lib,"glu32.lib")

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullScreen = false;

GLfloat angleSpin = 0.0f;

LRESULT CALLBACK AcCallBack(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam);

void update(void)
{
	angleSpin = angleSpin + 1.0f;
	if (angleSpin >= 360.0f)
	{
		angleSpin = 0.0f;
	}
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);
	void resize(int, int);

	WNDCLASSEX ac;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("Rotating Pyramid");
	bool bDone = false;

	ac.cbSize = sizeof(WNDCLASSEX);
	ac.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	ac.cbClsExtra = 0;
	ac.cbWndExtra = 0;
	ac.hInstance = hInstance;
	ac.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	ac.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	ac.hCursor = LoadCursor(NULL, IDC_ARROW);
	ac.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	ac.lpfnWndProc = AcCallBack;
	ac.lpszClassName = szClassName;
	ac.lpszMenuName = NULL;

	RegisterClassEx(&ac);

	hwnd = CreateWindowEx(
		WS_EX_APPWINDOW,
		szClassName,
		TEXT("Shree Ganesha"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);

	ghwnd = hwnd;

	initialize();

	ShowWindow(hwnd, SW_SHOWNORMAL);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				display();
				update();
				
				if (gbEscapeKeyIsPressed == true)
				{
					bDone = true;
				}
			}
		}
	}
	uninitialize();
	return ((int)msg.wParam);
}

void initialize(void)
{
	void resize(GLint width, GLint height);

	PIXELFORMATDESCRIPTOR pfd;
	GLint iPixelFormatIndex;

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cBlueBits = 8;
	pfd.cGreenBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	//Defined for GL_LIGHT0
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffused);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);

	//Defined Background color
	glClearColor(0.25f, 0.25f, 0.25f, 0.0f);

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glEnable(GL_LIGHT0);
	glEnable(GL_AUTO_NORMAL);
	glEnable(GL_NORMALIZE);
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT,light_model_ambient);
	glLightModelf(GL_LIGHT_MODEL_LOCAL_VIEWER,light_model_local_viewer);

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void uninitialize(void)
{
	int i, j;

	if (gbFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	if (alldat)
	{
		free(alldat);
		alldat = NULL;
	}

	/*
	if (quadric)
	{
		gluDeleteQuadric(quadric);
		quadric = NULL;
	}

	if (quadric1)
	{
		gluDeleteQuadric(quadric1);
		quadric1 = NULL;
	}

	if (quadric2)
	{
		gluDeleteQuadric(quadric2);
		quadric2 = NULL;
	}

	if (quadric3)
	{
		gluDeleteQuadric(quadric3);
		quadric3 = NULL;
	}

	if (quadric4)
	{
		gluDeleteQuadric(quadric4);
		quadric4 = NULL;
	}

	if (quadric5)
	{
		gluDeleteQuadric(quadric5);
		quadric5 = NULL;
	}

	if (quadric6)
	{
		gluDeleteQuadric(quadric6);
		quadric6 = NULL;
	}

	if (quadric7)
	{
		gluDeleteQuadric(quadric7);
		quadric7 = NULL;
	}

	if (quadric8)
	{
		gluDeleteQuadric(quadric8);
		quadric8 = NULL;
	}

	if (quadric9)
	{
		gluDeleteQuadric(quadric9);
		quadric9 = NULL;
	}

	if (quadric10)
	{
		gluDeleteQuadric(quadric10);
		quadric10 = NULL;
	}

	if (quadric11)
	{
		gluDeleteQuadric(quadric11);
		quadric11 = NULL;
	}

	if (quadric12)
	{
		gluDeleteQuadric(quadric12);
		quadric12 = NULL;
	}

	if (quadric13)
	{
		gluDeleteQuadric(quadric13);
		quadric13 = NULL;
	}

	if (quadric14)
	{
		gluDeleteQuadric(quadric14);
		quadric14 = NULL;
	}

	if (quadric15)
	{
		gluDeleteQuadric(quadric15);
		quadric15 = NULL;
	}

	if (quadric16)
	{
		gluDeleteQuadric(quadric16);
		quadric16 = NULL;
	}

	if (quadric17)
	{
		gluDeleteQuadric(quadric17);
		quadric17 = NULL;
	}

	if (quadric18)
	{
		gluDeleteQuadric(quadric18);
		quadric18 = NULL;
	}

	if (quadric19)
	{
		gluDeleteQuadric(quadric19);
		quadric19 = NULL;
	}

	if (quadric20)
	{
		gluDeleteQuadric(quadric20);
		quadric20 = NULL;
	}

	if (quadric21)
	{
		gluDeleteQuadric(quadric21);
		quadric21 = NULL;
	}

	if (quadric22)
	{
		gluDeleteQuadric(quadric22);
		quadric22 = NULL;
	}

	if (quadric23)
	{
		gluDeleteQuadric(quadric23);
		quadric23 = NULL;
	}
	*/

	for (i=0;i<=3;i++)
	{
		for (j=0;j<=5;j++)
		{
			gluDeleteQuadric(quadric[i][j]);
			quadric[i][j] = NULL;
		}
	}

	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(ghrc);
	ghrc = NULL;
	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;
	DestroyWindow(ghwnd);
	ghwnd = NULL;
}

void resize(GLint width, GLint height)
{
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void ToggleFullScreen(void)
{
	MONITORINFO mi;

	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle&WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle&~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
}

void DrawSphere(AllData *alldat)
{
	glMaterialfv(GL_FRONT, GL_AMBIENT, alldat->AmbientMatArr);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, alldat->DiffusedMatArr);
	glMaterialfv(GL_FRONT, GL_SPECULAR,alldat->SpecularMatArr);
	glMaterialf(GL_FRONT, GL_SHININESS, alldat->ShinynessMat);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(alldat->translateArr[0], alldat->translateArr[1], alldat->translateArr[2]);
	glScalef(alldat->ScalingArr[0], alldat->ScalingArr[1], alldat->ScalingArr[2]);

	if (xkey == TRUE && ykey == FALSE && zkey == FALSE)
	{
		glRotatef(alldat->angleSpin, 1.0f, 0.0f, 0.0f);
		light_position[0] = 0.0f;
		light_position[1] = alldat->angleSpin;
		light_position[2] = 0.0f;
		glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	}
	else if (ykey == TRUE && xkey == FALSE && zkey == FALSE)
	{
		glRotatef(alldat->angleSpin, 0.0f, 1.0f, 0.0f);
		light_position[0] = alldat->angleSpin;
		light_position[1] = 0.0f;
		light_position[2] = 0.0f;
		glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	}
	else if (zkey == TRUE && xkey == FALSE && ykey == FALSE)
	{
		glRotatef(alldat->angleSpin, 0.0f, 0.0f, 1.0f);
		light_position[0] = alldat->angleSpin;
		light_position[1] = 0.0f;
		light_position[2] = 0.0f;
		glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	}
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	alldat->quadric = gluNewQuadric();
	gluSphere(alldat->quadric, 0.75f, 30, 30);
}

void FillOneArraytoAnother(GLfloat *dest,GLfloat *source,int count)
{
	if (count==4)
	{
		for (int i = 0; i <= 3; i++)
		{
			*(dest + i) = *(source + i);
		}
	}
	else if(count==3)
	{
		for (int i = 0; i <= 2; i++)
		{
			*(dest + i) = *(source + i);
		}
	}
	else if (count == 1)
	{
		*dest = *source;
	}
}

void FillValueInArr(GLfloat *dest,GLfloat x,GLfloat y,GLfloat z)
{
	dest[0] = x;
	dest[1] = y;
	dest[2] = z;
}

void display(void)
{
	void DrawSphere(AllData *alldat);
	void FillOneArraytoAnother(GLfloat *dest, GLfloat *source, int count);
	void FillValueInArr(GLfloat *dest, GLfloat x, GLfloat y, GLfloat z);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//----------------------------------------

	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient10, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused11, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular12, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness13, 1);
	alldat->quadric = quadric[0][0];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.8f, 0.9f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);
		
	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient20, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused21, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular22, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness23, 1);
	alldat->quadric = quadric[0][1];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.2f, 0.9f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient30, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused31, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular32, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness33, 1);
	alldat->quadric = quadric[0][2];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 0.4f, 0.9f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient40, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused41, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular42, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness43, 1);
	alldat->quadric = quadric[0][3];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 1.0f, 0.9f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	//----------------------------------------

	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient50, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused51, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular52, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness53, 1);
	alldat->quadric = quadric [1][0];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.8f, 0.575f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);


	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient60, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused61, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular62, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness63, 1);
	alldat->quadric = quadric[1][1];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.2f, 0.575f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient70, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused71, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular72, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness73, 1);
	alldat->quadric = quadric[1][2];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 0.4f, 0.575f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	//----------------------------------------		
	
	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient80, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused81, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular82, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness83, 1);
	alldat->quadric = quadric [1][3];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 1.0f, 0.575f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient90, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused91, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular92, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness93, 1);
	alldat->quadric = quadric[2][0];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.8f, 0.25f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f); 
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient100, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused101, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular102, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness103, 1);
	alldat->quadric = quadric[2][1];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.2f, 0.25f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f); 
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient200, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused201, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular202, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness203, 1);
	alldat->quadric = quadric[2][2];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 0.4f, 0.25f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f); 
	DrawSphere(alldat);
	
	//----------------------------------------------

	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient300, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused301, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular302, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness303, 1);
	alldat->quadric = quadric[2][3];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 1.0f, 0.25f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f); 
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient400, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused401, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular402, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness403, 1);
	alldat->quadric = quadric[3][0];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.8f, -0.075f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f); 
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient500, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused501, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular502, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness503, 1);
	alldat->quadric = quadric[3][1];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.2f, -0.075f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient600, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused601, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular602, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness603, 1);
	alldat->quadric = quadric[3][2];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 0.4f, -0.075f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f); 
	DrawSphere(alldat);

	//----------------------------------------------

	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient700, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused701, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular702, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness703, 1);
	alldat->quadric = quadric[3][3];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 1.0f, -0.075f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f); 
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient800, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused801, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular802, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness803, 1);
	alldat->quadric = quadric[4][0];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.8f, -0.4f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f); 
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient900, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused901, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular902, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness903, 1);
	alldat->quadric = quadric[4][1];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.2f, -0.4f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient1000, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused1001, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular1002, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness1003, 1);
	alldat->quadric = quadric[4][2];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 0.4f, -0.4f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f); 
	DrawSphere(alldat);

	//----------------------------------------------
	
	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient2000, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused2001, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular2002, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness2003, 1);
	alldat->quadric = quadric[4][3];;
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 1.0f, -0.4f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient3000, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused3001, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular3002, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness3003, 1);
	alldat->quadric = quadric[5][0];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.8f, -0.725f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f); 
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient4000, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused4001, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular4002, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness4003, 1);
	alldat->quadric = quadric[5][1];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.2f, -0.725f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient5000, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused5001, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular5002, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness5003, 1);
	alldat->quadric = quadric[5][2];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 0.4f, -0.725f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f); 
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, material_ambient6000, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, material_diffused6001, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, material_specular6002, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &material_shinyness6003, 1);
	alldat->quadric = quadric[5][3];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 1.0f, -0.725f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	SwapBuffers(ghdc);
}

LRESULT CALLBACK AcCallBack(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void uninitialize(void);
	void resize(GLint width, GLint height);
	void ToggleFullScreen(void);

	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
		{
			gbActiveWindow = true;
		}
		else
		{
			gbActiveWindow = false;
		}
		break;

	case WM_ERASEBKGND:
		return  0;

	case WM_LBUTTONDOWN:
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_CLOSE:
		uninitialize();
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			if (gbEscapeKeyIsPressed == false)
			{
				gbEscapeKeyIsPressed = true;
			}
			break;

		case 0x46:
			if (gbFullScreen == false)
			{
				ToggleFullScreen();
				gbFullScreen = true;
			}
			else
			{
				ToggleFullScreen();
				gbFullScreen = false;
			}
			break;

		case 0x4C:
			//L key
			switch (lightkey)
			{
			case TRUE:
				lightkey = FALSE;
				glDisable(GL_LIGHTING);
				break;

			case FALSE:
				lightkey = TRUE;
				glEnable(GL_LIGHTING);
				break;
			}
			break;

		case 0x57:  //w button  (Stop at position and Resume at original position)
			xkey = FALSE;
			ykey = FALSE;
			zkey = FALSE;
			light_position[0] = 0.0f;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			break;

		case 0x58: //x button
			xkey = TRUE;
			ykey = FALSE;
			zkey = FALSE;
			angleSpin = 0.0f;
			break;

		case 0x59: //y button
			xkey = FALSE;
			ykey = TRUE;
			zkey = FALSE;
			angleSpin = 0.0f;
			break;	

		case 0x5A: //z button
			xkey = FALSE;
			ykey = FALSE;
			zkey = TRUE;
			angleSpin = 0.0f;
			break;

		default:
			break;
		}
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}
	return DefWindowProc(hwnd, iMsg, wParam, lParam);
}