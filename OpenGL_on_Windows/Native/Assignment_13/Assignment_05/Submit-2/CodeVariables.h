#pragma once
#include<windows.h>
#include "Header.h"

#define BUFFER_SIZE 256
#define S_EQUAL 0

#define NR_POINT_COORDS 3
#define NR_TEXTURE_COORDS 3
#define MIN_NR_FACE_TOKENS 3
#define MAX_NR_FACE_TOKENS 4

#define X_TRANSLATE 0.0f
#define Y_TRANSLATE -25.0f
#define Z_TRANSLATE -125.0f

#define X_SCALEFACTOR 0.75f
#define Y_SCLAEFACTOR 0.75f
#define Z_SCALEFACTOR 0.75f

#define START_ANGLE_POS 0.0f
#define END_ANGLE_POS 360.0f
#define ANGLE_INCREMENT  0.5f

std::vector<std::vector<float>> g_vertices;
std::vector<std::vector<float>> g_texture;
std::vector<std::vector<float>> g_normals;
std::vector<std::vector<int>> g_face_tri;
std::vector<std::vector<int>> g_face_texture;
std::vector<std::vector<int>> g_face_normals;

GLfloat g_rotate;

FILE *g_fp_meshfile = NULL;

FILE *g_fp_logfile = NULL;

char line[BUFFER_SIZE];