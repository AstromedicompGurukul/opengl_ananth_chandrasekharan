#pragma once
#include<Windows.h>
#include<gl/GL.h>
#include <gl/GLU.h>
#include<vector>

BOOL lightkey = FALSE;

BOOL stopRotation = FALSE;

GLUquadric *quadric[4][6];

typedef struct
{
	GLfloat material_ambient[4];
	GLfloat material_diffused[4];
	GLfloat material_specular[4];
	GLfloat material_shinyness;
}MaterialArray;

std::vector<MaterialArray> g_materials(25);

BOOL xkey = FALSE;
BOOL ykey = FALSE;
BOOL zkey = FALSE;

//For Light 0 
GLfloat light_ambient[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat light_diffused[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat light_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat light_position[] = { 0.0f,0.0f,0.0f,0.0f };

GLfloat light_model_ambient[] = {0.2f,0.2f ,0.2f ,0.0f};
GLfloat light_model_local_viewer = 0.0f;

//----------------------------------------------------------------

typedef struct
{
	GLfloat translateArr[3];
	GLfloat ScalingArr[3];
	GLfloat AmbientMatArr[4];
	GLfloat DiffusedMatArr[4];
	GLfloat SpecularMatArr[4];
	GLfloat ShinynessMat;
	GLUquadric *quadric;
	GLfloat angleSpin;
}AllData;

AllData *alldat = (AllData*)malloc(sizeof(AllData));



