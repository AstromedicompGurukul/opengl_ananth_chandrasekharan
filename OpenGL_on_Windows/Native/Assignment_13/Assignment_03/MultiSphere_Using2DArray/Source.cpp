#include<Windows.h>
#include<stdlib.h>
#include<gl/GL.h>
#include<gl/GLU.h>
#include<vector>

#include "ArrayHeader.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib,"opengl32.lib")
#pragma comment (lib,"glu32.lib")

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullScreen = false;

GLfloat angleSpin = 0.0f;

LRESULT CALLBACK AcCallBack(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam);


void update(void)
{
	angleSpin = angleSpin + 1.0f;
	if (angleSpin >= 360.0f)
	{
		angleSpin = 0.0f;
	}
}

void LoadMaterials()
{
	void FillValueInArr_V1(GLfloat dest[], GLfloat x, GLfloat y, GLfloat z, GLfloat w);
	
	int index = 1;
	TCHAR str[255];


	wsprintf(str,TEXT("%d"),index);
	MessageBox(NULL,str,TEXT("index"),MB_OK);

	FillValueInArr_V1(g_materials[index].material_ambient, 0.0215f, 0.1745f, 0.0215f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_diffused, 0.07568f, 0.61424f, 0.07568f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_specular, 0.633f, 0.0727811f, 0.633f, 1.0f);
	g_materials[index].material_shinyness = (0.6f * 128);

	index++;
	FillValueInArr_V1(g_materials[index].material_ambient, 0.135f, 0.2225f, 0.1575f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_diffused, 0.54f, 0.89f, 0.63f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_specular, 0.316228f, 0.316228f, 0.316228f, 1.0f);
	g_materials[index].material_shinyness = (0.1f * 128);

	wsprintf(str, TEXT("%d"), index);
	MessageBox(NULL, str, TEXT("index"), MB_OK);

	index++;
	FillValueInArr_V1(g_materials[index].material_ambient, 0.05375f, 0.05f, 0.06625f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_diffused, 0.18275f, 0.17f, 0.22525f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_specular, 0.332741f, 0.328634f, 0.346435f, 1.0f);
	g_materials[index].material_shinyness = (0.3f * 128);

	wsprintf(str, TEXT("%d"), index);
	MessageBox(NULL, str, TEXT("index"), MB_OK);

	index++;
	FillValueInArr_V1(g_materials[index].material_ambient, 0.25f, 0.20725f, 0.20725f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_diffused, 1.0f, 0.829f, 0.829f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_specular, 0.296648f, 0.296648f, 0.296648f, 1.0f);
	g_materials[index].material_shinyness = (0.088f * 128);
	
	wsprintf(str, TEXT("%d"), index);
	MessageBox(NULL, str, TEXT("index"), MB_OK);

	index++;
	FillValueInArr_V1(g_materials[index].material_ambient, 0.1745f, 0.01175f, 0.01175f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_diffused, 0.61424f, 0.04136f, 0.04136f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_specular, 0.727811f, 0.626959f, 0.626959f, 1.0f);
	g_materials[index].material_shinyness = (0.6f * 128);

	wsprintf(str, TEXT("%d"), index);
	MessageBox(NULL, str, TEXT("index"), MB_OK);

	index++;
	FillValueInArr_V1(g_materials[index].material_ambient, 0.1f, 0.18725f, 0.1745f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_diffused, 0.396f, 0.74151f, 0.69102f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_specular, 0.297254f, 0.30829f, 0.306678f, 1.0f);
	g_materials[index].material_shinyness = (0.1f * 128);
	
	wsprintf(str, TEXT("%d"), index);
	MessageBox(NULL, str, TEXT("index"), MB_OK);

	index++;
	FillValueInArr_V1(g_materials[index].material_ambient, 0.329412f, 0.223529f, 0.027451f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_diffused, 0.780392f, 0.568627f, 0.113725f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_specular, 0.992157f, 0.941176f, 0.807843f, 1.0f);
	g_materials[index].material_shinyness = (0.21794872f * 128);
	
	wsprintf(str, TEXT("%d"), index);
	MessageBox(NULL, str, TEXT("index"), MB_OK);

	index++;
	FillValueInArr_V1(g_materials[index].material_ambient, 0.2125f, 0.1275f, 0.054f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_diffused, 0.714f, 0.4284f, 0.18144f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_specular, 0.393548f, 0.271906f, 0.166721f, 1.0f);
	g_materials[index].material_shinyness = (0.2f * 128);

	wsprintf(str, TEXT("%d"), index);
	MessageBox(NULL, str, TEXT("index"), MB_OK);

	index++;
	FillValueInArr_V1(g_materials[index].material_ambient, 0.25f, 0.25f, 0.25f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_diffused, 0.4f, 0.4f, 0.4f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_specular, 0.774597f, 0.774597f, 0.774597f, 1.0f);
	g_materials[index].material_shinyness = (0.6f * 128);

	wsprintf(str, TEXT("%d"), index);
	MessageBox(NULL, str, TEXT("index"), MB_OK);

	index++;
	FillValueInArr_V1(g_materials[index].material_ambient, 0.19125f, 0.0735f, 0.0225f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_diffused, 0.7038f, 0.27048f, 0.0828f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_specular, 0.256777f, 0.137622f, 0.086014f, 1.0f);
	g_materials[index].material_shinyness = (0.1f * 128);

	wsprintf(str, TEXT("%d"), index);
	MessageBox(NULL, str, TEXT("index"), MB_OK);

	index++;
	FillValueInArr_V1(g_materials[index].material_ambient, 0.19225f, 0.19225f, 0.19225f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_diffused, 0.50754f, 0.50754f, 0.50754f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_specular, 0.508273f, 0.508273f, 0.508273f, 1.0f);
	g_materials[index].material_shinyness = (0.4f * 128);

	wsprintf(str, TEXT("%d"), index);
	MessageBox(NULL, str, TEXT("index"), MB_OK);

	index++;
	FillValueInArr_V1(g_materials[index].material_ambient, 0.0f, 0.0f, 0.0f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_diffused, 0.01f, 0.01f, 0.01f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_specular, 0.50f, 0.50f, 0.50f, 1.0f);
	g_materials[index].material_shinyness = (0.25f * 128);

	wsprintf(str, TEXT("%d"), index);
	MessageBox(NULL, str, TEXT("index"), MB_OK);

	index++;
	FillValueInArr_V1(g_materials[index].material_ambient, 0.0f, 0.1f, 0.06f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_diffused, 0.0f, 0.50980392f, 0.50980392f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_specular, 0.50196078f, 0.50196078f, 0.50196078f, 1.0f);
	g_materials[index].material_shinyness = (0.25f * 128);

	wsprintf(str, TEXT("%d"), index);
	MessageBox(NULL, str, TEXT("index"), MB_OK);

	index++;
	FillValueInArr_V1(g_materials[index].material_ambient, 0.0f, 0.0f, 0.0f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_diffused, 0.1f, 0.35f, 0.1f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_specular, 0.45f, 0.55f, 0.45f, 1.0f);
	g_materials[index].material_shinyness = (0.25f * 128);

	wsprintf(str, TEXT("%d"), index);
	MessageBox(NULL, str, TEXT("index"), MB_OK);

	index++;
	FillValueInArr_V1(g_materials[index].material_ambient, 0.0f, 0.0f, 0.0f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_diffused, 0.5f, 0.0f, 0.0f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_specular, 0.7f, 0.6f, 0.6f, 1.0f);
	g_materials[index].material_shinyness = (0.25f * 128);

	wsprintf(str, TEXT("%d"), index);
	MessageBox(NULL, str, TEXT("index"), MB_OK);

	index++;
	FillValueInArr_V1(g_materials[index].material_ambient, 0.0f, 0.0f, 0.0f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_diffused, 0.55f, 0.55f, 0.55f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_specular, 0.70f, 0.70f, 0.70f, 1.0f);
	g_materials[index].material_shinyness = (0.25f * 128);

	wsprintf(str, TEXT("%d"), index);
	MessageBox(NULL, str, TEXT("index"), MB_OK);

	index++;
	FillValueInArr_V1(g_materials[index].material_ambient, 0.0f, 0.0f, 0.0f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_diffused, 0.5f, 0.5f, 0.0f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_specular, 0.60f, 0.60f, 0.50f, 1.0f);
	g_materials[index].material_shinyness = (0.25f * 128);

	wsprintf(str, TEXT("%d"), index);
	MessageBox(NULL, str, TEXT("index"), MB_OK);

	index++;
	FillValueInArr_V1(g_materials[index].material_ambient, 0.02f, 0.02f, 0.02f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_diffused, 0.01f, 0.01f, 0.01f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_diffused, 0.01f, 0.01f, 0.01f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_specular, 0.40f, 0.40f, 0.40f, 1.0f);
	g_materials[index].material_shinyness = (0.078125f * 128);

	wsprintf(str, TEXT("%d"), index);
	MessageBox(NULL, str, TEXT("index"), MB_OK);

	index++;
	FillValueInArr_V1(g_materials[index].material_ambient, 0.0f, 0.05f, 0.05f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_diffused, 0.4f, 0.5f, 0.5f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_specular, 0.04f, 0.7f, 0.7f, 1.0f);
	g_materials[index].material_shinyness = (0.078125f * 128);

	wsprintf(str, TEXT("%d"), index);
	MessageBox(NULL, str, TEXT("index"), MB_OK);

	index++;
	FillValueInArr_V1(g_materials[index].material_ambient, 0.0f, 0.05f, 0.05f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_diffused, 0.4f, 0.5f, 0.5f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_specular, 0.04f, 0.7f, 0.7f, 1.0f);
	g_materials[index].material_shinyness = (0.078125f * 128);

	wsprintf(str, TEXT("%d"), index);
	MessageBox(NULL, str, TEXT("index"), MB_OK);

	index++;
	FillValueInArr_V1(g_materials[index].material_ambient, 0.0f, 0.05f, 0.0f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_diffused, 0.4f, 0.5f, 0.4f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_specular, 0.04f, 0.7f, 0.04f, 1.0f);
	g_materials[index].material_shinyness = (0.078125f * 128);

	wsprintf(str, TEXT("%d"), index);
	MessageBox(NULL, str, TEXT("index"), MB_OK);

	index++;
	FillValueInArr_V1(g_materials[index].material_ambient, 0.05f, 0.0f, 0.0f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_diffused, 0.5f, 0.4f, 0.4f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_specular, 0.7f, 0.04f, 0.04f, 1.0f);
	g_materials[index].material_shinyness = (0.078125f * 128);

	wsprintf(str, TEXT("%d"), index);
	MessageBox(NULL, str, TEXT("index"), MB_OK);

	index++;
	FillValueInArr_V1(g_materials[index].material_ambient, 0.05f, 0.05f, 0.05f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_diffused, 0.5f, 0.5f, 0.5f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_specular, 0.7f, 0.7f, 0.7f, 1.0f);
	g_materials[index].material_shinyness = (0.078125f * 128);

	wsprintf(str, TEXT("%d"), index);
	MessageBox(NULL, str, TEXT("index"), MB_OK);

	index++;
	FillValueInArr_V1(g_materials[index].material_ambient, 0.05f, 0.05f, 0.0f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_diffused, 0.5f, 0.5f, 0.4f, 1.0f);
	FillValueInArr_V1(g_materials[index].material_specular, 0.7f, 0.7f, 0.04f, 1.0f);
	g_materials[index].material_shinyness = (0.078125f * 128);

	wsprintf(str, TEXT("%d"), index);
	MessageBox(NULL, str, TEXT("index"), MB_OK);
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);
	void resize(int, int);
	void LoadMaterials();

	LoadMaterials();

	WNDCLASSEX ac;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("Rotating Pyramid");
	bool bDone = false;

	ac.cbSize = sizeof(WNDCLASSEX);
	ac.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	ac.cbClsExtra = 0;
	ac.cbWndExtra = 0;
	ac.hInstance = hInstance;
	ac.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	ac.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	ac.hCursor = LoadCursor(NULL, IDC_ARROW);
	ac.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	ac.lpfnWndProc = AcCallBack;
	ac.lpszClassName = szClassName;
	ac.lpszMenuName = NULL;

	RegisterClassEx(&ac);

	hwnd = CreateWindowEx(
		WS_EX_APPWINDOW,
		szClassName,
		TEXT("Shree Ganesha"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);

	ghwnd = hwnd;

	initialize();

	ShowWindow(hwnd, SW_SHOWNORMAL);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				display();
				update();
				
				if (gbEscapeKeyIsPressed == true)
				{
					bDone = true;
				}
			}
		}
	}
	uninitialize();
	return ((int)msg.wParam);
}

void initialize(void)
{
	void resize(GLint width, GLint height);

	PIXELFORMATDESCRIPTOR pfd;
	GLint iPixelFormatIndex;

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cBlueBits = 8;
	pfd.cGreenBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	//Defined for GL_LIGHT0
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffused);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);

	//Defined Background color
	glClearColor(0.25f, 0.25f, 0.25f, 0.0f);

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glEnable(GL_LIGHT0);
	glEnable(GL_AUTO_NORMAL);
	glEnable(GL_NORMALIZE);
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT,light_model_ambient);
	glLightModelf(GL_LIGHT_MODEL_LOCAL_VIEWER,light_model_local_viewer);

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void uninitialize(void)
{
	int i, j;

	if (gbFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	if (alldat)
	{
		free(alldat);
		alldat = NULL;
	}

	/*
	if (quadric)
	{
		gluDeleteQuadric(quadric);
		quadric = NULL;
	}

	if (quadric1)
	{
		gluDeleteQuadric(quadric1);
		quadric1 = NULL;
	}

	if (quadric2)
	{
		gluDeleteQuadric(quadric2);
		quadric2 = NULL;
	}

	if (quadric3)
	{
		gluDeleteQuadric(quadric3);
		quadric3 = NULL;
	}

	if (quadric4)
	{
		gluDeleteQuadric(quadric4);
		quadric4 = NULL;
	}

	if (quadric5)
	{
		gluDeleteQuadric(quadric5);
		quadric5 = NULL;
	}

	if (quadric6)
	{
		gluDeleteQuadric(quadric6);
		quadric6 = NULL;
	}

	if (quadric7)
	{
		gluDeleteQuadric(quadric7);
		quadric7 = NULL;
	}

	if (quadric8)
	{
		gluDeleteQuadric(quadric8);
		quadric8 = NULL;
	}

	if (quadric9)
	{
		gluDeleteQuadric(quadric9);
		quadric9 = NULL;
	}

	if (quadric10)
	{
		gluDeleteQuadric(quadric10);
		quadric10 = NULL;
	}

	if (quadric11)
	{
		gluDeleteQuadric(quadric11);
		quadric11 = NULL;
	}

	if (quadric12)
	{
		gluDeleteQuadric(quadric12);
		quadric12 = NULL;
	}

	if (quadric13)
	{
		gluDeleteQuadric(quadric13);
		quadric13 = NULL;
	}

	if (quadric14)
	{
		gluDeleteQuadric(quadric14);
		quadric14 = NULL;
	}

	if (quadric15)
	{
		gluDeleteQuadric(quadric15);
		quadric15 = NULL;
	}

	if (quadric16)
	{
		gluDeleteQuadric(quadric16);
		quadric16 = NULL;
	}

	if (quadric17)
	{
		gluDeleteQuadric(quadric17);
		quadric17 = NULL;
	}

	if (quadric18)
	{
		gluDeleteQuadric(quadric18);
		quadric18 = NULL;
	}

	if (quadric19)
	{
		gluDeleteQuadric(quadric19);
		quadric19 = NULL;
	}

	if (quadric20)
	{
		gluDeleteQuadric(quadric20);
		quadric20 = NULL;
	}

	if (quadric21)
	{
		gluDeleteQuadric(quadric21);
		quadric21 = NULL;
	}

	if (quadric22)
	{
		gluDeleteQuadric(quadric22);
		quadric22 = NULL;
	}

	if (quadric23)
	{
		gluDeleteQuadric(quadric23);
		quadric23 = NULL;
	}
	*/

	for (i=0;i<=3;i++)
	{
		for (j=0;j<=5;j++)
		{
			gluDeleteQuadric(quadric[i][j]);
			quadric[i][j] = NULL;
		}
	}

	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(ghrc);
	ghrc = NULL;
	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;
	DestroyWindow(ghwnd);
	ghwnd = NULL;
}

void resize(GLint width, GLint height)
{
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void ToggleFullScreen(void)
{
	MONITORINFO mi;

	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle&WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle&~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
}

void DrawSphere(AllData *alldat)
{
	glMaterialfv(GL_FRONT, GL_AMBIENT, alldat->AmbientMatArr);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, alldat->DiffusedMatArr);
	glMaterialfv(GL_FRONT, GL_SPECULAR,alldat->SpecularMatArr);
	glMaterialf(GL_FRONT, GL_SHININESS, alldat->ShinynessMat);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(alldat->translateArr[0], alldat->translateArr[1], alldat->translateArr[2]);
	glScalef(alldat->ScalingArr[0], alldat->ScalingArr[1], alldat->ScalingArr[2]);

	if (xkey == TRUE && ykey == FALSE && zkey == FALSE)
	{
		glRotatef(alldat->angleSpin, 1.0f, 0.0f, 0.0f);
		light_position[0] = 0.0f;
		light_position[1] = alldat->angleSpin;
		light_position[2] = 0.0f;
		glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	}
	else if (ykey == TRUE && xkey == FALSE && zkey == FALSE)
	{
		glRotatef(alldat->angleSpin, 0.0f, 1.0f, 0.0f);
		light_position[0] = alldat->angleSpin;
		light_position[1] = 0.0f;
		light_position[2] = 0.0f;
		glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	}
	else if (zkey == TRUE && xkey == FALSE && ykey == FALSE)
	{
		glRotatef(alldat->angleSpin, 0.0f, 0.0f, 1.0f);
		light_position[0] = alldat->angleSpin;
		light_position[1] = 0.0f;
		light_position[2] = 0.0f;
		glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	}
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	alldat->quadric = gluNewQuadric();
	gluSphere(alldat->quadric, 0.75f, 30, 30);
}

void FillOneArraytoAnother(GLfloat *dest,GLfloat *source,int count)
{
	if (count==4)
	{
		for (int i = 0; i <= 3; i++)
		{
			*(dest + i) = *(source + i);
		}
	}
	else if(count==3)
	{
		for (int i = 0; i <= 2; i++)
		{
			*(dest + i) = *(source + i);
		}
	}
	else if (count == 1)
	{
		*dest = *source;
	}
}

void FillValueInArr(GLfloat *dest,GLfloat x,GLfloat y,GLfloat z)
{
	dest[0] = x;
	dest[1] = y;
	dest[2] = z;
}

void FillValueInArr_V1(GLfloat dest[], GLfloat x, GLfloat y, GLfloat z,GLfloat w)
{
	dest[0] = x;
	dest[1] = y;
	dest[2] = z;
	dest[3] = w;
}


void display(void)
{
	void DrawSphere(AllData *alldat);
	void FillOneArraytoAnother(GLfloat *dest, GLfloat *source, int count);
	void FillValueInArr(GLfloat *dest, GLfloat x, GLfloat y, GLfloat z);

	int index = 1;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//----------------------------------------

	FillOneArraytoAnother(alldat->AmbientMatArr,g_materials[index].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, g_materials[index].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, g_materials[index].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &g_materials[index].material_shinyness, 1);
	alldat->quadric = quadric[0][0];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.8f, 0.9f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);
		
	index++;
	FillOneArraytoAnother(alldat->AmbientMatArr, g_materials[index].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, g_materials[index].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, g_materials[index].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &g_materials[index].material_shinyness, 1);
	alldat->quadric = quadric[0][1];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.2f, 0.9f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	index++;
	FillOneArraytoAnother(alldat->AmbientMatArr, g_materials[index].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, g_materials[index].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, g_materials[index].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &g_materials[index].material_shinyness, 1);
	alldat->quadric = quadric[0][2];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 0.4f, 0.9f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	index++;
	FillOneArraytoAnother(alldat->AmbientMatArr, g_materials[index].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, g_materials[index].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, g_materials[index].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &g_materials[index].material_shinyness, 1);
	alldat->quadric = quadric[0][3];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 1.0f, 0.9f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	//----------------------------------------

	index++;
	FillOneArraytoAnother(alldat->AmbientMatArr, g_materials[index].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, g_materials[index].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, g_materials[index].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &g_materials[index].material_shinyness, 1);
	alldat->quadric = quadric [1][0];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.8f, 0.575f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	index++;
	FillOneArraytoAnother(alldat->AmbientMatArr, g_materials[index].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr,g_materials[index].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, g_materials[index].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &g_materials[index].material_shinyness, 1);
	alldat->quadric = quadric[1][1];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.2f, 0.575f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	index++;
	FillOneArraytoAnother(alldat->AmbientMatArr, g_materials[index].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, g_materials[index].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, g_materials[index].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &g_materials[index].material_shinyness, 1);
	alldat->quadric = quadric[1][2];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 0.4f, 0.575f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	//----------------------------------------		
	
	index++;
	FillOneArraytoAnother(alldat->AmbientMatArr, g_materials[index].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, g_materials[index].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, g_materials[index].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &g_materials[index].material_shinyness, 1);
	alldat->quadric = quadric [1][3];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 1.0f, 0.575f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	index++;
	FillOneArraytoAnother(alldat->AmbientMatArr, g_materials[index].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, g_materials[index].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, g_materials[index].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &g_materials[index].material_shinyness, 1);
	alldat->quadric = quadric[2][0];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.8f, 0.25f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f); 
	DrawSphere(alldat);

	index++;
	FillOneArraytoAnother(alldat->AmbientMatArr, g_materials[index].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, g_materials[index].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, g_materials[index].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &g_materials[index].material_shinyness, 1);
	alldat->quadric = quadric[2][1];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.2f, 0.25f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f); 
	DrawSphere(alldat);

	index++;
	FillOneArraytoAnother(alldat->AmbientMatArr, g_materials[index].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, g_materials[index].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, g_materials[index].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &g_materials[index].material_shinyness, 1);
	alldat->quadric = quadric[2][2];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 0.4f, 0.25f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f); 
	DrawSphere(alldat);
	
	//----------------------------------------------

	index++;
	FillOneArraytoAnother(alldat->AmbientMatArr, g_materials[index].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, g_materials[index].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, g_materials[index].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &g_materials[index].material_shinyness, 1);
	alldat->quadric = quadric[2][3];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 1.0f, 0.25f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f); 
	DrawSphere(alldat);

	index++;
	FillOneArraytoAnother(alldat->AmbientMatArr, g_materials[index].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, g_materials[index].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, g_materials[index].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &g_materials[index].material_shinyness, 1);
	alldat->quadric = quadric[3][0];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.8f, -0.075f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f); 
	DrawSphere(alldat);

	index++;
	FillOneArraytoAnother(alldat->AmbientMatArr, g_materials[index].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, g_materials[index].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, g_materials[index].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &g_materials[index].material_shinyness, 1);
	alldat->quadric = quadric[3][1];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.2f, -0.075f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	index++;
	FillOneArraytoAnother(alldat->AmbientMatArr, g_materials[index].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, g_materials[index].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, g_materials[index].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &g_materials[index].material_shinyness, 1);
	alldat->quadric = quadric[3][2];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 0.4f, -0.075f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f); 
	DrawSphere(alldat);

	//----------------------------------------------

	index++;
	FillOneArraytoAnother(alldat->AmbientMatArr, g_materials[index].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, g_materials[index].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, g_materials[index].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &g_materials[index].material_shinyness, 1);
	alldat->quadric = quadric[3][3];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 1.0f, -0.075f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f); 
	DrawSphere(alldat);

	index++;
	FillOneArraytoAnother(alldat->AmbientMatArr, g_materials[index].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, g_materials[index].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, g_materials[index].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &g_materials[index].material_shinyness, 1);
	alldat->quadric = quadric[4][0];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.8f, -0.4f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f); 
	DrawSphere(alldat);

	index++;
	FillOneArraytoAnother(alldat->AmbientMatArr, g_materials[index].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, g_materials[index].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, g_materials[index].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &g_materials[index].material_shinyness, 1);
	alldat->quadric = quadric[4][1];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.2f, -0.4f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	index++;
	FillOneArraytoAnother(alldat->AmbientMatArr, g_materials[index].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, g_materials[index].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, g_materials[index].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &g_materials[index].material_shinyness, 1);
	alldat->quadric = quadric[4][2];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 0.4f, -0.4f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f); 
	DrawSphere(alldat);

	//----------------------------------------------
	
	index++;
	FillOneArraytoAnother(alldat->AmbientMatArr, g_materials[index].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, g_materials[index].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, g_materials[index].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &g_materials[index].material_shinyness, 1);
	alldat->quadric = quadric[4][3];;
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 1.0f, -0.4f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	index++;
	FillOneArraytoAnother(alldat->AmbientMatArr, g_materials[index].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, g_materials[index].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, g_materials[index].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &g_materials[index].material_shinyness, 1);
	alldat->quadric = quadric[5][0];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.8f, -0.725f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f); 
	DrawSphere(alldat);

	index++;
	FillOneArraytoAnother(alldat->AmbientMatArr, g_materials[index].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, g_materials[index].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, g_materials[index].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &g_materials[index].material_shinyness, 1);
	alldat->quadric = quadric[5][1];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.2f, -0.725f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	index++;
	FillOneArraytoAnother(alldat->AmbientMatArr, g_materials[index].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, g_materials[index].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, g_materials[index].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &g_materials[index].material_shinyness, 1);
	alldat->quadric = quadric[5][2];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 0.4f, -0.725f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f); 
	DrawSphere(alldat);

	index++;
	FillOneArraytoAnother(alldat->AmbientMatArr, g_materials[index].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, g_materials[index].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, g_materials[index].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &g_materials[index].material_shinyness, 1);
	alldat->quadric = quadric[5][3];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 1.0f, -0.725f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	index = 1;

	SwapBuffers(ghdc);
}


/*
void display(void)
{
	void DrawSphere(AllData *alldat);
	void FillOneArraytoAnother(GLfloat *dest, GLfloat *source, int count);
	void FillValueInArr(GLfloat *dest, GLfloat x, GLfloat y, GLfloat z);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//----------------------------------------

	FillOneArraytoAnother(alldat->AmbientMatArr, temp[0][0].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr,temp[0][0].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, temp[0][0].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &temp[0][0].material_shinyness, 1);
	alldat->quadric = quadric[0][0];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.8f, 0.9f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, temp[0][1].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr,temp[0][1].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, temp[0][1].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &temp[0][1].material_shinyness, 1);
	alldat->quadric = quadric[0][1];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.2f, 0.9f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, temp[0][2].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, temp[0][2].material_ambient, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, temp[0][2].material_ambient, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &temp[0][2].material_shinyness, 1);
	alldat->quadric = quadric[0][2];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 0.4f, 0.9f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, temp[0][3].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, temp[0][3].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, temp[0][3].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &temp[0][3].material_shinyness, 1);
	alldat->quadric = quadric[0][3];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 1.0f, 0.9f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	//----------------------------------------

	FillOneArraytoAnother(alldat->AmbientMatArr, temp[1][0].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, temp[1][0].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, temp[1][0].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &temp[1][0].material_shinyness, 1);
	alldat->quadric = quadric[1][0];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.8f, 0.575f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);


	FillOneArraytoAnother(alldat->AmbientMatArr, temp[1][1].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, temp[1][1].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, temp[1][1].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &temp[1][1].material_shinyness, 1);
	alldat->quadric = quadric[1][1];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.2f, 0.575f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, temp[1][2].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, temp[1][2].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, temp[1][2].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &temp[1][2].material_shinyness, 1);
	alldat->quadric = quadric[1][2];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 0.4f, 0.575f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	//----------------------------------------		

	FillOneArraytoAnother(alldat->AmbientMatArr, temp[1][3].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, temp[1][3].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, temp[1][3].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &temp[1][3].material_shinyness, 1);
	alldat->quadric = quadric[1][3];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 1.0f, 0.575f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, temp[2][0].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, temp[2][0].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, temp[2][0].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &temp[2][0].material_shinyness, 1);
	alldat->quadric = quadric[2][0];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.8f, 0.25f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, temp[2][1].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, temp[2][1].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, temp[2][1].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &temp[2][1].material_shinyness, 1);	
	alldat->quadric = quadric[2][1];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.2f, 0.25f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, temp[2][2].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, temp[2][2].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, temp[2][2].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &temp[2][2].material_shinyness, 1);
	alldat->quadric = quadric[2][2];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 0.4f, 0.25f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	//----------------------------------------------

	FillOneArraytoAnother(alldat->AmbientMatArr, temp[2][3].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, temp[2][3].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, temp[2][3].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &temp[2][3].material_shinyness, 1);
	alldat->quadric = quadric[2][3];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 1.0f, 0.25f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, temp[3][0].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, temp[3][0].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, temp[3][0].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &temp[3][0].material_shinyness, 1);
	alldat->quadric = quadric[3][0];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.8f, -0.075f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, temp[3][1].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, temp[3][1].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, temp[3][1].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &temp[3][1].material_shinyness, 1);
	alldat->quadric = quadric[3][1];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.2f, -0.075f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, temp[3][2].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, temp[3][2].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, temp[3][2].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &temp[3][2].material_shinyness, 1);
	alldat->quadric = quadric[3][2];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 0.4f, -0.075f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	//----------------------------------------------

	FillOneArraytoAnother(alldat->AmbientMatArr, temp[3][3].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, temp[3][3].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, temp[3][3].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &temp[3][3].material_shinyness, 1);
	alldat->quadric = quadric[3][3];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 1.0f, -0.075f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, temp[4][0].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, temp[4][0].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, temp[4][0].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &temp[4][0].material_shinyness, 1);
	alldat->quadric = quadric[4][0];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.8f, -0.4f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, temp[4][1].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, temp[4][1].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, temp[4][1].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &temp[4][1].material_shinyness, 1);
	alldat->quadric = quadric[4][1];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.2f, -0.4f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, temp[4][2].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, temp[4][2].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, temp[4][2].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &temp[4][2].material_shinyness, 1);
	alldat->quadric = quadric[4][2];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 0.4f, -0.4f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	//----------------------------------------------

	FillOneArraytoAnother(alldat->AmbientMatArr, temp[4][3].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, temp[4][3].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, temp[4][3].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &temp[4][3].material_shinyness, 1);
	alldat->quadric = quadric[4][3];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 1.0f, -0.4f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, temp[5][0].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, temp[5][0].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, temp[5][0].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &temp[5][0].material_shinyness, 1);
	alldat->quadric = quadric[5][0];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.8f, -0.725f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, temp[5][1].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, temp[5][1].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, temp[5][1].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &temp[5][1].material_shinyness, 1);
	alldat->quadric = quadric[5][1];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, -0.2f, -0.725f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, temp[5][2].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, temp[5][2].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, temp[5][2].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &temp[5][2].material_shinyness, 1);
	alldat->quadric = quadric[5][2];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 0.4f, -0.725f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	FillOneArraytoAnother(alldat->AmbientMatArr, temp[5][3].material_ambient, 4);
	FillOneArraytoAnother(alldat->DiffusedMatArr, temp[5][3].material_diffused, 4);
	FillOneArraytoAnother(alldat->SpecularMatArr, temp[5][3].material_specular, 4);
	FillOneArraytoAnother(&alldat->ShinynessMat, &temp[5][3].material_shinyness, 1);
	alldat->quadric = quadric[5][3];
	alldat->angleSpin = angleSpin;
	FillValueInArr(alldat->translateArr, 1.0f, -0.725f, -3.0f); //c
	FillValueInArr(alldat->ScalingArr, 0.2f, 0.2f, 0.2f);
	DrawSphere(alldat);

	SwapBuffers(ghdc);
}
*/

LRESULT CALLBACK AcCallBack(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void uninitialize(void);
	void resize(GLint width, GLint height);
	void ToggleFullScreen(void);

	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
		{
			gbActiveWindow = true;
		}
		else
		{
			gbActiveWindow = false;
		}
		break;

	case WM_ERASEBKGND:
		return  0;

	case WM_LBUTTONDOWN:
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_CLOSE:
		uninitialize();
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			if (gbEscapeKeyIsPressed == false)
			{
				gbEscapeKeyIsPressed = true;
			}
			break;

		case 0x46:
			if (gbFullScreen == false)
			{
				ToggleFullScreen();
				gbFullScreen = true;
			}
			else
			{
				ToggleFullScreen();
				gbFullScreen = false;
			}
			break;

		case 0x4C:
			//L key
			switch (lightkey)
			{
			case TRUE:
				lightkey = FALSE;
				glDisable(GL_LIGHTING);
				break;

			case FALSE:
				lightkey = TRUE;
				glEnable(GL_LIGHTING);
				break;
			}
			break;

		case 0x57:  //w button  (Stop at position and Resume at original position)
			xkey = FALSE;
			ykey = FALSE;
			zkey = FALSE;
			light_position[0] = 0.0f;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			break;

		case 0x58: //x button
			xkey = TRUE;
			ykey = FALSE;
			zkey = FALSE;
			angleSpin = 0.0f;
			break;

		case 0x59: //y button
			xkey = FALSE;
			ykey = TRUE;
			zkey = FALSE;
			angleSpin = 0.0f;
			break;	

		case 0x5A: //z button
			xkey = FALSE;
			ykey = FALSE;
			zkey = TRUE;
			angleSpin = 0.0f;
			break;

		default:
			break;
		}
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}
	return DefWindowProc(hwnd, iMsg, wParam, lParam);
}