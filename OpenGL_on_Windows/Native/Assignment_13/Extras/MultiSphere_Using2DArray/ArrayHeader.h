#pragma once
#include<Windows.h>
#include<gl/GL.h>
#include <gl/GLU.h>

BOOL lightkey = FALSE;

BOOL stopRotation = FALSE;

/*
//Defined quadric
GLUquadric *quadric = NULL;
GLUquadric *quadric1 = NULL;
GLUquadric *quadric2 = NULL;
GLUquadric *quadric3 = NULL;
GLUquadric *quadric4 = NULL;
GLUquadric *quadric5 = NULL;
GLUquadric *quadric6 = NULL;
GLUquadric *quadric7 = NULL;
GLUquadric *quadric8 = NULL;
GLUquadric *quadric9 = NULL;
GLUquadric *quadric10 = NULL;
GLUquadric *quadric11 = NULL;
GLUquadric *quadric12 = NULL;
GLUquadric *quadric13 = NULL;
GLUquadric *quadric14 = NULL;
GLUquadric *quadric15 = NULL;
GLUquadric *quadric16 = NULL;
GLUquadric *quadric17 = NULL;
GLUquadric *quadric18 = NULL;
GLUquadric *quadric19 = NULL;
GLUquadric *quadric20 = NULL;
GLUquadric *quadric21 = NULL;
GLUquadric *quadric22 = NULL;
GLUquadric *quadric23 = NULL;
*/

GLUquadric *quadric[4][6];

typedef struct
{
	GLfloat material_ambient[4];
	GLfloat material_diffused[4];
	GLfloat material_specular[4];
	GLfloat material_shinyness;
}MaterialArray;

MaterialArray temp[6][4];

BOOL xkey = FALSE;
BOOL ykey = FALSE;
BOOL zkey = FALSE;

//For Light 0 
GLfloat light_ambient[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat light_diffused[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat light_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat light_position[] = { 0.0f,0.0f,0.0f,0.0f };

GLfloat light_model_ambient[] = {0.2f,0.2f ,0.2f ,0.0f};
GLfloat light_model_local_viewer = 0.0f;

//----------------------------------------------------------------

typedef struct
{
	GLfloat translateArr[3];
	GLfloat ScalingArr[3];
	GLfloat AmbientMatArr[4];
	GLfloat DiffusedMatArr[4];
	GLfloat SpecularMatArr[4];
	GLfloat ShinynessMat;
	GLUquadric *quadric;
	GLfloat angleSpin;
}AllData;

AllData *alldat = (AllData*)malloc(sizeof(AllData));


