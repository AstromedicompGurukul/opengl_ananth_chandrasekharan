#pragma once
#include<Windows.h>

#include<gl/GL.h>
#include<gl/GLU.h>

#include<stdio.h>
#include<stdlib.h>

#include<vector>

LRESULT CALLBACK AcCallBack(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam);

#define WIN_INIT_X 100
#define WIN_INIT_Y 100
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#define VK_F 0x46
#define VK_f 0x60

#define FOV_ANGLE 45
#define ZNEAR 0.1
#define ZFAR 200.0

#define VIEWPORT_BOTTOM_LEFT_X 0
#define VIEWPORT_BOTTOM_LEFT_Y 0

#pragma comment(lib,"opengl32.lib")
#pragma comment (lib,"glu32.lib")
#pragma comment (lib,"kernel32.lib")
#pragma comment (lib,"user32.lib")
#pragma comment (lib,"gdi32.lib")

#define ERRORBOX1(lpszErrorMessage,lpszCaption)\
{\
MessageBox((HWND)NULL,TEXT(lpszErrorMessage),TEXT(lpszCaption),MB_ICONERROR);\
ExitProcess(EXIT_FAILURE);}\

#define ERRORBOX2(lpszErrorMessage,lpszCaption)\
{\
MessageBox((HWND)NULL,TEXT(lpszErrorMessage),TEXT(lpszCaption),MB_ICONERROR);\
DestroyWindow(hwnd)}\


HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullScreen = false;
bool lightkey = false;