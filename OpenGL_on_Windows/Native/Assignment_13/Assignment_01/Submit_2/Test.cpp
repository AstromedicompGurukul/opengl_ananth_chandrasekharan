#include "CodeVariables.h"

void update(void)
{
	g_rotate = g_rotate + MONKEY_HEAD_ANGLE_INCREMENT;

	if (g_rotate>=END_ANGLE_POS)
	{
		g_rotate = START_ANGLE_POS;
	}
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	WNDCLASSEX ac;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("Windows Template Code");
	bool bDone = false;

	ac.cbSize = sizeof(WNDCLASSEX);
	ac.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	ac.cbClsExtra = 0;
	ac.cbWndExtra = 0;
	ac.hInstance = hInstance;
	ac.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	ac.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	ac.hCursor = LoadCursor(NULL, IDC_ARROW);
	ac.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	ac.lpfnWndProc = AcCallBack;
	ac.lpszClassName = szClassName;
	ac.lpszMenuName = NULL;

	RegisterClassEx(&ac);

	hwnd = CreateWindowEx(
		WS_EX_APPWINDOW,
		szClassName,
		TEXT("Shree Ganesha"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		WIN_INIT_X,
		WIN_INIT_Y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);

	ghwnd = hwnd;

	initialize();

	ShowWindow(hwnd, SW_SHOWNORMAL);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				update();
				display();
				if (gbEscapeKeyIsPressed == true)
				{
					bDone = true;
				}
			}
		}
	}
	uninitialize();
	return ((int)msg.wParam);
}

void LoadMeshData(void)
{
	void uninitialize(void);

	int ret = -1;

	ret=fopen_s(&g_fp_meshfile,"MonkeyHead.OBJ","r");
	if (ret != 0)
	{
		uninitialize();
	}
	else
	{
		MessageBox(ghwnd,TEXT("MonkeyHead.OBJ file loaded"),TEXT("Message"),MB_OK);
	}

	char *sep_space = " ";

	char *sep_fslash = "/";

	char *first_token = NULL;

	char *token = NULL;

	char *face_tokens[NR_FACE_TOKENS];

	int nr_tokens;

	char *token_vertex_index = NULL;

	char *token_texture_index = NULL;

	char *token_normal_index = NULL;

	while (fgets(line,BUFFER_SIZE,g_fp_meshfile)!=NULL)
	{
		first_token=strtok(line,sep_space);
		
		if (strcmp(first_token,"v")==S_EQUAL)
		{
			std::vector<float> vec_point_coord(NR_POINT_COORDS);

			for (int i=0;i!=NR_POINT_COORDS;i++)
			{
				vec_point_coord[i] = atof(strtok(NULL,sep_space));
			}
			g_vertices.push_back(vec_point_coord);
		}
		else if (strcmp(first_token, "vt") == S_EQUAL)
		{
			std::vector<float> vec_texture_coord(NR_TEXTURE_COORDS);

			for (int i = 0; i != NR_TEXTURE_COORDS; i++)
			{
				vec_texture_coord[i] = atof(strtok(NULL, sep_space));
			}
			g_texture.push_back(vec_texture_coord);
		}
		else if (strcmp(first_token, "vn") == S_EQUAL)
		{
			std::vector<float> vec_normal_coord(NR_NORMAL_COORDS);

			for (int i = 0; i != NR_NORMAL_COORDS; i++)
			{
				vec_normal_coord[i] = atof(strtok(NULL, sep_space));
			}
			g_normals.push_back(vec_normal_coord);
		}
		else if (strcmp(first_token,"f")==S_EQUAL)
		{
			std::vector<int> triangle_vertex_indices(3);
			std::vector<int> texture_vertex_indices(3);
			std::vector<int> normal_vertex_indices(3);

			memset((void*)face_tokens,0,NR_FACE_TOKENS);

			nr_tokens = 0;
			while (token=strtok(NULL,sep_space))
			{
				if (strlen(token)<3)
				{
					break;
				}
				face_tokens[nr_tokens] = token;
				nr_tokens++;
			}

			for (int i=0;i!=NR_FACE_TOKENS;++i)
			{
				token_vertex_index = strtok(face_tokens[i],sep_fslash);
				token_texture_index = strtok(NULL, sep_fslash);
				token_normal_index = strtok(NULL, sep_fslash);
				triangle_vertex_indices[i] = atoi(token_vertex_index);
				texture_vertex_indices[i] = atoi(token_texture_index);
				normal_vertex_indices[i] = atoi(token_normal_index);
			}

			g_face_tri.push_back(triangle_vertex_indices);
			g_face_texture.push_back(texture_vertex_indices);
			g_face_normals.push_back(normal_vertex_indices);
		}
		memset((void*)line,(int)'\0',BUFFER_SIZE);
	}
	fclose(g_fp_meshfile);

	fprintf(g_fp_logfile,"g_vertices:%llu g_texture:%llu g_normals:%llu g_face_tri:%llu\n",g_vertices.size(),g_texture.size(),g_normals.size(),g_face_tri.size());
}

void initialize(void)
{
	void resize(UINT width, UINT height);
	void uninitialize(void);
	void LoadMeshData(void);
	
	int ret;

	 ret=fopen_s(&g_fp_logfile,"MonkeyHeader.log","w");
	if (ret!=0)
	{
		uninitialize();
	}

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex=-1;

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cBlueBits = 8;
	pfd.cGreenBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghrc = NULL;

	}

	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffused);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness);


	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glClearDepth(1.0f);
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);
	glEnable(GL_LIGHT0);

	LoadMeshData();

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void uninitialize(void)
{
	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
	
	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(ghrc);
	ghrc = NULL;
	
	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;
	
	fclose(g_fp_logfile);

	DestroyWindow(ghwnd);
}

void resize(UINT width, UINT height)
{
	if (height == 0)
	{
		height = 1;
	}
	glViewport(VIEWPORT_BOTTOM_LEFT_X, VIEWPORT_BOTTOM_LEFT_Y, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(FOV_ANGLE, (GLfloat)width / (GLfloat)height, ZNEAR, ZFAR);
}

void ToggleFullScreen(void)
{
	BOOL isWindowPlacement = FALSE;
	BOOL isMonitorInfo = FALSE;
	HMONITOR hMonitor = NULL;
	MONITORINFO mi;

	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			isWindowPlacement = GetWindowPlacement(ghwnd, &wpPrev);
			hMonitor = MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY);
			mi = { sizeof(MONITORINFO) };
			isMonitorInfo = GetMonitorInfo(hMonitor, &mi);

			if (isWindowPlacement == TRUE && isMonitorInfo == TRUE)
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle&~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(MONKEY_HEAD_X_TRANSLATE,MONKEY_HEAD_Y_TRANSLATE,MONKEY_HEAD_Z_TRANSLATE);
	glScalef(MONKEY_HEAD_X_SCALEFACTOR,MONKEY_HEAD_Y_SCLAEFACTOR,MONKEY_HEAD_Z_SCALEFACTOR);
	glRotatef(g_rotate, 0.0f, 1.0f, 0.0f);

	glFrontFace(GL_CCW);

	glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);

	for (int i=0;i!=g_face_tri.size();++i)
	{
		glBegin(GL_TRIANGLES);
		for (int j=0;j!=g_face_tri[i].size();j++)
		{
			int vi = g_face_tri[i][j] - 1;
			int ni = g_face_normals[i][j] - 1;
			glNormal3f(g_normals[ni][0],g_normals[ni][1],g_normals[ni][2]);
			glVertex3f(g_vertices[vi][0],g_vertices[vi][1],g_vertices[vi][2]);
		}
		glEnd();
	}
	
	SwapBuffers(ghdc);
}

LRESULT CALLBACK AcCallBack(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void uninitialize(void);
	void resize(UINT width, UINT height);
	void ToggleFullScreen(void);

	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
		{
			gbActiveWindow = true;
		}
		else
		{
			gbActiveWindow = false;
		}
		break;

	case WM_ERASEBKGND:
		return  0;

	case WM_LBUTTONDOWN:
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_CLOSE:
		uninitialize();
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			if (gbEscapeKeyIsPressed == false)
			{
				gbEscapeKeyIsPressed = true;
			}
			break;

		case 0x46:
			if (gbFullScreen == false)
			{
				ToggleFullScreen();
				gbFullScreen = true;
			}
			else
			{
				ToggleFullScreen();
				gbFullScreen = false;
			}
			break;

		case 0x4C:
			switch (lightkey)
			{
			case TRUE:
				lightkey = FALSE;
				glEnable(GL_LIGHT0);
				glDisable(GL_LIGHTING);
				break;

			case FALSE:
				lightkey = TRUE;
				glEnable(GL_LIGHT0);
				glEnable(GL_LIGHTING);
				break;
			}
			break;

		default:
			break;
		}
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}
	return DefWindowProc(hwnd, iMsg, wParam, lParam);
}