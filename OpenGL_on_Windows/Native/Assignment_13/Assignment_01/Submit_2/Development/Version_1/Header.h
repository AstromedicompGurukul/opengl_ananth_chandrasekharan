#pragma once
#include<Windows.h>

#include<gl/GL.h>
#include<gl/GLU.h>

#include<stdio.h>
#include<stdlib.h>

#include<vector>

//--------------------------------
#include<math.h>

#define BUFFER_SIZE 256
#define S_EQUAL 0

#define NR_POINT_COORDS 3
#define NR_TEXTURE_COORDS 2
#define NR_NORMAL_COORDS 3
#define NR_FACE_TOKENS 3

#define MONKEY_HEAD_X_TRANSLATE 0.0f
#define MONKEY_HEAD_Y_TRANSLATE 0.0f
#define MONKEY_HEAD_Z_TRANSLATE -5.0f

#define MONKEY_HEAD_X_SCALEFACTOR 0.0f
#define MONKEY_HEAD_Y_SCLAEFACTOR 0.0f
#define MONKEY_HEAD_Z_SCALEFACTOR -5.0f

#define START_ANGLE_POS 0.0f
#define END_ANGLE_POS 360.0f
#define MONKEY_HEAD_ANGLE_INCREMENT  1.0f
//--------------------------------

#define WIN_INIT_X 100
#define WIN_INIT_Y 100
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#define VK_F 0x46
#define VK_f 0x60

#define FOV_ANGLE 45
#define ZNEAR 0.1
#define ZFAR 200.0

#define VIEWPORT_BOTTOM_LEFT_X 0
#define VIEWPORT_BOTTOM_LEFT_Y 0

#pragma comment(lib,"opengl32.lib")
#pragma comment (lib,"glu32.lib")
#pragma comment (lib,"kernel32.lib")
#pragma comment (lib,"user32.lib")
#pragma comment (lib,"gdi32.lib")

#define ERRORBOX1(lpszErrorMessage,lpszCaption)\
{\
MessageBox((HWND)NULL,TEXT(lpszErrorMessage),TEXT(lpszCaption),MB_ICONERROR);\
ExitProcess(EXIT_FAILURE);}\

#define ERRORBOX2(lpszErrorMessage,lpszCaption)\
{\
MessageBox((HWND)NULL,TEXT(lpszErrorMessage),TEXT(lpszCaption),MB_ICONERROR);\
DestroyWindow(hwnd)}\


HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullScreen = false;
