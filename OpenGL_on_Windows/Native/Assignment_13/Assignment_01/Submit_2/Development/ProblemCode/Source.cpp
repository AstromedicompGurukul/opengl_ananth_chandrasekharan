#include "Header.h"
LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

HWND ghwnd = NULL;

int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpszCmdLine,int nCmdShow)
{
	void initialize(void);
	void update(void);
	void display(void);
	void uninitialize(void);

	static TCHAR szAppName[] = TEXT("Mesh Loading Version 2");

	HWND hwnd = NULL;
	HCURSOR hCursor = NULL;
	HBRUSH hBrush = NULL;
	HICON hIcon = NULL;
	HICON hIconSm = NULL;

	bool bDone = false;

	WNDCLASSEX wndclass;

	MSG msg;

	ZeroMemory((void*)&wndclass,sizeof(WNDCLASSEX));
	ZeroMemory((void*)&msg, sizeof(MSG));

	hBrush = (HBRUSH)GetStockObject(BLACK_BRUSH);
	if (!hBrush)
	{
		ERRORBOX1("Error in getting stock object","GetStockObject Error");
	}

	hCursor = LoadCursor((HINSTANCE)NULL,IDC_ARROW);
	if (!hCursor)
	{
		ERRORBOX1("Error in loading cursor", "LoadCursor Error");
	}

	hIcon=LoadIcon((HINSTANCE)NULL,IDI_APPLICATION);
	if (!hIcon)
	{
		ERRORBOX1("Error in loading icon", "LoadIcon Error");
	}

	hIconSm = LoadIcon((HINSTANCE)NULL, IDI_APPLICATION);
	if (!hIconSm)
	{
		ERRORBOX1("Error in loading icon", "LoadIcon Error");
	}

	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.hbrBackground = hBrush;
	wndclass.hCursor = hCursor;
	wndclass.hIcon = hIcon;
	wndclass.hIconSm = hIconSm;
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;


	if (!RegisterClassEx(&wndclass))
	{
		ERRORBOX1("Error in registering a class","RegisterClassEx");
	}

	hwnd = CreateWindowEx(
		WS_EX_APPWINDOW,
		szAppName,
		szAppName,
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		WIN_INIT_X,
		WIN_INIT_Y,
		WIN_WIDTH,
		WIN_HEIGHT,
		(HWND)NULL,
		(HMENU)NULL,
		hInstance,
		(LPVOID)NULL
	);

	if (!hwnd)
	{
		ERRORBOX1("Error in creating a window in memory","CreateWindow Error");
	}

	ghwnd = hwnd;

	initialize();

	ShowWindow(hwnd,SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	while(bDone==false)
	{
		if (PeekMessage(&msg,(HWND)NULL,0,0,PM_REMOVE))
		{
			if (msg.message==WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (g_bActiveWindow==true)
			{
				if (g_bEscapeKeyIsPressed)
				{
					bDone = true;
				}
				else
				{
					update();
					display();
				}
			}
		}
	}
	uninitialize();
	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void uninitialize(void);
	void resize(int width,int height);
	void ToggleFullScreen(void);

	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
		{
			g_bActiveWindow = true;
		}
		else
		{
			g_bActiveWindow = false;
		}
		break;

		case WM_ERASEBKGND:
		return  0;

	case WM_LBUTTONDOWN:
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_CLOSE:
		uninitialize();
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			if (g_bEscapeKeyIsPressed == false)
			{
				g_bEscapeKeyIsPressed = true;
			}
			break;

		case 0x46:
			if (g_bFullScreen == false)
			{
				ToggleFullScreen();
				g_bFullScreen = true;
			}
			else
			{
				ToggleFullScreen();
				g_bFullScreen = false;
			}
			break;

		default:
			break;
		}
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}
	return DefWindowProc(hwnd, iMsg, wParam, lParam);
}

void initialize(void)
{
	void resize(int width,int height);
	void uninitilaize(void);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = -1;

	ZeroMemory((void*)&pfd,sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL|PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cBlueBits = 8;
	pfd.cGreenBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 24;

	g_hdc = GetDC(g_hwnd);

	iPixelFormatIndex = ChoosePixelFormat(g_hdc,&pfd);
	if (iPixelFormatIndex==0)
	{
		ReleaseDC(g_hwnd,g_hdc);
		g_hdc = NULL;
	}

	if (SetPixelFormat(g_hdc,iPixelFormatIndex,&pfd)==false)
	{
		ReleaseDC(g_hwnd,g_hdc);
		g_hdc = NULL;
	}

	if (wglMakeCurrent(g_hdc,g_hrc)==false)
	{
		wglDeleteContext(g_hrc);
		g_hrc = NULL;
		ReleaseDC(g_hwnd, g_hdc);
		g_hdc = NULL;
	}

	glClearColor(0.0f,0.0f,0.0f,0.0f);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glClearDepth(1.0f);
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);

	resize(WIN_WIDTH,WIN_HEIGHT);
}

void update(void)
{

}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glLineWidth(3.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-1.0f, 0.0f, 0.0f);
	glVertex3f(1.0f, 0.0f, 0.0f);
	glEnd();
	SwapBuffers(g_hdc);
}

void uninitialize(void)
{
	if (g_bFullScreen==true)
	{
		g_dwStyle = GetWindowLong(g_hwnd,GWL_STYLE);
		SetWindowLong(g_hwnd,GWL_STYLE,g_dwStyle|WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(g_hwnd,&g_wpPrev);
		SetWindowPos(g_hwnd,HWND_TOP,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE|SWP_NOOWNERZORDER|SWP_NOZORDER|SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	wglMakeCurrent((HDC)NULL,(HGLRC)NULL);
	g_hrc = (HGLRC)NULL;

	ReleaseDC(g_hwnd,g_hdc);
	g_hdc = (HDC)NULL;

	DestroyWindow(ghwnd);
}

void resize(int width,int height)
{
	if (height==0)
	{
		height = 1;
	}

	glViewport(VIEWPORT_BOTTOMLEFT_X,VIEWPORT_BOTTOMLEFT_Y,(GLsizei)width,(GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(FOY_ANGLE,(GLfloat)width/(GLfloat)height,ZNEAR,ZFAR);
}

void ToggleFullScreen(void)
{
	BOOL isWindowPlacement = FALSE;
	BOOL isMonitorInfo = FALSE;
	HMONITOR hMonitor = NULL;
	MONITORINFO mi;

	if (g_bFullScreen == false)
	{
		g_dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (g_dwStyle & WS_OVERLAPPEDWINDOW)
		{
			isWindowPlacement = GetWindowPlacement(ghwnd, &g_wpPrev);
			hMonitor = MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY);
			mi = { sizeof(MONITORINFO) };
			isMonitorInfo = GetMonitorInfo(hMonitor, &mi);

			if (isWindowPlacement == TRUE && isMonitorInfo == TRUE)
			{
				SetWindowLong(ghwnd, GWL_STYLE, g_dwStyle&~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, g_dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &g_wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
}

