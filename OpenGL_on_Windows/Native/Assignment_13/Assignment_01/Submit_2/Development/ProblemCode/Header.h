#pragma once
#include<Windows.h>
#include<stdio.h>
#include<stdlib.h>

#include<vector>

#include<gl/GL.h>
#include<gl/GLU.h>

#define TRUE 1
#define FALSE 0

#define BUFFER_SIZE 256
#define S_EQUAL 0

#define WIN_INIT_X 100
#define WIN_INIT_Y 100
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#define VK_F 0x46
#define VK_f 0x60

#define NR_POINT_COORDS 3
#define NR_TEXTURE_COORDS 2
#define NR_NORMAL_COORDS 3
#define NR_FACE_TOKENS 3

#define FOY_ANGLE 45
#define ZNEAR 0.1
#define ZFAR 200.0

#define VIEWPORT_BOTTOMLEFT_X 0
#define VIEWPORT_BOTTOMLEFT_Y 0

#define MONKEY_HEAD_X_TRANSLATE 0.0f
#define MONKEY_HEAD_Y_TRANSLATE 0.0f
#define MONKEY_HEAD_Z_TRANSLATE -5.0f

#define MONKEY_HEAD_X_SCALEFACTOR 1.5f
#define MONKEY_HEAD_Y_SCALEFACTOR 1.5f
#define MONKEY_HEAD_Z_SCALEFACTOR 1.5f

#define START_ANGLE_POS 0.0f
#define END_ANGLE_POS 360.0f
#define MONKEY_HEAD_ANGLE_INCREMENT 1.0f

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"gdi32.lib")
#pragma comment(lib,"kernel32.lib")

#define ERRORBOX1(lpszErrorMessage,lpszCaption)\
{\
MessageBox((HWND)NULL,TEXT(lpszErrorMessage),TEXT(lpszCaption),MB_ICONERROR);\
ExitProcess(EXIT_FAILURE);}\

#define ERRORBOX2(lpszErrorMessage,lpszCaption)\
{\
MessageBox((HWND)NULL,TEXT(lpszErrorMessage),TEXT(lpszCaption),MB_ICONERROR);\
DestroyWindow(hwnd)}\

HWND g_hwnd = NULL;
HDC g_hdc = NULL;
HGLRC g_hrc = NULL;

DWORD g_dwStyle = NULL;
WINDOWPLACEMENT g_wpPrev;

bool g_bActiveWindow=false;
bool g_bEscapeKeyIsPressed = false;
bool g_bFullScreen = false;

GLfloat g_rotate;

std::vector<std::vector<float>> g_vertices;
std::vector<std::vector<float>> g_texture;
std::vector<std::vector<float>> g_normals;
std::vector<std::vector<int>> g_face_tri, g_face_texture, g_face_normals;

FILE *g_fp_meshfile = NULL;
FILE *g_fp_logfile = NULL;

char line[BUFFER_SIZE];