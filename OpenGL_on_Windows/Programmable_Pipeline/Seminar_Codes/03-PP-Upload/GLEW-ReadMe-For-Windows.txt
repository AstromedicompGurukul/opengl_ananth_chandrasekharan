I have performed the following steps on Microsoft Visual Studio 2015 and on 
Windows 10 (64-bit) Operating System.
=============================================================================

PROCEDURE :
************

Step 1 : DOWNLOADING GLEW !!!
-----------------------------------
Firsty, download the latest 'glew' for whichever Operating System you shall 
be working in. Suggested website for downloading glew :- 
http://glew.sourceforge.net/index.html. (Here, go to Box under the title 'Downloads'.
You will see something like this: "Source ZIP | TGZ Bineries Windows 32-bit and 64-bit".
Select 'ZIP' file from that. It will be downloaded as a Zip File Named : glew-<version number>-win32.zip

Step 2 : ABOUT THE DOWNLOADED GLEW PACKAGE !!!
----------------------------------------------------
The downloaded ZIP file will have a name like 'glew-2.1.0-win32'. Upon
decompressing it, a directory with the same name as the ZIP file will be
created. This directory will have 4 sub-directories as follows-

-> 'bin' directory - containing a sub-directory named 'Release' which contains the 
    32-bit and 64-bit DLLs. The 32-bit DLL shall be in 'Win32' directory and 
    64-bit DLL shall be in the 'x64' directory within the 'Release' directory.

-> 'doc' directory - containing documentation.

-> 'include' directory -> containing a sub-directory named 'GL' which contains
    four header files - eglew.h, glew.h, glxew.h and wglew.h

-> 'lib' directory -> containing a sub-directory named 'Release' which contains the 
    32-bit and 64-bit import libraries (.lib files). The 32-bit import library shall be 
    in 'Win32' directory and 64-bit import library shall be in the 'x64' directory within 
    the 'Release' directory.

-> LICENSE.txt -> A text file containing license and copyright information.

Step 3 : RENAME AND RELOCATE GLEW DIRECTORY !!!
-----------------------------------------------------
Rename the unzipped directory as 'glew', for easy referencing.
Move or copy this directory to an easily accessible and important location, for
instance, move it directly to the C: drive (i.e: Root Drive). Now, we shall
access glew from the C: drive itself (i.e: C:\glew\).

Step 4 : RELOCATE GLEW DLLs (DYNAMIC LINK LIBRARIES) !!!
------------------------------------------------------------
Visual Studio creates a 32-bit project by default. Hence, without making any
changes to it, copy the 32-bit DLL (C:\glew\bin\Release\Win32\glew32.dll) from the 
'bin\Release' directory, i.e: the DLL which is NOT in 'x64' sub-directory of 
'bin\Release' directory, to:

1) C:\Windows\System32\ 	(If your Operating System is 32-bit)
2) C:\Windows\SysWOW64\		(If your Operating System is 64-bit)   

Step 5 : CREATE A PROJECT !!!
------------------------------
1) Open Visual Studio IDE (Integrated Development Environment). DO NOT CLOSE THE
   IDE TILL YOU HAVE SUCCESSFULLY COMPLETED THE EXECUTION OF YOUR PROGRAM.

2) Go to 'File' menu -> 'New' -> 'Project...'

3) The 'New Project' dialog box appears. Select 'Visual C++' from the 'Installed
   Templates' under 'Recent Templates' in the left pane. Then, Select 'Win32
   Project' from the right pane. Mostly, this step is done by default.

4) a) Give a suitable name to the project by typing it in the 'Name' text box.
   b) Give the path (location) where the project is to be stored in the
      'Location' text box.
   c) Uncheck the 'Create directory for solution' checkbox.

5) Click the 'OK' button.

6) The 'Win32 Application Wizard - <project name>' dialog box appears.
   a) Select 'Application Settings' from the left pane.

   b) In the right pane, under 'Application type : ' select the 'Windows
      application' radio button and under 'Additional options : ' check the
      'Empty Project' checkbox.

   c) Click the 'Finish' button.
   
7) The project is now created.

8) Usually, the 'Solution Explorer' is open at the left side of the client area.
   If it is not present, open it by going to 'View' menu -> 'Solution Explorer'.
   
Step 6 : ADD NEW SOURCE CODE FILE(S) TO THE CREATED PROJECT !!!
----------------------------------------------------------------
1) In the Visual Studio IDE, go to the 'Project' menu -> 'Add New Item...' OR
   right-click on the project hive (NOT the solution hive) in the 'Solution
   Explorer' -> (from the floating menu) -> 'Add' -> 'New Item...'.
   The 'Add New Item - <project name>' dialog box appears.

2) In the left pane of this dialog box, under 'Installed Templates', select
   'Visual C++' (usually done by default). From the right pane, select the
    file type desired.

3) a) In the 'Name : ' text box below, type in the desired name for the file
      along with the correct extension.

   b) Check that the path specified by default in the 'Location : ' text box
      below, is the path of the appropriate project for which this file is to be
      written.

   c) Click the 'Add' button.
    
   d) The file, as specified, is added to the project and can be seen clearly in
      the client area as well as under 'Source Files' under the project hive in
      the 'Solution Explorer'.

Step 7 : MAKE THE PROJECT GLEW COMPATIBLE !!!
--------------------------------------------------
Assumption : The unzipped directory of the downloaded glew zip exists by the
name of 'glew' directly on C: drive (i.e: C:\glew\), in accordance with
'Step 3' above. It is not mandatory to store the glew directory directly on
C: drive. We have done so just for easy and quick access to it.

1) In the Visual Studio IDE, in the 'Solution Explorer', right-click on the
   project hive and from the floating menu, select 'Properties' or go to
   'Project' menu -> 'Properties'. The '<project name> Property Pages' dialog
   box appears.

2) a) In the left pane, click on the arrow against 'C/C++' and from the options
      that appear thereafter, select 'General'. In the right pane, against
      'Additional Include Directories', type in the path of the 'include'
      directory of 'glew'. That is, in our case, we shall type 
      'C:\glew\include' (without the quotes). Then, press 'Enter' key from  
      keyboard. The entered path appears bold, hence confirming it. Click the
      'Apply' button. Do not close the dialog box. Do not click the 'OK' button
      just yet as it shall close the dialog box.

   b) Now, in the left pane of the dialog box, click on the arrow against
      'Linker' and from the options that appear thereafter, select 'General'.
      In the right pane, against 'Additional Library Directories', type in the
      path of the 'lib' directory of 'glew'. That is, in our case, we shall
      type 'C:\glew\lib\Release\Win32' (without the quotes). Then, press 'Enter' key from
      keyboard. The entered path appears bold, hence confirming it. Click the
      'Apply' button. Now, click the 'OK' button.

Step 8 : START CODING !!!
--------------------------
You can now start coding in the source code files. Upon finishing the writing of
your code, save it and go to 'Build' menu -> 'Rebuild <project name>'. The
entire source code of your project gets compiled and linked. Any errors or
warnings occurring, will be displayed in the 'Output Window' below. Once, your
code is free of errors and warnings, 'Build succeeded' will appear in the
'Output Window'.

Step 9 : EXECUTE YOUR CODE !!!
-------------------------------
Open 'Windows Explorer' and browse to your project directory. Inside it, you
will find the 'Debug' directory. Open it. You will see your executable file in
it which will have the name of your project followed by he extension '.exe'.
That is, your executable file will look like '<project name>.exe'.
Double-click on the .exe file to execute your code.
Once satisfied with the execution of the program, you may close the window,
hence terminating the application.


CONCLUSION :
*************
From now onwards, whenever you create an OpenGL Project which is compatible with
GLEW, you must follow Steps 5 to 9 as mentioned above, for a successful
application. Steps 1 to 4 need to be done only once.
