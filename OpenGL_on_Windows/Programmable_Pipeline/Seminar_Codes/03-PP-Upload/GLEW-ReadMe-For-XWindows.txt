Using GLEW for XWindows

PROCEDURE :
************

Step 1 : DOWNLOADING GLEW !!!
-----------------------------------
Firsty, download the latest 'glew' for whichever Operating System you shall 
be working in. Suggested website for downloading glew :- 
http://glew.sourceforge.net/index.html. (Here, go to Box under the title 'Downloads'.
You will see something like this: "Source ZIP | TGZ Bineries Windows 32-bit and 64-bit".
Select 'TGZ' file from that. It will be downloaded as a TGZ File Named : glew-<version number>

The downloaded ZIP file will have a name like 'glew-2.1.0'

Step 2 : EXTRACT this directory.

Step 3 : On 'Terminal', 'cd' (without quotes) to that directory.

Step 4 : Type cmd: 'make' (without quotes) and press 'Enter'.

Step 5 : Type cmd: 'sudo make all install' (without quotes) and press 'Enter'.

Step 6 : Open ~/.bashrc file and give GLEW library path to LD_LIBRARY_PATH environmental variable like follows:

		a) Use your favourite editor to open above file with Super User privileges.
		   e.g. $sudo pico ~/.bashrc
		
		b) Go to the bottom of above file and add follwoing cmd to it. (If you find this file as empty, 
		   then you have made some mistake while opening this file, please check cmd given in Step "6.a)" again.)
		   Add cmd: export LD_LIBRARY_PATH=/usr/lib64:$LD_LIBRARY_PATH

		c) Assuming you are using 'pico' editor, press 'Ctrl + o' and then 'Enter' to save and press 'Ctrl + x'
		   and then 'Enter' to exit. Or, use your favourite editor's 'save' and 'exit' commands respectively.

Step 7 : Open your source code. In that, include GLEW's header file as: #include <GL/glew32.h>

Step 8 : When finished with writing the code, you will use the 'g++' command taught in 1st and 2nd seminar. But this time,
	 append/add '-lGLEW' to it. So your final cmd will be:
	 g++ -o oglpp oglpp.cpp -lX11 -lGL -lGLEW

Step 9 : Execute: ./oglpp