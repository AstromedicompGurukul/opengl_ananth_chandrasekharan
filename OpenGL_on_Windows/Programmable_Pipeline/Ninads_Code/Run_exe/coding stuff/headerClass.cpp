#include"zmath.h"
#include<vector>
#include<Windows.h>
#include"headerClass.h"
#include<gl/glew.h>

using namespace zmath;
using namespace std;

//shader
shaderLoader::shaderLoader(char const* vShaderFileName, char const* fShaderFileName)
{

	VertexShaderObject = loadThisShader(vShaderFileName, GL_VERTEX_SHADER);
	FragmentShaderObject = loadThisShader(fShaderFileName, GL_FRAGMENT_SHADER);
	loadThisProgram();
}
 GLchar* shaderLoader::load_ShaderFile(char const* path)
{
	 char* buffer = 0;
	long length;
	FILE *f;
	fopen_s(&f, path, "rb");

	if (f)
	{
		fseek(f, 0, SEEK_END);
		length = ftell(f);
		//rewind(f);
		fseek(f, 0, SEEK_SET);
		buffer = (char*)malloc((length + 1) * sizeof(char));//+1 null
		if (buffer)
		{
			fread(buffer, sizeof(char), length, f);
		}
		fclose(f);
	}
	buffer[length] = '\0';
	// for (int i = 0; i < length; i++) {
	//    printf("buffer[%d] == %c\n", i, buffer[i]);
	// }
	//printf("buffer = %s\n", buffer);

	return buffer;
}
GLuint shaderLoader::loadThisShader(const char* filename, GLenum fileType)
{
	FILE* shaderLog;
	fopen_s(&shaderLog, "shaderLog.txt", "w");

	GLuint shaderObj = glCreateShader(fileType);

	const GLchar *ShaderSourceCode = load_ShaderFile(filename);

	glShaderSource(shaderObj, 1, (const GLchar **)&ShaderSourceCode, NULL);
	glCompileShader(shaderObj);

	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;
	glGetShaderiv(shaderObj, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(shaderObj, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(shaderObj, iInfoLogLength, &written, szInfoLog);
				fprintf(shaderLog, "%s Compilation Log : %s\n", filename, szInfoLog);
				free(szInfoLog);
				cleanUp();
				exit(0);
			}
		}
	}
	if (shaderLog)
		fclose(shaderLog);
	return shaderObj;
	
}
void shaderLoader::loadThisProgram(void)
{
	FILE* shaderLog;
	fopen_s(&shaderLog, "shaderProgramLog.txt", "w");
	GLint iInfoLogLength = 0;
	char *szInfoLog = NULL;

	 ShaderProgramObject = glCreateProgram();
	glAttachShader(ShaderProgramObject, VertexShaderObject);
	glAttachShader(ShaderProgramObject, FragmentShaderObject);

	glBindAttribLocation(ShaderProgramObject, 0, "vPosition");
	glBindAttribLocation(ShaderProgramObject, 1, "vTex0Coord");
	glBindAttribLocation(ShaderProgramObject, 2, "vNormal");
	glBindAttribLocation(ShaderProgramObject, 3, "vTangent");

	glLinkProgram(ShaderProgramObject);
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(ShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(ShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(ShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(shaderLog, "Shader Program Link Log : %s\n", szInfoLog);
				free(szInfoLog);
				cleanUp();
				exit(0);
			}
		}
	}
	getAllUniformLocations();
	if (shaderLog)
		fclose(shaderLog);
}
void shaderLoader::getAllUniformLocations(void)
{
	// UNIFORMS

	//MVP matrices
	ModelxUniform = glGetUniformLocation(ShaderProgramObject , "uModelX");
	ViewxUniform = glGetUniformLocation(ShaderProgramObject, "uViewX");
	ProjxUniform = glGetUniformLocation(ShaderProgramObject, "uProjX");
//lights
	LightColorUniform = glGetUniformLocation(ShaderProgramObject, "uLightColor");
	LightPosUniform = glGetUniformLocation(ShaderProgramObject, "uLightPos");
	//KdUniform = glGetUniformLocation(ShaderProgramObject, "uKd");
//specular
	ShineDamperUniform = glGetUniformLocation(ShaderProgramObject, "uShineDamper");
	ReflectivityUniform = glGetUniformLocation(ShaderProgramObject, "uReflectivity");

	//sampler texture
	SamplerUniform = glGetUniformLocation(ShaderProgramObject, "tSampler");
	NormSamplerUniform = glGetUniformLocation(ShaderProgramObject, "normSampler");

	cubeMapSamplerUniform = glGetUniformLocation(ShaderProgramObject, "cubeMapTexture");
	//other
	LKeyPressedUniform = glGetUniformLocation(ShaderProgramObject, "Lkey");

	camPosUniform= glGetUniformLocation(ShaderProgramObject, "cameraPos");

}

//matrices
void shaderLoader::loadModelMatrix(GLfloat* modelMatrix)
{
	glUniformMatrix4fv(ModelxUniform, 1, GL_FALSE, modelMatrix);
}
void shaderLoader::loadViewMatrix(Camera cam)
{
	float viewMatrix[16];
	glUniform3f(camPosUniform, cam.getPosition()[0], cam.getPosition()[1], cam.getPosition()[2]);
	createViewMatrix(viewMatrix, cam);
	glUniformMatrix4fv(ViewxUniform, 1, GL_FALSE, viewMatrix);
	
}
void shaderLoader::loadProjectionMatrix(GLfloat* ProjectionMatrix)
{
	glUniformMatrix4fv(ProjxUniform, 1, GL_FALSE, ProjectionMatrix);
}

void shaderLoader::loadSkyViewMatrix(Camera cam)//update later and delete createSky method
{
	float viewMatrix[16];

	createSkyViewMatrix(viewMatrix, cam);
	glUniformMatrix4fv(ViewxUniform, 1, GL_FALSE, viewMatrix);

}
//lights
void shaderLoader::loadLightColor(GLfloat r, GLfloat g, GLfloat b)
{
	glUniform3f(LightColorUniform, r, g, b);
}
void shaderLoader::loadLightPosition(GLfloat* lightPosition)
{
	glUniform4fv(LightPosUniform, 1, lightPosition);
}
void shaderLoader::isLkeyPressed(bool gbLight)
{
	if (gbLight == true)
		glUniform1i(LKeyPressedUniform, 1);
	else
		glUniform1i(LKeyPressedUniform, 0);
}
void shaderLoader::loadShineDamperReflectivity(GLfloat damper, GLfloat reflectivity)
{
	glUniform1f(ShineDamperUniform, damper);
	glUniform1f(ReflectivityUniform,reflectivity);
}

//texture
void shaderLoader::loadModelTextureSampler(GLuint textureModel)
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textureModel);
	glUniform1i(SamplerUniform, 0);
}
void shaderLoader::loadModelNormalSampler(GLuint normalTextureModel)
{
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, normalTextureModel);
	glUniform1i(NormSamplerUniform, 1);
}
void shaderLoader::loadCubeMapSampler(GLuint CubeMapSampler)
{
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_CUBE_MAP, CubeMapSampler);
	glUniform1i(cubeMapSamplerUniform, 2);
}
void shaderLoader::cleanUp(void)
{
	glDetachShader(ShaderProgramObject, VertexShaderObject);
	glDetachShader(ShaderProgramObject, FragmentShaderObject);

	glDeleteShader(VertexShaderObject);
	VertexShaderObject = 0;
	glDeleteShader(FragmentShaderObject);
	FragmentShaderObject = 0;

	glDeleteProgram(ShaderProgramObject);
	ShaderProgramObject = 0;

	glUseProgram(0);
}



void Camera::moveDirection(float speed,WPARAM direction)
{
	if (direction == VK_DOWN)
		position[2] += speed;

	if (direction == VK_UP)
		position[2] -= speed;

	if (direction == VK_LEFT)
		position[0] -= speed;

	if (direction == VK_RIGHT)
		position[0] += speed;
}
void Camera::moveRotation(float anglePitch,float angleYaw)
{
	pitch = anglePitch;
	yaw = angleYaw;
}

