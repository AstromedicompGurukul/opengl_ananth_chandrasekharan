#pragma once
#include<Windows.h>
#include<gl/GL.h>

const GLfloat TexCoords_Case1[] =
{
	0.5f, 0.5f,
	0.0f, 0.5f,
	0.0f, 0.0f,
	0.5f, 0.0f
};

const GLfloat TexCoords_Case2[] =
{
	1.0f, 1.0f,
	0.0f, 1.0f,
	0.0f, 0.0f,
	1.0f, 0.0f
};

const GLfloat TexCoords_Case3[] =
{
	2.0f, 2.0f,
	0.0f, 2.0f,
	0.0f, 0.0f,
	2.0f, 0.0f
};

const GLfloat TexCoords_Case4[] =
{
	0.5f, 0.5f,
	0.5f, 0.5f,
	0.5f, 0.5f,
	0.5f, 0.5f
};