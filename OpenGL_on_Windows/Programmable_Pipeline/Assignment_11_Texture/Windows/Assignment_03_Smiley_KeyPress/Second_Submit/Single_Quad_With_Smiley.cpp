#include<Windows.h>
#include<stdio.h>

#include<gl\glew.h>

#include<gl/GL.h>

#include "vmath.h"
#include "Texture.h"

#include "TextureArrays.h"

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600
using namespace vmath;

enum
{
	AC_ATTRIBUTE_VERTEX = 0,
	AC_ATTRIBUTE_COLOR,
	AC_ATTRIBUTE_NORMAL,
	AC_ATTRIBUTE_TEXTURE0,
	AC_ATTRIBUTE_TEXTURE1
};

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
FILE *gpFile = NULL;

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullScreen = false;
bool bDone = false;

GLuint gVertexShaderObject = 0;
GLuint gFragmentShaderObject = 0;
GLuint gShaderProgramObject = 0;

GLuint gVao_Quad1;
GLuint gVbo_Quad_Position;
GLuint gVbo_Quad_Texture1;
GLuint gVbo_Quad_Texture2;

GLuint gMVPUniform;
GLuint gTexture_sampler_uniform;
GLuint gTexture_Smiley;

GLint keypressed = -1;

mat4 gPerspectiveProjectionMatrix; //Change 1

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void initialize(void);
	void update(void);
	void display(void);
	void uninitialize(void);

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;

	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log.txt cannot be created"), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log.txt is sucessfully opened\n");
	}

	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = TEXT("Shree Ganesha 3D-Rotation");
	wndclass.lpszMenuName = NULL;
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW, TEXT("Shree Ganesha 3D-Rotation"), TEXT("Shree Ganesha : 3D - Rotation"), WS_OVERLAPPEDWINDOW, 100, 100, WIN_WIDTH, WIN_HEIGHT, NULL, NULL, hInstance, NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	initialize();

	while (bDone == false)
	{
		if (PeekMessage(&msg, hwnd, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			update();
			display();

			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
				{
					bDone = true;
				}
			}
		}
	}

	uninitialize();
	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void resize(int width, int height);
	void ToggleFullScreen(void);
	void uninitialize(void);
	int LoadGLTexture(GLuint *texture, TCHAR imageResourceID[]);
	void Load_White_Texture(void);

	static WORD xMouse = NULL;
	static WORD yMouse = NULL;

	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
		{
			gbActiveWindow = true;
		}
		else
		{
			gbActiveWindow = false;
		}
		break;

	case WM_ERASEBKGND:
		return 0;
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			if (gbEscapeKeyIsPressed == false)
			{
				gbEscapeKeyIsPressed = true;
			}
			break;

		case 0x46:
			if (gbFullScreen == false)
			{
				ToggleFullScreen();
				gbFullScreen = true;
			}
			else
			{
				ToggleFullScreen();
				gbFullScreen = false;
			}
		break;

		case 0x30:
			keypressed = 0;
			glDeleteTextures(1, &gTexture_Smiley);
			Load_White_Texture();
			glBindTexture(GL_TEXTURE_2D, texname);
			glEnable(GL_TEXTURE_2D);
			break;

		case 0x31:
			keypressed = 1;
			LoadGLTexture(&gTexture_Smiley, MAKEINTRESOURCE(IDBITMAP_SMILEY));
			glBindTexture(GL_TEXTURE_2D, gTexture_Smiley);
			glEnable(GL_TEXTURE_2D);
			break;

		case 0x32:
			keypressed = 2;
			LoadGLTexture(&gTexture_Smiley, MAKEINTRESOURCE(IDBITMAP_SMILEY));
			glBindTexture(GL_TEXTURE_2D, gTexture_Smiley);
			glEnable(GL_TEXTURE_2D);
			break;

		case 0x33:
			keypressed = 3;
			LoadGLTexture(&gTexture_Smiley, MAKEINTRESOURCE(IDBITMAP_SMILEY));
			glBindTexture(GL_TEXTURE_2D, gTexture_Smiley);
			glEnable(GL_TEXTURE_2D);
			break;

		case 0x34:
			keypressed = 4;
			LoadGLTexture(&gTexture_Smiley, MAKEINTRESOURCE(IDBITMAP_SMILEY));
			glBindTexture(GL_TEXTURE_2D, gTexture_Smiley);
			glEnable(GL_TEXTURE_2D);
			break;

		default:
			break;
		}
		break;

	case WM_LBUTTONDOWN:
		break;

	case WM_CLOSE:
		uninitialize();
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}

	return DefWindowProc(hwnd, iMsg, wParam, lParam);
}

void initialize(void)
{
	void uninitialize(void);
	void resize(int width, int height);
	int LoadGLTexture(GLuint *texture, TCHAR imageResourceID[]);
	void Load_White_Texture(void);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	memset(&pfd, 0, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	const GLchar *vertexShaderSourceCode =
		"#version 440 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec2 vTexture0Coord;" \
		"out vec2 outTexture0Coord;" \
		"uniform mat4 u_mvp_matrix;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvp_matrix * vPosition;" \
		"outTexture0Coord=vTexture0Coord;" \
		"}";

	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

	glCompileShader(gVertexShaderObject);

	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength>0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}
	else
	{
		fprintf(gpFile, "Vertex Shader Compilation Log : No Errors\n");
	}

	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar *fragmentShaderSourceCode =
		"#version 440 core" \
		"\n" \
		"in vec2 outTexture0Coord;" \
		"out vec4 FragColor;" \
		"uniform sampler2D u_texture0_sampler;" \
		"void main(void)" \
		"{" \
		"FragColor = texture(u_texture0_sampler, outTexture0Coord);" \
		"}";

	glShaderSource(gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

	glCompileShader(gFragmentShaderObject);

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength>0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				szInfoLog = NULL;
				uninitialize();
				exit(0);
			}
		}
	}
	else
	{
		fprintf(gpFile, "Fragment Shader Compilation Log : No Errors\n");
	}

	gShaderProgramObject = glCreateProgram();
	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	glBindAttribLocation(gShaderProgramObject, AC_ATTRIBUTE_VERTEX, "vPosition");
	glBindAttribLocation(gShaderProgramObject, AC_ATTRIBUTE_TEXTURE0, "vTexture0Coord");

	glLinkProgram(gShaderProgramObject);

	GLint iShaderLinkProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderLinkProgramLinkStatus);
	if (iShaderLinkProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength>0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader program Link Log : %s\n", szInfoLog);
				free(szInfoLog);
				szInfoLog = NULL;
				uninitialize();
				exit(0);
			}
		}
	}
	else
	{
		fprintf(gpFile, "Shader program Link Log : No Errors\n");
	}

	gMVPUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");
	gTexture_sampler_uniform = glGetUniformLocation(gShaderProgramObject, "u_texture0_sampler");

	const GLfloat QuadVertices[] =
	{
		1.0f, 1.0f, 0.0f,
		-1.0f, 1.0f, 0.0f,
		-1.0f, -1.0f,0.0f,
		1.0f, -1.0f, 0.0f
	};
	
	//-------------
	glGenVertexArrays(1, &gVao_Quad1);
	glBindVertexArray(gVao_Quad1);

	//Vertices for Quad
	glGenBuffers(1, &gVbo_Quad_Position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Quad_Position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(QuadVertices), QuadVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AC_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AC_ATTRIBUTE_VERTEX);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Texture for Quad
	glGenBuffers(1, &gVbo_Quad_Texture1);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Quad_Texture1);
	glBufferData(GL_ARRAY_BUFFER, 8*sizeof(GLfloat), NULL, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AC_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AC_ATTRIBUTE_TEXTURE0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Texture for Quad
	glGenBuffers(1, &gVbo_Quad_Texture2);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Quad_Texture2);
	glBufferData(GL_ARRAY_BUFFER, 8 * sizeof(GLfloat), NULL, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AC_ATTRIBUTE_TEXTURE1, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AC_ATTRIBUTE_TEXTURE1);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glBindVertexArray(0);
	//-------------

	/*
	glGenVertexArrays(1, &gVao_Quad2);
	glBindVertexArray(gVao_Quad2);

	//Vertices for Quad
	glGenBuffers(1, &gVbo_Quad_Position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Quad_Position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(QuadVertices), QuadVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AC_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AC_ATTRIBUTE_VERTEX);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Texture for Quad
	glGenBuffers(1, &gVbo_Quad_Texture2);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Quad_Texture2);
	glBufferData(GL_ARRAY_BUFFER, sizeof(TexCoords_Case2), TexCoords_Case2, GL_STATIC_DRAW);
	glVertexAttribPointer(AC_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AC_ATTRIBUTE_TEXTURE0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
	
	//-------------

	glGenVertexArrays(1, &gVao_Quad3);
	glBindVertexArray(gVao_Quad3);

	//Vertices for Quad
	glGenBuffers(1, &gVbo_Quad_Position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Quad_Position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(QuadVertices), QuadVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AC_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AC_ATTRIBUTE_VERTEX);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Texture for Quad
	glGenBuffers(1, &gVbo_Quad_Texture3);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Quad_Texture3);
	glBufferData(GL_ARRAY_BUFFER, sizeof(TexCoords_Case3), TexCoords_Case3, GL_STATIC_DRAW);
	glVertexAttribPointer(AC_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AC_ATTRIBUTE_TEXTURE0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//-------------

	glGenVertexArrays(1, &gVao_Quad4);
	glBindVertexArray(gVao_Quad4);

	//Vertices for Quad
	glGenBuffers(1, &gVbo_Quad_Position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Quad_Position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(QuadVertices), QuadVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AC_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AC_ATTRIBUTE_VERTEX);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Texture for Quad
	glGenBuffers(1, &gVbo_Quad_Texture4);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Quad_Texture4);
	glBufferData(GL_ARRAY_BUFFER, sizeof(TexCoords_Case4), TexCoords_Case4, GL_STATIC_DRAW);
	glVertexAttribPointer(AC_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AC_ATTRIBUTE_TEXTURE0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//-------------
	*/

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_CULL_FACE);

	/*
	LoadGLTexture(&gTexture_Smiley, MAKEINTRESOURCE(IDBITMAP_SMILEY));
	glEnable(GL_TEXTURE_2D);
	*/

	Load_White_Texture();
	glEnable(GL_TEXTURE_2D);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	gPerspectiveProjectionMatrix = mat4::identity(); //Change 2

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void Load_White_Texture(void)
{
	void MakeCheckImage(void);

	MakeCheckImage();

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	glGenTextures(1, &texname);

	glBindTexture(GL_TEXTURE_2D, texname);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, CHECK_IMAGE_WIDTH, CHECK_IMAGE_HEIGHT, 0, GL_RGBA, GL_UNSIGNED_BYTE, checkImage);

	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
}

void MakeCheckImage(void)
{
	int i, j;
	int c;

	for (i = 0; i<CHECK_IMAGE_HEIGHT; i++)
	{
		for (j = 0; j<CHECK_IMAGE_WIDTH; j++)
		{
			//c = ( ((i&0x8)==0) ^ ((j&0x8)==0))*255;
			c = 0xff;
			checkImage[i][j][0] = (GLubyte)c;
			checkImage[i][j][1] = (GLubyte)c;
			checkImage[i][j][2] = (GLubyte)c;
			checkImage[i][j][3] = (GLubyte)255;
		}
	}
}

int LoadGLTexture(GLuint *texture, TCHAR imageResourceID[])
{
	HBITMAP hBitmap;
	BITMAP bmp;
	int iStatus = 0;

	glGenTextures(1, texture);
	hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), imageResourceID, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
	if (hBitmap)
	{
		iStatus = TRUE;
		GetObject(hBitmap, sizeof(bmp), &bmp);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		glBindTexture(GL_TEXTURE_2D, *texture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexImage2D(GL_TEXTURE_2D,
			0,
			GL_RGB,
			bmp.bmWidth,
			bmp.bmHeight,
			0,
			GL_BGR,
			GL_UNSIGNED_BYTE,
			bmp.bmBits
		);

		glGenerateMipmap(GL_TEXTURE_2D);
		DeleteObject(hBitmap);
	}
	return iStatus;

}

void update(void)
{
	
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	glUseProgram(gShaderProgramObject);

	mat4 modelViewMatrix = mat4::identity();
	mat4 modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	modelViewProjectionMatrix = gPerspectiveProjectionMatrix*modelViewMatrix;
	glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glBindVertexArray(gVao_Quad1);

	if (keypressed == 1)
	{
		glBindBuffer(GL_ARRAY_BUFFER, gVbo_Quad_Texture1);
		glBufferData(GL_ARRAY_BUFFER, sizeof(TexCoords_Case1), TexCoords_Case1, GL_DYNAMIC_DRAW);
		glBindTexture(GL_TEXTURE_2D, gTexture_Smiley);
		glUniform1i(gTexture_sampler_uniform, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	else if (keypressed == 2)
	{
		glBindBuffer(GL_ARRAY_BUFFER, gVbo_Quad_Texture1);
		glBufferData(GL_ARRAY_BUFFER, sizeof(TexCoords_Case2), TexCoords_Case2, GL_DYNAMIC_DRAW);
		glBindTexture(GL_TEXTURE_2D, gTexture_Smiley);
		glUniform1i(gTexture_sampler_uniform, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	else if (keypressed == 3)
	{
		glBindBuffer(GL_ARRAY_BUFFER, gVbo_Quad_Texture1);
		glBufferData(GL_ARRAY_BUFFER, sizeof(TexCoords_Case3), TexCoords_Case3, GL_DYNAMIC_DRAW);
		glBindTexture(GL_TEXTURE_2D, gTexture_Smiley);
		glUniform1i(gTexture_sampler_uniform, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	else if (keypressed == 4)
	{
		glBindBuffer(GL_ARRAY_BUFFER, gVbo_Quad_Texture1);
		glBufferData(GL_ARRAY_BUFFER, sizeof(TexCoords_Case4), TexCoords_Case4, GL_DYNAMIC_DRAW); glBindTexture(GL_TEXTURE_2D, gTexture_Smiley);
		glUniform1i(gTexture_sampler_uniform, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	else
	{
		glBindBuffer(GL_ARRAY_BUFFER, gVbo_Quad_Texture2);
		glBufferData(GL_ARRAY_BUFFER, sizeof(TexCoords_Case2), TexCoords_Case2, GL_DYNAMIC_DRAW);
		glBindTexture(GL_TEXTURE_2D, texname);
		glUniform1i(gTexture_sampler_uniform, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	//glActiveTexture(GL_TEXTURE0);

	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);
	//----------------

	glUseProgram(0);

	SwapBuffers(ghdc);
}

void uninitialize(void)
{
	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	if (gVao_Quad1)
	{
		glDeleteVertexArrays(1, &gVao_Quad1);
		gVao_Quad1 = 0;
	}

	if (gVbo_Quad_Position)
	{
		glDeleteBuffers(1, &gVbo_Quad_Position);
		gVbo_Quad_Position = 0;
	}

	if (gVbo_Quad_Texture1)
	{
		glDeleteBuffers(1, &gVbo_Quad_Texture1);
		gVbo_Quad_Texture1 = 0;
	}

	if (gVbo_Quad_Texture2)
	{
		glDeleteBuffers(1, &gVbo_Quad_Texture2);
		gVbo_Quad_Texture2 = 0;
	}

	if (gTexture_Smiley)
	{
		glDeleteBuffers(1, &gTexture_Smiley);
		gTexture_Smiley = 0;
	}

	if (texname)
	{
		glDeleteBuffers(1, &texname);
		texname = 0;
	}

	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;

	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	glUseProgram(0);

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	if (gpFile)
	{
		fprintf(gpFile, "Log.txt is sucessfully closed\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	gPerspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void ToggleFullScreen(void)
{
	MONITORINFO mi;

	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle && WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle&~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
}
