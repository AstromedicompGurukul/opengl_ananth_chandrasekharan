#pragma once
#include<Windows.h>
#include<gl/GL.h>

#define CHECK_IMAGE_WIDTH 64
#define CHECK_IMAGE_HEIGHT 64

GLubyte checkImage[CHECK_IMAGE_WIDTH][CHECK_IMAGE_HEIGHT][4];
GLuint texname;

const GLfloat TexCoords_Case1[] =
{
	0.5f, 0.5f,
	0.0f, 0.5f,
	0.0f, 0.0f,
	0.5f, 0.0f
};

const GLfloat TexCoords_Case2[] =
{
	1.0f, 1.0f,
	0.0f, 1.0f,
	0.0f, 0.0f,
	1.0f, 0.0f
};

const GLfloat TexCoords_Case3[] =
{
	2.0f, 2.0f,
	0.0f, 2.0f,
	0.0f, 0.0f,
	2.0f, 0.0f
};

const GLfloat TexCoords_Case4[] =
{
	0.5f, 0.5f,
	0.5f, 0.5f,
	0.5f, 0.5f,
	0.5f, 0.5f
};