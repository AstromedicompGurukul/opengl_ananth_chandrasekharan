#pragma once

#include<windows.h>
#include<gl/GL.h>

#define CHECK_IMAGE_WIDTH 64
#define CHECK_IMAGE_HEIGHT 64

const GLfloat QuadVertices[] =
{
	0.0f, 1.0f, 0.0f,
	-2.0f, 1.0f, 0.0f,
	-2.0f, -1.0f,0.0f,
	0.0f, -1.0f, 0.0f
};

const GLfloat QuadVertices1[] =
{
	2.4142f, 1.0f, -1.41421f,
	1.0f, 1.0f, 0.0f,
	1.0f, -1.0f, 0.0f,
	2.4142f, -1.0f, -1.41421f
};

const GLfloat TexCoords[] =
{
	1.0f, 1.0f,
	0.0f, 1.0f,
	0.0f, 0.0f,
	1.0f, 0.0f
};

GLubyte checkImage[CHECK_IMAGE_WIDTH][CHECK_IMAGE_HEIGHT][4];
GLuint texname;
