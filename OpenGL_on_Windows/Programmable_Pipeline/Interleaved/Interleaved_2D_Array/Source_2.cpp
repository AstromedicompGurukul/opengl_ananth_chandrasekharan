/*
Replaced 1D Array by 2D Array

const GLfloat cubeVertices[][11] =
{
{1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f,0.0f,1.0f, 0.0f,0.0f},
{-1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f,0.0f,1.0f, 1.0f,0.0f},
{-1.0f, -1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f,0.0f,1.0f, 1.0f,1.0f},
{1.0f, -1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f,0.0f,1.0f, 0.0f,1.0f},
{1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 1.0f,0.0f,0.0f, 1.0f,0.0f},
{1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f,0.0f,0.0f, 1.0f,1.0f},
{1.0f, -1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f,0.0f,0.0f, 0.0f,1.0f},
{1.0f, -1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 1.0f,0.0f,0.0f, 0.0f,0.0f},
{-1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f,0.0f,-1.0f, 1.0f,0.0f},
{1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f,0.0f,-1.0f, 1.0f,1.0f},
{1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f,0.0f,-1.0f, 0.0f,1.0f},
{-1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f,0.0f,-1.0f, 0.0f,0.0f},
{-1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, -1.0f,0.0f,0.0f, 0.0f,0.0f},
{-1.0f, 1.0f, -1.0f, 1.0f, 1.0f, 0.0f, -1.0f,0.0f,0.0f, 1.0f,0.0f},
{-1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 0.0f, -1.0f,0.0f,0.0f, 1.0f,1.0f},
{-1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 0.0f, -1.0f,0.0f,0.0f, 0.0f,1.0f},
{-1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f,1.0f,0.0f, 0.0f,1.0f},
{1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f,1.0f,0.0f, 0.0f,0.0f},
{1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 1.0f, 0.0f,1.0f,0.0f, 1.0f,0.0f},
{-1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 1.0f, 0.0f,1.0f,0.0f, 1.0f,1.0f},
{1.0f, -1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f,-1.0f,0.0f, 1.0f,1.0f},
{-1.0f, -1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f,-1.0f,0.0f, 0.0f,1.0f},
{-1.0f, -1.0f, -1.0f, 1.0f, 0.0f, 1.0f, 0.0f,-1.0f,0.0f, 0.0f,0.0f},
{1.0f, -1.0f, -1.0f,	 1.0f, 0.0f, 1.0f, 0.0f,-1.0f,0.0f, 1.0f,0.0f}
};


*/

#include <Windows.h>
#include <gl\glew.h>
#include <gl/GL.h>
#include <stdio.h> 
#include "vmath.h"
#include "RCheader.h"

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")

#define BUFFER_SIZE 256

#define WIN_WIDTH 800
#define WIN_HEIGHT 600
#define BUFFER_SIZE 256	

enum
{
	AC_ATTRIBUTE_VERTEX = 0,
	AC_ATTRIBUTE_COLOR,
	AC_ATTRIBUTE_NORMAL,
	AC_ATTRIBUTE_TEXTURE0,
};

using namespace vmath;

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullScreen = false;

GLuint ac_gVertexShaderObject;
GLuint ac_gFragmentShaderObject;
GLuint ac_gShaderProgramObject;

FILE *gpFile = NULL;

FILE *gpExtensionData = NULL;

//Cube Variables
GLuint gVao_cube;
GLuint gVbo_cube;
GLuint gMVPUniform;
mat4 gPerspectiveProjectionMatrix;
GLuint gModelMatrixUniform, gViewMatrixUniform, gProjectionMatrixUniform;
static float angle_pyramid = 0.0f;
static float angle_cube = 0.0f;

//Texture
GLuint gTexture_smapler_uniform;
GLuint gTexture_Marble;

//Light Variables
GLuint La_uniform;
GLuint Ld_Uniform;
GLuint Ls_Uniform;
GLuint Ka_Uniform;
GLuint Kd_Uniform;
GLuint Ks_Uniform;
GLuint Material_shininess_uniform;
GLuint gLightPositionUniform;
GLuint gLKeyPressedUniform;
bool bIsLkeyPressed;
bool gbLight;
GLfloat lightAmbient[] = { 0.25f,0.25f,0.25f,1.0f };
GLfloat lightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightPosition[] = { 2.0f,1.0f,1.0f,1.0f };
GLfloat material_Ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_Diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_Specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_shininess = 128.0f;


LRESULT CALLBACK AcCallBack(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void initialize(void);
	void uninitialize(void);
	void update(void);
	void display(void);
	void resize(int, int);

	WNDCLASSEX ac;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("Shree Ganesha : Interleaved using  1D Array");
	bool bDone = false;

	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can Not Be Created"), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Is Successfully Opened\n");
	}

	ac.cbSize = sizeof(WNDCLASSEX);
	ac.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	ac.cbClsExtra = 0;
	ac.cbWndExtra = 0;
	ac.hInstance = hInstance;
	ac.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	ac.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	ac.hCursor = LoadCursor(NULL, IDC_ARROW);
	ac.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	ac.lpfnWndProc = AcCallBack;
	ac.lpszClassName = szClassName;
	ac.lpszMenuName = NULL;

	RegisterClassEx(&ac);

	hwnd = CreateWindowEx(
		WS_EX_APPWINDOW,
		szClassName,
		TEXT("Shree Ganesha"),
		WS_OVERLAPPED | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);

	ghwnd = hwnd;

	initialize();

	ShowWindow(hwnd, SW_SHOWNORMAL);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
				{
					bDone = true;
				}
				update();
				display();
			}
		}
	}
	uninitialize();
	return ((int)msg.wParam);
}

void initialize(void)
{
	void resize(GLint width, GLint height);
	int LoadGLTextures(GLuint *texture, TCHAR imageResourceId[]);

	//OpenGL calls
	PIXELFORMATDESCRIPTOR pfd;
	GLint iPixelFormatIndex;

	//For getting extensions
	GLint num;

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cBlueBits = 8;
	pfd.cGreenBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	//Extensions supported by OpenGL
	if (fopen_s(&gpExtensionData, "Extensions.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Extensions.txt File Can Not Be Created"), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf_s(gpExtensionData, "Extension.txt File Is Successfully Opened\n");
	}

	fprintf_s(gpExtensionData, "This is Assignment to find extension supported by my graphic card\n");

	glGetIntegerv(GL_NUM_EXTENSIONS, &num);

	for (int i = 0; i<num; i++)
	{
		fprintf_s(gpExtensionData, "%d : %s\n", i + 1, (char*)glGetStringi(GL_EXTENSIONS, i));
	}

	fprintf_s(gpExtensionData, "Extension.txt File Is Successfully Closed\n");
	fclose(gpExtensionData);
	gpExtensionData = NULL;


	//------------------------------------------------------------------------------

	//Addding code for vertex and fragment shader here
	ac_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	/*
	const GLchar *vertexShaderSourceCode =
		"void main(void)" \
		"{" \
		"}";
	*/

	const GLchar *vertexShaderSourceCode =
		"#version 440 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec2 vTexture0_Coord;" \
		"in vec4 vColor;" \
		"in vec3 vNormal;" \
		"out vec4 out_color;"\
		"out vec2 out_texture0_coord;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform int u_LKeyPressed;" \
		"out vec3 transformed_normals;" \
		"out vec3 light_direction;"\
		"out vec3 viewer_vector;"\
		"uniform vec4 u_light_position;" \
		"void main (void)" \
		"{" \
		"if(u_LKeyPressed == 1)" \
		"{" \
		"vec4 eyeCoordinates =u_view_matrix * u_model_matrix * vPosition;" \
		"transformed_normals = mat3(u_view_matrix * u_model_matrix)*vNormal;" \
		"light_direction = vec3(u_light_position) - eyeCoordinates.xyz;" \
		"viewer_vector = -eyeCoordinates.xyz;" \
		"}" \
		"out_texture0_coord = vTexture0_Coord;" \
		"out_color = vColor;" \
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix *vPosition;" \
		"}";


	glShaderSource(ac_gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

	glCompileShader(ac_gVertexShaderObject);
	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char * szInfoLog = NULL;
	glGetShaderiv(ac_gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(ac_gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(ac_gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf_s(gpFile, "Vertex Shader Info Log: %s\n", szInfoLog);
				free(szInfoLog);
				exit(0);
			}
		}
	}

	ac_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	/*
	const GLchar *fragmentShaderSourceCode =
		"void main(void)" \
		"{" \
		"}";
	*/

	const GLchar * fragmentShaderSourceCode =
		"#version 440 core" \
		"\n" \
		"in vec2 out_texture0_coord;"
		"in vec3 transformed_normals;"\
		"in vec3 light_direction;"\
		"in vec3 viewer_vector;"\
		"uniform vec3 u_La;" \
		"uniform vec3 u_Ld;" \
		"uniform vec3 u_Ls;" \
		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Kd;" \
		"uniform vec3 u_Ks;" \
		"uniform float u_material_shininess;" \
		"in vec4 out_color;"
		"uniform int u_LKeyPressed;"\
		"uniform sampler2D u_texture0_sampler;" \
		"out vec4 FragColor;" \
		"void main (void)" \
		"{" \
		"vec4 color;" \
		"vec4 phong_ads_color;" \
		"if(u_LKeyPressed == 1)" \
		"{" \
		"vec3 normalized_transformed_normals = normalize(transformed_normals);"\
		"vec3 normalized_light_direction = normalize(light_direction);"\
		"vec3 normalized_viewer_vector = normalize(viewer_vector);"\
		"vec3 ambient = u_La * u_Ka;"\
		"float tn_dot_ld = max(dot(normalized_transformed_normals , normalized_light_direction),0.0);"\
		"vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;"\
		"vec3 reflection_vector = reflect(-normalized_light_direction,normalized_transformed_normals);"\
		"vec3 specular = u_Ls * u_Ks *pow(max(dot(reflection_vector,normalized_viewer_vector),0.0),u_material_shininess);"\
		"phong_ads_color = vec4(ambient + diffuse + specular,1.0);"\
		"color = phong_ads_color * texture(u_texture0_sampler,out_texture0_coord) * out_color;" \
		"}" \
		"else" \
		"{" \
		"color = texture(u_texture0_sampler,out_texture0_coord) + out_color;" \
		"}" \
		"FragColor = color;" \
		"}";

	glShaderSource(ac_gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	glCompileShader(ac_gFragmentShaderObject);

	glShaderSource(ac_gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
	glCompileShader(ac_gFragmentShaderObject);
	glGetShaderiv(ac_gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(ac_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(ac_gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf_s(gpFile, "Fragment Shader Info Log: %s\n", szInfoLog);
				free(szInfoLog);
				exit(0);
			}
		}
	}

	//Main Shader Program starts here
	ac_gShaderProgramObject = glCreateProgram();
	glAttachShader(ac_gShaderProgramObject, ac_gVertexShaderObject);
	glAttachShader(ac_gShaderProgramObject, ac_gFragmentShaderObject);

	//bind in variables before linking
	glBindAttribLocation(ac_gShaderProgramObject, AC_ATTRIBUTE_VERTEX, "vPosition");
	glBindAttribLocation(ac_gShaderProgramObject, AC_ATTRIBUTE_COLOR, "vColor");
	glBindAttribLocation(ac_gShaderProgramObject, AC_ATTRIBUTE_NORMAL, "vNormal");
	glBindAttribLocation(ac_gShaderProgramObject, AC_ATTRIBUTE_TEXTURE0, "vTexture0_Coord");

	glLinkProgram(ac_gShaderProgramObject);
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(ac_gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(ac_gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(ac_gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf_s(gpFile, "Program Link Log: %s \n", szInfoLog);
				free(szInfoLog);
				exit(0);
			}
		}
	}

	//get uniform's address
	gModelMatrixUniform = glGetUniformLocation(ac_gShaderProgramObject, "u_model_matrix");
	gViewMatrixUniform = glGetUniformLocation(ac_gShaderProgramObject, "u_view_matrix");
	gProjectionMatrixUniform = glGetUniformLocation(ac_gShaderProgramObject, "u_projection_matrix");

	//Light Uniform
	Ka_Uniform = glGetUniformLocation(ac_gShaderProgramObject, "u_Ka");
	Kd_Uniform = glGetUniformLocation(ac_gShaderProgramObject, "u_Kd");
	Ks_Uniform = glGetUniformLocation(ac_gShaderProgramObject, "u_Ks");
	Material_shininess_uniform = glGetUniformLocation(ac_gShaderProgramObject, "u_material_shininess");
	gLightPositionUniform = glGetUniformLocation(ac_gShaderProgramObject, "u_light_position");
	Ld_Uniform = glGetUniformLocation(ac_gShaderProgramObject, "u_Ld");
	La_uniform = glGetUniformLocation(ac_gShaderProgramObject, "u_La");
	La_uniform = glGetUniformLocation(ac_gShaderProgramObject, "u_Ls");
	gLKeyPressedUniform = glGetUniformLocation(ac_gShaderProgramObject, "u_LKeyPressed");

	//Texture Uniform
	gTexture_smapler_uniform = glGetUniformLocation(ac_gShaderProgramObject, "u_texture0_sampler");
	//------------------------------------------------------------------------------

	//cube vertices
	const GLfloat cubeVertices[][11] =
	{
		{ 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f,0.0f,1.0f, 0.0f,0.0f },
		{ -1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f,0.0f,1.0f, 1.0f,0.0f },
		{ -1.0f, -1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f,0.0f,1.0f, 1.0f,1.0f },
		{ 1.0f, -1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f,0.0f,1.0f, 0.0f,1.0f },
		{ 1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 1.0f,0.0f,0.0f, 1.0f,0.0f },
		{ 1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f,0.0f,0.0f, 1.0f,1.0f },
		{ 1.0f, -1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f,0.0f,0.0f, 0.0f,1.0f },
		{ 1.0f, -1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 1.0f,0.0f,0.0f, 0.0f,0.0f },
		{ -1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f,0.0f,-1.0f, 1.0f,0.0f },
		{ 1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f,0.0f,-1.0f, 1.0f,1.0f },
		{ 1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f,0.0f,-1.0f, 0.0f,1.0f },
		{ -1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f,0.0f,-1.0f, 0.0f,0.0f },
		{ -1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, -1.0f,0.0f,0.0f, 0.0f,0.0f },
		{ -1.0f, 1.0f, -1.0f, 1.0f, 1.0f, 0.0f, -1.0f,0.0f,0.0f, 1.0f,0.0f },
		{ -1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 0.0f, -1.0f,0.0f,0.0f, 1.0f,1.0f },
		{ -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 0.0f, -1.0f,0.0f,0.0f, 0.0f,1.0f },
		{ -1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f,1.0f,0.0f, 0.0f,1.0f },
		{ 1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f,1.0f,0.0f, 0.0f,0.0f },
		{ 1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 1.0f, 0.0f,1.0f,0.0f, 1.0f,0.0f },
		{ -1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 1.0f, 0.0f,1.0f,0.0f, 1.0f,1.0f },
		{ 1.0f, -1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f,-1.0f,0.0f, 1.0f,1.0f },
		{ -1.0f, -1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f,-1.0f,0.0f, 0.0f,1.0f },
		{ -1.0f, -1.0f, -1.0f, 1.0f, 0.0f, 1.0f, 0.0f,-1.0f,0.0f, 0.0f,0.0f },
		{ 1.0f, -1.0f, -1.0f,	 1.0f, 0.0f, 1.0f, 0.0f,-1.0f,0.0f, 1.0f,0.0f }
	};


	glGenVertexArrays(1, &gVao_cube);
	
	glBindVertexArray(gVao_cube);

	glGenBuffers(1, &gVbo_cube);

	glBindBuffer(GL_ARRAY_BUFFER, gVbo_cube);
	glBufferData(GL_ARRAY_BUFFER, 24 * 11 * sizeof(float), cubeVertices, GL_STATIC_DRAW);
	
	//vertex
	glVertexAttribPointer(AC_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(float), (void *)0);
	glEnableVertexAttribArray(AC_ATTRIBUTE_VERTEX);
	
	//color
	glVertexAttribPointer(AC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(float), (void *)(3 * sizeof(float)));
	glEnableVertexAttribArray(AC_ATTRIBUTE_COLOR);
	
	//normal
	glVertexAttribPointer(AC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(float), (void *)(6 * sizeof(float)));
	glEnableVertexAttribArray(AC_ATTRIBUTE_NORMAL);
	
	//tex coords
	glVertexAttribPointer(AC_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 11 * sizeof(float), (void *)(9 * sizeof(float)));
	glEnableVertexAttribArray(AC_ATTRIBUTE_TEXTURE0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	//glEnable(GL_CULL_FACE);

	LoadGLTextures(&gTexture_Marble, MAKEINTRESOURCE(IDB_MARBLE_BMP));

	glEnable(GL_TEXTURE_2D);

	//Defined Background color
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	gPerspectiveProjectionMatrix = mat4::identity();
	resize(WIN_WIDTH, WIN_HEIGHT);
}

int LoadGLTextures(GLuint *texture, TCHAR imageResourceId[])
{
	HBITMAP hBitmap;
	BITMAP bmp;
	int iStatus = FALSE;
	HRESULT hr;
	hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL),
		imageResourceId,
		IMAGE_BITMAP,
		0,
		0,
		LR_CREATEDIBSECTION);

	hr = HRESULT_FROM_WIN32(GetLastError());


	if (hBitmap)
	{

		iStatus = TRUE;
		GetObject(hBitmap, sizeof(bmp), &bmp);

		glGenTextures(1, texture);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);

		glBindTexture(GL_TEXTURE_2D, *texture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, bmp.bmWidth, bmp.bmHeight, 0, GL_BGR, GL_UNSIGNED_BYTE, bmp.bmBits);
		glGenerateMipmap(GL_TEXTURE_2D);
		DeleteObject(hBitmap);
	}

	return iStatus;
}


void uninitialize(void)
{
	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);

	}

	if (gVao_cube)
	{
		glDeleteVertexArrays(1, &gVao_cube);
		gVao_cube = 0;
	}

	if (gVbo_cube)
	{
		glDeleteBuffers(1, &gVbo_cube);
		gVbo_cube = 0;
	}

	if (gTexture_Marble)
	{
		glDeleteTextures(1, &gTexture_Marble);
		gTexture_Marble = 0;
	}

	glDetachShader(ac_gShaderProgramObject, ac_gVertexShaderObject);

	glDetachShader(ac_gShaderProgramObject, ac_gFragmentShaderObject);

	glDeleteShader(ac_gVertexShaderObject);
	ac_gVertexShaderObject = 0;

	glDeleteShader(ac_gFragmentShaderObject);
	ac_gFragmentShaderObject = 0;

	glDeleteProgram(ac_gShaderProgramObject);
	ac_gShaderProgramObject = 0;

	glUseProgram(0);

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	if (gpFile)
	{
		fprintf(gpFile, "\nLog file is Successfully closed");
		fclose(gpFile);
		gpFile = NULL;
	}

	DestroyWindow(ghwnd);
	ghwnd = NULL;
}

void resize(GLint width, GLint height)
{
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	
	gPerspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void ToggleFullScreen(void)
{
	MONITORINFO mi;

	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle&WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle&~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
}

void update(void)
{
	angle_cube = angle_cube + 0.5f;
	if (angle_cube >= 360.0f)
	{
		angle_cube -= 360.0f;
	}
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(ac_gShaderProgramObject);

	//Check for light switch
	if (gbLight == true)
	{
		glUniform1i(gLKeyPressedUniform, 1);

		//light properties
		glUniform3fv(La_uniform, 1, lightAmbient);
		glUniform3fv(Ld_Uniform, 1, lightDiffuse);
		glUniform3fv(Ls_Uniform, 1, lightSpecular);
		glUniform4fv(gLightPositionUniform, 1, lightPosition);

		glUniform3fv(Ka_Uniform, 1, material_Ambient);
		glUniform3fv(Kd_Uniform, 1, material_Diffuse);
		glUniform3fv(Ks_Uniform, 1, material_Specular);
		glUniform1f(Material_shininess_uniform, material_shininess);
	}
	else
	{
		glUniform1i(gLKeyPressedUniform, 0);
	}

	//---------------------
	//Now start displaying 
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 rotationMatrix = mat4::identity();

	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	modelMatrix = translate(0.0f, 0.0f, -5.0f);
	rotationMatrix = rotate(angle_cube, angle_cube, angle_cube);
	modelMatrix = modelMatrix * rotationMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(gViewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	//------------------------

	glBindVertexArray(gVao_cube);

	//texture bind code 
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, gTexture_Marble);
	glUniform1i(gTexture_smapler_uniform, 0);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 20, 4);

	glBindVertexArray(0);

	glUseProgram(0);

	SwapBuffers(ghdc);
}

LRESULT CALLBACK AcCallBack(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void uninitialize(void);
	void resize(GLint width, GLint height);
	void ToggleFullScreen(void);

	switch (iMsg)
	{
	case WM_CREATE:
		MessageBox(NULL,TEXT("Game Window created"),TEXT("Message"),MB_OK);
		break;

	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
		{
			gbActiveWindow = true;
		}
		else
		{
			gbActiveWindow = false;
		}
		break;

	case WM_ERASEBKGND:
		return  0;

	case WM_LBUTTONDOWN:
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_CLOSE:
		uninitialize();
		break;

	case WM_KEYDOWN:
		switch (LOWORD(wParam))
		{
		case VK_ESCAPE:
			if (gbEscapeKeyIsPressed == false)
			{
				gbEscapeKeyIsPressed = true;
			}
			break;

		case 0x46:
			if (gbFullScreen == false)
			{
				ToggleFullScreen();
				gbFullScreen = true;
			}
			else
			{
				ToggleFullScreen();
				gbFullScreen = false;
			}
			break;

		case 0x4C:  //L or l
			if (bIsLkeyPressed == false)
			{
				gbLight = true;
				bIsLkeyPressed = true;
			}
			else
			{
				gbLight = false;
				bIsLkeyPressed = false;
			}
			break;

		default:
			break;
		}
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}
	return DefWindowProc(hwnd, iMsg, wParam, lParam);
}